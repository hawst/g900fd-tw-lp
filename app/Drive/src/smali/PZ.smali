.class public LPZ;
.super LPK;
.source "RenameEventBinder.java"


# direct methods
.method public constructor <init>(LPD;LaGu;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, LPK;-><init>(LPD;LaGu;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, LPZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x2

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, LPZ;->a:LPD;

    invoke-virtual {v0}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getRename()Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 27
    sget v2, Lxg;->recent_activity_type_renamed_one_user:I

    sget v3, Lxg;->recent_activity_type_renamed_many_users:I

    sget v4, Lxg;->recent_activity_type_renamed_this_file:I

    sget v5, Lxg;->recent_activity_type_renamed_this_folder:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LPZ;->a(Landroid/content/res/Resources;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, LPK;->a(Landroid/view/View;I)V

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPW;

    .line 48
    invoke-virtual {v0}, LPW;->b()V

    .line 50
    iget-object v1, p0, LPZ;->a:LPD;

    invoke-virtual {v1}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Event;->getTarget()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v1

    .line 52
    iget-object v2, p0, LPZ;->a:LPD;

    invoke-virtual {v2}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/services/appsactivity/model/Event;->getRename()Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v2

    .line 54
    if-nez p2, :cond_0

    .line 55
    invoke-virtual {v2}, Lcom/google/api/services/appsactivity/model/Rename;->getOldTitle()Ljava/lang/String;

    move-result-object v2

    .line 56
    iget-object v3, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v2, v0, LPW;->a:Landroid/widget/TextView;

    iget-object v3, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 64
    :goto_0
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Target;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v1}, LaGv;->b(Ljava/lang/String;)LaGv;

    move-result-object v2

    .line 68
    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v1

    .line 69
    iget-object v0, v0, LPW;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    return-void

    .line 59
    :cond_0
    invoke-virtual {v2}, Lcom/google/api/services/appsactivity/model/Rename;->getNewTitle()Ljava/lang/String;

    move-result-object v2

    .line 60
    iget-object v3, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v2, v0, LPW;->a:Landroid/widget/TextView;

    iget-object v3, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    and-int/lit8 v3, v3, -0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method
