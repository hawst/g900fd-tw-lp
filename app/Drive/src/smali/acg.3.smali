.class Lacg;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaFO;

.field final synthetic a:Lace;

.field final synthetic a:Ljava/util/Set;


# direct methods
.method constructor <init>(Lace;Ljava/util/Set;LaFO;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lacg;->a:Lace;

    iput-object p2, p0, Lacg;->a:Ljava/util/Set;

    iput-object p3, p0, Lacg;->a:LaFO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 108
    iget-object v0, p0, Lacg;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-object v3

    .line 111
    :cond_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 113
    iget-object v0, p0, Lacg;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqo;

    .line 114
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    invoke-virtual {v0}, Lqo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 118
    if-nez v1, :cond_2

    .line 119
    new-instance v0, Labf;

    const-string v1, "Resource IDs in acl list should not be NULL."

    invoke-direct {v0, v1, v3}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 121
    :cond_2
    if-nez v2, :cond_3

    .line 126
    :goto_2
    invoke-static {v1, v0}, LaeB;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lqo;)LaeB;

    move-result-object v2

    .line 127
    invoke-virtual {v0}, Lqo;->a()Lqv;

    move-result-object v0

    sget-object v6, Lqv;->e:Lqv;

    if-ne v0, v6, :cond_4

    .line 128
    const-string v0, "delete"

    invoke-static {v2, v0}, Lbxi;->b(Lbxb;Ljava/lang/String;)V

    .line 132
    :goto_3
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    .line 133
    goto :goto_1

    .line 123
    :cond_3
    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 124
    new-instance v0, Labf;

    const-string v1, "All resource IDs in acl list should be the same."

    invoke-direct {v0, v1, v3}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 130
    :cond_4
    const-string v0, "update"

    invoke-static {v2, v0}, Lbxi;->b(Lbxb;Ljava/lang/String;)V

    goto :goto_3

    .line 134
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    :try_start_0
    iget-object v0, p0, Lacg;->a:Lace;

    invoke-static {v0}, Lace;->a(Lace;)Lbxw;

    move-result-object v0

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lael;

    .line 140
    invoke-static {v2}, Lqz;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lacg;->a:LaFO;

    .line 139
    invoke-virtual {v0, v1, v2, v4}, Lael;->a(Ljava/lang/String;LaFO;Ljava/util/Collection;)Lbxj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-interface {v0}, Lbxj;->a()V

    goto/16 :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_1
    new-instance v1, Labf;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_6

    .line 150
    invoke-interface {v3}, Lbxj;->a()V

    :cond_6
    throw v0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    :try_start_2
    new-instance v1, Labf;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse Exception in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 145
    :catch_2
    move-exception v0

    .line 146
    new-instance v1, Labf;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication Exception in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 147
    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Labf;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_7
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lacg;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
