.class public final enum LafT;
.super Ljava/lang/Enum;
.source "ContentSyncDetailStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LafT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LafT;

.field private static final synthetic a:[LafT;

.field public static final enum b:LafT;

.field public static final enum c:LafT;

.field public static final enum d:LafT;

.field public static final enum e:LafT;

.field public static final enum f:LafT;

.field public static final enum g:LafT;

.field public static final enum h:LafT;

.field public static final enum i:LafT;

.field public static final enum j:LafT;

.field public static final enum k:LafT;

.field public static final enum l:LafT;

.field public static final enum m:LafT;

.field public static final enum n:LafT;

.field public static final enum o:LafT;

.field public static final enum p:LafT;

.field public static final enum q:LafT;

.field public static final enum r:LafT;

.field public static final enum s:LafT;


# instance fields
.field private final a:Lagg;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 9
    new-instance v0, LafT;

    const-string v1, "ATTEMPT_LIMIT_REACHED"

    const-string v2, "attempt_limit_reached"

    sget-object v3, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v5, v2, v3}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->a:LafT;

    .line 11
    new-instance v0, LafT;

    const-string v1, "AUTHENTICATION_FAILURE"

    const-string v2, "authentication_failure"

    sget-object v3, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v6, v2, v3}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->b:LafT;

    .line 13
    new-instance v0, LafT;

    const-string v1, "CANCELED"

    const-string v2, "canceled"

    sget-object v3, Lagg;->g:Lagg;

    invoke-direct {v0, v1, v7, v2, v3}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->c:LafT;

    .line 15
    new-instance v0, LafT;

    const-string v1, "COMPLETED"

    const-string v2, "completed"

    sget-object v3, Lagg;->e:Lagg;

    invoke-direct {v0, v1, v8, v2, v3}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->d:LafT;

    .line 17
    new-instance v0, LafT;

    const-string v1, "CONNECTION_FAILURE"

    const-string v2, "connection_failure"

    sget-object v3, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v9, v2, v3}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->e:LafT;

    .line 19
    new-instance v0, LafT;

    const-string v1, "DOCUMENT_UNAVAILABLE"

    const/4 v2, 0x5

    const-string v3, "document_unavailable"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->f:LafT;

    .line 21
    new-instance v0, LafT;

    const-string v1, "EXTERNAL_STORAGE_NOT_READY"

    const/4 v2, 0x6

    const-string v3, "external_storage_not_ready"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->g:LafT;

    .line 23
    new-instance v0, LafT;

    const-string v1, "INSUFFICIENT_STORAGE"

    const/4 v2, 0x7

    const-string v3, "insufficient_storage"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->h:LafT;

    .line 25
    new-instance v0, LafT;

    const-string v1, "IO_ERROR"

    const/16 v2, 0x8

    const-string v3, "io_error"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->i:LafT;

    .line 27
    new-instance v0, LafT;

    const-string v1, "NO_DATA_NETWORK"

    const/16 v2, 0x9

    const-string v3, "no_data_network"

    sget-object v4, Lagg;->b:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->j:LafT;

    .line 29
    new-instance v0, LafT;

    const-string v1, "NO_WIFI_NETWORK"

    const/16 v2, 0xa

    const-string v3, "no_wifi_network"

    sget-object v4, Lagg;->b:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->k:LafT;

    .line 31
    new-instance v0, LafT;

    const-string v1, "PENDING"

    const/16 v2, 0xb

    const-string v3, "pending"

    sget-object v4, Lagg;->a:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->l:LafT;

    .line 33
    new-instance v0, LafT;

    const-string v1, "PROCESSING"

    const/16 v2, 0xc

    const-string v3, "processing"

    sget-object v4, Lagg;->d:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->m:LafT;

    .line 35
    new-instance v0, LafT;

    const-string v1, "STARTED"

    const/16 v2, 0xd

    const-string v3, "started"

    sget-object v4, Lagg;->c:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->n:LafT;

    .line 37
    new-instance v0, LafT;

    const-string v1, "UNKNOWN_INTERNAL"

    const/16 v2, 0xe

    const-string v3, "unknown_internal"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->o:LafT;

    .line 39
    new-instance v0, LafT;

    const-string v1, "USER_INTERRUPTED"

    const/16 v2, 0xf

    const-string v3, "user_interrupted"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->p:LafT;

    .line 41
    new-instance v0, LafT;

    const-string v1, "VIDEO_UNAVAILABLE"

    const/16 v2, 0x10

    const-string v3, "video_unavailable"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->q:LafT;

    .line 43
    new-instance v0, LafT;

    const-string v1, "VIEWER_UNAVAILABLE"

    const/16 v2, 0x11

    const-string v3, "viewer unavailable"

    sget-object v4, Lagg;->f:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->r:LafT;

    .line 45
    new-instance v0, LafT;

    const-string v1, "UNSET"

    const/16 v2, 0x12

    const-string v3, "unset"

    sget-object v4, Lagg;->h:Lagg;

    invoke-direct {v0, v1, v2, v3, v4}, LafT;-><init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V

    sput-object v0, LafT;->s:LafT;

    .line 7
    const/16 v0, 0x13

    new-array v0, v0, [LafT;

    sget-object v1, LafT;->a:LafT;

    aput-object v1, v0, v5

    sget-object v1, LafT;->b:LafT;

    aput-object v1, v0, v6

    sget-object v1, LafT;->c:LafT;

    aput-object v1, v0, v7

    sget-object v1, LafT;->d:LafT;

    aput-object v1, v0, v8

    sget-object v1, LafT;->e:LafT;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LafT;->f:LafT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LafT;->g:LafT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LafT;->h:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LafT;->i:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LafT;->j:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LafT;->k:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LafT;->l:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LafT;->m:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LafT;->n:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LafT;->o:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LafT;->p:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LafT;->q:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LafT;->r:LafT;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LafT;->s:LafT;

    aput-object v2, v0, v1

    sput-object v0, LafT;->a:[LafT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lagg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lagg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-object p3, p0, LafT;->a:Ljava/lang/String;

    .line 52
    iput-object p4, p0, LafT;->a:Lagg;

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LafT;
    .locals 1

    .prologue
    .line 7
    const-class v0, LafT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LafT;

    return-object v0
.end method

.method public static values()[LafT;
    .locals 1

    .prologue
    .line 7
    sget-object v0, LafT;->a:[LafT;

    invoke-virtual {v0}, [LafT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LafT;

    return-object v0
.end method


# virtual methods
.method public a()Lagg;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, LafT;->a:Lagg;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LafT;->a:Ljava/lang/String;

    return-object v0
.end method
