.class public Laeh;
.super Ljava/lang/Object;
.source "GDataModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method provideAclFeedGDataParserFactory(LaeJ;)LbwT;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "AclFeed"
    .end annotation

    .prologue
    .line 98
    return-object p1
.end method

.method provideChangedMetadataSyncerFactory(Laei;)Laeo;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 112
    return-object p1
.end method

.method provideDocFeedGDataParserFactory(LaeN;)LbwT;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "DocFeed"
    .end annotation

    .prologue
    .line 84
    return-object p1
.end method

.method provideDocsListFeedClient(Laes;)Laer;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 71
    return-object p1
.end method

.method provideGDataAuthenticatedEntryClient(Laes;)Laeu;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "DocFeed"
    .end annotation

    .prologue
    .line 91
    return-object p1
.end method

.method provideMetadataFeedGDataParserFactory(LaeH;)LbwT;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "AccountMetadata"
    .end annotation

    .prologue
    .line 105
    return-object p1
.end method

.method provideXmlParserFactory(Laej;)Lbxr;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Laej;->a()Lbxr;

    move-result-object v0

    return-object v0
.end method
