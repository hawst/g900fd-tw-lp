.class public Laxn;
.super Ljava/lang/Object;
.source "SearchModel.java"


# instance fields
.field private final a:Lawh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawh",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Laxz;

.field private final b:Lawh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawh",
            "<",
            "Laxo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laxz;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {v1}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Laxn;->a:Lawh;

    .line 24
    invoke-static {v1}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Laxn;->b:Lawh;

    .line 29
    iput-object p1, p0, Laxn;->a:Laxz;

    .line 30
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxo;

    invoke-virtual {v0, p1}, Laxo;->a(Z)Laxo;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Laxn;->b:Lawh;

    invoke-virtual {v1, v0}, Lawh;->b(Ljava/lang/Object;)V

    .line 92
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxo;

    .line 42
    if-eqz v0, :cond_0

    iget v0, v0, Laxo;->a:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/MatchRects;)LawI;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Laxn;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 100
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxo;

    invoke-virtual {v0}, Laxo;->a()LawI;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LawI;

    invoke-direct {v0, p2}, LawI;-><init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;)V

    goto :goto_0
.end method

.method public a()Lawe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lawe",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Laxn;->a:Lawh;

    return-object v0
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 54
    iget-object v1, p0, Laxn;->b:Lawh;

    invoke-virtual {v1}, Lawh;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laxo;

    .line 55
    if-nez v1, :cond_1

    .line 57
    iget-object v1, p0, Laxn;->b:Lawh;

    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1, p2}, Laxo;->a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;)Laxo;

    move-result-object v0

    invoke-virtual {v1, v0}, Lawh;->b(Ljava/lang/Object;)V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget v2, v1, Laxo;->a:I

    if-ne v2, p1, :cond_0

    iget-object v2, v1, Laxo;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lawc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v2, p0, Laxn;->b:Lawh;

    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0, p2}, Laxo;->a(Ljava/lang/String;Lcom/google/android/apps/viewer/pdflib/MatchRects;)Laxo;

    move-result-object v0

    invoke-virtual {v2, v0}, Lawh;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V
    .locals 3

    .prologue
    .line 66
    iget-object v1, p0, Laxn;->b:Lawh;

    new-instance v2, Laxo;

    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0, p1, p2, p3}, Laxo;-><init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    invoke-virtual {v1, v2}, Lawh;->b(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {p1}, Laxn;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    iget-object v1, p0, Laxn;->a:Lawh;

    invoke-virtual {v1}, Lawh;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lawc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Laxn;->a:Lawh;

    invoke-virtual {v1, v0}, Lawh;->b(Ljava/lang/Object;)V

    .line 50
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0, p1}, Laxn;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Laxn;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxo;

    iget v1, v0, Laxo;->a:I

    if-eqz p1, :cond_3

    const/4 v0, -0x1

    :goto_1
    add-int p2, v1, v0

    .line 83
    :cond_2
    iget-object v1, p0, Laxn;->a:Laxz;

    iget-object v0, p0, Laxn;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0, p2, p1}, Laxz;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()Lawe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lawe",
            "<",
            "Laxo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Laxn;->b:Lawh;

    return-object v0
.end method
