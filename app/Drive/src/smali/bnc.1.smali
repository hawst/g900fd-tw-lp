.class public final Lbnc;
.super LbmT;
.source "ImmutableSetMultimap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbmT",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, LbmT;-><init>()V

    .line 188
    new-instance v0, Lbne;

    invoke-direct {v0}, Lbne;-><init>()V

    iput-object v0, p0, Lbnc;->a:Lbph;

    .line 189
    return-void
.end method


# virtual methods
.method public a()Lbnb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbnb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lbnc;->a:Ljava/util/Comparator;

    if-eqz v0, :cond_1

    .line 272
    new-instance v1, Lbne;

    invoke-direct {v1}, Lbne;-><init>()V

    .line 273
    iget-object v0, p0, Lbnc;->a:Lbph;

    invoke-interface {v0}, Lbph;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 275
    iget-object v2, p0, Lbnc;->a:Ljava/util/Comparator;

    invoke-static {v2}, Lbpw;->a(Ljava/util/Comparator;)Lbpw;

    move-result-object v2

    new-instance v3, Lbnd;

    invoke-direct {v3, p0}, Lbnd;-><init>(Lbnc;)V

    invoke-virtual {v2, v3}, Lbpw;->a(LbiG;)Lbpw;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 282
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 283
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v1, v3, v0}, Lbph;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 285
    :cond_0
    iput-object v1, p0, Lbnc;->a:Lbph;

    .line 287
    :cond_1
    iget-object v0, p0, Lbnc;->a:Lbph;

    iget-object v1, p0, Lbnc;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Lbnb;->a(Lbph;Ljava/util/Comparator;)Lbnb;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lbnc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lbnc",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lbnc;->a:Lbph;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lbph;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 198
    return-object p0
.end method
