.class public Lakl;
.super Ljava/lang/Object;
.source "BatteryStatus.java"


# instance fields
.field private final a:LTc;

.field private final a:Lako;

.field private volatile a:Lakp;

.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lakn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LTc;)V
    .locals 4

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lakl;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 58
    new-instance v0, Lako;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lako;-><init>(Lakl;Lakm;)V

    iput-object v0, p0, Lakl;->a:Lako;

    .line 60
    sget-object v0, Lakp;->a:Lakp;

    iput-object v0, p0, Lakl;->a:Lakp;

    .line 64
    iput-object p1, p0, Lakl;->a:LTc;

    .line 65
    invoke-interface {p1}, LTc;->a()Landroid/content/Context;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lakl;->a:Lako;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    iget-object v1, p0, Lakl;->a:Lako;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    return-void
.end method

.method static synthetic a(Lakl;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lakl;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 73
    if-eqz p2, :cond_0

    sget-object v0, Lakp;->b:Lakp;

    :goto_0
    iput-object v0, p0, Lakl;->a:Lakp;

    .line 74
    invoke-virtual {p0}, Lakl;->a()Z

    move-result v1

    .line 75
    iget-object v0, p0, Lakl;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakn;

    .line 76
    invoke-interface {v0, p1, v1}, Lakn;->a(Landroid/content/Context;Z)V

    goto :goto_1

    .line 73
    :cond_0
    sget-object v0, Lakp;->c:Lakp;

    goto :goto_0

    .line 78
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lakn;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lakl;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 82
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 83
    iget-object v2, p0, Lakl;->a:LTc;

    invoke-interface {v2}, LTc;->a()Landroid/content/Context;

    move-result-object v2

    .line 84
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 85
    const-string v2, "plugged"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 86
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b(Lakn;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lakl;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method
