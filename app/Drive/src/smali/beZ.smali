.class LbeZ;
.super LbeP;
.source "GsonGenerator.java"


# instance fields
.field private final a:LbeY;

.field private final a:Lbum;


# direct methods
.method constructor <init>(LbeY;Lbum;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, LbeP;-><init>()V

    .line 40
    iput-object p1, p0, LbeZ;->a:LbeY;

    .line 41
    iput-object p2, p0, LbeZ;->a:Lbum;

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbum;->a(Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->flush()V

    .line 49
    return-void
.end method

.method public a(D)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1, p2}, Lbum;->a(D)Lbum;

    .line 104
    return-void
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, LbeZ;->a:Lbum;

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Lbum;->a(D)Lbum;

    .line 109
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, LbeZ;->a:Lbum;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lbum;->a(J)Lbum;

    .line 89
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1, p2}, Lbum;->a(J)Lbum;

    .line 94
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1}, Lbum;->a(Ljava/lang/String;)Lbum;

    .line 79
    return-void
.end method

.method public a(Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1}, Lbum;->a(Ljava/lang/Number;)Lbum;

    .line 114
    return-void
.end method

.method public a(Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1}, Lbum;->a(Ljava/lang/Number;)Lbum;

    .line 99
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1}, Lbum;->a(Z)Lbum;

    .line 64
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->a()Lbum;

    .line 164
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0, p1}, Lbum;->b(Ljava/lang/String;)Lbum;

    .line 174
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->b()Lbum;

    .line 69
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->c()Lbum;

    .line 169
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->d()Lbum;

    .line 74
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LbeZ;->a:Lbum;

    invoke-virtual {v0}, Lbum;->e()Lbum;

    .line 84
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, LbeZ;->a:Lbum;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Lbum;->a(Ljava/lang/String;)V

    .line 179
    return-void
.end method
