.class public final LHc;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHm;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGZ;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHn;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LHc;->a:LbrA;

    .line 44
    const-class v0, LHm;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->a:Lbsk;

    .line 47
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->b:Lbsk;

    .line 50
    const-class v0, LGZ;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->c:Lbsk;

    .line 53
    const-class v0, LHn;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->d:Lbsk;

    .line 56
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->e:Lbsk;

    .line 59
    const-class v0, LHa;

    invoke-static {v0, v1}, LHc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHc;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_0

    .line 193
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :pswitch_1
    new-instance v0, LHn;

    iget-object v1, p0, LHc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 100
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LHc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 98
    invoke-static {v1, v2}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LHc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LGu;

    iget-object v2, v2, LGu;->G:Lbsk;

    .line 106
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LHc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->G:Lbsk;

    .line 104
    invoke-static {v2, v3}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v3, p0, LHc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->E:Lbsk;

    .line 112
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LHc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LGu;

    iget-object v4, v4, LGu;->E:Lbsk;

    .line 110
    invoke-static {v3, v4}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaHL;

    iget-object v4, p0, LHc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LHc;

    iget-object v4, v4, LHc;->c:Lbsk;

    .line 118
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LHc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LHc;

    iget-object v5, v5, LHc;->c:Lbsk;

    .line 116
    invoke-static {v4, v5}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LGZ;

    iget-object v5, p0, LHc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 124
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LHc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 122
    invoke-static {v5, v6}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LtK;

    iget-object v6, p0, LHc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LqD;

    iget-object v6, v6, LqD;->c:Lbsk;

    .line 130
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LHc;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LqD;

    iget-object v7, v7, LqD;->c:Lbsk;

    .line 128
    invoke-static {v6, v7}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LqK;

    invoke-direct/range {v0 .. v6}, LHn;-><init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;LaHL;LGZ;LtK;LqK;)V

    .line 191
    :goto_0
    return-object v0

    .line 137
    :pswitch_2
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    iget-object v1, p0, LHc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 140
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LHc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 138
    invoke-static {v1, v2}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LHc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LGu;

    iget-object v2, v2, LGu;->G:Lbsk;

    .line 146
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LHc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->G:Lbsk;

    .line 144
    invoke-static {v2, v3}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v3, p0, LHc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->E:Lbsk;

    .line 152
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LHc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LGu;

    iget-object v4, v4, LGu;->E:Lbsk;

    .line 150
    invoke-static {v3, v4}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaHL;

    iget-object v4, p0, LHc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LHc;

    iget-object v4, v4, LHc;->c:Lbsk;

    .line 158
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LHc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LHc;

    iget-object v5, v5, LHc;->c:Lbsk;

    .line 156
    invoke-static {v4, v5}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LGZ;

    iget-object v5, p0, LHc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 164
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LHc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 162
    invoke-static {v5, v6}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LtK;

    iget-object v6, p0, LHc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LGu;

    iget-object v6, v6, LGu;->C:Lbsk;

    .line 170
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LHc;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LGu;

    iget-object v7, v7, LGu;->C:Lbsk;

    .line 168
    invoke-static {v6, v7}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    iget-object v7, p0, LHc;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LqD;

    iget-object v7, v7, LqD;->c:Lbsk;

    .line 176
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LHc;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LqD;

    iget-object v8, v8, LqD;->c:Lbsk;

    .line 174
    invoke-static {v7, v8}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LqK;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;-><init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;LaHL;LGZ;LtK;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;LqK;)V

    goto/16 :goto_0

    .line 183
    :pswitch_3
    new-instance v1, LHa;

    iget-object v0, p0, LHc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->f:Lbsk;

    .line 186
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LHc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->f:Lbsk;

    .line 184
    invoke-static {v0, v2}, LHc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeo;

    invoke-direct {v1, v0}, LHa;-><init>(Laeo;)V

    move-object v0, v1

    .line 191
    goto/16 :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x466
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 208
    .line 210
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 69
    const-class v0, LHm;

    iget-object v1, p0, LHc;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 70
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    iget-object v1, p0, LHc;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 71
    const-class v0, LGZ;

    iget-object v1, p0, LHc;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 72
    const-class v0, LHn;

    iget-object v1, p0, LHc;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 73
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    iget-object v1, p0, LHc;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 74
    const-class v0, LHa;

    iget-object v1, p0, LHc;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LHc;->a(Ljava/lang/Class;Lbsk;)V

    .line 75
    iget-object v0, p0, LHc;->a:Lbsk;

    iget-object v1, p0, LHc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->d:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 77
    iget-object v0, p0, LHc;->b:Lbsk;

    iget-object v1, p0, LHc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->e:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 79
    iget-object v0, p0, LHc;->c:Lbsk;

    iget-object v1, p0, LHc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->f:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 81
    iget-object v0, p0, LHc;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x466

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 83
    iget-object v0, p0, LHc;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x468

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 85
    iget-object v0, p0, LHc;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x469

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 200
    .line 202
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
