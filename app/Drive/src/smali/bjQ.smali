.class LbjQ;
.super Ljava/util/AbstractMap;
.source "LocalCache.java"

# interfaces
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:LbkF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbkF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field static final a:LbsW;

.field static final a:Ljava/util/logging/Logger;

.field static final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:I

.field final a:J

.field final a:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final a:LbjA;

.field final a:LbjE;

.field final a:LbjM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjM",
            "<-TK;TV;>;"
        }
    .end annotation
.end field

.field final a:LbjY;

.field final a:Lbkv;

.field final a:Lblf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lblf",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Lbln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbln",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lblg",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field final a:[Lbks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lbks",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final b:I

.field final b:J

.field final b:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lbkv;

.field b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final c:J

.field final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const-class v0, LbjQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LbjQ;->a:Ljava/util/logging/Logger;

    .line 157
    invoke-static {}, LbsY;->a()LbsW;

    move-result-object v0

    sput-object v0, LbjQ;->a:LbsW;

    .line 690
    new-instance v0, LbjR;

    invoke-direct {v0}, LbjR;-><init>()V

    sput-object v0, LbjQ;->a:LbkF;

    .line 983
    new-instance v0, LbjS;

    invoke-direct {v0}, LbjS;-><init>()V

    sput-object v0, LbjQ;->b:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(LbjG;LbjM;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbjG",
            "<-TK;-TV;>;",
            "LbjM",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 235
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 236
    invoke-virtual {p1}, LbjG;->b()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LbjQ;->c:I

    .line 238
    invoke-virtual {p1}, LbjG;->a()Lbkv;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:Lbkv;

    .line 239
    invoke-virtual {p1}, LbjG;->b()Lbkv;

    move-result-object v0

    iput-object v0, p0, LbjQ;->b:Lbkv;

    .line 241
    invoke-virtual {p1}, LbjG;->a()LbiD;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:LbiD;

    .line 242
    invoke-virtual {p1}, LbjG;->b()LbiD;

    move-result-object v0

    iput-object v0, p0, LbjQ;->b:LbiD;

    .line 244
    invoke-virtual {p1}, LbjG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LbjQ;->a:J

    .line 245
    invoke-virtual {p1}, LbjG;->a()Lbln;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:Lbln;

    .line 246
    invoke-virtual {p1}, LbjG;->c()J

    move-result-wide v0

    iput-wide v0, p0, LbjQ;->b:J

    .line 247
    invoke-virtual {p1}, LbjG;->b()J

    move-result-wide v0

    iput-wide v0, p0, LbjQ;->c:J

    .line 248
    invoke-virtual {p1}, LbjG;->d()J

    move-result-wide v0

    iput-wide v0, p0, LbjQ;->d:J

    .line 250
    invoke-virtual {p1}, LbjG;->a()Lblf;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:Lblf;

    .line 251
    iget-object v0, p0, LbjQ;->a:Lblf;

    sget-object v1, LbjK;->a:LbjK;

    if-ne v0, v1, :cond_2

    invoke-static {}, LbjQ;->a()Ljava/util/Queue;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LbjQ;->a:Ljava/util/Queue;

    .line 255
    invoke-virtual {p0}, LbjQ;->j()Z

    move-result v0

    invoke-virtual {p1, v0}, LbjG;->a(Z)LbjA;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:LbjA;

    .line 256
    iget-object v0, p0, LbjQ;->a:Lbkv;

    invoke-virtual {p0}, LbjQ;->l()Z

    move-result v1

    invoke-virtual {p0}, LbjQ;->k()Z

    move-result v3

    invoke-static {v0, v1, v3}, LbjY;->a(Lbkv;ZZ)LbjY;

    move-result-object v0

    iput-object v0, p0, LbjQ;->a:LbjY;

    .line 257
    invoke-virtual {p1}, LbjG;->a()Lbjv;

    move-result-object v0

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjE;

    iput-object v0, p0, LbjQ;->a:LbjE;

    .line 258
    iput-object p2, p0, LbjQ;->a:LbjM;

    .line 260
    invoke-virtual {p1}, LbjG;->a()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 261
    invoke-virtual {p0}, LbjQ;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LbjQ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    iget-wide v6, p0, LbjQ;->a:J

    long-to-int v1, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    move v1, v2

    move v3, v4

    .line 272
    :goto_1
    iget v5, p0, LbjQ;->c:I

    if-ge v1, v5, :cond_3

    invoke-virtual {p0}, LbjQ;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    mul-int/lit8 v5, v1, 0x14

    int-to-long v6, v5

    iget-wide v8, p0, LbjQ;->a:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_3

    .line 273
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 274
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 251
    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_0

    .line 276
    :cond_3
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, LbjQ;->b:I

    .line 277
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, LbjQ;->a:I

    .line 279
    invoke-virtual {p0, v1}, LbjQ;->a(I)[Lbks;

    move-result-object v3

    iput-object v3, p0, LbjQ;->a:[Lbks;

    .line 281
    div-int v3, v0, v1

    .line 282
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_8

    .line 283
    add-int/lit8 v0, v3, 0x1

    :goto_2
    move v5, v2

    .line 287
    :goto_3
    if-ge v5, v0, :cond_4

    .line 288
    shl-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 291
    :cond_4
    invoke-virtual {p0}, LbjQ;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 293
    iget-wide v2, p0, LbjQ;->a:J

    int-to-long v6, v1

    div-long/2addr v2, v6

    add-long/2addr v2, v10

    .line 294
    iget-wide v6, p0, LbjQ;->a:J

    int-to-long v0, v1

    rem-long/2addr v6, v0

    move-wide v0, v2

    .line 295
    :goto_4
    iget-object v2, p0, LbjQ;->a:[Lbks;

    array-length v2, v2

    if-ge v4, v2, :cond_6

    .line 296
    int-to-long v2, v4

    cmp-long v2, v2, v6

    if-nez v2, :cond_7

    .line 297
    sub-long v2, v0, v10

    .line 299
    :goto_5
    iget-object v1, p0, LbjQ;->a:[Lbks;

    invoke-virtual {p1}, LbjG;->a()Lbjv;

    move-result-object v0

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjE;

    invoke-virtual {p0, v5, v2, v3, v0}, LbjQ;->a(IJLbjE;)Lbks;

    move-result-object v0

    aput-object v0, v1, v4

    .line 295
    add-int/lit8 v4, v4, 0x1

    move-wide v0, v2

    goto :goto_4

    .line 303
    :cond_5
    :goto_6
    iget-object v0, p0, LbjQ;->a:[Lbks;

    array-length v0, v0

    if-ge v4, v0, :cond_6

    .line 304
    iget-object v1, p0, LbjQ;->a:[Lbks;

    const-wide/16 v2, -0x1

    invoke-virtual {p1}, LbjG;->a()Lbjv;

    move-result-object v0

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbjE;

    invoke-virtual {p0, v5, v2, v3, v0}, LbjQ;->a(IJLbjE;)Lbks;

    move-result-object v0

    aput-object v0, v1, v4

    .line 303
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 308
    :cond_6
    return-void

    :cond_7
    move-wide v2, v0

    goto :goto_5

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method static a(I)I
    .locals 3

    .prologue
    .line 1759
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 1760
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 1761
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 1762
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 1763
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1764
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static a()LbkF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LbkF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 730
    sget-object v0, LbjQ;->a:LbkF;

    return-object v0
.end method

.method static a()Lbkr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbkr",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 980
    sget-object v0, Lbkq;->a:Lbkq;

    return-object v0
.end method

.method static a()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1014
    sget-object v0, LbjQ;->b:Ljava/util/Queue;

    return-object v0
.end method

.method static a(Lbkr;Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1878
    invoke-interface {p0, p1}, Lbkr;->a(Lbkr;)V

    .line 1879
    invoke-interface {p1, p0}, Lbkr;->b(Lbkr;)V

    .line 1880
    return-void
.end method

.method static b(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1884
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    .line 1885
    invoke-interface {p0, v0}, Lbkr;->a(Lbkr;)V

    .line 1886
    invoke-interface {p0, v0}, Lbkr;->b(Lbkr;)V

    .line 1887
    return-void
.end method

.method static b(Lbkr;Lbkr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1891
    invoke-interface {p0, p1}, Lbkr;->c(Lbkr;)V

    .line 1892
    invoke-interface {p1, p0}, Lbkr;->d(Lbkr;)V

    .line 1893
    return-void
.end method

.method static c(Lbkr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1897
    invoke-static {}, LbjQ;->a()Lbkr;

    move-result-object v0

    .line 1898
    invoke-interface {p0, v0}, Lbkr;->c(Lbkr;)V

    .line 1899
    invoke-interface {p0, v0}, Lbkr;->d(Lbkr;)V

    .line 1900
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1797
    iget-object v0, p0, LbjQ;->a:LbiD;

    invoke-virtual {v0, p1}, LbiD;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1798
    invoke-static {v0}, LbjQ;->a(I)I

    move-result v0

    return v0
.end method

.method a()J
    .locals 6

    .prologue
    .line 3839
    iget-object v1, p0, LbjQ;->a:[Lbks;

    .line 3840
    const-wide/16 v2, 0x0

    .line 3841
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 3842
    aget-object v4, v1, v0

    iget v4, v4, Lbks;->a:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3841
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3844
    :cond_0
    return-wide v2
.end method

.method a(I)Lbks;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbks",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1829
    iget-object v0, p0, LbjQ;->a:[Lbks;

    iget v1, p0, LbjQ;->b:I

    ushr-int v1, p1, v1

    iget v2, p0, LbjQ;->a:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method a(IJLbjE;)Lbks;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "LbjE;",
            ")",
            "Lbks",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1833
    new-instance v1, Lbks;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lbks;-><init>(LbjQ;IJLbjE;)V

    return-object v1
.end method

.method a(Lbkr;J)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1844
    invoke-interface {p1}, Lbkr;->a()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1855
    :cond_0
    :goto_0
    return-object v0

    .line 1847
    :cond_1
    invoke-interface {p1}, Lbkr;->a()LbkF;

    move-result-object v1

    invoke-interface {v1}, LbkF;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1848
    if-eqz v1, :cond_0

    .line 1852
    invoke-virtual {p0, p1, p2, p3}, LbjQ;->a(Lbkr;J)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1855
    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3864
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3865
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lbks;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 3866
    if-nez v0, :cond_0

    .line 3867
    iget-object v1, p0, LbjQ;->a:LbjE;

    invoke-interface {v1, v2}, LbjE;->b(I)V

    .line 3871
    :goto_0
    return-object v0

    .line 3869
    :cond_0
    iget-object v1, p0, LbjQ;->a:LbjE;

    invoke-interface {v1, v2}, LbjE;->a(I)V

    goto :goto_0
.end method

.method a(Ljava/lang/Object;LbjM;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LbjM",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 3875
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3876
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lbks;->a(Ljava/lang/Object;ILbjM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .locals 4

    .prologue
    .line 1909
    :goto_0
    iget-object v0, p0, LbjQ;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblg;

    if-eqz v0, :cond_0

    .line 1911
    :try_start_0
    iget-object v1, p0, LbjQ;->a:Lblf;

    invoke-interface {v1, v0}, Lblf;->a(Lblg;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1912
    :catch_0
    move-exception v0

    .line 1913
    sget-object v1, LbjQ;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1916
    :cond_0
    return-void
.end method

.method a(LbkF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbkF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1802
    invoke-interface {p1}, LbkF;->a()Lbkr;

    move-result-object v0

    .line 1803
    invoke-interface {v0}, Lbkr;->a()I

    move-result v1

    .line 1804
    invoke-virtual {p0, v1}, LbjQ;->a(I)Lbks;

    move-result-object v2

    invoke-interface {v0}, Lbkr;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lbks;->a(Ljava/lang/Object;ILbkF;)Z

    .line 1805
    return-void
.end method

.method a(Lbkr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1808
    invoke-interface {p1}, Lbkr;->a()I

    move-result v0

    .line 1809
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lbks;->a(Lbkr;I)Z

    .line 1810
    return-void
.end method

.method a()Z
    .locals 4

    .prologue
    .line 311
    iget-wide v0, p0, LbjQ;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lbkr;J)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkr",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1864
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865
    invoke-virtual {p0}, LbjQ;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lbkr;->a()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, LbjQ;->b:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 1871
    :cond_0
    :goto_0
    return v0

    .line 1868
    :cond_1
    invoke-virtual {p0}, LbjQ;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lbkr;->b()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-wide v4, p0, LbjQ;->c:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1871
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(I)[Lbks;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lbks",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1920
    new-array v0, p1, [Lbks;

    return-object v0
.end method

.method b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 3880
    iget-object v0, p0, LbjQ;->a:LbjM;

    invoke-virtual {p0, p1, v0}, LbjQ;->a(Ljava/lang/Object;LbjM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method b()Z
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, LbjQ;->a:Lbln;

    sget-object v1, LbjL;->a:LbjL;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 4

    .prologue
    .line 323
    iget-wide v0, p0, LbjQ;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 4142
    iget-object v1, p0, LbjQ;->a:[Lbks;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 4143
    invoke-virtual {v3}, Lbks;->k()V

    .line 4142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4145
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 4036
    if-nez p1, :cond_0

    .line 4037
    const/4 v0, 0x0

    .line 4040
    :goto_0
    return v0

    .line 4039
    :cond_0
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4040
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lbks;->a(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 20

    .prologue
    .line 4046
    if-nez p1, :cond_0

    .line 4047
    const/4 v4, 0x0

    .line 4081
    :goto_0
    return v4

    .line 4055
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LbjQ;->a:LbjA;

    invoke-virtual {v4}, LbjA;->a()J

    move-result-wide v14

    .line 4056
    move-object/from16 v0, p0

    iget-object v11, v0, LbjQ;->a:[Lbks;

    .line 4057
    const-wide/16 v8, -0x1

    .line 4058
    const/4 v4, 0x0

    move v10, v4

    move-wide v12, v8

    :goto_1
    const/4 v4, 0x3

    if-ge v10, v4, :cond_5

    .line 4059
    const-wide/16 v6, 0x0

    .line 4060
    array-length v0, v11

    move/from16 v16, v0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_4

    aget-object v7, v11, v6

    .line 4063
    iget v4, v7, Lbks;->a:I

    .line 4065
    iget-object v0, v7, Lbks;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-object/from16 v17, v0

    .line 4066
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 4067
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbkr;

    :goto_4
    if-eqz v4, :cond_2

    .line 4068
    invoke-virtual {v7, v4, v14, v15}, Lbks;->a(Lbkr;J)Ljava/lang/Object;

    move-result-object v18

    .line 4069
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, LbjQ;->b:LbiD;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LbiD;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 4070
    const/4 v4, 0x1

    goto :goto_0

    .line 4067
    :cond_1
    invoke-interface {v4}, Lbkr;->a()Lbkr;

    move-result-object v4

    goto :goto_4

    .line 4066
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 4074
    :cond_3
    iget v4, v7, Lbks;->c:I

    int-to-long v4, v4

    add-long/2addr v8, v4

    .line 4060
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    .line 4076
    :cond_4
    cmp-long v4, v8, v12

    if-nez v4, :cond_6

    .line 4081
    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    .line 4058
    :cond_6
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move-wide v12, v8

    goto :goto_1
.end method

.method d()Z
    .locals 4

    .prologue
    .line 327
    iget-wide v0, p0, LbjQ;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Z
    .locals 4

    .prologue
    .line 331
    iget-wide v0, p0, LbjQ;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 4178
    iget-object v0, p0, LbjQ;->b:Ljava/util/Set;

    .line 4179
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbki;

    invoke-direct {v0, p0, p0}, Lbki;-><init>(LbjQ;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LbjQ;->b:Ljava/util/Set;

    goto :goto_0
.end method

.method f()Z
    .locals 1

    .prologue
    .line 335
    invoke-virtual {p0}, LbjQ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbjQ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g()Z
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, LbjQ;->c()Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3855
    if-nez p1, :cond_0

    .line 3856
    const/4 v0, 0x0

    .line 3859
    :goto_0
    return-object v0

    .line 3858
    :cond_0
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3859
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lbks;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method h()Z
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0}, LbjQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbjQ;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()Z
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, LbjQ;->d()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3815
    .line 3816
    iget-object v6, p0, LbjQ;->a:[Lbks;

    move v0, v1

    move-wide v2, v4

    .line 3817
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3818
    aget-object v7, v6, v0

    iget v7, v7, Lbks;->a:I

    if-eqz v7, :cond_1

    .line 3835
    :cond_0
    :goto_1
    return v1

    .line 3821
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lbks;->c:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3817
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3824
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3825
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3826
    aget-object v7, v6, v0

    iget v7, v7, Lbks;->a:I

    if-nez v7, :cond_0

    .line 3829
    aget-object v7, v6, v0

    iget v7, v7, Lbks;->c:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3825
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3831
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3835
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method j()Z
    .locals 1

    .prologue
    .line 351
    invoke-virtual {p0}, LbjQ;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbjQ;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()Z
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, LbjQ;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbjQ;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 4159
    iget-object v0, p0, LbjQ;->a:Ljava/util/Set;

    .line 4160
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbkl;

    invoke-direct {v0, p0, p0}, Lbkl;-><init>(LbjQ;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LbjQ;->a:Ljava/util/Set;

    goto :goto_0
.end method

.method l()Z
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p0}, LbjQ;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbjQ;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method m()Z
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, LbjQ;->a:Lbkv;

    sget-object v1, Lbkv;->a:Lbkv;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()Z
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, LbjQ;->b:Lbkv;

    sget-object v1, Lbkv;->a:Lbkv;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4086
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4087
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4088
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4089
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lbks;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 4101
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LbjQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4104
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4093
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4094
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4095
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4096
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lbks;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 4108
    if-nez p1, :cond_0

    .line 4109
    const/4 v0, 0x0

    .line 4112
    :goto_0
    return-object v0

    .line 4111
    :cond_0
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4112
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lbks;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 4116
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 4117
    :cond_0
    const/4 v0, 0x0

    .line 4120
    :goto_0
    return v0

    .line 4119
    :cond_1
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4120
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lbks;->a(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 4134
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4135
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4136
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4137
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lbks;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 4124
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4125
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4126
    if-nez p2, :cond_0

    .line 4127
    const/4 v0, 0x0

    .line 4130
    :goto_0
    return v0

    .line 4129
    :cond_0
    invoke-virtual {p0, p1}, LbjQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 4130
    invoke-virtual {p0, v0}, LbjQ;->a(I)Lbks;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lbks;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 3849
    invoke-virtual {p0}, LbjQ;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbsy;->b(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 4168
    iget-object v0, p0, LbjQ;->a:Ljava/util/Collection;

    .line 4169
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LbkG;

    invoke-direct {v0, p0, p0}, LbkG;-><init>(LbjQ;Ljava/util/concurrent/ConcurrentMap;)V

    iput-object v0, p0, LbjQ;->a:Ljava/util/Collection;

    goto :goto_0
.end method
