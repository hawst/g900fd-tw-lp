.class public final enum LalQ;
.super Ljava/lang/Enum;
.source "LightOutMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LalQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LalQ;

.field private static final synthetic a:[LalQ;

.field public static final enum b:LalQ;


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, LalQ;

    const-string v1, "LIGHTS_OUT"

    invoke-direct {v0, v1, v2, v3}, LalQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LalQ;->a:LalQ;

    .line 18
    new-instance v0, LalQ;

    const-string v1, "LIGHTS_ON"

    invoke-direct {v0, v1, v3, v2}, LalQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LalQ;->b:LalQ;

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [LalQ;

    sget-object v1, LalQ;->a:LalQ;

    aput-object v1, v0, v2

    sget-object v1, LalQ;->b:LalQ;

    aput-object v1, v0, v3

    sput-object v0, LalQ;->a:[LalQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput-boolean p3, p0, LalQ;->a:Z

    .line 24
    return-void
.end method

.method public static a(LalR;LalQ;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x10

    .line 47
    iget-boolean v0, p1, LalQ;->a:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p0, v0}, LalR;->c(Z)V

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_2

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    .line 53
    const/16 v1, 0x500

    .line 56
    :cond_0
    iget-boolean v0, p1, LalQ;->a:Z

    if-eqz v0, :cond_1

    .line 57
    or-int/lit8 v1, v1, 0x1

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    .line 59
    or-int/lit8 v1, v1, 0x4

    .line 63
    :cond_1
    invoke-interface {p0}, LalR;->a()Landroid/view/View;

    move-result-object v0

    .line 64
    if-nez v0, :cond_4

    .line 75
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 47
    goto :goto_0

    .line 68
    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 69
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 70
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_2

    .line 71
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 72
    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LalQ;
    .locals 1

    .prologue
    .line 16
    const-class v0, LalQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LalQ;

    return-object v0
.end method

.method public static values()[LalQ;
    .locals 1

    .prologue
    .line 16
    sget-object v0, LalQ;->a:[LalQ;

    invoke-virtual {v0}, [LalQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LalQ;

    return-object v0
.end method
