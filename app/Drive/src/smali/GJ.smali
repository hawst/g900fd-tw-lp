.class public LGJ;
.super Landroid/webkit/WebViewClient;
.source "UrlLoadingWebViewClient.java"


# static fields
.field public static final a:Ljava/lang/String;

.field static final synthetic a:Z


# instance fields
.field private final a:LGH;

.field private final a:LQr;

.field private final a:LTd;

.field private final a:LUT;

.field private final a:LaFO;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/content/SharedPreferences;

.field private final a:Landroid/os/Handler;

.field private final a:LbiU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiU",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/regex/Pattern;

.field private b:LaFO;

.field private final b:Ljava/lang/String;

.field private final b:Ljava/util/regex/Pattern;

.field private b:Z

.field private final c:Ljava/util/regex/Pattern;

.field private final d:Ljava/util/regex/Pattern;

.field private final e:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, LGJ;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LGJ;->a:Z

    .line 80
    const-string v0, "https://drive.google.com/?androidweblogin"

    .line 81
    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LGJ;->a:Ljava/lang/String;

    .line 80
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LGH;",
            "LaFO;",
            "LQr;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;",
            "LUT;",
            "Landroid/content/SharedPreferences;",
            "LTd;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, LGJ;->b:Z

    .line 164
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LGJ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 186
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    iput-object p1, p0, LGJ;->a:Landroid/content/Context;

    .line 195
    iput-object p2, p0, LGJ;->a:LGH;

    .line 196
    const-string v0, "whitelistUrl"

    const-string v1, "https://accounts\\.google(\\.co(m?))?(\\.\\w{2})?/.*"

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LGJ;->a:Ljava/util/regex/Pattern;

    .line 198
    const-string v0, "whitelistPath"

    const-string v1, "(/TokenAuth|/accounts(?:/.+)?)"

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LGJ;->b:Ljava/util/regex/Pattern;

    .line 200
    const-string v0, "homePath"

    const-string v1, "/(m?|(fe/m)?)"

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LGJ;->c:Ljava/util/regex/Pattern;

    .line 202
    const-string v0, "gaiaLoginPathPattern"

    const-string v1, ".*/ServiceLogin$"

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LGJ;->d:Ljava/util/regex/Pattern;

    .line 204
    const-string v0, "gaiaLogoutPathPattern"

    const-string v1, ".*/logout$"

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LGJ;->e:Ljava/util/regex/Pattern;

    .line 206
    iput-object p3, p0, LGJ;->a:LaFO;

    .line 207
    iput-object p5, p0, LGJ;->a:Ljava/lang/Class;

    .line 208
    iput-object p6, p0, LGJ;->a:LUT;

    .line 209
    iput-object p7, p0, LGJ;->a:Landroid/content/SharedPreferences;

    .line 210
    iput-object p8, p0, LGJ;->a:LTd;

    .line 211
    iput-object p9, p0, LGJ;->a:Landroid/os/Handler;

    .line 212
    iput-object p4, p0, LGJ;->a:LQr;

    .line 213
    const-string v0, "webloginEncodedContinueUrl"

    sget-object v1, LGJ;->a:Ljava/lang/String;

    invoke-interface {p4, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LGJ;->b:Ljava/lang/String;

    .line 215
    iget-object v0, p0, LGJ;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 216
    const-string v1, "webloginAlternateContinueUrlRegex"

    .line 217
    invoke-interface {p4, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 218
    new-instance v2, LGK;

    invoke-direct {v2, p0, v1, v0}, LGK;-><init>(LGJ;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v2, p0, LGJ;->a:LbiU;

    .line 230
    invoke-direct {p0}, LGJ;->c()V

    .line 231
    return-void
.end method

.method static synthetic a(LGJ;)LGH;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->a:LGH;

    return-object v0
.end method

.method static synthetic a(LGJ;)LQr;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->a:LQr;

    return-object v0
.end method

.method static synthetic a(LGJ;)LTd;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->a:LTd;

    return-object v0
.end method

.method static synthetic a(LGJ;LaFO;)LaFO;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, LGJ;->b:LaFO;

    return-object p1
.end method

.method static synthetic a(LGJ;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LGJ;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->b:Ljava/lang/String;

    return-object v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 389
    if-nez p1, :cond_1

    .line 390
    const-string p1, ""

    .line 399
    :cond_0
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 408
    sget v0, Lxi;->error_ssl_generic_template:I

    .line 411
    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 412
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 413
    return-object v0

    .line 392
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 393
    if-eqz v0, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 402
    :pswitch_0
    sget v0, Lxi;->error_ssl_validity_template:I

    goto :goto_1

    .line 405
    :pswitch_1
    sget v0, Lxi;->error_ssl_idmismatch_template:I

    goto :goto_1

    .line 399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(LGJ;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LGJ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic a(LGJ;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, LGJ;->d()V

    return-void
.end method

.method static synthetic a(LGJ;LaFO;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, LGJ;->a(LaFO;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(LGJ;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, LGJ;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method private a(LaFO;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 535
    const-string v0, "UrlLoadingWebViewClient"

    const-string v1, "in loadSecureUrl"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v0, p0, LGJ;->a:Landroid/os/Handler;

    new-instance v1, LGL;

    invoke-direct {v1, p0, p1, p2}, LGL;-><init>(LGJ;LaFO;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 551
    return-void
.end method

.method private a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 555
    const-string v0, "UrlLoadingWebViewClient"

    const-string v1, "in loginUser"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    new-instance v0, LGM;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LGM;-><init>(LGJ;LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-interface {v1, v0}, LGH;->a(LDL;)V

    .line 616
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 504
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 505
    const-string v1, "ncl"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 506
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LGH;->a(Ljava/lang/String;)V

    .line 507
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 619
    const-string v0, "UrlLoadingWebViewClient"

    const-string v1, "in showAuthenticationError %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 620
    iget-object v0, p0, LGJ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->authentication_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 621
    iget-object v1, p0, LGJ;->a:Landroid/os/Handler;

    new-instance v2, LGN;

    invoke-direct {v2, p0, v0}, LGN;-><init>(LGJ;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 632
    return-void
.end method

.method static synthetic a(LGJ;Z)Z
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, LGJ;->b:Z

    return p1
.end method

.method static a(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-static {p1}, Lanl;->a(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v0

    .line 284
    invoke-static {p0}, Lanl;->a(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v1

    .line 283
    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 285
    :goto_0
    return v0

    .line 283
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 449
    iget-object v2, p0, LGJ;->a:LUT;

    iget-object v3, p0, LGJ;->a:Landroid/content/Context;

    invoke-interface {v2, v3, p2}, LUT;->a(Landroid/content/Context;Landroid/net/Uri;)LVa;

    move-result-object v2

    .line 451
    iget-object v3, p0, LGJ;->a:LGH;

    invoke-interface {v3}, LGH;->a()LGI;

    move-result-object v3

    .line 452
    invoke-virtual {v2}, LVa;->a()Ljava/lang/String;

    move-result-object v4

    .line 453
    invoke-interface {v3}, LGI;->a()LVa;

    move-result-object v5

    invoke-virtual {v5}, LVa;->a()Ljava/lang/String;

    move-result-object v5

    .line 454
    const-string v6, "UrlLoadingWebViewClient"

    const-string v7, "Current resource id %s new uri %s parsed resource id %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v1

    aput-object p2, v8, v0

    const/4 v9, 0x2

    aput-object v4, v8, v9

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 456
    if-eqz v4, :cond_4

    .line 458
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 465
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v6

    sget-object v7, LaGv;->g:LaGv;

    invoke-virtual {v6, v7}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 466
    invoke-interface {v3}, LGI;->a()LVa;

    move-result-object v6

    invoke-virtual {v6}, LVa;->a()LaGv;

    move-result-object v6

    sget-object v7, LaGv;->g:LaGv;

    invoke-virtual {v6, v7}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 467
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0xa

    if-ge v4, v5, :cond_0

    .line 468
    invoke-direct {p0, p2}, LGJ;->a(Landroid/net/Uri;)V

    .line 499
    :goto_0
    return v0

    .line 470
    :cond_0
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v2

    sget-object v4, LaGv;->i:LaGv;

    invoke-virtual {v2, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 471
    invoke-interface {v3}, LGI;->a()LVa;

    move-result-object v2

    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v2

    sget-object v3, LaGv;->i:LaGv;

    invoke-virtual {v2, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 474
    goto :goto_0

    .line 477
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 478
    iget-object v3, p0, LGJ;->a:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 479
    const-string v3, "accountName"

    iget-object v4, p0, LGJ;->a:LaFO;

    invoke-static {v4}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    :try_start_0
    iget-object v3, p0, LGJ;->a:LGH;

    invoke-interface {v3, v2}, LGH;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 483
    :catch_0
    move-exception v0

    .line 485
    sget-boolean v0, LGJ;->a:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 488
    :cond_2
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v3

    sget-object v4, LaGv;->g:LaGv;

    invoke-virtual {v3, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "ncl=true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 491
    invoke-direct {p0, p2}, LGJ;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 493
    :cond_3
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v2

    sget-object v3, LaGv;->b:LaGv;

    invoke-virtual {v2, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "source=cm"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 494
    invoke-direct {p0, p2}, LGJ;->b(Landroid/net/Uri;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 499
    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 436
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 437
    iget-object v1, p0, LGJ;->a:Landroid/content/Context;

    iget-object v2, p0, LGJ;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 438
    const/high16 v1, 0x24080000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 441
    const-string v1, "preserveOriginalIntent"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 442
    const-string v1, "accountName"

    iget-object v2, p0, LGJ;->a:LaFO;

    invoke-static {v2}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-interface {v1, v0}, LGH;->a(Landroid/content/Intent;)V

    .line 444
    return-void
.end method

.method private b(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 512
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 513
    const-string v1, "source"

    const-string v2, "cm"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 519
    const-string v1, "viewopt"

    const-string v2, "33"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 520
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LGH;->a(Ljava/lang/String;)V

    .line 521
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 524
    iget-object v0, p0, LGJ;->a:Landroid/content/SharedPreferences;

    const-string v1, "currentAccount"

    const/4 v2, 0x0

    .line 525
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LGJ;->b:LaFO;

    .line 526
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 421
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 422
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 424
    :try_start_0
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-interface {v1, v0}, LGH;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :goto_0
    return-void

    .line 425
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 529
    iget-object v0, p0, LGJ;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 530
    const-string v1, "currentAccount"

    iget-object v2, p0, LGJ;->b:LaFO;

    invoke-static {v2}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 531
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 532
    return-void
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, LGJ;->b:LaFO;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, LGJ;->b:Z

    .line 238
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, LGJ;->a:LaFO;

    if-eqz v0, :cond_0

    iget-object v0, p0, LGJ;->a:LaFO;

    iget-object v1, p0, LGJ;->b:LaFO;

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    :cond_0
    iget-object v0, p0, LGJ;->a:LGH;

    invoke-interface {v0, p1}, LGH;->a(Ljava/lang/String;)V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {p0, p1}, LGJ;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 261
    const-string v0, "UrlLoadingWebViewClient"

    const-string v1, "in loginUserAndLoadUrl"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, LGJ;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 269
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 270
    iget-object v0, p0, LGJ;->a:LaFO;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p1, v1}, LGJ;->a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 380
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 381
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-virtual {p3}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_0
    iget-object v1, p0, LGJ;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/net/http/SslError;->getPrimaryError()I

    move-result v2

    invoke-static {v1, v0, v2}, LGJ;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 383
    iget-object v1, p0, LGJ;->a:LGH;

    invoke-interface {v1, v0}, LGH;->b(Ljava/lang/String;)V

    .line 384
    return-void

    .line 381
    :cond_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 290
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 291
    iget-object v3, p0, LGJ;->a:LbiU;

    invoke-interface {v3, v0}, LbiU;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    iget-object v0, p0, LGJ;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 293
    if-eqz v0, :cond_0

    .line 294
    const-string v3, "UrlLoadingWebViewClient"

    const-string v4, "Rewriting %s -> %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v2

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 295
    iget-object v2, p0, LGJ;->a:LGH;

    invoke-interface {v2, v0}, LGH;->a(Ljava/lang/String;)V

    :goto_0
    move v0, v1

    .line 373
    :goto_1
    return v0

    .line 297
    :cond_0
    const-string v0, "UrlLoadingWebViewClient"

    const-string v2, "Correct targetUrl unknown when attempting to load AUTH_LOGIN_URL"

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_1
    iget-object v3, p0, LGJ;->a:LGH;

    invoke-interface {v3}, LGH;->a()LGI;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v2

    .line 302
    goto :goto_1

    .line 305
    :cond_2
    const-string v3, "UrlLoadingWebViewClient"

    const-string v4, "Loading url %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p2, v5, v2

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 309
    invoke-static {v0}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 310
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 311
    if-eqz v4, :cond_9

    invoke-static {v0}, LFO;->c(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_9

    if-eqz v3, :cond_9

    .line 312
    const-string v4, "/url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 313
    const-string v1, "q"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-virtual {p0, p1, v0}, LGJ;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 315
    :cond_3
    iget-object v4, p0, LGJ;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 317
    iget-object v0, p0, LGJ;->a:LGH;

    invoke-interface {v0}, LGH;->a()V

    move v0, v1

    .line 318
    goto :goto_1

    .line 319
    :cond_4
    iget-object v4, p0, LGJ;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 320
    iget-object v3, p0, LGJ;->a:LaFO;

    if-eqz v3, :cond_5

    iget-boolean v3, p0, LGJ;->b:Z

    if-eqz v3, :cond_5

    .line 322
    iput-boolean v2, p0, LGJ;->b:Z

    .line 325
    iget-object v2, p0, LGJ;->a:LaFO;

    const-string v3, "continue"

    .line 326
    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "service"

    .line 327
    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 325
    invoke-direct {p0, v2, v3, v4, v0}, LGJ;->a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 328
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 330
    goto/16 :goto_1

    .line 331
    :cond_6
    const-string v4, "/cloudprint/client/mobile.html"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 333
    const-string v0, "UrlLoadingWebViewClient"

    const-string v3, "Whitelisted cloud print %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 334
    goto/16 :goto_1

    .line 335
    :cond_7
    const-string v4, "/fusiontables"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 337
    const-string v0, "UrlLoadingWebViewClient"

    const-string v3, "Whitelisted fusion table %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 338
    goto/16 :goto_1

    .line 339
    :cond_8
    iget-object v4, p0, LGJ;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 341
    const-string v0, "UrlLoadingWebViewClient"

    const-string v3, "Whitelisted path %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 342
    goto/16 :goto_1

    .line 346
    :cond_9
    iget-object v4, p0, LGJ;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 347
    const-string v0, "UrlLoadingWebViewClient"

    const-string v3, "Whitelisted URL %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 348
    goto/16 :goto_1

    .line 351
    :cond_a
    sget-object v4, LaEG;->k:LaEG;

    invoke-virtual {v4}, LaEG;->a()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 352
    const-string v0, "UrlLoadingWebViewClient"

    const-string v3, "FileProvider URL %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 353
    goto/16 :goto_1

    .line 356
    :cond_b
    if-eqz v3, :cond_e

    .line 357
    invoke-static {v0}, LFO;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 358
    invoke-static {v0}, LFO;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 360
    :cond_c
    iget-object v2, p0, LGJ;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 362
    invoke-direct {p0}, LGJ;->b()V

    move v0, v1

    .line 363
    goto/16 :goto_1

    .line 366
    :cond_d
    invoke-direct {p0, p2, v0}, LGJ;->a(Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    goto/16 :goto_1

    .line 370
    :cond_e
    invoke-direct {p0, p2}, LGJ;->c(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    move v0, v1

    .line 373
    goto/16 :goto_1
.end method
