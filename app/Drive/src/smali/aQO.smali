.class public final LaQO;
.super LaRw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaRw",
        "<",
        "LaQZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LaRe;

.field private static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private a:D

.field private a:I

.field private final a:J

.field private final a:LaCd;

.field private a:LaDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDk",
            "<",
            "LaBW;",
            ">;"
        }
    .end annotation
.end field

.field private a:LaQV;

.field private final a:LaRc;

.field private a:Landroid/os/Bundle;

.field private a:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private final a:Lcom/google/android/gms/cast/CastDevice;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LaCe;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final a:Ljava/util/concurrent/atomic/AtomicLong;

.field private b:LaDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDk",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LaDk",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, LaRe;

    const-string v1, "CastClientImpl"

    invoke-direct {v0, v1}, LaRe;-><init>(Ljava/lang/String;)V

    sput-object v0, LaQO;->a:LaRe;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LaQO;->a:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LaQO;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLaCd;LaCX;LaCY;)V
    .locals 6

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    invoke-direct/range {v0 .. v5}, LaRw;-><init>(Landroid/content/Context;Landroid/os/Looper;LaCX;LaCY;[Ljava/lang/String;)V

    iput-object p3, p0, LaQO;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object p6, p0, LaQO;->a:LaCd;

    iput-wide p4, p0, LaQO;->a:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LaQO;->b:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaQO;->a:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaQO;->e:Z

    const/4 v0, -0x1

    iput v0, p0, LaQO;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, LaQO;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    const/4 v0, 0x0

    iput-object v0, p0, LaQO;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaQO;->a:D

    const/4 v0, 0x0

    iput-boolean v0, p0, LaQO;->b:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaQO;->b:Ljava/util/Map;

    new-instance v0, LaQV;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaQV;-><init>(LaQO;LaQP;)V

    iput-object v0, p0, LaQO;->a:LaQV;

    iget-object v0, p0, LaQO;->a:LaQV;

    invoke-virtual {p0, v0}, LaQO;->a(LaCY;)V

    new-instance v0, LaQP;

    invoke-direct {v0, p0}, LaQP;-><init>(LaQO;)V

    iput-object v0, p0, LaQO;->a:LaRc;

    return-void
.end method

.method static synthetic a(LaQO;)LaCd;
    .locals 1

    iget-object v0, p0, LaQO;->a:LaCd;

    return-object v0
.end method

.method static synthetic a(LaQO;)LaDk;
    .locals 1

    iget-object v0, p0, LaQO;->a:LaDk;

    return-object v0
.end method

.method static synthetic a(LaQO;LaDk;)LaDk;
    .locals 0

    iput-object p1, p0, LaQO;->a:LaDk;

    return-object p1
.end method

.method static synthetic a()LaRe;
    .locals 1

    sget-object v0, LaQO;->a:LaRe;

    return-object v0
.end method

.method static synthetic a(LaQO;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, LaQO;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LaQO;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 0

    iput-object p1, p0, LaQO;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    return-object p1
.end method

.method static synthetic a(LaQO;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    iget-object v0, p0, LaQO;->a:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    sget-object v0, LaQO;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(LaQO;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, LaQO;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(LaQO;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, LaQO;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(LaQO;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private a(LaDk;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    sget-object v1, LaQO;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaQO;->b:LaDk;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x7d1

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, LaDk;->a(Ljava/lang/Object;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, LaQO;->b:LaDk;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(LaQO;)V
    .locals 0

    invoke-direct {p0}, LaQO;->f()V

    return-void
.end method

.method static synthetic a(LaQO;Lcom/google/android/gms/internal/fv;)V
    .locals 0

    invoke-direct {p0, p1}, LaQO;->a(Lcom/google/android/gms/internal/fv;)V

    return-void
.end method

.method static synthetic a(LaQO;Lcom/google/android/gms/internal/ga;)V
    .locals 0

    invoke-direct {p0, p1}, LaQO;->a(Lcom/google/android/gms/internal/ga;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/fv;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/fv;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaQO;->a:Ljava/lang/String;

    invoke-static {v0, v3}, LaQW;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iput-object v0, p0, LaQO;->a:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, LaQO;->a:LaRe;

    const-string v4, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, LaQO;->c:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LaQO;->a:LaCd;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    iget-boolean v0, p0, LaQO;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LaQO;->a:LaCd;

    invoke-virtual {v0}, LaCd;->a()V

    :cond_1
    iput-boolean v2, p0, LaQO;->c:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/internal/ga;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/ga;->a()D

    move-result-wide v4

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_6

    iget-wide v6, p0, LaQO;->a:D

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_6

    iput-wide v4, p0, LaQO;->a:D

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ga;->a()Z

    move-result v3

    iget-boolean v4, p0, LaQO;->b:Z

    if-eq v3, v4, :cond_0

    iput-boolean v3, p0, LaQO;->b:Z

    move v0, v1

    :cond_0
    sget-object v3, LaQO;->a:LaRe;

    const-string v4, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, LaQO;->d:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, LaQO;->a:LaCd;

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    iget-boolean v0, p0, LaQO;->d:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, LaQO;->a:LaCd;

    invoke-virtual {v0}, LaCd;->b()V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ga;->b()I

    move-result v0

    iget v3, p0, LaQO;->a:I

    if-eq v0, v3, :cond_5

    iput v0, p0, LaQO;->a:I

    move v0, v1

    :goto_1
    sget-object v3, LaQO;->a:LaRe;

    const-string v4, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, LaQO;->d:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LaQO;->a:LaCd;

    if-eqz v1, :cond_4

    if-nez v0, :cond_3

    iget-boolean v0, p0, LaQO;->d:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, LaQO;->a:LaCd;

    iget v1, p0, LaQO;->a:I

    invoke-virtual {v0, v1}, LaCd;->b(I)V

    :cond_4
    iput-boolean v2, p0, LaQO;->d:Z

    return-void

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(LaQO;Z)Z
    .locals 0

    iput-boolean p1, p0, LaQO;->e:Z

    return p1
.end method

.method static synthetic b(LaQO;)LaDk;
    .locals 1

    iget-object v0, p0, LaQO;->b:LaDk;

    return-object v0
.end method

.method static synthetic b(LaQO;LaDk;)LaDk;
    .locals 0

    iput-object p1, p0, LaQO;->b:LaDk;

    return-object p1
.end method

.method static synthetic b()Ljava/lang/Object;
    .locals 1

    sget-object v0, LaQO;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(LaQO;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, LaQO;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(LaQO;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, LaQO;->b:Ljava/util/Map;

    return-object v0
.end method

.method private f()V
    .locals 3

    sget-object v0, LaQO;->a:LaRe;

    const-string v1, "removing all MessageReceivedCallbacks"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LaQO;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaQO;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private g()V
    .locals 2

    iget-boolean v0, p0, LaQO;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected to a device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)LaQZ;
    .locals 1

    invoke-static {p1}, LaRa;->a(Landroid/os/IBinder;)LaQZ;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, LaQO;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaQO;->a:Landroid/os/Bundle;

    const/4 v1, 0x0

    iput-object v1, p0, LaQO;->a:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LaRw;->a()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, LaQO;->a(Landroid/os/IBinder;)LaQZ;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE"

    return-object v0
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x3e9

    const/4 v0, 0x0

    const/4 v5, 0x1

    sget-object v1, LaQO;->a:LaRe;

    const-string v2, "in onPostInitHandler; statusCode=%d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_2

    :cond_0
    iput-boolean v5, p0, LaQO;->e:Z

    iput-boolean v5, p0, LaQO;->c:Z

    iput-boolean v5, p0, LaQO;->d:Z

    :goto_0
    if-ne p1, v6, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, LaQO;->a:Landroid/os/Bundle;

    iget-object v1, p0, LaQO;->a:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move p1, v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, LaRw;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_2
    iput-boolean v0, p0, LaQO;->e:Z

    goto :goto_0
.end method

.method protected a(LaRS;LaRA;)V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v0, LaQO;->a:LaRe;

    const-string v1, "getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LaQO;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LaQO;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, LaQO;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)V

    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    iget-wide v2, p0, LaQO;->a:J

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, LaQO;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_application_id"

    iget-object v1, p0, LaQO;->b:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, LaQO;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_session_id"

    iget-object v1, p0, LaQO;->c:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const v2, 0x4da6e8

    invoke-virtual {p0}, LaQO;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LaQO;->a:LaRc;

    invoke-interface {v0}, LaRc;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, LaRS;->a(LaRP;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Ljava/lang/String;LaDk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LaDk",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, LaQO;->a(LaDk;)V

    invoke-virtual {p0}, LaQO;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaQZ;

    invoke-interface {v0, p1}, LaQZ;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaDk;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LaDk",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The message payload cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message exceeds maximum size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, LaQW;->a(Ljava/lang/String;)V

    invoke-direct {p0}, LaQO;->g()V

    iget-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    invoke-virtual {p0}, LaQO;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaQZ;

    invoke-interface {v0, p1, p2, v2, v3}, LaQZ;->a(Ljava/lang/String;Ljava/lang/String;J)V

    iget-object v0, p0, LaQO;->b:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.internal.ICastDeviceController"

    return-object v0
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, LaQO;->a:LaRe;

    const-string v1, "disconnect(); mDisconnecting=%b, isConnected=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, LaQO;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, LaQO;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaQO;->a:LaRe;

    const-string v1, "mDisconnecting is set, so short-circuiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LaRe;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, LaQO;->f()V

    :try_start_0
    invoke-virtual {p0}, LaQO;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, LaQO;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, LaQO;->a()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LaQZ;

    invoke-interface {v0}, LaQZ;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-super {p0}, LaRw;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v1, LaQO;->a:LaRe;

    const-string v2, "Error while disconnecting the controller interface: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, LaRe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, LaRw;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, LaRw;->b()V

    throw v0
.end method
