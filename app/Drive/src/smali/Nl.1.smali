.class public final enum LNl;
.super Ljava/lang/Enum;
.source "NewEntryCreationInfo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LNl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LNl;

.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaGv;",
            "LNl;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LNl;

.field public static final enum b:LNl;

.field public static final enum c:LNl;

.field public static final enum d:LNl;

.field public static final enum e:LNl;


# instance fields
.field private final a:I

.field private final a:LaGv;

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, LNl;

    const-string v1, "COLLECTION"

    sget-object v3, LaGv;->a:LaGv;

    sget v4, Lxi;->default_new_folder_title:I

    sget v5, Lxi;->new_folder_title:I

    sget v6, Lxi;->creating_folder:I

    sget v7, Lxi;->create_new_error_folder:I

    invoke-direct/range {v0 .. v7}, LNl;-><init>(Ljava/lang/String;ILaGv;IIII)V

    sput-object v0, LNl;->a:LNl;

    .line 21
    new-instance v3, LNl;

    const-string v4, "DOCUMENT"

    sget-object v6, LaGv;->b:LaGv;

    sget v7, Lxi;->default_new_kix_title:I

    sget v8, Lxi;->new_kix_title:I

    sget v9, Lxi;->creating_document:I

    sget v10, Lxi;->create_new_error_document:I

    move v5, v11

    invoke-direct/range {v3 .. v10}, LNl;-><init>(Ljava/lang/String;ILaGv;IIII)V

    sput-object v3, LNl;->b:LNl;

    .line 26
    new-instance v3, LNl;

    const-string v4, "DRAWING"

    sget-object v6, LaGv;->c:LaGv;

    sget v7, Lxi;->default_new_drawing_title:I

    sget v8, Lxi;->new_drawing_title:I

    sget v9, Lxi;->creating_drawing:I

    sget v10, Lxi;->create_new_error_drawing:I

    move v5, v12

    invoke-direct/range {v3 .. v10}, LNl;-><init>(Ljava/lang/String;ILaGv;IIII)V

    sput-object v3, LNl;->c:LNl;

    .line 31
    new-instance v3, LNl;

    const-string v4, "PRESENTATION"

    sget-object v6, LaGv;->g:LaGv;

    sget v7, Lxi;->default_new_punch_title:I

    sget v8, Lxi;->new_punch_title:I

    sget v9, Lxi;->creating_presentation:I

    sget v10, Lxi;->create_new_error_presentation:I

    move v5, v13

    invoke-direct/range {v3 .. v10}, LNl;-><init>(Ljava/lang/String;ILaGv;IIII)V

    sput-object v3, LNl;->d:LNl;

    .line 36
    new-instance v3, LNl;

    const-string v4, "SPREADSHEET"

    sget-object v6, LaGv;->i:LaGv;

    sget v7, Lxi;->default_new_trix_title:I

    sget v8, Lxi;->new_trix_title:I

    sget v9, Lxi;->creating_spreadsheet:I

    sget v10, Lxi;->create_new_error_spreadsheet:I

    move v5, v14

    invoke-direct/range {v3 .. v10}, LNl;-><init>(Ljava/lang/String;ILaGv;IIII)V

    sput-object v3, LNl;->e:LNl;

    .line 15
    const/4 v0, 0x5

    new-array v0, v0, [LNl;

    sget-object v1, LNl;->a:LNl;

    aput-object v1, v0, v2

    sget-object v1, LNl;->b:LNl;

    aput-object v1, v0, v11

    sget-object v1, LNl;->c:LNl;

    aput-object v1, v0, v12

    sget-object v1, LNl;->d:LNl;

    aput-object v1, v0, v13

    sget-object v1, LNl;->e:LNl;

    aput-object v1, v0, v14

    sput-object v0, LNl;->a:[LNl;

    .line 43
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    sget-object v1, LaGv;->a:LaGv;

    sget-object v2, LNl;->a:LNl;

    .line 44
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->b:LaGv;

    sget-object v2, LNl;->b:LNl;

    .line 45
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->i:LaGv;

    sget-object v2, LNl;->e:LNl;

    .line 46
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->g:LaGv;

    sget-object v2, LNl;->d:LNl;

    .line 47
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->c:LaGv;

    sget-object v2, LNl;->c:LNl;

    .line 48
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LNl;->a:Ljava/util/Map;

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaGv;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGv;",
            "IIII)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput-object p3, p0, LNl;->a:LaGv;

    .line 63
    iput p4, p0, LNl;->a:I

    .line 64
    iput p5, p0, LNl;->b:I

    .line 65
    iput p6, p0, LNl;->c:I

    .line 66
    iput p7, p0, LNl;->d:I

    .line 67
    return-void
.end method

.method public static a(LaGv;)LNl;
    .locals 3

    .prologue
    .line 90
    sget-object v0, LNl;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNl;

    .line 91
    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No matching creation info for entry of kind : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LNl;
    .locals 1

    .prologue
    .line 15
    const-class v0, LNl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LNl;

    return-object v0
.end method

.method public static values()[LNl;
    .locals 1

    .prologue
    .line 15
    sget-object v0, LNl;->a:[LNl;

    invoke-virtual {v0}, [LNl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LNl;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, LNl;->a:I

    return v0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LNl;->a:LaGv;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, LNl;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, LNl;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, LNl;->d:I

    return v0
.end method
