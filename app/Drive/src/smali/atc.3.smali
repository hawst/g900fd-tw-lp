.class public final enum Latc;
.super Ljava/lang/Enum;
.source "WelcomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Latc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Latc;

.field private static final synthetic a:[Latc;

.field public static final enum b:Latc;

.field public static final enum c:Latc;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Latc;

    const-string v1, "MDPI"

    invoke-direct {v0, v1, v2}, Latc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Latc;->a:Latc;

    .line 73
    new-instance v0, Latc;

    const-string v1, "HDPI"

    invoke-direct {v0, v1, v3}, Latc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Latc;->b:Latc;

    .line 74
    new-instance v0, Latc;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, Latc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Latc;->c:Latc;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Latc;

    sget-object v1, Latc;->a:Latc;

    aput-object v1, v0, v2

    sget-object v1, Latc;->b:Latc;

    aput-object v1, v0, v3

    sget-object v1, Latc;->c:Latc;

    aput-object v1, v0, v4

    sput-object v0, Latc;->a:[Latc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Latc;
    .locals 1

    .prologue
    .line 71
    const-class v0, Latc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Latc;

    return-object v0
.end method

.method public static values()[Latc;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Latc;->a:[Latc;

    invoke-virtual {v0}, [Latc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Latc;

    return-object v0
.end method
