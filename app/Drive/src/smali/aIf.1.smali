.class public final enum LaIf;
.super Ljava/lang/Enum;
.source "ProprietaryExtensionHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaIf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaIf;

.field private static final synthetic a:[LaIf;

.field public static final enum b:LaIf;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, LaIf;

    const-string v1, "CALL_NAME_DOCUMENT_INFO"

    const-string v2, "documentInfo"

    invoke-direct {v0, v1, v3, v2}, LaIf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaIf;->a:LaIf;

    .line 53
    new-instance v0, LaIf;

    const-string v1, "CALL_NAME_DETAILS_PREVIEW"

    const-string v2, "detailsPreview"

    invoke-direct {v0, v1, v4, v2}, LaIf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaIf;->b:LaIf;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [LaIf;

    sget-object v1, LaIf;->a:LaIf;

    aput-object v1, v0, v3

    sget-object v1, LaIf;->b:LaIf;

    aput-object v1, v0, v4

    sput-object v0, LaIf;->a:[LaIf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-object p3, p0, LaIf;->a:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static a(Ljava/lang/String;)LaIf;
    .locals 5

    .prologue
    .line 66
    invoke-static {}, LaIf;->values()[LaIf;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 67
    invoke-virtual {v0}, LaIf;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 71
    :goto_1
    return-object v0

    .line 66
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LaIf;
    .locals 1

    .prologue
    .line 39
    const-class v0, LaIf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaIf;

    return-object v0
.end method

.method public static values()[LaIf;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LaIf;->a:[LaIf;

    invoke-virtual {v0}, [LaIf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaIf;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LaIf;->a:Ljava/lang/String;

    return-object v0
.end method
