.class public LaeF;
.super Lbxb;
.source "GdataDocEntry.java"

# interfaces
.implements LaJT;


# instance fields
.field private a:I

.field private a:Ljava/lang/Long;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LaJU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lbxb;-><init>()V

    .line 76
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LaeF;->a:Ljava/util/Set;

    .line 79
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaeF;->a:Ljava/util/Map;

    .line 81
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LaeF;->a:Ljava/util/LinkedList;

    .line 30
    invoke-direct {p0}, LaeF;->c()V

    .line 31
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    iput-object v2, p0, LaeF;->a:Ljava/lang/String;

    .line 175
    iput-boolean v1, p0, LaeF;->a:Z

    .line 176
    iput-boolean v1, p0, LaeF;->c:Z

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, LaeF;->b:Z

    .line 178
    iput-boolean v1, p0, LaeF;->d:Z

    .line 179
    iput-boolean v1, p0, LaeF;->h:Z

    .line 180
    iput-boolean v1, p0, LaeF;->f:Z

    .line 181
    iput-boolean v1, p0, LaeF;->g:Z

    .line 182
    iput-object v2, p0, LaeF;->b:Ljava/lang/String;

    .line 183
    iput v1, p0, LaeF;->a:I

    .line 184
    iput-object v2, p0, LaeF;->d:Ljava/lang/String;

    .line 185
    iput-object v2, p0, LaeF;->f:Ljava/lang/String;

    .line 186
    iput-object v2, p0, LaeF;->g:Ljava/lang/String;

    .line 187
    iput-object v2, p0, LaeF;->h:Ljava/lang/String;

    .line 188
    iput-object v2, p0, LaeF;->i:Ljava/lang/String;

    .line 189
    iget-object v0, p0, LaeF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 190
    iget-object v0, p0, LaeF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 191
    iget-object v0, p0, LaeF;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 192
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, LaeF;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, LaeF;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, LaeF;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaJU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, LaeF;->a:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, LaeF;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, LaeF;->c()V

    .line 241
    invoke-super {p0}, Lbxb;->a()V

    .line 242
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 291
    iput p1, p0, LaeF;->a:I

    .line 292
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 410
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaeF;->a:Ljava/lang/Long;

    .line 411
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, LaeF;->e:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, LaeF;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, LaeF;->a:Ljava/util/LinkedList;

    new-instance v1, LaJU;

    invoke-direct {v1, p1, p2}, LaJU;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method protected a(Ljava/lang/StringBuffer;)V
    .locals 2

    .prologue
    .line 426
    invoke-super {p0, p1}, Lbxb;->a(Ljava/lang/StringBuffer;)V

    .line 427
    const-string v0, "Resource Id"

    iget-object v1, p0, LaeF;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, LaeF;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, LaeF;->a:Z

    .line 87
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, LaeF;->a:Z

    return v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, LaeF;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, LaeF;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 421
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaeF;->b:Ljava/lang/Long;

    .line 422
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, LaeF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 221
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, LaeF;->b:Z

    .line 97
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, LaeF;->b:Z

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, LaeF;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, LaeF;->a:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 106
    iput-boolean p1, p0, LaeF;->c:Z

    .line 107
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, LaeF;->c:Z

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, LaeF;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, LaeF;->b:Ljava/lang/String;

    .line 276
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, LaeF;->d:Z

    .line 117
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, LaeF;->d:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, LaeF;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, LaeF;->j:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 126
    iput-boolean p1, p0, LaeF;->h:Z

    .line 127
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, LaeF;->h:Z

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, LaeF;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, LaeF;->d:Ljava/lang/String;

    .line 351
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, LaeF;->e:Z

    .line 148
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, LaeF;->e:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, LaeF;->h:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, LaeF;->f:Ljava/lang/String;

    .line 359
    return-void
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 161
    iput-boolean p1, p0, LaeF;->f:Z

    .line 162
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, LaeF;->f:Z

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, LaeF;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, LaeF;->g:Ljava/lang/String;

    .line 367
    return-void
.end method

.method public h(Z)V
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, LaeF;->g:Z

    .line 171
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, LaeF;->g:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, LaeF;->j:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, LaeF;->h:Ljava/lang/String;

    .line 375
    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, LaeF;->c:Ljava/lang/String;

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, LaeF;->i:Ljava/lang/String;

    .line 384
    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, LaeF;->c:Ljava/lang/String;

    .line 400
    return-void
.end method
