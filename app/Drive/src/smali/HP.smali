.class public LHP;
.super LzM;
.source "ListToGridCellAdapterVisibleRange.java"


# instance fields
.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LBC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LzN;Lbjv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LzN;",
            "Lbjv",
            "<",
            "LBC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1}, LzM;-><init>(LzN;)V

    .line 23
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LHP;->a:Lbjv;

    .line 24
    return-void
.end method

.method private a(LBC;I)I
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p1, p2}, LBC;->a(I)LBF;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, LBF;->a()I

    move-result v0

    return v0
.end method

.method private b(LBC;I)I
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p1}, LBC;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 54
    invoke-virtual {p1, v0}, LBC;->a(I)LBF;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, LBF;->a()I

    move-result v1

    .line 56
    invoke-virtual {v0}, LBF;->b()I

    move-result v0

    .line 57
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method protected a(LalS;)LalS;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    iget-object v0, p0, LHP;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    .line 29
    invoke-virtual {p1}, LalS;->a()I

    move-result v1

    invoke-virtual {v0}, LBC;->a()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 30
    invoke-static {v3, v3}, LalS;->b(II)LalS;

    move-result-object v0

    .line 38
    :goto_0
    return-object v0

    .line 33
    :cond_0
    invoke-virtual {p1}, LalS;->a()I

    move-result v1

    invoke-direct {p0, v0, v1}, LHP;->a(LBC;I)I

    move-result v1

    .line 34
    invoke-virtual {p1}, LalS;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35
    invoke-static {v1, v3}, LalS;->b(II)LalS;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {p1}, LalS;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v0, v2}, LHP;->b(LBC;I)I

    move-result v0

    .line 38
    add-int/lit8 v0, v0, 0x1

    invoke-static {v1, v0}, LalS;->a(II)LalS;

    move-result-object v0

    goto :goto_0
.end method
