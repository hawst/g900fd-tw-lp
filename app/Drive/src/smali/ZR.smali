.class public final LZR;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZP;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZN;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LZP;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LZS;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LZP;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 47
    iput-object p1, p0, LZR;->a:LbrA;

    .line 48
    const-class v0, LZP;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LZR;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->a:Lbsk;

    .line 51
    const-class v0, LZN;

    invoke-static {v0, v3}, LZR;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->b:Lbsk;

    .line 54
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 55
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 54
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->c:Lbsk;

    .line 57
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 58
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 57
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->d:Lbsk;

    .line 60
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 61
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 60
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->e:Lbsk;

    .line 63
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 64
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 63
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->f:Lbsk;

    .line 66
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 67
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 66
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->g:Lbsk;

    .line 69
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZP;

    aput-object v2, v1, v4

    .line 70
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 69
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->h:Lbsk;

    .line 72
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v4

    .line 73
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 72
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->i:Lbsk;

    .line 75
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LZP;

    aput-object v2, v1, v4

    .line 76
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 75
    invoke-static {v0, v3}, LZR;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LZR;->j:Lbsk;

    .line 78
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 123
    sparse-switch p1, :sswitch_data_0

    .line 151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :sswitch_0
    new-instance v1, LZP;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->H:Lbsk;

    .line 128
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LZR;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->H:Lbsk;

    .line 126
    invoke-static {v0, v2}, LZR;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    invoke-direct {v1, v0}, LZP;-><init>(LaKM;)V

    move-object v0, v1

    .line 149
    :goto_0
    return-object v0

    .line 135
    :sswitch_1
    new-instance v2, LZN;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 138
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 136
    invoke-static {v0, v1}, LZR;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->S:Lbsk;

    .line 144
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LZR;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 142
    invoke-static {v1, v3}, LZR;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKM;

    invoke-direct {v2, v0, v1}, LZN;-><init>(LQr;LaKM;)V

    move-object v0, v2

    .line 149
    goto :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_1
        0x25c -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 166
    packed-switch p2, :pswitch_data_0

    .line 272
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :pswitch_1
    check-cast p1, LZO;

    .line 170
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v2, v0, LpG;->e:Lbsk;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 177
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 181
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 170
    invoke-virtual {p1, v2, v0, v1}, LZO;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    .line 257
    :goto_0
    return-object v0

    .line 185
    :pswitch_2
    check-cast p1, LZO;

    .line 187
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LZR;

    iget-object v0, v0, LZR;->g:Lbsk;

    .line 190
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 187
    invoke-virtual {p1, v0}, LZO;->getLazy3(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 194
    :pswitch_3
    check-cast p1, LZO;

    .line 196
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v2, v0, LaoA;->p:Lbsk;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 203
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 207
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 196
    invoke-virtual {p1, v2, v0, v1}, LZO;->get4(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 211
    :pswitch_4
    check-cast p1, LZO;

    .line 213
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LZR;

    iget-object v0, v0, LZR;->c:Lbsk;

    .line 216
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 213
    invoke-virtual {p1, v0}, LZO;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 220
    :pswitch_5
    check-cast p1, LZO;

    .line 222
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v2, v0, LaoA;->o:Lbsk;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 229
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 233
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 222
    invoke-virtual {p1, v2, v0, v1}, LZO;->get3(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 237
    :pswitch_6
    check-cast p1, LZO;

    .line 239
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LZR;

    iget-object v0, v0, LZR;->j:Lbsk;

    .line 242
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 239
    invoke-virtual {p1, v0}, LZO;->getLazy2(Laja;)Lajw;

    move-result-object v0

    goto/16 :goto_0

    .line 246
    :pswitch_7
    check-cast p1, LZO;

    .line 248
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LZR;

    iget-object v0, v0, LZR;->e:Lbsk;

    .line 251
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 248
    invoke-virtual {p1, v0}, LZO;->getLazy4(Laja;)Lajw;

    move-result-object v0

    goto/16 :goto_0

    .line 255
    :pswitch_8
    check-cast p1, LZO;

    .line 257
    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v2, v0, LpG;->g:Lbsk;

    iget-object v0, p0, LZR;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 264
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LZR;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 268
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 257
    invoke-virtual {p1, v2, v0, v1}, LZO;->get2(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto/16 :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x259
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    const-class v0, LZP;

    iget-object v1, p0, LZR;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Ljava/lang/Class;Lbsk;)V

    .line 86
    const-class v0, LZN;

    iget-object v1, p0, LZR;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Ljava/lang/Class;Lbsk;)V

    .line 87
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 88
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 89
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 90
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 91
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 92
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZP;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 93
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZS;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 94
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LZP;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LZR;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LZR;->a(Lbuv;Lbsk;)V

    .line 95
    iget-object v0, p0, LZR;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x25c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 97
    iget-object v0, p0, LZR;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x191

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 99
    iget-object v0, p0, LZR;->c:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x259

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 101
    iget-object v0, p0, LZR;->d:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x25a

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 103
    iget-object v0, p0, LZR;->e:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x25d

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 105
    iget-object v0, p0, LZR;->f:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x25e

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 107
    iget-object v0, p0, LZR;->g:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x25b

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 109
    iget-object v0, p0, LZR;->h:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x25f

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 111
    iget-object v0, p0, LZR;->i:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x261

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 113
    iget-object v0, p0, LZR;->j:Lbsk;

    const-class v1, LZO;

    const/16 v2, 0x260

    invoke-virtual {p0, v1, v2}, LZR;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 115
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 158
    .line 160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method
