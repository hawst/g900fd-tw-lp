.class public LbgS;
.super Ljava/lang/Object;
.source "BatchRectifier.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/content/SharedPreferences;

.field private final a:Landroid/os/Handler;

.field private a:LbgU;

.field private final a:LbgZ;

.field private a:Ljava/lang/Integer;

.field private a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, LbgS;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LbgZ;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LbgS;->a:Landroid/os/Handler;

    .line 30
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LbgS;->a:Ljava/util/concurrent/ExecutorService;

    .line 32
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    .line 43
    iput-object p2, p0, LbgS;->a:LbgZ;

    .line 44
    iput-object p1, p0, LbgS;->a:Landroid/content/Context;

    .line 45
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LbgS;->a:Landroid/content/SharedPreferences;

    .line 46
    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, LbgS;->a:Landroid/content/SharedPreferences;

    iget-object v1, p0, LbgS;->a:Landroid/content/Context;

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_jpeg_quality_key:I

    .line 101
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LbgS;->a:Landroid/content/Context;

    sget v3, Lcom/google/bionics/scanner/docscanner/R$string;->ds_jpeg_quality_default:I

    .line 102
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 100
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 24
    sget-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method static synthetic a(LbgS;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, LbgS;->d()V

    return-void
.end method

.method static synthetic b(LbgS;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, LbgS;->c()V

    return-void
.end method

.method private declared-synchronized c()V
    .locals 3

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    sget-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Batch rectification task has finished"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    iget-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    .line 76
    iget-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LbgS;->a:LbgU;

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "posting idle notification to UI thread"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, LbgS;->a:Landroid/os/Handler;

    new-instance v1, LbgT;

    invoke-direct {v1, p0}, LbgT;-><init>(LbgS;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LbgS;->a:LbgU;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, LbgS;->a:LbgU;

    invoke-interface {v0}, LbgU;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :cond_0
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(LbgY;)Ljava/util/concurrent/FutureTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbgY;",
            ")",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "LbgY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, LbgW;

    iget-object v1, p0, LbgS;->a:LbgZ;

    invoke-direct {p0}, LbgS;->a()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, LbgW;-><init>(LbgY;LbgZ;I)V

    invoke-virtual {p0, v0, p1}, LbgS;->a(Ljava/util/concurrent/Callable;LbgY;)Ljava/util/concurrent/FutureTask;

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized a(Ljava/util/concurrent/Callable;LbgY;)Ljava/util/concurrent/FutureTask;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "LbgY;",
            ">;",
            "LbgY;",
            ")",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "LbgY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LbgS;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LbgS;->a:Ljava/util/concurrent/ExecutorService;

    .line 159
    :cond_0
    sget-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Submitting new rectification job to batch rectifier..."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    new-instance v0, LbgV;

    invoke-direct {v0, p0, p1, p2}, LbgV;-><init>(LbgS;Ljava/util/concurrent/Callable;LbgY;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :try_start_1
    iget-object v1, p0, LbgS;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 163
    iget-object v1, p0, LbgS;->a:Ljava/lang/Integer;

    iget-object v1, p0, LbgS;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, LbgS;->a:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    :goto_0
    monitor-exit p0

    return-object v0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    :try_start_2
    sget-object v1, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Task got rejected by executor!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 166
    const/4 v0, 0x0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 66
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LbgS;->a:LbgU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LbgU;)V
    .locals 3

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    sget-object v0, LbgS;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "registerIdleListener()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iput-object p1, p0, LbgS;->a:LbgU;

    .line 57
    iget-object v0, p0, LbgS;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LbgS;->a:LbgU;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, LbgS;->a:LbgU;

    invoke-interface {v0}, LbgU;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :cond_0
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LbgS;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
