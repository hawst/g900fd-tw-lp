.class public final LVa;
.super Ljava/lang/Object;
.source "UrlParserResult.java"


# instance fields
.field private final a:LVc;

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LVc;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, LVa;->a:Ljava/lang/String;

    .line 32
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVc;

    iput-object v0, p0, LVa;->a:LVc;

    .line 33
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LVa;->a:Landroid/net/Uri;

    .line 34
    return-void
.end method


# virtual methods
.method public a()LVc;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LVa;->a:LVc;

    return-object v0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LVa;->a:LVc;

    invoke-virtual {v0}, LVc;->a()LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LVa;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LVa;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 65
    instance-of v1, p1, LVa;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    check-cast p1, LVa;

    .line 69
    iget-object v1, p0, LVa;->a:Ljava/lang/String;

    iget-object v2, p1, LVa;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LVa;->a:LVc;

    iget-object v2, p1, LVa;->a:LVc;

    .line 70
    invoke-virtual {v1, v2}, LVc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LVa;->a:Landroid/net/Uri;

    iget-object v2, p1, LVa;->a:Landroid/net/Uri;

    .line 71
    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LVa;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LVa;->a:LVc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LVa;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string v0, "UrlParserResult [resourceId=%s, type=%s, uri=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LVa;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LVa;->a:LVc;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LVa;->a:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
