.class LGM;
.super Ljava/lang/Object;
.source "UrlLoadingWebViewClient.java"

# interfaces
.implements LDL;


# instance fields
.field final synthetic a:LGJ;

.field final synthetic a:LaFO;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(LGJ;LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, LGM;->a:LGJ;

    iput-object p2, p0, LGM;->a:LaFO;

    iput-object p3, p0, LGM;->a:Ljava/lang/String;

    iput-object p4, p0, LGM;->b:Ljava/lang/String;

    iput-object p5, p0, LGM;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 7

    .prologue
    .line 567
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 569
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iget-object v1, p0, LGM;->a:LGJ;

    .line 570
    invoke-static {v1}, LGJ;->a(LGJ;)LQr;

    move-result-object v1

    const-string v4, "webloginMinApiVersion"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 571
    iget-object v0, p0, LGM;->a:LGJ;

    invoke-static {v0}, LGJ;->a(LGJ;)LTd;

    move-result-object v0

    iget-object v1, p0, LGM;->a:LaFO;

    iget-object v4, p0, LGM;->a:LGJ;

    .line 572
    invoke-static {v4}, LGJ;->a(LGJ;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LGM;->a:Ljava/lang/String;

    .line 571
    invoke-interface {v0, v1, v4, v5}, LTd;->b(LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 573
    iget-object v0, p0, LGM;->a:LGJ;

    invoke-static {v0}, LGJ;->a(LGJ;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    iget-object v4, p0, LGM;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574
    if-eqz v0, :cond_1

    .line 575
    const-string v1, "UrlLoadingWebViewClient"

    const-string v4, "Overwrote existing continue URL for weblogin; this shouldn\'t happen: %s -> %s; falling back to %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-object v6, p0, LGM;->b:Ljava/lang/String;

    aput-object v6, v5, v0

    const/4 v0, 0x2

    iget-object v6, p0, LGM;->c:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-static {v1, v4, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 579
    iget-object v0, p0, LGM;->a:LGJ;

    invoke-static {v0}, LGJ;->a(LGJ;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    iget-object v1, p0, LGM;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 580
    iget-object v0, p0, LGM;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 585
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 586
    const-string v1, "UrlLoadingWebViewClient"

    const-string v4, "Secure url received time taken %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v1, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 587
    iget-object v1, p0, LGM;->a:LGJ;

    iget-object v2, p0, LGM;->a:LaFO;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, LGJ;->a(LGJ;LaFO;Ljava/lang/String;)V

    .line 602
    :goto_1
    const/4 v0, -0x1

    return v0

    .line 583
    :cond_0
    iget-object v0, p0, LGM;->a:LGJ;

    invoke-static {v0}, LGJ;->a(LGJ;)LTd;

    move-result-object v0

    iget-object v1, p0, LGM;->a:LaFO;

    iget-object v4, p0, LGM;->b:Ljava/lang/String;

    iget-object v5, p0, LGM;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v5}, LTd;->a(LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 588
    :catch_0
    move-exception v0

    .line 591
    const-string v1, "UrlLoadingWebViewClient"

    const-string v2, "Error getting credential"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 592
    iget-object v1, p0, LGM;->a:LGJ;

    invoke-static {v1, v0}, LGJ;->a(LGJ;Ljava/lang/Exception;)V

    goto :goto_1

    .line 593
    :catch_1
    move-exception v0

    .line 596
    const-string v1, "UrlLoadingWebViewClient"

    const-string v2, "Error getting credential"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 597
    iget-object v1, p0, LGM;->a:LGJ;

    invoke-static {v1, v0}, LGJ;->a(LGJ;Ljava/lang/Exception;)V

    goto :goto_1

    .line 598
    :catch_2
    move-exception v0

    .line 599
    const-string v1, "UrlLoadingWebViewClient"

    const-string v2, "Error getting credential"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 600
    iget-object v1, p0, LGM;->a:LGJ;

    invoke-static {v1, v0}, LGJ;->a(LGJ;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 559
    iget-object v0, p0, LGM;->a:LGJ;

    invoke-static {v0}, LGJ;->a(LGJ;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->getting_authentication_information:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 561
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LGM;->a:LaFO;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lamr;)V
    .locals 0

    .prologue
    .line 613
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 607
    const/4 v0, 0x0

    return v0
.end method
