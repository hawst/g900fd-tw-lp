.class public LadG;
.super Ljava/lang/Object;
.source "FileManagerModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 78
    const-class v0, Ladb;

    const-class v1, LQR;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 79
    const-class v0, LtB;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 80
    return-void
.end method

.method provideContentStorage(LbiP;Ladi;)Ladb;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "Ladb;",
            ">;",
            "Ladi;",
            ")",
            "Ladb;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p1, p2}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladb;

    return-object v0
.end method

.method provideDocumentFileManager(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Ladi;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 46
    return-object p1
.end method

.method provideExposingDocumentUriCreator(Ladv;)Ladu;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 53
    return-object p1
.end method

.method provideFileInstanceHelper(LadF;)LadE;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 33
    return-object p1
.end method

.method provideGarbageCollector(Ladi;)LadL;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 39
    return-object p1
.end method

.method provideLocalFileManager(LadS;)LadQ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 66
    return-object p1
.end method

.method provideTempFileStore(Laec;)Laeb;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 59
    return-object p1
.end method
