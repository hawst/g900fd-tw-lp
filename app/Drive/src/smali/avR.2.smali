.class LavR;
.super LavT;
.source "Analytics.java"


# instance fields
.field private final a:Lazc;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 124
    invoke-direct {p0}, LavT;-><init>()V

    .line 125
    invoke-static {p1}, LayV;->a(Landroid/content/Context;)LayV;

    move-result-object v0

    const-string v1, "UA-21125203-15"

    invoke-virtual {v0, v1}, LayV;->a(Ljava/lang/String;)Lazc;

    move-result-object v0

    iput-object v0, p0, LavR;->a:Lazc;

    .line 126
    iget-object v0, p0, LavR;->a:Lazc;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LavR;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazc;->b(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, LavR;->a:Lazc;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LavR;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazc;->c(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, LavR;->a:Lazc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazc;->a(Z)V

    .line 129
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LavP;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0, p1}, LavR;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 162
    :cond_0
    const-string v0, "no-app-name"

    goto :goto_0
.end method

.method private static b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 168
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string v0, "no-version"

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, LavR;->a:Lazc;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazc;->a(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, LavR;->a:Lazc;

    new-instance v1, LayY;

    invoke-direct {v1}, LayY;-><init>()V

    invoke-virtual {v1}, LayY;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lazc;->a(Ljava/util/Map;)V

    .line 135
    return-void
.end method

.method public a(LavQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, LavR;->a:Lazc;

    new-instance v1, LayZ;

    invoke-direct {v1}, LayZ;-><init>()V

    .line 140
    invoke-virtual {p1}, LavQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LayZ;->a(Ljava/lang/String;)LayZ;

    move-result-object v1

    invoke-virtual {v1, p2}, LayZ;->b(Ljava/lang/String;)LayZ;

    move-result-object v1

    invoke-virtual {v1, p3}, LayZ;->c(Ljava/lang/String;)LayZ;

    move-result-object v1

    invoke-virtual {v1}, LayZ;->a()Ljava/util/Map;

    move-result-object v1

    .line 139
    invoke-virtual {v0, v1}, Lazc;->a(Ljava/util/Map;)V

    .line 141
    return-void
.end method

.method public a(LavQ;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 145
    iget-object v0, p0, LavR;->a:Lazc;

    new-instance v1, LayZ;

    invoke-direct {v1}, LayZ;-><init>()V

    .line 146
    invoke-virtual {p1}, LavQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LayZ;->a(Ljava/lang/String;)LayZ;

    move-result-object v1

    .line 147
    invoke-virtual {v1, p2}, LayZ;->b(Ljava/lang/String;)LayZ;

    move-result-object v1

    .line 148
    invoke-virtual {v1, p3}, LayZ;->c(Ljava/lang/String;)LayZ;

    move-result-object v1

    .line 149
    invoke-virtual {v1, p4, p5}, LayZ;->a(J)LayZ;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, LayZ;->a()Ljava/util/Map;

    move-result-object v1

    .line 145
    invoke-virtual {v0, v1}, Lazc;->a(Ljava/util/Map;)V

    .line 151
    return-void
.end method
