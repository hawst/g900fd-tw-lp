.class public LaeP;
.super Ljava/lang/Object;
.source "DocsXmlEntryGDataSerializer.java"

# interfaces
.implements LaeL;


# instance fields
.field private final a:Lbxb;

.field private final a:Lbxr;

.field private final a:Z


# direct methods
.method public constructor <init>(Lbxr;Lbxb;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, LaeP;->a:Lbxr;

    .line 47
    iput-object p2, p0, LaeP;->a:Lbxb;

    .line 48
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LaeP;->a:Z

    .line 49
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 182
    :cond_0
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 183
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 184
    sget-object v0, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 253
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    sget-object v0, Lbxq;->t:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 257
    sget-object v0, Lbxq;->v:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 258
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 259
    sget-object v0, Lbxq;->v:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 260
    sget-object v0, Lbxq;->u:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 261
    invoke-interface {p0, p2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 262
    sget-object v0, Lbxq;->u:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 263
    sget-object v0, Lbxq;->t:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method protected static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 214
    :cond_0
    sget-object v0, Lbxq;->i:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 215
    sget-object v0, Lbxq;->h:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 216
    sget-object v0, Lbxq;->j:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 217
    invoke-static {p3}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 218
    sget-object v0, Lbxq;->l:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 220
    :cond_1
    invoke-static {p4}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 221
    sget-object v0, Lbxq;->k:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 223
    :cond_2
    sget-object v0, Lbxq;->i:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 111
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p1}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 114
    return-void
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 193
    :cond_0
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 194
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 195
    sget-object v0, Lbxq;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 270
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 274
    :cond_0
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 275
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    sget-object v0, Lbxq;->x:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 278
    :cond_1
    invoke-static {p2}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    sget-object v0, Lbxq;->y:Ljava/lang/String;

    invoke-interface {p0, v1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 281
    :cond_2
    sget-object v0, Lbxq;->w:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private c(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    const-string v0, "http://schemas.google.com/g/2005"

    sget-object v1, Lbxq;->k:Ljava/lang/String;

    iget-object v2, p0, LaeP;->a:Lbxb;

    .line 312
    invoke-virtual {v2}, Lbxb;->m()Ljava/lang/String;

    move-result-object v2

    .line 311
    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 314
    :cond_0
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-static {v0}, Lbxi;->b(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->B:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 316
    const/4 v0, 0x0

    sget-object v1, Lbxq;->l:Ljava/lang/String;

    iget-object v2, p0, LaeP;->a:Lbxb;

    .line 317
    invoke-static {v2}, Lbxi;->b(Lbxb;)Ljava/lang/String;

    move-result-object v2

    .line 316
    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 318
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->B:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 320
    :cond_1
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-static {v0}, Lbxi;->a(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 322
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-static {v0}, Lbxi;->a(Lbxb;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 323
    const-string v0, "http://schemas.google.com/gdata/batch"

    sget-object v1, Lbxq;->p:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 325
    :cond_2
    return-void
.end method

.method private c(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 127
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 128
    invoke-direct {p0, p1}, LaeP;->c(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 131
    :cond_0
    if-eq p2, v3, :cond_1

    .line 132
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 135
    :cond_1
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeP;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 137
    if-eq p2, v3, :cond_2

    .line 138
    sget-object v0, Lbxq;->f:Ljava/lang/String;

    iget-object v1, p0, LaeP;->a:Lbxb;

    .line 139
    invoke-virtual {v1}, Lbxb;->o()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-static {p1, v0, v1, v4, v4}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lbxq;->g:Ljava/lang/String;

    iget-object v1, p0, LaeP;->a:Lbxb;

    .line 141
    invoke-virtual {v1}, Lbxb;->r()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbxq;->o:Ljava/lang/String;

    .line 140
    invoke-static {p1, v0, v1, v2, v4}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_2
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeP;->c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaeP;->d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->t()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaeP;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaeP;->a:Lbxb;

    invoke-virtual {v1}, Lbxb;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LaeP;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    if-nez p2, :cond_3

    .line 153
    iget-object v0, p0, LaeP;->a:Lbxb;

    .line 154
    invoke-virtual {v0}, Lbxb;->p()Ljava/lang/String;

    move-result-object v0

    .line 153
    invoke-static {p1, v0}, LaeP;->e(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 157
    :cond_3
    if-eq p2, v3, :cond_4

    .line 158
    iget-object v0, p0, LaeP;->a:Lbxb;

    .line 159
    invoke-virtual {v0}, Lbxb;->n()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {p1, v0}, LaeP;->f(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 162
    :cond_4
    invoke-virtual {p0, p1, p2}, LaeP;->b(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 163
    return-void
.end method

.method private static c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 229
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 232
    :cond_0
    sget-object v0, Lbxq;->r:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 233
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 234
    sget-object v0, Lbxq;->r:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 240
    if-nez p1, :cond_0

    .line 247
    :goto_0
    return-void

    .line 243
    :cond_0
    sget-object v0, Lbxq;->s:Ljava/lang/String;

    invoke-interface {p0, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 244
    sget-object v0, Lbxq;->l:Ljava/lang/String;

    sget-object v1, Lbxq;->n:Ljava/lang/String;

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 245
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 246
    sget-object v0, Lbxq;->s:Ljava/lang/String;

    invoke-interface {p0, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static e(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 288
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 291
    :cond_0
    sget-object v0, Lbxq;->z:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 292
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 293
    sget-object v0, Lbxq;->z:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static f(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 300
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 303
    :cond_0
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 304
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 305
    sget-object v0, Lbxq;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method


# virtual methods
.method protected a()Lbxb;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, LaeP;->a:Lbxb;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "application/atom+xml"

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;I)V
    .locals 3

    .prologue
    .line 99
    .line 101
    :try_start_0
    iget-object v0, p0, LaeP;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    sget-object v1, Lbxq;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0, v0, p2}, LaeP;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 107
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Lbxk;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public a(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 72
    if-eq p2, v3, :cond_0

    .line 73
    sget-object v0, Lbxq;->e:Ljava/lang/String;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 74
    invoke-direct {p0, p1}, LaeP;->b(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 76
    :cond_0
    iget-object v0, p0, LaeP;->a:Lbxb;

    invoke-virtual {v0}, Lbxb;->z()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, LaeP;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    const-string v1, "http://schemas.google.com/g/2005"

    sget-object v2, Lbxq;->a:Ljava/lang/String;

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 79
    const/4 v1, 0x0

    sget-object v2, Lbxq;->b:Ljava/lang/String;

    invoke-interface {p1, v1, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 82
    :cond_1
    const-string v0, "http://www.w3.org/2005/Atom"

    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 83
    invoke-direct {p0, p1, p2}, LaeP;->c(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 84
    const-string v0, "http://www.w3.org/2005/Atom"

    sget-object v1, Lbxq;->c:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 86
    invoke-virtual {p0}, LaeP;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    const-string v0, "http://schemas.google.com/g/2005"

    sget-object v1, Lbxq;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 90
    :cond_2
    if-eq p2, v3, :cond_3

    .line 91
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 93
    :cond_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 94
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, LaeP;->a:Z

    return v0
.end method

.method protected b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 0

    .prologue
    .line 176
    return-void
.end method
