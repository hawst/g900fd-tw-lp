.class public LKR;
.super Ljava/lang/Object;
.source "MenuFunctionViewFactorySelector.java"

# interfaces
.implements LKP;


# instance fields
.field private a:LKP;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LKP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([LKP;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, LKR;->a:LKP;

    .line 25
    invoke-static {p1}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, LKR;->a:LbmF;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, LKR;->a:LKP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 37
    iget-object v0, p0, LKR;->a:LKP;

    invoke-interface {v0, p1, p2}, LKP;->a(Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/widget/ListPopupWindow;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LKR;->a:LKP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 31
    iget-object v0, p0, LKR;->a:LKP;

    invoke-interface {v0, p1, p2}, LKP;->a(Landroid/view/ViewGroup;Landroid/widget/ListPopupWindow;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, LKR;->a:LKP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LKR;->a:LKP;

    invoke-interface {v0}, LKP;->a()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LKR;->a:LKP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, LKR;->a:LbmF;

    invoke-virtual {v1}, LbmF;->a()Lbqv;

    move-result-object v2

    move-object v1, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 45
    invoke-interface {v0, p1, p2}, LKP;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v3

    .line 46
    if-nez v1, :cond_1

    if-eqz v3, :cond_1

    move-object v1, v0

    .line 49
    :cond_1
    if-eqz v3, :cond_0

    invoke-interface {v0}, LKP;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    iput-object v0, p0, LKR;->a:LKP;

    .line 52
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 55
    :cond_2
    iput-object v1, p0, LKR;->a:LKP;

    .line 56
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LKR;->a:LKP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LKR;->a:LKP;

    invoke-interface {v0}, LKP;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
