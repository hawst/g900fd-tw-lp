.class LFn;
.super Landroid/os/Handler;
.source "OperationDialogFragment.java"


# instance fields
.field final synthetic a:LFm;


# direct methods
.method constructor <init>(LFm;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, LFn;->a:LFm;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, LFn;->a:LFm;

    iget-object v0, v0, LFm;->a:Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v1

    .line 69
    if-eqz v1, :cond_0

    .line 70
    iget-object v0, p0, LFn;->a:LFm;

    invoke-static {v0}, LFm;->a(LFm;)LbmL;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iget-object v2, p0, LFn;->a:LFm;

    iget-object v2, v2, LFm;->a:Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 75
    iget-object v0, p0, LFn;->a:LFm;

    iget-object v0, v0, LFm;->a:Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->u()V

    goto :goto_0
.end method
