.class public abstract LHq;
.super Ljava/lang/Object;
.source "BaseEntryViewHolder.java"


# instance fields
.field private a:I

.field private final a:LDT;

.field private final a:LDi;

.field protected final a:LEd;

.field private a:LaGA;

.field private a:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageView;

.field protected final a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

.field protected final a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lapd;Lapc;Landroid/view/View;LDk;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v1, LEd;

    sget v0, Lxc;->thumbnail:I

    .line 57
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocThumbnailView;

    invoke-direct {v1, p1, p2, v0}, LEd;-><init>(Lapd;Lapc;Lcom/google/android/apps/docs/view/DocThumbnailView;)V

    iput-object v1, p0, LHq;->a:LEd;

    .line 58
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    iput-object v0, p0, LHq;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    .line 59
    sget v0, Lxc;->thumbnail_container:I

    .line 60
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 59
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    iput-object v0, p0, LHq;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    .line 61
    sget v0, Lxc;->sync_state_background:I

    .line 62
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LHq;->a:Landroid/view/View;

    .line 63
    sget v0, Lxc;->sync_video_background:I

    .line 64
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LHq;->a:Landroid/widget/ImageView;

    .line 65
    sget v0, Lxc;->more_actions_button:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LHq;->b:Landroid/view/View;

    .line 67
    new-instance v0, LDT;

    invoke-direct {v0, p3}, LDT;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LHq;->a:LDT;

    .line 69
    sget v0, Lxc;->select_thumbnail_background:I

    sget v1, Lxc;->unselect_thumbnail_background:I

    invoke-virtual {p4, p3, v0, v1}, LDk;->a(Landroid/view/View;II)LDi;

    move-result-object v0

    iput-object v0, p0, LHq;->a:LDi;

    .line 71
    return-void
.end method

.method static synthetic a(LHq;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, LHq;->a:I

    return v0
.end method


# virtual methods
.method public a()LDT;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, LHq;->a:LDT;

    return-object v0
.end method

.method public a()LDi;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LHq;->a:LDi;

    return-object v0
.end method

.method public a()LEd;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, LHq;->a:LEd;

    return-object v0
.end method

.method public a()LaGu;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, LHq;->a:LaGA;

    iget v1, p0, LHq;->a:I

    invoke-interface {v0, v1}, LaGA;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 154
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LHq;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGu;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LHq;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, LHq;->a:LEd;

    invoke-virtual {v0}, LEd;->a()V

    .line 93
    iget-object v0, p0, LHq;->a:Lakv;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, LHq;->a:Lakv;

    invoke-virtual {v0}, Lakv;->a()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, LHq;->a:Lakv;

    .line 97
    :cond_0
    return-void
.end method

.method public a(LaGA;I)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, LHq;->a:LaGA;

    .line 142
    iput p2, p0, LHq;->a:I

    .line 143
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)V
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LHq;->c(Z)V

    .line 179
    iget-object v0, p0, LHq;->a:LEd;

    invoke-virtual {v0, p1}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Z

    .line 181
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 183
    iget-object v1, p0, LHq;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 184
    return-void
.end method

.method a(LtK;LCt;)V
    .locals 2

    .prologue
    .line 105
    new-instance v0, LHr;

    invoke-direct {v0, p0, p2}, LHr;-><init>(LHq;LCt;)V

    .line 115
    iget-object v1, p0, LHq;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v1, p0, LHq;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, LHq;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_0
    sget-object v0, Lry;->S:Lry;

    invoke-interface {p1, v0}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    new-instance v0, LHs;

    invoke-direct {v0, p0, p2}, LHs;-><init>(LHq;LCt;)V

    .line 133
    iget-object v1, p0, LHq;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 134
    iget-object v1, p0, LHq;->b:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, LHq;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 138
    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, LHq;->a:LEd;

    invoke-virtual {v0, p1}, LEd;->c(Z)V

    .line 79
    return-void
.end method

.method public abstract c(Z)V
.end method

.method public d(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 167
    iget-object v3, p0, LHq;->a:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 168
    iget-object v0, p0, LHq;->a:LEd;

    invoke-virtual {v0}, LEd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LHq;->a:LEd;

    .line 169
    invoke-virtual {v0}, LEd;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 170
    :goto_1
    iget-object v3, p0, LHq;->a:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    iget-object v0, p0, LHq;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, LHq;->b:Landroid/view/View;

    if-eqz p1, :cond_5

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 175
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 167
    goto :goto_0

    :cond_3
    move v0, v1

    .line 169
    goto :goto_1

    :cond_4
    move v0, v2

    .line 170
    goto :goto_2

    :cond_5
    move v2, v1

    .line 173
    goto :goto_3
.end method
