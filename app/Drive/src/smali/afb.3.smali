.class public final Lafb;
.super Ljava/lang/Object;
.source "FeedSelectorImpl.java"

# interfaces
.implements Lafa;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    const-string v0, "https://docs.google.com/feeds/default/private/full?showdeleted=true&showroot=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "https://docs.google.com/feeds/default/private/full/-/folder?showdeleted=true&showroot=true"

    .line 38
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 37
    invoke-direct {p0, v0, v1}, Lafb;-><init>(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 39
    return-void
.end method

.method constructor <init>(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lafb;->a:Landroid/net/Uri;

    .line 44
    iput-object p2, p0, Lafb;->b:Landroid/net/Uri;

    .line 45
    return-void
.end method


# virtual methods
.method public a(LaeZ;)Lafe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaeZ;",
            ")",
            "Lafe",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p1, LaeZ;->a:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v1, p0, Lafb;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 50
    iget-object v0, p1, LaeZ;->b:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v2, p0, Lafb;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 51
    new-instance v2, Lafe;

    invoke-direct {v2, v1, v0}, Lafe;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method
