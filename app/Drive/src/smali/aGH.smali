.class public final LaGH;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGO;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGG;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGV;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGl;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaFN;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGR;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGh;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGU;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHa;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGg;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGF;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaGM;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaEi;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljavax/crypto/KeyGenerator;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lake;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 52
    iput-object p1, p0, LaGH;->a:LbrA;

    .line 53
    const-class v0, LaGO;

    const-class v1, LbuO;

    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->a:Lbsk;

    .line 56
    const-class v0, LaGG;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->b:Lbsk;

    .line 59
    const-class v0, LaGV;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->c:Lbsk;

    .line 62
    const-class v0, LaGl;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->d:Lbsk;

    .line 65
    const-class v0, LaFN;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->e:Lbsk;

    .line 68
    const-class v0, LaGR;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->f:Lbsk;

    .line 71
    const-class v0, LaGh;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->g:Lbsk;

    .line 74
    const-class v0, LaGU;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->h:Lbsk;

    .line 77
    const-class v0, LaHa;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->i:Lbsk;

    .line 80
    const-class v0, LaGg;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->j:Lbsk;

    .line 83
    const-class v0, LaGF;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->k:Lbsk;

    .line 86
    const-class v0, LaGM;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->l:Lbsk;

    .line 89
    const-class v0, LaEi;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->m:Lbsk;

    .line 92
    const-class v0, Ljavax/crypto/KeyGenerator;

    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->n:Lbsk;

    .line 95
    const-class v0, Lake;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaGH;->o:Lbsk;

    .line 98
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 158
    sparse-switch p1, :sswitch_data_0

    .line 320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :sswitch_0
    new-instance v1, LaGO;

    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ax:Lbsk;

    .line 163
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LbiH;

    iget-object v2, v2, LbiH;->ax:Lbsk;

    .line 161
    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    invoke-direct {v1, v0}, LaGO;-><init>(LbiP;)V

    move-object v0, v1

    .line 318
    :goto_0
    return-object v0

    .line 170
    :sswitch_1
    new-instance v1, LaGG;

    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 173
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 171
    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    invoke-direct {v1, v0}, LaGG;-><init>(LtK;)V

    move-object v0, v1

    .line 178
    goto :goto_0

    .line 180
    :sswitch_2
    new-instance v0, LaGV;

    iget-object v1, p0, LaGH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 183
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 181
    invoke-static {v1, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LSK;

    iget-object v2, v2, LSK;->b:Lbsk;

    .line 189
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaGH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LSK;

    iget-object v3, v3, LSK;->b:Lbsk;

    .line 187
    invoke-static {v2, v3}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LSF;

    iget-object v3, p0, LaGH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lqa;

    iget-object v3, v3, Lqa;->b:Lbsk;

    .line 195
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaGH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lqa;

    iget-object v4, v4, Lqa;->b:Lbsk;

    .line 193
    invoke-static {v3, v4}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LpW;

    iget-object v4, p0, LaGH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->o:Lbsk;

    .line 201
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaGH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->o:Lbsk;

    .line 199
    invoke-static {v4, v5}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LadL;

    iget-object v5, p0, LaGH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->g:Lbsk;

    .line 207
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaGH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->g:Lbsk;

    .line 205
    invoke-static {v5, v6}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LakW;

    iget-object v6, p0, LaGH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaHX;

    iget-object v6, v6, LaHX;->m:Lbsk;

    .line 213
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaGH;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaHX;

    iget-object v7, v7, LaHX;->m:Lbsk;

    .line 211
    invoke-static {v6, v7}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LaIm;

    invoke-direct/range {v0 .. v6}, LaGV;-><init>(LaGM;LSF;LpW;LadL;LakW;LaIm;)V

    goto/16 :goto_0

    .line 220
    :sswitch_3
    new-instance v1, LaGl;

    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaEV;

    iget-object v0, v0, LaEV;->b:Lbsk;

    .line 223
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaEV;

    iget-object v2, v2, LaEV;->b:Lbsk;

    .line 221
    invoke-static {v0, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEz;

    invoke-direct {v1, v0}, LaGl;-><init>(LaEz;)V

    move-object v0, v1

    .line 228
    goto/16 :goto_0

    .line 230
    :sswitch_4
    new-instance v0, LaFN;

    invoke-direct {v0}, LaFN;-><init>()V

    goto/16 :goto_0

    .line 234
    :sswitch_5
    new-instance v3, LaGR;

    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 237
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaGH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 235
    invoke-static {v0, v1}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LaGH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 243
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 241
    invoke-static {v1, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->a:Lbsk;

    .line 249
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LaGH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->a:Lbsk;

    .line 247
    invoke-static {v2, v4}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGO;

    invoke-direct {v3, v0, v1, v2}, LaGR;-><init>(LaGM;Laja;LaGO;)V

    move-object v0, v3

    .line 254
    goto/16 :goto_0

    .line 256
    :sswitch_6
    new-instance v0, LaGh;

    iget-object v1, p0, LaGH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaEV;

    iget-object v1, v1, LaEV;->b:Lbsk;

    .line 259
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaEV;

    iget-object v2, v2, LaEV;->b:Lbsk;

    .line 257
    invoke-static {v1, v2}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaEz;

    iget-object v2, p0, LaGH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 265
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaGH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 263
    invoke-static {v2, v3}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LaGH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 271
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaGH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->S:Lbsk;

    .line 269
    invoke-static {v3, v4}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaKM;

    iget-object v4, p0, LaGH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 277
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaGH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 275
    invoke-static {v4, v5}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LtK;

    iget-object v5, p0, LaGH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->e:Lbsk;

    .line 283
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaGH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->e:Lbsk;

    .line 281
    invoke-static {v5, v6}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaFN;

    iget-object v6, p0, LaGH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->K:Lbsk;

    .line 289
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaGH;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LagN;

    iget-object v7, v7, LagN;->K:Lbsk;

    .line 287
    invoke-static {v6, v7}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lahf;

    iget-object v7, p0, LaGH;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LagN;

    iget-object v7, v7, LagN;->M:Lbsk;

    .line 295
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LaGH;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LagN;

    iget-object v8, v8, LagN;->M:Lbsk;

    .line 293
    invoke-static {v7, v8}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lahj;

    iget-object v8, p0, LaGH;->a:LbrA;

    iget-object v8, v8, LbrA;->a:Lajo;

    iget-object v8, v8, Lajo;->X:Lbsk;

    .line 301
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LaGH;->a:LbrA;

    iget-object v9, v9, LbrA;->a:Lajo;

    iget-object v9, v9, Lajo;->X:Lbsk;

    .line 299
    invoke-static {v8, v9}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Laja;

    iget-object v9, p0, LaGH;->a:LbrA;

    iget-object v9, v9, LbrA;->a:Lajo;

    iget-object v9, v9, Lajo;->an:Lbsk;

    .line 307
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LaGH;->a:LbrA;

    iget-object v10, v10, LbrA;->a:Lajo;

    iget-object v10, v10, Lajo;->an:Lbsk;

    .line 305
    invoke-static {v9, v10}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Laja;

    iget-object v10, p0, LaGH;->a:LbrA;

    iget-object v10, v10, LbrA;->a:Lajo;

    iget-object v10, v10, Lajo;->Q:Lbsk;

    .line 313
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    iget-object v11, p0, LaGH;->a:LbrA;

    iget-object v11, v11, LbrA;->a:Lajo;

    iget-object v11, v11, Lajo;->Q:Lbsk;

    .line 311
    invoke-static {v10, v11}, LaGH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Laja;

    invoke-direct/range {v0 .. v10}, LaGh;-><init>(LaEz;LQr;LaKM;LtK;LaFN;Lahf;Lahj;Laja;Laja;Laja;)V

    goto/16 :goto_0

    .line 158
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_5
        0x28b -> :sswitch_2
        0x28c -> :sswitch_3
        0x28d -> :sswitch_6
        0x28e -> :sswitch_0
        0x290 -> :sswitch_1
        0x295 -> :sswitch_4
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 335
    sparse-switch p2, :sswitch_data_0

    .line 405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :sswitch_0
    check-cast p1, LaFY;

    .line 339
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->c:Lbsk;

    .line 342
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGV;

    .line 339
    invoke-virtual {p1, v0}, LaFY;->provideObsoleteDataCleaner(LaGV;)LaGU;

    move-result-object v0

    .line 402
    :goto_0
    return-object v0

    .line 346
    :sswitch_1
    check-cast p1, LaFY;

    .line 348
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->d:Lbsk;

    .line 351
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGl;

    .line 348
    invoke-virtual {p1, v0}, LaFY;->provideResourceIdGenerator(LaGl;)LaHa;

    move-result-object v0

    goto :goto_0

    .line 355
    :sswitch_2
    check-cast p1, LaFY;

    .line 357
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->g:Lbsk;

    .line 360
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGh;

    .line 357
    invoke-virtual {p1, v0}, LaFY;->provideDatabaseModelLoader(LaGh;)LaGg;

    move-result-object v0

    goto :goto_0

    .line 364
    :sswitch_3
    check-cast p1, LaFY;

    .line 366
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->b:Lbsk;

    .line 369
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGG;

    .line 366
    invoke-virtual {p1, v0}, LaFY;->provideGDataConverter(LaGG;)LaGF;

    move-result-object v0

    goto :goto_0

    .line 373
    :sswitch_4
    check-cast p1, LaFY;

    .line 375
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->bk:Lbsk;

    .line 378
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LaGH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 382
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    .line 375
    invoke-virtual {p1, v0, v1}, LaFY;->provideModelLoader(LbiP;LaGg;)LaGM;

    move-result-object v0

    goto :goto_0

    .line 386
    :sswitch_5
    check-cast p1, LaFY;

    .line 388
    iget-object v0, p0, LaGH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 391
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    .line 388
    invoke-virtual {p1, v0}, LaFY;->provideAppCacheManifestModelLoader(LaGg;)LaEi;

    move-result-object v0

    goto :goto_0

    .line 395
    :sswitch_6
    check-cast p1, LaFY;

    .line 397
    invoke-virtual {p1}, LaFY;->provideKeyGenerator()Ljavax/crypto/KeyGenerator;

    move-result-object v0

    goto :goto_0

    .line 400
    :sswitch_7
    check-cast p1, LaFY;

    .line 402
    invoke-virtual {p1}, LaFY;->provideNotificationManager()Lake;

    move-result-object v0

    goto :goto_0

    .line 335
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_2
        0x16 -> :sswitch_4
        0x163 -> :sswitch_1
        0x1a8 -> :sswitch_3
        0x276 -> :sswitch_0
        0x294 -> :sswitch_5
        0x296 -> :sswitch_6
        0x297 -> :sswitch_7
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 105
    const-class v0, LaGO;

    iget-object v1, p0, LaGH;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 106
    const-class v0, LaGG;

    iget-object v1, p0, LaGH;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 107
    const-class v0, LaGV;

    iget-object v1, p0, LaGH;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 108
    const-class v0, LaGl;

    iget-object v1, p0, LaGH;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 109
    const-class v0, LaFN;

    iget-object v1, p0, LaGH;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 110
    const-class v0, LaGR;

    iget-object v1, p0, LaGH;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 111
    const-class v0, LaGh;

    iget-object v1, p0, LaGH;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 112
    const-class v0, LaGU;

    iget-object v1, p0, LaGH;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 113
    const-class v0, LaHa;

    iget-object v1, p0, LaGH;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 114
    const-class v0, LaGg;

    iget-object v1, p0, LaGH;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 115
    const-class v0, LaGF;

    iget-object v1, p0, LaGH;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 116
    const-class v0, LaGM;

    iget-object v1, p0, LaGH;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 117
    const-class v0, LaEi;

    iget-object v1, p0, LaGH;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 118
    const-class v0, Ljavax/crypto/KeyGenerator;

    iget-object v1, p0, LaGH;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 119
    const-class v0, Lake;

    iget-object v1, p0, LaGH;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, LaGH;->a(Ljava/lang/Class;Lbsk;)V

    .line 120
    iget-object v0, p0, LaGH;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x28e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 122
    iget-object v0, p0, LaGH;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x290

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 124
    iget-object v0, p0, LaGH;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x28b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 126
    iget-object v0, p0, LaGH;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x28c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 128
    iget-object v0, p0, LaGH;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x295

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 130
    iget-object v0, p0, LaGH;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x22

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 132
    iget-object v0, p0, LaGH;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x28d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 134
    iget-object v0, p0, LaGH;->h:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x276

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 136
    iget-object v0, p0, LaGH;->i:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x163

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 138
    iget-object v0, p0, LaGH;->j:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x11

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 140
    iget-object v0, p0, LaGH;->k:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x1a8

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 142
    iget-object v0, p0, LaGH;->l:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x16

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 144
    iget-object v0, p0, LaGH;->m:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x294

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 146
    iget-object v0, p0, LaGH;->n:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x296

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 148
    iget-object v0, p0, LaGH;->o:Lbsk;

    const-class v1, LaFY;

    const/16 v2, 0x297

    invoke-virtual {p0, v1, v2}, LaGH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 150
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 327
    .line 329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method
