.class public abstract Lbuo;
.super Ljava/lang/Object;
.source "AbstractModule.java"

# interfaces
.implements LbuC;


# instance fields
.field a:Lcom/google/inject/Binder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lbuv;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Class;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method protected a(LbuP;)LbuQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(LbuP;)LbuQ;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Class;)LbuQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    return-object v0
.end method

.method protected a()LbuR;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/inject/Binder;->a()LbuR;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbuv;)LbuT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    return-object v0
.end method

.method protected a()Lcom/google/inject/Binder;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lbuo;->a:Lcom/google/inject/Binder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The binder can only be used inside configure()"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 79
    iget-object v0, p0, Lbuo;->a:Lcom/google/inject/Binder;

    return-object v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a()V
.end method

.method protected a(LbuC;)V
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/inject/Binder;->a(LbuC;)V

    .line 123
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbuo;->a:Lcom/google/inject/Binder;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Re-entry is not allowed."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 60
    const-string v0, "builder"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/inject/Binder;

    iput-object v0, p0, Lbuo;->a:Lcom/google/inject/Binder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62
    :try_start_1
    invoke-virtual {p0}, Lbuo;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lbuo;->a:Lcom/google/inject/Binder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67
    monitor-exit p0

    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lbuo;->a:Lcom/google/inject/Binder;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 58
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Ljava/lang/Class;LbuH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lbuo;->a()Lcom/google/inject/Binder;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/inject/Binder;->a(Ljava/lang/Class;LbuH;)V

    .line 88
    return-void
.end method
