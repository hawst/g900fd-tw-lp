.class public final LaPF;
.super Ljava/lang/Object;


# instance fields
.field public final a:I

.field public final a:J

.field public final a:LaNE;

.field public final a:LaNF;

.field public final a:LaNH;

.field public final a:LaNR;

.field public final a:Lcom/google/android/gms/internal/an;

.field public final a:Lcom/google/android/gms/internal/aq;

.field public final a:Lcom/google/android/gms/internal/fc;

.field public final a:Ljava/lang/String;

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Lorg/json/JSONObject;

.field public final a:Z

.field public final b:I

.field public final b:J

.field public final b:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final c:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:J

.field public final e:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/fc;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLaNE;LaNR;Ljava/lang/String;LaNF;LaNH;JLcom/google/android/gms/internal/aq;JJJLjava/lang/String;Lorg/json/JSONObject;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/an;",
            "Lcom/google/android/gms/internal/fc;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IJ",
            "Ljava/lang/String;",
            "Z",
            "LaNE;",
            "LaNR;",
            "Ljava/lang/String;",
            "LaNF;",
            "LaNH;",
            "J",
            "Lcom/google/android/gms/internal/aq;",
            "JJJ",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaPF;->a:Lcom/google/android/gms/internal/an;

    iput-object p2, p0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, LaPF;->a:Ljava/util/List;

    iput p4, p0, LaPF;->a:I

    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, LaPF;->b:Ljava/util/List;

    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, LaPF;->c:Ljava/util/List;

    iput p7, p0, LaPF;->b:I

    iput-wide p8, p0, LaPF;->a:J

    iput-object p10, p0, LaPF;->a:Ljava/lang/String;

    iput-boolean p11, p0, LaPF;->a:Z

    move-object/from16 v0, p12

    iput-object v0, p0, LaPF;->a:LaNE;

    move-object/from16 v0, p13

    iput-object v0, p0, LaPF;->a:LaNR;

    move-object/from16 v0, p14

    iput-object v0, p0, LaPF;->b:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, LaPF;->a:LaNF;

    move-object/from16 v0, p16

    iput-object v0, p0, LaPF;->a:LaNH;

    move-wide/from16 v0, p17

    iput-wide v0, p0, LaPF;->b:J

    move-object/from16 v0, p19

    iput-object v0, p0, LaPF;->a:Lcom/google/android/gms/internal/aq;

    move-wide/from16 v0, p20

    iput-wide v0, p0, LaPF;->c:J

    move-wide/from16 v0, p22

    iput-wide v0, p0, LaPF;->d:J

    move-wide/from16 v0, p24

    iput-wide v0, p0, LaPF;->e:J

    move-object/from16 v0, p26

    iput-object v0, p0, LaPF;->c:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, LaPF;->a:Lorg/json/JSONObject;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method
