.class public LwM;
.super Ljava/lang/Object;
.source "ServerManifestImpl.java"

# interfaces
.implements LwL;


# instance fields
.field private final a:I

.field private final a:J

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZI)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, LwM;->a:Ljava/util/Set;

    .line 38
    iput-object p2, p0, LwM;->a:Ljava/lang/String;

    .line 39
    iput-wide p3, p0, LwM;->a:J

    .line 40
    invoke-static {p5}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LwM;->b:Ljava/lang/String;

    .line 41
    invoke-static {p6}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LwM;->c:Ljava/lang/String;

    .line 42
    iput-object p7, p0, LwM;->d:Ljava/lang/String;

    .line 43
    iput-object p8, p0, LwM;->e:Ljava/lang/String;

    .line 44
    iput-boolean p9, p0, LwM;->a:Z

    .line 45
    iput p10, p0, LwM;->a:I

    .line 46
    return-void
.end method

.method public static a(Ljava/util/Set;JLjava/lang/String;)LwM;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            ")",
            "LwM;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 54
    new-instance v1, LwM;

    const-string v6, "prod"

    move-object v2, p0

    move-wide v4, p1

    move-object/from16 v7, p3

    move-object v8, v3

    move-object v9, v3

    move v11, v10

    invoke-direct/range {v1 .. v11}, LwM;-><init>(Ljava/util/Set;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, LwM;->a:I

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, LwM;->a:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, LwM;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, LwM;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, LwM;->a:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, LwM;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, LwM;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, LwM;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LwM;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 127
    instance-of v1, p1, LwM;

    if-eqz v1, :cond_0

    .line 128
    check-cast p1, LwM;

    .line 129
    iget-object v1, p0, LwM;->a:Ljava/util/Set;

    iget-object v2, p1, LwM;->a:Ljava/util/Set;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LwM;->a:Ljava/lang/String;

    iget-object v2, p1, LwM;->a:Ljava/lang/String;

    .line 130
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, LwM;->a:J

    iget-wide v4, p1, LwM;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, LwM;->b:Ljava/lang/String;

    iget-object v2, p1, LwM;->b:Ljava/lang/String;

    .line 132
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LwM;->c:Ljava/lang/String;

    iget-object v2, p1, LwM;->c:Ljava/lang/String;

    .line 133
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LwM;->d:Ljava/lang/String;

    iget-object v2, p1, LwM;->d:Ljava/lang/String;

    .line 134
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LwM;->e:Ljava/lang/String;

    iget-object v2, p1, LwM;->e:Ljava/lang/String;

    .line 135
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LwM;->a:Z

    iget-boolean v2, p1, LwM;->a:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, LwM;->a:I

    iget v2, p1, LwM;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 139
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 114
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LwM;->a:Ljava/util/Set;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LwM;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, LwM;->a:J

    .line 116
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LwM;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LwM;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LwM;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LwM;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, LwM;->a:Z

    .line 121
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, LwM;->a:I

    .line 122
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 114
    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
