.class public abstract LaRw;
.super Ljava/lang/Object;

# interfaces
.implements LaCR;
.implements LaRH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;",
        "LaCR;",
        "LaRH;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private volatile a:I

.field private a:LaRB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaRw",
            "<TT;>.aRB;"
        }
    .end annotation
.end field

.field private final a:LaRF;

.field private final a:Landroid/content/Context;

.field final a:Landroid/os/Handler;

.field private a:Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final a:Landroid/os/Looper;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LaRw",
            "<TT;>.aRy<*>;>;"
        }
    .end annotation
.end field

.field a:Z

.field private final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, LaRw;->a:[Ljava/lang/String;

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;LaCF;LaCG;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, LaRz;

    invoke-direct {v3, p2}, LaRz;-><init>(LaCF;)V

    new-instance v4, LaRC;

    invoke-direct {v4, p3}, LaRC;-><init>(LaCG;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LaRw;-><init>(Landroid/content/Context;Landroid/os/Looper;LaCX;LaCY;[Ljava/lang/String;)V

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;LaCX;LaCY;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaRw;->a:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, LaRw;->a:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LaRw;->a:Z

    invoke-static {p1}, LaSc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LaRw;->a:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, LaRw;->a:Landroid/os/Looper;

    new-instance v0, LaRF;

    invoke-direct {v0, p1, p2, p0}, LaRF;-><init>(Landroid/content/Context;Landroid/os/Looper;LaRH;)V

    iput-object v0, p0, LaRw;->a:LaRF;

    new-instance v0, LaRx;

    invoke-direct {v0, p0, p2}, LaRx;-><init>(LaRw;Landroid/os/Looper;)V

    iput-object v0, p0, LaRw;->a:Landroid/os/Handler;

    invoke-virtual {p0, p5}, LaRw;->a([Ljava/lang/String;)V

    iput-object p5, p0, LaRw;->b:[Ljava/lang/String;

    invoke-static {p3}, LaSc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCX;

    invoke-virtual {p0, v0}, LaRw;->a(LaCX;)V

    invoke-static {p4}, LaSc;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCY;

    invoke-virtual {p0, v0}, LaRw;->a(LaCY;)V

    return-void
.end method

.method static synthetic a(LaRw;)LaRB;
    .locals 1

    iget-object v0, p0, LaRw;->a:LaRB;

    return-object v0
.end method

.method static synthetic a(LaRw;LaRB;)LaRB;
    .locals 0

    iput-object p1, p0, LaRw;->a:LaRB;

    return-object p1
.end method

.method static synthetic a(LaRw;)LaRF;
    .locals 1

    iget-object v0, p0, LaRw;->a:LaRF;

    return-object v0
.end method

.method static synthetic a(LaRw;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LaRw;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, LaRw;->a:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic a(LaRw;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, LaRw;->a:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(LaRw;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, LaRw;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(LaRw;I)V
    .locals 0

    invoke-direct {p0, p1}, LaRw;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 2

    const/4 v1, 0x3

    iget v0, p0, LaRw;->a:I

    iput p1, p0, LaRw;->a:I

    if-eq v0, p1, :cond_0

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, LaRw;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, LaRw;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, LaRw;->e()V

    iget-object v0, p0, LaRw;->a:Landroid/os/IInterface;

    return-object v0
.end method

.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public final a()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, LaRw;->a:Landroid/os/Looper;

    return-object v0
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, LaRw;->a:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, LaRw;->b(I)V

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    invoke-static {v0}, LaCJ;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, LaRw;->b(I)V

    iget-object v1, p0, LaRw;->a:Landroid/os/Handler;

    iget-object v2, p0, LaRw;->a:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaRw;->a:LaRB;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, LaRw;->a:Landroid/os/IInterface;

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    invoke-static {v0}, LaRI;->a(Landroid/content/Context;)LaRI;

    move-result-object v0

    invoke-virtual {p0}, LaRw;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaRw;->a:LaRB;

    invoke-virtual {v0, v1, v2}, LaRI;->a(Ljava/lang/String;LaRB;)V

    :cond_2
    new-instance v0, LaRB;

    invoke-direct {v0, p0}, LaRB;-><init>(LaRw;)V

    iput-object v0, p0, LaRw;->a:LaRB;

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    invoke-static {v0}, LaRI;->a(Landroid/content/Context;)LaRI;

    move-result-object v0

    invoke-virtual {p0}, LaRw;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaRw;->a:LaRB;

    invoke-virtual {v0, v1, v2}, LaRI;->a(Ljava/lang/String;LaRB;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to connect to service: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LaRw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, LaRw;->a:Landroid/os/Handler;

    iget-object v1, p0, LaRw;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    iget-object v0, p0, LaRw;->a:Landroid/os/Handler;

    iget-object v1, p0, LaRw;->a:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, LaRw;->a:Landroid/os/Handler;

    iget-object v1, p0, LaRw;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, LaRD;

    invoke-direct {v3, p0, p1, p2, p3}, LaRD;-><init>(LaRw;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(LaCX;)V
    .locals 1

    iget-object v0, p0, LaRw;->a:LaRF;

    invoke-virtual {v0, p1}, LaRF;->a(LaCX;)V

    return-void
.end method

.method public a(LaCY;)V
    .locals 1

    iget-object v0, p0, LaRw;->a:LaRF;

    invoke-virtual {v0, p1}, LaRF;->a(LaCG;)V

    return-void
.end method

.method protected abstract a(LaRS;LaRA;)V
.end method

.method public final a(LaRy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaRw",
            "<TT;>.aRy<*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v1, p0, LaRw;->a:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaRw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LaRw;->a:Landroid/os/Handler;

    iget-object v1, p0, LaRw;->a:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected final a(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, LaRT;->a(Landroid/os/IBinder;)LaRS;

    move-result-object v0

    new-instance v1, LaRA;

    invoke-direct {v1, p0}, LaRA;-><init>(LaRw;)V

    invoke-virtual {p0, v0, v1}, LaRw;->a(LaRS;LaRA;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 2

    iget v0, p0, LaRw;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaRw;->a:Z

    iget-object v2, p0, LaRw;->a:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, LaRw;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LaRw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaRy;

    invoke-virtual {v0}, LaRy;->e()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, LaRw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaRw;->b(I)V

    iput-object v4, p0, LaRw;->a:Landroid/os/IInterface;

    iget-object v0, p0, LaRw;->a:LaRB;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaRw;->a:Landroid/content/Context;

    invoke-static {v0}, LaRI;->a(Landroid/content/Context;)LaRI;

    move-result-object v0

    invoke-virtual {p0}, LaRw;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaRw;->a:LaRB;

    invoke-virtual {v0, v1, v2}, LaRI;->a(Ljava/lang/String;LaRB;)V

    iput-object v4, p0, LaRw;->a:LaRB;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, LaRw;->a:Z

    return v0
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method public c()Z
    .locals 2

    iget v0, p0, LaRw;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected final e()V
    .locals 2

    invoke-virtual {p0}, LaRw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
