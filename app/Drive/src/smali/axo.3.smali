.class Laxo;
.super Ljava/lang/Object;
.source "SelectedMatch.java"


# instance fields
.field public final a:I

.field public final a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p3}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35
    const/4 v2, -0x1

    if-ne p4, v2, :cond_0

    :goto_0
    const-string v1, "selected match should be -1 if the page contains no matches"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 42
    :goto_1
    iput-object p1, p0, Laxo;->a:Ljava/lang/String;

    .line 43
    iput p2, p0, Laxo;->a:I

    .line 44
    iput-object p3, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    .line 45
    iput p4, p0, Laxo;->b:I

    .line 46
    return-void

    :cond_0
    move v0, v1

    .line 35
    goto :goto_0

    .line 38
    :cond_1
    if-ltz p4, :cond_2

    invoke-virtual {p3}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->size()I

    move-result v2

    if-ge p4, v2, :cond_2

    :goto_2
    const-string v1, "selected match is out of range"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private a(I)Laxo;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Laxo;

    iget-object v1, p0, Laxo;->a:Ljava/lang/String;

    iget v2, p0, Laxo;->a:I

    iget-object v3, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-direct {v0, v1, v2, v3, p1}, Laxo;-><init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Laxo;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Laxo;

    sget-object v1, Lcom/google/android/apps/viewer/pdflib/MatchRects;->NO_MATCHES:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    const/4 v2, -0x1

    invoke-direct {v0, p0, p1, v1, v2}, Laxo;-><init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;)Laxo;
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Laxo;->a(Ljava/lang/String;I)Laxo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laxo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Laxo;-><init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Laxo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    iget v1, p0, Laxo;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->getFirstRect(I)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LawI;
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Laxo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LawI;

    iget-object v1, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    iget v2, p0, Laxo;->b:I

    invoke-direct {v0, v1, v2}, LawI;-><init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/apps/viewer/pdflib/MatchRects;)Laxo;
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget v0, p0, Laxo;->a:I

    invoke-static {p1, v0}, Laxo;->a(Ljava/lang/String;I)Laxo;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 85
    :cond_0
    invoke-virtual {p0}, Laxo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget v0, p0, Laxo;->a:I

    invoke-static {p1, v0, p2}, Laxo;->a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;)Laxo;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0}, Laxo;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 90
    invoke-virtual {p2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->flattenToFirstRectPerMatch()Ljava/util/List;

    move-result-object v1

    .line 91
    invoke-static {v1, v0}, Lawk;->a(Ljava/util/List;Landroid/graphics/Rect;)I

    move-result v1

    .line 92
    new-instance v0, Laxo;

    iget v2, p0, Laxo;->a:I

    invoke-direct {v0, p1, v2, p2, v1}, Laxo;-><init>(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;I)V

    goto :goto_0
.end method

.method public a(Z)Laxo;
    .locals 2

    .prologue
    .line 65
    if-eqz p1, :cond_0

    iget v0, p0, Laxo;->b:I

    if-lez v0, :cond_0

    .line 66
    iget v0, p0, Laxo;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Laxo;->a(I)Laxo;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    .line 67
    :cond_0
    if-nez p1, :cond_1

    iget v0, p0, Laxo;->b:I

    iget-object v1, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 68
    iget v0, p0, Laxo;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Laxo;->a(I)Laxo;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Laxo;->a:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->isEmpty()Z

    move-result v0

    return v0
.end method
