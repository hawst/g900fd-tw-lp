.class Lpn;
.super LoX;
.source "PreIcsClassLoaderExtender.java"


# instance fields
.field private final a:Lpc;

.field private final a:Lps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lps",
            "<",
            "Ljava/lang/ClassLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lps;Lpc;Lpw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lps",
            "<",
            "Ljava/lang/ClassLoader;",
            ">;",
            "Lpc;",
            "Lpw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, LoZ;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, LoZ;-><init>(I)V

    new-instance v1, LoY;

    const/16 v2, 0xd

    invoke-direct {v1, v2}, LoY;-><init>(I)V

    invoke-direct {p0, v0, v1, p3}, LoX;-><init>(LoZ;LoY;Lpw;)V

    .line 31
    iput-object p1, p0, Lpn;->a:Lps;

    .line 32
    iput-object p2, p0, Lpn;->a:Lpc;

    .line 33
    return-void
.end method

.method private a(Ljava/util/List;Ljava/io/File;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/Object;

    .line 61
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 62
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 67
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".optimized"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0, v0, v3}, Lpn;->a(Ljava/io/File;Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v2, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 74
    :cond_0
    return-object v2
.end method

.method private a(Ljava/util/List;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 52
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 51
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 55
    :cond_0
    return-object v2
.end method


# virtual methods
.method a(Ljava/io/File;Ljava/io/File;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 79
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ldalvik/system/DexFile;->loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/util/List;Ljava/io/File;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    :try_start_0
    iget-object v0, p0, Lpn;->a:Lps;

    const-string v1, "mPaths"

    invoke-virtual {v0, v1}, Lps;->a(Ljava/lang/String;)Lpp;

    move-result-object v1

    .line 40
    iget-object v0, p0, Lpn;->a:Lps;

    const-string v2, "mDexs"

    invoke-virtual {v0, v2}, Lps;->a(Ljava/lang/String;)Lpp;

    move-result-object v2

    .line 42
    iget-object v3, p0, Lpn;->a:Lpc;

    invoke-direct {p0, p1}, Lpn;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0}, Lpc;->a(Lpp;[Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lpn;->a:Lpc;

    invoke-direct {p0, p1, p2}, Lpn;->a(Ljava/util/List;Ljava/io/File;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lpc;->a(Lpp;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
