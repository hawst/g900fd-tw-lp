.class public final LFd;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFC;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFc;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFE;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFa;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFq;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFi;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LEF;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LET;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 45
    iput-object p1, p0, LFd;->a:LbrA;

    .line 46
    const-class v0, LFC;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->a:Lbsk;

    .line 49
    const-class v0, LFc;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->b:Lbsk;

    .line 52
    const-class v0, LFE;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->c:Lbsk;

    .line 55
    const-class v0, LFa;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->d:Lbsk;

    .line 58
    const-class v0, LFq;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->e:Lbsk;

    .line 61
    const-class v0, LFi;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->f:Lbsk;

    .line 64
    const-class v0, LEF;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->g:Lbsk;

    .line 67
    const-class v0, LET;

    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LFd;->h:Lbsk;

    .line 70
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 367
    .line 369
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 468
    packed-switch p2, :pswitch_data_0

    .line 529
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :pswitch_1
    check-cast p1, LED;

    .line 472
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 475
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 472
    invoke-virtual {p1, v0}, LED;->provideOnFilterSelectedListener(Landroid/content/Context;)LFc;

    move-result-object v0

    .line 522
    :goto_0
    return-object v0

    .line 479
    :pswitch_2
    check-cast p1, LED;

    .line 481
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 484
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 481
    invoke-virtual {p1, v0}, LED;->provideSortSelectionListener(Landroid/content/Context;)LFE;

    move-result-object v0

    goto :goto_0

    .line 488
    :pswitch_3
    check-cast p1, LED;

    .line 490
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 493
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 490
    invoke-virtual {p1, v0}, LED;->provideEditTitleDialogFragmentListener(Landroid/content/Context;)LFa;

    move-result-object v0

    goto :goto_0

    .line 497
    :pswitch_4
    check-cast p1, LED;

    .line 499
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 502
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 499
    invoke-virtual {p1, v0}, LED;->provideOnAccountPickedListener(Landroid/content/Context;)LFq;

    move-result-object v0

    goto :goto_0

    .line 506
    :pswitch_5
    check-cast p1, LED;

    .line 508
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 511
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 508
    invoke-virtual {p1, v0}, LED;->provideStartupDialogListener(Landroid/content/Context;)LFi;

    move-result-object v0

    goto :goto_0

    .line 515
    :pswitch_6
    check-cast p1, LEK;

    .line 517
    invoke-virtual {p1}, LEK;->provideDialogRegistryFactory()LEF;

    move-result-object v0

    goto :goto_0

    .line 520
    :pswitch_7
    check-cast p1, LED;

    .line 522
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 525
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 522
    invoke-virtual {p1, v0}, LED;->provideOnRetryButtonClickedListener(Landroid/content/Context;)LET;

    move-result-object v0

    goto :goto_0

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 293
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 296
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x5

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 299
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x6

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 302
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/4 v2, 0x7

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 305
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x9

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 308
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xa

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 311
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xc

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 314
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x8

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 317
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xd

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 320
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xe

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 323
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 326
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xf

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 329
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x10

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 332
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x11

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LFd;->a(LbuP;LbuB;)V

    .line 335
    const-class v0, LFC;

    iget-object v1, p0, LFd;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 336
    const-class v0, LFc;

    iget-object v1, p0, LFd;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 337
    const-class v0, LFE;

    iget-object v1, p0, LFd;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 338
    const-class v0, LFa;

    iget-object v1, p0, LFd;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 339
    const-class v0, LFq;

    iget-object v1, p0, LFd;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 340
    const-class v0, LFi;

    iget-object v1, p0, LFd;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 341
    const-class v0, LEF;

    iget-object v1, p0, LFd;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 342
    const-class v0, LET;

    iget-object v1, p0, LFd;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LFd;->a(Ljava/lang/Class;Lbsk;)V

    .line 343
    iget-object v0, p0, LFd;->a:Lbsk;

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->x:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 345
    iget-object v0, p0, LFd;->b:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x48

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 347
    iget-object v0, p0, LFd;->c:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x50

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 349
    iget-object v0, p0, LFd;->d:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x4b

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 351
    iget-object v0, p0, LFd;->e:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x53

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 353
    iget-object v0, p0, LFd;->f:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x4a

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 355
    iget-object v0, p0, LFd;->g:Lbsk;

    const-class v1, LEK;

    const/16 v2, 0x54

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 357
    iget-object v0, p0, LFd;->h:Lbsk;

    const-class v1, LED;

    const/16 v2, 0x4c

    invoke-virtual {p0, v1, v2}, LFd;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 359
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 376
    packed-switch p1, :pswitch_data_0

    .line 462
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;

    .line 380
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 381
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;)V

    .line 464
    :goto_0
    return-void

    .line 384
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    .line 386
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 387
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V

    goto :goto_0

    .line 390
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    .line 392
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 393
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;)V

    goto :goto_0

    .line 396
    :pswitch_4
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;

    .line 398
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 399
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;)V

    goto :goto_0

    .line 402
    :pswitch_5
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;

    .line 404
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 405
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;)V

    goto :goto_0

    .line 408
    :pswitch_6
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    .line 410
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 411
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;)V

    goto :goto_0

    .line 414
    :pswitch_7
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;

    .line 416
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 417
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;)V

    goto :goto_0

    .line 420
    :pswitch_8
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;

    .line 422
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 423
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;)V

    goto :goto_0

    .line 426
    :pswitch_9
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    .line 428
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 429
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)V

    goto :goto_0

    .line 432
    :pswitch_a
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;

    .line 434
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 435
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;)V

    goto :goto_0

    .line 438
    :pswitch_b
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    .line 440
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 441
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    goto :goto_0

    .line 444
    :pswitch_c
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;

    .line 446
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 447
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;)V

    goto :goto_0

    .line 450
    :pswitch_d
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;

    .line 452
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 453
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;)V

    goto :goto_0

    .line 456
    :pswitch_e
    check-cast p2, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    .line 458
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 459
    invoke-virtual {v0, p2}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)V

    goto/16 :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 178
    invoke-virtual {v0, p1}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 179
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->f:Lbsk;

    .line 182
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->f:Lbsk;

    .line 180
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a:LUi;

    .line 186
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 191
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 192
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->w:Lbsk;

    .line 195
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->w:Lbsk;

    .line 193
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsP;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LsP;

    .line 199
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 202
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 200
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGM;

    .line 206
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 172
    invoke-virtual {v0, p1}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;)V

    .line 173
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 108
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 109
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 110
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LaGM;

    .line 116
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    iget-object v0, v0, LFd;->h:Lbsk;

    .line 119
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LET;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LET;

    .line 121
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->m:Lbsk;

    .line 124
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->m:Lbsk;

    .line 122
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Ladi;

    .line 128
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 97
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 98
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    iget-object v0, v0, LFd;->d:Lbsk;

    .line 101
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFa;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LFa;

    .line 103
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 253
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 254
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    iget-object v0, v0, LFd;->b:Lbsk;

    .line 257
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LFd;

    iget-object v1, v1, LFd;->b:Lbsk;

    .line 255
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFc;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a:LFc;

    .line 261
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 77
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 78
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->i:Lbsk;

    .line 81
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->i:Lbsk;

    .line 79
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a:Lrx;

    .line 85
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    iget-object v0, v0, LFd;->f:Lbsk;

    .line 88
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LFd;

    iget-object v1, v1, LFd;->f:Lbsk;

    .line 86
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFi;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a:LFi;

    .line 92
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 234
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 235
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 238
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 236
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a:LaGM;

    .line 242
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 266
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 267
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 270
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 268
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LSF;

    .line 274
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->s:Lbsk;

    .line 277
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->s:Lbsk;

    .line 275
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Laja;

    .line 281
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 284
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 282
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Lald;

    .line 288
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 133
    invoke-virtual {v0, p1}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;)V

    .line 134
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 247
    invoke-virtual {v0, p1}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;)V

    .line 248
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;)V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    .line 152
    invoke-virtual {v0, p1}, LFd;->a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 153
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->f:Lbsk;

    .line 156
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->f:Lbsk;

    .line 154
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LUi;

    .line 160
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->j:Lbsk;

    .line 163
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->j:Lbsk;

    .line 161
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LvU;

    .line 167
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 211
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 212
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LFd;

    iget-object v0, v0, LFd;->a:Lbsk;

    .line 215
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFC;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LFC;

    .line 217
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 220
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 218
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LtK;

    .line 224
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->J:Lbsk;

    .line 227
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFB;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LFB;

    .line 229
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 139
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 140
    iget-object v0, p0, LFd;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->aa:Lbsk;

    .line 143
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LFd;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->aa:Lbsk;

    .line 141
    invoke-static {v0, v1}, LFd;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;->a:Laja;

    .line 147
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 363
    return-void
.end method
