.class public final LCT;
.super Ljava/lang/Object;
.source "SelectionItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/SelectionItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/SelectionItem;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Landroid/os/Parcel;LCT;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/SelectionItem;
    .locals 1

    .prologue
    .line 91
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/SelectionItem;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, LCT;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, LCT;->a(I)[Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    return-object v0
.end method
