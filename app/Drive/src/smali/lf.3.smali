.class public final Llf;
.super Landroid/os/Handler;
.source "MediaRouteProviderService.java"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v7/media/MediaRouteProviderService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v7/media/MediaRouteProviderService;)V
    .locals 1

    .prologue
    .line 581
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 582
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Llf;->a:Ljava/lang/ref/WeakReference;

    .line 583
    return-void
.end method

.method private a(ILandroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 611
    iget-object v0, p0, Llf;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouteProviderService;

    .line 612
    if-eqz v0, :cond_0

    .line 613
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 674
    :goto_0
    return v0

    .line 615
    :pswitch_0
    invoke-static {v0, p2, p3, p4}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    .line 618
    :pswitch_1
    invoke-static {v0, p2, p3}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;I)Z

    move-result v0

    goto :goto_0

    .line 621
    :pswitch_2
    const-string v2, "routeId"

    invoke-virtual {p6, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 622
    if-eqz v2, :cond_0

    .line 623
    invoke-static {v0, p2, p3, p4, v2}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;IILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 630
    :pswitch_3
    invoke-static {v0, p2, p3, p4}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    .line 633
    :pswitch_4
    invoke-static {v0, p2, p3, p4}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    .line 636
    :pswitch_5
    invoke-static {v0, p2, p3, p4}, Landroid/support/v7/media/MediaRouteProviderService;->d(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    .line 639
    :pswitch_6
    const-string v2, "volume"

    const/4 v3, -0x1

    invoke-virtual {p6, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 640
    if-ltz v2, :cond_0

    .line 641
    invoke-static {v0, p2, p3, p4, v2}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;III)Z

    move-result v0

    goto :goto_0

    .line 648
    :pswitch_7
    const-string v2, "volume"

    invoke-virtual {p6, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 649
    if-eqz v2, :cond_0

    .line 650
    invoke-static {v0, p2, p3, p4, v2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;III)Z

    move-result v0

    goto :goto_0

    .line 657
    :pswitch_8
    instance-of v2, p5, Landroid/content/Intent;

    if-eqz v2, :cond_0

    .line 658
    check-cast p5, Landroid/content/Intent;

    invoke-static {v0, p2, p3, p4, p5}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;IILandroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 664
    :pswitch_9
    if-eqz p5, :cond_1

    instance-of v2, p5, Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 665
    :cond_1
    check-cast p5, Landroid/os/Bundle;

    invoke-static {p5}, LkQ;->a(Landroid/os/Bundle;)LkQ;

    move-result-object v1

    .line 667
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LkQ;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-static {v0, p2, p3, v1}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;ILkQ;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 587
    iget-object v2, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 588
    invoke-static {v2}, Lla;->a(Landroid/os/Messenger;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    iget v1, p1, Landroid/os/Message;->what:I

    .line 590
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 591
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 592
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 593
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v6

    move-object v0, p0

    .line 594
    invoke-direct/range {v0 .. v6}, Llf;->a(ILandroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 595
    invoke-static {}, Landroid/support/v7/media/MediaRouteProviderService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    const-string v0, "MediaRouteProviderSrv"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": Message failed, what="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", requestId="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", arg="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", obj="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", data="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_0
    invoke-static {v2, v3}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/os/Messenger;I)V

    .line 607
    :cond_1
    :goto_0
    return-void

    .line 603
    :cond_2
    invoke-static {}, Landroid/support/v7/media/MediaRouteProviderService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    const-string v0, "MediaRouteProviderSrv"

    const-string v1, "Ignoring message without valid reply messenger."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
