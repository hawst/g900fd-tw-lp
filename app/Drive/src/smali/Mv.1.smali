.class public LMv;
.super Ljava/lang/Object;
.source "DocListViewSnapshotter.java"


# instance fields
.field private final a:LMB;

.field private final a:Lcom/google/android/apps/docs/view/DocListView;

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/view/TextureView;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    iput-object v0, p0, LMv;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 34
    new-instance v0, LMB;

    invoke-direct {v0, p0, p2}, LMB;-><init>(LMv;Landroid/view/TextureView;)V

    iput-object v0, p0, LMv;->a:LMB;

    .line 35
    invoke-virtual {p1}, Lcom/google/android/apps/docs/view/DocListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LMw;

    invoke-direct {v1, p0, p1}, LMw;-><init>(LMv;Lcom/google/android/apps/docs/view/DocListView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 55
    new-instance v0, LMy;

    invoke-direct {v0, p0, p1}, LMy;-><init>(LMv;Lcom/google/android/apps/docs/view/DocListView;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 72
    return-void
.end method

.method static synthetic a(LMv;)LMB;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LMv;->a:LMB;

    return-object v0
.end method

.method static synthetic a(LMv;)Lcom/google/android/apps/docs/view/DocListView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LMv;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method static synthetic a(LMv;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, LMv;->a:Z

    return v0
.end method

.method static synthetic a(LMv;Z)Z
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, LMv;->b:Z

    return p1
.end method

.method static synthetic b(LMv;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, LMv;->b:Z

    return v0
.end method

.method static synthetic b(LMv;Z)Z
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, LMv;->a:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, LMv;->a:Z

    .line 76
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, LMv;->a:Z

    .line 80
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, LMv;->a:Lcom/google/android/apps/docs/view/DocListView;

    new-instance v1, LMA;

    invoke-direct {v1, p0}, LMA;-><init>(LMv;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->post(Ljava/lang/Runnable;)Z

    .line 89
    return-void
.end method
