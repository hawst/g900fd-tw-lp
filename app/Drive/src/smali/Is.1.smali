.class public LIs;
.super LIq;
.source "FoldersThenTitleGrouper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LIq",
        "<",
        "LIt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/text/Collator;

.field private final a:Ljava/util/Locale;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, LIs;-><init>(Ljava/util/Locale;)V

    .line 53
    return-void
.end method

.method constructor <init>(Ljava/util/Locale;)V
    .locals 2

    .prologue
    .line 62
    sget-object v0, LaES;->a:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LIg;->a:LIg;

    invoke-direct {p0, v0, v1}, LIq;-><init>(Ljava/lang/String;LIg;)V

    .line 63
    iput-object p1, p0, LIs;->a:Ljava/util/Locale;

    .line 66
    invoke-static {p1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, LIs;->a:Ljava/text/Collator;

    .line 67
    iget-object v0, p0, LIs;->a:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 68
    return-void
.end method


# virtual methods
.method protected a(LCv;)LIt;
    .locals 1

    .prologue
    .line 82
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, LIt;->a:LIt;

    return-object v0
.end method

.method protected bridge synthetic a(LCv;)LIy;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, LIs;->a(LCv;)LIt;

    move-result-object v0

    return-object v0
.end method

.method protected a(LCv;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 124
    new-instance v0, LIu;

    invoke-interface {p1}, LCv;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LIs;->a:Ljava/util/Locale;

    iget-object v3, p0, LIs;->a:Ljava/text/Collator;

    invoke-direct {v0, v1, v2, v3}, LIu;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/text/Collator;)V

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "upper(trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LIs;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")) COLLATE LOCALIZED,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LIs;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
