.class public LFU;
.super Ljava/lang/Object;
.source "DocumentOpenerArgs.java"


# direct methods
.method public static final a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    .line 33
    if-eqz p0, :cond_0

    .line 34
    const-string v0, "documentOpenMethod"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 36
    :cond_0
    if-nez v0, :cond_1

    .line 37
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 39
    :cond_1
    return-object v0
.end method

.method public static final a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    .line 24
    const-string v0, "documentOpenMethod"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 25
    return-void
.end method

.method public static final a(Landroid/os/Bundle;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    .line 19
    const-string v0, "documentOpenMethod"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 20
    return-void
.end method
