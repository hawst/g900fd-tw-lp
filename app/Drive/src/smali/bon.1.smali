.class final Lbon;
.super Ljava/util/AbstractQueue;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "Lbox",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3014
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3015
    new-instance v0, Lboo;

    invoke-direct {v0, p0}, Lboo;-><init>(Lbon;)V

    iput-object v0, p0, Lbon;->a:Lbox;

    return-void
.end method


# virtual methods
.method public a()Lbox;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3056
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    .line 3057
    iget-object v1, p0, Lbon;->a:Lbox;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(Lbox;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 3046
    invoke-interface {p1}, Lbox;->e()Lbox;

    move-result-object v0

    invoke-interface {p1}, Lbox;->d()Lbox;

    move-result-object v1

    invoke-static {v0, v1}, LbnY;->b(Lbox;Lbox;)V

    .line 3049
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->e()Lbox;

    move-result-object v0

    invoke-static {v0, p1}, LbnY;->b(Lbox;Lbox;)V

    .line 3050
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-static {p1, v0}, LbnY;->b(Lbox;Lbox;)V

    .line 3052
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lbox;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3061
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    .line 3062
    iget-object v1, p0, Lbon;->a:Lbox;

    if-ne v0, v1, :cond_0

    .line 3063
    const/4 v0, 0x0

    .line 3067
    :goto_0
    return-object v0

    .line 3066
    :cond_0
    invoke-virtual {p0, v0}, Lbon;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 3105
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    .line 3106
    :goto_0
    iget-object v1, p0, Lbon;->a:Lbox;

    if-eq v0, v1, :cond_0

    .line 3107
    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v1

    .line 3108
    invoke-static {v0}, LbnY;->c(Lbox;)V

    move-object v0, v1

    .line 3110
    goto :goto_0

    .line 3112
    :cond_0
    iget-object v0, p0, Lbon;->a:Lbox;

    iget-object v1, p0, Lbon;->a:Lbox;

    invoke-interface {v0, v1}, Lbox;->c(Lbox;)V

    .line 3113
    iget-object v0, p0, Lbon;->a:Lbox;

    iget-object v1, p0, Lbon;->a:Lbox;

    invoke-interface {v0, v1}, Lbox;->d(Lbox;)V

    .line 3114
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3085
    check-cast p1, Lbox;

    .line 3086
    invoke-interface {p1}, Lbox;->d()Lbox;

    move-result-object v0

    sget-object v1, Lbow;->a:Lbow;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 3091
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    iget-object v1, p0, Lbon;->a:Lbox;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lbox",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3118
    new-instance v0, Lbop;

    invoke-virtual {p0}, Lbon;->a()Lbox;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbop;-><init>(Lbon;Lbox;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3014
    check-cast p1, Lbox;

    invoke-virtual {p0, p1}, Lbon;->a(Lbox;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3014
    invoke-virtual {p0}, Lbon;->a()Lbox;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3014
    invoke-virtual {p0}, Lbon;->b()Lbox;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3073
    check-cast p1, Lbox;

    .line 3074
    invoke-interface {p1}, Lbox;->e()Lbox;

    move-result-object v0

    .line 3075
    invoke-interface {p1}, Lbox;->d()Lbox;

    move-result-object v1

    .line 3076
    invoke-static {v0, v1}, LbnY;->b(Lbox;Lbox;)V

    .line 3077
    invoke-static {p1}, LbnY;->c(Lbox;)V

    .line 3079
    sget-object v0, Lbow;->a:Lbow;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    .prologue
    .line 3096
    const/4 v1, 0x0

    .line 3097
    iget-object v0, p0, Lbon;->a:Lbox;

    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lbon;->a:Lbox;

    if-eq v0, v2, :cond_0

    .line 3098
    add-int/lit8 v1, v1, 0x1

    .line 3097
    invoke-interface {v0}, Lbox;->d()Lbox;

    move-result-object v0

    goto :goto_0

    .line 3100
    :cond_0
    return v1
.end method
