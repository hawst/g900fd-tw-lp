.class final enum Lakp;
.super Ljava/lang/Enum;
.source "BatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lakp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lakp;

.field private static final synthetic a:[Lakp;

.field public static final enum b:Lakp;

.field public static final enum c:Lakp;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lakp;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lakp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakp;->a:Lakp;

    .line 49
    new-instance v0, Lakp;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v3}, Lakp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakp;->b:Lakp;

    .line 51
    new-instance v0, Lakp;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, Lakp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakp;->c:Lakp;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lakp;

    sget-object v1, Lakp;->a:Lakp;

    aput-object v1, v0, v2

    sget-object v1, Lakp;->b:Lakp;

    aput-object v1, v0, v3

    sget-object v1, Lakp;->c:Lakp;

    aput-object v1, v0, v4

    sput-object v0, Lakp;->a:[Lakp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lakp;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lakp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lakp;

    return-object v0
.end method

.method public static values()[Lakp;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lakp;->a:[Lakp;

    invoke-virtual {v0}, [Lakp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lakp;

    return-object v0
.end method
