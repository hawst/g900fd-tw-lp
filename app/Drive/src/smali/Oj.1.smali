.class LOj;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "StreamRequestHandler.java"


# instance fields
.field private a:J

.field private a:Ljava/net/HttpURLConnection;


# direct methods
.method private constructor <init>(JLjava/net/HttpURLConnection;)V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 181
    iput-wide p1, p0, LOj;->a:J

    .line 182
    iput-object p3, p0, LOj;->a:Ljava/net/HttpURLConnection;

    .line 183
    return-void
.end method

.method synthetic constructor <init>(JLjava/net/HttpURLConnection;LOi;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0, p1, p2, p3}, LOj;-><init>(JLjava/net/HttpURLConnection;)V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, LOj;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 209
    iget-wide v0, p0, LOj;->a:J

    return-wide v0
.end method

.method public isRepeatable()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, LOj;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 189
    :try_start_0
    invoke-static {v0, p1}, Lbrh;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 192
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 193
    iget-object v0, p0, LOj;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 195
    return-void

    .line 191
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 192
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 193
    iget-object v0, p0, LOj;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v1
.end method
