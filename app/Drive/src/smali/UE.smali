.class LUE;
.super Ljava/lang/Object;
.source "OpenEntryLookupHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LaGu;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LUD;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/ResourceSpec;


# direct methods
.method constructor <init>(LUD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, LUE;->a:LUD;

    iput-object p2, p0, LUE;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LaGu;
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, LUE;->a:LUD;

    invoke-static {v0}, LUD;->a(LUD;)LaGM;

    move-result-object v0

    iget-object v1, p0, LUE;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 109
    iget-object v1, p0, LUE;->a:LUD;

    invoke-static {v1}, LUD;->a(LUD;)Laer;

    move-result-object v1

    iget-object v2, p0, LUE;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v1, v2}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v1

    .line 111
    const-string v2, "OpenEntryLookupHelper"

    const-string v3, "Got Entry!"

    invoke-static {v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v2, p0, LUE;->a:LUD;

    invoke-static {v2}, LUD;->a(LUD;)Lagl;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;)V

    .line 114
    invoke-interface {v1}, LaJT;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LaGv;->a:LaGv;

    invoke-virtual {v2}, LaGv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, LUE;->a:LUD;

    invoke-static {v1}, LUD;->a(LUD;)LSF;

    move-result-object v1

    invoke-interface {v1, v0}, LSF;->a(LaFM;)V

    .line 121
    :cond_0
    iget-object v0, p0, LUE;->a:LUD;

    invoke-static {v0}, LUD;->a(LUD;)LaGM;

    move-result-object v0

    iget-object v1, p0, LUE;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v0

    .line 122
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, LUE;->a()LaGu;

    move-result-object v0

    return-object v0
.end method
