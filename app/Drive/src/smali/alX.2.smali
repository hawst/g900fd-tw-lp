.class public LalX;
.super Ljava/lang/Object;
.source "MathUtils.java"


# direct methods
.method public static a(III)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18
    if-gt p1, p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "min %s < max %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 19
    if-ge p0, p1, :cond_1

    .line 24
    :goto_1
    return p1

    :cond_0
    move v0, v2

    .line 18
    goto :goto_0

    .line 21
    :cond_1
    if-le p0, p2, :cond_2

    move p1, p2

    .line 22
    goto :goto_1

    :cond_2
    move p1, p0

    .line 24
    goto :goto_1
.end method
