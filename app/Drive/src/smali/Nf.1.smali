.class public LNf;
.super LMO;
.source "SynchronousDocsUploaderImpl.java"

# interfaces
.implements LNi;


# instance fields
.field final synthetic a:LNe;


# direct methods
.method constructor <init>(LNe;LMZ;LagH;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, LNf;->a:LNe;

    .line 42
    invoke-direct {p0, p1, p2, p3}, LMO;-><init>(LMN;LMZ;LagH;)V

    .line 43
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LNf;->a:J

    .line 105
    iget-object v0, p0, LNf;->a:LMZ;

    invoke-virtual {v0}, LMZ;->a()Z

    move-result v1

    .line 106
    iget-object v0, p0, LNf;->a:LMZ;

    invoke-virtual {v0}, LMZ;->a()LaFO;

    move-result-object v3

    .line 108
    invoke-virtual {p0}, LNf;->a()Landroid/net/Uri;

    move-result-object v4

    move-object v0, p0

    move v5, v2

    .line 109
    invoke-virtual/range {v0 .. v5}, LNf;->a(ZZLaFO;Landroid/net/Uri;Z)V

    .line 110
    return-void
.end method


# virtual methods
.method protected a(LMZ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p1, p2}, LMZ;->a(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public c()LaJT;
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, LNf;->a:LNe;

    invoke-static {v0}, LNe;->a(LNe;)LqK;

    move-result-object v0

    const-string v1, "upload"

    const-string v2, "uploadStarted"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x0

    .line 54
    :try_start_0
    const-string v1, "SynchronousDocsUploaderImpl"

    const-string v2, "Uploading %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LNf;->a:LMZ;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 56
    invoke-static {}, LamS;->a()V
    :try_end_0
    .catch LQp; {:try_start_0 .. :try_end_0} :catch_1
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :try_start_1
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, LNf;->a:LMZ;

    invoke-virtual {v2}, LMZ;->a()Ljava/io/File;

    move-result-object v2

    const-string v3, "r"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, LNf;->a:Ljava/io/RandomAccessFile;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LQp; {:try_start_1 .. :try_end_1} :catch_1
    .catch LNh; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :try_start_2
    iget-object v1, p0, LNf;->a:LagH;

    invoke-interface {v1}, LagH;->a()V

    .line 66
    const-string v1, "SynchronousDocsUploaderImpl"

    const-string v2, "Beginning resumable upload"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-direct {p0}, LNf;->c()V

    .line 68
    invoke-virtual {p0, v0}, LNf;->a(LaJT;)LaJT;

    move-result-object v0

    .line 70
    invoke-static {}, LamS;->a()V

    .line 72
    iget-object v1, p0, LNf;->a:LagH;

    invoke-interface {v1}, LagH;->b()V

    .line 74
    iget-object v1, p0, LNf;->a:LNe;

    invoke-static {v1}, LNe;->a(LNe;)LqK;

    move-result-object v1

    const-string v2, "upload"

    const-string v3, "uploadSucceeded"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch LQp; {:try_start_2 .. :try_end_2} :catch_1
    .catch LNh; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    iget-object v1, p0, LNf;->a:LMZ;

    invoke-virtual {v1}, LMZ;->close()V

    .line 89
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    :try_start_3
    new-instance v1, LNh;

    const-string v2, "Unable to upload file: "

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch LQp; {:try_start_3 .. :try_end_3} :catch_1
    .catch LNh; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 76
    :catch_1
    move-exception v0

    .line 77
    :try_start_4
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "Upload interrupted."

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 86
    :catchall_0
    move-exception v0

    iget-object v1, p0, LNf;->a:LMZ;

    invoke-virtual {v1}, LMZ;->close()V

    throw v0

    .line 78
    :catch_2
    move-exception v0

    .line 79
    :try_start_5
    iget-object v1, p0, LNf;->a:LagH;

    sget-object v2, LafT;->i:LafT;

    invoke-interface {v1, v2}, LagH;->a(LafT;)V

    .line 80
    throw v0

    .line 81
    :catch_3
    move-exception v0

    .line 82
    iget-object v1, p0, LNf;->a:LagH;

    sget-object v2, LafT;->o:LafT;

    invoke-interface {v1, v2}, LagH;->a(LafT;)V

    .line 83
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
