.class public LaEM;
.super Ljava/lang/Object;
.source "EntryChangeDatabaseHelper.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "LaEC;",
            ">;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Ljava/util/List",
            "<",
            "LaEO;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, LboS;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaEM;->a:Ljava/util/Map;

    .line 52
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, LaEM;->a:Ljava/lang/ref/ReferenceQueue;

    .line 39
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 125
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x2

    if-ge v1, v0, :cond_0

    .line 126
    iget-object v0, p0, LaEM;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, LaEO;

    .line 127
    if-nez v0, :cond_1

    .line 132
    :cond_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0, v0}, LaEM;->a(LaEO;)V

    .line 125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method a(LaEO;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, LaEM;->a:Ljava/util/Map;

    iget-object v1, p1, LaEO;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 103
    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 107
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaEO;

    .line 109
    invoke-virtual {v1}, LaEO;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 110
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 113
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, LaEM;->a:Ljava/util/Map;

    iget-object v1, p1, LaEO;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)V
    .locals 3

    .prologue
    .line 67
    invoke-static {}, LamV;->a()V

    .line 68
    iget-object v0, p0, LaEM;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 69
    if-nez v0, :cond_0

    .line 70
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 71
    iget-object v1, p0, LaEM;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :cond_0
    new-instance v1, LaEO;

    iget-object v2, p0, LaEM;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, p1, p2, v2}, LaEO;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    invoke-direct {p0}, LaEM;->a()V

    .line 75
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LaGu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 166
    iget-object v1, p0, LaEM;->a:Ljava/util/Map;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 167
    if-eqz v1, :cond_0

    .line 170
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v3

    new-instance v4, LaEN;

    invoke-direct {v4, v0, v1}, LaEN;-><init>(LaGu;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 172
    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaEC;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-static {}, LamV;->a()V

    .line 82
    iget-object v0, p0, LaEM;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 83
    if-nez v0, :cond_0

    move v0, v2

    .line 97
    :goto_0
    return v0

    .line 86
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 87
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 88
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaEO;

    .line 89
    invoke-virtual {v1}, LaEO;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 91
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, LaEM;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 97
    goto :goto_0
.end method
