.class public Lbed;
.super Lbfz;
.source "HttpHeaders.java"


# instance fields
.field private a:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Accept-Encoding"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Authorization"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Content-Encoding"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Content-Length"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Content-Range"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Content-Type"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation runtime LbfE;
        a = "If-Modified-Since"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation runtime LbfE;
        a = "If-Match"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation runtime LbfE;
        a = "If-None-Match"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation runtime LbfE;
        a = "If-Unmodified-Since"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation runtime LbfE;
        a = "If-Range"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Location"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation runtime LbfE;
        a = "Range"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation runtime LbfE;
        a = "User-Agent"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    sget-object v0, LbfC;->a:LbfC;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lbfz;-><init>(Ljava/util/EnumSet;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "gzip"

    .line 70
    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lbed;->a:Ljava/util/List;

    .line 62
    return-void
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 1177
    invoke-static {p1, p0}, Lbfs;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1178
    invoke-static {v0, p2}, Lbfs;->a(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1017
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 899
    instance-of v0, p0, Ljava/lang/Enum;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Enum;

    .line 900
    invoke-static {p0}, Lbfy;->a(Ljava/lang/Enum;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1022
    if-nez p1, :cond_0

    .line 1023
    const/4 v0, 0x0

    .line 1027
    :goto_0
    return-object v0

    .line 1025
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1026
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;)V
    .locals 6

    .prologue
    .line 916
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;Ljava/io/Writer;)V

    .line 917
    return-void
.end method

.method static a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;Ljava/io/Writer;)V
    .locals 10

    .prologue
    .line 941
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 942
    invoke-virtual {p0}, Lbed;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 943
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 944
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "multiple headers of the same name (headers are case insensitive): %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LbfN;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 946
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 947
    if-eqz v5, :cond_0

    .line 950
    invoke-virtual {p0}, Lbed;->getClassInfo()Lbfq;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbfq;->a(Ljava/lang/String;)Lbfy;

    move-result-object v0

    .line 951
    if-eqz v0, :cond_5

    .line 952
    invoke-virtual {v0}, Lbfy;->a()Ljava/lang/String;

    move-result-object v4

    .line 954
    :goto_1
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 955
    instance-of v1, v5, Ljava/lang/Iterable;

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 956
    :cond_1
    invoke-static {v5}, LbfV;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v6, p5

    .line 957
    invoke-static/range {v0 .. v6}, Lbed;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lbet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_2

    :cond_2
    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v6, p5

    .line 966
    invoke-static/range {v0 .. v6}, Lbed;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lbet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_0

    .line 970
    :cond_3
    if-eqz p5, :cond_4

    .line 971
    invoke-virtual {p5}, Ljava/io/Writer;->flush()V

    .line 973
    :cond_4
    return-void

    :cond_5
    move-object v4, v1

    goto :goto_1
.end method

.method public static a(Lbed;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Ljava/io/Writer;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 932
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;Ljava/io/Writer;)V

    .line 933
    return-void
.end method

.method private static a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lbet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 863
    if-eqz p5, :cond_0

    invoke-static {p5}, Lbfs;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 893
    :cond_0
    :goto_0
    return-void

    .line 867
    :cond_1
    invoke-static {p5}, Lbed;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 870
    const-string v0, "Authorization"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Cookie"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    if-eqz p0, :cond_3

    sget-object v0, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    .line 871
    invoke-virtual {p0, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 872
    :cond_3
    const-string v0, "<Not Logged>"

    .line 874
    :goto_1
    if-eqz p1, :cond_4

    .line 875
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 876
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 877
    sget-object v2, LbfS;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 879
    :cond_4
    if-eqz p2, :cond_5

    .line 880
    const-string v2, " -H \'"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 883
    :cond_5
    if-eqz p3, :cond_6

    .line 884
    invoke-virtual {p3, p4, v1}, Lbet;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_6
    if-eqz p6, :cond_0

    .line 888
    invoke-virtual {p6, p4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 889
    const-string v0, ": "

    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 890
    invoke-virtual {p6, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 891
    const-string v0, "\r\n"

    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()Lbed;
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Lbfz;->clone()Lbfz;

    move-result-object v0

    check-cast v0, Lbed;

    return-object v0
.end method

.method public a(Ljava/lang/Long;)Lbed;
    .locals 1

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->d:Ljava/util/List;

    .line 341
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->a:Ljava/util/List;

    .line 222
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lbed;
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, Lbfz;->set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;

    move-result-object v0

    check-cast v0, Lbed;

    return-object v0
.end method

.method public a(Ljava/util/List;)Lbed;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lbed;"
        }
    .end annotation

    .prologue
    .line 268
    iput-object p1, p0, Lbed;->b:Ljava/util/List;

    .line 269
    return-object p0
.end method

.method public final a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lbed;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lbed;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lbed;)V
    .locals 5

    .prologue
    .line 1082
    :try_start_0
    new-instance v0, Lbef;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbef;-><init>(Lbed;Ljava/lang/StringBuilder;)V

    .line 1083
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Lbee;

    invoke-direct {v4, p0, v0}, Lbee;-><init>(Lbed;Lbef;)V

    invoke-static {p1, v1, v2, v3, v4}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;)V

    .line 1085
    invoke-virtual {v0}, Lbef;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1090
    return-void

    .line 1086
    :catch_0
    move-exception v0

    .line 1088
    invoke-static {v0}, LbfU;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final a(Lbeu;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    .line 985
    invoke-virtual {p0}, Lbed;->clear()V

    .line 986
    new-instance v1, Lbef;

    invoke-direct {v1, p0, p2}, Lbef;-><init>(Lbed;Ljava/lang/StringBuilder;)V

    .line 987
    invoke-virtual {p1}, Lbeu;->b()I

    move-result v2

    .line 988
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 989
    invoke-virtual {p1, v0}, Lbeu;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0}, Lbeu;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v1}, Lbed;->a(Ljava/lang/String;Ljava/lang/String;Lbef;)V

    .line 988
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 991
    :cond_0
    invoke-virtual {v1}, Lbef;->a()V

    .line 992
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Lbef;)V
    .locals 6

    .prologue
    .line 1130
    iget-object v2, p3, Lbef;->a:Ljava/util/List;

    .line 1131
    iget-object v0, p3, Lbef;->a:Lbfq;

    .line 1132
    iget-object v1, p3, Lbef;->a:Lbfk;

    .line 1133
    iget-object v3, p3, Lbef;->a:Ljava/lang/StringBuilder;

    .line 1135
    if-eqz v3, :cond_0

    .line 1136
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LbfS;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139
    :cond_0
    invoke-virtual {v0, p1}, Lbfq;->a(Ljava/lang/String;)Lbfy;

    move-result-object v3

    .line 1140
    if-eqz v3, :cond_5

    .line 1141
    invoke-virtual {v3}, Lbfy;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v2, v0}, Lbfs;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v4

    .line 1143
    invoke-static {v4}, LbfV;->a(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1146
    invoke-static {v4}, LbfV;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v2, v0}, LbfV;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 1147
    invoke-virtual {v3}, Lbfy;->a()Ljava/lang/reflect/Field;

    move-result-object v3

    .line 1148
    invoke-static {v0, v2, p2}, Lbed;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 1147
    invoke-virtual {v1, v3, v0, v2}, Lbfk;->a(Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 1174
    :goto_0
    return-void

    .line 1150
    :cond_1
    invoke-static {v2, v4}, LbfV;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Iterable;

    .line 1149
    invoke-static {v0, v1}, LbfV;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1153
    invoke-virtual {v3, p0}, Lbfy;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1154
    if-nez v0, :cond_2

    .line 1155
    invoke-static {v4}, Lbfs;->a(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v0

    .line 1156
    invoke-virtual {v3, p0, v0}, Lbfy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1158
    :cond_2
    const-class v1, Ljava/lang/Object;

    if-ne v4, v1, :cond_3

    const/4 v1, 0x0

    .line 1159
    :goto_1
    invoke-static {v1, v2, p2}, Lbed;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1158
    :cond_3
    invoke-static {v4}, LbfV;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    goto :goto_1

    .line 1162
    :cond_4
    invoke-static {v4, v2, p2}, Lbed;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, Lbfy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1167
    :cond_5
    invoke-virtual {p0, p1}, Lbed;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1168
    if-nez v0, :cond_6

    .line 1169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1170
    invoke-virtual {p0, p1, v0}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    .line 1172
    :cond_6
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbed;->a(Ljava/util/List;)Lbed;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lbed;->f:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 316
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->c:Ljava/util/List;

    .line 317
    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lbed;->l:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbed;->a()Lbed;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbed;->a()Lbed;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->e:Ljava/util/List;

    .line 389
    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lbed;->m:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->f:Ljava/util/List;

    .line 413
    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lbed;->n:Ljava/util/List;

    invoke-direct {p0, v0}, Lbed;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->g:Ljava/util/List;

    .line 537
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 560
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->h:Ljava/util/List;

    .line 561
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 584
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->i:Ljava/util/List;

    .line 585
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 608
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->j:Ljava/util/List;

    .line 609
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 632
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->k:Ljava/util/List;

    .line 633
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 728
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->m:Ljava/util/List;

    .line 729
    return-object p0
.end method

.method public l(Ljava/lang/String;)Lbed;
    .locals 1

    .prologue
    .line 776
    invoke-direct {p0, p1}, Lbed;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbed;->n:Ljava/util/List;

    .line 777
    return-object p0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    move-result-object v0

    return-object v0
.end method
