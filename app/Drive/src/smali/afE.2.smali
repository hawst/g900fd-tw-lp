.class public LafE;
.super Ljava/lang/Object;
.source "AccountMetadataUpdater.java"


# instance fields
.field private final a:LQr;

.field private final a:LTT;

.field private final a:LTh;

.field private final a:LaGg;

.field private final a:LaJR;

.field private final a:LaJZ;

.field private final a:LaKM;

.field private final a:LaeH;

.field private final a:LalO;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LajN;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method constructor <init>(Landroid/content/Context;LaKM;LaGg;LTh;LaeH;LtK;LaJZ;Ljava/util/Map;LaJR;LalO;LTT;LqK;LQr;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation runtime Lann;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LaKM;",
            "LaGg;",
            "LTh;",
            "LaeH;",
            "LtK;",
            "LaJZ;",
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LajN;",
            ">;",
            "LaJR;",
            "LalO;",
            "LTT;",
            "LqK;",
            "LQr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, LafE;->a:Landroid/content/Context;

    .line 102
    iput-object p2, p0, LafE;->a:LaKM;

    .line 103
    iput-object p3, p0, LafE;->a:LaGg;

    .line 104
    iput-object p4, p0, LafE;->a:LTh;

    .line 105
    iput-object p5, p0, LafE;->a:LaeH;

    .line 106
    iput-object p6, p0, LafE;->a:LtK;

    .line 107
    iput-object p7, p0, LafE;->a:LaJZ;

    .line 108
    iput-object p8, p0, LafE;->a:Ljava/util/Map;

    .line 109
    iput-object p9, p0, LafE;->a:LaJR;

    .line 110
    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalO;

    iput-object v0, p0, LafE;->a:LalO;

    .line 111
    iput-object p11, p0, LafE;->a:LTT;

    .line 112
    iput-object p12, p0, LafE;->a:LqK;

    .line 113
    iput-object p13, p0, LafE;->a:LQr;

    .line 114
    return-void
.end method

.method private a(LaeH;Ljava/lang/String;)LaJQ;
    .locals 2

    .prologue
    .line 282
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 283
    invoke-virtual {p1, v0}, LaeH;->a(Ljava/io/InputStream;)LaeG;

    move-result-object v1

    .line 285
    :try_start_0
    invoke-virtual {v1}, LaeG;->b()Lbxb;

    move-result-object v0

    check-cast v0, LaJQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    invoke-virtual {v1}, LaeG;->a()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LaeG;->a()V

    throw v0
.end method

.method private a(LaFO;II)Ljava/lang/String;
    .locals 5

    .prologue
    .line 200
    const-string v0, "https://docs.google.com/feeds/metadata/default"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 203
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "include-installed-apps"

    const-string v4, "true"

    .line 207
    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "remaining-changestamps-first"

    .line 208
    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "remaining-changestamps-limit"

    .line 209
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 210
    iget-object v1, p0, LafE;->a:LtK;

    sget-object v2, Lry;->au:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, LafE;->a:LalO;

    invoke-virtual {v1, p1, v0}, LalO;->a(LaFO;Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 213
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 215
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 216
    const-string v0, "GData-Version"

    const-string v2, "3.0"

    invoke-virtual {v1, v0, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :try_start_0
    iget-object v0, p0, LafE;->a:LTh;

    invoke-interface {v0, p1, v1}, LTh;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    .line 221
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 226
    iget-object v1, p0, LafE;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 227
    iget-object v1, p0, LafE;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    return-object v0

    .line 223
    :cond_1
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server returned error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    :catchall_0
    move-exception v0

    iget-object v1, p0, LafE;->a:LTh;

    invoke-interface {v1}, LTh;->a()V

    .line 227
    iget-object v1, p0, LafE;->a:LTh;

    invoke-interface {v1}, LTh;->b()V

    throw v0
.end method

.method private a(LaFO;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 303
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    :try_start_0
    iget-object v0, p0, LafE;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LMl;->b:LMl;

    .line 308
    invoke-virtual {v1, p2}, LMl;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->d:Lajw;

    .line 309
    invoke-virtual {v2}, Lajw;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LbmF;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Lajw;

    .line 310
    invoke-virtual {v3}, Lajw;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaFr;

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 311
    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    .line 307
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    move-object v1, v0

    .line 326
    :goto_0
    if-nez v1, :cond_0

    .line 364
    :goto_1
    return-void

    .line 312
    :catch_0
    move-exception v0

    .line 313
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Permission denied for provider %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p2, v3, v8

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 315
    :catch_1
    move-exception v0

    .line 319
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "%1$s CrossAppStateProvider doesn\'t recognize ACCOUNT_METADATA URI. Consider upgrading %1$s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p2, v3, v8

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 322
    :catch_2
    move-exception v0

    .line 323
    iget-object v1, p0, LafE;->a:LqK;

    iget-object v2, p0, LafE;->a:LQr;

    invoke-static {v0, v1, v2}, LMq;->a(Ljava/lang/RuntimeException;LqK;LQr;)V

    move-object v1, v6

    goto :goto_0

    .line 332
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 333
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    if-le v0, v9, :cond_2

    .line 334
    :cond_1
    const-string v2, "AccountMetadataUpdater"

    const-string v3, "%s did not return exactly one row of account metadata. Number of rows = %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    .line 335
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    .line 334
    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 339
    :cond_2
    :try_start_2
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 340
    iget-object v2, p0, LafE;->a:LaGg;

    invoke-interface {v2}, LaGg;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 342
    :try_start_3
    iget-object v2, p0, LafE;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(LaFM;)LaFP;

    move-result-object v2

    .line 344
    sget-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->b:Lajw;

    .line 345
    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFr;

    invoke-virtual {v0, v1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 346
    invoke-virtual {v2}, LaFP;->a()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-ltz v0, :cond_3

    iget-object v0, p0, LafE;->a:LaKM;

    .line 347
    invoke-interface {v0}, LaKM;->a()J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    .line 359
    :cond_3
    :try_start_4
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 351
    :cond_4
    :try_start_5
    sget-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->c:Lajw;

    .line 352
    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFr;

    invoke-virtual {v0, v1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 353
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v0, v3}, LaFP;->a(Ljava/lang/String;Ljava/util/Date;)V

    .line 354
    invoke-virtual {v2}, LaFP;->e()V

    .line 355
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 359
    :try_start_6
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 362
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 356
    :catch_3
    move-exception v0

    .line 357
    :try_start_7
    const-string v2, "AccountMetadataUpdater"

    const-string v3, "Failed to retrieve metatadata from %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 359
    :try_start_8
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 362
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 359
    :catchall_1
    move-exception v0

    :try_start_9
    iget-object v2, p0, LafE;->a:LaGg;

    invoke-interface {v2}, LaGg;->b()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
.end method

.method private b(LaFO;II)LaJQ;
    .locals 6

    .prologue
    .line 181
    iget-object v0, p0, LafE;->a:LaJZ;

    .line 182
    invoke-interface {v0, p1}, LaJZ;->a(LaFO;)LaJY;

    move-result-object v0

    .line 183
    int-to-long v2, p2

    int-to-long v4, p3

    invoke-interface {v0, v2, v3, v4, v5}, LaJY;->a(JJ)LaJQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LaFO;)LaJQ;
    .locals 3

    .prologue
    .line 243
    :try_start_0
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, LaFQ;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, LafE;->a(LaFO;II)LaJQ;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    .line 257
    :goto_0
    return-object v0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Error updating account capability"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 257
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :catch_1
    move-exception v0

    .line 248
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Error updating account capability"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1

    .line 249
    :catch_2
    move-exception v0

    .line 250
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Error updating account capability"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1

    .line 251
    :catch_3
    move-exception v0

    .line 252
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Error updating account capability"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1

    .line 253
    :catch_4
    move-exception v0

    .line 254
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "Error updating account capability"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(LaFO;II)LaJQ;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 131
    if-lez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 135
    iget-object v0, p0, LafE;->a:LtK;

    sget-object v2, Lry;->J:Lry;

    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    invoke-direct {p0, p1, p2, p3}, LafE;->b(LaFO;II)LaJQ;

    move-result-object v2

    .line 138
    if-eqz v2, :cond_2

    invoke-interface {v2}, LaJQ;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v2

    .line 146
    :cond_0
    :goto_2
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 147
    iget-object v2, p0, LafE;->a:LaGg;

    invoke-interface {v2}, LaGg;->a()V

    .line 149
    :try_start_0
    iget-object v2, p0, LafE;->a:LaGg;

    invoke-interface {v2, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v2

    .line 150
    iget-object v3, p0, LafE;->a:LaGg;

    invoke-interface {v3, v2}, LaGg;->a(LaFM;)LaFP;

    move-result-object v2

    .line 151
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v0, v3}, LaFP;->a(Ljava/lang/String;Ljava/util/Date;)V

    .line 152
    invoke-virtual {v2}, LaFP;->e()V

    .line 153
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    iget-object v0, p0, LafE;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 156
    const-string v0, "AccountMetadataUpdater"

    const-string v2, "capability updated"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, LafE;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    return-object v1

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 138
    goto :goto_1

    .line 140
    :cond_3
    invoke-direct {p0, p1, p2, p3}, LafE;->a(LaFO;II)Ljava/lang/String;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_0

    iget-object v1, p0, LafE;->a:LaeH;

    invoke-direct {p0, v1, v0}, LafE;->a(LaeH;Ljava/lang/String;)LaJQ;

    move-result-object v1

    goto :goto_2

    .line 155
    :catchall_0
    move-exception v0

    iget-object v1, p0, LafE;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    .line 156
    const-string v1, "AccountMetadataUpdater"

    const-string v2, "capability updated"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    .line 161
    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No account metadata found for account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LaFO;Ljava/lang/String;)LaJQ;
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, LafE;->a:LtK;

    sget-object v1, Lry;->J:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, LafE;->a:LaJR;

    invoke-interface {v0, p2}, LaJR;->a(Ljava/lang/String;)LaJQ;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LafE;->a:LaeH;

    invoke-direct {p0, v0, p2}, LafE;->a(LaeH;Ljava/lang/String;)LaJQ;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaFO;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, LafE;->a:LTT;

    invoke-interface {v0}, LTT;->a()Ljava/util/List;

    move-result-object v0

    .line 297
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 298
    invoke-direct {p0, p1, v0}, LafE;->a(LaFO;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_0
    return-void
.end method
