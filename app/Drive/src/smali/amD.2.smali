.class final enum LamD;
.super Ljava/lang/Enum;
.source "RateLimitedExecutorImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LamD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LamD;

.field private static final synthetic a:[LamD;

.field public static final enum b:LamD;

.field public static final enum c:LamD;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, LamD;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LamD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LamD;->a:LamD;

    .line 33
    new-instance v0, LamD;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LamD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LamD;->b:LamD;

    .line 39
    new-instance v0, LamD;

    const-string v1, "READY_FOR_SCHEDULING"

    invoke-direct {v0, v1, v4}, LamD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LamD;->c:LamD;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [LamD;

    sget-object v1, LamD;->a:LamD;

    aput-object v1, v0, v2

    sget-object v1, LamD;->b:LamD;

    aput-object v1, v0, v3

    sget-object v1, LamD;->c:LamD;

    aput-object v1, v0, v4

    sput-object v0, LamD;->a:[LamD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LamD;
    .locals 1

    .prologue
    .line 28
    const-class v0, LamD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LamD;

    return-object v0
.end method

.method public static values()[LamD;
    .locals 1

    .prologue
    .line 28
    sget-object v0, LamD;->a:[LamD;

    invoke-virtual {v0}, [LamD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LamD;

    return-object v0
.end method
