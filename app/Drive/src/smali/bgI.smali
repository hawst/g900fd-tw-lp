.class public abstract LbgI;
.super Ljava/lang/Object;
.source "PdfObject.java"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LbgI;->a:J

    return-void
.end method

.method public static final a(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 47
    :try_start_0
    const-string v0, "ISO-8859-1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ljava/io/OutputStream;)I
.end method

.method public a()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, LbgI;->a:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 27
    iput-wide p1, p0, LbgI;->a:J

    .line 28
    return-void
.end method

.method protected abstract b(Ljava/io/OutputStream;)I
.end method

.method protected abstract c(Ljava/io/OutputStream;)I
.end method

.method public d(Ljava/io/OutputStream;)I
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0, p1}, LbgI;->b(Ljava/io/OutputStream;)I

    move-result v0

    .line 63
    invoke-virtual {p0, p1}, LbgI;->a(Ljava/io/OutputStream;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    invoke-virtual {p0, p1}, LbgI;->c(Ljava/io/OutputStream;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    return v0
.end method
