.class public final Lbvd;
.super Ljava/lang/Object;
.source "Errors.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lbvi",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final a:Lbvd;

.field private final a:Ljava/lang/Object;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbwy;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbvd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lbvd;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 670
    new-instance v0, Lbvf;

    const-class v1, Ljava/lang/Class;

    invoke-direct {v0, v1}, Lbvf;-><init>(Ljava/lang/Class;)V

    new-instance v1, Lbvg;

    const-class v2, Ljava/lang/reflect/Member;

    invoke-direct {v1, v2}, Lbvg;-><init>(Ljava/lang/Class;)V

    new-instance v2, Lbvh;

    const-class v3, Lbuv;

    invoke-direct {v2, v3}, Lbvh;-><init>(Ljava/lang/Class;)V

    invoke-static {v0, v1, v2}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    sput-object v0, Lbvd;->a:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p0, p0, Lbvd;->a:Lbvd;

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lbvd;->b:Lbvd;

    .line 114
    sget-object v0, LbvG;->a:Ljava/lang/Object;

    iput-object v0, p0, Lbvd;->a:Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p0, p0, Lbvd;->a:Lbvd;

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lbvd;->b:Lbvd;

    .line 120
    iput-object p1, p0, Lbvd;->a:Ljava/lang/Object;

    .line 121
    return-void
.end method

.method private varargs a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Lbvd;
    .locals 3

    .prologue
    .line 537
    invoke-static {p2, p3}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 538
    new-instance v1, Lbwy;

    invoke-virtual {p0}, Lbvd;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2, v0, p1}, Lbwy;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v1}, Lbvd;->a(Lbwy;)Lbvd;

    .line 539
    return-object p0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 693
    const/4 v0, 0x0

    .line 694
    instance-of v1, p0, Lbwv;

    if-eqz v1, :cond_0

    .line 695
    check-cast p0, Lbwv;

    .line 696
    invoke-virtual {p0}, Lbwv;->a()Ljava/lang/Object;

    move-result-object v0

    .line 698
    :goto_0
    invoke-static {v0, p0}, Lbvd;->a(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, v0

    move-object v0, p0

    move-object p0, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 702
    sget-object v0, Lbvd;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvi;

    .line 703
    invoke-virtual {v0, p0}, Lbvi;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 704
    invoke-virtual {v0, p0}, Lbvi;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lbvd;->b(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v0

    .line 707
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1}, Lbvd;->b(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lbwv;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 722
    if-nez p0, :cond_0

    .line 723
    const-string v0, ""

    .line 748
    :goto_0
    return-object v0

    .line 725
    :cond_0
    invoke-virtual {p0}, Lbwv;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 727
    :goto_1
    invoke-virtual {p0}, Lbwv;->a()Lbwv;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 728
    invoke-virtual {p0}, Lbwv;->a()Lbwv;

    move-result-object p0

    .line 729
    const/4 v0, 0x0

    invoke-virtual {p0}, Lbwv;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    goto :goto_1

    .line 731
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 732
    const-string v0, ""

    goto :goto_0

    .line 740
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, " (via modules: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 741
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 742
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    if-eqz v1, :cond_3

    .line 744
    const-string v0, " -> "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 747
    :cond_4
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lbwy;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 572
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    const-string v1, ":%n%n"

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    .line 574
    invoke-static {p1}, Lbvd;->a(Ljava/util/Collection;)Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    move v1, v2

    .line 576
    :goto_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwy;

    .line 577
    const-string v8, "%s) %s%n"

    const/4 v5, 0x2

    new-array v9, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v3

    invoke-virtual {v0}, Lbwy;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v2

    invoke-virtual {v6, v8, v9}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 579
    invoke-virtual {v0}, Lbwy;->a()Ljava/util/List;

    move-result-object v8

    .line 580
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    :goto_2
    if-ltz v4, :cond_1

    .line 581
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .line 582
    invoke-static {v6, v9}, Lbvd;->a(Ljava/util/Formatter;Ljava/lang/Object;)V

    .line 580
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    :cond_0
    move v1, v3

    .line 574
    goto :goto_0

    .line 585
    :cond_1
    invoke-virtual {v0}, Lbwy;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 586
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 587
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 588
    new-instance v8, Ljava/io/PrintWriter;

    invoke-direct {v8, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v8}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 589
    const-string v0, "Caused by: %s"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-virtual {v6, v0, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 592
    :cond_2
    const-string v0, "%n"

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move v4, v5

    .line 593
    goto :goto_1

    .line 595
    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 596
    const-string v0, "1 error"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v6, v0, v1}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 601
    :goto_3
    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 598
    :cond_4
    const-string v0, "%s errors"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v6, v0, v1}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_3
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 551
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 552
    aget-object v1, p1, v0

    invoke-static {v1}, Lbvd;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, p1, v0

    .line 551
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 554
    :cond_0
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Ljava/lang/Throwable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lbwy;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 630
    .line 631
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwy;

    .line 632
    invoke-virtual {v0}, Lbwy;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 633
    if-eqz v0, :cond_0

    .line 637
    if-eqz v1, :cond_1

    .line 644
    :goto_1
    return-object v2

    :cond_1
    move-object v1, v0

    .line 642
    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 644
    goto :goto_1
.end method

.method public static a(Ljava/util/Formatter;Lbwt;Lbww;Lbwv;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Formatter;",
            "Lbwt",
            "<*>;",
            "Lbww;",
            "Lbwv;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 794
    invoke-virtual {p2}, Lbww;->a()Ljava/lang/reflect/Member;

    move-result-object v1

    .line 795
    invoke-static {v1}, LbvF;->a(Ljava/lang/reflect/Member;)Ljava/lang/Class;

    move-result-object v0

    .line 797
    const-class v2, Ljava/lang/reflect/Field;

    if-ne v0, v2, :cond_0

    .line 798
    invoke-virtual {p2}, Lbww;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwt;

    .line 799
    const-string v2, "  while locating %s%n"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lbwt;->a()Lbuv;

    move-result-object v0

    invoke-static {v0, p3}, Lbvd;->a(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 800
    const-string v0, "    for field at %s%n"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1}, LbvH;->a(Ljava/lang/reflect/Member;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v2, v4

    invoke-virtual {p0, v0, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 810
    :goto_0
    return-void

    .line 802
    :cond_0
    if-eqz p1, :cond_1

    .line 803
    const-string v0, "  while locating %s%n"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lbwt;->a()Lbuv;

    move-result-object v3

    invoke-static {v3, p3}, Lbvd;->a(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 804
    const-string v0, "    for parameter %s at %s%n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 805
    invoke-virtual {p1}, Lbwt;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1}, LbvH;->a(Ljava/lang/reflect/Member;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v2, v5

    .line 804
    invoke-virtual {p0, v0, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0

    .line 808
    :cond_1
    invoke-virtual {p2}, Lbww;->a()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-static {p0, v0}, Lbvd;->a(Ljava/util/Formatter;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Formatter;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 752
    const/4 v0, 0x0

    .line 753
    instance-of v1, p1, Lbwv;

    if-eqz v1, :cond_0

    .line 754
    check-cast p1, Lbwv;

    .line 755
    invoke-virtual {p1}, Lbwv;->a()Ljava/lang/Object;

    move-result-object v0

    .line 757
    :goto_0
    invoke-static {p0, v0, p1}, Lbvd;->a(Ljava/util/Formatter;Ljava/lang/Object;Lbwv;)V

    .line 758
    return-void

    :cond_0
    move-object v2, v0

    move-object v0, p1

    move-object p1, v2

    goto :goto_0
.end method

.method public static a(Ljava/util/Formatter;Ljava/lang/Object;Lbwv;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 761
    invoke-static {p2}, Lbvd;->a(Lbwv;)Ljava/lang/String;

    move-result-object v0

    .line 762
    instance-of v1, p1, Lbwt;

    if-eqz v1, :cond_1

    .line 763
    check-cast p1, Lbwt;

    .line 764
    invoke-virtual {p1}, Lbwt;->a()Lbww;

    move-result-object v0

    .line 765
    if-eqz v0, :cond_0

    .line 766
    invoke-static {p0, p1, v0, p2}, Lbvd;->a(Ljava/util/Formatter;Lbwt;Lbww;Lbwv;)V

    .line 790
    :goto_0
    return-void

    .line 768
    :cond_0
    invoke-virtual {p1}, Lbwt;->a()Lbuv;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lbvd;->a(Ljava/util/Formatter;Ljava/lang/Object;Lbwv;)V

    goto :goto_0

    .line 771
    :cond_1
    instance-of v1, p1, Lbww;

    if-eqz v1, :cond_2

    .line 772
    const/4 v0, 0x0

    check-cast p1, Lbww;

    invoke-static {p0, v0, p1, p2}, Lbvd;->a(Ljava/util/Formatter;Lbwt;Lbww;Lbwv;)V

    goto :goto_0

    .line 774
    :cond_2
    instance-of v1, p1, Ljava/lang/Class;

    if-eqz v1, :cond_3

    .line 775
    const-string v1, "  at %s%s%n"

    new-array v2, v2, [Ljava/lang/Object;

    check-cast p1, Ljava/lang/Class;

    invoke-static {p1}, LbvH;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0

    .line 777
    :cond_3
    instance-of v1, p1, Ljava/lang/reflect/Member;

    if-eqz v1, :cond_4

    .line 778
    const-string v1, "  at %s%s%n"

    new-array v2, v2, [Ljava/lang/Object;

    check-cast p1, Ljava/lang/reflect/Member;

    invoke-static {p1}, LbvH;->a(Ljava/lang/reflect/Member;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0

    .line 780
    :cond_4
    instance-of v1, p1, LbuP;

    if-eqz v1, :cond_5

    .line 781
    const-string v1, "  while locating %s%s%n"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0

    .line 783
    :cond_5
    instance-of v1, p1, Lbuv;

    if-eqz v1, :cond_6

    .line 784
    check-cast p1, Lbuv;

    .line 785
    const-string v0, "  while locating %s%n"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lbvd;->a(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0

    .line 788
    :cond_6
    const-string v1, "  at %s%s%n"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    goto :goto_0
.end method

.method private static b(Ljava/lang/Object;Lbwv;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 711
    invoke-static {p1}, Lbvd;->a(Lbwv;)Ljava/lang/String;

    move-result-object v0

    .line 712
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 715
    :goto_0
    return-object p0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public a(LbuP;)Lbvd;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuP",
            "<*>;)",
            "Lbvd;"
        }
    .end annotation

    .prologue
    .line 394
    const-string v0, "%s cannot be used as a key; It is not fully specified."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Lbvd;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbwy;)Lbvd;
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lbvd;->a:Lbvd;

    iget-object v0, v0, Lbvd;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 544
    iget-object v0, p0, Lbvd;->a:Lbvd;

    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lbvd;->a:Ljava/util/List;

    .line 546
    :cond_0
    iget-object v0, p0, Lbvd;->a:Lbvd;

    iget-object v0, v0, Lbvd;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 547
    return-object p0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;)Lbvd;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbvd;"
        }
    .end annotation

    .prologue
    .line 316
    const-string v0, "More than one scope annotation was found: %s and %s."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Lbvd;

    move-result-object v0

    return-object v0
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)Lbvd;
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lbvd;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Lbvd;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/reflect/Member;Ljava/lang/Class;Ljava/lang/Class;)Lbvd;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Member;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbvd;"
        }
    .end annotation

    .prologue
    .line 290
    const-string v0, "%s has more than one annotation annotated with @BindingAnnotation: %s and %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-virtual {p0, v0, v1}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Lbvd;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 507
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 508
    :goto_0
    if-eqz p0, :cond_1

    .line 509
    iget-object v1, p0, Lbvd;->a:Ljava/lang/Object;

    sget-object v2, LbvG;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 510
    const/4 v1, 0x0

    iget-object v2, p0, Lbvd;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 508
    :cond_0
    iget-object p0, p0, Lbvd;->b:Lbvd;

    goto :goto_0

    .line 513
    :cond_1
    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbwy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    iget-object v0, p0, Lbvd;->a:Lbvd;

    iget-object v0, v0, Lbvd;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 559
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    .line 567
    :goto_0
    return-object v0

    .line 562
    :cond_0
    new-instance v0, Lbve;

    invoke-direct {v0, p0}, Lbve;-><init>(Lbvd;)V

    iget-object v1, p0, Lbvd;->a:Lbvd;

    iget-object v1, v1, Lbvd;->a:Ljava/util/List;

    .line 567
    invoke-virtual {v0, v1}, Lbve;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
