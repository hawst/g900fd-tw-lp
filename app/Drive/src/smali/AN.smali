.class final LAN;
.super LAL;
.source "DocListAdapterState.java"


# instance fields
.field final synthetic b:LAJ;


# direct methods
.method private constructor <init>(LAJ;)V
    .locals 1

    .prologue
    .line 272
    iput-object p1, p0, LAN;->b:LAJ;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LAL;-><init>(LAJ;LAK;)V

    return-void
.end method

.method synthetic constructor <init>(LAJ;LAK;)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0, p1}, LAN;-><init>(LAJ;)V

    return-void
.end method

.method private a(LBy;)V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p1, LBy;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 353
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-object v0, p1, LBy;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(LBy;Lcom/google/android/apps/docs/doclist/SelectionItem;ILaGv;)V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p1, LBy;->a:LDi;

    .line 395
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v1, p0, LAN;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LDg;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3, p4}, LDg;->a(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;ILaGv;)V

    .line 397
    return-void
.end method

.method private a(LBy;Z)V
    .locals 3

    .prologue
    .line 323
    invoke-virtual {p0}, LAN;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v1, p1, LBy;->f:Landroid/view/View;

    .line 328
    if-eqz v1, :cond_0

    .line 332
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 333
    if-eqz p2, :cond_2

    .line 334
    sget v2, LwZ;->list_entry_selected:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 336
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lxb;->state_selector_background:I

    :goto_1
    invoke-direct {p0, v1, v0}, LAN;->a(Landroid/view/View;I)V

    goto :goto_0

    :cond_3
    sget v0, Lxb;->doclist_state_selector_background:I

    goto :goto_1
.end method

.method private a(LaGA;)V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCv;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 316
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to bind view with a wrong cursor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 319
    :cond_0
    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LBf;

    move-result-object v0

    iget-object v1, p0, LAN;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LCv;

    move-result-object v1

    invoke-interface {v0, v1}, LBf;->a(LCv;)V

    .line 320
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 344
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 345
    invoke-static {p1, v0}, LUv;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 346
    return-void
.end method

.method private b(LBy;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LDM;

    move-result-object v0

    invoke-virtual {v0}, LDM;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCE;

    move-result-object v0

    iget-object v1, p1, LBy;->a:LCF;

    invoke-virtual {v0, v1}, LCE;->a(LCF;)V

    .line 359
    :cond_0
    return-void
.end method

.method private b(LBy;Z)V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p1, LBy;->a:LEd;

    .line 368
    invoke-virtual {v0}, LEd;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-virtual {v0}, LEd;->a()Lcom/google/android/apps/docs/view/FixedSizeImageView;

    move-result-object v0

    .line 372
    invoke-virtual {p0, v0}, LAN;->a(Lcom/google/android/apps/docs/view/FixedSizeImageView;)V

    .line 374
    invoke-virtual {p0}, LAN;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    if-eqz p2, :cond_2

    .line 377
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedSizeImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 379
    :cond_2
    sget v1, Lxb;->doc_icon_state_selector_background:I

    invoke-direct {p0, v0, v1}, LAN;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private c(LBy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 362
    invoke-virtual {p0}, LAN;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LDM;

    move-result-object v0

    invoke-virtual {v0}, LDM;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 363
    :goto_0
    iget-object v2, p1, LBy;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364
    return-void

    :cond_2
    move v0, v1

    .line 362
    goto :goto_0
.end method

.method private d(LBy;)V
    .locals 4

    .prologue
    .line 385
    const-string v0, "%s %s %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, LBy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 386
    invoke-virtual {v3}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, LBy;->a:LCF;

    .line 387
    invoke-virtual {v3}, LCF;->a()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p1, LBy;->a:LCF;

    .line 388
    invoke-virtual {v3}, LCF;->b()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    .line 386
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 389
    iget-object v1, p1, LBy;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 390
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;LaGA;Z)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 275
    iget-object v0, p0, LAN;->b:LAJ;

    invoke-static {v0, p1}, LAJ;->a(LAJ;Landroid/view/View;)LBe;

    move-result-object v0

    check-cast v0, LBy;

    .line 276
    iget-object v1, v0, LBy;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 279
    iget-object v4, v0, LBy;->a:LEd;

    invoke-virtual {v4}, LEd;->a()V

    .line 280
    invoke-virtual {v0}, LBy;->a()V

    .line 282
    iget-object v4, p0, LAN;->b:LAJ;

    invoke-static {v4}, LAJ;->a(LAJ;)LtK;

    move-result-object v4

    sget-object v5, Lry;->u:Lry;

    invoke-interface {v4, v5}, LtK;->a(LtJ;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 283
    iget-object v4, p0, LAN;->b:LAJ;

    invoke-virtual {v4}, LAJ;->a()Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v4

    .line 284
    if-eqz v4, :cond_0

    .line 285
    iget-object v5, v0, LBy;->a:LEd;

    invoke-virtual {v5, v4}, LEd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Z

    .line 289
    :cond_0
    invoke-interface {p2}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    .line 290
    invoke-interface {p2}, LaGA;->a()LaGv;

    move-result-object v5

    sget-object v6, LaGv;->a:LaGv;

    invoke-virtual {v5, v6}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 291
    invoke-virtual {p0}, LAN;->b()Z

    move-result v6

    if-nez v6, :cond_1

    sget v6, LwY;->is_twocolumn:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 293
    :goto_0
    invoke-direct {p0, p2}, LAN;->a(LaGA;)V

    .line 295
    invoke-direct {p0, v0, p3}, LAN;->a(LBy;Z)V

    .line 296
    iget-object v6, p0, LAN;->b:LAJ;

    invoke-static {v6}, LAJ;->a(LAJ;)LCE;

    move-result-object v6

    iget-object v7, v0, LBy;->a:LCF;

    invoke-virtual {v6, v7, v4, v1}, LCE;->a(LCF;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 297
    invoke-direct {p0, v0, p3}, LAN;->b(LBy;Z)V

    .line 298
    invoke-direct {p0, v0}, LAN;->a(LBy;)V

    .line 300
    iget-object v1, p0, LAN;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LDM;

    move-result-object v1

    iget-object v6, v0, LBy;->a:LDT;

    invoke-virtual {v1, v6, v4}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 301
    invoke-direct {p0, v0}, LAN;->b(LBy;)V

    .line 303
    invoke-direct {p0, v0}, LAN;->c(LBy;)V

    .line 304
    invoke-direct {p0, v0}, LAN;->d(LBy;)V

    .line 306
    iget-object v1, p0, LAN;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v3

    .line 308
    :goto_1
    new-instance v2, Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-direct {v2, v4, v5, v1}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 309
    invoke-interface {p2}, LaGA;->b()I

    move-result v1

    invoke-interface {p2}, LaGA;->a()LaGv;

    move-result-object v3

    .line 308
    invoke-direct {p0, v0, v2, v1, v3}, LAN;->a(LBy;Lcom/google/android/apps/docs/doclist/SelectionItem;ILaGv;)V

    .line 311
    invoke-virtual {v0}, LBy;->b()V

    .line 312
    return-void

    .line 291
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 306
    :cond_2
    new-instance v1, Lcom/google/android/apps/docs/doclist/SelectionItem;

    iget-object v6, p0, LAN;->b:LAJ;

    .line 307
    invoke-static {v6}, LAJ;->a(LAJ;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-direct {v1, v6, v2, v3}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    goto :goto_1
.end method
