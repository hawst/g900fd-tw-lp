.class public final LNb;
.super Ljava/lang/Object;
.source "ItemToUpload.java"


# instance fields
.field private final a:LMZ;

.field private a:LalY;

.field private a:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(LMZ;)V
    .locals 4

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v0, LMZ;

    .line 151
    invoke-static {p1}, LMZ;->a(LMZ;)Lalo;

    move-result-object v1

    invoke-static {p1}, LMZ;->a(LMZ;)LalY;

    move-result-object v2

    invoke-static {p1}, LMZ;->a(LMZ;)Laeb;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LMZ;-><init>(Lalo;LalY;Laeb;)V

    iput-object v0, p0, LNb;->a:LMZ;

    .line 152
    invoke-static {p1}, LMZ;->b(LMZ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LNb;->a(Ljava/lang/String;)LNb;

    move-result-object v0

    .line 153
    invoke-static {p1}, LMZ;->a(LMZ;)LalU;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->a(LalU;)LNb;

    move-result-object v0

    .line 154
    invoke-virtual {p1}, LMZ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    move-result-object v0

    .line 155
    invoke-static {p1}, LMZ;->a(LMZ;)LNd;

    move-result-object v1

    invoke-direct {v0, v1}, LNb;->a(LNd;)LNb;

    move-result-object v0

    .line 156
    invoke-static {p1}, LMZ;->a(LMZ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->d(Ljava/lang/String;)LNb;

    move-result-object v0

    .line 157
    invoke-static {p1}, LMZ;->a(LMZ;)LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->a(LaFO;)LNb;

    move-result-object v0

    .line 158
    invoke-static {p1}, LMZ;->b(LMZ;)Z

    move-result v1

    invoke-virtual {v0, v1}, LNb;->a(Z)LNb;

    move-result-object v0

    .line 159
    invoke-static {p1}, LMZ;->a(LMZ;)Z

    move-result v1

    invoke-virtual {v0, v1}, LNb;->b(Z)LNb;

    move-result-object v0

    .line 160
    invoke-static {p1}, LMZ;->a(LMZ;)I

    move-result v1

    invoke-virtual {v0, v1}, LNb;->a(I)LNb;

    move-result-object v0

    .line 161
    invoke-static {p1}, LMZ;->a(LMZ;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LNb;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    .line 162
    return-void
.end method

.method constructor <init>(Landroid/content/ContentResolver;Lalo;LalY;Laeb;)V
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    new-instance v0, LMZ;

    invoke-direct {v0, p2, p3, p4}, LMZ;-><init>(Lalo;LalY;Laeb;)V

    iput-object v0, p0, LNb;->a:LMZ;

    .line 144
    iput-object p1, p0, LNb;->a:Landroid/content/ContentResolver;

    .line 145
    iput-object p3, p0, LNb;->a:LalY;

    .line 146
    return-void
.end method

.method private a(LNd;)LNb;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;LNd;)LNd;

    .line 278
    return-object p0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LalU;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LNd;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 210
    :try_start_0
    iget-object v0, p0, LNb;->a:LMZ;

    iget-object v3, p0, LNb;->a:LMZ;

    invoke-static {v3}, LMZ;->a(LMZ;)LNd;

    move-result-object v3

    invoke-interface {v3}, LNd;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v0, v3}, LMZ;->a(LMZ;Ljava/io/InputStream;)Ljava/io/InputStream;
    :try_end_0
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    const-string v3, "ItemToUpload.Builder"

    const-string v4, "Open dataSource failed: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    move v0, v2

    .line 208
    goto :goto_1
.end method


# virtual methods
.method public a()LMZ;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LalU;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LNd;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 272
    invoke-direct {p0}, LNb;->a()V

    .line 273
    iget-object v0, p0, LNb;->a:LMZ;

    return-object v0

    .line 271
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()LNb;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, LNb;->a:LMZ;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LMZ;->a(LMZ;Z)Z

    .line 195
    return-object p0
.end method

.method public a(I)LNb;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;I)I

    .line 262
    return-object p0
.end method

.method public a(LaFO;)LNb;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;LaFO;)LaFO;

    .line 237
    return-object p0
.end method

.method public a(LalU;)LNb;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;LalU;)LalU;

    .line 178
    return-object p0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)LNb;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LalU;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 222
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LNd;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 223
    iget-object v0, p0, LNb;->a:LMZ;

    new-instance v1, LNk;

    iget-object v2, p0, LNb;->a:Landroid/content/ContentResolver;

    iget-object v3, p0, LNb;->a:LalY;

    invoke-direct {v1, p1, v2, v3}, LNk;-><init>(Landroid/net/Uri;Landroid/content/ContentResolver;LalY;)V

    invoke-static {v0, v1}, LMZ;->a(LMZ;LNd;)LNd;

    .line 225
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p2}, LMZ;->b(LMZ;Ljava/lang/String;)Ljava/lang/String;

    .line 226
    return-object p0

    :cond_0
    move v0, v2

    .line 221
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222
    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-virtual {v0, p1}, LMZ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 166
    return-object p0
.end method

.method public a(Ljava/lang/String;)LNb;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;Ljava/lang/String;)Ljava/lang/String;

    .line 172
    return-object p0
.end method

.method public a(Z)LNb;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->b(LMZ;Z)Z

    .line 245
    return-object p0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->a(LMZ;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 267
    return-object p0
.end method

.method public b(Ljava/lang/String;)LNb;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LalU;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 183
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0}, LMZ;->a(LMZ;)LNd;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 184
    iget-object v0, p0, LNb;->a:LMZ;

    new-instance v1, LNg;

    invoke-direct {v1, p1}, LNg;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, LMZ;->a(LMZ;LNd;)LNd;

    .line 185
    iget-object v0, p0, LNb;->a:LMZ;

    const-string v1, "text/plain"

    invoke-static {v0, v1}, LMZ;->b(LMZ;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    return-object p0

    :cond_0
    move v0, v2

    .line 182
    goto :goto_0

    :cond_1
    move v1, v2

    .line 183
    goto :goto_1
.end method

.method public b(Z)LNb;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->c(LMZ;Z)Z

    .line 253
    return-object p0
.end method

.method public c(Ljava/lang/String;)LNb;
    .locals 1

    .prologue
    .line 190
    const-string v0, ""

    invoke-virtual {p0, v0}, LNb;->b(Ljava/lang/String;)LNb;

    move-result-object v0

    invoke-virtual {v0, p1}, LNb;->d(Ljava/lang/String;)LNb;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)LNb;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, LNb;->a:LMZ;

    invoke-static {v0, p1}, LMZ;->b(LMZ;Ljava/lang/String;)Ljava/lang/String;

    .line 231
    return-object p0
.end method
