.class LyQ;
.super Ljava/lang/Object;
.source "ActivityEntryAdapter.java"


# instance fields
.field public a:Landroid/view/View;

.field public a:Landroid/widget/ImageView;

.field public a:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)LyQ;
    .locals 2

    .prologue
    .line 161
    new-instance v1, LyQ;

    invoke-direct {v1}, LyQ;-><init>()V

    .line 162
    sget v0, Lxc;->recent_event_target:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LyQ;->a:Landroid/view/View;

    .line 163
    sget v0, Lxc;->recent_event_target_icon:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LyQ;->a:Landroid/widget/ImageView;

    .line 164
    sget v0, Lxc;->recent_event_target_title:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LyQ;->a:Landroid/widget/TextView;

    .line 165
    return-object v1
.end method
