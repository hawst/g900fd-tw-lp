.class public final enum LaEd;
.super Ljava/lang/Enum;
.source "AccountMetadataTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEd;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEd;

.field private static final synthetic a:[LaEd;

.field public static final enum b:LaEd;

.field public static final enum c:LaEd;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xe

    .line 46
    new-instance v0, LaEd;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LaEc;->b()LaEc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 48
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 47
    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaEd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEd;->a:LaEd;

    .line 50
    new-instance v0, LaEd;

    const-string v1, "CAPABILITY_CONTENT"

    invoke-static {}, LaEc;->b()LaEc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "capabilityContent"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 51
    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEd;->b:LaEd;

    .line 53
    new-instance v0, LaEd;

    const-string v1, "LAST_UPDATED_TIME"

    invoke-static {}, LaEc;->b()LaEc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "lastUpdatedDate"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEd;->c:LaEd;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [LaEd;

    sget-object v1, LaEd;->a:LaEd;

    aput-object v1, v0, v7

    sget-object v1, LaEd;->b:LaEd;

    aput-object v1, v0, v8

    sget-object v1, LaEd;->c:LaEd;

    aput-object v1, v0, v9

    sput-object v0, LaEd;->a:[LaEd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEd;->a:LaFr;

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEd;
    .locals 1

    .prologue
    .line 45
    const-class v0, LaEd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEd;

    return-object v0
.end method

.method public static values()[LaEd;
    .locals 1

    .prologue
    .line 45
    sget-object v0, LaEd;->a:[LaEd;

    invoke-virtual {v0}, [LaEd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEd;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LaEd;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LaEd;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
