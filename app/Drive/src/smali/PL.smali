.class public abstract enum LPL;
.super Ljava/lang/Enum;
.source "EventType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPL;

.field private static final synthetic a:[LPL;

.field public static final enum b:LPL;

.field public static final enum c:LPL;

.field public static final enum d:LPL;

.field public static final enum e:LPL;

.field public static final enum f:LPL;

.field public static final enum g:LPL;

.field public static final enum h:LPL;

.field public static final enum i:LPL;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 17
    new-instance v0, LPM;

    const-string v1, "PERMISSION_CHANGE"

    const-string v2, "permissionChange"

    invoke-direct {v0, v1, v4, v2}, LPM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->a:LPL;

    .line 27
    new-instance v0, LPN;

    const-string v1, "EDIT"

    const-string v2, "edit"

    invoke-direct {v0, v1, v5, v2}, LPN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->b:LPL;

    .line 37
    new-instance v0, LPO;

    const-string v1, "RENAME"

    const-string v2, "rename"

    invoke-direct {v0, v1, v6, v2}, LPO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->c:LPL;

    .line 43
    new-instance v0, LPP;

    const-string v1, "MOVE"

    const-string v2, "move"

    invoke-direct {v0, v1, v7, v2}, LPP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->d:LPL;

    .line 49
    new-instance v0, LPQ;

    const-string v1, "UPLOAD"

    const-string v2, "upload"

    invoke-direct {v0, v1, v8, v2}, LPQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->e:LPL;

    .line 59
    new-instance v0, LPR;

    const-string v1, "TRASH"

    const/4 v2, 0x5

    const-string v3, "trash"

    invoke-direct {v0, v1, v2, v3}, LPR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->f:LPL;

    .line 69
    new-instance v0, LPS;

    const-string v1, "CREATE"

    const/4 v2, 0x6

    const-string v3, "create"

    invoke-direct {v0, v1, v2, v3}, LPS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->g:LPL;

    .line 79
    new-instance v0, LPT;

    const-string v1, "COMMENT"

    const/4 v2, 0x7

    const-string v3, "comment"

    invoke-direct {v0, v1, v2, v3}, LPT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->h:LPL;

    .line 89
    new-instance v0, LPU;

    const-string v1, "EMPTYTRASH"

    const/16 v2, 0x8

    const-string v3, "emptyTrash"

    invoke-direct {v0, v1, v2, v3}, LPU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LPL;->i:LPL;

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [LPL;

    sget-object v1, LPL;->a:LPL;

    aput-object v1, v0, v4

    sget-object v1, LPL;->b:LPL;

    aput-object v1, v0, v5

    sget-object v1, LPL;->c:LPL;

    aput-object v1, v0, v6

    sget-object v1, LPL;->d:LPL;

    aput-object v1, v0, v7

    sget-object v1, LPL;->e:LPL;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LPL;->f:LPL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LPL;->g:LPL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LPL;->h:LPL;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LPL;->i:LPL;

    aput-object v2, v0, v1

    sput-object v0, LPL;->a:[LPL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 106
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LPL;->a:Ljava/lang/String;

    .line 107
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;LPM;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, LPL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LPL;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-static {}, LPL;->values()[LPL;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 113
    iget-object v5, v0, LPL;->a:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 118
    :goto_1
    return-object v0

    .line 112
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117
    :cond_1
    const-string v0, "EventType"

    const-string v1, "Event type \"%s\" is unsupported"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    invoke-static {v0, v1, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 118
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LPL;
    .locals 1

    .prologue
    .line 16
    const-class v0, LPL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPL;

    return-object v0
.end method

.method public static values()[LPL;
    .locals 1

    .prologue
    .line 16
    sget-object v0, LPL;->a:[LPL;

    invoke-virtual {v0}, [LPL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPL;

    return-object v0
.end method


# virtual methods
.method public abstract a(LPD;LaGu;)LPK;
.end method
