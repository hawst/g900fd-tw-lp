.class public LMZ;
.super Ljava/lang/Object;
.source "ItemToUpload.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private a:I

.field private a:LNd;

.field private a:LaFO;

.field private a:Ladj;

.field private final a:Laeb;

.field private a:LalU;

.field private final a:LalY;

.field private final a:Lalo;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Ljava/io/InputStream;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>(Lalo;LalY;Laeb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-boolean v1, p0, LMZ;->c:Z

    .line 98
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalo;

    iput-object v0, p0, LMZ;->a:Lalo;

    .line 99
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalY;

    iput-object v0, p0, LMZ;->a:LalY;

    .line 100
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeb;

    iput-object v0, p0, LMZ;->a:Laeb;

    .line 101
    iput v1, p0, LMZ;->a:I

    .line 102
    return-void
.end method

.method static synthetic a(LMZ;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, LMZ;->a:I

    return v0
.end method

.method static synthetic a(LMZ;I)I
    .locals 0

    .prologue
    .line 42
    iput p1, p0, LMZ;->a:I

    return p1
.end method

.method static synthetic a(LMZ;)LNd;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:LNd;

    return-object v0
.end method

.method static synthetic a(LMZ;LNd;)LNd;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->a:LNd;

    return-object p1
.end method

.method static synthetic a(LMZ;)LaFO;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(LMZ;LaFO;)LaFO;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->a:LaFO;

    return-object p1
.end method

.method static a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ladi;Lalo;)Ladj;
    .locals 2

    .prologue
    .line 533
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    :try_start_0
    invoke-interface {p3, p1, p2}, Ladi;->a(Ljava/lang/String;Ljava/lang/String;)Ladj;

    move-result-object v0

    .line 538
    invoke-interface {v0}, Ladj;->a()Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 540
    :try_start_1
    invoke-interface {p4, p0, v1}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 544
    const/4 v1, 0x0

    .line 545
    invoke-interface {v0}, Ladj;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547
    if-eqz v1, :cond_0

    .line 548
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    return-object v0

    .line 547
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 548
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 554
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method static synthetic a(LMZ;)Laeb;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:Laeb;

    return-object v0
.end method

.method static synthetic a(LMZ;)LalU;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:LalU;

    return-object v0
.end method

.method static synthetic a(LMZ;LalU;)LalU;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->a:LalU;

    return-object p1
.end method

.method static synthetic a(LMZ;)LalY;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:LalY;

    return-object v0
.end method

.method static synthetic a(LMZ;)Lalo;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:Lalo;

    return-object v0
.end method

.method static synthetic a(LMZ;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method static synthetic a(LMZ;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object p1
.end method

.method static synthetic a(LMZ;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->a:Ljava/io/InputStream;

    return-object p1
.end method

.method static synthetic a(LMZ;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LMZ;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(LMZ;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, LMZ;->b:Z

    return v0
.end method

.method static synthetic a(LMZ;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, LMZ;->d:Z

    return p1
.end method

.method static synthetic b(LMZ;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LMZ;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(LMZ;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, LMZ;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(LMZ;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, LMZ;->a:Z

    return v0
.end method

.method static synthetic b(LMZ;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, LMZ;->a:Z

    return p1
.end method

.method static synthetic c(LMZ;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, LMZ;->b:Z

    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, LMZ;->a:LNd;

    invoke-interface {v0}, LNd;->a()I

    move-result v0

    return v0
.end method

.method public a()LMZ;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 407
    invoke-virtual {p0}, LMZ;->a()LalU;

    move-result-object v0

    if-nez v0, :cond_1

    .line 408
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 409
    new-instance v0, LNh;

    const-string v1, "Data source not opened"

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :cond_0
    :try_start_0
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    invoke-virtual {p0, v0}, LMZ;->a(Ljava/io/InputStream;)LalU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 416
    :try_start_1
    iget-object v1, p0, LMZ;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 420
    :goto_0
    iput-object v2, p0, LMZ;->a:Ljava/io/InputStream;

    .line 422
    new-instance v1, LNb;

    invoke-direct {v1, p0}, LNb;-><init>(LMZ;)V

    .line 423
    invoke-virtual {v1, v0}, LNb;->a(LalU;)LNb;

    move-result-object v0

    const/4 v1, 0x1

    .line 425
    invoke-virtual {v0, v1}, LNb;->b(Z)LNb;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, LNb;->a()LMZ;

    move-result-object v0

    .line 427
    invoke-virtual {p0}, LMZ;->close()V

    move-object p0, v0

    .line 430
    :cond_1
    return-object p0

    .line 415
    :catchall_0
    move-exception v0

    .line 416
    :try_start_2
    iget-object v1, p0, LMZ;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 420
    :goto_1
    iput-object v2, p0, LMZ;->a:Ljava/io/InputStream;

    throw v0

    .line 417
    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, LMZ;->a:LaFO;

    return-object v0
.end method

.method public a(Ladi;)Ladj;
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, LMZ;->a:Ladj;

    if-nez v0, :cond_0

    .line 474
    invoke-virtual {p0, p1}, LMZ;->a(Ladi;)V

    .line 477
    :cond_0
    iget-object v0, p0, LMZ;->a:Ladj;

    .line 478
    const/4 v1, 0x0

    iput-object v1, p0, LMZ;->a:Ladj;

    .line 479
    return-object v0
.end method

.method public a()LalU;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, LMZ;->a:LalU;

    return-object v0
.end method

.method public a(Ljava/io/InputStream;)LalU;
    .locals 4

    .prologue
    .line 442
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    :try_start_0
    new-instance v0, LalU;

    iget-object v1, p0, LMZ;->a:Laeb;

    invoke-interface {v1}, Laeb;->a()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, LalU;-><init>(Ljava/io/File;)V

    .line 448
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {v0}, LalU;->a()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 449
    new-instance v2, LMQ;

    new-instance v3, LNa;

    invoke-direct {v3, p0}, LNa;-><init>(LMZ;)V

    invoke-direct {v2, p1, v3}, LMQ;-><init>(Ljava/io/InputStream;LMR;)V

    .line 457
    iget-object v3, p0, LMZ;->a:Lalo;

    invoke-interface {v3, v2, v1}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catch LMS; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 464
    return-object v0

    .line 458
    :catch_0
    move-exception v0

    .line 459
    throw v0

    .line 460
    :catch_1
    move-exception v0

    .line 461
    new-instance v1, LNh;

    const-string v2, "Error while creating temp file for uploading."

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, LMZ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, LMZ;->a:LalU;

    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x0

    .line 299
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LMZ;->a:LalU;

    invoke-virtual {v0}, LalU;->a()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, LMZ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ladi;)V
    .locals 4

    .prologue
    .line 488
    iget-object v0, p0, LMZ;->a:LalU;

    if-nez v0, :cond_4

    .line 489
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 490
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Data source is not open"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_0
    iget-object v0, p0, LMZ;->a:LNd;

    invoke-interface {v0}, LNd;->a()Ljava/io/File;

    move-result-object v1

    .line 493
    if-eqz v1, :cond_3

    .line 494
    iget-boolean v0, p0, LMZ;->d:Z

    if-eqz v0, :cond_2

    .line 495
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :goto_0
    move-object v1, v0

    .line 509
    :goto_1
    :try_start_0
    invoke-virtual {p0}, LMZ;->b()Ljava/lang/String;

    move-result-object v0

    .line 510
    invoke-virtual {p0}, LMZ;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LMZ;->a:Lalo;

    .line 509
    invoke-static {v1, v0, v2, p1, v3}, LMZ;->a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ladi;Lalo;)Ladj;

    move-result-object v0

    iput-object v0, p0, LMZ;->a:Ladj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 512
    iget-object v0, p0, LMZ;->a:LalU;

    if-eqz v0, :cond_1

    .line 513
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 516
    :cond_1
    :goto_2
    return-void

    .line 498
    :cond_2
    invoke-virtual {p0}, LMZ;->b()Ljava/lang/String;

    move-result-object v0

    .line 497
    invoke-interface {p1, v0, v1}, Ladi;->a(Ljava/lang/String;Ljava/io/File;)Ladj;

    move-result-object v0

    iput-object v0, p0, LMZ;->a:Ladj;

    goto :goto_2

    .line 502
    :cond_3
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    goto :goto_0

    .line 505
    :cond_4
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, LMZ;->a:LalU;

    invoke-virtual {v1}, LalU;->a()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v1, v0

    goto :goto_1

    .line 512
    :catchall_0
    move-exception v0

    iget-object v2, p0, LMZ;->a:LalU;

    if-eqz v2, :cond_5

    .line 513
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_5
    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, LMZ;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 284
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, LMZ;->c:Ljava/lang/String;

    .line 359
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, LMZ;->a:Z

    return v0
.end method

.method public b()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, LMZ;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, LMZ;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized b()Z
    .locals 1

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LMZ;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, LMZ;->c:Ljava/lang/String;

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, LMZ;->a:LalU;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LMZ;->b:Z

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, LMZ;->a:LalU;

    invoke-virtual {v0}, LalU;->a()Z

    .line 382
    :cond_0
    iget-object v0, p0, LMZ;->a:Ladj;

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, LMZ;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 386
    :cond_1
    :try_start_0
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :cond_2
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LMZ;->a:Ljava/io/InputStream;

    .line 393
    return-void

    .line 389
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload Item"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " TITLE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LMZ;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " MIME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LMZ;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ACCOUNTNAME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LMZ;->a:LaFO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " CONVERT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LMZ;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " DELETEAFTERUPLOAD="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LMZ;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ORIENTATION="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LMZ;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
