.class public LPy;
.super Ljava/lang/Object;
.source "ActivityApi.java"


# instance fields
.field private final a:LPC;

.field private final a:LQr;

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:LbsW;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;LPC;LQr;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    invoke-static {v0}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, LPy;->a:LbsW;

    .line 38
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LPy;->a:Ljava/util/List;

    .line 39
    iget-object v0, p0, LPy;->a:Ljava/util/List;

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, LPy;->a:LbsU;

    .line 45
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPC;

    iput-object v0, p0, LPy;->a:LPC;

    .line 46
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iput-object p4, p0, LPy;->a:LQr;

    .line 52
    iget-object v0, p0, LPy;->a:LbsW;

    new-instance v1, LPz;

    invoke-direct {v1, p0, p2, p1}, LPz;-><init>(LPy;Lcom/google/android/gms/drive/database/data/EntrySpec;LaGM;)V

    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    iput-object v0, p0, LPy;->a:Ljava/util/concurrent/Future;

    .line 64
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LPy;->a(I)LbsU;

    .line 65
    return-void
.end method

.method static synthetic a(LPy;)LPC;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LPy;->a:LPC;

    return-object v0
.end method

.method private declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LPy;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(LPy;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, LPy;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LPy;Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;)Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, LPy;->a(Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->getNextPageToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LPy;->a:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->getActivities()Ljava/util/List;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_1

    .line 80
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    iget-object v2, p0, LPy;->a:Ljava/util/List;

    invoke-virtual {v1, v2}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v1

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Activity;

    .line 82
    new-instance v3, LPD;

    invoke-direct {v3, v0}, LPD;-><init>(Lcom/google/api/services/appsactivity/model/Activity;)V

    invoke-virtual {v1, v3}, LbmH;->a(Ljava/lang/Object;)LbmH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :cond_0
    :try_start_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LPy;->a:Ljava/util/List;

    .line 87
    :cond_1
    iget-object v0, p0, LPy;->a:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method static synthetic a(LPy;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LPy;->a:Ljava/util/concurrent/Future;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, LPy;->a:Ljava/util/List;

    iget-object v1, p0, LPy;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPD;

    .line 162
    invoke-virtual {v0}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getTimestamp()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized a(I)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LbsU",
            "<",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LPy;->a:LbsU;

    invoke-interface {v0}, LbsU;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, LPy;->a:LbsU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :goto_0
    monitor-exit p0

    return-object v0

    .line 126
    :cond_0
    :try_start_1
    iget-object v0, p0, LPy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_1

    .line 128
    iget-object v0, p0, LPy;->a:LbsU;

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p0}, LPy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, LPy;->a:LbsU;

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, LPy;->a:LbsW;

    new-instance v1, LPA;

    invoke-direct {v1, p0}, LPA;-><init>(LPy;)V

    .line 137
    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 146
    iput-object v0, p0, LPy;->a:LbsU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    iget-object v0, p0, LPy;->a:LQr;

    const-string v3, "recentActivityResultsLimit"

    const/16 v4, 0x2710

    invoke-interface {v0, v3, v4}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 153
    iget-object v3, p0, LPy;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 154
    :goto_0
    iget-object v3, p0, LPy;->a:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, LPy;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 157
    :goto_1
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 153
    goto :goto_0

    :cond_3
    move v3, v2

    .line 154
    goto :goto_1
.end method
