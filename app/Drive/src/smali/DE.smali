.class public final LDE;
.super Ljava/lang/Object;
.source "SortedMergeAdapterIndexer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LDC;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:[I

.field private final a:[[I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LDE;->a:Ljava/util/List;

    .line 36
    invoke-static {p1}, LDE;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, LDE;->a:I

    .line 37
    iget v0, p0, LDE;->a:I

    invoke-static {v0}, LbnG;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LDE;->b:Ljava/util/List;

    .line 38
    iget v0, p0, LDE;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, LDE;->a:[I

    .line 39
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [[I

    iput-object v0, p0, LDE;->a:[[I

    .line 41
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 42
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    .line 43
    iget-object v2, p0, LDE;->a:[[I

    invoke-interface {v0}, LDC;->getCount()I

    move-result v0

    new-array v0, v0, [I

    aput-object v0, v2, v1

    .line 41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45
    :cond_0
    invoke-direct {p0}, LDE;->a()V

    .line 46
    return-void
.end method

.method private static a(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LDC;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    .line 88
    invoke-interface {v0}, LDC;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 89
    goto :goto_0

    .line 90
    :cond_0
    return v1
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 50
    iget-object v0, p0, LDE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 51
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 54
    iget-object v0, p0, LDE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    .line 55
    invoke-interface {v0}, LDC;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 56
    invoke-interface {v0, v1}, LDC;->a(I)LIL;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58
    :cond_0
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :cond_1
    :goto_1
    iget v0, p0, LDE;->a:I

    if-ge v1, v0, :cond_3

    .line 64
    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v0

    invoke-virtual {v0}, Lbpw;->c()Lbpw;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbpw;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIL;

    .line 65
    invoke-interface {v3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 66
    aget v5, v2, v4

    .line 67
    iget-object v0, p0, LDE;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    .line 69
    iget-object v6, p0, LDE;->b:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v6, p0, LDE;->a:[I

    aput v5, v6, v1

    .line 71
    iget-object v6, p0, LDE;->a:[[I

    aget-object v6, v6, v4

    aput v1, v6, v5

    .line 74
    add-int/lit8 v5, v5, 0x1

    .line 75
    aput v5, v2, v4

    .line 77
    invoke-interface {v0}, LDC;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 78
    invoke-interface {v0, v5}, LDC;->a(I)LIL;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 63
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 80
    :cond_2
    invoke-interface {v3, v4, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 83
    :cond_3
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, LDE;->a:I

    return v0
.end method

.method a(I)I
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, LDE;->a:Ljava/util/List;

    invoke-virtual {p0, p1}, LDE;->a(I)LDC;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method a(II)I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, LDE;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    invoke-virtual {p0, v0, p2}, LDE;->a(LDC;I)I

    move-result v0

    return v0
.end method

.method public a(LDC;I)I
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, LDE;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    .line 107
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "Position %s belong to %s adapter and not the given adapter %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 108
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    aput-object p1, v3, v0

    .line 107
    invoke-static {v1, v2, v3}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, LDE;->a:[I

    aget v0, v0, p2

    return v0
.end method

.method public a(I)LDC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, LDE;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDC;

    return-object v0
.end method

.method b(II)I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, LDE;->a:[[I

    aget-object v0, v0, p1

    aget v0, v0, p2

    return v0
.end method
