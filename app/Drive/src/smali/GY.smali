.class LGY;
.super Ljava/lang/Object;
.source "AbstractDriveAppOpenerOption.java"

# interfaces
.implements LDL;


# instance fields
.field final synthetic a:LFT;

.field final synthetic a:LGX;

.field final synthetic a:LaGo;

.field private a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(LGX;Ljava/lang/String;LFT;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaGo;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, LGY;->a:LGX;

    iput-object p2, p0, LGY;->a:Ljava/lang/String;

    iput-object p3, p0, LGY;->a:LFT;

    iput-object p4, p0, LGY;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object p5, p0, LGY;->a:LaGo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 68
    if-nez p1, :cond_1

    .line 69
    iget-object v1, p0, LGY;->a:LGX;

    iget-object v1, v1, LGX;->a:LGZ;

    iget-object v2, p0, LGY;->a:LFT;

    iget-object v3, p0, LGY;->a:LGX;

    iget-object v3, v3, LGX;->a:Ljava/lang/String;

    iget-object v4, p0, LGY;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v1, v2, v3, v4}, LGZ;->a(LFT;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v1

    .line 70
    if-nez v1, :cond_0

    .line 78
    :goto_0
    return v0

    .line 73
    :cond_0
    iget-object v0, p0, LGY;->a:LGX;

    iget-object v2, p0, LGY;->a:LaGo;

    invoke-virtual {v0, v2, v1}, LGX;->a(LaGo;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, LGY;->a:Landroid/content/Intent;

    .line 74
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    iget-object v1, p0, LGY;->a:Landroid/content/Intent;

    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v1, p0, LGY;->a:LFT;

    iget-object v2, p0, LGY;->a:Landroid/content/Intent;

    invoke-interface {v1, v2}, LFT;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, LGY;->a:LGX;

    iget-object v0, v0, LGX;->a:Landroid/content/Context;

    sget v1, Lxi;->opening_document:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LGY;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lamr;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method
