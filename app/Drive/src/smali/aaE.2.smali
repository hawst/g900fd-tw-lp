.class public LaaE;
.super Ljava/lang/Object;
.source "ExtraStreamDataSource.java"

# interfaces
.implements LaaC;


# instance fields
.field private final a:LaaK;

.field private final a:LaaP;

.field private final a:LaaR;

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LaaG;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, LaaG;->a(LaaG;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LaaE;->a:Landroid/net/Uri;

    .line 81
    invoke-static {p1}, LaaG;->a(LaaG;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaaE;->a:Ljava/lang/String;

    .line 82
    invoke-static {p1}, LaaG;->a(LaaG;)Z

    move-result v0

    iput-boolean v0, p0, LaaE;->a:Z

    .line 83
    invoke-static {p1}, LaaG;->b(LaaG;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaaE;->b:Ljava/lang/String;

    .line 84
    invoke-static {p1}, LaaG;->a(LaaG;)LaaR;

    move-result-object v0

    iput-object v0, p0, LaaE;->a:LaaR;

    .line 85
    invoke-static {p1}, LaaG;->a(LaaG;)LaaP;

    move-result-object v0

    iput-object v0, p0, LaaE;->a:LaaP;

    .line 86
    invoke-static {p1}, LaaG;->a(LaaG;)LaaK;

    move-result-object v0

    iput-object v0, p0, LaaE;->a:LaaK;

    .line 87
    return-void
.end method

.method synthetic constructor <init>(LaaG;LaaF;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, LaaE;-><init>(LaaG;)V

    return-void
.end method


# virtual methods
.method public a(LNb;)LMZ;
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, LaaE;->a:Landroid/net/Uri;

    invoke-virtual {p0}, LaaE;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LNb;->a(Landroid/net/Uri;Ljava/lang/String;)LNb;

    move-result-object v0

    iget-object v1, p0, LaaE;->a:LaaR;

    iget-object v2, p0, LaaE;->a:Landroid/net/Uri;

    .line 124
    invoke-virtual {p0}, LaaE;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LaaR;->a(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LNb;->a(I)LNb;

    .line 125
    invoke-virtual {p1}, LNb;->a()LMZ;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, LaaE;->a:LaaR;

    iget-object v1, p0, LaaE;->a:Landroid/net/Uri;

    invoke-virtual {p0}, LaaE;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LaaR;->a(Landroid/net/Uri;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LaaE;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 106
    iget-boolean v0, p0, LaaE;->a:Z

    if-nez v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, LaaE;->a:LaaP;

    iget-object v1, p0, LaaE;->a:Landroid/net/Uri;

    invoke-interface {v0, v1}, LaaP;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, LaaE;->a:LaaK;

    iget-object v1, p0, LaaE;->a:Landroid/net/Uri;

    iget-object v2, p0, LaaE;->b:Ljava/lang/String;

    iget-object v3, p0, LaaE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LaaK;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
