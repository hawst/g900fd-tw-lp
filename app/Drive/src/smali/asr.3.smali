.class Lasr;
.super Ljava/lang/Object;
.source "Page.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lasq;


# direct methods
.method constructor <init>(Lasq;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lasr;->a:Lasq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    const-string v1, "if (typeof(adjustLayout) == \"function\") {adjustLayout(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasr;->a:Lasq;

    iget-object v2, v2, Lasq;->a:Lcom/google/android/apps/docs/welcome/Page;

    .line 365
    invoke-static {v2}, Lcom/google/android/apps/docs/welcome/Page;->a(Lcom/google/android/apps/docs/welcome/Page;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\');}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    const-string v1, "if (typeof(i18nize) == \"function\") {i18nize(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasr;->a:Lasq;

    iget-object v2, v2, Lasq;->a:Lcom/google/android/apps/docs/welcome/Page;

    .line 367
    invoke-static {v2}, Lcom/google/android/apps/docs/welcome/Page;->b(Lcom/google/android/apps/docs/welcome/Page;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\');}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    const-string v1, "if (typeof(exportTextForTalkBack) == \"function\") {exportTextForTalkBack();}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    const-string v1, "if (typeof(exportButtonTexts) == \"function\") {exportButtonTexts();}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    const-string v1, "window.pageLoadListener.onPageLoaded();"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    iget-object v1, p0, Lasr;->a:Lasq;

    iget-object v1, v1, Lasq;->a:Lcom/google/android/apps/docs/welcome/Page;

    invoke-static {v1}, Lcom/google/android/apps/docs/welcome/Page;->a(Lcom/google/android/apps/docs/welcome/Page;)LUf;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LUf;->a(Ljava/lang/String;)V

    .line 374
    return-void
.end method
