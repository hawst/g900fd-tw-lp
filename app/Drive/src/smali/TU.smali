.class public LTU;
.super Ljava/lang/Object;
.source "DriveAppIntegrationManager.java"

# interfaces
.implements LTT;
.implements Laaa;


# annotations
.annotation runtime LbuO;
.end annotation


# instance fields
.field private final a:LTS;

.field private final a:LTZ;

.field private final a:Landroid/content/Context;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Lye;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LTY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;LTS;LTZ;Laab;LbuE;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LTS;",
            "LTZ;",
            "Laab;",
            "LbuE",
            "<",
            "Lye;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LTU;->a:Ljava/util/Map;

    .line 46
    iput-object p1, p0, LTU;->a:Landroid/content/Context;

    .line 47
    iput-object p2, p0, LTU;->a:LTS;

    .line 48
    iput-object p3, p0, LTU;->a:LTZ;

    .line 49
    iput-object p5, p0, LTU;->a:LbuE;

    .line 51
    invoke-virtual {p4, p0}, Laab;->a(Laaa;)V

    .line 52
    return-void
.end method

.method private a(Ljava/lang/String;)LTY;
    .locals 2

    .prologue
    .line 55
    invoke-static {p1}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    sget-object v0, LTY;->a:LTY;

    .line 69
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    iget-object v0, p0, LTU;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTY;

    .line 60
    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, LTU;->a:LTS;

    invoke-virtual {v0, p1}, LTS;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 62
    if-nez v0, :cond_2

    .line 63
    sget-object v0, LTY;->a:LTY;

    .line 66
    :cond_2
    iget-object v1, p0, LTU;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(LaGu;LTY;)Z
    .locals 1

    .prologue
    .line 128
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LTY;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaFO;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0, p2}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, LTY;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, LTU;->a:LTZ;

    invoke-virtual {v1, v0, p1, p2}, LTZ;->a(LTY;LaFO;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGu;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 115
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 116
    invoke-direct {p0, p1, v0}, LTU;->a(LaGu;LTY;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, LTU;->a:LTZ;

    invoke-virtual {v1, p1, v0, p2}, LTZ;->a(LaGu;LTY;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, LTU;->a:LTS;

    invoke-virtual {v0}, LTS;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, LTU;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 165
    return-void
.end method

.method public a(LaGu;)Z
    .locals 1

    .prologue
    .line 74
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-direct {p0, v0}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, LTY;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 133
    invoke-direct {p0, p1}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, LTY;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    :goto_0
    return v1

    .line 140
    :cond_0
    iget-object v0, p0, LTU;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    invoke-interface {v0, p1}, Lye;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LTU;->a:LbuE;

    .line 141
    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    invoke-interface {v0, p1}, Lye;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, LTU;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 172
    return-void
.end method

.method public b(LaGu;)Z
    .locals 3

    .prologue
    .line 87
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-direct {p0, v0}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, LTY;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.docs.statesyncer.DOCUMENT_PIN_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v0}, LTY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    iget-object v0, p0, LTU;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 94
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(LaGu;)Z
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, LTU;->d(LaGu;)Z

    move-result v0

    return v0
.end method

.method public d(LaGu;)Z
    .locals 1

    .prologue
    .line 108
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-direct {p0, v0}, LTU;->a(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 110
    invoke-direct {p0, p1, v0}, LTU;->a(LaGu;LTY;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
