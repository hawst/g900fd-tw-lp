.class abstract enum LuZ;
.super Ljava/lang/Enum;
.source "ProjectorSharingMenuManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LuZ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LuZ;

.field private static final synthetic a:[LuZ;

.field public static final enum b:LuZ;

.field public static final enum c:LuZ;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lva;

    const-string v1, "SHARE_LINK"

    sget v4, Lxi;->action_card_share_link:I

    sget v5, Lxb;->ic_link_alpha:I

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lva;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LuZ;->a:LuZ;

    .line 49
    new-instance v3, Lvb;

    const-string v4, "ADD_PEOPLE"

    sget v7, Lxi;->action_card_share:I

    sget v8, Lxb;->ic_addpeople_alpha:I

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lvb;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LuZ;->b:LuZ;

    .line 55
    new-instance v3, Lvc;

    const-string v4, "SEND_FILE"

    sget v7, Lxi;->action_card_export:I

    sget v8, Lxb;->ic_send_file_alpha:I

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lvc;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LuZ;->c:LuZ;

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [LuZ;

    sget-object v1, LuZ;->a:LuZ;

    aput-object v1, v0, v2

    sget-object v1, LuZ;->b:LuZ;

    aput-object v1, v0, v9

    sget-object v1, LuZ;->c:LuZ;

    aput-object v1, v0, v10

    sput-object v0, LuZ;->a:[LuZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, LuZ;->a:I

    .line 68
    iput p4, p0, LuZ;->b:I

    .line 69
    iput p5, p0, LuZ;->c:I

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIIILuY;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct/range {p0 .. p5}, LuZ;-><init>(Ljava/lang/String;IIII)V

    return-void
.end method

.method static a(I)LuZ;
    .locals 5

    .prologue
    .line 73
    invoke-static {}, LuZ;->values()[LuZ;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 74
    iget v4, v3, LuZ;->a:I

    if-ne p0, v4, :cond_0

    .line 75
    return-object v3

    .line 73
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected share action id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LuZ;
    .locals 1

    .prologue
    .line 42
    const-class v0, LuZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LuZ;

    return-object v0
.end method

.method public static values()[LuZ;
    .locals 1

    .prologue
    .line 42
    sget-object v0, LuZ;->a:[LuZ;

    invoke-virtual {v0}, [LuZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LuZ;

    return-object v0
.end method


# virtual methods
.method abstract a(LKU;)LLw;
.end method
