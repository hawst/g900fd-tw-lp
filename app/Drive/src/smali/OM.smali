.class LOM;
.super Ljava/lang/Object;
.source "PhotosPreferencesInstaller.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:LOJ;

.field final synthetic a:Landroid/preference/CheckBoxPreference;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, LOM;->a:LOJ;

    iput-object p2, p0, LOM;->a:Landroid/preference/CheckBoxPreference;

    iput-object p3, p0, LOM;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 216
    check-cast p2, Ljava/lang/Boolean;

    .line 217
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 219
    :goto_0
    if-nez v0, :cond_0

    .line 220
    iget-object v3, p0, LOM;->a:LOJ;

    iget-object v3, v3, LOJ;->a:Landroid/app/Activity;

    iget-object v4, p0, LOM;->a:LOJ;

    iget-object v4, v4, LOJ;->a:Landroid/app/Activity;

    sget v5, LOV;->prefs_auto_add_disabled_message:I

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 221
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 223
    :cond_0
    iget-object v3, p0, LOM;->a:LOJ;

    iget-object v3, v3, LOJ;->a:LqK;

    const-string v4, "preferences"

    if-eqz v0, :cond_2

    const-string v0, "settingsPhotoBackupAddToMyDriveEnable"

    :goto_1
    invoke-virtual {v3, v4, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    new-instance v0, LOP;

    iget-object v3, p0, LOM;->a:LOJ;

    iget-object v4, p0, LOM;->a:Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, LOM;->a:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v5}, LOP;-><init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    new-array v3, v1, [Ljava/lang/Boolean;

    aput-object p2, v3, v2

    invoke-virtual {v0, v3}, LOP;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 229
    return v1

    :cond_1
    move v0, v2

    .line 217
    goto :goto_0

    .line 223
    :cond_2
    const-string v0, "settingsPhotoBackupAddToMyDriveDisable"

    goto :goto_1
.end method
