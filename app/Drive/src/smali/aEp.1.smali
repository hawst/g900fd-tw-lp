.class public final enum LaEp;
.super Ljava/lang/Enum;
.source "CacheListTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEp;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEp;

.field private static final synthetic a:[LaEp;

.field public static final enum b:LaEp;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 44
    new-instance v0, LaEp;

    const-string v1, "APP_ID"

    invoke-static {}, LaEo;->b()LaEo;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 45
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 46
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 45
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LaEp;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEp;->a:LaEp;

    .line 49
    new-instance v0, LaEp;

    const-string v1, "CONTENT_ID"

    invoke-static {}, LaEo;->b()LaEo;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "contentId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 50
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 51
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 50
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaEp;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEp;->b:LaEp;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [LaEp;

    sget-object v1, LaEp;->a:LaEp;

    aput-object v1, v0, v6

    sget-object v1, LaEp;->b:LaEp;

    aput-object v1, v0, v7

    sput-object v0, LaEp;->a:[LaEp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEp;->a:LaFr;

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEp;
    .locals 1

    .prologue
    .line 42
    const-class v0, LaEp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEp;

    return-object v0
.end method

.method public static values()[LaEp;
    .locals 1

    .prologue
    .line 42
    sget-object v0, LaEp;->a:[LaEp;

    invoke-virtual {v0}, [LaEp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEp;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LaEp;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, LaEp;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
