.class public Lato;
.super Ljava/lang/Object;
.source "WelcomeController.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Lasa;

.field private a:Latn;


# direct methods
.method constructor <init>(Landroid/content/Context;Lasa;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    iput-object p1, p0, Lato;->a:Landroid/content/Context;

    .line 317
    iput-object p2, p0, Lato;->a:Lasa;

    .line 318
    return-void
.end method

.method private b()Latn;
    .locals 5

    .prologue
    .line 343
    iget-object v0, p0, Lato;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "WelcomeLatest"

    const-string v2, "{}"

    .line 344
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lato;->a:Lasa;

    .line 346
    invoke-virtual {v1}, Lasa;->a()I

    move-result v1

    invoke-static {v0, v1}, Latn;->a(Ljava/lang/String;I)Latn;

    move-result-object v0

    .line 347
    const-string v1, "WelcomeController"

    const-string v2, "from Preferences: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 348
    return-object v0
.end method


# virtual methods
.method a()Latn;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lato;->a:Latn;

    .line 329
    if-nez v0, :cond_1

    .line 330
    monitor-enter p0

    .line 331
    :try_start_0
    iget-object v0, p0, Lato;->a:Latn;

    .line 332
    if-nez v0, :cond_0

    .line 333
    invoke-direct {p0}, Lato;->b()Latn;

    move-result-object v0

    .line 334
    iput-object v0, p0, Lato;->a:Latn;

    .line 336
    :cond_0
    monitor-exit p0

    .line 339
    :cond_1
    return-object v0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method declared-synchronized a(Latn;)V
    .locals 3

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latn;

    iput-object v0, p0, Lato;->a:Latn;

    .line 323
    iget-object v0, p0, Lato;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WelcomeLatest"

    .line 324
    invoke-virtual {p1}, Latn;->a()Ljava/lang/String;

    move-result-object v2

    .line 323
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    monitor-exit p0

    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
