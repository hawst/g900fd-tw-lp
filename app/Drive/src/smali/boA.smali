.class abstract enum LboA;
.super Ljava/lang/Enum;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LboA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LboA;

.field private static final synthetic a:[LboA;

.field public static final enum b:LboA;

.field public static final enum c:LboA;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 292
    new-instance v0, LboB;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, LboB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LboA;->a:LboA;

    .line 306
    new-instance v0, LboC;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, LboC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LboA;->b:LboA;

    .line 320
    new-instance v0, LboD;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, LboD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LboA;->c:LboA;

    .line 286
    const/4 v0, 0x3

    new-array v0, v0, [LboA;

    sget-object v1, LboA;->a:LboA;

    aput-object v1, v0, v2

    sget-object v1, LboA;->b:LboA;

    aput-object v1, v0, v3

    sget-object v1, LboA;->c:LboA;

    aput-object v1, v0, v4

    sput-object v0, LboA;->a:[LboA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbnZ;)V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, LboA;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LboA;
    .locals 1

    .prologue
    .line 286
    const-class v0, LboA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LboA;

    return-object v0
.end method

.method public static values()[LboA;
    .locals 1

    .prologue
    .line 286
    sget-object v0, LboA;->a:[LboA;

    invoke-virtual {v0}, [LboA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LboA;

    return-object v0
.end method


# virtual methods
.method abstract a()LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(Lboy;Lbox;Ljava/lang/Object;)LboK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lboy",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;TV;)",
            "LboK",
            "<TK;TV;>;"
        }
    .end annotation
.end method
