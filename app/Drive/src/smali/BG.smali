.class public LBG;
.super Ljava/lang/Object;
.source "DocListStickyHeaderAdapter.java"

# interfaces
.implements LaqN;


# instance fields
.field private final a:I

.field private final a:LCq;

.field private final a:LIK;

.field private final a:LKs;

.field private final a:Z

.field private final b:I

.field private final b:Z

.field private final c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LKs;LIK;LCq;LzO;LapF;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v1, p0, LBG;->d:I

    .line 50
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCq;

    iput-object v0, p0, LBG;->a:LCq;

    .line 51
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    iput-object v0, p0, LBG;->a:LIK;

    .line 52
    iput-boolean p7, p0, LBG;->b:Z

    .line 53
    iput-object p2, p0, LBG;->a:LKs;

    .line 55
    sget-object v0, LBH;->a:[I

    invoke-virtual {p5}, LzO;->a()LzP;

    move-result-object v2

    invoke-virtual {v2}, LzP;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 68
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected Arrangement Mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 57
    :pswitch_0
    sget-object v0, LapG;->c:LapG;

    invoke-interface {p6}, LapF;->a()LapG;

    move-result-object v2

    invoke-virtual {v0, v2}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    sget v0, Lxe;->doc_entry_group_title_sticky_onecolumn:I

    iput v0, p0, LBG;->c:I

    .line 71
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 72
    sget v0, LwZ;->doclist_sticky_header_background:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LBG;->a:I

    .line 75
    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 76
    :goto_1
    iput v0, p0, LBG;->b:I

    .line 79
    invoke-static {v2}, LakQ;->d(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, LakQ;->b(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, LBG;->a:Z

    .line 80
    return-void

    .line 61
    :cond_2
    sget v0, Lxe;->doc_entry_group_title_sticky:I

    iput v0, p0, LBG;->c:I

    goto :goto_0

    .line 65
    :pswitch_1
    sget v0, Lxe;->doc_grid_title_sticky:I

    iput v0, p0, LBG;->c:I

    goto :goto_0

    .line 75
    :cond_3
    sget v0, Lxa;->doclist_group_separator_height:I

    .line 76
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, LBG;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)LIB;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LBG;->a:LCq;

    invoke-interface {v0, p1}, LCq;->a(Landroid/view/View;)LIB;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    .line 85
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, LBG;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 86
    iget-object v1, p0, LBG;->a:LKs;

    invoke-virtual {v1, v0}, LKs;->a(Landroid/view/View;)V

    .line 88
    invoke-static {v0}, LCy;->a(Landroid/view/View;)LCy;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 90
    sget v2, LwZ;->doclist_sticky_header_background:I

    invoke-virtual {v1, v2}, LCy;->b(I)V

    .line 93
    iget-boolean v2, p0, LBG;->b:Z

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, LBG;->a:LIK;

    invoke-virtual {v2}, LIK;->b()I

    move-result v2

    invoke-virtual {v1, v2}, LCy;->a(I)V

    .line 98
    :cond_0
    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    iget-object v1, v1, LCy;->b:Landroid/view/View;

    iget v2, p0, LBG;->d:I

    iget v3, p0, LBG;->d:I

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_1
    iget v1, p0, LBG;->d:I

    iget v2, p0, LBG;->d:I

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 177
    iput p1, p0, LBG;->d:I

    .line 178
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, LBG;->a:LCq;

    invoke-interface {v0, p1}, LCq;->a(Landroid/view/View;)V

    .line 119
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCy;

    .line 135
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 136
    iget-object v0, v0, LCy;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setAlpha(F)V

    .line 138
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;LIB;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 109
    invoke-interface {p2, p3}, LIB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCy;

    .line 112
    invoke-virtual {v0, v1}, LCy;->a(Ljava/lang/String;)V

    .line 114
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;LapG;)V
    .locals 2

    .prologue
    .line 148
    iget-boolean v0, p0, LBG;->a:Z

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCy;

    .line 150
    sget-object v1, LapG;->b:LapG;

    invoke-virtual {p2, v1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    iget-object v0, v0, LCy;->b:Landroid/view/View;

    sget v1, Lxb;->doclist_sticky_header_background_with_drop_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v0, v0, LCy;->b:Landroid/view/View;

    iget v1, p0, LBG;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;LaqM;)V
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCy;

    .line 162
    sget-object v1, LaqM;->a:LaqM;

    invoke-virtual {p2, v1}, LaqM;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, LCy;->a(Z)V

    .line 163
    return-void
.end method

.method public a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, LBG;->a:LCq;

    invoke-interface {v0, p1}, LCq;->b(Landroid/view/View;)V

    .line 124
    return-void
.end method

.method public b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LBG;->a:LCq;

    invoke-interface {v0, p1}, LCq;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
