.class public final LapK;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaqE;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laqx;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaqT;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LapZ;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LapR;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lapn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LapK;->a:LbrA;

    .line 44
    const-class v0, LaqE;

    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->a:Lbsk;

    .line 47
    const-class v0, Laqx;

    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->b:Lbsk;

    .line 50
    const-class v0, LaqT;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->c:Lbsk;

    .line 53
    const-class v0, LapZ;

    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->d:Lbsk;

    .line 56
    const-class v0, LapR;

    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->e:Lbsk;

    .line 59
    const-class v0, Lapn;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LapK;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 272
    sparse-switch p1, :sswitch_data_0

    .line 406
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :sswitch_0
    new-instance v0, LaqE;

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 277
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 275
    invoke-static {v1, v2}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 283
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LapK;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 281
    invoke-static {v2, v3}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, LapK;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->b:Lbsk;

    .line 289
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LapK;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoA;

    iget-object v4, v4, LaoA;->b:Lbsk;

    .line 287
    invoke-static {v3, v4}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LanR;

    iget-object v4, p0, LapK;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LapK;

    iget-object v4, v4, LapK;->d:Lbsk;

    .line 295
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LapK;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LapK;

    iget-object v5, v5, LapK;->d:Lbsk;

    .line 293
    invoke-static {v4, v5}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LapZ;

    iget-object v5, p0, LapK;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LapK;

    iget-object v5, v5, LapK;->e:Lbsk;

    .line 301
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LapK;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LapK;

    iget-object v6, v6, LapK;->e:Lbsk;

    .line 299
    invoke-static {v5, v6}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LapR;

    iget-object v6, p0, LapK;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->h:Lbsk;

    .line 307
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LapK;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoA;

    iget-object v7, v7, LaoA;->h:Lbsk;

    .line 305
    invoke-static {v6, v7}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Laoh;

    invoke-direct/range {v0 .. v6}, LaqE;-><init>(LaGM;LtK;LanR;LapZ;LapR;Laoh;)V

    .line 404
    :goto_0
    return-object v0

    .line 314
    :sswitch_1
    new-instance v1, Laqx;

    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 317
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LPt;

    iget-object v2, v2, LPt;->d:Lbsk;

    .line 315
    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    invoke-direct {v1, v0}, Laqx;-><init>(LPp;)V

    move-object v0, v1

    .line 322
    goto :goto_0

    .line 324
    :sswitch_2
    new-instance v0, LaqT;

    invoke-direct {v0}, LaqT;-><init>()V

    goto :goto_0

    .line 328
    :sswitch_3
    new-instance v3, LapZ;

    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 331
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->d:Lbsk;

    .line 329
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 337
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 335
    invoke-static {v1, v2}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->f:Lbsk;

    .line 343
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LapK;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->f:Lbsk;

    .line 341
    invoke-static {v2, v4}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGR;

    invoke-direct {v3, v0, v1, v2}, LapZ;-><init>(LPp;Landroid/content/Context;LaGR;)V

    move-object v0, v3

    .line 348
    goto :goto_0

    .line 350
    :sswitch_4
    new-instance v1, LapR;

    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 353
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 351
    invoke-static {v0, v2}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LapR;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 358
    goto/16 :goto_0

    .line 360
    :sswitch_5
    new-instance v0, Lapn;

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 363
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->j:Lbsk;

    .line 361
    invoke-static {v1, v2}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaFO;

    iget-object v2, p0, LapK;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lwc;

    iget-object v2, v2, Lwc;->l:Lbsk;

    .line 369
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LapK;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->l:Lbsk;

    .line 367
    invoke-static {v2, v3}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LvY;

    iget-object v3, p0, LapK;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->j:Lbsk;

    .line 375
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LapK;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lwc;

    iget-object v4, v4, Lwc;->j:Lbsk;

    .line 373
    invoke-static {v3, v4}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LvU;

    iget-object v4, p0, LapK;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LRo;

    iget-object v4, v4, LRo;->d:Lbsk;

    .line 381
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LapK;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LRo;

    iget-object v5, v5, LRo;->d:Lbsk;

    .line 379
    invoke-static {v4, v5}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LQV;

    iget-object v5, p0, LapK;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->a:Lbsk;

    .line 387
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LapK;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->a:Lbsk;

    .line 385
    invoke-static {v5, v6}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaGO;

    iget-object v6, p0, LapK;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 393
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LapK;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LpG;

    iget-object v7, v7, LpG;->m:Lbsk;

    .line 391
    invoke-static {v6, v7}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LtK;

    iget-object v7, p0, LapK;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LRo;

    iget-object v7, v7, LRo;->a:Lbsk;

    .line 399
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LapK;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LRo;

    iget-object v8, v8, LRo;->a:Lbsk;

    .line 397
    invoke-static {v7, v8}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LRa;

    invoke-direct/range {v0 .. v7}, Lapn;-><init>(LaFO;LvY;LvU;LQV;LaGO;LtK;LRa;)V

    goto/16 :goto_0

    .line 272
    nop

    :sswitch_data_0
    .sparse-switch
        0x35 -> :sswitch_5
        0x1d3 -> :sswitch_2
        0x462 -> :sswitch_0
        0x463 -> :sswitch_3
        0x464 -> :sswitch_4
        0x465 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 463
    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 225
    const-class v0, Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x73

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 228
    const-class v0, Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x74

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 231
    const-class v0, Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x76

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 234
    const-class v0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x77

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 237
    const-class v0, Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x78

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 240
    const-class v0, Lcom/google/android/apps/docs/view/FastScrollView;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x75

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 243
    const-class v0, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x79

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LapK;->a(LbuP;LbuB;)V

    .line 246
    const-class v0, LaqE;

    iget-object v1, p0, LapK;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 247
    const-class v0, Laqx;

    iget-object v1, p0, LapK;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 248
    const-class v0, LaqT;

    iget-object v1, p0, LapK;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 249
    const-class v0, LapZ;

    iget-object v1, p0, LapK;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 250
    const-class v0, LapR;

    iget-object v1, p0, LapK;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 251
    const-class v0, Lapn;

    iget-object v1, p0, LapK;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LapK;->a(Ljava/lang/Class;Lbsk;)V

    .line 252
    iget-object v0, p0, LapK;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x462

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 254
    iget-object v0, p0, LapK;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x465

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 256
    iget-object v0, p0, LapK;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1d3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 258
    iget-object v0, p0, LapK;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x463

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 260
    iget-object v0, p0, LapK;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x464

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 262
    iget-object v0, p0, LapK;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x35

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 264
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 413
    packed-switch p1, :pswitch_data_0

    .line 457
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;

    .line 417
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 418
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;)V

    .line 459
    :goto_0
    return-void

    .line 421
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/view/DocListView;

    .line 423
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 424
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/DocListView;)V

    goto :goto_0

    .line 427
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/view/GestureImageView;

    .line 429
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 430
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/GestureImageView;)V

    goto :goto_0

    .line 433
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    .line 435
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 436
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    goto :goto_0

    .line 439
    :pswitch_4
    check-cast p2, Lcom/google/android/apps/docs/view/ThumbnailView;

    .line 441
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 442
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/ThumbnailView;)V

    goto :goto_0

    .line 445
    :pswitch_5
    check-cast p2, Lcom/google/android/apps/docs/view/FastScrollView;

    .line 447
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 448
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/FastScrollView;)V

    goto :goto_0

    .line 451
    :pswitch_6
    check-cast p2, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;

    .line 453
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 454
    invoke-virtual {v0, p2}, LapK;->a(Lcom/google/android/apps/docs/view/PinWarningDialogFragment;)V

    goto :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x73
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    .line 80
    invoke-virtual {v0, p1}, LapK;->a(Lcom/google/android/apps/docs/view/FastScrollView;)V

    .line 81
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->y:Lbsk;

    .line 84
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->y:Lbsk;

    .line 82
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->d:Lajw;

    .line 88
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->g:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->g:Lbsk;

    .line 89
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZP;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LZP;

    .line 95
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ad:Lbsk;

    .line 98
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->ad:Lbsk;

    .line 96
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    .line 102
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->aj:Lbsk;

    .line 105
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->aj:Lbsk;

    .line 103
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:Lajw;

    .line 109
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 110
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LQr;

    .line 116
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ag:Lbsk;

    .line 119
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->ag:Lbsk;

    .line 117
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->c:Lajw;

    .line 123
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->d:Lbsk;

    .line 126
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->d:Lbsk;

    .line 124
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    .line 130
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->aa:Lbsk;

    .line 133
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 131
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LsC;

    .line 137
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->B:Lbsk;

    .line 140
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->B:Lbsk;

    .line 138
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->e:Lajw;

    .line 144
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->l:Lbsk;

    .line 147
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->l:Lbsk;

    .line 145
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->f:Lajw;

    .line 151
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/FastScrollView;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 210
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 208
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/FastScrollView;->a:LtK;

    .line 214
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/GestureImageView;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->ai:Lbsk;

    .line 158
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->ai:Lbsk;

    .line 156
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqL;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/GestureImageView;->a:LaqL;

    .line 162
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/PinWarningDialogFragment;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 219
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 220
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->T:Lbsk;

    .line 169
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->T:Lbsk;

    .line 167
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqA;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqA;

    .line 173
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 176
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 174
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LqK;

    .line 180
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 183
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->d:Lbsk;

    .line 181
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LPp;

    .line 187
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->ai:Lbsk;

    .line 190
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->ai:Lbsk;

    .line 188
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqL;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqL;

    .line 194
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/ThumbnailView;)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->W:Lbsk;

    .line 201
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/ThumbnailView;->a:LaqQ;

    .line 203
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, LapK;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    iget-object v0, v0, LapK;->c:Lbsk;

    .line 71
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LapK;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LapK;

    iget-object v1, v1, LapK;->c:Lbsk;

    .line 69
    invoke-static {v0, v1}, LapK;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqT;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;->a:LaqT;

    .line 75
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method
