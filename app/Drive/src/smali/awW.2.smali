.class LawW;
.super Ljava/lang/Object;
.source "PasswordDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic a:LawT;


# direct methods
.method constructor <init>(LawT;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, LawW;->a:LawT;

    iput-object p2, p0, LawW;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, LawW;->a:LawT;

    iget-object v0, v0, LawT;->a:LawS;

    invoke-static {v0}, LawS;->a(LawS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, LawW;->a:LawT;

    iget-object v0, v0, LawT;->a:LawS;

    invoke-static {v0}, LawS;->a(LawS;)V

    .line 93
    :cond_0
    iget-object v1, p0, LawW;->a:Landroid/widget/Button;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 94
    return-void

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
