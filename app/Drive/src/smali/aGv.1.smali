.class public final enum LaGv;
.super Ljava/lang/Enum;
.source "Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaGv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaGv;

.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LaGv;

.field public static final enum b:LaGv;

.field public static final enum c:LaGv;

.field public static final enum d:LaGv;

.field public static final enum e:LaGv;

.field public static final enum f:LaGv;

.field public static final enum g:LaGv;

.field public static final enum h:LaGv;

.field public static final enum i:LaGv;

.field public static final enum j:LaGv;

.field public static final enum k:LaGv;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:I

.field private final b:Z

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    .line 150
    new-instance v0, LaGv;

    const-string v1, "COLLECTION"

    const/4 v2, 0x0

    const-string v3, "folder"

    sget v4, Lxb;->ic_type_folder:I

    sget v5, Lxb;->ic_type_folder:I

    sget v6, Lxb;->ic_type_folder_black:I

    sget v7, Lxb;->ic_type_folder_black_big:I

    sget v8, Lxb;->ic_type_folder_shared:I

    sget v9, Lxb;->ic_type_folder_shared_black_big:I

    sget v10, Lxi;->document_type_folder:I

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIIIZZ)V

    sput-object v0, LaGv;->a:LaGv;

    .line 156
    new-instance v0, LaGv;

    const-string v1, "DOCUMENT"

    const/4 v2, 0x1

    const-string v3, "document"

    sget v4, Lxb;->ic_type_doc:I

    sget v5, Lxb;->ic_type_doc_alpha:I

    sget v6, Lxb;->ic_type_doc_black:I

    sget v7, Lxb;->ic_type_doc_black_big:I

    sget v8, Lxi;->document_type_google_document:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->b:LaGv;

    .line 160
    new-instance v0, LaGv;

    const-string v1, "DRAWING"

    const/4 v2, 0x2

    const-string v3, "drawing"

    sget v4, Lxb;->ic_type_drawing:I

    sget v5, Lxb;->ic_type_drawing_alpha:I

    sget v6, Lxb;->ic_type_drawing_black:I

    sget v7, Lxb;->ic_type_drawing_black_big:I

    sget v8, Lxi;->document_type_google_drawing:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->c:LaGv;

    .line 165
    new-instance v0, LaGv;

    const-string v1, "FILE"

    const/4 v2, 0x3

    const-string v3, "file"

    sget v4, Lxb;->ic_type_file:I

    sget v5, Lxb;->ic_type_file_alpha:I

    sget v6, Lxb;->ic_type_file_black:I

    sget v7, Lxb;->ic_type_file_black_big:I

    sget v8, Lxi;->document_type_file:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->d:LaGv;

    .line 169
    new-instance v0, LaGv;

    const-string v1, "FORM"

    const/4 v2, 0x4

    const-string v3, "form"

    sget v4, Lxb;->ic_type_form:I

    sget v5, Lxb;->ic_type_form_alpha:I

    sget v6, Lxb;->ic_type_form_black:I

    sget v7, Lxb;->ic_type_form_black_big:I

    sget v8, Lxi;->document_type_google_form:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->e:LaGv;

    .line 173
    new-instance v0, LaGv;

    const-string v1, "PDF"

    const/4 v2, 0x5

    const-string v3, "pdf"

    sget v4, Lxb;->ic_type_pdf:I

    sget v5, Lxb;->ic_type_pdf_alpha:I

    sget v6, Lxb;->ic_type_pdf_black:I

    sget v7, Lxb;->ic_type_pdf_black_big:I

    sget v8, Lxi;->document_type_pdf:I

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->f:LaGv;

    .line 177
    new-instance v0, LaGv;

    const-string v1, "PRESENTATION"

    const/4 v2, 0x6

    const-string v3, "presentation"

    sget v4, Lxb;->ic_type_presentation:I

    sget v5, Lxb;->ic_type_presentation_alpha:I

    sget v6, Lxb;->ic_type_presentation_black:I

    sget v7, Lxb;->ic_type_presentation_black_big:I

    sget v8, Lxi;->document_type_google_presentation:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->g:LaGv;

    .line 183
    new-instance v0, LaGv;

    const-string v1, "SITE"

    const/4 v2, 0x7

    const-string v3, "site"

    sget v4, Lxb;->ic_type_site:I

    sget v5, Lxb;->ic_type_site_alpha:I

    sget v6, Lxb;->ic_type_site_black:I

    sget v7, Lxb;->ic_type_site_black_big:I

    sget v8, Lxi;->document_type_google_site:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->h:LaGv;

    .line 187
    new-instance v0, LaGv;

    const-string v1, "SPREADSHEET"

    const/16 v2, 0x8

    const-string v3, "spreadsheet"

    sget v4, Lxb;->ic_type_sheet:I

    sget v5, Lxb;->ic_type_sheet_alpha:I

    sget v6, Lxb;->ic_type_sheet_black:I

    sget v7, Lxb;->ic_type_sheet_black_big:I

    sget v8, Lxi;->document_type_google_spreadsheet:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->i:LaGv;

    .line 192
    new-instance v0, LaGv;

    const-string v1, "TABLE"

    const/16 v2, 0x9

    const-string v3, "table"

    sget v4, Lxb;->ic_type_fusion:I

    sget v5, Lxb;->ic_type_fusion_alpha:I

    sget v6, Lxb;->ic_type_fusion_black:I

    sget v7, Lxb;->ic_type_fusion_black_big:I

    sget v8, Lxi;->document_type_google_table:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->j:LaGv;

    .line 200
    new-instance v0, LaGv;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xa

    const-string v3, "unknown"

    sget v4, Lxb;->ic_type_file:I

    sget v5, Lxb;->ic_type_file_alpha:I

    sget v6, Lxb;->ic_type_file_black:I

    sget v7, Lxb;->ic_type_file_black_big:I

    sget v8, Lxi;->document_type_unknown:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V

    sput-object v0, LaGv;->k:LaGv;

    .line 148
    const/16 v0, 0xb

    new-array v0, v0, [LaGv;

    const/4 v1, 0x0

    sget-object v2, LaGv;->a:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaGv;->b:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaGv;->c:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaGv;->d:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaGv;->e:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaGv;->f:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaGv;->g:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaGv;->h:LaGv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaGv;->i:LaGv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaGv;->j:LaGv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaGv;->k:LaGv;

    aput-object v2, v0, v1

    sput-object v0, LaGv;->a:[LaGv;

    .line 327
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v1

    .line 328
    invoke-static {}, LaGv;->values()[LaGv;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 329
    invoke-virtual {v4}, LaGv;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 330
    invoke-virtual {v4}, LaGv;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 328
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LaGv;->a:LbmL;

    .line 334
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIIIIIIZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIIIIIIZZ)V"
        }
    .end annotation

    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 245
    iput-object p3, p0, LaGv;->a:Ljava/lang/String;

    .line 246
    iput p4, p0, LaGv;->a:I

    .line 247
    iput p5, p0, LaGv;->b:I

    .line 248
    iput p6, p0, LaGv;->c:I

    .line 249
    iput p7, p0, LaGv;->d:I

    .line 250
    iput p8, p0, LaGv;->e:I

    .line 251
    iput p9, p0, LaGv;->f:I

    .line 252
    iput p10, p0, LaGv;->g:I

    .line 253
    iput-boolean p11, p0, LaGv;->a:Z

    .line 254
    iput-boolean p12, p0, LaGv;->b:Z

    .line 255
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIIIIZZ)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIIIIZZ)V"
        }
    .end annotation

    .prologue
    .line 223
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p4

    move/from16 v9, p6

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v0 .. v12}, LaGv;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIIIZZ)V

    .line 233
    return-void
.end method

.method public static a(Ljava/lang/String;)LaGv;
    .locals 1

    .prologue
    .line 337
    sget-object v0, LaGv;->a:LbmL;

    invoke-virtual {v0, p0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    .line 338
    if-nez v0, :cond_0

    .line 339
    sget-object v0, LaGv;->k:LaGv;

    .line 341
    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LaGv;
    .locals 2

    .prologue
    .line 350
    const-string v0, "application/vnd.google-apps."

    .line 351
    if-nez p0, :cond_0

    .line 352
    sget-object v0, LaGv;->k:LaGv;

    .line 358
    :goto_0
    return-object v0

    .line 353
    :cond_0
    const-string v1, "application/pdf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    sget-object v0, LaGv;->f:LaGv;

    goto :goto_0

    .line 355
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 356
    sget-object v0, LaGv;->d:LaGv;

    goto :goto_0

    .line 358
    :cond_2
    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LaGv;
    .locals 1

    .prologue
    .line 148
    const-class v0, LaGv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaGv;

    return-object v0
.end method

.method public static values()[LaGv;
    .locals 1

    .prologue
    .line 148
    sget-object v0, LaGv;->a:[LaGv;

    invoke-virtual {v0}, [LaGv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaGv;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, LaGv;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, LaGv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, LaGv;->a:Z

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, LaGv;->b:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 312
    sget-object v0, LaGv;->k:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const/4 v0, 0x0

    .line 319
    :goto_0
    return-object v0

    .line 314
    :cond_0
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    const-string v0, ""

    goto :goto_0

    .line 316
    :cond_1
    sget-object v0, LaGv;->f:LaGv;

    invoke-virtual {p0, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    const-string v0, "application/pdf"

    goto :goto_0

    .line 319
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "application/vnd.google-apps."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaGv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, LaGv;->b:Z

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, LaGv;->d:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, LaGv;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, LaGv;->f:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 289
    iget v0, p0, LaGv;->g:I

    return v0
.end method
