.class final enum LOw;
.super Ljava/lang/Enum;
.source "IncomingTabsDocListHeader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LOw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LOw;

.field private static final synthetic a:[LOw;

.field public static final enum b:LOw;


# instance fields
.field final a:I

.field final a:LCl;

.field final a:Ljava/lang/String;

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, LOw;

    const-string v1, "FROM_PEOPLE"

    const-string v3, "FromPeople"

    sget v4, LOV;->incoming_tab_label_from_people:I

    sget-object v5, LCe;->o:LCe;

    sget v6, LOT;->tab1:I

    invoke-direct/range {v0 .. v6}, LOw;-><init>(Ljava/lang/String;ILjava/lang/String;ILCl;I)V

    sput-object v0, LOw;->a:LOw;

    .line 40
    new-instance v3, LOw;

    const-string v4, "FROM_PHOTOS"

    const-string v6, "FromPhotos"

    sget v7, LOV;->incoming_tab_label_from_google_photos:I

    sget-object v8, LCe;->r:LCe;

    sget v9, LOT;->tab2:I

    move v5, v10

    invoke-direct/range {v3 .. v9}, LOw;-><init>(Ljava/lang/String;ILjava/lang/String;ILCl;I)V

    sput-object v3, LOw;->b:LOw;

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [LOw;

    sget-object v1, LOw;->a:LOw;

    aput-object v1, v0, v2

    sget-object v1, LOw;->b:LOw;

    aput-object v1, v0, v10

    sput-object v0, LOw;->a:[LOw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ILCl;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LCl;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput-object p3, p0, LOw;->a:Ljava/lang/String;

    .line 50
    iput p4, p0, LOw;->a:I

    .line 51
    iput-object p5, p0, LOw;->a:LCl;

    .line 52
    iput p6, p0, LOw;->b:I

    .line 53
    return-void
.end method

.method static a(LCl;)LOw;
    .locals 5

    .prologue
    .line 67
    invoke-static {}, LOw;->values()[LOw;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 68
    iget-object v4, v0, LOw;->a:LCl;

    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 72
    :goto_1
    return-object v0

    .line 67
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 72
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Ljava/lang/String;)LOw;
    .locals 5

    .prologue
    .line 57
    invoke-static {}, LOw;->values()[LOw;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 58
    iget-object v4, v0, LOw;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    :goto_1
    return-object v0

    .line 57
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LOw;
    .locals 1

    .prologue
    .line 37
    const-class v0, LOw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LOw;

    return-object v0
.end method

.method public static values()[LOw;
    .locals 1

    .prologue
    .line 37
    sget-object v0, LOw;->a:[LOw;

    invoke-virtual {v0}, [LOw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LOw;

    return-object v0
.end method
