.class public LagA;
.super Ljava/lang/Object;
.source "FeedProcessorDriverHelperImpl.java"

# interfaces
.implements LagD;


# instance fields
.field a:LZS;
    .annotation runtime Lbxv;
        a = "wapiFeedProcessor"
    .end annotation
.end field

.field a:LaGg;

.field a:LaKM;

.field a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/SyncResult;LaKa;Lagy;)V
    .locals 17

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-object v2, v0, LagA;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v6

    .line 53
    invoke-interface/range {p2 .. p2}, LaKa;->a()Lbxc;

    move-result-object v2

    check-cast v2, LaeD;

    .line 55
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lagy;->a(LaeD;)V

    .line 57
    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LtK;

    sget-object v4, Lry;->f:Lry;

    invoke-interface {v3, v4}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 58
    invoke-interface/range {p2 .. p2}, LaKa;->a()Ljava/util/List;

    move-result-object v5

    .line 59
    if-nez v5, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LaKM;

    invoke-interface {v3}, LaKM;->a()J

    move-result-wide v8

    .line 64
    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LaGg;

    move-object/from16 v0, p0

    iget-object v4, v0, LagA;->a:LZS;

    invoke-interface {v3, v4}, LaGg;->a(LZS;)V

    .line 65
    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LaGg;

    invoke-interface {v3}, LaGg;->a()V

    .line 67
    :try_start_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaJT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :try_start_1
    move-object/from16 v0, p3

    invoke-interface {v0, v2, v3}, Lagy;->a(LaeD;LaJT;)V

    .line 70
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 71
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    :catch_0
    move-exception v4

    .line 74
    :try_start_2
    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v12, v11, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    iput-wide v12, v11, Landroid/content/SyncStats;->numParseExceptions:J

    .line 75
    const-string v11, "FeedProcessorDriverHelperImpl"

    const-string v12, "Error parsing entry %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v3, v13, v14

    invoke-static {v11, v4, v12, v13}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 77
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LaGg;

    move-object/from16 v0, p0

    iget-object v4, v0, LagA;->a:LZS;

    invoke-interface {v3, v4}, LaGg;->a(LZS;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 82
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, LagA;->a:LaGg;

    invoke-interface {v3}, LaGg;->b()V

    throw v2

    .line 79
    :cond_3
    :try_start_3
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lagy;->b(LaeD;)V

    .line 80
    move-object/from16 v0, p0

    iget-object v2, v0, LagA;->a:LaGg;

    invoke-interface {v2}, LaGg;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, LagA;->a:LaGg;

    invoke-interface {v2}, LaGg;->b()V

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, LagA;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    sub-long/2addr v2, v8

    .line 85
    const-string v4, "FeedProcessorDriverHelperImpl"

    const-string v8, "stored %d entries (%d msec)"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v10

    const/4 v5, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v9, v5

    invoke-static {v4, v8, v9}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 104
    :goto_2
    const-string v2, "FeedProcessorDriverHelperImpl"

    const-string v3, "Sync stats after %d msec: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, LagA;->a:LaKM;

    invoke-interface {v8}, LaKM;->a()J

    move-result-wide v8

    sub-long v6, v8, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 87
    :cond_4
    const/4 v3, 0x0

    move-object v4, v3

    .line 88
    :goto_3
    invoke-interface/range {p2 .. p2}, LaKa;->a()Lbxj;

    move-result-object v3

    invoke-interface {v3}, Lbxj;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 89
    invoke-static {}, LpD;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 93
    :try_start_4
    invoke-interface/range {p2 .. p2}, LaKa;->a()Lbxj;

    move-result-object v3

    invoke-interface {v3, v4}, Lbxj;->a(Lbxb;)Lbxb;

    move-result-object v3

    check-cast v3, LaeF;
    :try_end_4
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_1

    .line 94
    :try_start_5
    move-object/from16 v0, p3

    invoke-interface {v0, v2, v3}, Lagy;->a(LaeD;LaJT;)V
    :try_end_5
    .catch Ljava/text/ParseException; {:try_start_5 .. :try_end_5} :catch_2

    move-object v4, v3

    .line 98
    goto :goto_3

    .line 95
    :catch_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v4

    move-object/from16 v4, v16

    .line 96
    :goto_4
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 97
    const-string v5, "FeedProcessorDriverHelperImpl"

    const-string v8, "Error parsing entry %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    invoke-static {v5, v4, v8, v9}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v4, v3

    .line 98
    goto :goto_3

    .line 101
    :cond_5
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lagy;->b(LaeD;)V

    goto :goto_2

    .line 95
    :catch_2
    move-exception v4

    goto :goto_4
.end method
