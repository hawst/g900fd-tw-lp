.class public final LaHH;
.super LaHp;
.source "UntrashOp.java"


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V
    .locals 1

    .prologue
    .line 24
    const-string v0, "untrash"

    invoke-direct {p0, p1, p2, v0}, LaHp;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHH;
    .locals 1

    .prologue
    .line 30
    new-instance v0, LaHH;

    invoke-direct {v0, p0, p1}, LaHH;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    return-object v0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 3

    .prologue
    .line 35
    new-instance v0, LaHG;

    iget-object v1, p0, LaHH;->a:LaGg;

    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaHG;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    .line 36
    invoke-virtual {p1}, LaGe;->d()V

    .line 37
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 42
    invoke-super {p0}, LaHp;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 43
    const-string v1, "operationName"

    const-string v2, "untrash"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    return-object v0
.end method

.method protected a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 50
    instance-of v0, p1, LaHH;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    .line 53
    :cond_0
    check-cast p1, LaHH;

    .line 54
    invoke-virtual {p0, p1}, LaHH;->a(LaHp;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, LaHH;->b()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 64
    const-string v0, "UntrashOp[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LaHH;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
