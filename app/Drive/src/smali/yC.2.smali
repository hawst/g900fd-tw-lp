.class LyC;
.super Ljava/lang/Object;
.source "ActivityCard.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/util/List",
        "<",
        "LPD;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LyB;


# direct methods
.method constructor <init>(LyB;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, LyC;->a:LyB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, LyC;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 71
    const-string v0, "ActivityCard"

    const-string v1, "Activity failed to load"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 72
    iget-object v0, p0, LyC;->a:LyB;

    invoke-virtual {v0}, LyB;->notifyDataSetChanged()V

    .line 73
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v2

    .line 78
    const/4 v0, 0x0

    .line 79
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPD;

    .line 80
    invoke-virtual {v0}, LPD;->a()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, LyC;->a:LyB;

    invoke-static {v4}, LyB;->a(LyB;)LtK;

    move-result-object v4

    sget-object v5, Lry;->at:Lry;

    invoke-interface {v4, v5}, LtK;->a(LtJ;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 82
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 83
    iget-object v4, p0, LyC;->a:LyB;

    invoke-static {v4}, LyB;->a(LyB;)LyN;

    move-result-object v4

    iget-object v5, p0, LyC;->a:LyB;

    invoke-static {v5}, LyB;->a(LyB;)LaGu;

    move-result-object v5

    iget-object v6, p0, LyC;->a:LyB;

    invoke-static {v6}, LyB;->a(LyB;)Lamw;

    move-result-object v6

    invoke-virtual {v4, v0, v5, v6}, LyN;->a(LPD;LaGu;Lamw;)LyI;

    move-result-object v0

    .line 84
    invoke-virtual {v2, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 86
    iget-object v0, p0, LyC;->a:LyB;

    invoke-static {v0}, LyB;->a(LyB;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    if-ne v1, v0, :cond_2

    .line 91
    :cond_1
    iget-object v0, p0, LyC;->a:LyB;

    new-instance v1, Laqs;

    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v2

    invoke-direct {v1, v2}, Laqs;-><init>(LbmF;)V

    invoke-static {v0, v1}, LyB;->a(LyB;Laqs;)Laqs;

    .line 92
    iget-object v0, p0, LyC;->a:LyB;

    invoke-static {v0}, LyB;->a(LyB;)Laqs;

    move-result-object v0

    iget-object v1, p0, LyC;->a:LyB;

    invoke-static {v1}, LyB;->a(LyB;)Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Laqs;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 93
    iget-object v0, p0, LyC;->a:LyB;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, LyB;->a(LyB;I)I

    .line 94
    iget-object v0, p0, LyC;->a:LyB;

    invoke-virtual {v0}, LyB;->notifyDataSetChanged()V

    .line 95
    return-void

    :cond_2
    move v0, v1

    move v1, v0

    .line 90
    goto :goto_0
.end method
