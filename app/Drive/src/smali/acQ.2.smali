.class public final LacQ;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LacR;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LacP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 39
    iput-object p1, p0, LacQ;->a:LbrA;

    .line 40
    const-class v0, LacR;

    const-class v1, LbuO;

    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LacQ;->a:Lbsk;

    .line 43
    const-class v0, LacP;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LacQ;->b:Lbsk;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 136
    packed-switch p1, :pswitch_data_0

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :pswitch_0
    new-instance v2, LacR;

    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 141
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 139
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 147
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LacQ;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 145
    invoke-static {v1, v3}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    invoke-direct {v2, v0, v1}, LacR;-><init>(Landroid/content/Context;LaGM;)V

    move-object v0, v2

    .line 174
    :goto_0
    return-object v0

    .line 154
    :pswitch_1
    new-instance v3, LacP;

    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 157
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 155
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 163
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LacQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 161
    invoke-static {v1, v2}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LacQ;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaHX;

    iget-object v2, v2, LaHX;->o:Lbsk;

    .line 169
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LacQ;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaHX;

    iget-object v4, v4, LaHX;->o:Lbsk;

    .line 167
    invoke-static {v2, v4}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaIc;

    invoke-direct {v3, v0, v1, v2}, LacP;-><init>(Landroid/content/Context;LaGM;LaIc;)V

    move-object v0, v3

    .line 174
    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x2bc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 209
    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 113
    const-class v0, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x6a

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LacQ;->a(LbuP;LbuB;)V

    .line 116
    const-class v0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x6b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LacQ;->a(LbuP;LbuB;)V

    .line 119
    const-class v0, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x6d

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LacQ;->a(LbuP;LbuB;)V

    .line 122
    const-class v0, LacR;

    iget-object v1, p0, LacQ;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LacQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 123
    const-class v0, LacP;

    iget-object v1, p0, LacQ;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LacQ;->a(Ljava/lang/Class;Lbsk;)V

    .line 124
    iget-object v0, p0, LacQ;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2bc

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 126
    iget-object v0, p0, LacQ;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2bd

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 128
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 183
    packed-switch p1, :pswitch_data_0

    .line 203
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;

    .line 187
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LacQ;

    .line 188
    invoke-virtual {v0, p2}, LacQ;->a(Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;)V

    .line 205
    :goto_0
    return-void

    .line 191
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;

    .line 193
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LacQ;

    .line 194
    invoke-virtual {v0, p2}, LacQ;->a(Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;)V

    goto :goto_0

    .line 197
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;

    .line 199
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LacQ;

    .line 200
    invoke-virtual {v0, p2}, LacQ;->a(Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;)V

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x6a
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 100
    invoke-virtual {v0, p1}, LtQ;->a(LqS;)V

    .line 101
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LacQ;

    iget-object v0, v0, LacQ;->b:Lbsk;

    .line 104
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LacQ;

    iget-object v1, v1, LacQ;->b:Lbsk;

    .line 102
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacP;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;->a:LacP;

    .line 108
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 87
    invoke-virtual {v0, p1}, LtQ;->a(LqS;)V

    .line 88
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->c:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->c:Lbsk;

    .line 89
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvq;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:Lvq;

    .line 95
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 53
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 54
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 57
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 55
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:LaGM;

    .line 61
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHX;

    iget-object v0, v0, LaHX;->m:Lbsk;

    .line 64
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHX;

    iget-object v1, v1, LaHX;->m:Lbsk;

    .line 62
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIm;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:LaIm;

    .line 68
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 71
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 69
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:Lald;

    .line 75
    iget-object v0, p0, LacQ;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->d:Lbsk;

    .line 78
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LacQ;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->d:Lbsk;

    .line 76
    invoke-static {v0, v1}, LacQ;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lub;

    iput-object v0, p1, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:Lub;

    .line 82
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method
