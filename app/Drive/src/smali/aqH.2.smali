.class public LaqH;
.super Ljava/lang/Object;
.source "PreviewPagerAdapter.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Laqz;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, LaqH;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Laqz;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LaqH;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;Laqz;)V

    .line 116
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    check-cast p1, Laqz;

    invoke-virtual {p0, p1}, LaqH;->a(Laqz;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 109
    const-string v0, "PreviewPagerAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load preview for document: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, LaqH;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    .line 111
    return-void
.end method
