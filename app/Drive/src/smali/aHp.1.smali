.class public abstract LaHp;
.super Ljava/lang/Object;
.source "OperationImpl.java"

# interfaces
.implements LaHo;


# instance fields
.field private final a:J

.field protected final a:LaGg;

.field protected final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Date;


# direct methods
.method constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p0, LaHp;->a:LaGg;

    .line 35
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 36
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaHp;->a:Ljava/util/Date;

    .line 37
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaHp;->a:Ljava/lang/String;

    .line 38
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v0

    iput-wide v0, p0, LaHp;->a:J

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaHp;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LaHp;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LaHp;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 92
    const-string v1, "entrySqlId"

    iget-wide v2, p0, LaHp;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 93
    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, LaHp;->a:LaGg;

    invoke-virtual {p0}, LaHp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(LaHp;)Z
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p1, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(LaHy;LaHD;)Z
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LaHp;->a:LaGg;

    invoke-virtual {p0}, LaHp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 71
    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2, v0}, LaHp;->a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->hashCode()I

    move-result v0

    return v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LaHp;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
