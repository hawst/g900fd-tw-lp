.class public LTe;
.super Ljava/lang/Object;
.source "AuthTokenManagerImpl.java"

# interfaces
.implements LTd;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:LSZ;

.field private final a:LTF;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LTf;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;


# direct methods
.method public constructor <init>(LSZ;LTF;LQr;LqK;)V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LTe;->a:Ljava/util/Map;

    .line 165
    iput-object p1, p0, LTe;->a:LSZ;

    .line 166
    iput-object p2, p0, LTe;->a:LTF;

    .line 167
    iput-object p3, p0, LTe;->a:LQr;

    .line 168
    iput-object p4, p0, LTe;->a:LqK;

    .line 169
    return-void
.end method

.method static synthetic a(LTe;)LSZ;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LTe;->a:LSZ;

    return-object v0
.end method

.method private declared-synchronized a(LaFO;)LTf;
    .locals 2

    .prologue
    .line 214
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LTe;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTf;

    .line 215
    if-nez v0, :cond_0

    .line 216
    new-instance v0, LTf;

    invoke-direct {v0, p0, p1}, LTf;-><init>(LTe;LaFO;)V

    .line 217
    iget-object v1, p0, LTe;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_0
    monitor-exit p0

    return-object v0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static a(ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 305
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    if-nez p2, :cond_0

    .line 309
    const-string p2, "wise"

    .line 313
    :cond_0
    if-eqz p0, :cond_1

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "weblogin:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&continue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&de=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    .line 323
    :cond_1
    const-string v0, "&"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 324
    const-string v0, "weblogin:service=%s&continue=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v2

    aput-object p1, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 323
    goto :goto_1
.end method


# virtual methods
.method public a(LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 282
    const-string v0, "https://www.google.com/accounts/TokenAuth"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 284
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LTe;->a(LaFO;I)Ljava/lang/String;

    move-result-object v1

    .line 286
    const-string v2, "auth"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 287
    const-string v1, "continue"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 288
    if-eqz p3, :cond_0

    .line 289
    const-string v1, "service"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 291
    :cond_0
    const-string v1, "source"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 293
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method a(LaFO;I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 228
    if-lez p2, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 229
    iget-object v0, p0, LTe;->a:LqK;

    const-string v3, "authentication"

    const-string v4, "uberAuthTokenRequested"

    invoke-virtual {v0, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, LTe;->a:LSZ;

    invoke-interface {v0}, LSZ;->a()V

    .line 234
    :goto_1
    add-int/lit8 v1, v1, 0x1

    if-gt v1, p2, :cond_3

    .line 235
    :try_start_0
    const-string v0, "LSID"

    invoke-virtual {p0, p1, v0}, LTe;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    const-string v3, "SID"

    invoke-virtual {p0, p1, v3}, LTe;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 238
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    const-string v5, "https://www.google.com/accounts/IssueAuthToken"

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 239
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 240
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "SID"

    invoke-direct {v6, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "LSID"

    invoke-direct {v3, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "service"

    const-string v6, "gaia"

    invoke-direct {v0, v3, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v0, v5}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 244
    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :try_start_1
    iget-object v0, p0, LTe;->a:LTF;

    invoke-interface {v0, v4}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 247
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 248
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 249
    if-nez v0, :cond_1

    move-object v0, v2

    .line 250
    :goto_2
    const/16 v4, 0xc8

    if-ne v3, v4, :cond_2

    if-eqz v0, :cond_2

    .line 251
    iget-object v2, p0, LTe;->a:LqK;

    const-string v3, "authentication"

    const-string v4, "uberAuthTokenRequestSucceeded"

    const/4 v5, 0x0

    int-to-long v6, v1

    .line 253
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 251
    invoke-virtual {v2, v3, v4, v5, v1}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 266
    :try_start_2
    iget-object v1, p0, LTe;->a:LTF;

    invoke-interface {v1}, LTF;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 271
    iget-object v1, p0, LTe;->a:LSZ;

    invoke-interface {v1}, LSZ;->b()V

    return-object v0

    :cond_0
    move v0, v1

    .line 228
    goto/16 :goto_0

    .line 249
    :cond_1
    :try_start_3
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 256
    :cond_2
    const-string v4, "SID"

    invoke-virtual {p0, p1, v4}, LTe;->a(LaFO;Ljava/lang/String;)V

    .line 257
    const-string v4, "LSID"

    invoke-virtual {p0, p1, v4}, LTe;->a(LaFO;Ljava/lang/String;)V

    .line 258
    const-string v4, "AuthTokenManagerImpl"

    const-string v5, "getUberAuthToken attempt #%d/%d: statusCode %d, entityString=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 259
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x3

    aput-object v0, v6, v3

    .line 258
    invoke-static {v4, v5, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 261
    iget-object v0, p0, LTe;->a:LTF;

    invoke-interface {v0}, LTF;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 266
    :try_start_4
    iget-object v0, p0, LTe;->a:LTF;

    invoke-interface {v0}, LTF;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 271
    :catchall_0
    move-exception v0

    iget-object v1, p0, LTe;->a:LSZ;

    invoke-interface {v1}, LSZ;->b()V

    throw v0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 266
    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, LTe;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    throw v0

    .line 269
    :cond_3
    new-instance v0, LTr;

    invoke-direct {v0}, LTr;-><init>()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public a(LaFO;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0, p1}, LTe;->a(LaFO;)LTf;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p2}, LTf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, LTe;->a:LSZ;

    invoke-interface {v0}, LSZ;->a()V

    .line 350
    return-void
.end method

.method public a(LaFO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0, p1}, LTe;->a(LaFO;)LTf;

    move-result-object v0

    .line 196
    invoke-virtual {v0, p2}, LTf;->a(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public b(LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 333
    iget-object v0, p0, LTe;->a:LQr;

    const-string v1, "useDoubleUrlEncodingForWeblogin"

    .line 334
    invoke-interface {v0, v1, v4}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 336
    invoke-static {v0, p2, p3}, LTe;->a(ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    const-string v1, "AuthTokenManagerImpl"

    const-string v2, "Requesting weblogin authentication, tokenType: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 339
    invoke-virtual {p0, p1, v0}, LTe;->b(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 341
    invoke-virtual {p0, p1, v0}, LTe;->a(LaFO;Ljava/lang/String;)V

    .line 343
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 344
    return-object v0
.end method

.method public b(LaFO;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0, p1}, LTe;->a(LaFO;)LTf;

    move-result-object v0

    .line 188
    invoke-virtual {v0, p2}, LTf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, LTe;->a:LSZ;

    invoke-interface {v0}, LSZ;->b()V

    .line 355
    return-void
.end method
