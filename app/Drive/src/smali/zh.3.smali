.class public Lzh;
.super Ljava/lang/Object;
.source "LinkSharingCard.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/animation/RotateAnimation;

.field final synthetic a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

.field final synthetic a:Lqt;

.field final synthetic b:Lqt;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Landroid/view/animation/RotateAnimation;Lqt;Lqt;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    iput-object p2, p0, Lzh;->a:Landroid/view/animation/RotateAnimation;

    iput-object p3, p0, Lzh;->a:Lqt;

    iput-object p4, p0, Lzh;->b:Lqt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 207
    iget-object v0, p0, Lzh;->a:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 208
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Z)Z

    .line 209
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacj;

    move-result-object v0

    invoke-virtual {v0}, Lacj;->c()V

    .line 210
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LvU;

    move-result-object v0

    invoke-interface {v0}, LvU;->a()V

    .line 211
    invoke-static {}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a()LbmF;

    move-result-object v0

    iget-object v1, p0, Lzh;->a:Lqt;

    invoke-virtual {v0, v1}, LbmF;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-static {}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a()LbmF;

    move-result-object v0

    iget-object v1, p0, Lzh;->b:Lqt;

    invoke-virtual {v0, v1}, LbmF;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lach;

    move-result-object v0

    iget-object v1, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaGu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lach;->a(LaGu;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/ClipboardManager;

    move-result-object v1

    iget-object v2, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaGu;

    move-result-object v2

    invoke-interface {v2}, LaGu;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 215
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    .line 216
    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->share_card_link_shared_and_copied:I

    .line 215
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 218
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 196
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lzh;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Z)Z

    .line 200
    iget-object v0, p0, Lzh;->a:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v2}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 201
    iget-object v0, p0, Lzh;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    .line 202
    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_message_unable_to_change:I

    .line 201
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 203
    return-void
.end method
