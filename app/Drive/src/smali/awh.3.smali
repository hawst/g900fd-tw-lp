.class public Lawh;
.super Lawi;
.source "Observables.java"

# interfaces
.implements Lawe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lawi",
        "<",
        "Lawf",
        "<TV;>;>;",
        "Lawe",
        "<TV;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Lawi;-><init>()V

    .line 90
    iput-object p1, p0, Lawh;->a:Ljava/lang/Object;

    .line 91
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lawh;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lawh;->a:Ljava/lang/Object;

    .line 98
    iput-object p1, p0, Lawh;->a:Ljava/lang/Object;

    .line 99
    invoke-virtual {p0, v0}, Lawh;->c(Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method protected c(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lawh;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawf;

    .line 109
    iget-object v2, p0, Lawh;->a:Ljava/lang/Object;

    invoke-interface {v0, p1, v2}, Lawf;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :cond_0
    return-void
.end method
