.class final enum Lavf;
.super Ljava/lang/Enum;
.source "DisplayData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavf;

.field private static final synthetic a:[Lavf;

.field public static final enum b:Lavf;

.field public static final enum c:Lavf;

.field public static final enum d:Lavf;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lavf;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v2}, Lavf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavf;->a:Lavf;

    .line 37
    new-instance v0, Lavf;

    const-string v1, "FILE"

    invoke-direct {v0, v1, v3}, Lavf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavf;->b:Lavf;

    .line 38
    new-instance v0, Lavf;

    const-string v1, "BUFFER"

    invoke-direct {v0, v1, v4}, Lavf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavf;->c:Lavf;

    .line 39
    new-instance v0, Lavf;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lavf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavf;->d:Lavf;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [Lavf;

    sget-object v1, Lavf;->a:Lavf;

    aput-object v1, v0, v2

    sget-object v1, Lavf;->b:Lavf;

    aput-object v1, v0, v3

    sget-object v1, Lavf;->c:Lavf;

    aput-object v1, v0, v4

    sget-object v1, Lavf;->d:Lavf;

    aput-object v1, v0, v5

    sput-object v0, Lavf;->a:[Lavf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lavf;
    .locals 1

    .prologue
    .line 35
    const-class v0, Lavf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavf;

    return-object v0
.end method

.method public static values()[Lavf;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lavf;->a:[Lavf;

    invoke-virtual {v0}, [Lavf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavf;

    return-object v0
.end method
