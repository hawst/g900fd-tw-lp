.class public final LWw;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWx;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWx;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWs;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWy;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWa;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LWx;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LWx;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LWx;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LWx;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 46
    iput-object p1, p0, LWw;->a:LbrA;

    .line 47
    const-class v0, LWx;

    sget-object v1, LQH;->b:Ljava/lang/Class;

    .line 48
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 47
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->a:Lbsk;

    .line 50
    const-class v0, LWx;

    sget-object v1, LQH;->h:Ljava/lang/Class;

    .line 51
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 50
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->b:Lbsk;

    .line 53
    const-class v0, LWs;

    invoke-static {v0, v3}, LWw;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->c:Lbsk;

    .line 56
    const-class v0, LWy;

    invoke-static {v0, v3}, LWw;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->d:Lbsk;

    .line 59
    const-class v0, LWa;

    invoke-static {v0, v3}, LWw;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->e:Lbsk;

    .line 62
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v4

    .line 63
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->h:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 62
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->f:Lbsk;

    .line 65
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v4

    .line 66
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 65
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->g:Lbsk;

    .line 68
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v4

    .line 69
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->h:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 68
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->h:Lbsk;

    .line 71
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v4

    .line 72
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 71
    invoke-static {v0, v3}, LWw;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWw;->i:Lbsk;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 291
    sparse-switch p1, :sswitch_data_0

    .line 311
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :sswitch_0
    new-instance v0, LWs;

    invoke-direct {v0}, LWs;-><init>()V

    .line 295
    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    .line 296
    invoke-virtual {v1, v0}, LWw;->a(LWs;)V

    .line 309
    :goto_0
    return-object v0

    .line 299
    :sswitch_1
    new-instance v0, LWy;

    invoke-direct {v0}, LWy;-><init>()V

    .line 301
    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    .line 302
    invoke-virtual {v1, v0}, LWw;->a(LWy;)V

    goto :goto_0

    .line 305
    :sswitch_2
    new-instance v0, LWa;

    invoke-direct {v0}, LWa;-><init>()V

    .line 307
    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    .line 308
    invoke-virtual {v1, v0}, LWw;->a(LWa;)V

    goto :goto_0

    .line 291
    nop

    :sswitch_data_0
    .sparse-switch
        0x532 -> :sswitch_1
        0x542 -> :sswitch_0
        0x544 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 344
    sparse-switch p2, :sswitch_data_0

    .line 398
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :sswitch_0
    check-cast p1, LWr;

    .line 348
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v2, v0, LWw;->b:Lbsk;

    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 355
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 359
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 348
    invoke-virtual {p1, v2, v0, v1}, LWr;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    .line 391
    :goto_0
    return-object v0

    .line 363
    :sswitch_1
    check-cast p1, LWr;

    .line 365
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v2, v0, LWw;->a:Lbsk;

    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 372
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 376
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 365
    invoke-virtual {p1, v2, v0, v1}, LWr;->get2(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 380
    :sswitch_2
    check-cast p1, LWr;

    .line 382
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v0, v0, LWw;->f:Lbsk;

    .line 385
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 382
    invoke-virtual {p1, v0}, LWr;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 389
    :sswitch_3
    check-cast p1, LWr;

    .line 391
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v0, v0, LWw;->g:Lbsk;

    .line 394
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 391
    invoke-virtual {p1, v0}, LWr;->getLazy2(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 344
    :sswitch_data_0
    .sparse-switch
        0x3c9 -> :sswitch_3
        0x3dc -> :sswitch_2
        0x545 -> :sswitch_0
        0x547 -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 247
    const-class v0, LWa;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xac

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LWw;->a(LbuP;LbuB;)V

    .line 250
    const-class v0, LWy;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xab

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LWw;->a(LbuP;LbuB;)V

    .line 253
    const-class v0, LWs;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xaa

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LWw;->a(LbuP;LbuB;)V

    .line 256
    const-class v0, LWx;

    sget-object v1, LQH;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 257
    const-class v0, LWx;

    sget-object v1, LQH;->h:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 258
    const-class v0, LWs;

    iget-object v1, p0, LWw;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Ljava/lang/Class;Lbsk;)V

    .line 259
    const-class v0, LWy;

    iget-object v1, p0, LWw;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Ljava/lang/Class;Lbsk;)V

    .line 260
    const-class v0, LWa;

    iget-object v1, p0, LWw;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Ljava/lang/Class;Lbsk;)V

    .line 261
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->h:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 262
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 263
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->h:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 264
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LWx;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LWw;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LWw;->a(Lbuv;Lbsk;)V

    .line 265
    iget-object v0, p0, LWw;->a:Lbsk;

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    iget-object v1, v1, LWw;->c:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 267
    iget-object v0, p0, LWw;->b:Lbsk;

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOs;

    iget-object v1, v1, LOs;->j:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 269
    iget-object v0, p0, LWw;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x542

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 271
    iget-object v0, p0, LWw;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x532

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 273
    iget-object v0, p0, LWw;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x544

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 275
    iget-object v0, p0, LWw;->f:Lbsk;

    const-class v1, LWr;

    const/16 v2, 0x545

    invoke-virtual {p0, v1, v2}, LWw;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 277
    iget-object v0, p0, LWw;->g:Lbsk;

    const-class v1, LWr;

    const/16 v2, 0x547

    invoke-virtual {p0, v1, v2}, LWw;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 279
    iget-object v0, p0, LWw;->h:Lbsk;

    const-class v1, LWr;

    const/16 v2, 0x3dc

    invoke-virtual {p0, v1, v2}, LWw;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 281
    iget-object v0, p0, LWw;->i:Lbsk;

    const-class v1, LWr;

    const/16 v2, 0x3c9

    invoke-virtual {p0, v1, v2}, LWw;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 283
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 318
    packed-switch p1, :pswitch_data_0

    .line 338
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :pswitch_0
    check-cast p2, LWa;

    .line 322
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    .line 323
    invoke-virtual {v0, p2}, LWw;->a(LWa;)V

    .line 340
    :goto_0
    return-void

    .line 326
    :pswitch_1
    check-cast p2, LWy;

    .line 328
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    .line 329
    invoke-virtual {v0, p2}, LWw;->a(LWy;)V

    goto :goto_0

    .line 332
    :pswitch_2
    check-cast p2, LWs;

    .line 334
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    .line 335
    invoke-virtual {v0, p2}, LWw;->a(LWs;)V

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0xaa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(LWa;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->o:Lbsk;

    .line 83
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->o:Lbsk;

    .line 81
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadL;

    iput-object v0, p1, LWa;->a:LadL;

    .line 87
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 90
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 88
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, LWa;->a:LSF;

    .line 94
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 97
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 95
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p1, LWa;->a:LaGg;

    .line 101
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->B:Lbsk;

    .line 104
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->B:Lbsk;

    .line 102
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p1, LWa;->a:Lamn;

    .line 108
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 111
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 109
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, LWa;->a:LQr;

    .line 115
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->al:Lbsk;

    .line 118
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->al:Lbsk;

    .line 116
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LWa;->a:Laja;

    .line 122
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 125
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 123
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, LWa;->a:LaKR;

    .line 129
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 132
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 130
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LWa;->a:LtK;

    .line 136
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:La;

    iget-object v0, v0, La;->a:Lbsk;

    .line 139
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 137
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p1, LWa;->a:Landroid/app/Activity;

    .line 143
    return-void
.end method

.method public a(LWs;)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->o:Lbsk;

    .line 175
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->o:Lbsk;

    .line 173
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadL;

    iput-object v0, p1, LWs;->a:LadL;

    .line 179
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->aa:Lbsk;

    .line 182
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 180
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    iput-object v0, p1, LWs;->a:LsC;

    .line 186
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 189
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 187
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, LWs;->a:LSF;

    .line 193
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->B:Lbsk;

    .line 196
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->B:Lbsk;

    .line 194
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p1, LWs;->a:Lamn;

    .line 200
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 203
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 201
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, LWs;->a:LajO;

    .line 207
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LarZ;

    iget-object v0, v0, LarZ;->s:Lbsk;

    .line 210
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LarZ;

    iget-object v1, v1, LarZ;->s:Lbsk;

    .line 208
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latd;

    iput-object v0, p1, LWs;->a:Latd;

    .line 214
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 217
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 215
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, LWs;->a:LQr;

    .line 221
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 224
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 222
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, LWs;->a:LqK;

    .line 228
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 231
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 229
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LWs;->a:LtK;

    .line 235
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:La;

    iget-object v0, v0, La;->a:Lbsk;

    .line 238
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 236
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p1, LWs;->a:Landroid/app/Activity;

    .line 242
    return-void
.end method

.method public a(LWy;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v0, v0, LWw;->a:Lbsk;

    .line 150
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    iget-object v1, v1, LWw;->a:Lbsk;

    .line 148
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    iput-object v0, p1, LWy;->b:LWx;

    .line 154
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v0, v0, LWw;->b:Lbsk;

    .line 157
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    iget-object v1, v1, LWw;->b:Lbsk;

    .line 155
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    iput-object v0, p1, LWy;->a:LWx;

    .line 161
    iget-object v0, p0, LWw;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWw;

    iget-object v0, v0, LWw;->e:Lbsk;

    .line 164
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWw;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LWw;

    iget-object v1, v1, LWw;->e:Lbsk;

    .line 162
    invoke-static {v0, v1}, LWw;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWa;

    iput-object v0, p1, LWy;->a:LWa;

    .line 168
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method
