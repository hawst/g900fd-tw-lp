.class public final enum LakY;
.super Ljava/lang/Enum;
.source "DiskCacheDir.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LakY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LakY;

.field private static final synthetic a:[LakY;

.field public static final enum b:LakY;

.field public static final enum c:LakY;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    new-instance v0, LakY;

    const-string v1, "FETCHING"

    const-string v2, "fetching"

    invoke-direct {v0, v1, v3, v2}, LakY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LakY;->a:LakY;

    .line 36
    new-instance v0, LakY;

    const-string v1, "SKETCHY_IMAGES"

    const-string v2, "SketchyImages"

    invoke-direct {v0, v1, v4, v2}, LakY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LakY;->b:LakY;

    .line 37
    new-instance v0, LakY;

    const-string v1, "CACHED_BLOB_FILES"

    const-string v2, "CachedBlobFiles"

    invoke-direct {v0, v1, v5, v2}, LakY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LakY;->c:LakY;

    .line 34
    const/4 v0, 0x3

    new-array v0, v0, [LakY;

    sget-object v1, LakY;->a:LakY;

    aput-object v1, v0, v3

    sget-object v1, LakY;->b:LakY;

    aput-object v1, v0, v4

    sget-object v1, LakY;->c:LakY;

    aput-object v1, v0, v5

    sput-object v0, LakY;->a:[LakY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    const-string v0, "\\w+"

    invoke-static {v0, p3}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 43
    iput-object p3, p0, LakY;->a:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public static a(Ljava/lang/String;)LakY;
    .locals 5

    .prologue
    .line 48
    invoke-static {}, LakY;->values()[LakY;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 49
    iget-object v4, v0, LakY;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 53
    :goto_1
    return-object v0

    .line 48
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LakY;
    .locals 1

    .prologue
    .line 34
    const-class v0, LakY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LakY;

    return-object v0
.end method

.method public static values()[LakY;
    .locals 1

    .prologue
    .line 34
    sget-object v0, LakY;->a:[LakY;

    invoke-virtual {v0}, [LakY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LakY;

    return-object v0
.end method
