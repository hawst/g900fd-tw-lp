.class public final Lafc;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaeV;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LafA;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lafb;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LafB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, Lafc;->a:LbrA;

    .line 42
    const-class v0, LaeV;

    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lafc;->a:Lbsk;

    .line 45
    const-class v0, LafA;

    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lafc;->b:Lbsk;

    .line 48
    const-class v0, Lafb;

    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lafc;->c:Lbsk;

    .line 51
    const-class v0, LafB;

    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lafc;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 81
    packed-switch p1, :pswitch_data_0

    .line 183
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_1
    new-instance v4, LaeV;

    iget-object v0, p0, Lafc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 86
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lafc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 84
    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iget-object v1, p0, Lafc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 92
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 90
    invoke-static {v1, v2}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->I:Lbsk;

    .line 98
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lafc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->I:Lbsk;

    .line 96
    invoke-static {v2, v3}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lafz;

    iget-object v3, p0, Lafc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->t:Lbsk;

    .line 104
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Lafc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->t:Lbsk;

    .line 102
    invoke-static {v3, v5}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laja;

    invoke-direct {v4, v0, v1, v2, v3}, LaeV;-><init>(LtK;LQr;Lafz;Laja;)V

    move-object v0, v4

    .line 181
    :goto_0
    return-object v0

    .line 111
    :pswitch_2
    new-instance v0, LafA;

    iget-object v1, p0, Lafc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lafc;

    iget-object v1, v1, Lafc;->d:Lbsk;

    .line 114
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lafc;

    iget-object v2, v2, Lafc;->d:Lbsk;

    .line 112
    invoke-static {v1, v2}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LafB;

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->B:Lbsk;

    .line 120
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lafc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->B:Lbsk;

    .line 118
    invoke-static {v2, v3}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LagS;

    iget-object v3, p0, Lafc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->J:Lbsk;

    .line 126
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lafc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LagN;

    iget-object v4, v4, LagN;->J:Lbsk;

    .line 124
    invoke-static {v3, v4}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lagl;

    iget-object v4, p0, Lafc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LagN;

    iget-object v4, v4, LagN;->G:Lbsk;

    .line 132
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lafc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LagN;

    iget-object v5, v5, LagN;->G:Lbsk;

    .line 130
    invoke-static {v4, v5}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LagE;

    iget-object v5, p0, Lafc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->k:Lbsk;

    .line 138
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lafc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->k:Lbsk;

    .line 136
    invoke-static {v5, v6}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaGF;

    iget-object v6, p0, Lafc;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LSK;

    iget-object v6, v6, LSK;->b:Lbsk;

    .line 144
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, Lafc;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LSK;

    iget-object v7, v7, LSK;->b:Lbsk;

    .line 142
    invoke-static {v6, v7}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LSF;

    iget-object v7, p0, Lafc;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaGH;

    iget-object v7, v7, LaGH;->l:Lbsk;

    .line 150
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, Lafc;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaGH;

    iget-object v8, v8, LaGH;->l:Lbsk;

    .line 148
    invoke-static {v7, v8}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LaGM;

    invoke-direct/range {v0 .. v7}, LafA;-><init>(LafB;LagS;Lagl;LagE;LaGF;LSF;LaGM;)V

    goto/16 :goto_0

    .line 157
    :pswitch_3
    new-instance v0, Lafb;

    invoke-direct {v0}, Lafb;-><init>()V

    goto/16 :goto_0

    .line 161
    :pswitch_4
    new-instance v3, LafB;

    iget-object v0, p0, Lafc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 164
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lafc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 162
    invoke-static {v0, v1}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, Lafc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->k:Lbsk;

    .line 170
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->k:Lbsk;

    .line 168
    invoke-static {v1, v2}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGF;

    iget-object v2, p0, Lafc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->C:Lbsk;

    .line 176
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Lafc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LagN;

    iget-object v4, v4, LagN;->C:Lbsk;

    .line 174
    invoke-static {v2, v4}, Lafc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lafa;

    invoke-direct {v3, v0, v1, v2}, LafB;-><init>(LaGM;LaGF;Lafa;)V

    move-object v0, v3

    .line 181
    goto/16 :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1a3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 198
    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 61
    const-class v0, LaeV;

    iget-object v1, p0, Lafc;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lafc;->a(Ljava/lang/Class;Lbsk;)V

    .line 62
    const-class v0, LafA;

    iget-object v1, p0, Lafc;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lafc;->a(Ljava/lang/Class;Lbsk;)V

    .line 63
    const-class v0, Lafb;

    iget-object v1, p0, Lafc;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lafc;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    const-class v0, LafB;

    iget-object v1, p0, Lafc;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lafc;->a(Ljava/lang/Class;Lbsk;)V

    .line 65
    iget-object v0, p0, Lafc;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1a3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, Lafc;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1a4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, Lafc;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1a9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, Lafc;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1a5

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 190
    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
