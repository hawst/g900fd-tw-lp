.class public final LauL;
.super Ljava/lang/Object;
.source "Dimensions.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/client/Dimensions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/viewer/client/Dimensions;-><init>(II)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 1

    .prologue
    .line 57
    new-array v0, p1, [Lcom/google/android/apps/viewer/client/Dimensions;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, LauL;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, LauL;->a(I)[Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    return-object v0
.end method
