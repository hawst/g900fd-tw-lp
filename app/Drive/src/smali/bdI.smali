.class public abstract LbdI;
.super Ljava/lang/Object;
.source "AbstractGoogleClient.java"


# instance fields
.field applicationName:Ljava/lang/String;

.field googleClientRequestInitializer:LbdM;

.field httpRequestInitializer:Lbek;

.field final objectParser:LbfL;

.field rootUrl:Ljava/lang/String;

.field servicePath:Ljava/lang/String;

.field suppressPatternChecks:Z

.field suppressRequiredParameterChecks:Z

.field final transport:Lbeq;


# direct methods
.method protected constructor <init>(Lbeq;Ljava/lang/String;Ljava/lang/String;LbfL;Lbek;)V
    .locals 1

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeq;

    iput-object v0, p0, LbdI;->transport:Lbeq;

    .line 334
    iput-object p4, p0, LbdI;->objectParser:LbfL;

    .line 335
    invoke-virtual {p0, p2}, LbdI;->setRootUrl(Ljava/lang/String;)LbdI;

    .line 336
    invoke-virtual {p0, p3}, LbdI;->setServicePath(Ljava/lang/String;)LbdI;

    .line 337
    iput-object p5, p0, LbdI;->httpRequestInitializer:Lbek;

    .line 338
    return-void
.end method


# virtual methods
.method public abstract build()LbdH;
.end method

.method public final getApplicationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, LbdI;->applicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGoogleClientRequestInitializer()LbdM;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, LbdI;->googleClientRequestInitializer:LbdM;

    return-object v0
.end method

.method public final getHttpRequestInitializer()Lbek;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, LbdI;->httpRequestInitializer:Lbek;

    return-object v0
.end method

.method public getObjectParser()LbfL;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, LbdI;->objectParser:LbfL;

    return-object v0
.end method

.method public final getRootUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, LbdI;->rootUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getServicePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, LbdI;->servicePath:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuppressPatternChecks()Z
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, LbdI;->suppressPatternChecks:Z

    return v0
.end method

.method public final getSuppressRequiredParameterChecks()Z
    .locals 1

    .prologue
    .line 512
    iget-boolean v0, p0, LbdI;->suppressRequiredParameterChecks:Z

    return v0
.end method

.method public final getTransport()Lbeq;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, LbdI;->transport:Lbeq;

    return-object v0
.end method

.method public setApplicationName(Ljava/lang/String;)LbdI;
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, LbdI;->applicationName:Ljava/lang/String;

    .line 481
    return-object p0
.end method

.method public setGoogleClientRequestInitializer(LbdM;)LbdI;
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, LbdI;->googleClientRequestInitializer:LbdM;

    .line 441
    return-object p0
.end method

.method public setHttpRequestInitializer(Lbek;)LbdI;
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, LbdI;->httpRequestInitializer:Lbek;

    .line 459
    return-object p0
.end method

.method public setRootUrl(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 385
    invoke-static {p1}, LbdH;->normalizeRootUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbdI;->rootUrl:Ljava/lang/String;

    .line 386
    return-object p0
.end method

.method public setServicePath(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 421
    invoke-static {p1}, LbdH;->normalizeServicePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbdI;->servicePath:Ljava/lang/String;

    .line 422
    return-object p0
.end method

.method public setSuppressAllChecks(Z)LbdI;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 545
    invoke-virtual {p0, v1}, LbdI;->setSuppressPatternChecks(Z)LbdI;

    move-result-object v0

    invoke-virtual {v0, v1}, LbdI;->setSuppressRequiredParameterChecks(Z)LbdI;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressPatternChecks(Z)LbdI;
    .locals 0

    .prologue
    .line 502
    iput-boolean p1, p0, LbdI;->suppressPatternChecks:Z

    .line 503
    return-object p0
.end method

.method public setSuppressRequiredParameterChecks(Z)LbdI;
    .locals 0

    .prologue
    .line 530
    iput-boolean p1, p0, LbdI;->suppressRequiredParameterChecks:Z

    .line 531
    return-object p0
.end method
