.class public Lapf;
.super Ljava/lang/Object;
.source "ThumbnailFetcher.java"


# instance fields
.field private final a:I

.field private a:J

.field private final a:LZS;

.field private final a:LakR;

.field private final a:LaoJ;

.field private final a:LaoX;

.field private final a:LaoY;

.field private final a:Laom;

.field private final a:Lapl;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final a:LtK;

.field private final b:I

.field private final b:LZS;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(LQr;LakR;LaoJ;Laom;Lapl;LaoY;LaoG;LZS;LZS;LtK;)V
    .locals 3
    .param p2    # LakR;
        .annotation runtime LaoC;
        .end annotation
    .end param
    .param p8    # LZS;
        .annotation runtime LaoD;
        .end annotation
    .end param
    .param p9    # LZS;
        .annotation runtime LaoE;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/16 v0, 0x32

    iput v0, p0, Lapf;->a:I

    .line 107
    const/4 v0, 0x4

    iput v0, p0, Lapf;->b:I

    .line 108
    iput v2, p0, Lapf;->c:I

    .line 109
    iput v2, p0, Lapf;->d:I

    .line 110
    const-wide/32 v0, 0x500000

    iput-wide v0, p0, Lapf;->a:J

    .line 112
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lapf;->a:Ljava/util/List;

    .line 113
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lapf;->b:Ljava/util/List;

    .line 141
    const-string v0, "RequestPool"

    const/16 v1, 0x3e8

    invoke-direct {p0, v0, v2, v1}, Lapf;->a(Ljava/lang/String;II)LaoX;

    move-result-object v0

    iput-object v0, p0, Lapf;->a:LaoX;

    .line 142
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoJ;

    iput-object v0, p0, Lapf;->a:LaoJ;

    .line 143
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laom;

    iput-object v0, p0, Lapf;->a:Laom;

    .line 144
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapl;

    iput-object v0, p0, Lapf;->a:Lapl;

    .line 145
    iput-object p6, p0, Lapf;->a:LaoY;

    .line 146
    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, Lapf;->a:LtK;

    .line 148
    const-string v0, "thumbnailBitmapCacheExpirationTimeSeconds"

    const/16 v1, 0x12c

    invoke-interface {p1, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lapf;->e:I

    .line 152
    const-string v0, "maxNumberOfThumbnailDownloadRetries"

    const/4 v1, 0x3

    invoke-interface {p1, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lapf;->f:I

    .line 156
    iput-object p9, p0, Lapf;->a:LZS;

    .line 157
    iput-object p8, p0, Lapf;->b:LZS;

    .line 158
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakR;

    iput-object v0, p0, Lapf;->a:LakR;

    .line 159
    iget-object v0, p0, Lapf;->a:Ljava/util/List;

    invoke-virtual {p7}, LaoG;->a()LakH;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    return-void
.end method

.method private a()LanN;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 208
    invoke-direct {p0}, Lapf;->a()Lapj;

    move-result-object v0

    .line 210
    const-string v1, "thumbnail"

    .line 212
    iget-object v2, p0, Lapf;->a:Laom;

    iget-object v3, p0, Lapf;->b:LZS;

    .line 213
    invoke-virtual {v2, v0, v3}, Laom;->a(Laoo;LZS;)Laok;

    move-result-object v0

    .line 215
    const-string v2, "LocalThumbnailFetcherPool"

    const/16 v3, 0x32

    invoke-direct {p0, v2, v4, v3}, Lapf;->a(Ljava/lang/String;II)LaoX;

    move-result-object v2

    .line 218
    iget-object v3, p0, Lapf;->a:LaoJ;

    .line 219
    invoke-virtual {v3, v4, v2, v0}, LaoJ;->a(ZLaop;Laoo;)LaoH;

    move-result-object v0

    .line 221
    iget-object v2, p0, Lapf;->a:LakR;

    .line 222
    invoke-static {v2, v1, v0}, LanN;->a(LakR;Ljava/lang/String;Laoo;)LanN;

    move-result-object v0

    .line 223
    return-object v0
.end method

.method private a(Laoo;)LaoO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)",
            "LaoO;"
        }
    .end annotation

    .prologue
    .line 228
    const-string v0, "DecompressFetcherPool"

    const/4 v1, 0x1

    const/16 v2, 0x32

    invoke-direct {p0, v0, v1, v2}, Lapf;->a(Ljava/lang/String;II)LaoX;

    move-result-object v0

    .line 231
    new-instance v1, LaoO;

    iget-object v2, p0, Lapf;->a:LtK;

    iget-object v3, p0, Lapf;->a:LaoY;

    invoke-direct {v1, p1, v0, v2, v3}, LaoO;-><init>(Laoo;Laop;LtK;LaoY;)V

    return-object v1
.end method

.method private a(Ljava/lang/String;II)LaoX;
    .locals 2

    .prologue
    .line 183
    const-wide/16 v0, 0x0

    .line 184
    invoke-static {v0, v1, p3}, LakH;->a(JI)LakH;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lapf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-static {p2, p1}, Lalg;->a(ILjava/lang/String;)LbsW;

    move-result-object v1

    .line 191
    invoke-static {v1, v0}, LalE;->a(Ljava/util/concurrent/ExecutorService;Lalf;)LalE;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lapf;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    new-instance v1, LaoX;

    invoke-direct {v1, v0}, LaoX;-><init>(LalD;)V

    return-object v1
.end method

.method private a(Laoo;)Laoo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 240
    .line 241
    invoke-direct {p0, p1}, Lapf;->a(Laoo;)LaoO;

    move-result-object v0

    .line 243
    new-instance v1, LanI;

    iget-object v2, p0, Lapf;->a:LaoX;

    invoke-direct {v1, v0, v2}, LanI;-><init>(Laoo;Laop;)V

    return-object v1
.end method

.method private a()Lapd;
    .locals 7

    .prologue
    .line 249
    invoke-direct {p0}, Lapf;->a()LanN;

    move-result-object v3

    .line 251
    new-instance v0, LanY;

    iget v1, p0, Lapf;->f:I

    iget-object v2, p0, Lapf;->a:LZS;

    invoke-direct {v0, v3, v1, v2}, LanY;-><init>(Laoo;ILZS;)V

    .line 255
    invoke-direct {p0, v0}, Lapf;->a(Laoo;)Laoo;

    move-result-object v2

    .line 258
    invoke-direct {p0}, Lapf;->a()LbjG;

    move-result-object v1

    .line 259
    new-instance v0, Lapd;

    iget-object v4, p0, Lapf;->b:Ljava/util/List;

    iget-object v5, p0, Lapf;->a:Ljava/util/List;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lapd;-><init>(LbjG;Laoo;LanN;Ljava/util/List;Ljava/util/List;Lape;)V

    .line 265
    return-object v0
.end method

.method private a()Lapj;
    .locals 5

    .prologue
    .line 198
    const-string v0, "UriFetcherPool"

    const/4 v1, 0x4

    const/16 v2, 0x32

    .line 199
    invoke-direct {p0, v0, v1, v2}, Lapf;->a(Ljava/lang/String;II)LaoX;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lapf;->a:Lapl;

    const/4 v2, 0x1

    iget-object v3, p0, Lapf;->b:LZS;

    iget-object v4, p0, Lapf;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3, v4}, Lapl;->a(Laop;ZLZS;Ljava/lang/String;)Lapj;

    move-result-object v0

    .line 204
    return-object v0
.end method

.method private a()LbjG;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjG",
            "<",
            "Laon;",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 171
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    iget-wide v2, p0, Lapf;->a:J

    invoke-virtual {v0, v2, v3}, LbjG;->b(J)LbjG;

    move-result-object v0

    iget v1, p0, Lapf;->e:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 172
    invoke-virtual {v0, v2, v3, v1}, LbjG;->b(JLjava/util/concurrent/TimeUnit;)LbjG;

    move-result-object v0

    new-instance v1, Lapg;

    invoke-direct {v1, p0}, Lapg;-><init>(Lapf;)V

    .line 173
    invoke-virtual {v0, v1}, LbjG;->a(Lbln;)LbjG;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(JLjava/lang/String;)Lapd;
    .locals 3

    .prologue
    .line 163
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 164
    iput-wide p1, p0, Lapf;->a:J

    .line 165
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lapf;->a:Ljava/lang/String;

    .line 166
    invoke-direct {p0}, Lapf;->a()Lapd;

    move-result-object v0

    return-object v0

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
