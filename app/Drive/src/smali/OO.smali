.class abstract LOO;
.super Landroid/os/AsyncTask;
.source "PhotosPreferencesInstaller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TT;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/preference/CheckBoxPreference;

.field private final a:Ljava/lang/String;

.field final synthetic b:LOJ;


# direct methods
.method constructor <init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    iput-object p1, p0, LOO;->b:LOJ;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 51
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, LOO;->a:Landroid/preference/CheckBoxPreference;

    .line 52
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LOO;->a:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, LOO;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 64
    iget-object v0, p0, LOO;->a:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 65
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LOO;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, LOO;->a:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 58
    iget-object v0, p0, LOO;->a:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, LOO;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 59
    return-void
.end method
