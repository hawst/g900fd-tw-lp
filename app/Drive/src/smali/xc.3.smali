.class public final Lxc;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final account_address:I = 0x7f0c0075

.field public static final account_badge:I = 0x7f0c0079

.field public static final account_display_name:I = 0x7f0c007c

.field public static final account_email:I = 0x7f0c007e

.field public static final account_info_banner:I = 0x7f0c0078

.field public static final account_list_button:I = 0x7f0c01ef

.field public static final account_middle_padding:I = 0x7f0c007d

.field public static final account_name:I = 0x7f0c0088

.field public static final account_spinner:I = 0x7f0c01b1

.field public static final account_switcher:I = 0x7f0c0181

.field public static final account_text:I = 0x7f0c01eb

.field public static final account_top_padding:I = 0x7f0c007b

.field public static final accounts_list:I = 0x7f0c0076

.field public static final accounts_wrapper:I = 0x7f0c0084

.field public static final action:I = 0x7f0c01bf

.field public static final action_add_people:I = 0x7f0c027e

.field public static final action_add_to_drive:I = 0x7f0c0277

.field public static final action_bar:I = 0x7f0c0066

.field public static final action_bar_activity_content:I = 0x7f0c000e

.field public static final action_bar_container:I = 0x7f0c0065

.field public static final action_bar_popup:I = 0x7f0c014d

.field public static final action_bar_root:I = 0x7f0c0061

.field public static final action_bar_spinner:I = 0x7f0c000d

.field public static final action_bar_subtitle:I = 0x7f0c0054

.field public static final action_bar_title:I = 0x7f0c0053

.field public static final action_card_container:I = 0x7f0c008a

.field public static final action_card_overflow_icon:I = 0x7f0c008e

.field public static final action_card_popup_anchor:I = 0x7f0c008b

.field public static final action_context_bar:I = 0x7f0c0067

.field public static final action_details:I = 0x7f0c027a

.field public static final action_download:I = 0x7f0c027c

.field public static final action_edit:I = 0x7f0c0276

.field public static final action_find:I = 0x7f0c0275

.field public static final action_menu_divider:I = 0x7f0c0010

.field public static final action_menu_presenter:I = 0x7f0c0011

.field public static final action_mode_bar:I = 0x7f0c0063

.field public static final action_mode_bar_stub:I = 0x7f0c0062

.field public static final action_mode_close_button:I = 0x7f0c0055

.field public static final action_print:I = 0x7f0c0279

.field public static final action_send:I = 0x7f0c0278

.field public static final action_share:I = 0x7f0c027b

.field public static final action_share_link:I = 0x7f0c027d

.field public static final activity_chooser_view_content:I = 0x7f0c0056

.field public static final add_account_container:I = 0x7f0c0086

.field public static final add_account_text:I = 0x7f0c0090

.field public static final add_collaborator_icon:I = 0x7f0c0100

.field public static final add_collaborators:I = 0x7f0c00e6

.field public static final add_collaborators_button:I = 0x7f0c020e

.field public static final always:I = 0x7f0c0047

.field public static final app_root:I = 0x7f0c015b

.field public static final avatar:I = 0x7f0c0074

.field public static final avatar_recents_one:I = 0x7f0c0000

.field public static final avatar_recents_two:I = 0x7f0c00a6

.field public static final back_button:I = 0x7f0c0089

.field public static final background:I = 0x7f0c0108

.field public static final banner_account_name_and_email:I = 0x7f0c007a

.field public static final beginning:I = 0x7f0c004e

.field public static final blurb:I = 0x7f0c0238

.field public static final body:I = 0x7f0c01d0

.field public static final book_now:I = 0x7f0c0037

.field public static final bottom:I = 0x7f0c0051

.field public static final breadcrumb_arrow:I = 0x7f0c01a2

.field public static final breadcrumb_text:I = 0x7f0c01a1

.field public static final btn_cancel:I = 0x7f0c016a

.field public static final btn_ok:I = 0x7f0c016b

.field public static final buttons:I = 0x7f0c011c

.field public static final buyButton:I = 0x7f0c0033

.field public static final buy_now:I = 0x7f0c0038

.field public static final buy_with_google:I = 0x7f0c0039

.field public static final cancel_button:I = 0x7f0c0077

.field public static final capture_layout:I = 0x7f0c0156

.field public static final card_title:I = 0x7f0c01f2

.field public static final checkbox:I = 0x7f0c005e

.field public static final choice_create_background:I = 0x7f0c00b0

.field public static final choice_create_doc:I = 0x7f0c00b5

.field public static final choice_create_folder:I = 0x7f0c00b2

.field public static final choice_create_panel:I = 0x7f0c00b1

.field public static final choice_create_scan:I = 0x7f0c00b4

.field public static final choice_create_sheet:I = 0x7f0c00b6

.field public static final choice_create_slide:I = 0x7f0c00b7

.field public static final choice_create_upload:I = 0x7f0c00b3

.field public static final classic:I = 0x7f0c003a

.field public static final clickable_area:I = 0x7f0c007f

.field public static final close_button:I = 0x7f0c0008

.field public static final collapseActionView:I = 0x7f0c0048

.field public static final collection_path:I = 0x7f0c01a0

.field public static final collection_scroller:I = 0x7f0c019f

.field public static final contact:I = 0x7f0c001d

.field public static final content_text:I = 0x7f0c01bc

.field public static final control_button:I = 0x7f0c019a

.field public static final controls:I = 0x7f0c0231

.field public static final cover_photo:I = 0x7f0c01e9

.field public static final create_bar:I = 0x7f0c00ac

.field public static final create_bar_new:I = 0x7f0c00af

.field public static final create_bar_new_from_camera:I = 0x7f0c00ae

.field public static final create_bar_upload:I = 0x7f0c00ad

.field public static final create_contact_button:I = 0x7f0c020f

.field public static final created:I = 0x7f0c00d5

.field public static final created_row:I = 0x7f0c00d4

.field public static final crossfade_avatar_recents_one:I = 0x7f0c00a4

.field public static final crossfade_avatar_recents_two:I = 0x7f0c00a5

.field public static final current_slide_panel:I = 0x7f0c01d8

.field public static final custom_layer:I = 0x7f0c021e

.field public static final decor_content_parent:I = 0x7f0c0064

.field public static final default_activity_button:I = 0x7f0c0059

.field public static final demote_common_words:I = 0x7f0c0018

.field public static final demote_rfc822_hostnames:I = 0x7f0c0019

.field public static final detail_card_preview:I = 0x7f0c00dc

.field public static final detail_drawer_fragment:I = 0x7f0c0106

.field public static final detail_fragment_close_button:I = 0x7f0c00ee

.field public static final detail_fragment_drawer:I = 0x7f0c00eb

.field public static final detail_fragment_header:I = 0x7f0c0107

.field public static final detail_fragment_listview:I = 0x7f0c010b

.field public static final detail_fragment_scrollview:I = 0x7f0c00ec

.field public static final detail_panel_container:I = 0x7f0c0105

.field public static final details_triangle:I = 0x7f0c0125

.field public static final dialog:I = 0x7f0c004c

.field public static final dialog_view:I = 0x7f0c01c8

.field public static final disableHome:I = 0x7f0c0041

.field public static final divider:I = 0x7f0c0135

.field public static final doc_entry:I = 0x7f0c0123

.field public static final doc_entry_container:I = 0x7f0c0122

.field public static final doc_entry_root:I = 0x7f0c0006

.field public static final doc_icon:I = 0x7f0c00f0

.field public static final doc_list_body:I = 0x7f0c0139

.field public static final doc_list_fragment:I = 0x7f0c021c

.field public static final doc_list_row_group_entry_tag:I = 0x7f0c0004

.field public static final doc_list_row_view_type:I = 0x7f0c0005

.field public static final doc_list_snapshot:I = 0x7f0c0219

.field public static final doc_list_syncing_spinner:I = 0x7f0c0144

.field public static final doc_list_view:I = 0x7f0c013a

.field public static final doclist_create_button:I = 0x7f0c01b4

.field public static final doclist_sticky_header_shadow:I = 0x7f0c0117

.field public static final document_opener_option:I = 0x7f0c01b5

.field public static final document_pager:I = 0x7f0c01c1

.field public static final done:I = 0x7f0c0235

.field public static final drag_knob:I = 0x7f0c0149

.field public static final drag_knob_container:I = 0x7f0c01d6

.field public static final drag_knob_pic:I = 0x7f0c014b

.field public static final drag_shadow_double:I = 0x7f0c0204

.field public static final drop_to_current_folder_highlight:I = 0x7f0c0202

.field public static final dropdown:I = 0x7f0c004d

.field public static final ds_actionbar_compat:I = 0x7f0c0150

.field public static final ds_add_button:I = 0x7f0c0162

.field public static final ds_camera_layout:I = 0x7f0c0157

.field public static final ds_camera_preview:I = 0x7f0c0158

.field public static final ds_capture_buttons:I = 0x7f0c0151

.field public static final ds_capture_grid:I = 0x7f0c015a

.field public static final ds_document_editor:I = 0x7f0c0167

.field public static final ds_editor_buttons:I = 0x7f0c0161

.field public static final ds_finish_button:I = 0x7f0c0164

.field public static final ds_flash_button:I = 0x7f0c0155

.field public static final ds_main_layout:I = 0x7f0c0160

.field public static final ds_menu_delete_page:I = 0x7f0c0245

.field public static final ds_menu_edit_page:I = 0x7f0c0243

.field public static final ds_menu_enhance:I = 0x7f0c0244

.field public static final ds_menu_enhance_bw:I = 0x7f0c0249

.field public static final ds_menu_enhance_color:I = 0x7f0c024a

.field public static final ds_menu_enhance_color_drawing:I = 0x7f0c024b

.field public static final ds_menu_enhance_none:I = 0x7f0c0248

.field public static final ds_menu_rename_scan:I = 0x7f0c0246

.field public static final ds_menu_settings:I = 0x7f0c0247

.field public static final ds_nonstop_debug_view:I = 0x7f0c015e

.field public static final ds_preview_overlay:I = 0x7f0c0159

.field public static final ds_progess_bar:I = 0x7f0c015d

.field public static final ds_quad_editor:I = 0x7f0c0166

.field public static final ds_retake_button:I = 0x7f0c0163

.field public static final ds_review_layout:I = 0x7f0c015f

.field public static final ds_shutter_button_dark:I = 0x7f0c0153

.field public static final ds_shutter_button_layout:I = 0x7f0c0152

.field public static final ds_shutter_button_lit:I = 0x7f0c0154

.field public static final dummy:I = 0x7f0c0220

.field public static final edit_query:I = 0x7f0c0068

.field public static final editor_state_view_full_slide:I = 0x7f0c00aa

.field public static final editor_state_view_state:I = 0x7f0c00a9

.field public static final email:I = 0x7f0c001e

.field public static final email_address:I = 0x7f0c00e9

.field public static final empty_group_title:I = 0x7f0c012c

.field public static final empty_list_icon:I = 0x7f0c0141

.field public static final empty_list_message:I = 0x7f0c0142

.field public static final empty_list_message_details:I = 0x7f0c0143

.field public static final empty_sharing_list:I = 0x7f0c020b

.field public static final end:I = 0x7f0c004f

.field public static final error_message:I = 0x7f0c00e5

.field public static final error_status_message:I = 0x7f0c01b9

.field public static final expand_activities_button:I = 0x7f0c0057

.field public static final expanded_menu:I = 0x7f0c005d

.field public static final file_icon_frame:I = 0x7f0c016d

.field public static final file_icon_image:I = 0x7f0c016e

.field public static final file_icon_name:I = 0x7f0c016f

.field public static final film_scroll_view:I = 0x7f0c0176

.field public static final film_view:I = 0x7f0c0177

.field public static final find_next_btn:I = 0x7f0c017a

.field public static final find_prev_btn:I = 0x7f0c0179

.field public static final find_query_box:I = 0x7f0c0178

.field public static final first_label:I = 0x7f0c01b7

.field public static final floating_create_bar:I = 0x7f0c013d

.field public static final floating_handle_all:I = 0x7f0c0203

.field public static final folder_name:I = 0x7f0c014e

.field public static final folder_type_icon:I = 0x7f0c0114

.field public static final general_info_panel:I = 0x7f0c00f8

.field public static final general_info_title:I = 0x7f0c00f9

.field public static final gif:I = 0x7f0c017b

.field public static final grayscale:I = 0x7f0c003b

.field public static final grid:I = 0x7f0c003d

.field public static final grid_slide_picker_container:I = 0x7f0c01d7

.field public static final grid_view:I = 0x7f0c01d1

.field public static final group_icon:I = 0x7f0c01a6

.field public static final group_order:I = 0x7f0c0118

.field public static final group_title:I = 0x7f0c0116

.field public static final group_title_container:I = 0x7f0c0115

.field public static final heading:I = 0x7f0c0237

.field public static final highlight:I = 0x7f0c014f

.field public static final history_icon:I = 0x7f0c01e6

.field public static final holo_dark:I = 0x7f0c002e

.field public static final holo_light:I = 0x7f0c002f

.field public static final home:I = 0x7f0c000b

.field public static final homeAsUp:I = 0x7f0c0042

.field public static final home_screen_list:I = 0x7f0c0183

.field public static final host:I = 0x7f0c0081

.field public static final html:I = 0x7f0c0014

.field public static final hybrid:I = 0x7f0c0029

.field public static final icon:I = 0x7f0c005b

.field public static final iconView:I = 0x7f0c00b9

.field public static final icon_new:I = 0x7f0c01c0

.field public static final icon_open_app_image:I = 0x7f0c01ac

.field public static final icon_uri:I = 0x7f0c0020

.field public static final ifRoom:I = 0x7f0c0049

.field public static final image:I = 0x7f0c0058

.field public static final image_container:I = 0x7f0c0171

.field public static final image_viewer:I = 0x7f0c0172

.field public static final images:I = 0x7f0c0236

.field public static final imageview:I = 0x7f0c017c

.field public static final indicators:I = 0x7f0c0233

.field public static final information_bar:I = 0x7f0c019b

.field public static final instant_message:I = 0x7f0c001f

.field public static final intent_action:I = 0x7f0c0021

.field public static final intent_activity:I = 0x7f0c0022

.field public static final intent_data:I = 0x7f0c0023

.field public static final intent_data_id:I = 0x7f0c0024

.field public static final intent_extra_data:I = 0x7f0c0025

.field public static final internal_release_dialog_line_1:I = 0x7f0c018f

.field public static final internal_release_dialog_line_2:I = 0x7f0c0190

.field public static final internal_release_dialog_ok:I = 0x7f0c0191

.field public static final internal_release_dialog_title:I = 0x7f0c018d

.field public static final internal_release_logo:I = 0x7f0c018e

.field public static final item_name:I = 0x7f0c01b8

.field public static final label:I = 0x7f0c010c

.field public static final large_icon_uri:I = 0x7f0c0026

.field public static final last_modified:I = 0x7f0c00fa

.field public static final last_modified_by_me:I = 0x7f0c00fb

.field public static final last_opened_by_me:I = 0x7f0c00fc

.field public static final left_drop_zone:I = 0x7f0c0200

.field public static final linear_layout_list_view_root_layout:I = 0x7f0c0192

.field public static final link_sharing:I = 0x7f0c00e1

.field public static final link_sharing_access:I = 0x7f0c00e3

.field public static final link_sharing_role:I = 0x7f0c00e4

.field public static final link_sharing_status:I = 0x7f0c00e2

.field public static final list:I = 0x7f0c003e

.field public static final listMode:I = 0x7f0c003f

.field public static final list_item:I = 0x7f0c005a

.field public static final list_slide_picker_container:I = 0x7f0c01e2

.field public static final list_view:I = 0x7f0c0145

.field public static final list_view_frame:I = 0x7f0c0138

.field public static final list_view_refresh_frame:I = 0x7f0c0137

.field public static final loadingView:I = 0x7f0c00bc

.field public static final loading_bar:I = 0x7f0c0199

.field public static final loading_sharing_list:I = 0x7f0c020c

.field public static final loading_spinner:I = 0x7f0c00ab

.field public static final location:I = 0x7f0c00d3

.field public static final location_row:I = 0x7f0c00d2

.field public static final main_body:I = 0x7f0c011b

.field public static final main_container:I = 0x7f0c01e4

.field public static final main_layout:I = 0x7f0c015c

.field public static final manage_accounts_icon:I = 0x7f0c008f

.field public static final manage_accounts_text:I = 0x7f0c0193

.field public static final match_global_nicknames:I = 0x7f0c001a

.field public static final match_parent:I = 0x7f0c0035

.field public static final media_route_control_frame:I = 0x7f0c0197

.field public static final media_route_disconnect_button:I = 0x7f0c0198

.field public static final media_route_list:I = 0x7f0c0194

.field public static final media_route_menu_item:I = 0x7f0c027f

.field public static final media_route_volume_layout:I = 0x7f0c0195

.field public static final media_route_volume_slider:I = 0x7f0c0196

.field public static final mediacontroller_progress:I = 0x7f0c019d

.field public static final menu_account_settings:I = 0x7f0c024c

.field public static final menu_add_new_office_doc:I = 0x7f0c0268

.field public static final menu_body:I = 0x7f0c0182

.field public static final menu_clear_search:I = 0x7f0c025f

.field public static final menu_create_new_doc:I = 0x7f0c024e

.field public static final menu_create_shortcut:I = 0x7f0c025a

.field public static final menu_delete:I = 0x7f0c025c

.field public static final menu_delete_forever:I = 0x7f0c025e

.field public static final menu_download:I = 0x7f0c0259

.field public static final menu_dump_database:I = 0x7f0c026c

.field public static final menu_edit_icon:I = 0x7f0c026f

.field public static final menu_filter_by:I = 0x7f0c0263

.field public static final menu_full_screen:I = 0x7f0c0271

.field public static final menu_grid_mode:I = 0x7f0c0261

.field public static final menu_help:I = 0x7f0c0272

.field public static final menu_icon:I = 0x7f0c008c

.field public static final menu_list_mode:I = 0x7f0c0260

.field public static final menu_move_to_folder:I = 0x7f0c0255

.field public static final menu_open_with:I = 0x7f0c0269

.field public static final menu_open_with_picker:I = 0x7f0c000a

.field public static final menu_pin:I = 0x7f0c026a

.field public static final menu_print:I = 0x7f0c025b

.field public static final menu_refresh:I = 0x7f0c0274

.field public static final menu_refresh_icon:I = 0x7f0c0262

.field public static final menu_refresh_status:I = 0x7f0c0250

.field public static final menu_rename:I = 0x7f0c0257

.field public static final menu_search:I = 0x7f0c024f

.field public static final menu_selection_all:I = 0x7f0c0267

.field public static final menu_selection_clear:I = 0x7f0c0266

.field public static final menu_selection_start:I = 0x7f0c0265

.field public static final menu_send:I = 0x7f0c0256

.field public static final menu_send_feedback:I = 0x7f0c024d

.field public static final menu_send_link:I = 0x7f0c0258

.field public static final menu_settings:I = 0x7f0c026e

.field public static final menu_sharing:I = 0x7f0c0254

.field public static final menu_show_live_editing:I = 0x7f0c0270

.field public static final menu_sortings:I = 0x7f0c0264

.field public static final menu_switch_account:I = 0x7f0c0009

.field public static final menu_title:I = 0x7f0c008d

.field public static final menu_unpin:I = 0x7f0c026b

.field public static final menu_untrash:I = 0x7f0c025d

.field public static final message:I = 0x7f0c0093

.field public static final middle:I = 0x7f0c0050

.field public static final modified:I = 0x7f0c00d7

.field public static final modified_row:I = 0x7f0c00d6

.field public static final monochrome:I = 0x7f0c003c

.field public static final more_actions_button:I = 0x7f0c0119

.field public static final more_actions_button_container:I = 0x7f0c011a

.field public static final name:I = 0x7f0c00e7

.field public static final nav_container:I = 0x7f0c0082

.field public static final nav_drawer:I = 0x7f0c021a

.field public static final nav_icon_drive:I = 0x7f0c01ab

.field public static final navigate_to_drive:I = 0x7f0c01aa

.field public static final navigate_to_drive_layout:I = 0x7f0c01a9

.field public static final navigation_folders:I = 0x7f0c01b2

.field public static final navigation_folders_top_margin:I = 0x7f0c01b3

.field public static final navigation_fragment:I = 0x7f0c021f

.field public static final navigation_icon:I = 0x7f0c01ae

.field public static final navigation_list_view:I = 0x7f0c01b0

.field public static final navigation_name:I = 0x7f0c01af

.field public static final navigation_warning_image:I = 0x7f0c01ad

.field public static final never:I = 0x7f0c004a

.field public static final new_account_button:I = 0x7f0c0087

.field public static final new_name:I = 0x7f0c0169

.field public static final next:I = 0x7f0c0234

.field public static final next_previous_container:I = 0x7f0c01df

.field public static final next_state_view_container:I = 0x7f0c01e1

.field public static final no_notes:I = 0x7f0c0215

.field public static final no_preview_icon:I = 0x7f0c01c5

.field public static final no_preview_open_in_another_app:I = 0x7f0c01c7

.field public static final none:I = 0x7f0c002a

.field public static final normal:I = 0x7f0c002b

.field public static final notes_present:I = 0x7f0c0218

.field public static final offline_status:I = 0x7f0c0121

.field public static final offline_title:I = 0x7f0c00f2

.field public static final offscreen_account_address:I = 0x7f0c01ee

.field public static final offscreen_account_display_name:I = 0x7f0c01ed

.field public static final offscreen_avatar:I = 0x7f0c00a3

.field public static final offscreen_cover_photo:I = 0x7f0c01e8

.field public static final offscreen_text:I = 0x7f0c01ec

.field public static final omnibox_title_section:I = 0x7f0c001b

.field public static final omnibox_url_section:I = 0x7f0c001c

.field public static final open_button:I = 0x7f0c018a

.field public static final open_detail_panel:I = 0x7f0c0253

.field public static final open_file:I = 0x7f0c026d

.field public static final opened:I = 0x7f0c00d9

.field public static final opened_row:I = 0x7f0c00d8

.field public static final owner:I = 0x7f0c0185

.field public static final owner_image:I = 0x7f0c0186

.field public static final owner_name:I = 0x7f0c0187

.field public static final page_container:I = 0x7f0c01c6

.field public static final pager:I = 0x7f0c0230

.field public static final password:I = 0x7f0c010d

.field public static final password_alert:I = 0x7f0c010e

.field public static final pause_button:I = 0x7f0c011e

.field public static final pdf_page_num:I = 0x7f0c01bb

.field public static final pdf_viewer:I = 0x7f0c0174

.field public static final pin:I = 0x7f0c00db

.field public static final pin_card_title:I = 0x7f0c00da

.field public static final pin_checkbox:I = 0x7f0c00f1

.field public static final pin_compoundButton:I = 0x7f0c00ff

.field public static final pin_mode_selected:I = 0x7f0c0124

.field public static final plain:I = 0x7f0c0015

.field public static final playPauseView:I = 0x7f0c00bb

.field public static final preview_offline_app_icon:I = 0x7f0c01c2

.field public static final preview_offline_click_to_open_text:I = 0x7f0c01c4

.field public static final preview_offline_message_text:I = 0x7f0c01c3

.field public static final previous_state_view_container:I = 0x7f0c01e0

.field public static final primary_button:I = 0x7f0c017d

.field public static final print_webview:I = 0x7f0c010f

.field public static final production:I = 0x7f0c0030

.field public static final progress:I = 0x7f0c00c1

.field public static final progress_bar:I = 0x7f0c01cf

.field public static final progress_block:I = 0x7f0c01cb

.field public static final progress_circular:I = 0x7f0c0012

.field public static final progress_horizontal:I = 0x7f0c0013

.field public static final progress_spinner:I = 0x7f0c01cc

.field public static final progress_text:I = 0x7f0c01cd

.field public static final punch_central_slide_view_container:I = 0x7f0c00a7

.field public static final punch_detachable_slide_view:I = 0x7f0c01dd

.field public static final punch_presentation_container:I = 0x7f0c01d2

.field public static final punch_presentation_mode_view_container:I = 0x7f0c01de

.field public static final punch_web_view_container:I = 0x7f0c00a8

.field public static final punch_web_view_fragment_root:I = 0x7f0c01dc

.field public static final quota_used:I = 0x7f0c00fd

.field public static final radio:I = 0x7f0c0060

.field public static final recent_activity_button_load_more:I = 0x7f0c00bf

.field public static final recent_container:I = 0x7f0c00ca

.field public static final recent_empty_list_message:I = 0x7f0c013e

.field public static final recent_empty_list_message_subtitle:I = 0x7f0c0140

.field public static final recent_empty_list_message_title:I = 0x7f0c013f

.field public static final recent_event_avatar:I = 0x7f0c00c9

.field public static final recent_event_eventType:I = 0x7f0c00cc

.field public static final recent_event_expandButton:I = 0x7f0c00c3

.field public static final recent_event_extra:I = 0x7f0c00c4

.field public static final recent_event_extra_image:I = 0x7f0c00c5

.field public static final recent_event_extra_pointer:I = 0x7f0c00c8

.field public static final recent_event_extra_rule:I = 0x7f0c00c7

.field public static final recent_event_extra_text:I = 0x7f0c00c6

.field public static final recent_event_target:I = 0x7f0c00ce

.field public static final recent_event_target_icon:I = 0x7f0c00cf

.field public static final recent_event_target_title:I = 0x7f0c00c0

.field public static final recent_event_timestamp:I = 0x7f0c00cd

.field public static final recent_event_username:I = 0x7f0c00cb

.field public static final recents_status:I = 0x7f0c00c2

.field public static final refresh_progress_bar:I = 0x7f0c021d

.field public static final removeView:I = 0x7f0c00ba

.field public static final replies:I = 0x7f0c01e5

.field public static final resume_button:I = 0x7f0c011d

.field public static final retry_button:I = 0x7f0c0148

.field public static final rfc822:I = 0x7f0c0016

.field public static final right_drop_zone:I = 0x7f0c01ff

.field public static final root_layout:I = 0x7f0c0085

.field public static final root_node:I = 0x7f0c0168

.field public static final sandbox:I = 0x7f0c0031

.field public static final satellite:I = 0x7f0c002c

.field public static final save_sharing_button:I = 0x7f0c020d

.field public static final scrim:I = 0x7f0c01ea

.field public static final search_badge:I = 0x7f0c006a

.field public static final search_bar:I = 0x7f0c0069

.field public static final search_button:I = 0x7f0c006b

.field public static final search_close_btn:I = 0x7f0c0070

.field public static final search_edit_frame:I = 0x7f0c006c

.field public static final search_go_btn:I = 0x7f0c0072

.field public static final search_mag_icon:I = 0x7f0c006d

.field public static final search_plate:I = 0x7f0c006e

.field public static final search_src_text:I = 0x7f0c006f

.field public static final search_text:I = 0x7f0c014c

.field public static final search_voice_btn:I = 0x7f0c0073

.field public static final secondary_button:I = 0x7f0c017e

.field public static final select_button:I = 0x7f0c0127

.field public static final select_button_background:I = 0x7f0c0126

.field public static final select_folder_button:I = 0x7f0c0111

.field public static final select_folder_button_background:I = 0x7f0c0110

.field public static final select_folder_padding:I = 0x7f0c012f

.field public static final select_thumbnail_background:I = 0x7f0c012d

.field public static final selected_account_container:I = 0x7f0c0083

.field public static final selected_entry_card:I = 0x7f0c01f0

.field public static final selectionDetails:I = 0x7f0c0034

.field public static final selection_actions_container:I = 0x7f0c01fd

.field public static final selection_drop_zone2_left_spacer:I = 0x7f0c01f6

.field public static final selection_drop_zone2_middle_spacer:I = 0x7f0c01f8

.field public static final selection_drop_zone2_new_folder:I = 0x7f0c01f9

.field public static final selection_drop_zone2_this_folder:I = 0x7f0c01f7

.field public static final selection_drop_zone_dismiss_area:I = 0x7f0c01f5

.field public static final selection_drop_zone_group_view:I = 0x7f0c01f4

.field public static final selection_floating_handle:I = 0x7f0c01fa

.field public static final selection_floating_handle_description:I = 0x7f0c01fc

.field public static final selection_floating_overlay_layout:I = 0x7f0c01fe

.field public static final selection_floating_visual:I = 0x7f0c0201

.field public static final selection_highlight_overlay:I = 0x7f0c0132

.field public static final selection_popup_anchor:I = 0x7f0c01fb

.field public static final selection_underlay:I = 0x7f0c0136

.field public static final separator:I = 0x7f0c017f

.field public static final shadow:I = 0x7f0c01f1

.field public static final share_actions_place_holder:I = 0x7f0c0252

.field public static final share_badge:I = 0x7f0c0102

.field public static final share_description:I = 0x7f0c0104

.field public static final share_email:I = 0x7f0c0208

.field public static final share_file:I = 0x7f0c0251

.field public static final share_item:I = 0x7f0c0101

.field public static final share_list:I = 0x7f0c00f5

.field public static final share_list_progress_bar:I = 0x7f0c00f7

.field public static final share_list_warning:I = 0x7f0c00f6

.field public static final share_name:I = 0x7f0c0103

.field public static final share_options:I = 0x7f0c0207

.field public static final share_panel:I = 0x7f0c00f3

.field public static final share_role:I = 0x7f0c00ea

.field public static final share_user_view:I = 0x7f0c0206

.field public static final sharee_badge:I = 0x7f0c0094

.field public static final sharee_description:I = 0x7f0c0096

.field public static final sharee_name:I = 0x7f0c0095

.field public static final sharing_group_header:I = 0x7f0c0205

.field public static final sharing_group_title:I = 0x7f0c020a

.field public static final sharing_item:I = 0x7f0c00e8

.field public static final sharing_option:I = 0x7f0c0209

.field public static final sharing_options:I = 0x7f0c0092

.field public static final sharing_title:I = 0x7f0c00f4

.field public static final shortcut:I = 0x7f0c005f

.field public static final showCustom:I = 0x7f0c0043

.field public static final showHome:I = 0x7f0c0044

.field public static final showTitle:I = 0x7f0c0045

.field public static final show_preview_button:I = 0x7f0c012b

.field public static final sidebar_action_top_margin:I = 0x7f0c0210

.field public static final sidebar_actions:I = 0x7f0c01a4

.field public static final sidebar_actions_layout:I = 0x7f0c01a3

.field public static final sign_in:I = 0x7f0c0211

.field public static final size_and_kind:I = 0x7f0c00d1

.field public static final size_and_type_row:I = 0x7f0c00d0

.field public static final skip:I = 0x7f0c0232

.field public static final slide_index:I = 0x7f0c01d3

.field public static final slide_picker_open_spacer:I = 0x7f0c01e3

.field public static final sortLabel:I = 0x7f0c011f

.field public static final sort_label:I = 0x7f0c0188

.field public static final spacer:I = 0x7f0c0130

.field public static final speaker_notes:I = 0x7f0c0217

.field public static final speaker_notes_close_button:I = 0x7f0c0212

.field public static final speaker_notes_content_panel:I = 0x7f0c01db

.field public static final speaker_notes_divider:I = 0x7f0c0214

.field public static final speaker_notes_indicator:I = 0x7f0c01d4

.field public static final speaker_notes_presence_panel:I = 0x7f0c01da

.field public static final speaker_notes_title:I = 0x7f0c0213

.field public static final speaker_notes_wrapper:I = 0x7f0c0216

.field public static final spinner_account_name_and_email:I = 0x7f0c0080

.field public static final splash:I = 0x7f0c0239

.field public static final splash_blurb:I = 0x7f0c023c

.field public static final splash_image:I = 0x7f0c023a

.field public static final splash_text:I = 0x7f0c023b

.field public static final split_action_bar:I = 0x7f0c000f

.field public static final star_cb:I = 0x7f0c00ef

.field public static final statusLabels:I = 0x7f0c0120

.field public static final sticky_header:I = 0x7f0c0146

.field public static final storage_display:I = 0x7f0c01a5

.field public static final storage_summary:I = 0x7f0c01a7

.field public static final storage_summary_additional_info:I = 0x7f0c01a8

.field public static final strict_sandbox:I = 0x7f0c0032

.field public static final subTitleView:I = 0x7f0c00be

.field public static final submit_area:I = 0x7f0c0071

.field public static final subtitle:I = 0x7f0c01b6

.field public static final swipable_doc_list:I = 0x7f0c013c

.field public static final swipable_doc_list_stub:I = 0x7f0c013b

.field public static final sync_in_progress:I = 0x7f0c01ba

.field public static final sync_more_spinner:I = 0x7f0c0147

.field public static final sync_progress_updater:I = 0x7f0c0002

.field public static final sync_state_background:I = 0x7f0c0133

.field public static final sync_video_background:I = 0x7f0c0134

.field public static final tab1:I = 0x7f0c018b

.field public static final tab2:I = 0x7f0c018c

.field public static final tabMode:I = 0x7f0c0040

.field public static final tablet_mainContainer:I = 0x7f0c021b

.field public static final terrain:I = 0x7f0c002d

.field public static final text:I = 0x7f0c01e7

.field public static final text1:I = 0x7f0c0027

.field public static final text2:I = 0x7f0c0028

.field public static final text_box:I = 0x7f0c01be

.field public static final text_view:I = 0x7f0c0091

.field public static final text_viewer:I = 0x7f0c0175

.field public static final thumbnail:I = 0x7f0c00de

.field public static final thumbnailImage:I = 0x7f0c0221

.field public static final thumbnail_border:I = 0x7f0c01d5

.field public static final thumbnail_container:I = 0x7f0c00dd

.field public static final thumbnail_gradient:I = 0x7f0c00e0

.field public static final thumbnail_progress_bar:I = 0x7f0c0222

.field public static final thumbnail_shadow:I = 0x7f0c00df

.field public static final time:I = 0x7f0c019e

.field public static final time_current:I = 0x7f0c019c

.field public static final title:I = 0x7f0c005c

.field public static final titleView:I = 0x7f0c00bd

.field public static final title_background:I = 0x7f0c01f3

.field public static final title_buttons:I = 0x7f0c0131

.field public static final title_container:I = 0x7f0c00ed

.field public static final title_editor:I = 0x7f0c016c

.field public static final title_icon:I = 0x7f0c01ce

.field public static final title_row:I = 0x7f0c0189

.field public static final title_shadow:I = 0x7f0c010a

.field public static final titlebar:I = 0x7f0c0109

.field public static final toggle_subtitles:I = 0x7f0c0273

.field public static final top:I = 0x7f0c0052

.field public static final top_collections_list:I = 0x7f0c0170

.field public static final top_frame:I = 0x7f0c00a2

.field public static final trashed_item_description:I = 0x7f0c00fe

.field public static final type:I = 0x7f0c00b8

.field public static final type_icon:I = 0x7f0c012a

.field public static final undo_button:I = 0x7f0c0225

.field public static final undo_handle_overlay:I = 0x7f0c0223

.field public static final undo_message:I = 0x7f0c0224

.field public static final unselect_button:I = 0x7f0c0129

.field public static final unselect_button_background:I = 0x7f0c0128

.field public static final unselect_folder_button:I = 0x7f0c0113

.field public static final unselect_folder_button_background:I = 0x7f0c0112

.field public static final unselect_thumbnail_background:I = 0x7f0c012e

.field public static final up:I = 0x7f0c000c

.field public static final up_affordance:I = 0x7f0c01bd

.field public static final upload_conversion_options_layout:I = 0x7f0c022c

.field public static final upload_doclist_convert:I = 0x7f0c022d

.field public static final upload_edittext_document_title:I = 0x7f0c0227

.field public static final upload_folder:I = 0x7f0c022b

.field public static final upload_image_preview:I = 0x7f0c0228

.field public static final upload_multiple_listview_document_title:I = 0x7f0c0229

.field public static final upload_spinner_account:I = 0x7f0c022a

.field public static final upload_textview_document_title:I = 0x7f0c0226

.field public static final url:I = 0x7f0c0017

.field public static final useLogo:I = 0x7f0c0046

.field public static final video_play_overlay:I = 0x7f0c0184

.field public static final video_view:I = 0x7f0c022e

.field public static final view_holder:I = 0x7f0c0001

.field public static final view_switcher:I = 0x7f0c0165

.field public static final view_tag_key_selection_original_resource:I = 0x7f0c0007

.field public static final visible_band:I = 0x7f0c014a

.field public static final web_view_container:I = 0x7f0c01d9

.field public static final webview:I = 0x7f0c023d

.field public static final webview_fragment:I = 0x7f0c01ca

.field public static final webview_frame:I = 0x7f0c01c9

.field public static final welcome:I = 0x7f0c022f

.field public static final welcome_button_close:I = 0x7f0c0240

.field public static final welcome_button_continue:I = 0x7f0c023f

.field public static final welcome_button_positive:I = 0x7f0c0242

.field public static final welcome_fragment:I = 0x7f0c0003

.field public static final welcome_page_indicator:I = 0x7f0c0241

.field public static final welcome_pager:I = 0x7f0c023e

.field public static final widget:I = 0x7f0c0097

.field public static final widget_account_name:I = 0x7f0c00a0

.field public static final widget_app_logo:I = 0x7f0c009c

.field public static final widget_app_title:I = 0x7f0c009f

.field public static final widget_app_title_account_container:I = 0x7f0c009e

.field public static final widget_app_title_only:I = 0x7f0c009d

.field public static final widget_broken_title:I = 0x7f0c00a1

.field public static final widget_home:I = 0x7f0c009b

.field public static final widget_newdoc:I = 0x7f0c0098

.field public static final widget_newdocfromcamera:I = 0x7f0c0099

.field public static final widget_upload:I = 0x7f0c009a

.field public static final withText:I = 0x7f0c004b

.field public static final wrap_content:I = 0x7f0c0036

.field public static final wrapper_on_optional_account_switcher:I = 0x7f0c0180

.field public static final zoomed_view:I = 0x7f0c0173


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
