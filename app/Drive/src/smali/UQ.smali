.class public LUQ;
.super Ljava/lang/Object;
.source "UrlOpenerActivityIntentFactoryMap.java"


# instance fields
.field private final a:LUP;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LVc;",
            "LUP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LUP;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LUP;",
            "Ljava/util/Map",
            "<",
            "LVc;",
            "LUP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LUQ;->a:Ljava/util/Map;

    .line 40
    iput-object p1, p0, LUQ;->a:LUP;

    .line 41
    iget-object v0, p0, LUQ;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 42
    return-void
.end method

.method synthetic constructor <init>(LUP;Ljava/util/Map;LUR;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, LUQ;-><init>(LUP;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public a(LVc;)LUP;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, LUQ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUP;

    .line 50
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LUQ;->a:LUP;

    goto :goto_0
.end method
