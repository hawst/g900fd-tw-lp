.class public LaHd;
.super LaGm;
.source "SyncRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaFi;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field private a:Ljava/lang/Long;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private a:Z

.field private final b:J

.field private b:Z

.field private c:J

.field private c:Z

.field private d:J

.field private d:Z

.field private e:J

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(LaEz;JJ)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    .line 60
    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 61
    cmp-long v0, p2, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 62
    iget-wide v4, p0, LaHd;->c:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 63
    iput-wide p2, p0, LaHd;->b:J

    .line 64
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, LaHd;->a:Ljava/util/Date;

    .line 65
    iput-boolean v2, p0, LaHd;->a:Z

    .line 66
    iput-boolean v2, p0, LaHd;->b:Z

    .line 67
    iput-boolean v1, p0, LaHd;->c:Z

    .line 68
    iput-boolean v2, p0, LaHd;->d:Z

    .line 69
    iput-boolean v2, p0, LaHd;->e:Z

    .line 70
    iput-wide v6, p0, LaHd;->c:J

    .line 71
    iput-wide v6, p0, LaHd;->d:J

    .line 72
    iput-wide p4, p0, LaHd;->f:J

    .line 73
    return-void

    :cond_0
    move v0, v2

    .line 61
    goto :goto_0

    :cond_1
    move v0, v2

    .line 62
    goto :goto_1
.end method

.method public static a(LaEz;)J
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 261
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaFj;->i:LaFj;

    invoke-virtual {v3}, LaFj;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " DESC"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 263
    const-string v8, "1"

    .line 264
    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v1

    invoke-virtual {v1}, LaFi;->c()Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 267
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v5, v2

    move-object v6, v2

    .line 264
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 273
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 274
    const-wide/16 v0, -0x1

    .line 278
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v0

    .line 276
    :cond_0
    :try_start_1
    sget-object v0, LaFj;->i:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v2}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 278
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaHd;
    .locals 6

    .prologue
    .line 385
    sget-object v0, LaFj;->a:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 386
    sget-object v0, LaFj;->o:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    .line 387
    new-instance v0, LaHd;

    if-eqz v1, :cond_0

    .line 388
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_0
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LaHd;-><init>(LaEz;JJ)V

    .line 389
    new-instance v1, Ljava/util/Date;

    sget-object v2, LaFj;->b:LaFj;

    .line 390
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 389
    invoke-virtual {v0, v1}, LaHd;->a(Ljava/util/Date;)V

    .line 391
    sget-object v1, LaFj;->d:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, LaHd;->a(Z)V

    .line 392
    sget-object v1, LaFj;->e:LaFj;

    .line 393
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 392
    invoke-virtual {v0, v1}, LaHd;->b(Z)V

    .line 394
    sget-object v1, LaFj;->g:LaFj;

    .line 395
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 394
    invoke-virtual {v0, v1}, LaHd;->c(Z)V

    .line 396
    sget-object v1, LaFj;->h:LaFj;

    .line 397
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v1

    iput-boolean v1, v0, LaHd;->e:Z

    .line 398
    sget-object v1, LaFj;->f:LaFj;

    .line 399
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, LaHd;->d:Z

    .line 400
    sget-object v1, LaFj;->l:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHd;->e(J)V

    .line 401
    sget-object v1, LaFj;->i:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHd;->f(J)V

    .line 402
    sget-object v1, LaFj;->j:LaFj;

    .line 403
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 402
    invoke-virtual {v0, v2, v3}, LaHd;->b(J)V

    .line 404
    sget-object v1, LaFj;->k:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHd;->d(J)V

    .line 406
    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v1

    invoke-virtual {v1}, LaFi;->d()Ljava/lang/String;

    move-result-object v1

    .line 405
    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHd;->c(J)V

    .line 407
    sget-object v1, LaFj;->m:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LaHd;->a:Ljava/lang/String;

    .line 408
    sget-object v1, LaFj;->n:LaFj;

    .line 409
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, LaHd;->a:Ljava/lang/Long;

    .line 410
    return-object v0

    .line 388
    :cond_0
    const-wide/16 v4, -0x1

    goto/16 :goto_0
.end method

.method public static a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 417
    sget-object v0, LaFj;->j:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 418
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " & "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " != 0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private a(J)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaFj;->i:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaFj;->d:LaFj;

    .line 284
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaFj;->g:LaFj;

    .line 285
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaFj;->e:LaFj;

    .line 286
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaFj;->l:LaFj;

    .line 287
    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 288
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    .line 289
    invoke-virtual {v0}, LaEz;->a()LQr;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LQr;)I

    move-result v0

    int-to-long v4, v0

    .line 291
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    const-string v6, "0"

    const-string v7, "0"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 290
    invoke-static {v0, v3, v6, v7, v4}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 292
    const-string v8, "1"

    .line 293
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v1

    invoke-virtual {v1}, LaFi;->c()Ljava/lang/String;

    move-result-object v1

    .line 295
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 296
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 293
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 302
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 304
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 333
    iget-wide v0, p0, LaHd;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, LaHd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaHd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaHd;->e()J

    move-result-wide v2

    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    .line 346
    invoke-virtual {v0}, LaEz;->a()LQr;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LQr;)I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, LaHd;->b:J

    return-wide v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, LaHd;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, LaHd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 4

    .prologue
    .line 93
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, LaHd;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, LaHd;->e:Z

    .line 130
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 89
    iput-wide p1, p0, LaHd;->f:J

    .line 90
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    iget-wide v4, p0, LaHd;->b:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 352
    iget-object v0, p0, LaHd;->a:Ljava/util/Date;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, LaFj;->a:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, LaHd;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 354
    sget-object v0, LaFj;->b:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaHd;->a:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    sget-object v0, LaFj;->d:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaHd;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 356
    sget-object v0, LaFj;->e:LaFj;

    .line 357
    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaHd;->b:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 356
    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 358
    sget-object v0, LaFj;->g:LaFj;

    .line 359
    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaHd;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 358
    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    sget-object v0, LaFj;->h:LaFj;

    .line 361
    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaHd;->e:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 362
    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 360
    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    sget-object v0, LaFj;->f:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, LaHd;->d:Z

    if-eqz v3, :cond_5

    .line 364
    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 363
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    sget-object v0, LaFj;->l:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHd;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 366
    sget-object v0, LaFj;->i:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHd;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 367
    sget-object v0, LaFj;->j:LaFj;

    .line 368
    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHd;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 367
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 369
    sget-object v0, LaFj;->k:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHd;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 370
    sget-object v0, LaFj;->m:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaHd;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-wide v0, p0, LaHd;->f:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_6

    .line 372
    sget-object v0, LaFj;->o:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHd;->f:J

    .line 373
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 372
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 377
    :goto_6
    sget-object v0, LaFj;->n:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaHd;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 379
    return-void

    :cond_0
    move v0, v2

    .line 351
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 355
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 357
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 359
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 361
    goto/16 :goto_4

    :cond_5
    move v1, v2

    .line 363
    goto/16 :goto_5

    .line 375
    :cond_6
    sget-object v0, LaFj;->o:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_6
.end method

.method public a(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, LaHd;->a:Ljava/lang/Long;

    .line 208
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, LaHd;->a:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 4

    .prologue
    .line 97
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaHd;->a:Ljava/util/Date;

    .line 98
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, LaHd;->a:Z

    .line 106
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, LaHd;->a:Z

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, LaHd;->f:J

    return-wide v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, LaHd;->d:Z

    .line 155
    return-void
.end method

.method b(J)V
    .locals 1

    .prologue
    .line 141
    iput-wide p1, p0, LaHd;->a:J

    .line 142
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, LaHd;->b:Z

    .line 114
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, LaHd;->b:Z

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 186
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaHd;->c:J

    .line 187
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 121
    iput-boolean p1, p0, LaHd;->c:Z

    .line 122
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, LaHd;->c:Z

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 158
    iget-wide v0, p0, LaHd;->e:J

    return-wide v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 190
    iget-wide v0, p0, LaHd;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LaHd;->c:J

    .line 191
    return-void
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 162
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 163
    iput-wide p1, p0, LaHd;->e:J

    .line 164
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, LaHd;->e:Z

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, LaHd;->c:J

    return-wide v0
.end method

.method public e()V
    .locals 6

    .prologue
    .line 310
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 312
    :try_start_0
    invoke-direct {p0}, LaHd;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    invoke-virtual {p0}, LaHd;->f()J

    move-result-wide v2

    .line 315
    iget-boolean v0, p0, LaHd;->a:Z

    if-nez v0, :cond_2

    .line 316
    invoke-direct {p0}, LaHd;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide/16 v0, 0x1

    .line 317
    :goto_0
    iget-boolean v4, p0, LaHd;->b:Z

    if-nez v4, :cond_0

    iget-wide v4, p0, LaHd;->d:J

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 318
    :cond_0
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LaHd;->a:J

    .line 320
    :cond_1
    iget-wide v4, p0, LaHd;->a:J

    or-long/2addr v0, v4

    iput-wide v0, p0, LaHd;->a:J

    .line 322
    :cond_2
    invoke-virtual {p0, v2, v3}, LaHd;->f(J)V

    .line 325
    :cond_3
    invoke-super {p0}, LaGm;->e()V

    .line 326
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    .line 330
    return-void

    .line 316
    :cond_4
    const-wide/16 v0, 0x2

    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    throw v1
.end method

.method public e(J)V
    .locals 3

    .prologue
    .line 171
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 172
    iput-wide p1, p0, LaHd;->c:J

    .line 173
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 133
    iget-wide v0, p0, LaHd;->a:J

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()J
    .locals 4

    .prologue
    .line 240
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 242
    :try_start_0
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-static {v0}, LaHd;->a(LaEz;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 243
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 244
    const-wide/16 v2, 0x0

    .line 252
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 253
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    move-wide v0, v2

    :goto_0
    return-wide v0

    .line 246
    :cond_0
    :try_start_1
    invoke-direct {p0, v2, v3}, LaHd;->a(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 253
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    move-wide v0, v2

    goto :goto_0

    .line 249
    :cond_1
    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    .line 252
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 253
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    move-wide v0, v2

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 253
    iget-object v0, p0, LaHd;->a:LaFm;

    check-cast v0, LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    throw v1
.end method

.method f(J)V
    .locals 3

    .prologue
    .line 181
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 182
    iput-wide p1, p0, LaHd;->d:J

    .line 183
    return-void

    .line 181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 137
    iget-wide v0, p0, LaHd;->a:J

    const-wide/16 v2, 0x2

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, LaHd;->h()V

    .line 217
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaHd;->c:J

    .line 218
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, LaHd;->d:Z

    return v0
.end method

.method public h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 224
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, LaHd;->a:Ljava/util/Date;

    .line 225
    iput-boolean v2, p0, LaHd;->a:Z

    .line 226
    iput-boolean v2, p0, LaHd;->b:Z

    .line 227
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaHd;->e:J

    .line 228
    iput-object v3, p0, LaHd;->a:Ljava/lang/String;

    .line 229
    iput-object v3, p0, LaHd;->a:Ljava/lang/Long;

    .line 230
    iput-boolean v2, p0, LaHd;->e:Z

    .line 231
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 423
    const-string v1, "SyncRequest[sqlId=%d, entrySqlId=%d, requestTime=%s, %s, attempted %d times, uploadUri=%s, documentContentId=%d, uploadSnapshotLastModifiedTime=%s]"

    const/16 v0, 0x8

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 426
    invoke-virtual {p0}, LaHd;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    .line 427
    invoke-virtual {p0}, LaHd;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    .line 428
    invoke-virtual {p0}, LaHd;->a()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    .line 429
    invoke-virtual {p0}, LaHd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "completed"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x4

    .line 430
    invoke-virtual {p0}, LaHd;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    .line 431
    invoke-virtual {p0}, LaHd;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    .line 432
    invoke-virtual {p0}, LaHd;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x7

    iget-object v3, p0, LaHd;->a:Ljava/lang/Long;

    aput-object v3, v2, v0

    .line 423
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 429
    :cond_0
    const-string v0, "pending"

    goto :goto_0
.end method
