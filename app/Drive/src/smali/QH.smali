.class public final LQH;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQE;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQv;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQD;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQr;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQt;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQy;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LQE;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQE;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQB;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LQE;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LQM;

    sput-object v0, LQH;->a:Ljava/lang/Class;

    .line 33
    const-class v0, LQO;

    sput-object v0, LQH;->b:Ljava/lang/Class;

    .line 34
    const-class v0, LQP;

    sput-object v0, LQH;->c:Ljava/lang/Class;

    .line 35
    const-class v0, LQL;

    sput-object v0, LQH;->d:Ljava/lang/Class;

    .line 36
    const-class v0, LQS;

    sput-object v0, LQH;->e:Ljava/lang/Class;

    .line 37
    const-class v0, LQJ;

    sput-object v0, LQH;->f:Ljava/lang/Class;

    .line 38
    const-class v0, LQK;

    sput-object v0, LQH;->g:Ljava/lang/Class;

    .line 39
    const-class v0, LQQ;

    sput-object v0, LQH;->h:Ljava/lang/Class;

    .line 40
    const-class v0, LQR;

    sput-object v0, LQH;->i:Ljava/lang/Class;

    .line 41
    const-class v0, LQN;

    sput-object v0, LQH;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 57
    iput-object p1, p0, LQH;->a:LbrA;

    .line 58
    const-class v0, LQE;

    sget-object v1, LQH;->f:Ljava/lang/Class;

    .line 59
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    const-class v1, LbuO;

    .line 58
    invoke-static {v0, v1}, LQH;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->a:Lbsk;

    .line 61
    const-class v0, LQv;

    invoke-static {v0, v3}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->b:Lbsk;

    .line 64
    const-class v0, LQD;

    invoke-static {v0, v3}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->c:Lbsk;

    .line 67
    const-class v0, LQr;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->d:Lbsk;

    .line 70
    const-class v0, LQt;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->e:Lbsk;

    .line 73
    const-class v0, LQy;

    invoke-static {v0, v3}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->f:Lbsk;

    .line 76
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LQE;

    aput-object v2, v1, v4

    .line 77
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 76
    invoke-static {v0, v3}, LQH;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->g:Lbsk;

    .line 79
    const-class v0, LQE;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->h:Lbsk;

    .line 82
    const-class v0, LQB;

    invoke-static {v0, v3}, LQH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->i:Lbsk;

    .line 85
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LQE;

    aput-object v2, v1, v4

    .line 86
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 85
    invoke-static {v0, v3}, LQH;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LQH;->j:Lbsk;

    .line 88
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 133
    sparse-switch p1, :sswitch_data_0

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :sswitch_0
    new-instance v1, LQv;

    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 138
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LQH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 136
    invoke-static {v0, v2}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LQv;-><init>(Laja;)V

    move-object v0, v1

    .line 177
    :goto_0
    return-object v0

    .line 145
    :sswitch_1
    new-instance v0, LQD;

    iget-object v1, p0, LQH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 148
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LQH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->h:Lbsk;

    .line 146
    invoke-static {v1, v2}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTh;

    iget-object v2, p0, LQH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 154
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LQH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 152
    invoke-static {v2, v3}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LQH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->e:Lbsk;

    .line 160
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LQH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->e:Lbsk;

    .line 158
    invoke-static {v3, v4}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LQt;

    iget-object v4, p0, LQH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->f:Lbsk;

    .line 166
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LQH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->f:Lbsk;

    .line 164
    invoke-static {v4, v5}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LQy;

    iget-object v5, p0, LQH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->h:Lbsk;

    .line 172
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LQH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LQH;

    iget-object v6, v6, LQH;->h:Lbsk;

    .line 170
    invoke-static {v5, v6}, LQH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LQE;

    invoke-direct/range {v0 .. v5}, LQD;-><init>(LTh;LQr;LQt;LQy;LQE;)V

    goto :goto_0

    .line 133
    nop

    :sswitch_data_0
    .sparse-switch
        0x3b -> :sswitch_0
        0x43 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 194
    packed-switch p2, :pswitch_data_0

    .line 263
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :pswitch_1
    check-cast p1, LQF;

    .line 198
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->e:Lbsk;

    .line 201
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQt;

    .line 198
    invoke-virtual {p1, v0}, LQF;->provideClientFlag(LQt;)LQr;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 205
    :pswitch_2
    check-cast p1, LQF;

    .line 207
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->b:Lbsk;

    .line 210
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQv;

    .line 207
    invoke-virtual {p1, v0}, LQF;->provideClientFlagDatabase(LQv;)LQt;

    move-result-object v0

    goto :goto_0

    .line 214
    :pswitch_3
    check-cast p1, LQF;

    .line 216
    invoke-virtual {p1}, LQF;->provideClientFlagJsonParser()LQy;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_4
    check-cast p1, LQG;

    .line 221
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->j:Lbsk;

    .line 224
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 221
    invoke-virtual {p1, v0}, LQG;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 228
    :pswitch_5
    check-cast p1, LQF;

    .line 230
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->O:Lbsk;

    .line 233
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    .line 230
    invoke-virtual {p1, v0}, LQF;->provideClientFlagUriResolver(LbiP;)LQE;

    move-result-object v0

    goto :goto_0

    .line 237
    :pswitch_6
    check-cast p1, LQF;

    .line 239
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->c:Lbsk;

    .line 242
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQD;

    .line 239
    invoke-virtual {p1, v0}, LQF;->provideClientFlagSynchronizer(LQD;)LQB;

    move-result-object v0

    goto :goto_0

    .line 246
    :pswitch_7
    check-cast p1, LQG;

    .line 248
    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v2, v0, LQH;->a:Lbsk;

    iget-object v0, p0, LQH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 255
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LQH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 259
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 248
    invoke-virtual {p1, v2, v0, v1}, LQG;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x39
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    const-class v0, LQE;

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LQH;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Lbuv;Lbsk;)V

    .line 96
    const-class v0, LQv;

    iget-object v1, p0, LQH;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 97
    const-class v0, LQD;

    iget-object v1, p0, LQH;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 98
    const-class v0, LQr;

    iget-object v1, p0, LQH;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 99
    const-class v0, LQt;

    iget-object v1, p0, LQH;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 100
    const-class v0, LQy;

    iget-object v1, p0, LQH;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 101
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LQE;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LQH;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Lbuv;Lbsk;)V

    .line 102
    const-class v0, LQE;

    iget-object v1, p0, LQH;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, LQB;

    iget-object v1, p0, LQH;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Ljava/lang/Class;Lbsk;)V

    .line 104
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LQE;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LQH;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LQH;->a(Lbuv;Lbsk;)V

    .line 105
    iget-object v0, p0, LQH;->a:Lbsk;

    iget-object v1, p0, LQH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNA;

    iget-object v1, v1, LNA;->c:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 107
    iget-object v0, p0, LQH;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x3b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 109
    iget-object v0, p0, LQH;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x43

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 111
    iget-object v0, p0, LQH;->d:Lbsk;

    const-class v1, LQF;

    const/16 v2, 0x39

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 113
    iget-object v0, p0, LQH;->e:Lbsk;

    const-class v1, LQF;

    const/16 v2, 0x3a

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 115
    iget-object v0, p0, LQH;->f:Lbsk;

    const-class v1, LQF;

    const/16 v2, 0x3e

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 117
    iget-object v0, p0, LQH;->g:Lbsk;

    const-class v1, LQG;

    const/16 v2, 0x3f

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 119
    iget-object v0, p0, LQH;->h:Lbsk;

    const-class v1, LQF;

    const/16 v2, 0x41

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 121
    iget-object v0, p0, LQH;->i:Lbsk;

    const-class v1, LQF;

    const/16 v2, 0x45

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 123
    iget-object v0, p0, LQH;->j:Lbsk;

    const-class v1, LQG;

    const/16 v2, 0x40

    invoke-virtual {p0, v1, v2}, LQH;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 125
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 186
    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 129
    return-void
.end method
