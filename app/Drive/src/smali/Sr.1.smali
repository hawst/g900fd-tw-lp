.class public final LSr;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSf;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSo;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSv;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LSr;->a:LbrA;

    .line 42
    const-class v0, LSf;

    invoke-static {v0, v1}, LSr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSr;->a:Lbsk;

    .line 45
    const-class v0, LSo;

    invoke-static {v0, v1}, LSr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSr;->b:Lbsk;

    .line 48
    const-class v0, LSv;

    invoke-static {v0, v1}, LSr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSr;->c:Lbsk;

    .line 51
    const-class v0, LSu;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LSr;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LSr;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 81
    packed-switch p1, :pswitch_data_0

    .line 97
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_1
    new-instance v1, LSf;

    iget-object v0, p0, LSr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 86
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LSr;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 84
    invoke-static {v0, v2}, LSr;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LSf;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 95
    :goto_0
    return-object v0

    .line 93
    :pswitch_2
    new-instance v0, LSo;

    invoke-direct {v0}, LSo;-><init>()V

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x2c1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 112
    packed-switch p2, :pswitch_data_0

    .line 132
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :pswitch_1
    check-cast p1, LSp;

    .line 116
    iget-object v0, p0, LSr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSr;

    iget-object v0, v0, LSr;->b:Lbsk;

    .line 119
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSo;

    .line 116
    invoke-virtual {p1, v0}, LSp;->provideFeedbackHelper(LSo;)LSv;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    .line 123
    :pswitch_2
    check-cast p1, LSp;

    .line 125
    iget-object v0, p0, LSr;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSr;

    iget-object v0, v0, LSr;->a:Lbsk;

    .line 128
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSf;

    .line 125
    invoke-virtual {p1, v0}, LSp;->provideAutoBackup(LSf;)LSu;

    move-result-object v0

    goto :goto_0

    .line 112
    :pswitch_data_0
    .packed-switch 0x2c2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 61
    const-class v0, LSf;

    iget-object v1, p0, LSr;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LSr;->a(Ljava/lang/Class;Lbsk;)V

    .line 62
    const-class v0, LSo;

    iget-object v1, p0, LSr;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LSr;->a(Ljava/lang/Class;Lbsk;)V

    .line 63
    const-class v0, LSv;

    iget-object v1, p0, LSr;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LSr;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    const-class v0, LSu;

    iget-object v1, p0, LSr;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LSr;->a(Ljava/lang/Class;Lbsk;)V

    .line 65
    iget-object v0, p0, LSr;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, LSr;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2c3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, LSr;->c:Lbsk;

    const-class v1, LSp;

    const/16 v2, 0x2c2

    invoke-virtual {p0, v1, v2}, LSr;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, LSr;->d:Lbsk;

    const-class v1, LSp;

    const/16 v2, 0x2c4

    invoke-virtual {p0, v1, v2}, LSr;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 104
    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
