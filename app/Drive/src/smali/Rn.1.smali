.class public LRn;
.super Ljava/lang/Object;
.source "FullscreenSwitcherFragment.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, LRn;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_1

    move v0, v1

    .line 128
    :goto_0
    iget-object v3, p0, LRn;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-static {v3}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)I

    move-result v3

    xor-int/2addr v3, p1

    .line 129
    iget-object v4, p0, LRn;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-static {v4, p1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;I)I

    .line 130
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    move v3, v1

    .line 133
    :goto_1
    const-string v4, "FullscreenSwitcherFragment"

    const-string v5, "in onSystemUiVisibilityChange isButtonsOn=%b buttonsVisibilityChanged=%b"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 135
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v1

    .line 133
    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 137
    if-eqz v3, :cond_0

    .line 138
    if-eqz v0, :cond_0

    iget-object v0, p0, LRn;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, LRn;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a()V

    .line 142
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 127
    goto :goto_0

    :cond_2
    move v3, v2

    .line 130
    goto :goto_1
.end method
