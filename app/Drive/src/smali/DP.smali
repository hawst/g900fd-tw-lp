.class LDP;
.super Ljava/lang/Object;
.source "SyncViewState.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LahB;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:LvU;


# direct methods
.method public constructor <init>(LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;LvU;)V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p0, LDP;->a:LahB;

    .line 201
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LDP;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 202
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p0, LDP;->a:LvU;

    .line 203
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, LDP;->a:LahB;

    iget-object v1, p0, LDP;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LahB;->c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 208
    iget-object v0, p0, LDP;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    .line 209
    return-void
.end method
