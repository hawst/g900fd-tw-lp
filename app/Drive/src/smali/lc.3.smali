.class public final Llc;
.super Ljava/lang/Object;
.source "MediaRouteProviderService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final a:I

.field public final a:Landroid/os/Messenger;

.field final synthetic a:Landroid/support/v7/media/MediaRouteProviderService;

.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LkW;",
            ">;"
        }
    .end annotation
.end field

.field public a:LkQ;


# direct methods
.method public constructor <init>(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;I)V
    .locals 1

    .prologue
    .line 489
    iput-object p1, p0, Llc;->a:Landroid/support/v7/media/MediaRouteProviderService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    .line 490
    iput-object p2, p0, Llc;->a:Landroid/os/Messenger;

    .line 491
    iput p3, p0, Llc;->a:I

    .line 492
    return-void
.end method


# virtual methods
.method public a(I)LkW;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkW;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 505
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    .line 506
    :goto_0
    if-ge v1, v3, :cond_0

    .line 507
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkW;

    invoke-virtual {v0}, LkW;->a()V

    .line 506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 509
    :cond_0
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 511
    iget-object v0, p0, Llc;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 513
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Llc;->a(LkQ;)Z

    .line 514
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 496
    :try_start_0
    iget-object v1, p0, Llc;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    const/4 v0, 0x1

    .line 501
    :goto_0
    return v0

    .line 498
    :catch_0
    move-exception v1

    .line 499
    invoke-virtual {p0}, Llc;->binderDied()V

    goto :goto_0
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkW;

    .line 534
    if-eqz v0, :cond_0

    .line 535
    iget-object v1, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 536
    invoke-virtual {v0}, LkW;->a()V

    .line 537
    const/4 v0, 0x1

    .line 539
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Messenger;)Z
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Llc;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_0

    .line 522
    iget-object v0, p0, Llc;->a:Landroid/support/v7/media/MediaRouteProviderService;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;)LkR;

    move-result-object v0

    invoke-virtual {v0, p1}, LkR;->a(Ljava/lang/String;)LkW;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Llc;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 526
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LkQ;)Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Llc;->a:LkQ;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Llc;->a:LkQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llc;->a:LkQ;

    invoke-virtual {v0, p1}, LkQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 549
    :cond_0
    iput-object p1, p0, Llc;->a:LkQ;

    .line 550
    iget-object v0, p0, Llc;->a:Landroid/support/v7/media/MediaRouteProviderService;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;)Z

    move-result v0

    .line 552
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public binderDied()V
    .locals 3

    .prologue
    .line 558
    iget-object v0, p0, Llc;->a:Landroid/support/v7/media/MediaRouteProviderService;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/support/v7/media/MediaRouteProviderService;)Lld;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Llc;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lld;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 559
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Llc;->a:Landroid/os/Messenger;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
