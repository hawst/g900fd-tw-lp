.class public Lapj;
.super Lans;
.source "ThumbnailUriFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lans",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LZS;

.field private final a:LamW;

.field private final a:Ljava/lang/String;

.field private final a:LqK;

.field private final a:Z


# direct methods
.method private constructor <init>(LamW;Laop;ZLZS;LqK;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LamW;",
            "Laop",
            "<-",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;Z",
            "LZS;",
            "LqK;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p2}, Lans;-><init>(Laop;)V

    .line 69
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamW;

    iput-object v0, p0, Lapj;->a:LamW;

    .line 70
    iput-boolean p3, p0, Lapj;->a:Z

    .line 71
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p0, Lapj;->a:LZS;

    .line 72
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p0, Lapj;->a:LqK;

    .line 73
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lapj;->a:Ljava/lang/String;

    .line 74
    return-void
.end method

.method synthetic constructor <init>(LamW;Laop;ZLZS;LqK;Ljava/lang/String;Lapk;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct/range {p0 .. p6}, Lapj;-><init>(LamW;Laop;ZLZS;LqK;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 79
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lapj;->a:LZS;

    invoke-interface {v0}, LZS;->b()V

    .line 84
    :try_start_0
    iget-object v0, p0, Lapj;->a:LqK;

    iget-object v1, p0, Lapj;->a:Ljava/lang/String;

    const-string v2, "thumbnailDownloadRequested"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 87
    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lapj;->a:LamW;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LamW;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lapj;->a:LamW;

    .line 91
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v2

    iget-boolean v3, p0, Lapj;->a:Z

    .line 90
    invoke-interface {v1, v2, v0, v3}, LamW;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    new-instance v1, LaoK;

    const-string v2, "An exception occured when retrieving an entry thumbnail URI."

    invoke-direct {v1, v2, v0}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 96
    :catch_1
    move-exception v0

    .line 97
    new-instance v1, LaoK;

    const-string v2, "An exception occured when retrieving an entry thumbnail URI."

    invoke-direct {v1, v2, v0}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, Lapj;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
