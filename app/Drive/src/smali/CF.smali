.class public LCF;
.super Ljava/lang/Object;
.source "LabelViewState.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/widget/TextView;

.field private a:Z

.field private final b:Landroid/widget/TextView;

.field private b:Z

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LCF;->a:Landroid/content/Context;

    .line 130
    sget v0, Lxc;->statusLabels:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LCF;->a:Landroid/widget/TextView;

    .line 131
    sget v0, Lxc;->sortLabel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LCF;->b:Landroid/widget/TextView;

    .line 132
    sget v0, Lxc;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LCF;->c:Landroid/widget/TextView;

    .line 133
    return-void
.end method

.method static synthetic a(LCF;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LCF;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LCF;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LCF;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(LCF;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LCF;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(LCF;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LCF;->c:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, LCF;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, LCF;->a:Z

    .line 101
    return-void
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, LCF;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, LCF;->a:Z

    .line 105
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, LCF;->b:Z

    .line 109
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, LCF;->b:Z

    .line 113
    return-void
.end method

.method e()V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0}, LCF;->b()V

    .line 117
    invoke-virtual {p0}, LCF;->d()V

    .line 118
    return-void
.end method

.method f()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 124
    iget-object v3, p0, LCF;->a:Landroid/widget/TextView;

    iget-boolean v0, p0, LCF;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, LCF;->b:Landroid/widget/TextView;

    iget-boolean v3, p0, LCF;->b:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    return-void

    :cond_0
    move v0, v2

    .line 124
    goto :goto_0

    :cond_1
    move v1, v2

    .line 125
    goto :goto_1
.end method
