.class public LTl;
.super Ljava/lang/Object;
.source "HttpContentSource.java"


# instance fields
.field private final a:LTF;


# direct methods
.method public constructor <init>(LTF;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LTl;->a:LTF;

    .line 43
    return-void
.end method

.method private a(Landroid/net/Uri;)Lorg/apache/http/HttpEntity;
    .locals 5

    .prologue
    .line 156
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 157
    const-string v1, "HttpContentSource"

    const-string v2, "HttpUtil.open %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 158
    iget-object v1, p0, LTl;->a:LTF;

    invoke-interface {v1, v0}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 159
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LTl;->a:LTF;

    invoke-interface {v0}, LTF;->b()V

    .line 164
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    .line 111
    :try_start_0
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v3, 0xc8

    if-eq v0, v3, :cond_1

    .line 112
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Http %s returns status %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 113
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 112
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, LTl;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    :cond_0
    throw v0

    .line 116
    :cond_1
    :try_start_1
    const-string v0, "HttpContentSource"

    const-string v3, "Downloading %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 117
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 118
    if-nez v0, :cond_2

    .line 119
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Http %s has no entity"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_2
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v3

    .line 124
    iget-object v4, p0, LTl;->a:LTF;

    invoke-interface {v4}, LTF;->a()Ljava/io/Closeable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :try_start_2
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 130
    invoke-virtual {v3, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 131
    new-instance v1, LTm;

    invoke-direct {v1, p0, v0, v3}, LTm;-><init>(LTl;Ljava/io/InputStream;Lbrm;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146
    return-object v1

    .line 140
    :catchall_1
    move-exception v0

    .line 141
    :try_start_3
    invoke-virtual {v3}, Lbrm;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 145
    :catchall_2
    move-exception v0

    move v1, v2

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 52
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :try_start_0
    invoke-direct {p0, p1}, LTl;->a(Landroid/net/Uri;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 55
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    .line 56
    const-string v2, "HttpContentSource"

    const-string v3, "Received contents for %s : encoding(%s), length(%d b)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x2

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    .line 56
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 64
    invoke-direct {p0}, LTl;->a()V

    :goto_1
    return-object v0

    .line 57
    :cond_0
    :try_start_1
    const-string v0, "none"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    :try_start_2
    const-string v1, "HttpContentSource"

    const-string v2, "Can\'t open or read string %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 62
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    invoke-direct {p0}, LTl;->a()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LTl;->a()V

    throw v0
.end method
