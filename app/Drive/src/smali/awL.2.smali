.class public final LawL;
.super Landroid/graphics/Paint;
.source "MosaicView.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/graphics/Paint;-><init>()V

    .line 55
    const v0, -0xffff01

    invoke-virtual {p0, v0}, LawL;->setColor(I)V

    .line 56
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, v0}, LawL;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p0, v0}, LawL;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 58
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, v0}, LawL;->setTextSize(F)V

    .line 59
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, LawL;->setStrokeWidth(F)V

    .line 60
    return-void
.end method
