.class public Ljz;
.super Ljava/lang/Object;
.source "AbsActionBarView.java"

# interfaces
.implements Lfn;


# instance fields
.field a:I

.field final synthetic a:Ljy;

.field private a:Z


# direct methods
.method protected constructor <init>(Ljy;)V
    .locals 1

    .prologue
    .line 255
    iput-object p1, p0, Ljz;->a:Ljy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljz;->a:Z

    return-void
.end method


# virtual methods
.method public a(LeZ;I)Ljz;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Ljz;->a:Ljy;

    iput-object p1, v0, Ljy;->a:LeZ;

    .line 262
    iput p2, p0, Ljz;->a:I

    .line 263
    return-object p0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Ljz;->a:Ljy;

    invoke-virtual {v0, v1}, Ljy;->setVisibility(I)V

    .line 269
    iput-boolean v1, p0, Ljz;->a:Z

    .line 270
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 274
    iget-boolean v0, p0, Ljz;->a:Z

    if-eqz v0, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget-object v0, p0, Ljz;->a:Ljy;

    const/4 v1, 0x0

    iput-object v1, v0, Ljy;->a:LeZ;

    .line 277
    iget-object v0, p0, Ljz;->a:Ljy;

    iget v1, p0, Ljz;->a:I

    invoke-virtual {v0, v1}, Ljy;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Ljz;->a:Ljy;

    iget-object v0, v0, Ljy;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljz;->a:Ljy;

    iget-object v0, v0, Ljy;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Ljz;->a:Ljy;

    iget-object v0, v0, Ljy;->a:Landroid/support/v7/widget/ActionMenuView;

    iget v1, p0, Ljz;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setVisibility(I)V

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljz;->a:Z

    .line 286
    return-void
.end method
