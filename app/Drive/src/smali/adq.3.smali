.class public Ladq;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements Ladj;


# instance fields
.field a:LacY;

.field private final a:LadY;

.field private final a:Ladw;

.field final synthetic a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/io/File;

.field private volatile a:Ljava/util/Date;

.field private volatile a:Z

.field private volatile b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 112
    iput-object p1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    sget-object v0, Ladz;->b:Ladz;

    invoke-virtual {p3}, Ladw;->a()Ladz;

    move-result-object v2

    invoke-virtual {v0, v2}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p3}, Ladw;->a()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 113
    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 115
    invoke-virtual {p3}, Ladw;->a()LadY;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iput-object p2, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 117
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladw;

    iput-object v0, p0, Ladq;->a:Ladw;

    .line 118
    invoke-virtual {p3}, Ladw;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ladq;->a:Ljava/io/File;

    .line 119
    invoke-virtual {p3}, Ladw;->a()LadY;

    move-result-object v0

    iput-object v0, p0, Ladq;->a:LadY;

    .line 120
    iput-boolean v1, p0, Ladq;->a:Z

    .line 121
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacY;

    iput-object v0, p0, Ladq;->a:LacY;

    .line 122
    iput-boolean p5, p0, Ladq;->c:Z

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Ladq;->a:Ljava/util/Date;

    .line 124
    iput-boolean v1, p0, Ladq;->b:Z

    .line 125
    return-void

    :cond_1
    move v0, v1

    .line 114
    goto :goto_0
.end method

.method private a(LaGp;LaGo;)V
    .locals 6

    .prologue
    .line 352
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 353
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    iget-object v1, p0, Ladq;->a:LacY;

    invoke-interface {p2, v1}, LaGo;->a(LacY;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGg;->a(J)LaGp;

    move-result-object v1

    .line 356
    if-eqz v1, :cond_0

    .line 357
    invoke-virtual {v1}, LaGp;->c()J

    move-result-wide v2

    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 356
    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 363
    if-eqz v1, :cond_1

    .line 364
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 365
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGp;)LaGp;

    move-result-object v0

    .line 366
    if-eqz v0, :cond_1

    .line 369
    invoke-virtual {p1, v0}, LaGp;->a(LaGp;)V

    .line 370
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;

    move-result-object v0

    invoke-interface {v0, p1}, LadE;->a(LaGp;)V

    .line 377
    :cond_1
    return-void

    .line 357
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 282
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v1

    .line 284
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->a()V

    .line 287
    :try_start_0
    iget-object v0, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 289
    :goto_0
    invoke-virtual {v1}, LaGp;->a()V

    .line 290
    if-eqz v0, :cond_3

    .line 291
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v2

    .line 292
    iget-boolean v3, p0, Ladq;->c:Z

    if-nez v3, :cond_6

    .line 297
    iget-object v3, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 298
    invoke-static {v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v3

    iget-object v4, p0, Ladq;->a:LacY;

    invoke-interface {v3, v0, v4}, LaGg;->a(LaGo;LacY;)LaGp;

    move-result-object v3

    .line 299
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LaGp;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->b()V

    .line 343
    :goto_1
    return-void

    .line 287
    :cond_0
    :try_start_1
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 288
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    iget-object v2, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v2}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_1
    invoke-direct {p0, v1, v0}, Ladq;->a(LaGp;LaGo;)V

    .line 310
    :goto_2
    invoke-virtual {v1}, LaGp;->c()J

    move-result-wide v4

    iget-object v0, p0, Ladq;->a:LacY;

    invoke-virtual {v2, v4, v5, v0}, LaGc;->a(JLacY;)LaGc;

    .line 311
    invoke-virtual {v2}, LaGc;->b()LaGb;

    move-result-object v0

    .line 315
    iget-boolean v2, p0, Ladq;->c:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Ladq;->a:LadY;

    invoke-virtual {v2}, LadY;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, LaGb;->f()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 317
    :cond_2
    :try_start_2
    iget-object v2, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Ladq;->a:Ladw;

    iget-object v4, p0, Ladq;->a:Ljava/util/Date;

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGo;Ladw;Ljava/util/Date;)V
    :try_end_2
    .catch LQm; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    :cond_3
    :goto_3
    :try_start_3
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 326
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LaGp;->b(J)V

    .line 328
    iget-boolean v0, p0, Ladq;->c:Z

    if-nez v0, :cond_4

    .line 329
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v0

    iget-object v2, p0, Ladq;->a:LadY;

    invoke-interface {v0, v2}, Lalo;->a(LadY;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LaGp;->d(J)V

    .line 331
    :cond_4
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;

    move-result-object v0

    invoke-interface {v0, v1}, LadE;->a(LaGp;)V

    .line 332
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->c()V

    .line 337
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LtK;

    move-result-object v0

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 338
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LMs;

    move-result-object v0

    invoke-virtual {v0}, LMs;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 341
    :cond_5
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->b()V

    goto/16 :goto_1

    .line 305
    :cond_6
    :try_start_4
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LaGc;->a(Ljava/lang/Long;)LaGc;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 341
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 318
    :catch_0
    move-exception v0

    .line 319
    :try_start_5
    const-string v2, "DocumentFileManager"

    const-string v3, "failed to start paging file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    .line 320
    :catch_1
    move-exception v0

    .line 321
    const-string v2, "DocumentFileManager"

    const-string v3, "failed to start paging file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_3
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 380
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v3

    .line 381
    invoke-virtual {v3}, LaGp;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, LaGp;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 386
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->a()V

    .line 388
    :try_start_0
    iget-object v0, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 389
    :goto_1
    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {v3}, LaGp;->d()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, LaGp;->e()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Ladq;->b:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Ladq;->c:Z

    if-eqz v4, :cond_1

    .line 394
    :cond_0
    iget-object v2, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v2

    invoke-interface {v2, v3}, LaGg;->a(LaGp;)V

    .line 395
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    .line 396
    invoke-virtual {v3}, LaGp;->c()J

    move-result-wide v2

    iget-object v4, p0, Ladq;->a:LacY;

    invoke-virtual {v0, v2, v3, v4}, LaGc;->a(JLacY;)LaGc;

    move-result-object v0

    iget-object v2, p0, Ladq;->a:Ladw;

    .line 397
    invoke-virtual {v2}, Ladw;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LaGc;->a(Ljava/lang/Long;)LaGc;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, LaGc;->b()LaGb;

    move-result-object v0

    move v2, v1

    .line 402
    :cond_1
    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->b()V

    .line 407
    if-eqz v2, :cond_2

    .line 409
    :try_start_1
    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Ladq;->a:Ladw;

    iget-object v3, p0, Ladq;->a:Ljava/util/Date;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGo;Ladw;Ljava/util/Date;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LQm; {:try_start_1 .. :try_end_1} :catch_1

    .line 416
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 381
    goto :goto_0

    .line 388
    :cond_4
    :try_start_2
    iget-object v0, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    iget-object v4, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v4}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 404
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v1

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 410
    :catch_0
    move-exception v0

    .line 411
    const-string v1, "DocumentFileManager"

    const-string v2, "Got an exception when trying to page cached file content to sdcard: "

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_2

    .line 412
    :catch_1
    move-exception v0

    .line 413
    const-string v1, "DocumentFileManager"

    const-string v2, "Got an exception when trying to page cached file content to sdcard: "

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaGp;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()LaGp;

    move-result-object v0

    return-object v0
.end method

.method public a()LadY;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Ladq;->a:LadY;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl$DocumentFileCloseTask;
    .locals 5

    .prologue
    .line 436
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 437
    new-instance v2, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    iget-object v3, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, p0, Ladq;->a:LacY;

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;JLacY;)V

    .line 439
    new-instance v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    invoke-direct {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;-><init>(Lcom/google/android/apps/docs/sync/filemanager/FileSpec;)V

    return-object v0
.end method

.method public bridge synthetic a()Lcom/google/android/apps/docs/utils/ParcelableTask;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Ladq;->a()Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl$DocumentFileCloseTask;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Ladq;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Ladq;->a:Ljava/io/File;

    return-object v0
.end method

.method public a()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 158
    invoke-virtual {p0}, Ladq;->a()Ljava/io/File;

    move-result-object v2

    .line 159
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 160
    if-eqz v1, :cond_0

    .line 161
    iget-object v2, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lalo;->a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;

    move-result-object v0

    .line 163
    :cond_0
    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladq;->a:Z

    .line 169
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Ladq;->a:Ljava/util/Date;

    .line 179
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Ladq;->a:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladq;->b:Z

    .line 210
    return-void
.end method

.method public close()V
    .locals 6

    .prologue
    .line 214
    iget-object v3, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    monitor-enter v3

    .line 216
    const-wide/16 v0, 0x0

    .line 218
    :try_start_0
    iget-object v2, p0, Ladq;->a:Ladw;

    invoke-virtual {v2}, Ladw;->a()Ladz;

    move-result-object v2

    sget-object v4, Ladz;->b:Ladz;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    .line 219
    :goto_0
    iget-object v4, p0, Ladq;->a:Ladw;

    invoke-virtual {v4}, Ladw;->c()V

    .line 220
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v4

    .line 221
    if-eqz v2, :cond_5

    .line 224
    iget-boolean v2, p0, Ladq;->a:Z

    if-eqz v2, :cond_3

    .line 225
    invoke-direct {p0}, Ladq;->c()V

    .line 227
    iget-object v2, p0, Ladq;->a:Ladw;

    invoke-virtual {v2}, Ladw;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()J

    move-result-wide v0

    .line 237
    :cond_0
    :goto_1
    iget-object v2, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :try_start_1
    iget-object v0, p0, Ladq;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()Ladz;

    move-result-object v0

    sget-object v1, Ladz;->c:Ladz;

    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 242
    iget-object v2, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 246
    return-void

    .line 218
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 231
    :cond_3
    :try_start_2
    iget-object v2, p0, Ladq;->a:Ladw;

    invoke-virtual {v2}, Ladw;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 240
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Ladq;->a:Ladw;

    invoke-virtual {v1}, Ladw;->a()Ladz;

    move-result-object v1

    sget-object v2, Ladz;->c:Ladz;

    invoke-virtual {v1, v2}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 241
    invoke-virtual {p0}, Ladq;->a()LaGp;

    move-result-object v1

    invoke-virtual {v1}, LaGp;->c()J

    move-result-wide v4

    .line 242
    iget-object v1, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_4
    throw v0

    .line 245
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 233
    :cond_5
    :try_start_4
    invoke-virtual {v4}, LaGp;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v4}, LaGp;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    invoke-direct {p0}, Ladq;->d()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 425
    const-string v0, "DocumentFileImpl[%s, manager=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ladq;->a:Ladw;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ladq;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
