.class public Lagt;
.super LafC;
.source "EditorOfflineDbSyncable.java"


# instance fields
.field private final a:LaGM;

.field private final a:LacS;

.field private final a:LadQ;


# direct methods
.method public constructor <init>(LahJ;LacS;LaGM;LagH;LadQ;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p4}, LafC;-><init>(LahJ;LagH;)V

    .line 37
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacS;

    iput-object v0, p0, Lagt;->a:LacS;

    .line 38
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, Lagt;->a:LaGM;

    .line 39
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadQ;

    iput-object v0, p0, Lagt;->a:LadQ;

    .line 40
    return-void
.end method

.method private d()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 63
    iget-object v2, p0, Lagt;->a:LaGM;

    invoke-virtual {p0}, Lagt;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v2, v3}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v2

    .line 64
    if-nez v2, :cond_0

    .line 65
    invoke-virtual {p0}, Lagt;->a()V

    .line 118
    :goto_0
    return v0

    .line 69
    :cond_0
    invoke-virtual {p0}, Lagt;->b()V

    .line 76
    invoke-interface {v2}, LaGo;->b()Ljava/util/Date;

    move-result-object v3

    .line 77
    invoke-interface {v2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    .line 78
    invoke-interface {v2}, LaGo;->b()Z

    move-result v5

    .line 79
    if-eqz v5, :cond_1

    .line 80
    const-string v6, "EditorOfflineDbSyncable"

    const-string v7, "Syncing a local only document %s"

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v4, v8, v0

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 84
    :goto_1
    iget-object v6, p0, Lagt;->a:LadQ;

    invoke-interface {v6, v4}, LadQ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LadR;

    move-result-object v6

    .line 85
    if-nez v6, :cond_2

    .line 86
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to open or create a document file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    const-string v6, "EditorOfflineDbSyncable"

    const-string v7, "Syncing a remote document %s"

    new-array v8, v1, [Ljava/lang/Object;

    invoke-interface {v2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v9

    aput-object v9, v8, v0

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 90
    :cond_2
    invoke-virtual {p0, v10, v11, v10, v11}, Lagt;->a(JJ)V

    .line 92
    iget-object v4, v4, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 93
    invoke-interface {v6}, LadR;->a()Ljava/lang/String;

    move-result-object v7

    .line 94
    const-string v8, "EditorOfflineDbSyncable"

    const-string v9, "Created/Opened document file for doc %s"

    new-array v10, v1, [Ljava/lang/Object;

    aput-object v7, v10, v0

    invoke-static {v8, v9, v10}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 95
    iget-object v8, p0, Lagt;->a:LacS;

    invoke-interface {v8, v6, v4, v7}, LacS;->a(LadR;LaFO;Ljava/lang/String;)LacT;

    move-result-object v4

    .line 96
    sget-object v8, LacT;->a:LacT;

    if-ne v4, v8, :cond_5

    .line 97
    const-string v4, "EditorOfflineDbSyncable"

    const-string v8, "Sync of %s completed successfuly."

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v7, v1, v0

    invoke-static {v4, v8, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    if-eqz v5, :cond_3

    .line 99
    invoke-interface {v6}, LadR;->c()V

    .line 101
    :cond_3
    invoke-interface {v6, v3}, LadR;->a(Ljava/util/Date;)V

    .line 102
    invoke-interface {v6}, LadR;->a()V

    .line 103
    invoke-interface {v2}, LaGo;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 106
    invoke-interface {v6}, LadR;->b()V

    .line 108
    :cond_4
    invoke-interface {v6}, LadR;->close()V

    .line 109
    invoke-virtual {p0}, Lagt;->c()V

    goto :goto_0

    .line 112
    :cond_5
    const-string v2, "EditorOfflineDbSyncable"

    const-string v3, "Sync of %s failed. SyncResult: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v7, v5, v0

    aput-object v4, v5, v1

    invoke-static {v2, v3, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 114
    invoke-interface {v6}, LadR;->close()V

    .line 115
    sget-object v0, LacT;->b:LacT;

    if-ne v4, v0, :cond_6

    move v0, v1

    .line 118
    goto/16 :goto_0

    .line 121
    :cond_6
    sget-object v0, LafT;->o:LafT;

    invoke-virtual {p0, v0}, Lagt;->a(LafT;)V

    .line 122
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to sync "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 5

    .prologue
    .line 51
    invoke-virtual {p0}, Lagt;->a()LahJ;

    move-result-object v0

    invoke-interface {v0}, LahJ;->a()LahS;

    move-result-object v0

    .line 52
    sget-object v1, Lagu;->a:[I

    invoke-virtual {v0}, LahS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 58
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid task type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :pswitch_0
    const-string v1, "EditorOfflineDbSyncable"

    const-string v2, "Sync request for EditorOfflineDbSyncable: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 56
    invoke-direct {p0}, Lagt;->d()Z

    move-result v0

    return v0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
