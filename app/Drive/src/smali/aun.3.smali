.class public Laun;
.super Ljava/lang/Object;
.source "CloudPrintDialog.java"

# interfaces
.implements Lauv;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final a:Landroid/webkit/WebView;

.field private final a:Lavl;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lavl;)V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Laun;->a:Landroid/app/Activity;

    .line 67
    iput-object p2, p0, Laun;->a:Lavl;

    .line 69
    sget v0, Lauk;->dialog_print:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setContentView(I)V

    .line 70
    sget v0, Lauj;->print_webview:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Laun;->a:Landroid/webkit/WebView;

    .line 71
    iget-object v0, p0, Laun;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 72
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 74
    iget-object v0, p0, Laun;->a:Landroid/webkit/WebView;

    new-instance v1, Lauo;

    invoke-direct {v1, p0}, Lauo;-><init>(Laun;)V

    const-string v2, "AndroidPrintDialog"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Laun;->a:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Laun;->a(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method static synthetic a(Laun;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laun;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private a()Landroid/net/Uri$Builder;
    .locals 3

    .prologue
    .line 152
    const-string v0, "https://www.google.com/cloudprint/dialog.html"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 153
    const-string v1, "skin"

    const-string v2, "holo"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 154
    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Laun;->a()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 162
    invoke-direct {p0}, Laun;->a()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 163
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 164
    const-string v1, "contentType"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 165
    const-string v1, "content"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide v6, 0x3fc999999999999aL    # 0.2

    const-wide v4, 0x3fa999999999999aL    # 0.05

    .line 79
    .line 82
    iget-object v0, p0, Laun;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 83
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 84
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 86
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 87
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 89
    iget-object v2, p0, Laun;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 90
    invoke-static {v2}, Laun;->b(Landroid/content/res/Resources;)Z

    move-result v3

    .line 91
    invoke-static {v2}, Laun;->a(Landroid/content/res/Resources;)Z

    move-result v2

    .line 94
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 95
    int-to-double v2, v1

    const-wide v4, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v2, v4

    int-to-double v0, v0

    mul-double v4, v0, v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Laun;->a(Landroid/view/View;DD)V

    .line 103
    :goto_0
    return-void

    .line 96
    :cond_0
    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    .line 97
    int-to-double v2, v1

    mul-double/2addr v2, v4

    int-to-double v0, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Laun;->a(Landroid/view/View;DD)V

    goto :goto_0

    .line 98
    :cond_1
    if-nez v3, :cond_2

    if-eqz v2, :cond_2

    .line 99
    int-to-double v2, v1

    mul-double/2addr v2, v6

    int-to-double v0, v0

    mul-double v4, v0, v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Laun;->a(Landroid/view/View;DD)V

    goto :goto_0

    .line 101
    :cond_2
    int-to-double v2, v1

    mul-double/2addr v2, v4

    int-to-double v0, v0

    mul-double/2addr v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Laun;->a(Landroid/view/View;DD)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;DD)V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 107
    double-to-int v1, p4

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 108
    double-to-int v1, p4

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 109
    double-to-int v1, p2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 110
    double-to-int v1, p2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 111
    return-void
.end method

.method private static a(Landroid/content/res/Resources;)Z
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/res/Resources;)Z
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 120
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;I)V
    .locals 7

    .prologue
    .line 128
    new-instance v0, LavN;

    iget-object v1, p0, Laun;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LavN;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LavN;->a()Landroid/accounts/Account;

    move-result-object v5

    .line 129
    iget-object v0, p3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lavl;->a(Landroid/net/Uri;)Z

    move-result v6

    .line 130
    if-eqz v6, :cond_0

    .line 131
    iget-object v0, p0, Laun;->a:Lavl;

    invoke-virtual {v0, p3}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v0

    .line 134
    :try_start_0
    invoke-virtual {v0}, Lavk;->a()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 138
    new-instance v0, Laup;

    iget-object v1, p0, Laun;->a:Landroid/app/Activity;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Laup;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V

    .line 142
    :goto_0
    iget-object v1, p0, Laun;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 143
    if-eqz v6, :cond_1

    invoke-direct {p0}, Laun;->a()Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_1
    iget-object v1, p0, Laun;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lavs;->b(Landroid/content/Context;)V

    .line 145
    iget-object v1, p0, Laun;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 146
    return-void

    .line 135
    :catch_0
    move-exception v1

    .line 136
    new-instance v1, Lauu;

    iget-object v2, p0, Laun;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Lavk;->a()I

    move-result v0

    int-to-long v4, v0

    invoke-direct {v1, v2, p1, v4, v5}, Lauu;-><init>(Landroid/content/res/Resources;Ljava/lang/String;J)V

    throw v1

    .line 140
    :cond_0
    new-instance v0, Lauq;

    iget-object v1, p0, Laun;->a:Landroid/app/Activity;

    invoke-direct {v0, v1, v5}, Lauq;-><init>(Landroid/app/Activity;Landroid/accounts/Account;)V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Laun;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
