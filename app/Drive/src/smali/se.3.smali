.class public Lse;
.super Ljava/lang/Object;
.source "DelayedSpinnerProgressDialog.java"


# direct methods
.method public static a(Landroid/app/Activity;LbsU;Ljava/lang/String;)Landroid/app/ProgressDialog;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LbsU",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/ProgressDialog;"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 25
    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 26
    new-instance v1, Lsf;

    invoke-direct {v1, p1}, Lsf;-><init>(LbsU;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 33
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lsg;

    invoke-direct {v2, p1, p0, v0}, Lsg;-><init>(LbsU;Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 42
    return-object v0
.end method
