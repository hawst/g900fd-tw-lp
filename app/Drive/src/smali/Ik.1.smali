.class public LIk;
.super Ljava/lang/Object;
.source "FastScroller.java"


# static fields
.field private static a:I

.field private static final a:[I

.field private static final b:[I


# instance fields
.field private final a:LIl;

.field private final a:LIm;

.field private a:LIn;

.field private a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/RectF;

.field private a:Landroid/graphics/drawable/Drawable;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/AbsListView;

.field private a:Landroid/widget/ListAdapter;

.field private a:Landroid/widget/SectionIndexer;

.field a:Ljava/lang/String;

.field private a:Z

.field private a:[Ljava/lang/Object;

.field private b:I

.field private b:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:I

.field private c:Landroid/graphics/drawable/Drawable;

.field private c:Z

.field private d:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    const/4 v0, 0x4

    sput v0, LIk;->a:I

    .line 78
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, LIk;->a:[I

    .line 82
    new-array v0, v2, [I

    sput-object v0, LIk;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;LIm;LIl;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, -0x1

    iput v0, p0, LIk;->h:I

    .line 110
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LIk;->a:Landroid/os/Handler;

    .line 124
    iput-object p2, p0, LIk;->a:Landroid/widget/AbsListView;

    .line 125
    iput-object p3, p0, LIk;->a:Landroid/view/View;

    .line 126
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIm;

    iput-object v0, p0, LIk;->a:LIm;

    .line 127
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIl;

    iput-object v0, p0, LIk;->a:LIl;

    .line 128
    invoke-direct {p0, p1}, LIk;->a(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 163
    sget-object v0, LIl;->a:LIl;

    iget-object v1, p0, LIk;->a:LIl;

    invoke-virtual {v0, v1}, LIl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget p1, p0, LIk;->c:I

    :cond_0
    return p1
.end method

.method private a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 231
    invoke-static {p1}, LakQ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    :try_start_0
    const-class v0, Landroid/content/Context;

    const-string v2, "getDrawable"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 236
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    return-object v0

    .line 237
    :catch_0
    move-exception v0

    .line 241
    :cond_0
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(LIk;)Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    return-object v0
.end method

.method private a(F)V
    .locals 13

    .prologue
    .line 424
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v7

    .line 425
    const/4 v0, 0x0

    iput-boolean v0, p0, LIk;->a:Z

    .line 426
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v7

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000    # 8.0f

    div-float v8, v0, v1

    .line 427
    iget-object v9, p0, LIk;->a:[Ljava/lang/Object;

    .line 430
    if-eqz v9, :cond_6

    array-length v0, v9

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    .line 431
    array-length v10, v9

    .line 432
    int-to-float v0, v10

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 433
    if-lt v0, v10, :cond_0

    .line 434
    add-int/lit8 v0, v10, -0x1

    .line 438
    :cond_0
    iget-object v1, p0, LIk;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v1, v0}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v1

    .line 448
    add-int/lit8 v5, v0, 0x1

    .line 450
    add-int/lit8 v2, v10, -0x1

    if-ge v0, v2, :cond_10

    .line 451
    iget-object v2, p0, LIk;->a:Landroid/widget/SectionIndexer;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v2, v3}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v2

    move v6, v2

    .line 455
    :goto_0
    if-ne v6, v1, :cond_f

    move v2, v1

    move v3, v0

    .line 457
    :goto_1
    if-lez v3, :cond_e

    .line 458
    add-int/lit8 v2, v3, -0x1

    .line 459
    iget-object v3, p0, LIk;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v3, v2}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v3

    .line 460
    if-eq v3, v1, :cond_2

    move v1, v2

    move v12, v3

    move v3, v2

    move v2, v12

    .line 473
    :goto_2
    if-nez v3, :cond_1

    .line 474
    iget-object v3, p0, LIk;->a:Landroid/widget/SectionIndexer;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v3

    .line 482
    :cond_1
    :goto_3
    add-int/lit8 v4, v5, 0x1

    .line 483
    :goto_4
    if-ge v4, v10, :cond_3

    iget-object v11, p0, LIk;->a:Landroid/widget/SectionIndexer;

    .line 484
    invoke-interface {v11, v4}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v11

    if-ne v11, v6, :cond_3

    .line 485
    add-int/lit8 v4, v4, 0x1

    .line 486
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 464
    :cond_2
    if-nez v2, :cond_d

    .line 467
    const/4 v2, 0x0

    move v1, v0

    move v12, v3

    move v3, v2

    move v2, v12

    .line 468
    goto :goto_2

    .line 491
    :cond_3
    int-to-float v4, v1

    int-to-float v11, v10

    div-float/2addr v4, v11

    .line 492
    int-to-float v5, v5

    int-to-float v10, v10

    div-float/2addr v5, v10

    .line 493
    if-ne v1, v0, :cond_5

    sub-float v0, p1, v4

    cmpg-float v0, v0, v8

    if-gez v0, :cond_5

    move v0, v2

    .line 500
    :goto_5
    add-int/lit8 v1, v7, -0x1

    if-le v0, v1, :cond_4

    add-int/lit8 v0, v7, -0x1

    :cond_4
    move v1, v0

    .line 507
    :goto_6
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_7

    .line 508
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ExpandableListView;

    .line 509
    iget v2, p0, LIk;->g:I

    add-int/2addr v1, v2

    .line 510
    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v4

    .line 509
    invoke-virtual {v0, v4, v5}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 517
    :goto_7
    if-ltz v3, :cond_c

    .line 518
    if-nez v9, :cond_9

    .line 519
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sectionIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for null sections. This should be impossible."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 496
    :cond_5
    sub-int v0, v6, v2

    int-to-float v0, v0

    sub-float v1, p1, v4

    mul-float/2addr v0, v1

    sub-float v1, v5, v4

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    goto :goto_5

    .line 503
    :cond_6
    int-to-float v0, v7

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 504
    const/4 v3, -0x1

    move v1, v0

    goto :goto_6

    .line 511
    :cond_7
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_8

    .line 512
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v2, p0, LIk;->g:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_7

    .line 514
    :cond_8
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    iget v2, p0, LIk;->g:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_7

    .line 522
    :cond_9
    aget-object v0, v9, v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LIk;->a:Ljava/lang/String;

    .line 523
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_b

    :cond_a
    array-length v0, v9

    if-ge v3, v0, :cond_b

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, LIk;->c:Z

    .line 528
    :goto_9
    return-void

    .line 523
    :cond_b
    const/4 v0, 0x0

    goto :goto_8

    .line 526
    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, LIk;->c:Z

    goto :goto_9

    :cond_d
    move v12, v3

    move v3, v2

    move v2, v12

    goto/16 :goto_1

    :cond_e
    move v1, v0

    move v3, v0

    goto/16 :goto_2

    :cond_f
    move v2, v1

    move v3, v0

    move v1, v0

    goto/16 :goto_3

    :cond_10
    move v6, v7

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 192
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 193
    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 195
    sget v2, Lxb;->scrollbar_handle_accelerated_anim2:I

    invoke-virtual {v1, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 196
    invoke-direct {p0, p1, v2}, LIk;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, p1, v2}, LIk;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LwY;->show_fastscroller_track:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    .line 202
    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 204
    sget v1, Lxb;->menu_submenu_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LIk;->c:Landroid/graphics/drawable/Drawable;

    .line 206
    iput-boolean v5, p0, LIk;->a:Z

    .line 208
    invoke-virtual {p0}, LIk;->c()V

    .line 210
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->fastscroll_overlay_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LIk;->e:I

    .line 212
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LIk;->a:Landroid/graphics/RectF;

    .line 213
    new-instance v0, LIn;

    invoke-direct {v0, p0}, LIn;-><init>(LIk;)V

    iput-object v0, p0, LIk;->a:LIn;

    .line 214
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    .line 215
    iget-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 216
    iget-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 217
    iget-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    iget v1, p0, LIk;->e:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v5, [I

    const v2, 0x1010036

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 220
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 221
    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    .line 222
    iget-object v1, p0, LIk;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 223
    iget-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 225
    iput v4, p0, LIk;->i:I

    .line 226
    invoke-direct {p0}, LIk;->e()V

    .line 227
    return-void

    .line 200
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 192
    :array_0
    .array-data 4
        0x1010336
        0x1010339
    .end array-data
.end method

.method private a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 181
    iput-object p2, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    .line 182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->fastscroll_thumb_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LIk;->c:I

    .line 184
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->fastscroll_thumb_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LIk;->b:I

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, LIk;->d:Z

    .line 187
    return-void
.end method

.method private b()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, LIk;->a(I)I

    move-result v0

    return v0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 171
    sget-object v0, LIl;->a:LIl;

    iget-object v1, p0, LIk;->a:LIl;

    invoke-virtual {v0, v1}, LIl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LIk;->c:I

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method private c()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, LIk;->b(I)I

    move-result v0

    return v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 176
    iget-object v0, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, LIk;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0}, LIk;->b()I

    move-result v3

    iget v4, p0, LIk;->b:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 177
    iget-object v0, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 178
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 245
    iget v0, p0, LIk;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    sget-object v0, LIk;->a:[I

    .line 247
    :goto_0
    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 250
    :cond_0
    iget-object v1, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 253
    :cond_1
    return-void

    .line 245
    :cond_2
    sget-object v0, LIk;->b:[I

    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    .line 532
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 533
    iget-object v1, p0, LIk;->a:Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 534
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 535
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, LIk;->i:I

    return v0
.end method

.method a()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, LIk;->a:LIm;

    sget-object v1, LIm;->a:LIm;

    invoke-virtual {v0, v1}, LIm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LIk;->a(I)V

    .line 259
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 132
    packed-switch p1, :pswitch_data_0

    .line 150
    :goto_0
    :pswitch_0
    iput p1, p0, LIk;->i:I

    .line 151
    invoke-direct {p0}, LIk;->e()V

    .line 152
    return-void

    .line 134
    :pswitch_1
    iget-object v0, p0, LIk;->a:Landroid/os/Handler;

    iget-object v1, p0, LIk;->a:LIn;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 135
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 138
    :pswitch_2
    iget v0, p0, LIk;->i:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 139
    invoke-direct {p0}, LIk;->d()V

    .line 143
    :cond_0
    :pswitch_3
    iget-object v0, p0, LIk;->a:Landroid/os/Handler;

    iget-object v1, p0, LIk;->a:LIn;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 146
    :pswitch_4
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-direct {p0}, LIk;->c()I

    move-result v1

    iget v2, p0, LIk;->d:I

    .line 147
    invoke-direct {p0}, LIk;->b()I

    move-result v3

    iget v4, p0, LIk;->d:I

    iget v5, p0, LIk;->b:I

    add-int/2addr v4, v5

    .line 146
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method a(IIII)V
    .locals 5

    .prologue
    .line 335
    iget-object v0, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, LIk;->b(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, LIk;->a(I)I

    move-result v3

    iget v4, p0, LIk;->b:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 338
    :cond_0
    iget-object v0, p0, LIk;->a:Landroid/graphics/RectF;

    .line 339
    iget v1, p0, LIk;->e:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 340
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, LIk;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 341
    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 342
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, LIk;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 343
    iget-object v1, p0, LIk;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, LIk;->c:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 347
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 271
    iget v0, p0, LIk;->i:I

    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget v4, p0, LIk;->d:I

    .line 277
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 278
    iget-object v3, p0, LIk;->a:LIn;

    .line 280
    const/4 v1, -0x1

    .line 281
    iget v5, p0, LIk;->i:I

    if-ne v5, v9, :cond_7

    .line 282
    invoke-virtual {v3}, LIn;->a()I

    move-result v3

    .line 283
    const/16 v1, 0x68

    if-ge v3, v1, :cond_2

    .line 284
    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    mul-int/lit8 v5, v3, 0x2

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 287
    :cond_2
    iget-object v1, p0, LIk;->a:LIl;

    sget-object v5, LIl;->a:LIl;

    invoke-virtual {v1, v5}, LIl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 289
    iget v0, p0, LIk;->c:I

    mul-int/2addr v0, v3

    div-int/lit16 v0, v0, 0xd0

    move v1, v2

    .line 294
    :goto_1
    iget-object v5, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    iget v6, p0, LIk;->b:I

    invoke-virtual {v5, v1, v2, v0, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, LIk;->d:Z

    move v0, v3

    .line 298
    :goto_2
    iget-object v1, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 299
    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 300
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 301
    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    .line 302
    iget-object v5, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 303
    iget v6, p0, LIk;->c:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    div-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    .line 304
    iget-object v6, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    add-int/2addr v5, v3

    iget-object v7, p0, LIk;->a:Landroid/widget/AbsListView;

    .line 305
    invoke-virtual {v7}, Landroid/widget/AbsListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v1

    .line 304
    invoke-virtual {v6, v3, v1, v5, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 306
    iget-object v1, p0, LIk;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 309
    :cond_3
    int-to-float v1, v4

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 310
    iget-object v1, p0, LIk;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 311
    neg-int v1, v4

    int-to-float v1, v1

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 314
    iget v1, p0, LIk;->i:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, LIk;->c:Z

    if-eqz v1, :cond_5

    .line 315
    iget-object v0, p0, LIk;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, LIk;->a(Landroid/graphics/Canvas;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 291
    :cond_4
    iget v1, p0, LIk;->c:I

    mul-int/2addr v1, v3

    div-int/lit16 v1, v1, 0xd0

    sub-int v1, v0, v1

    .line 292
    goto :goto_1

    .line 316
    :cond_5
    iget v1, p0, LIk;->i:I

    if-ne v1, v9, :cond_0

    .line 317
    if-nez v0, :cond_6

    .line 318
    invoke-virtual {p0, v2}, LIk;->a(I)V

    goto/16 :goto_0

    .line 320
    :cond_6
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-direct {p0}, LIk;->c()I

    move-result v1

    invoke-direct {p0}, LIk;->b()I

    move-result v2

    iget v3, p0, LIk;->b:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 326
    iget-object v0, p0, LIk;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 327
    iget-object v0, p0, LIk;->a:Landroid/graphics/Paint;

    .line 328
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v1

    .line 329
    iget-object v2, p0, LIk;->a:Landroid/graphics/RectF;

    .line 330
    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v4

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    iget v4, p0, LIk;->e:I

    div-int/lit8 v4, v4, 0x4

    add-int/2addr v2, v4

    int-to-float v2, v2

    sub-float v1, v2, v1

    invoke-virtual {p1, p2, v3, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 332
    return-void
.end method

.method a(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 352
    iget v0, p0, LIk;->h:I

    if-eq v0, p4, :cond_0

    if-lez p3, :cond_0

    .line 353
    iput p4, p0, LIk;->h:I

    .line 354
    iget v0, p0, LIk;->h:I

    div-int/2addr v0, p3

    sget v3, LIk;->a:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LIk;->b:Z

    .line 356
    :cond_0
    iget-boolean v0, p0, LIk;->b:Z

    if-nez v0, :cond_3

    .line 357
    iget v0, p0, LIk;->i:I

    if-eqz v0, :cond_1

    .line 358
    invoke-virtual {p0, v2}, LIk;->a(I)V

    .line 385
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 354
    goto :goto_0

    .line 362
    :cond_3
    sub-int v0, p4, p3

    if-lez v0, :cond_5

    iget v0, p0, LIk;->i:I

    if-eq v0, v5, :cond_5

    .line 363
    iget v0, p0, LIk;->d:I

    .line 364
    iget-object v3, p0, LIk;->a:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getHeight()I

    move-result v3

    iget v4, p0, LIk;->b:I

    sub-int/2addr v3, v4

    mul-int/2addr v3, p2

    sub-int v4, p4, p3

    div-int/2addr v3, v4

    iput v3, p0, LIk;->d:I

    .line 366
    iget-boolean v3, p0, LIk;->d:Z

    if-eqz v3, :cond_4

    .line 367
    invoke-direct {p0}, LIk;->d()V

    .line 368
    iput-boolean v2, p0, LIk;->d:Z

    .line 370
    :cond_4
    iget v2, p0, LIk;->d:I

    if-eq v2, v0, :cond_5

    .line 371
    iget-object v0, p0, LIk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 374
    :cond_5
    iput-boolean v1, p0, LIk;->a:Z

    .line 375
    iget v0, p0, LIk;->f:I

    if-eq p2, v0, :cond_1

    .line 378
    iput p2, p0, LIk;->f:I

    .line 379
    iget v0, p0, LIk;->i:I

    if-eq v0, v5, :cond_1

    .line 380
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LIk;->a(I)V

    .line 381
    iget-object v0, p0, LIk;->a:LIm;

    sget-object v1, LIm;->b:LIm;

    invoke-virtual {v0, v1}, LIm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, LIk;->a:Landroid/os/Handler;

    iget-object v1, p0, LIk;->a:LIn;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method a()Z
    .locals 1

    .prologue
    .line 266
    iget v0, p0, LIk;->i:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(FF)Z
    .locals 2

    .prologue
    .line 599
    invoke-direct {p0}, LIk;->c()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, LIk;->b()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, LIk;->d:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, LIk;->d:I

    iget v1, p0, LIk;->b:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 538
    iget v0, p0, LIk;->i:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 539
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, LIk;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LIk;->a(I)V

    .line 541
    const/4 v0, 0x1

    .line 544
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LIk;->a(I)V

    .line 263
    return-void
.end method

.method b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 548
    iget v1, p0, LIk;->i:I

    if-nez v1, :cond_1

    .line 595
    :cond_0
    :goto_0
    return v0

    .line 552
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 554
    if-nez v1, :cond_3

    .line 555
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v1, v3}, LIk;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    invoke-virtual {p0, v4}, LIk;->a(I)V

    .line 557
    iget-object v0, p0, LIk;->a:Landroid/widget/ListAdapter;

    if-nez v0, :cond_2

    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 558
    invoke-virtual {p0}, LIk;->c()V

    .line 561
    :cond_2
    invoke-direct {p0}, LIk;->f()V

    move v0, v2

    .line 562
    goto :goto_0

    .line 564
    :cond_3
    if-ne v1, v2, :cond_5

    .line 565
    iget v1, p0, LIk;->i:I

    if-ne v1, v4, :cond_0

    .line 566
    invoke-virtual {p0, v5}, LIk;->a(I)V

    .line 567
    iget-object v0, p0, LIk;->a:Landroid/os/Handler;

    .line 568
    iget-object v1, p0, LIk;->a:LIn;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 569
    iget-object v1, p0, LIk;->a:LIm;

    sget-object v3, LIm;->b:LIm;

    invoke-virtual {v1, v3}, LIm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570
    iget-object v1, p0, LIk;->a:LIn;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    move v0, v2

    .line 572
    goto :goto_0

    .line 574
    :cond_5
    if-ne v1, v5, :cond_0

    .line 575
    iget v1, p0, LIk;->i:I

    if-ne v1, v4, :cond_0

    .line 576
    iget-object v1, p0, LIk;->a:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getHeight()I

    move-result v3

    .line 578
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget v4, p0, LIk;->b:I

    sub-int/2addr v1, v4

    add-int/lit8 v1, v1, 0xa

    .line 579
    if-gez v1, :cond_6

    .line 584
    :goto_1
    iget v1, p0, LIk;->d:I

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v5, :cond_7

    move v0, v2

    .line 585
    goto :goto_0

    .line 581
    :cond_6
    iget v0, p0, LIk;->b:I

    add-int/2addr v0, v1

    if-le v0, v3, :cond_9

    .line 582
    iget v0, p0, LIk;->b:I

    sub-int v0, v3, v0

    goto :goto_1

    .line 587
    :cond_7
    iput v0, p0, LIk;->d:I

    .line 589
    iget-boolean v0, p0, LIk;->a:Z

    if-eqz v0, :cond_8

    .line 590
    iget v0, p0, LIk;->d:I

    int-to-float v0, v0

    iget v1, p0, LIk;->b:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, LIk;->a(F)V

    :cond_8
    move v0, v2

    .line 592
    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_1
.end method

.method c()V
    .locals 3

    .prologue
    .line 399
    iget-object v0, p0, LIk;->a:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 400
    const/4 v1, 0x0

    iput-object v1, p0, LIk;->a:Landroid/widget/SectionIndexer;

    .line 401
    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 402
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v1

    iput v1, p0, LIk;->g:I

    .line 403
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 413
    :cond_0
    iput-object v0, p0, LIk;->a:Landroid/widget/ListAdapter;

    .line 415
    instance-of v1, v0, Landroid/widget/SectionIndexer;

    if-eqz v1, :cond_1

    .line 416
    check-cast v0, Landroid/widget/SectionIndexer;

    iput-object v0, p0, LIk;->a:Landroid/widget/SectionIndexer;

    .line 417
    iget-object v0, p0, LIk;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LIk;->a:[Ljava/lang/Object;

    .line 421
    :goto_0
    return-void

    .line 419
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    iput-object v0, p0, LIk;->a:[Ljava/lang/Object;

    goto :goto_0
.end method
