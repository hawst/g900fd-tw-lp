.class public LacR;
.super Ljava/lang/Object;
.source "ShortcutCreatorImpl.java"

# interfaces
.implements Lvq;


# annotations
.annotation runtime LbuO;
.end annotation


# instance fields
.field private final a:I

.field private final a:LaGM;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LaGM;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p2, p0, LacR;->a:LaGM;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 47
    sget v1, LatK;->shortcut_icon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LacR;->a:I

    .line 48
    sget v1, LatK;->shortcut_file_icon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LacR;->b:I

    .line 49
    sget v1, LatK;->shortcut_file_icon_inset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LacR;->c:I

    .line 50
    sget v1, LatK;->shortcut_arrow_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LacR;->d:I

    .line 51
    sget v1, LatK;->shortcut_arrow_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LacR;->e:I

    .line 52
    return-void
.end method

.method private a(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 109
    iget v2, p0, LacR;->c:I

    iget v3, p0, LacR;->c:I

    iget v4, p0, LacR;->c:I

    iget v5, p0, LacR;->b:I

    add-int/2addr v4, v5

    iget v5, p0, LacR;->c:I

    iget v6, p0, LacR;->b:I

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 112
    sget v2, LatL;->ic_shortcut_bg:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 113
    iget v3, p0, LacR;->a:I

    iget v4, p0, LacR;->a:I

    invoke-virtual {v2, v7, v7, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 115
    sget v3, LatL;->ic_shortcut_arrow:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 116
    iget v3, p0, LacR;->c:I

    iget v4, p0, LacR;->d:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    .line 117
    iget v4, p0, LacR;->c:I

    iget v5, p0, LacR;->b:I

    add-int/2addr v4, v5

    iget v5, p0, LacR;->e:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    .line 118
    iget v5, p0, LacR;->d:I

    add-int/2addr v5, v3

    iget v6, p0, LacR;->e:I

    add-int/2addr v6, v4

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 120
    new-instance v3, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v4, v7

    const/4 v2, 0x1

    aput-object v1, v4, v2

    const/4 v1, 0x2

    aput-object v0, v4, v1

    invoke-direct {v3, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget v0, p0, LacR;->a:I

    iget v1, p0, LacR;->a:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 123
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 124
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/LayerDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 125
    return-object v0
.end method

.method private a(Landroid/content/Intent;Landroid/content/Context;LaGu;)V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, LacR;->a:LaGM;

    .line 89
    invoke-interface {p3}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 88
    invoke-static {p2, v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 91
    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 93
    invoke-interface {p3}, LaGu;->a()LaGv;

    move-result-object v0

    .line 94
    invoke-interface {p3}, LaGu;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3}, LaGu;->d()Z

    move-result v2

    .line 93
    invoke-static {v0, v1, v2}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 95
    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-direct {p0, p2, v0}, LacR;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 96
    const-string v0, "android.intent.extra.shortcut.NAME"

    invoke-interface {p3}, LaGu;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 57
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, LacR;->a:LaGM;

    invoke-interface {v0, p2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v1

    .line 61
    if-nez v1, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    .line 64
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    invoke-direct {p0, v0, p1, v1}, LacR;->a(Landroid/content/Intent;Landroid/content/Context;LaGu;)V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 72
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, LacR;->a:LaGM;

    invoke-interface {v0, p2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v1

    .line 76
    if-nez v1, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    .line 79
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, v0, p1, v1}, LacR;->a(Landroid/content/Intent;Landroid/content/Context;LaGu;)V

    goto :goto_0
.end method
