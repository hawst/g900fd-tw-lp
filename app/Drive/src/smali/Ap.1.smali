.class public LAp;
.super Ljava/lang/Object;
.source "CreateBarControllerImpl.java"

# interfaces
.implements LAn;
.implements LKe;


# instance fields
.field private a:F

.field private a:I

.field a:LJR;

.field a:LaGR;

.field private a:Landroid/view/View;

.field private final a:Landroid/view/animation/Animation$AnimationListener;

.field a:Lwm;

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x2

    iput v0, p0, LAp;->a:I

    .line 63
    iput-boolean v1, p0, LAp;->a:Z

    .line 64
    iput-boolean v1, p0, LAp;->b:Z

    .line 70
    new-instance v0, LAq;

    invoke-direct {v0, p0}, LAq;-><init>(LAp;)V

    iput-object v0, p0, LAp;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 198
    return-void
.end method

.method static synthetic a(LAp;F)F
    .locals 0

    .prologue
    .line 47
    iput p1, p0, LAp;->a:F

    return p1
.end method

.method static synthetic a(LAp;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, LAp;->a:I

    return v0
.end method

.method static synthetic a(LAp;I)I
    .locals 0

    .prologue
    .line 47
    iput p1, p0, LAp;->a:I

    return p1
.end method

.method static synthetic a(LAp;)Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    return-object v0
.end method

.method private a(FI)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 130
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, LAp;->a:F

    invoke-direct {v0, v2, v2, v1, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 131
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 132
    if-lez p2, :cond_0

    .line 133
    iget-object v1, p0, LAp;->a:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 135
    :cond_0
    iget-object v1, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 136
    return-void
.end method

.method static synthetic a(LAp;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, LAp;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 139
    iget-object v1, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 140
    :goto_0
    if-ne v1, p1, :cond_2

    .line 150
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v0

    .line 139
    goto :goto_0

    .line 144
    :cond_2
    iget-object v1, p0, LAp;->a:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    if-eqz p1, :cond_0

    .line 148
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_1

    .line 144
    :cond_3
    const/16 v0, 0x8

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;LtK;)Z
    .locals 1

    .prologue
    .line 103
    invoke-static {p0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    .line 104
    if-nez v0, :cond_0

    sget-object v0, LNn;->a:LNn;

    .line 105
    invoke-interface {p1, v0}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-static {p0}, LwN;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-static {p0}, LUs;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/app/Activity;LAm;LAo;LaFO;)V
    .locals 11

    .prologue
    .line 246
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iput-object p1, p0, LAp;->a:Landroid/view/View;

    .line 249
    sget v0, Lxc;->create_bar_new_from_camera:I

    .line 250
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 253
    invoke-static {p2}, LwN;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    invoke-static {}, LwN;->a()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 255
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    :goto_0
    invoke-static {}, LAi;->values()[LAi;

    move-result-object v8

    array-length v9, v8

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v9, :cond_1

    aget-object v2, v8, v7

    .line 263
    invoke-virtual {v2}, LAi;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 264
    new-instance v0, LAr;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, LAr;-><init>(LAp;LAi;Landroid/app/Activity;LAm;LAo;LaFO;)V

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 258
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 270
    :cond_1
    iget-object v0, p0, LAp;->a:LJR;

    invoke-virtual {v0, p0}, LJR;->b(LKe;)V

    .line 271
    iget-object v0, p0, LAp;->a:LJR;

    invoke-virtual {v0, p0}, LJR;->a(LKe;)V

    .line 272
    return-void
.end method

.method public a(Landroid/widget/AbsListView;Z)V
    .locals 4

    .prologue
    const/16 v3, 0xc8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 154
    iget-boolean v0, p0, LAp;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LAp;->a:Z

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 157
    :cond_1
    if-eqz p2, :cond_2

    .line 158
    iget v0, p0, LAp;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 171
    :pswitch_1
    iput v1, p0, LAp;->a:I

    .line 172
    invoke-direct {p0, v1}, LAp;->a(Z)V

    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3}, LAp;->a(FI)V

    goto :goto_0

    .line 165
    :pswitch_2
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 166
    const/4 v0, 0x2

    iput v0, p0, LAp;->a:I

    .line 167
    invoke-direct {p0, v1}, LAp;->a(Z)V

    goto :goto_0

    .line 177
    :cond_2
    iget v0, p0, LAp;->a:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 184
    :pswitch_3
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 185
    iput v2, p0, LAp;->a:I

    .line 186
    invoke-direct {p0, v2}, LAp;->a(Z)V

    goto :goto_0

    .line 190
    :pswitch_4
    const/4 v0, 0x3

    iput v0, p0, LAp;->a:I

    .line 191
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0, v3}, LAp;->a(FI)V

    .line 192
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LAp;->a:F

    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 177
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 112
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LAp;->b:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, LAp;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 114
    invoke-direct {p0, v1}, LAp;->a(Z)V

    .line 116
    :cond_0
    iput-boolean v1, p0, LAp;->a:Z

    .line 117
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 276
    iput-boolean v1, p0, LAp;->b:Z

    .line 277
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, LAp;->a(Landroid/widget/AbsListView;Z)V

    .line 278
    return-void
.end method

.method public c_()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, LAp;->a:LJR;

    invoke-virtual {v0}, LJR;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p0}, LAp;->d()V

    .line 293
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-virtual {p0}, LAp;->c()V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 282
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, LAp;->a(Landroid/widget/AbsListView;Z)V

    .line 283
    iput-boolean v1, p0, LAp;->b:Z

    .line 284
    return-void
.end method
