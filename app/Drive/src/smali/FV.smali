.class public final enum LFV;
.super Ljava/lang/Enum;
.source "DocumentOpenerError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LFV;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LFV;

.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "LafT;",
            "LFV;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LFV;

.field public static final enum b:LFV;

.field public static final enum c:LFV;

.field public static final enum d:LFV;

.field public static final enum e:LFV;

.field public static final enum f:LFV;

.field public static final enum g:LFV;

.field public static final enum h:LFV;


# instance fields
.field private final a:LafT;

.field private final a:Ljava/lang/Integer;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, LFV;

    const-string v1, "USER_INTERRUPTED"

    sget-object v3, LafT;->p:LafT;

    const/4 v4, 0x0

    move v5, v2

    invoke-direct/range {v0 .. v5}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v0, LFV;->a:LFV;

    .line 22
    new-instance v3, LFV;

    const-string v4, "DOCUMENT_UNAVAILABLE"

    sget-object v6, LafT;->f:LafT;

    sget v0, Lxi;->error_document_not_available:I

    .line 23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v5, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->b:LFV;

    .line 26
    new-instance v3, LFV;

    const-string v4, "VIEWER_UNAVAILABLE"

    sget-object v6, LafT;->r:LafT;

    sget v0, Lxi;->error_no_viewer_available:I

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v5, v10

    move v8, v2

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->c:LFV;

    .line 30
    new-instance v3, LFV;

    const-string v4, "VIDEO_UNAVAILABLE"

    sget-object v6, LafT;->q:LafT;

    sget v0, Lxi;->error_video_not_available:I

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v5, v11

    move v8, v2

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->d:LFV;

    .line 34
    new-instance v3, LFV;

    const-string v4, "EXTERNAL_STORAGE_NOT_READY"

    sget-object v6, LafT;->g:LafT;

    sget v0, Lxi;->pin_error_external_storage_not_ready:I

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v5, v12

    move v8, v9

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->e:LFV;

    .line 38
    new-instance v3, LFV;

    const-string v4, "AUTHENTICATION_FAILURE"

    const/4 v5, 0x5

    sget-object v6, LafT;->b:LafT;

    sget v0, Lxi;->error_access_denied_html:I

    .line 39
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v8, v2

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->f:LFV;

    .line 42
    new-instance v3, LFV;

    const-string v4, "CONNECTION_FAILURE"

    const/4 v5, 0x6

    sget-object v6, LafT;->e:LafT;

    sget v0, Lxi;->error_network_error_html:I

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v8, v9

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->g:LFV;

    .line 46
    new-instance v3, LFV;

    const-string v4, "UNKNOWN_INTERNAL"

    const/4 v5, 0x7

    sget-object v6, LafT;->o:LafT;

    sget v0, Lxi;->error_internal_error_html:I

    .line 47
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move v8, v2

    invoke-direct/range {v3 .. v8}, LFV;-><init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V

    sput-object v3, LFV;->h:LFV;

    .line 16
    const/16 v0, 0x8

    new-array v0, v0, [LFV;

    sget-object v1, LFV;->a:LFV;

    aput-object v1, v0, v2

    sget-object v1, LFV;->b:LFV;

    aput-object v1, v0, v9

    sget-object v1, LFV;->c:LFV;

    aput-object v1, v0, v10

    sget-object v1, LFV;->d:LFV;

    aput-object v1, v0, v11

    sget-object v1, LFV;->e:LFV;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, LFV;->f:LFV;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, LFV;->g:LFV;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, LFV;->h:LFV;

    aput-object v3, v0, v1

    sput-object v0, LFV;->a:[LFV;

    .line 54
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    .line 55
    invoke-static {}, LFV;->values()[LFV;

    move-result-object v1

    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 56
    invoke-virtual {v4}, LFV;->a()LafT;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 58
    :cond_0
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LFV;->a:LbmL;

    .line 59
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILafT;Ljava/lang/Integer;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LafT;",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput-object p3, p0, LFV;->a:LafT;

    .line 83
    iput-object p4, p0, LFV;->a:Ljava/lang/Integer;

    .line 84
    iput-boolean p5, p0, LFV;->a:Z

    .line 85
    return-void
.end method

.method public static a(LafT;)LFV;
    .locals 4

    .prologue
    .line 65
    sget-object v0, LFV;->a:LbmL;

    invoke-virtual {v0, p0}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFV;

    .line 66
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reason not recognized: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 67
    return-object v0

    .line 66
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LFV;
    .locals 1

    .prologue
    .line 16
    const-class v0, LFV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LFV;

    return-object v0
.end method

.method public static values()[LFV;
    .locals 1

    .prologue
    .line 16
    sget-object v0, LFV;->a:[LFV;

    invoke-virtual {v0}, [LFV;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LFV;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, LFV;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not reportable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    iget-object v0, p0, LFV;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public a()LafT;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, LFV;->a:LafT;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, LFV;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, LFV;->a:Z

    return v0
.end method
