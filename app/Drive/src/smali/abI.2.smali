.class public LabI;
.super Ljava/lang/Object;
.source "ContactPhotoLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field public static final a:I

.field private static final a:LabN;

.field private static final a:[Ljava/lang/String;


# instance fields
.field private a:LabL;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "LabK;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "LabM;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, LabI;->a:[Ljava/lang/String;

    .line 152
    sget v0, Lxb;->contact_android:I

    sput v0, LabI;->a:I

    .line 191
    new-instance v0, LabJ;

    invoke-direct {v0}, LabJ;-><init>()V

    sput-object v0, LabI;->a:LabN;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data15"

    aput-object v2, v0, v1

    iput-object v0, p0, LabI;->b:[Ljava/lang/String;

    .line 207
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 214
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 220
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LabI;->a:Landroid/os/Handler;

    .line 247
    iput-object p1, p0, LabI;->a:Landroid/content/Context;

    .line 248
    return-void
.end method

.method static synthetic a(LabI;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LabI;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(J[B)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 428
    iget-boolean v0, p0, LabI;->b:Z

    if-eqz v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 432
    :cond_0
    new-instance v0, LabK;

    invoke-direct {v0, v1}, LabK;-><init>(LabJ;)V

    .line 433
    const/4 v1, 0x2

    iput v1, v0, LabK;->a:I

    .line 434
    if-eqz p3, :cond_1

    .line 436
    const/4 v1, 0x0

    :try_start_0
    array-length v2, p3

    const/4 v3, 0x0

    invoke-static {p3, v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 437
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LabK;->a:Ljava/lang/ref/SoftReference;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 442
    :cond_1
    :goto_1
    iget-object v1, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 438
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method static synthetic a(LabI;J[B)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, LabI;->a(J[B)V

    return-void
.end method

.method static synthetic a(LabI;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, LabI;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 449
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 450
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 459
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 460
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabM;

    iget-wide v2, v0, LabM;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 462
    iget-object v0, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabK;

    .line 463
    if-eqz v0, :cond_0

    iget v3, v0, LabK;->a:I

    if-nez v3, :cond_0

    .line 465
    const/4 v3, 0x1

    iput v3, v0, LabK;->a:I

    .line 466
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 470
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Object;LabM;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 289
    iget-object v0, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, p2, LabM;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabK;

    .line 290
    if-nez v0, :cond_1

    .line 291
    new-instance v0, LabK;

    invoke-direct {v0, v6}, LabK;-><init>(LabJ;)V

    .line 292
    iget-object v1, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, p2, LabM;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_0
    :goto_0
    iget-object v1, p2, LabM;->a:LabN;

    invoke-interface {v1, p1, v6}, LabN;->a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 313
    iput v3, v0, LabK;->a:I

    move v0, v3

    .line 314
    :goto_1
    return v0

    .line 293
    :cond_1
    iget v1, v0, LabK;->a:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 295
    iget-object v1, v0, LabK;->a:Ljava/lang/ref/SoftReference;

    if-nez v1, :cond_2

    .line 296
    iget-object v0, p2, LabM;->a:LabN;

    invoke-interface {v0, p1, v6}, LabN;->a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    move v0, v2

    .line 297
    goto :goto_1

    .line 300
    :cond_2
    iget-object v1, v0, LabK;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 301
    if-eqz v1, :cond_3

    .line 302
    iget-object v0, p2, LabM;->a:LabN;

    invoke-interface {v0, p1, v1}, LabN;->a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    move v0, v2

    .line 303
    goto :goto_1

    .line 308
    :cond_3
    iput-object v6, v0, LabK;->a:Ljava/lang/ref/SoftReference;

    goto :goto_0
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, LabI;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LabI;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LabI;->b:[Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, LabI;->a:LabL;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, LabI;->a:LabL;

    invoke-virtual {v0}, LabL;->quit()Z

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, LabI;->a:LabL;

    .line 322
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 365
    const-string v0, "ContactPhotoLoader"

    const-string v1, "Loading Requested."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-boolean v0, p0, LabI;->a:Z

    if-nez v0, :cond_0

    .line 367
    iput-boolean v2, p0, LabI;->a:Z

    .line 368
    iget-object v0, p0, LabI;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 370
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 408
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 410
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabM;

    .line 411
    invoke-direct {p0, v2, v0}, LabI;->a(Ljava/lang/Object;LabM;)Z

    move-result v0

    .line 412
    if-eqz v0, :cond_0

    .line 413
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 417
    :cond_1
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    invoke-direct {p0}, LabI;->e()V

    .line 422
    :goto_1
    return-void

    .line 420
    :cond_2
    invoke-direct {p0}, LabI;->f()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 330
    invoke-virtual {p0}, LabI;->c()V

    .line 331
    invoke-direct {p0}, LabI;->e()V

    .line 332
    invoke-virtual {p0}, LabI;->b()V

    .line 333
    return-void
.end method

.method public a(Landroid/widget/ImageView;J)V
    .locals 2

    .prologue
    .line 256
    sget-object v0, LabI;->a:LabN;

    invoke-virtual {p0, p1, p2, p3, v0}, LabI;->a(Ljava/lang/Object;JLabN;)V

    .line 257
    return-void
.end method

.method public a(Ljava/lang/Object;JLabN;)V
    .locals 2

    .prologue
    .line 264
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 266
    const/4 v0, 0x0

    invoke-interface {p4, p1, v0}, LabN;->a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 267
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    new-instance v0, LabM;

    invoke-direct {v0, p2, p3, p4}, LabM;-><init>(JLabN;)V

    .line 270
    invoke-direct {p0, p1, v0}, LabI;->a(Ljava/lang/Object;LabM;)Z

    move-result v1

    .line 271
    if-eqz v1, :cond_2

    .line 272
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 274
    :cond_2
    iget-object v1, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-boolean v0, p0, LabI;->b:Z

    if-nez v0, :cond_0

    .line 277
    invoke-direct {p0}, LabI;->f()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 337
    iget-object v0, p0, LabI;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 338
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, LabI;->b:Z

    .line 345
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    iput-boolean v0, p0, LabI;->b:Z

    .line 353
    iget-object v0, p0, LabI;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    invoke-direct {p0}, LabI;->f()V

    .line 356
    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 377
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 379
    :pswitch_0
    iput-boolean v1, p0, LabI;->a:Z

    .line 380
    iget-boolean v1, p0, LabI;->b:Z

    if-nez v1, :cond_0

    .line 381
    iget-object v1, p0, LabI;->a:LabL;

    if-nez v1, :cond_1

    .line 382
    new-instance v1, LabL;

    iget-object v2, p0, LabI;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LabL;-><init>(LabI;Landroid/content/ContentResolver;)V

    iput-object v1, p0, LabI;->a:LabL;

    .line 383
    iget-object v1, p0, LabI;->a:LabL;

    invoke-virtual {v1}, LabL;->start()V

    .line 386
    :cond_1
    iget-object v1, p0, LabI;->a:LabL;

    invoke-virtual {v1}, LabL;->a()V

    goto :goto_0

    .line 392
    :pswitch_1
    iget-boolean v1, p0, LabI;->b:Z

    if-nez v1, :cond_0

    .line 393
    invoke-direct {p0}, LabI;->g()V

    goto :goto_0

    .line 377
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
