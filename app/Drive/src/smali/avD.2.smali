.class public final LavD;
.super Ljava/lang/Object;
.source "MatchRects.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/MatchRects;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/MatchRects;
    .locals 3

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;

    const-class v1, Landroid/graphics/Rect;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    const-class v2, Ljava/lang/Integer;

    .line 124
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/pdflib/MatchRects;
    .locals 1

    .prologue
    .line 129
    new-array v0, p1, [Lcom/google/android/apps/viewer/pdflib/MatchRects;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0, p1}, LavD;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0, p1}, LavD;->a(I)[Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method
