.class public final LzD;
.super Landroid/widget/BaseAdapter;
.source "SharingCard.java"

# interfaces
.implements LabN;
.implements Laco;


# instance fields
.field private a:LaGu;

.field private final a:LaKR;

.field private final a:LabI;

.field private final a:LacF;

.field private final a:Lacj;

.field private a:Lacr;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/View;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LaKR;Lacj;LacF;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 61
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LzD;->a:Ljava/util/List;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, LzD;->a:LaGu;

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, LzD;->a:Z

    .line 73
    iput-object p1, p0, LzD;->a:Landroid/content/Context;

    .line 74
    iput-object p2, p0, LzD;->a:LaKR;

    .line 75
    iput-object p4, p0, LzD;->a:LacF;

    .line 76
    new-instance v0, LabI;

    invoke-direct {v0, p1}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LzD;->a:LabI;

    .line 77
    iput-object p3, p0, LzD;->a:Lacj;

    .line 79
    invoke-direct {p0}, LzD;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LzD;->a:Landroid/view/View;

    .line 80
    return-void
.end method

.method static synthetic a(LzD;)LaGu;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LzD;->a:LaGu;

    return-object v0
.end method

.method static synthetic a(LzD;)LaKR;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LzD;->a:LaKR;

    return-object v0
.end method

.method static synthetic a(LzD;)Lacj;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LzD;->a:Lacj;

    return-object v0
.end method

.method static synthetic a(LzD;)Lacr;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LzD;->a:Lacr;

    return-object v0
.end method

.method static synthetic a(LzD;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LzD;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    iget-object v0, p0, LzD;->a:Landroid/content/Context;

    sget v1, Lxe;->detail_card_sharing:I

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 84
    sget v1, Lxc;->add_collaborators:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LzF;

    invoke-direct {v2, p0, v3}, LzF;-><init>(LzD;LzE;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-object v0
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    if-eqz p1, :cond_0

    .line 189
    :goto_0
    return-object p1

    .line 185
    :cond_0
    iget-object v0, p0, LzD;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 186
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 187
    sget v1, Lxe;->detail_card_sharing_row:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 188
    sget v0, Lxc;->sharing_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LzG;

    invoke-direct {v1, p0, v2}, LzG;-><init>(LzD;LzE;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0}, LzD;->a(III)V

    .line 114
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 121
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    sget v1, Lxc;->error_message:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0, v2}, LzD;->a(III)V

    .line 123
    return-void
.end method

.method private a(III)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    sget v1, Lxc;->loading_spinner:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    sget v1, Lxc;->error_message:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    sget v1, Lxc;->add_collaborators:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setVisibility(I)V

    .line 130
    return-void
.end method

.method private a(Landroid/view/View;LacD;)V
    .locals 6

    .prologue
    .line 193
    sget v0, Lxc;->sharing_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 195
    sget v0, Lxc;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 196
    iget-object v1, p0, LzD;->a:Landroid/content/Context;

    invoke-static {p2, v1}, LacH;->a(LacD;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    sget v0, Lxc;->share_role:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 199
    invoke-virtual {p2}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqt;

    move-result-object v1

    iget-object v2, p0, LzD;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lqt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual {p2}, LacD;->a()LabD;

    move-result-object v1

    .line 202
    if-eqz v1, :cond_0

    .line 203
    sget v0, Lxc;->image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 204
    iget-object v2, p0, LzD;->a:LabI;

    invoke-interface {v1}, LabD;->b()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5, p0}, LabI;->a(Ljava/lang/Object;JLabN;)V

    .line 207
    :cond_0
    iget-object v0, p0, LzD;->a:Landroid/content/Context;

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    sget v0, Lxc;->email_address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 209
    if-eqz v0, :cond_1

    .line 210
    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 213
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    :cond_1
    return-void

    .line 210
    :cond_2
    invoke-interface {v1}, LabD;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 213
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private a(LacD;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 234
    iget-object v1, p0, LzD;->a:Lacj;

    invoke-virtual {v1}, Lacj;->a()Lacr;

    move-result-object v1

    .line 235
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lacr;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqv;

    move-result-object v1

    sget-object v2, Lqv;->a:Lqv;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 238
    :cond_0
    return v0
.end method

.method static synthetic a(LzD;LacD;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, LzD;->a(LacD;)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LzD;->a(III)V

    .line 118
    return-void
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 0

    .prologue
    .line 251
    if-nez p1, :cond_0

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_0
    iput-object p1, p0, LzD;->a:LaGu;

    goto :goto_0
.end method

.method public a(Lacr;)V
    .locals 2

    .prologue
    .line 93
    iput-object p1, p0, LzD;->a:Lacr;

    .line 94
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LzD;->a:Ljava/util/List;

    .line 96
    if-eqz p1, :cond_1

    .line 97
    invoke-interface {p1}, Lacr;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LzD;->a:Ljava/util/List;

    .line 98
    invoke-direct {p0}, LzD;->a()V

    .line 99
    iget-object v0, p0, LzD;->a:LacF;

    invoke-virtual {v0}, LacF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, LzD;->a:Lacj;

    iget-object v1, p0, LzD;->a:LaGu;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p0}, LzD;->notifyDataSetChanged()V

    .line 110
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, LzD;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    invoke-direct {p0}, LzD;->b()V

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, LzD;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lxi;->empty_sharing_list:I

    .line 107
    :goto_1
    invoke-direct {p0, v0}, LzD;->a(I)V

    goto :goto_0

    .line 105
    :cond_3
    sget v0, Lxi;->sharing_list_offline:I

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 243
    instance-of v0, p1, Landroid/widget/ImageView;

    invoke-static {v0}, LbiT;->b(Z)V

    .line 244
    check-cast p1, Landroid/widget/ImageView;

    .line 245
    if-eqz p2, :cond_0

    .line 246
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, LzD;->a:Z

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, LzD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LzD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 148
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LzD;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 150
    invoke-virtual {p0, p1}, LzD;->getItemViewType(I)I

    move-result v0

    .line 152
    if-ne v0, v1, :cond_1

    .line 153
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    .line 155
    :goto_1
    return-object v0

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, LzD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 161
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LzD;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 162
    int-to-long v0, p1

    return-wide v0

    .line 161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LzD;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 222
    invoke-virtual {p0}, LzD;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 225
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 221
    goto :goto_0

    :cond_1
    move v1, v2

    .line 225
    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 167
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LzD;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 169
    invoke-virtual {p0, p1}, LzD;->getItemViewType(I)I

    move-result v0

    .line 171
    if-ne v0, v1, :cond_1

    .line 172
    iget-object v0, p0, LzD;->a:Landroid/view/View;

    .line 176
    :goto_1
    return-object v0

    .line 167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :cond_1
    invoke-direct {p0, p2}, LzD;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 175
    iget-object v0, p0, LzD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    invoke-direct {p0, v1, v0}, LzD;->a(Landroid/view/View;LacD;)V

    move-object v0, v1

    .line 176
    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x1

    return v0
.end method
