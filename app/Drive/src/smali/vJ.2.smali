.class public LvJ;
.super Ljava/lang/Object;
.source "BreadcrumbItem.java"


# instance fields
.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LvJ;->a:Ljava/lang/String;

    .line 28
    invoke-static {p2}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LvJ;->a:LbmF;

    .line 29
    return-void
.end method


# virtual methods
.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, LvJ;->a:LbmF;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, LvJ;->a:Ljava/lang/String;

    return-object v0
.end method
