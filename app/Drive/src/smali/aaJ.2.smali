.class public LaaJ;
.super Ljava/lang/Object;
.source "MetadataHelper.java"


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, LaaJ;->a:Landroid/content/ContentResolver;

    .line 24
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 69
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :try_start_0
    iget-object v0, p0, LaaJ;->a:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 81
    if-eqz v0, :cond_1

    .line 82
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    :goto_0
    return-object v0

    .line 74
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "MetadataHelper"

    const-string v2, "Suppressing exception thrown by resolver.query(%s, {%s}, ...)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v7

    aput-object p2, v3, v8

    invoke-static {v1, v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    .line 79
    goto :goto_0

    .line 85
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    .line 86
    goto :goto_0

    :cond_1
    move-object v0, v6

    .line 89
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_0

    .line 30
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p3

    .line 34
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 37
    :cond_0
    :goto_0
    return p3

    .line 31
    :catch_0
    move-exception v1

    .line 34
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;J)J
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 44
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide p3

    .line 48
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 51
    :cond_0
    :goto_0
    return-wide p3

    .line 45
    :catch_0
    move-exception v1

    .line 48
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 58
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p3

    .line 62
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 65
    :cond_0
    :goto_0
    return-object p3

    .line 59
    :catch_0
    move-exception v1

    .line 62
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method
