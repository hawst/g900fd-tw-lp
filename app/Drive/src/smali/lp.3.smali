.class final Llp;
.super Landroid/os/Handler;
.source "MediaRouter.java"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llm;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Llo;


# direct methods
.method private constructor <init>(Llo;)V
    .locals 1

    .prologue
    .line 2201
    iput-object p1, p0, Llp;->a:Llo;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llp;->a:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Llo;Llk;)V
    .locals 0

    .prologue
    .line 2201
    invoke-direct {p0, p1}, Llp;-><init>(Llo;)V

    return-void
.end method

.method private a(Llm;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 2273
    iget-object v0, p1, Llm;->a:Llj;

    .line 2274
    iget-object v1, p1, Llm;->a:Lll;

    .line 2275
    const v2, 0xff00

    and-int/2addr v2, p2

    sparse-switch v2, :sswitch_data_0

    .line 2321
    :cond_0
    :goto_0
    return-void

    .line 2277
    :sswitch_0
    check-cast p3, Llv;

    .line 2278
    invoke-virtual {p1, p3}, Llm;->a(Llv;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2281
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 2283
    :pswitch_0
    invoke-virtual {v1, v0, p3}, Lll;->a(Llj;Llv;)V

    goto :goto_0

    .line 2286
    :pswitch_1
    invoke-virtual {v1, v0, p3}, Lll;->b(Llj;Llv;)V

    goto :goto_0

    .line 2289
    :pswitch_2
    invoke-virtual {v1, v0, p3}, Lll;->c(Llj;Llv;)V

    goto :goto_0

    .line 2292
    :pswitch_3
    invoke-virtual {v1, v0, p3}, Lll;->f(Llj;Llv;)V

    goto :goto_0

    .line 2295
    :pswitch_4
    invoke-virtual {v1, v0, p3}, Lll;->g(Llj;Llv;)V

    goto :goto_0

    .line 2298
    :pswitch_5
    invoke-virtual {v1, v0, p3}, Lll;->d(Llj;Llv;)V

    goto :goto_0

    .line 2301
    :pswitch_6
    invoke-virtual {v1, v0, p3}, Lll;->e(Llj;Llv;)V

    goto :goto_0

    .line 2307
    :sswitch_1
    check-cast p3, Llu;

    .line 2308
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 2310
    :pswitch_7
    invoke-virtual {v1, v0, p3}, Lll;->a(Llj;Llu;)V

    goto :goto_0

    .line 2313
    :pswitch_8
    invoke-virtual {v1, v0, p3}, Lll;->b(Llj;Llu;)V

    goto :goto_0

    .line 2316
    :pswitch_9
    invoke-virtual {v1, v0, p3}, Lll;->c(Llj;Llu;)V

    goto :goto_0

    .line 2275
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    .line 2281
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 2308
    :pswitch_data_1
    .packed-switch 0x201
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private b(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2256
    packed-switch p1, :pswitch_data_0

    .line 2270
    :goto_0
    :pswitch_0
    return-void

    .line 2258
    :pswitch_1
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Lmc;

    move-result-object v0

    check-cast p2, Llv;

    invoke-virtual {v0, p2}, Lmc;->a(Llv;)V

    goto :goto_0

    .line 2261
    :pswitch_2
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Lmc;

    move-result-object v0

    check-cast p2, Llv;

    invoke-virtual {v0, p2}, Lmc;->b(Llv;)V

    goto :goto_0

    .line 2264
    :pswitch_3
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Lmc;

    move-result-object v0

    check-cast p2, Llv;

    invoke-virtual {v0, p2}, Lmc;->c(Llv;)V

    goto :goto_0

    .line 2267
    :pswitch_4
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Lmc;

    move-result-object v0

    check-cast p2, Llv;

    invoke-virtual {v0, p2}, Lmc;->d(Llv;)V

    goto :goto_0

    .line 2256
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2222
    invoke-virtual {p0, p1, p2}, Llp;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2223
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 2227
    iget v2, p1, Landroid/os/Message;->what:I

    .line 2228
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2231
    invoke-direct {p0, v2, v3}, Llp;->b(ILjava/lang/Object;)V

    .line 2237
    :try_start_0
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 2238
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 2239
    if-nez v0, :cond_0

    .line 2240
    iget-object v0, p0, Llp;->a:Llo;

    invoke-static {v0}, Llo;->a(Llo;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    move v0, v1

    .line 2244
    goto :goto_0

    .line 2242
    :cond_0
    iget-object v4, p0, Llp;->a:Ljava/util/ArrayList;

    iget-object v0, v0, Llj;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2251
    :catchall_0
    move-exception v0

    iget-object v1, p0, Llp;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v0

    .line 2246
    :cond_1
    :try_start_1
    iget-object v0, p0, Llp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2247
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_2

    .line 2248
    iget-object v0, p0, Llp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    invoke-direct {p0, v0, v2, v3}, Llp;->a(Llm;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2247
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2251
    :cond_2
    iget-object v0, p0, Llp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2253
    return-void
.end method
