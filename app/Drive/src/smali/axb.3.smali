.class Laxb;
.super Ljava/lang/Object;
.source "PdfViewer.java"

# interfaces
.implements Lawf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lawf",
        "<",
        "LawH;",
        ">;"
    }
.end annotation


# instance fields
.field private a:F

.field private a:I

.field final synthetic a:LawZ;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LawZ;)V
    .locals 1

    .prologue
    .line 415
    iput-object p1, p0, Laxb;->a:LawZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423
    new-instance v0, Laxc;

    invoke-direct {v0, p0}, Laxc;-><init>(Laxb;)V

    iput-object v0, p0, Laxb;->a:Ljava/util/List;

    .line 435
    new-instance v0, Laxd;

    invoke-direct {v0, p0}, Laxd;-><init>(Laxb;)V

    iput-object v0, p0, Laxb;->b:Ljava/util/List;

    return-void
.end method

.method private a(LawH;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 553
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, LawH;->a:I

    iget v2, p1, LawH;->b:I

    iget v3, p1, LawH;->a:I

    iget-object v4, p0, Laxb;->a:LawZ;

    .line 554
    invoke-static {v4}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p1, LawH;->b:I

    iget-object v5, p0, Laxb;->a:LawZ;

    invoke-static {v5}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private a(LawH;)Laxl;
    .locals 3

    .prologue
    .line 559
    iget v0, p1, LawH;->b:I

    int-to-float v0, v0

    iget v1, p1, LawH;->a:F

    div-float/2addr v0, v1

    .line 560
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p1, LawH;->a:F

    div-float/2addr v1, v2

    add-float/2addr v1, v0

    .line 563
    iget-object v2, p0, Laxb;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 564
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 566
    iget-object v2, p0, Laxb;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v2, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v1

    .line 567
    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 569
    new-instance v2, Laxl;

    invoke-direct {v2, v0, v1}, Laxl;-><init>(II)V

    return-object v2
.end method

.method private a(Laxl;ILaxl;)Laxl;
    .locals 1

    .prologue
    .line 574
    sget-boolean v0, LavX;->m:Z

    if-eqz v0, :cond_0

    .line 578
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1, p2, p3}, Laxl;->a(ILaxl;)Laxl;

    move-result-object p1

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 538
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0, p1}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v1

    .line 539
    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxz;

    move-result-object v0

    invoke-virtual {v0, p1}, Laxz;->d(I)V

    .line 542
    :cond_0
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxw;

    move-result-object v0

    invoke-virtual {v0}, Laxw;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 543
    new-instance v2, LawI;

    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxw;

    move-result-object v0

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    invoke-direct {v2, v0}, LawI;-><init>(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 549
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 545
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxz;

    move-result-object v1

    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxn;

    move-result-object v0

    invoke-virtual {v0}, Laxn;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, p1, v0}, Laxz;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Laxl;)V
    .locals 4

    .prologue
    .line 512
    invoke-virtual {p1}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 513
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2}, LawZ;->a(LawZ;)Laxz;

    move-result-object v2

    invoke-virtual {v2, v0}, Laxz;->c(I)V

    .line 514
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v2

    .line 515
    invoke-virtual {v2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->b()V

    .line 516
    iget v3, p0, Laxb;->a:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a(I)V

    .line 517
    invoke-direct {p0, v0}, Laxb;->a(I)V

    goto :goto_0

    .line 519
    :cond_0
    return-void
.end method

.method private a(Laxl;FLandroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 505
    invoke-virtual {p1}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 506
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    .line 507
    invoke-virtual {v0, p3, p2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a(Landroid/graphics/Rect;F)V

    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method

.method private b(LawH;)Laxl;
    .locals 5

    .prologue
    .line 586
    iget v0, p1, LawH;->b:I

    int-to-float v0, v0

    iget v1, p1, LawH;->a:F

    div-float/2addr v0, v1

    .line 587
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p1, LawH;->a:F

    div-float/2addr v1, v2

    add-float/2addr v1, v0

    .line 589
    iget-object v2, p0, Laxb;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v2

    .line 590
    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 592
    iget-object v3, p0, Laxb;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v3

    .line 593
    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 595
    if-ge v3, v2, :cond_0

    .line 597
    iget-object v2, p0, Laxb;->a:Ljava/util/List;

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 598
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 599
    new-instance v0, Laxl;

    invoke-direct {v0, v1, v1}, Laxl;-><init>(II)V

    .line 602
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laxl;

    invoke-direct {v0, v2, v3}, Laxl;-><init>(II)V

    goto :goto_0
.end method

.method private b(Laxl;)V
    .locals 3

    .prologue
    .line 530
    invoke-virtual {p1}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 531
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2}, LawZ;->a(LawZ;)Laxz;

    move-result-object v2

    invoke-virtual {v2, v0}, Laxz;->a(I)V

    .line 532
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->c()V

    goto :goto_0

    .line 535
    :cond_0
    return-void
.end method

.method private b(Laxl;FLandroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 522
    invoke-virtual {p1}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 523
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v2

    .line 524
    iget v3, p0, Laxb;->a:I

    invoke-virtual {v2, v3, p3, p2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a(ILandroid/graphics/Rect;F)V

    .line 525
    invoke-direct {p0, v0}, Laxb;->a(I)V

    goto :goto_0

    .line 527
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LawH;LawH;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 449
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-direct {p0, p2}, Laxb;->a(LawH;)Laxl;

    move-result-object v2

    invoke-static {v1, v2}, LawZ;->a(LawZ;Laxl;)Laxl;

    .line 450
    iget-boolean v1, p2, LawH;->a:Z

    if-eqz v1, :cond_1

    .line 451
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)LawP;

    move-result-object v1

    invoke-direct {p0, p2}, Laxb;->b(LawH;)Laxl;

    move-result-object v2

    iget v3, p2, LawH;->a:F

    invoke-virtual {v1, v2, v3}, LawP;->a(Laxl;F)V

    .line 457
    :goto_0
    iget-boolean v1, p2, LawH;->a:Z

    if-eqz v1, :cond_0

    .line 458
    iget v1, p2, LawH;->a:F

    iput v1, p0, Laxb;->a:F

    .line 459
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2}, LawZ;->a(LawZ;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2}, LawZ;->a(LawZ;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 460
    int-to-float v1, v1

    iget v2, p2, LawH;->a:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 461
    iget v2, p0, Laxb;->a:I

    if-eq v1, v2, :cond_0

    .line 462
    iput v1, p0, Laxb;->a:I

    .line 466
    :cond_0
    invoke-direct {p0, p2}, Laxb;->a(LawH;)Landroid/graphics/Rect;

    move-result-object v2

    .line 468
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)Laxl;

    move-result-object v3

    .line 469
    const/4 v1, 0x1

    .line 470
    iget-object v4, p0, Laxb;->a:LawZ;

    invoke-static {v4}, LawZ;->b(LawZ;)Laxl;

    move-result-object v4

    invoke-direct {p0, v4, v1, v3}, Laxb;->a(Laxl;ILaxl;)Laxl;

    move-result-object v4

    .line 473
    invoke-virtual {v3, v4}, Laxl;->a(Laxl;)[Laxl;

    move-result-object v5

    .line 474
    array-length v6, v5

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_2

    aget-object v7, v5, v1

    .line 475
    invoke-direct {p0, v7}, Laxb;->b(Laxl;)V

    .line 474
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 453
    :cond_1
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->a(LawZ;)LawP;

    move-result-object v1

    invoke-direct {p0, p2}, Laxb;->b(LawH;)Laxl;

    move-result-object v2

    invoke-virtual {v1, v2}, LawP;->a(Laxl;)V

    goto :goto_0

    .line 479
    :cond_2
    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->b(LawZ;)Laxl;

    move-result-object v1

    invoke-virtual {v4, v1}, Laxl;->a(Laxl;)[Laxl;

    move-result-object v1

    .line 480
    array-length v4, v1

    :goto_2
    if-ge v0, v4, :cond_3

    aget-object v5, v1, v0

    .line 481
    invoke-direct {p0, v5}, Laxb;->a(Laxl;)V

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 484
    :cond_3
    iget-boolean v0, p2, LawH;->a:Z

    if-eqz v0, :cond_6

    .line 486
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    iget v1, p2, LawH;->a:F

    invoke-direct {p0, v0, v1, v2}, Laxb;->b(Laxl;FLandroid/graphics/Rect;)V

    .line 492
    :cond_4
    :goto_3
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_7

    .line 493
    invoke-virtual {v3}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 494
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 496
    iget-object v2, p0, Laxb;->a:LawZ;

    invoke-virtual {v2}, LawZ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 487
    :cond_6
    iget v0, p0, Laxb;->a:F

    iget v1, p2, LawH;->a:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 489
    iget-object v0, p0, Laxb;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    iget v1, p2, LawH;->a:F

    invoke-direct {p0, v0, v1, v2}, Laxb;->a(Laxl;FLandroid/graphics/Rect;)V

    goto :goto_3

    .line 501
    :cond_7
    iget-object v0, p0, Laxb;->a:LawZ;

    iget-object v1, p0, Laxb;->a:LawZ;

    invoke-static {v1}, LawZ;->b(LawZ;)Laxl;

    move-result-object v1

    iget v1, v1, Laxl;->b:I

    invoke-static {v0, v1}, LawZ;->a(LawZ;I)V

    .line 502
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 415
    check-cast p1, LawH;

    check-cast p2, LawH;

    invoke-virtual {p0, p1, p2}, Laxb;->a(LawH;LawH;)V

    return-void
.end method
