.class public final Laku;
.super Ljava/lang/Object;
.source "CachingSupplier.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbjv",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lbjv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbjv",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, Laku;->a:Lbjv;

    .line 24
    return-void
.end method

.method public static a(Lbjv;)Laku;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbjv",
            "<TT;>;)",
            "Laku",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Laku;

    invoke-direct {v0, p0}, Laku;-><init>(Lbjv;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Laku;->a:Ljava/lang/Object;

    .line 50
    if-nez v0, :cond_1

    .line 51
    monitor-enter p0

    .line 52
    :try_start_0
    iget-object v1, p0, Laku;->a:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 53
    iget-object v0, p0, Laku;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "supplier delegate returns null"

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 54
    iput-object v0, p0, Laku;->a:Ljava/lang/Object;

    .line 56
    :cond_0
    monitor-exit p0

    .line 59
    :cond_1
    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 38
    monitor-enter p0

    .line 39
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Laku;->a:Ljava/lang/Object;

    .line 40
    monitor-exit p0

    .line 41
    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
