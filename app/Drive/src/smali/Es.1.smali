.class public LEs;
.super Ljava/lang/Object;
.source "GoogleDocsEntryCreator.java"

# interfaces
.implements LCN;


# instance fields
.field public final a:I

.field public final a:LNl;

.field private final a:LTT;

.field private final a:LtK;


# direct methods
.method constructor <init>(LtK;LTT;LNl;I)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LEs;->a:LtK;

    .line 77
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTT;

    iput-object v0, p0, LEs;->a:LTT;

    .line 78
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNl;

    iput-object v0, p0, LEs;->a:LNl;

    .line 79
    iput p4, p0, LEs;->a:I

    .line 80
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, LEs;->a:I

    return v0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, LEs;->a:LNl;

    invoke-virtual {v0}, LNl;->a()LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, LEs;->a:LNl;

    .line 93
    invoke-virtual {v0}, LNl;->a()LaGv;

    move-result-object v0

    .line 92
    invoke-static {p1, v0, p2, p3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;LaGv;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 108
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {p0}, LEs;->a()LaGv;

    move-result-object v2

    invoke-virtual {v1, v2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    iget-object v1, p0, LEs;->a:LtK;

    sget-object v2, Lry;->x:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LEs;->a:LTT;

    .line 113
    invoke-virtual {p0}, LEs;->a()LaGv;

    move-result-object v2

    invoke-virtual {v2}, LaGv;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LTT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, LEs;->a:LNl;

    invoke-virtual {v0}, LNl;->a()LaGv;

    move-result-object v0

    invoke-virtual {v0}, LaGv;->a()I

    move-result v0

    return v0
.end method
