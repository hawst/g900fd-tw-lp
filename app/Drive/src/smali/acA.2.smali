.class public LacA;
.super Landroid/widget/BaseAdapter;
.source "SharingListAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LabI;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/widget/SectionIndexer;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, LacC;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LacC;-><init>(LacB;)V

    sput-object v0, LacA;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;LabI;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LacD;",
            ">;",
            "LabI;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 92
    iput-object p1, p0, LacA;->a:Landroid/content/Context;

    .line 93
    iput-object p2, p0, LacA;->a:Ljava/util/List;

    .line 94
    iput-object p3, p0, LacA;->a:LabI;

    .line 95
    iput-object p4, p0, LacA;->a:Ljava/lang/String;

    .line 96
    iput-boolean p5, p0, LacA;->a:Z

    .line 97
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LacA;->b:Z

    .line 98
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LacA;->a:Landroid/view/LayoutInflater;

    .line 99
    new-instance v0, LacJ;

    invoke-direct {v0, p2}, LacJ;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LacA;->a:Landroid/widget/SectionIndexer;

    .line 100
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 113
    sget v0, Lxc;->share_user_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 114
    sget v0, Lxc;->sharing_option:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    sget v0, Lxc;->sharing_option:I

    iget-object v3, p0, LacA;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v3}, LacA;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 116
    sget v0, Lxc;->share_options:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 117
    iget-boolean v3, p0, LacA;->a:Z

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    sget v1, Lxb;->ic_btn_round_more:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    invoke-direct {p0, p1, v2}, LacA;->a(Landroid/view/View;Z)V

    .line 120
    return-object p1
.end method

.method private a(Landroid/view/View;I)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 124
    sget v0, Lxc;->share_user_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    sget v0, Lxc;->sharing_option:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, LacA;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 127
    invoke-virtual {v0}, LacD;->a()LabD;

    move-result-object v5

    .line 128
    invoke-interface {v5}, LabD;->a()Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-interface {v5}, LabD;->b()Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 132
    sget v6, Lxc;->share_name:I

    invoke-direct {p0, p1, v6, v1}, LacA;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 141
    :goto_0
    sget v6, Lxc;->share_email:I

    if-eqz v2, :cond_4

    move-object v1, v2

    :goto_1
    invoke-direct {p0, p1, v6, v1}, LacA;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 143
    iget-boolean v1, p0, LacA;->b:Z

    if-eqz v1, :cond_5

    .line 144
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqt;

    move-result-object v1

    invoke-direct {p0, p2, p1, v1}, LacA;->a(ILandroid/view/View;Lqt;)V

    .line 149
    :goto_2
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqv;

    move-result-object v6

    .line 150
    sget v0, Lxc;->share_options:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 151
    sget-object v1, Lqv;->a:Lqv;

    if-eq v6, v1, :cond_6

    move v1, v3

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    sget-object v1, Lqv;->e:Lqv;

    if-ne v6, v1, :cond_7

    sget v1, Lxb;->ic_btn_round_plus:I

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 154
    sget v0, Lxc;->share_badge:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 155
    instance-of v1, v0, Landroid/widget/QuickContactBadge;

    if-eqz v1, :cond_8

    .line 156
    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 157
    sget v1, LabI;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setImageResource(I)V

    .line 158
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    .line 161
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setMode(I)V

    .line 162
    iget-object v1, p0, LacA;->a:LabI;

    invoke-interface {v5}, LabD;->b()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 166
    :goto_5
    return-object p1

    .line 133
    :cond_1
    if-eqz v2, :cond_3

    .line 134
    const/16 v1, 0x40

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 135
    if-lez v1, :cond_2

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 136
    :goto_6
    sget v6, Lxc;->share_name:I

    invoke-direct {p0, p1, v6, v1}, LacA;->a(Landroid/view/View;ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 135
    goto :goto_6

    .line 138
    :cond_3
    const-string v1, "SharingListAdapter"

    const-string v6, "Both display name and email of the contact are null."

    invoke-static {v1, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 141
    :cond_4
    const-string v1, ""

    goto :goto_1

    .line 146
    :cond_5
    invoke-direct {p0, p1, v3}, LacA;->a(Landroid/view/View;Z)V

    goto :goto_2

    :cond_6
    move v1, v4

    .line 151
    goto :goto_3

    .line 152
    :cond_7
    sget v1, Lxb;->ic_btn_round_more:I

    goto :goto_4

    .line 164
    :cond_8
    const-string v0, "SharingListAdapter"

    const-string v1, "Unable to find badge view for the contact."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 194
    if-nez p1, :cond_0

    .line 195
    iget-object v0, p0, LacA;->a:Landroid/view/LayoutInflater;

    sget v1, Lxe;->sharing_entry_group:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 199
    :cond_0
    return-object p1
.end method

.method public static a()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    sget-object v0, LacA;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method private a(ILandroid/view/View;Lqt;)V
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0, p1}, LacA;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    sget v0, Lxc;->sharing_group_title:I

    iget-object v1, p0, LacA;->a:Landroid/content/Context;

    invoke-virtual {p3, v1}, Lqt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, LacA;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 228
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, LacA;->a(Landroid/view/View;Z)V

    .line 232
    :goto_0
    return-void

    .line 230
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, LacA;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 203
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 204
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 205
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    :goto_0
    return-void

    .line 207
    :cond_0
    const-string v0, "SharingListAdapter"

    const-string v1, "Text view not found (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 217
    sget v0, Lxc;->sharing_group_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_1

    .line 219
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 223
    :goto_1
    return-void

    .line 219
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 221
    :cond_1
    const-string v0, "SharingListAdapter"

    const-string v1, "Unable to find header view."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private a(I)Z
    .locals 3

    .prologue
    .line 212
    if-eqz p1, :cond_0

    iget-object v0, p0, LacA;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqt;

    move-result-object v1

    iget-object v0, p0, LacA;->a:Ljava/util/List;

    add-int/lit8 v2, p1, -0x1

    .line 213
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqt;

    move-result-object v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, LacA;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LacA;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LacA;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, LacA;->b:Z

    if-eqz v0, :cond_1

    .line 177
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 179
    :goto_0
    return-object v0

    .line 177
    :cond_0
    iget-object v0, p0, LacA;->a:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    goto :goto_0

    .line 179
    :cond_1
    iget-object v0, p0, LacA;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 185
    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, LacA;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, LacA;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, LacA;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0, p2, p3}, LacA;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 105
    iget-boolean v1, p0, LacA;->b:Z

    if-eqz v1, :cond_1

    .line 106
    if-nez p1, :cond_0

    invoke-direct {p0, v0}, LacA;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 108
    :goto_0
    return-object v0

    .line 106
    :cond_0
    add-int/lit8 v1, p1, -0x1

    invoke-direct {p0, v0, v1}, LacA;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_1
    invoke-direct {p0, v0, p1}, LacA;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
