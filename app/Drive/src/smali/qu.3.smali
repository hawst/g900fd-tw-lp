.class public final enum Lqu;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lqu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lqu;

.field private static final synthetic a:[Lqu;

.field public static final enum b:Lqu;

.field public static final enum c:Lqu;

.field public static final enum d:Lqu;

.field public static final enum e:Lqu;

.field public static final enum f:Lqu;

.field public static final enum g:Lqu;

.field public static final enum h:Lqu;

.field public static final enum i:Lqu;

.field public static final enum j:Lqu;

.field public static final enum k:Lqu;

.field public static final enum l:Lqu;

.field public static final enum m:Lqu;

.field public static final enum n:Lqu;


# instance fields
.field private final a:I

.field private final a:Lqt;

.field private final a:Lqx;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 225
    new-instance v0, Lqu;

    const-string v1, "PRIVATE"

    sget-object v3, Lqt;->g:Lqt;

    sget-object v4, Lqx;->e:Lqx;

    sget v6, Lxi;->sharing_option_private:I

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v0, Lqu;->a:Lqu;

    .line 227
    new-instance v3, Lqu;

    const-string v4, "ANYONE_CAN_EDIT"

    sget-object v6, Lqt;->b:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_can_edit:I

    move v5, v10

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->b:Lqu;

    .line 229
    new-instance v3, Lqu;

    const-string v4, "ANYONE_WITH_LINK_CAN_EDIT"

    sget-object v6, Lqt;->b:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_with_link_can_edit:I

    move v5, v11

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->c:Lqu;

    .line 231
    new-instance v3, Lqu;

    const-string v4, "ANYONE_CAN_COMMENT"

    sget-object v6, Lqt;->c:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_can_comment:I

    move v5, v12

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->d:Lqu;

    .line 233
    new-instance v3, Lqu;

    const-string v4, "ANYONE_WITH_LINK_CAN_COMMENT"

    sget-object v6, Lqt;->c:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_with_link_can_comment:I

    move v5, v13

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->e:Lqu;

    .line 235
    new-instance v3, Lqu;

    const-string v4, "ANYONE_CAN_VIEW"

    const/4 v5, 0x5

    sget-object v6, Lqt;->d:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_can_view:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->f:Lqu;

    .line 237
    new-instance v3, Lqu;

    const-string v4, "ANYONE_WITH_LINK_CAN_VIEW"

    const/4 v5, 0x6

    sget-object v6, Lqt;->d:Lqt;

    sget-object v7, Lqx;->d:Lqx;

    sget v9, Lxi;->sharing_option_anyone_with_link_can_view:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->g:Lqu;

    .line 240
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_CAN_EDIT"

    const/4 v5, 0x7

    sget-object v6, Lqt;->b:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_can_edit:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->h:Lqu;

    .line 242
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_EDIT"

    const/16 v5, 0x8

    sget-object v6, Lqt;->b:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_with_link_can_edit:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->i:Lqu;

    .line 244
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_CAN_COMMENT"

    const/16 v5, 0x9

    sget-object v6, Lqt;->c:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_can_comment:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->j:Lqu;

    .line 246
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_COMMENT"

    const/16 v5, 0xa

    sget-object v6, Lqt;->c:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_with_link_can_comment:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->k:Lqu;

    .line 248
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_CAN_VIEW"

    const/16 v5, 0xb

    sget-object v6, Lqt;->d:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_can_view:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->l:Lqu;

    .line 250
    new-instance v3, Lqu;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_VIEW"

    const/16 v5, 0xc

    sget-object v6, Lqt;->d:Lqt;

    sget-object v7, Lqx;->c:Lqx;

    sget v9, Lxi;->sharing_option_anyone_from_with_link_can_view:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->m:Lqu;

    .line 252
    new-instance v3, Lqu;

    const-string v4, "UNKNOWN"

    const/16 v5, 0xd

    sget-object v6, Lqt;->g:Lqt;

    sget-object v7, Lqx;->e:Lqx;

    sget v9, Lxi;->sharing_option_unknown:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, Lqu;-><init>(Ljava/lang/String;ILqt;Lqx;ZI)V

    sput-object v3, Lqu;->n:Lqu;

    .line 224
    const/16 v0, 0xe

    new-array v0, v0, [Lqu;

    sget-object v1, Lqu;->a:Lqu;

    aput-object v1, v0, v2

    sget-object v1, Lqu;->b:Lqu;

    aput-object v1, v0, v10

    sget-object v1, Lqu;->c:Lqu;

    aput-object v1, v0, v11

    sget-object v1, Lqu;->d:Lqu;

    aput-object v1, v0, v12

    sget-object v1, Lqu;->e:Lqu;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, Lqu;->f:Lqu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lqu;->g:Lqu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lqu;->h:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lqu;->i:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lqu;->j:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lqu;->k:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lqu;->l:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lqu;->m:Lqu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lqu;->n:Lqu;

    aput-object v2, v0, v1

    sput-object v0, Lqu;->a:[Lqu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILqt;Lqx;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lqt;",
            "Lqx;",
            "ZI)V"
        }
    .end annotation

    .prologue
    .line 263
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 264
    iput-object p3, p0, Lqu;->a:Lqt;

    .line 265
    iput-object p4, p0, Lqu;->a:Lqx;

    .line 266
    iput-boolean p5, p0, Lqu;->a:Z

    .line 267
    iput p6, p0, Lqu;->a:I

    .line 268
    return-void
.end method

.method public static a(Lqt;Lqx;Z)Lqu;
    .locals 5

    .prologue
    .line 293
    sget-object v0, Lqt;->g:Lqt;

    invoke-virtual {p0, v0}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    invoke-static {}, Lqu;->values()[Lqu;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 295
    iget-object v4, v0, Lqu;->a:Lqt;

    invoke-virtual {v4, p0}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lqu;->a:Lqx;

    .line 296
    invoke-virtual {v4, p1}, Lqx;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, v0, Lqu;->a:Z

    if-ne v4, p2, :cond_0

    .line 302
    :goto_1
    return-object v0

    .line 294
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 302
    :cond_1
    sget-object v0, Lqu;->n:Lqu;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lqu;
    .locals 1

    .prologue
    .line 224
    const-class v0, Lqu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lqu;

    return-object v0
.end method

.method public static values()[Lqu;
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lqu;->a:[Lqu;

    invoke-virtual {v0}, [Lqu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqu;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lqu;->a:Lqx;

    sget-object v1, Lqx;->c:Lqx;

    invoke-virtual {v0, v1}, Lqx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget v0, p0, Lqu;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lqu;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Lqt;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lqu;->a:Lqt;

    return-object v0
.end method

.method public a()Lqx;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lqu;->a:Lqx;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lqu;->a:Z

    return v0
.end method
