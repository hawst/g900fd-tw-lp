.class public final LME;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LMF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 38
    iput-object p1, p0, LME;->a:LbrA;

    .line 39
    const-class v0, LMF;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LME;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LME;->a:Lbsk;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 88
    packed-switch p1, :pswitch_data_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :pswitch_0
    new-instance v0, LMF;

    invoke-direct {v0}, LMF;-><init>()V

    .line 92
    iget-object v1, p0, LME;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LME;

    .line 93
    invoke-virtual {v1, v0}, LME;->a(LMF;)V

    .line 94
    return-object v0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x2b4
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 117
    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 74
    const-class v0, LMF;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb0

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LME;->a(LbuP;LbuB;)V

    .line 77
    const-class v0, LMF;

    iget-object v1, p0, LME;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LME;->a(Ljava/lang/Class;Lbsk;)V

    .line 78
    iget-object v0, p0, LME;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2b4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 80
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 103
    packed-switch p1, :pswitch_data_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :pswitch_0
    check-cast p2, LMF;

    .line 107
    iget-object v0, p0, LME;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LME;

    .line 108
    invoke-virtual {v0, p2}, LME;->a(LMF;)V

    .line 113
    return-void

    .line 103
    :pswitch_data_0
    .packed-switch 0xb0
        :pswitch_0
    .end packed-switch
.end method

.method public a(LMF;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, LME;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->K:Lbsk;

    .line 51
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LME;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->K:Lbsk;

    .line 49
    invoke-static {v0, v1}, LME;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAP;

    iput-object v0, p1, LMF;->a:LAP;

    .line 55
    iget-object v0, p0, LME;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->g:Lbsk;

    .line 58
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LME;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->g:Lbsk;

    .line 56
    invoke-static {v0, v1}, LME;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwm;

    iput-object v0, p1, LMF;->a:Lwm;

    .line 62
    iget-object v0, p0, LME;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    iget-object v0, v0, LapK;->f:Lbsk;

    .line 65
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LME;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LapK;

    iget-object v1, v1, LapK;->f:Lbsk;

    .line 63
    invoke-static {v0, v1}, LME;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn;

    iput-object v0, p1, LMF;->a:Lapn;

    .line 69
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
