.class public LUG;
.super Ljava/lang/Object;
.source "OpenUrlActivityDelegate.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "LaGu;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LUP;

.field final synthetic a:Landroid/app/ProgressDialog;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field final synthetic a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;LUP;Landroid/net/Uri;Lcom/google/android/gms/drive/database/data/ResourceSpec;ZLandroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iput-object p2, p0, LUG;->a:LUP;

    iput-object p3, p0, LUG;->a:Landroid/net/Uri;

    iput-object p4, p0, LUG;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-boolean p5, p0, LUG;->a:Z

    iput-object p6, p0, LUG;->a:Landroid/app/ProgressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 6

    .prologue
    .line 354
    iget-object v0, p0, LUG;->a:LUP;

    iget-object v1, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v2, p0, LUG;->a:Landroid/net/Uri;

    iget-object v3, p0, LUG;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    iget-boolean v5, p0, LUG;->a:Z

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LUP;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;LaGu;Z)Landroid/content/Intent;

    move-result-object v0

    .line 357
    iget-object v1, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/content/Intent;)V

    .line 358
    iget-object v0, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v1, p0, LUG;->a:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/app/ProgressDialog;)V

    .line 359
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 346
    check-cast p1, LaGu;

    invoke-virtual {p0, p1}, LUG;->a(LaGu;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v1, p0, LUG;->a:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/app/ProgressDialog;)V

    .line 364
    iget-object v0, p0, LUG;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/Throwable;)V

    .line 365
    return-void
.end method
