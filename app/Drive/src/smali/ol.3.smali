.class Lol;
.super Ljava/util/TimerTask;
.source "GAServiceProxy.java"


# instance fields
.field final synthetic a:Log;


# direct methods
.method private constructor <init>(Log;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lol;->a:Log;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Log;Loh;)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0, p1}, Lol;-><init>(Log;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 364
    iget-object v0, p0, Lol;->a:Log;

    invoke-static {v0}, Log;->a(Log;)Lok;

    move-result-object v0

    sget-object v1, Lok;->b:Lok;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lol;->a:Log;

    invoke-static {v0}, Log;->a(Log;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lol;->a:Log;

    invoke-static {v0}, Log;->a(Log;)J

    move-result-wide v0

    iget-object v2, p0, Lol;->a:Log;

    invoke-static {v2}, Log;->b(Log;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lol;->a:Log;

    invoke-static {v2}, Log;->a(Log;)LnY;

    move-result-object v2

    invoke-interface {v2}, LnY;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 367
    const-string v0, "Disconnecting due to inactivity"

    invoke-static {v0}, LoD;->e(Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lol;->a:Log;

    invoke-static {v0}, Log;->d(Log;)V

    .line 372
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v0, p0, Lol;->a:Log;

    invoke-static {v0}, Log;->a(Log;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lol;

    iget-object v2, p0, Lol;->a:Log;

    invoke-direct {v1, v2}, Lol;-><init>(Log;)V

    iget-object v2, p0, Lol;->a:Log;

    invoke-static {v2}, Log;->b(Log;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method
