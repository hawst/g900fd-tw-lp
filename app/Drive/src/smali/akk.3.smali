.class public Lakk;
.super Ljava/lang/Object;
.source "AppValidator.java"


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lakz;

.field private final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const-string v0, "24bb24c05e47e0aefa68a58a766179d9b613a600"

    const-string v1, "38918a453d07199354f8b19af05ec6562ced5788"

    const-string v2, "58e1c4133f7441ec3d2c270270a14802da47ba0e"

    invoke-static {v0, v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lakk;->a:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lakz;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lakk;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lakk;->a:Lakz;

    .line 25
    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lakk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 43
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 44
    invoke-virtual {p0, v4}, Lakk;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 45
    const/4 v0, 0x1

    .line 48
    :cond_0
    return v0

    .line 43
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lakk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 66
    const/16 v2, 0x40

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 68
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 69
    iget-object v5, p0, Lakk;->a:Lakz;

    invoke-virtual {v4}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v4

    sget-object v6, Lakk;->a:Ljava/util/Set;

    invoke-interface {v5, v4, v6}, Lakz;->a([BLjava/util/Set;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-eqz v4, :cond_1

    .line 70
    const/4 v0, 0x1

    .line 77
    :cond_0
    :goto_1
    return v0

    .line 68
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v1

    goto :goto_1
.end method
