.class public final enum Laca;
.super Ljava/lang/Enum;
.source "ModifySharingActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laca;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laca;

.field private static final synthetic a:[Laca;

.field public static final enum b:Laca;

.field public static final enum c:Laca;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    new-instance v0, Laca;

    const-string v1, "POPULATING_ACLS"

    invoke-direct {v0, v1, v2}, Laca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laca;->a:Laca;

    .line 104
    new-instance v0, Laca;

    const-string v1, "SAVING_ACLS"

    invoke-direct {v0, v1, v3}, Laca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laca;->b:Laca;

    .line 105
    new-instance v0, Laca;

    const-string v1, "EDITING_ACLS"

    invoke-direct {v0, v1, v4}, Laca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laca;->c:Laca;

    .line 101
    const/4 v0, 0x3

    new-array v0, v0, [Laca;

    sget-object v1, Laca;->a:Laca;

    aput-object v1, v0, v2

    sget-object v1, Laca;->b:Laca;

    aput-object v1, v0, v3

    sget-object v1, Laca;->c:Laca;

    aput-object v1, v0, v4

    sput-object v0, Laca;->a:[Laca;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laca;
    .locals 1

    .prologue
    .line 101
    const-class v0, Laca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laca;

    return-object v0
.end method

.method public static values()[Laca;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Laca;->a:[Laca;

    invoke-virtual {v0}, [Laca;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laca;

    return-object v0
.end method
