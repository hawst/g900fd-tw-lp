.class LNk;
.super Ljava/lang/Object;
.source "UriDataSource.java"

# interfaces
.implements LNd;


# instance fields
.field private final a:LalY;

.field private final a:Landroid/content/ContentResolver;

.field private final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/content/ContentResolver;LalY;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LNk;->a:Landroid/net/Uri;

    .line 34
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LNk;->a:Landroid/content/ContentResolver;

    .line 35
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalY;

    iput-object v0, p0, LNk;->a:LalY;

    .line 36
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    .line 60
    const/4 v0, -0x1

    .line 61
    const/4 v1, 0x0

    .line 63
    :try_start_0
    iget-object v2, p0, LNk;->a:Landroid/content/ContentResolver;

    iget-object v3, p0, LNk;->a:Landroid/net/Uri;

    const-string v4, "r"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 64
    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    long-to-int v0, v2

    .line 71
    :cond_0
    if-eqz v1, :cond_1

    .line 72
    :try_start_1
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 78
    :cond_1
    :goto_0
    return v0

    .line 74
    :catch_0
    move-exception v1

    .line 75
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 67
    :catch_1
    move-exception v2

    .line 68
    :try_start_2
    const-string v2, "ItemToUpload.UriDataSource"

    const-string v3, "Error opening file to obtain size."

    invoke-static {v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 71
    if-eqz v1, :cond_1

    .line 72
    :try_start_3
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 74
    :catch_2
    move-exception v1

    .line 75
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    .line 71
    if-eqz v1, :cond_2

    .line 72
    :try_start_4
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 76
    :cond_2
    :goto_1
    throw v0

    .line 74
    :catch_3
    move-exception v1

    .line 75
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a()Ljava/io/File;
    .locals 3

    .prologue
    .line 84
    const-string v0, "file"

    iget-object v1, p0, LNk;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LNk;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, LNk;->a:LalY;

    iget-object v1, p0, LNk;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LalY;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, LNk;->a:LalY;

    iget-object v1, p0, LNk;->a:Landroid/content/ContentResolver;

    iget-object v2, p0, LNk;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LalY;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 40
    .line 42
    :try_start_0
    iget-object v0, p0, LNk;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LNk;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 44
    if-nez v0, :cond_0

    .line 45
    new-instance v0, LNh;

    const-string v1, "Failed to open input stream"

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    new-instance v0, LNh;

    const-string v1, "Shared item not found."

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    return-object v0
.end method
