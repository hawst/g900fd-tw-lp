.class LaxC;
.super Laxy;
.source "PdfLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "LavJ;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laxz;

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Laxz;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 352
    iput-object p1, p0, LaxC;->a:Laxz;

    .line 353
    sget-object v0, LaxR;->a:LaxR;

    invoke-direct {p0, p1, v0}, Laxy;-><init>(Laxz;LaxR;)V

    .line 354
    iput-object p2, p0, LaxC;->a:Ljava/lang/String;

    .line 355
    return-void
.end method


# virtual methods
.method protected a(LavE;)LavJ;
    .locals 3

    .prologue
    .line 359
    iget-object v0, p0, LaxC;->a:Laxz;

    invoke-static {v0}, Laxz;->a(Laxz;)LauW;

    move-result-object v0

    if-nez v0, :cond_0

    .line 360
    const-string v0, "PdfLoader"

    const-string v1, "Can\'t load file (data unavailable)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    sget-object v0, LavJ;->a:LavJ;

    .line 369
    :goto_0
    return-object v0

    .line 363
    :cond_0
    iget-object v0, p0, LaxC;->a:Laxz;

    invoke-static {v0}, Laxz;->a(Laxz;)LauW;

    move-result-object v0

    invoke-virtual {v0}, LauW;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 364
    if-nez v0, :cond_1

    .line 365
    const-string v0, "PdfLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t load file (doesn\'t open) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaxC;->a:Laxz;

    invoke-static {v2}, Laxz;->a(Laxz;)LauW;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    sget-object v0, LavJ;->a:LavJ;

    goto :goto_0

    .line 368
    :cond_1
    iget-object v1, p0, LaxC;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LavE;->a(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V

    .line 369
    invoke-static {}, LavJ;->values()[LavJ;

    move-result-object v0

    invoke-interface {p1}, LavE;->a()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349
    invoke-virtual {p0, p1}, LaxC;->a(LavE;)LavJ;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;LavJ;)V
    .locals 2

    .prologue
    .line 374
    sget-object v0, LaxB;->a:[I

    invoke-virtual {p2}, LavJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 387
    :goto_0
    return-void

    .line 376
    :pswitch_0
    iget-object v0, p0, LaxC;->a:Laxz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laxz;->a(Laxz;LauW;)LauW;

    .line 377
    iget-object v0, p0, LaxC;->a:Laxz;

    invoke-virtual {v0}, Laxz;->a()I

    move-result v0

    invoke-interface {p1, v0}, LaxH;->a(I)V

    goto :goto_0

    .line 380
    :pswitch_1
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LaxH;->a(Z)V

    goto :goto_0

    .line 384
    :pswitch_2
    invoke-interface {p1}, LaxH;->a()V

    goto :goto_0

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 349
    check-cast p2, LavJ;

    invoke-virtual {p0, p1, p2}, LaxC;->a(LaxH;LavJ;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390
    const-string v0, "ApplyPasswordTask"

    return-object v0
.end method
