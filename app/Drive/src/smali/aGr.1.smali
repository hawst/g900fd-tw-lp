.class public LaGr;
.super Ljava/lang/Object;
.source "DocumentContent.java"


# instance fields
.field private final a:LaEz;

.field private a:Ljava/io/File;

.field private a:Ljava/lang/Long;

.field a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private a:Ljavax/crypto/SecretKey;

.field private a:Z

.field private b:Ljava/io/File;

.field private b:Ljava/lang/Long;

.field private final b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private e:Z


# direct methods
.method constructor <init>(LaEz;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, LaGr;->a:LaEz;

    .line 66
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGr;->b:Ljava/lang/String;

    .line 67
    iput-object v1, p0, LaGr;->a:Ljava/lang/Long;

    .line 68
    iput-boolean v2, p0, LaGr;->a:Z

    .line 69
    iput-object v1, p0, LaGr;->c:Ljava/lang/Long;

    .line 70
    iput-object v1, p0, LaGr;->d:Ljava/lang/Long;

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGr;->c:Z

    .line 72
    iput-boolean v2, p0, LaGr;->d:Z

    .line 73
    iput-boolean v2, p0, LaGr;->e:Z

    .line 74
    iput-object v1, p0, LaGr;->a:Ljava/lang/String;

    .line 75
    iput-object v1, p0, LaGr;->e:Ljava/lang/String;

    .line 76
    return-void
.end method


# virtual methods
.method public a()LaGp;
    .locals 22

    .prologue
    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, LaGr;->a:Ljava/io/File;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LaGr;->b:Ljava/io/File;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LbiT;->b(Z)V

    .line 196
    new-instance v1, LaGp;

    move-object/from16 v0, p0

    iget-object v2, v0, LaGr;->a:LaEz;

    move-object/from16 v0, p0

    iget-object v3, v0, LaGr;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LaGr;->a:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, LaGr;->b:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, LaGr;->a:Ljavax/crypto/SecretKey;

    move-object/from16 v0, p0

    iget-object v7, v0, LaGr;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, LaGr;->a:Z

    move-object/from16 v0, p0

    iget-object v9, v0, LaGr;->c:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-boolean v10, v0, LaGr;->b:Z

    move-object/from16 v0, p0

    iget-object v11, v0, LaGr;->d:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v12, v0, LaGr;->b:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v13, v0, LaGr;->a:Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v14, v0, LaGr;->a:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v15, v0, LaGr;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, LaGr;->c:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LaGr;->d:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LaGr;->e:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LaGr;->a:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LaGr;->e:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-direct/range {v1 .. v21}, LaGp;-><init>(LaEz;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;LaGq;)V

    .line 216
    return-object v1

    .line 195
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public a(LaGp;)LaGr;
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p1}, LaGp;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGr;->b(Ljava/lang/String;)LaGr;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, LaGp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->a(Ljava/lang/String;)LaGr;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, LaGp;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->a(Ljava/util/Date;)LaGr;

    move-result-object v0

    .line 185
    invoke-virtual {p1}, LaGp;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->c(Ljava/lang/Long;)LaGr;

    move-result-object v0

    .line 186
    invoke-virtual {p1}, LaGp;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->d(Ljava/lang/Long;)LaGr;

    move-result-object v0

    .line 187
    invoke-virtual {p1}, LaGp;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, LaGr;->c(Z)LaGr;

    move-result-object v0

    .line 188
    invoke-virtual {p1}, LaGp;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, LaGr;->d(Z)LaGr;

    move-result-object v0

    .line 189
    invoke-virtual {p1}, LaGp;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->c(Ljava/lang/String;)LaGr;

    move-result-object v0

    .line 190
    invoke-virtual {p1}, LaGp;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->d(Ljava/lang/String;)LaGr;

    .line 191
    return-object p0
.end method

.method public a(Ljava/io/File;)LaGr;
    .locals 1

    .prologue
    .line 115
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LaGr;->b:Ljava/io/File;

    .line 116
    return-object p0
.end method

.method public a(Ljava/io/File;Ljavax/crypto/SecretKey;)LaGr;
    .locals 1

    .prologue
    .line 120
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LaGr;->a:Ljava/io/File;

    .line 121
    iput-object p2, p0, LaGr;->a:Ljavax/crypto/SecretKey;

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, LaGr;->a:Z

    .line 123
    return-object p0
.end method

.method a(Ljava/lang/Long;)LaGr;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, LaGr;->c:Ljava/lang/Long;

    .line 86
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaGr;
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, LaGr;->d:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method public a(Ljava/util/Date;)LaGr;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, LaGr;->a:Ljava/util/Date;

    .line 96
    return-object p0
.end method

.method public a(Z)LaGr;
    .locals 0

    .prologue
    .line 134
    iput-boolean p1, p0, LaGr;->b:Z

    .line 135
    return-object p0
.end method

.method public b()LaGp;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, LaGr;->a:LaEz;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    invoke-virtual {p0}, LaGr;->a()LaGp;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, LaGp;->e()V

    .line 223
    return-object v0
.end method

.method public b(Ljava/io/File;)LaGr;
    .locals 1

    .prologue
    .line 127
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LaGr;->a:Ljava/io/File;

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, LaGr;->a:Ljavax/crypto/SecretKey;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGr;->a:Z

    .line 130
    return-object p0
.end method

.method public b(Ljava/lang/Long;)LaGr;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, LaGr;->a:Ljava/lang/Long;

    .line 91
    return-object p0
.end method

.method public b(Ljava/lang/String;)LaGr;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, LaGr;->c:Ljava/lang/String;

    .line 111
    return-object p0
.end method

.method public b(Z)LaGr;
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, LaGr;->c:Z

    .line 148
    return-object p0
.end method

.method public c(Ljava/lang/Long;)LaGr;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, LaGr;->b:Ljava/lang/Long;

    .line 106
    return-object p0
.end method

.method public c(Ljava/lang/String;)LaGr;
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, LaGr;->a:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public c(Z)LaGr;
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, LaGr;->d:Z

    .line 156
    return-object p0
.end method

.method public d(Ljava/lang/Long;)LaGr;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, LaGr;->d:Ljava/lang/Long;

    .line 140
    return-object p0
.end method

.method public d(Ljava/lang/String;)LaGr;
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, LaGr;->e:Ljava/lang/String;

    .line 174
    return-object p0
.end method

.method public d(Z)LaGr;
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, LaGr;->e:Z

    .line 164
    return-object p0
.end method
