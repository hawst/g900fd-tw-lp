.class public Laur;
.super Ljava/lang/Object;
.source "FileActions.java"


# instance fields
.field private final a:Landroid/app/Activity;

.field private a:LauM;

.field private a:Laww;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LauM;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Laur;->a:Landroid/app/Activity;

    .line 53
    iput-object p2, p0, Laur;->a:LauM;

    .line 54
    if-eqz p2, :cond_0

    .line 55
    iget-object v0, p2, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v0, v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Laur;->a(Landroid/net/Uri;)V

    .line 57
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Intent;LauM;Laww;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p1, LauM;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const-string v0, "android.intent.extra.SUBJECT"

    iget-object v1, p1, LauM;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    const-string v0, "android.intent.extra.STREAM"

    iget-object v1, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 130
    instance-of v0, p2, Lawv;

    if-eqz v0, :cond_0

    .line 131
    check-cast p2, Lawv;

    invoke-interface {p2}, Lawv;->a()Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    :cond_0
    return-object p0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 112
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 116
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 117
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v4, "com.google.android.apps.docs"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.apps.docs"

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-object v0, v1

    .line 122
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    invoke-static {v0}, LavO;->a(Landroid/content/Context;)LavT;

    move-result-object v0

    iget-object v1, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LavT;->a(Landroid/app/Activity;)V

    .line 66
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    invoke-virtual {v0, p1}, LavT;->a(Landroid/net/Uri;)V

    .line 67
    return-void
.end method

.method private b()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Laur;->a:LauM;

    iget-object v0, v0, LauM;->a:Landroid/content/Intent;

    return-object v0
.end method

.method private m()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 179
    :try_start_0
    iget-object v1, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Laur;->a:Landroid/app/Activity;

    .line 180
    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 181
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "enableAddToDrive"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 184
    :goto_0
    return v0

    .line 182
    :catch_0
    move-exception v1

    .line 183
    const-string v2, "FileActions"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Laur;->a:LauM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laur;->a:LauM;

    iget-object v0, v0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 301
    iget-object v0, p0, Laur;->a:LauM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laur;->a:LauM;

    iget-object v0, v0, LauM;->a:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 302
    :cond_0
    const/4 v0, 0x0

    .line 309
    :goto_0
    return-object v0

    .line 305
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 306
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.apps.docs"

    const-string v3, "com.google.android.apps.docs.app.detailpanel.DetailActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 307
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 78
    iget-object v2, p0, Laur;->a:LauM;

    if-nez v2, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Laur;->a:LauM;

    iget-object v4, p0, Laur;->a:Laww;

    invoke-static {v2, v3, v4}, Laur;->a(Landroid/content/Intent;LauM;Laww;)Landroid/content/Intent;

    move-result-object v2

    .line 84
    iget-object v3, p0, Laur;->a:Landroid/app/Activity;

    sget v4, Laum;->title_send_intent:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Laur;->a:LauM;

    iget-object v6, v6, LauM;->b:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 85
    invoke-static {v2, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    .line 86
    iget-object v4, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 87
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 88
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v2, "send"

    invoke-virtual {v0, v2}, LavT;->a(Ljava/lang/String;)V

    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    iget-object v1, p0, Laur;->a:LauM;

    if-nez v1, :cond_0

    .line 107
    :goto_0
    return v0

    .line 99
    :cond_0
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Laur;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 100
    if-nez v1, :cond_1

    .line 101
    const-string v1, "FileActions"

    const-string v2, "AddToDrive called and Drive intent not available. "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Laur;->a:LauM;

    iget-object v2, p0, Laur;->a:Laww;

    invoke-static {v1, v0, v2}, Laur;->a(Landroid/content/Intent;LauM;Laww;)Landroid/content/Intent;

    .line 105
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 106
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "addToDrive"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    .line 107
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Laur;->a:LauM;

    iget-object v0, v0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v0, v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v0}, LavY;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-direct {p0}, Laur;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    invoke-direct {p0}, Laur;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v1}, LavY;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Laur;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 174
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-direct {p0}, Laur;->n()Z

    move-result v1

    if-nez v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 207
    :cond_1
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 212
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v1}, LavY;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 230
    invoke-direct {p0}, Laur;->n()Z

    move-result v1

    if-nez v1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 235
    :cond_1
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 240
    iget-object v1, p0, Laur;->a:LauM;

    iget-object v1, v1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v1}, LavY;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 251
    iget-object v1, p0, Laur;->a:LauM;

    if-nez v1, :cond_0

    .line 262
    :goto_0
    return v0

    .line 256
    :cond_0
    iget-object v1, p0, Laur;->a:Laww;

    instance-of v1, v1, Lawu;

    if-eqz v1, :cond_1

    .line 257
    iget-object v0, p0, Laur;->a:Laww;

    check-cast v0, Lawu;

    invoke-interface {v0}, Lawu;->a()I

    move-result v0

    .line 259
    :cond_1
    iget-object v1, p0, Laur;->a:Landroid/app/Activity;

    iget-object v2, p0, Laur;->a:LauM;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/viewer/action/PrintDialogActivity;->a(Landroid/content/Context;LauM;I)Landroid/content/Intent;

    move-result-object v1

    .line 260
    iget-object v2, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 261
    invoke-static {}, LavO;->a()LavT;

    move-result-object v1

    const-string v2, "print"

    invoke-virtual {v1, v2, v0}, LavT;->a(Ljava/lang/String;I)V

    .line 262
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 267
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v0, v2, :cond_0

    .line 269
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    const-string v2, "user"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 270
    iget-object v2, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 271
    const-string v2, "PrintingEnabled"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 275
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public i()Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Laur;->a:LauM;

    if-nez v0, :cond_0

    .line 284
    const/4 v0, 0x0

    .line 294
    :goto_0
    return v0

    .line 289
    :cond_0
    new-instance v1, Landroid/app/DownloadManager$Request;

    iget-object v0, p0, Laur;->a:LauM;

    iget-object v0, v0, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v0, v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 290
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 291
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    const-string v2, "download"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 292
    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    .line 293
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "download"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    .line 294
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 324
    invoke-direct {p0}, Laur;->b()Landroid/content/Intent;

    move-result-object v0

    .line 325
    iget-object v1, p0, Laur;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 326
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "edit"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public k()Z
    .locals 3

    .prologue
    .line 331
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    const-string v1, "TODO: Share link..."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 332
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "shareLink"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    .line 333
    const/4 v0, 0x1

    return v0
.end method

.method public l()Z
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Laur;->a:Landroid/app/Activity;

    const-string v1, "TODO: Add people..."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 338
    invoke-static {}, LavO;->a()LavT;

    move-result-object v0

    const-string v1, "addPeople"

    invoke-virtual {v0, v1}, LavT;->a(Ljava/lang/String;)V

    .line 339
    const/4 v0, 0x1

    return v0
.end method
