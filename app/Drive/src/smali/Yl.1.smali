.class public LYl;
.super Ljava/lang/Object;
.source "PunchSvgWebView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSvg()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    .line 431
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYm;

    invoke-direct {v1, p0}, LYm;-><init>(LYl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 437
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public log(Ljava/lang/String;)Z
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 444
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "jslog: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 445
    return v4
.end method

.method public onTap()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 452
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "in onTap"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, LYl;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b()V

    .line 454
    return-void
.end method
