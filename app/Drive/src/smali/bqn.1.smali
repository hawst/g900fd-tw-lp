.class public abstract enum Lbqn;
.super Ljava/lang/Enum;
.source "SortedLists.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbqn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbqn;

.field private static final synthetic a:[Lbqn;

.field public static final enum b:Lbqn;

.field public static final enum c:Lbqn;

.field public static final enum d:Lbqn;

.field public static final enum e:Lbqn;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lbqo;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lbqo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqn;->a:Lbqn;

    .line 65
    new-instance v0, Lbqp;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lbqp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqn;->b:Lbqn;

    .line 90
    new-instance v0, Lbqq;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lbqq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqn;->c:Lbqn;

    .line 117
    new-instance v0, Lbqr;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Lbqr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqn;->d:Lbqn;

    .line 129
    new-instance v0, Lbqs;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lbqs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbqn;->e:Lbqn;

    .line 49
    const/4 v0, 0x5

    new-array v0, v0, [Lbqn;

    sget-object v1, Lbqn;->a:Lbqn;

    aput-object v1, v0, v2

    sget-object v1, Lbqn;->b:Lbqn;

    aput-object v1, v0, v3

    sget-object v1, Lbqn;->c:Lbqn;

    aput-object v1, v0, v4

    sget-object v1, Lbqn;->d:Lbqn;

    aput-object v1, v0, v5

    sget-object v1, Lbqn;->e:Lbqn;

    aput-object v1, v0, v6

    sput-object v0, Lbqn;->a:[Lbqn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbqi;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lbqn;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbqn;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lbqn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbqn;

    return-object v0
.end method

.method public static values()[Lbqn;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lbqn;->a:[Lbqn;

    invoke-virtual {v0}, [Lbqn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbqn;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;",
            "Ljava/util/List",
            "<+TE;>;I)I"
        }
    .end annotation
.end method
