.class public abstract Lbvw;
.super Ljava/lang/Object;
.source "ProviderMethod.java"

# interfaces
.implements LbwE;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field protected final a:Ljava/lang/Object;

.field protected final a:Ljava/lang/reflect/Method;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LbuE",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:Z


# direct methods
.method private constructor <init>(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<TT;>;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            "LbmY",
            "<",
            "Lbwt",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LbuE",
            "<*>;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lbvw;->a:Lbuv;

    .line 99
    iput-object p6, p0, Lbvw;->a:Ljava/lang/Class;

    .line 100
    iput-object p3, p0, Lbvw;->a:Ljava/lang/Object;

    .line 101
    iput-object p4, p0, Lbvw;->a:LbmY;

    .line 102
    iput-object p2, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    .line 103
    iput-object p5, p0, Lbvw;->a:Ljava/util/List;

    .line 104
    const-class v0, Lbus;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    iput-boolean v0, p0, Lbvw;->a:Z

    .line 105
    return-void
.end method

.method synthetic constructor <init>(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;Lbvx;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lbvw;-><init>(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;)V

    return-void
.end method

.method static a(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;Z)Lbvw;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            "LbmY",
            "<",
            "Lbwt",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LbuE",
            "<*>;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;Z)",
            "Lbvw",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v0

    .line 74
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 79
    :cond_1
    new-instance v0, Lbvy;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lbvy;-><init>(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lbvw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/Object;

    .line 142
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 143
    iget-object v0, p0, Lbvw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v2, v1

    .line 142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 148
    :cond_0
    :try_start_0
    invoke-virtual {p0, v2}, Lbvw;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 149
    return-object v0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 152
    :catch_1
    move-exception v0

    .line 153
    invoke-static {v0}, Lbvj;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method abstract a([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public a()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public a(Lcom/google/inject/Binder;)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Ljava/lang/Object;)Lcom/google/inject/Binder;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lbvw;->a:Ljava/lang/Class;

    if-eqz v1, :cond_1

    .line 128
    iget-object v1, p0, Lbvw;->a:Lbuv;

    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v1

    invoke-interface {v1, p0}, LbuT;->a(LbuE;)LbuU;

    move-result-object v1

    iget-object v2, p0, Lbvw;->a:Ljava/lang/Class;

    invoke-interface {v1, v2}, LbuU;->a(Ljava/lang/Class;)V

    .line 133
    :goto_0
    iget-boolean v1, p0, Lbvw;->a:Z

    if-eqz v1, :cond_0

    .line 136
    check-cast v0, LbuD;

    iget-object v1, p0, Lbvw;->a:Lbuv;

    invoke-interface {v0, v1}, LbuD;->a(Lbuv;)V

    .line 138
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lbvw;->a:Lbuv;

    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v1

    invoke-interface {v1, p0}, LbuT;->a(LbuE;)LbuU;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 180
    instance-of v1, p1, Lbvw;

    if-eqz v1, :cond_0

    .line 181
    check-cast p1, Lbvw;

    .line 182
    iget-object v1, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    iget-object v2, p1, Lbvw;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbvw;->a:Ljava/lang/Object;

    iget-object v2, p1, Lbvw;->a:Ljava/lang/Object;

    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 185
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "@Provides "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbvw;->a:Ljava/lang/reflect/Method;

    invoke-static {v1}, LbvH;->a(Ljava/lang/reflect/Member;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
