.class public LWB;
.super Landroid/print/PrintDocumentAdapter;
.source "KitKatPrintActivity.java"


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Lcom/google/android/apps/docs/print/KitKatPrintActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/print/KitKatPrintActivity;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, LWB;->a:Lcom/google/android/apps/docs/print/KitKatPrintActivity;

    iput-object p2, p0, LWB;->a:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 139
    const-string v0, "PrintActivity"

    const-string v1, "PrintDocumentAdapter::onFinish -- finishing activity."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, LWB;->a:Lcom/google/android/apps/docs/print/KitKatPrintActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->finish()V

    .line 141
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, LWB;->a:Lcom/google/android/apps/docs/print/KitKatPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a(Lcom/google/android/apps/docs/print/KitKatPrintActivity;)Landroid/print/PrintDocumentInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 105
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 2

    .prologue
    .line 110
    const-string v0, "PrintActivity"

    const-string v1, "PrintDocumentAdapter::onWrite"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v0, LWC;

    invoke-direct {v0, p0, p3, p2, p4}, LWC;-><init>(LWB;Landroid/os/CancellationSignal;Landroid/os/ParcelFileDescriptor;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 134
    invoke-virtual {v0, v1}, LWC;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 135
    return-void
.end method
