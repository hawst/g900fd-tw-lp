.class public final LWA;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWM;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWK;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LWA;->a:LbrA;

    .line 41
    const-class v0, LWM;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWA;->a:Lbsk;

    .line 44
    const-class v0, LWK;

    invoke-static {v0, v2}, LWA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWA;->b:Lbsk;

    .line 47
    const-class v0, LWE;

    invoke-static {v0, v2}, LWA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LWA;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 118
    sparse-switch p1, :sswitch_data_0

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :sswitch_0
    new-instance v2, LWM;

    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->Y:Lbsk;

    .line 123
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->Y:Lbsk;

    .line 121
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->j:Lbsk;

    .line 129
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LWA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->j:Lbsk;

    .line 127
    invoke-static {v1, v3}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    invoke-direct {v2, v0, v1}, LWM;-><init>(Laja;Laja;)V

    move-object v0, v2

    .line 196
    :goto_0
    return-object v0

    .line 136
    :sswitch_1
    new-instance v3, LWK;

    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 139
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 137
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 145
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LWA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->L:Lbsk;

    .line 143
    invoke-static {v1, v2}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKR;

    iget-object v2, p0, LWA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 151
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LWA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 149
    invoke-static {v2, v4}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    invoke-direct {v3, v0, v1, v2}, LWK;-><init>(Landroid/content/Context;LaKR;LtK;)V

    move-object v0, v3

    .line 156
    goto :goto_0

    .line 158
    :sswitch_2
    new-instance v0, LWE;

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 161
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LWA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 159
    invoke-static {v1, v2}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LWA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 167
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LWA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 165
    invoke-static {v2, v3}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LtK;

    iget-object v3, p0, LWA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LtQ;

    iget-object v3, v3, LtQ;->w:Lbsk;

    .line 173
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LWA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LtQ;

    iget-object v4, v4, LtQ;->w:Lbsk;

    .line 171
    invoke-static {v3, v4}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LsP;

    iget-object v4, p0, LWA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 179
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LWA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->l:Lbsk;

    .line 177
    invoke-static {v4, v5}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaGM;

    iget-object v5, p0, LWA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->L:Lbsk;

    .line 185
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LWA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->L:Lbsk;

    .line 183
    invoke-static {v5, v6}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaKR;

    iget-object v6, p0, LWA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->e:Lbsk;

    .line 191
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LWA;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LagN;

    iget-object v7, v7, LagN;->e:Lbsk;

    .line 189
    invoke-static {v6, v7}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lago;

    invoke-direct/range {v0 .. v6}, LWE;-><init>(Landroid/content/Context;LtK;LsP;LaGM;LaKR;Lago;)V

    goto/16 :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        0x289 -> :sswitch_0
        0x303 -> :sswitch_1
        0x3f7 -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 219
    .line 221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 98
    const-class v0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x81

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LWA;->a(LbuP;LbuB;)V

    .line 101
    const-class v0, LWM;

    iget-object v1, p0, LWA;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LWA;->a(Ljava/lang/Class;Lbsk;)V

    .line 102
    const-class v0, LWK;

    iget-object v1, p0, LWA;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LWA;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, LWE;

    iget-object v1, p0, LWA;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LWA;->a(Ljava/lang/Class;Lbsk;)V

    .line 104
    iget-object v0, p0, LWA;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x289

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, LWA;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x303

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    iget-object v0, p0, LWA;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x3f7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 110
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 205
    packed-switch p1, :pswitch_data_0

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    .line 209
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWA;

    .line 210
    invoke-virtual {v0, p2}, LWA;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V

    .line 215
    return-void

    .line 205
    :pswitch_data_0
    .packed-switch 0x81
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 57
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 58
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 61
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 59
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lald;

    .line 65
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 68
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 66
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iput-object v0, p1, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LTd;

    .line 72
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->f:Lbsk;

    .line 75
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->f:Lbsk;

    .line 73
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUT;

    iput-object v0, p1, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LUT;

    .line 79
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 82
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 80
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LQr;

    .line 86
    iget-object v0, p0, LWA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->l:Lbsk;

    .line 89
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LWA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->l:Lbsk;

    .line 87
    invoke-static {v0, v1}, LWA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Ljava/lang/Class;

    .line 93
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method
