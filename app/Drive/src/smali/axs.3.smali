.class public Laxs;
.super Ljava/lang/Object;
.source "SelectionHandles.java"


# instance fields
.field private final a:Landroid/view/View$OnTouchListener;

.field private final a:Landroid/widget/ImageView;

.field private final a:Landroid/widget/LinearLayout;

.field private final a:Laxw;

.field private final a:Lcom/google/android/apps/viewer/client/Dimensions;

.field private final a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

.field private final a:Ljava/lang/Object;

.field private final b:Landroid/widget/ImageView;

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Laxw;Lcom/google/android/apps/viewer/viewer/image/ZoomView;Landroid/widget/LinearLayout;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Laxs;->a:Laxw;

    .line 43
    iput-object p2, p0, Laxs;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 44
    iput-object p3, p0, Laxs;->a:Landroid/widget/LinearLayout;

    .line 45
    new-instance v0, Laxv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laxv;-><init>(Laxs;Laxt;)V

    iput-object v0, p0, Laxs;->a:Landroid/view/View$OnTouchListener;

    .line 47
    sget v0, Lauj;->zoomed_view:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 48
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Laxs;->a(Landroid/view/ViewGroup;Z)Landroid/widget/ImageView;

    move-result-object v1

    iput-object v1, p0, Laxs;->a:Landroid/widget/ImageView;

    .line 49
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Laxs;->a(Landroid/view/ViewGroup;Z)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Laxs;->b:Landroid/widget/ImageView;

    .line 51
    invoke-virtual {p3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Laui;->text_select_handle_left:I

    invoke-static {v0, v1}, Laxs;->a(Landroid/content/Context;I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    iput-object v0, p0, Laxs;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 53
    invoke-virtual {p1}, Laxw;->a()Lawe;

    move-result-object v0

    new-instance v1, Laxt;

    invoke-direct {v1, p0}, Laxt;-><init>(Laxs;)V

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laxs;->a:Ljava/lang/Object;

    .line 60
    invoke-virtual {p2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    new-instance v1, Laxu;

    invoke-direct {v1, p0}, Laxu;-><init>(Laxs;)V

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laxs;->b:Ljava/lang/Object;

    .line 68
    return-void
.end method

.method private static a(FFFF)F
    .locals 2

    .prologue
    .line 113
    .line 115
    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p2, v1

    mul-float/2addr v0, v1

    add-float/2addr v0, p0

    .line 117
    mul-float v1, p3, p1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 118
    return v0
.end method

.method private a(Landroid/view/ViewGroup;Z)Landroid/widget/ImageView;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 125
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 126
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    if-eqz p2, :cond_0

    sget v0, Laum;->desc_selection_stop:I

    .line 128
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Laxs;->a:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    return-object v1

    .line 127
    :cond_0
    sget v0, Laum;->desc_selection_start:I

    goto :goto_0
.end method

.method static synthetic a(Laxs;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laxs;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Laxs;)Laxw;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laxs;->a:Laxw;

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 138
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 139
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 140
    new-instance v1, Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/viewer/client/Dimensions;-><init>(II)V

    return-object v1
.end method

.method static synthetic a(Laxs;)Lcom/google/android/apps/viewer/viewer/image/ZoomView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laxs;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Z)V
    .locals 6

    .prologue
    .line 88
    iget-boolean v0, p3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->isRtl:Z

    xor-int v1, p4, v0

    .line 89
    if-eqz v1, :cond_0

    sget v0, Laui;->text_select_handle_right:I

    .line 92
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 94
    iget-object v0, p0, Laxs;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 99
    if-eqz v1, :cond_1

    const/high16 v0, -0x41800000    # -0.25f

    .line 100
    :goto_1
    const v1, -0x42b33333    # -0.05f

    .line 101
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Laxs;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v4

    div-float/2addr v3, v4

    .line 102
    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v4

    iget v5, p3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->x:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget-object v5, p0, Laxs;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v5, v5, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    int-to-float v5, v5

    invoke-static {v4, v5, v3, v0}, Laxs;->a(FFFF)F

    move-result v0

    .line 103
    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v2

    iget v4, p3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->y:I

    int-to-float v4, v4

    add-float/2addr v2, v4

    iget-object v4, p0, Laxs;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v4, v4, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    int-to-float v4, v4

    invoke-static {v2, v4, v3, v1}, Laxs;->a(FFFF)F

    move-result v1

    .line 105
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 106
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 107
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 108
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 109
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    return-void

    .line 89
    :cond_0
    sget v0, Laui;->text_select_handle_left:I

    goto :goto_0

    .line 99
    :cond_1
    const/high16 v0, -0x40c00000    # -0.75f

    goto :goto_1
.end method

.method static synthetic a(Laxs;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Laxs;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/16 v1, 0x8

    .line 76
    iget-object v0, p0, Laxs;->a:Laxw;

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    invoke-interface {v0}, Lawe;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/pdflib/Selection;

    .line 77
    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Laxs;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Laxs;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v1, p0, Laxs;->a:Landroid/widget/ImageView;

    iget v2, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    iget-object v3, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const/4 v4, 0x0

    invoke-direct {p0, v1, v2, v3, v4}, Laxs;->a(Landroid/widget/ImageView;ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Z)V

    .line 82
    iget-object v1, p0, Laxs;->b:Landroid/widget/ImageView;

    iget v2, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    iget-object v0, v0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v0, v3}, Laxs;->a(Landroid/widget/ImageView;ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Laxs;->a:Laxw;

    invoke-virtual {v0}, Laxw;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, Laxs;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Laxs;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Lawe;

    move-result-object v0

    iget-object v1, p0, Laxs;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lawe;->a(Ljava/lang/Object;)V

    .line 73
    return-void
.end method
