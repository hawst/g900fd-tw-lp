.class public Lakh;
.super LbsC;
.source "AnyFuture.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LbsC",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LbsU",
            "<+TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, LbsC;-><init>()V

    .line 38
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lakh;->a:Ljava/util/List;

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 89
    monitor-enter p0

    .line 90
    :try_start_0
    iget-object v0, p0, Lakh;->a:Ljava/util/List;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 91
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsU;

    .line 93
    invoke-interface {v0, p1}, LbsU;->cancel(Z)Z

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 95
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LbsU;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 45
    monitor-enter p0

    .line 46
    :try_start_0
    iget-object v0, p0, Lakh;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    new-instance v0, Lakj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lakj;-><init>(Lakh;LbsU;Laki;)V

    invoke-static {p1, v0}, LbsK;->a(LbsU;LbsJ;)V

    .line 49
    invoke-virtual {p0}, Lakh;->isDone()Z

    move-result v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    const/4 v1, 0x1

    invoke-interface {p1, v1}, LbsU;->cancel(Z)Z

    .line 53
    :cond_0
    return v0

    .line 47
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected a(LbsU;Ljava/lang/Throwable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<+TV;>;",
            "Ljava/lang/Throwable;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 66
    .line 67
    monitor-enter p0

    .line 68
    :try_start_0
    iget-object v0, p0, Lakh;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 69
    iget-object v0, p0, Lakh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 70
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    invoke-super {p0, p2}, LbsC;->a(Ljava/lang/Throwable;)Z

    move-result v0

    .line 74
    :goto_0
    return v0

    .line 70
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 80
    invoke-super {p0, p1}, LbsC;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lakh;->a(Z)V

    .line 84
    :cond_0
    return v0
.end method

.method public cancel(Z)Z
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, LbsC;->cancel(Z)Z

    move-result v0

    .line 59
    if-eqz v0, :cond_0

    .line 60
    invoke-direct {p0, p1}, Lakh;->a(Z)V

    .line 62
    :cond_0
    return v0
.end method
