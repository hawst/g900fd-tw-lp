.class final Lbwe;
.super LbvX;
.source "OptionalBinder.java"

# interfaces
.implements LbuC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LbvX",
        "<TT;>;",
        "LbuC;"
    }
.end annotation


# instance fields
.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "LbiP",
            "<",
            "LbuE",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private a:Lbup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbup",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Lcom/google/inject/Binder;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation
.end field

.field private b:Lbup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbup",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "LbiP",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "LbiP",
            "<",
            "Lbxw",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private final d:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "LbiP",
            "<",
            "LbuE",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private final e:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final f:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/inject/Binder;Lbuv;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/inject/Binder;",
            "Lbuv",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LbvX;-><init>(LbvY;)V

    .line 266
    iput-object p1, p0, Lbwe;->a:Lcom/google/inject/Binder;

    .line 267
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, Lbwe;->a:Lbuv;

    .line 268
    invoke-virtual {p2}, Lbuv;->a()LbuP;

    move-result-object v0

    .line 269
    invoke-static {v0}, Lbwe;->a(LbuP;)LbuP;

    move-result-object v1

    invoke-virtual {p2, v1}, Lbuv;->b(LbuP;)Lbuv;

    move-result-object v1

    iput-object v1, p0, Lbwe;->b:Lbuv;

    .line 270
    invoke-static {v0}, Lbwe;->b(LbuP;)LbuP;

    move-result-object v1

    invoke-virtual {p2, v1}, Lbuv;->b(LbuP;)Lbuv;

    move-result-object v1

    iput-object v1, p0, Lbwe;->c:Lbuv;

    .line 271
    invoke-static {v0}, Lbwe;->c(LbuP;)LbuP;

    move-result-object v0

    invoke-virtual {p2, v0}, Lbuv;->b(LbuP;)Lbuv;

    move-result-object v0

    iput-object v0, p0, Lbwe;->d:Lbuv;

    .line 272
    iget-object v0, p0, Lbwe;->d:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuE;

    move-result-object v0

    iput-object v0, p0, Lbwe;->a:LbuE;

    .line 273
    invoke-static {p2}, Lbwj;->a(Lbuv;)Ljava/lang/String;

    move-result-object v0

    .line 274
    invoke-virtual {p2}, Lbuv;->a()LbuP;

    move-result-object v1

    new-instance v2, Lbwd;

    invoke-direct {v2, v0}, Lbwd;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v1

    iput-object v1, p0, Lbwe;->e:Lbuv;

    .line 275
    invoke-virtual {p2}, Lbuv;->a()LbuP;

    move-result-object v1

    new-instance v2, Lbwa;

    invoke-direct {v2, v0}, Lbwa;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iput-object v0, p0, Lbwe;->f:Lbuv;

    .line 278
    const-class v0, Lbuu;

    invoke-static {v0}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {v0}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lbwe;->a:Ljava/util/Set;

    .line 279
    const-class v0, Lbuu;

    .line 280
    invoke-static {v0}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {v0}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lbwe;->b:Ljava/util/Set;

    .line 281
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/inject/Binder;Lbuv;LbvY;)V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Lbwe;-><init>(Lcom/google/inject/Binder;Lbuv;)V

    return-void
.end method

.method static synthetic a(Lbwe;)LbuE;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->a:LbuE;

    return-object v0
.end method

.method static synthetic a(Lbwe;)Lbup;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->b:Lbup;

    return-object v0
.end method

.method static synthetic a(Lbwe;Lbup;)Lbup;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lbwe;->b:Lbup;

    return-object p1
.end method

.method static synthetic a(Lbwe;)Lbuv;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->a:Lbuv;

    return-object v0
.end method

.method static synthetic a(Lbwe;Lcom/google/inject/Binder;)Lcom/google/inject/Binder;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lbwe;->a:Lcom/google/inject/Binder;

    return-object p1
.end method

.method static synthetic a(Lbwe;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lbwe;->a:Ljava/util/Set;

    return-object p1
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lbwe;->a:Lcom/google/inject/Binder;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lbwe;)Lbup;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->a:Lbup;

    return-object v0
.end method

.method static synthetic b(Lbwe;Lbup;)Lbup;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lbwe;->a:Lbup;

    return-object p1
.end method

.method static synthetic b(Lbwe;)Lbuv;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->f:Lbuv;

    return-object v0
.end method

.method static synthetic b(Lbwe;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lbwe;->b:Ljava/util/Set;

    return-object p1
.end method

.method private b(Lcom/google/inject/Binder;)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lbwe;->a:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    new-instance v1, Lbwf;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lbwf;-><init>(Lbwe;LbvY;)V

    invoke-interface {v0, v1}, LbuT;->a(LbuE;)LbuU;

    .line 289
    return-void
.end method

.method static synthetic c(Lbwe;)Lbuv;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbwe;->e:Lbuv;

    return-object v0
.end method


# virtual methods
.method public a()LbuT;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 292
    invoke-direct {p0}, Lbwe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "already initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LbvT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lbwe;->a:Lcom/google/inject/Binder;

    invoke-direct {p0, v0}, Lbwe;->b(Lcom/google/inject/Binder;)V

    .line 294
    iget-object v0, p0, Lbwe;->a:Lcom/google/inject/Binder;

    iget-object v1, p0, Lbwe;->e:Lbuv;

    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 292
    goto :goto_0
.end method

.method public a(Lcom/google/inject/Binder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 304
    invoke-direct {p0}, Lbwe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "OptionalBinder was already initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LbvT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 306
    iget-object v0, p0, Lbwe;->d:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    new-instance v1, Lbwi;

    invoke-direct {v1, p0, v3}, Lbwi;-><init>(Lbwe;LbvY;)V

    invoke-interface {v0, v1}, LbuT;->a(LbuE;)LbuU;

    .line 311
    iget-object v0, p0, Lbwe;->d:Lbuv;

    .line 312
    iget-object v1, p0, Lbwe;->c:Lbuv;

    invoke-interface {p1, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v1

    invoke-interface {v1, v0}, LbuT;->a(Lbuv;)LbuU;

    .line 314
    iget-object v0, p0, Lbwe;->b:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    new-instance v1, Lbwh;

    invoke-direct {v1, p0, v3}, Lbwh;-><init>(Lbwe;LbvY;)V

    invoke-interface {v0, v1}, LbuT;->a(LbuE;)LbuU;

    .line 315
    return-void

    :cond_0
    move v0, v1

    .line 304
    goto :goto_0
.end method

.method public b()LbuT;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 298
    invoke-direct {p0}, Lbwe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "already initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LbvT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 299
    iget-object v0, p0, Lbwe;->a:Lcom/google/inject/Binder;

    invoke-direct {p0, v0}, Lbwe;->b(Lcom/google/inject/Binder;)V

    .line 300
    iget-object v0, p0, Lbwe;->a:Lcom/google/inject/Binder;

    iget-object v1, p0, Lbwe;->f:Lbuv;

    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 298
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 478
    instance-of v0, p1, Lbwe;

    if-eqz v0, :cond_0

    check-cast p1, Lbwe;

    iget-object v0, p1, Lbwe;->a:Lbuv;

    iget-object v1, p0, Lbwe;->a:Lbuv;

    .line 479
    invoke-virtual {v0, v1}, Lbuv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lbwe;->a:Lbuv;

    invoke-virtual {v0}, Lbuv;->hashCode()I

    move-result v0

    return v0
.end method
