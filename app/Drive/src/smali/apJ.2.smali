.class public final enum LapJ;
.super Ljava/lang/Enum;
.source "EmptyDoclistLayout.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LapJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LapJ;

.field private static final synthetic a:[LapJ;

.field public static final enum b:LapJ;

.field public static final enum c:LapJ;

.field public static final enum d:LapJ;

.field public static final enum e:LapJ;

.field public static final enum f:LapJ;

.field public static final enum g:LapJ;

.field public static final enum h:LapJ;

.field public static final enum i:LapJ;

.field public static final enum j:LapJ;


# instance fields
.field private final a:I

.field private final a:LCn;

.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 19
    new-instance v0, LapJ;

    const-string v1, "INCOMING_PHOTOS"

    const/4 v2, 0x0

    sget-object v3, LCn;->k:LCn;

    sget v4, Lxb;->ic_empty_photos:I

    sget v5, Lxi;->empty_doclist_for_google_photos_view:I

    sget v6, Lxi;->empty_doclist_for_google_photos_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->a:LapJ;

    .line 22
    new-instance v0, LapJ;

    const-string v1, "INCOMING"

    const/4 v2, 0x1

    sget-object v3, LCn;->f:LCn;

    sget v4, Lxb;->ic_empty_incoming:I

    sget v5, Lxi;->empty_doclist_for_incoming_view:I

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->b:LapJ;

    .line 24
    new-instance v0, LapJ;

    const-string v1, "STARRED"

    const/4 v2, 0x2

    sget-object v3, LCn;->g:LCn;

    sget v4, Lxb;->ic_empty_star:I

    sget v5, Lxi;->empty_doclist_for_starred_view:I

    sget v6, Lxi;->empty_doclist_for_starred_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->c:LapJ;

    .line 26
    new-instance v0, LapJ;

    const-string v1, "RECENT"

    const/4 v2, 0x3

    sget-object v3, LCn;->e:LCn;

    sget v4, Lxb;->ic_empty_recent:I

    sget v5, Lxi;->empty_doclist_for_recent_view:I

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->d:LapJ;

    .line 28
    new-instance v0, LapJ;

    const-string v1, "PINNED"

    const/4 v2, 0x4

    sget-object v3, LCn;->i:LCn;

    sget v4, Lxb;->ic_empty_pin:I

    sget v5, Lxi;->empty_doclist_for_pinned_view:I

    sget v6, Lxi;->empty_doclist_for_pinned_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->e:LapJ;

    .line 30
    new-instance v0, LapJ;

    const-string v1, "UPLOADS"

    const/4 v2, 0x5

    sget-object v3, LCn;->h:LCn;

    sget v4, Lxb;->ic_empty_upload:I

    sget v5, Lxi;->empty_doclist_for_uploads_view:I

    sget v6, Lxi;->empty_doclist_for_uploads_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->f:LapJ;

    .line 32
    new-instance v0, LapJ;

    const-string v1, "MY_DRIVE"

    const/4 v2, 0x6

    sget-object v3, LCn;->a:LCn;

    sget v4, Lxb;->ic_empty_drive:I

    sget v5, Lxi;->empty_doclist_for_my_drive_view:I

    sget v6, Lxi;->empty_doclist_for_my_drive_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->g:LapJ;

    .line 34
    new-instance v0, LapJ;

    const-string v1, "MY_DRIVE_WITH_ONE_DOCUMENT"

    const/4 v2, 0x7

    const/4 v3, 0x0

    sget v4, Lxb;->ic_empty_drive:I

    const/4 v5, -0x1

    sget v6, Lxi;->empty_doclist_for_my_drive_view_details:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->h:LapJ;

    .line 36
    new-instance v0, LapJ;

    const-string v1, "PENDING"

    const/16 v2, 0x8

    const/4 v3, 0x0

    sget v4, Lxb;->ic_empty_drive:I

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->i:LapJ;

    .line 37
    new-instance v0, LapJ;

    const-string v1, "OTHER"

    const/16 v2, 0x9

    const/4 v3, 0x0

    sget v4, Lxb;->ic_empty_drive:I

    const/4 v5, -0x1

    sget v6, Lxi;->empty_doclist:I

    invoke-direct/range {v0 .. v6}, LapJ;-><init>(Ljava/lang/String;ILCn;III)V

    sput-object v0, LapJ;->j:LapJ;

    .line 18
    const/16 v0, 0xa

    new-array v0, v0, [LapJ;

    const/4 v1, 0x0

    sget-object v2, LapJ;->a:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LapJ;->b:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LapJ;->c:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LapJ;->d:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LapJ;->e:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LapJ;->f:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LapJ;->g:LapJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LapJ;->h:LapJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LapJ;->i:LapJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LapJ;->j:LapJ;

    aput-object v2, v0, v1

    sput-object v0, LapJ;->a:[LapJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILCn;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCn;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-object p3, p0, LapJ;->a:LCn;

    .line 48
    iput p4, p0, LapJ;->a:I

    .line 49
    iput p5, p0, LapJ;->b:I

    .line 50
    iput p6, p0, LapJ;->c:I

    .line 51
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LCn;)Landroid/view/View;
    .locals 1

    .prologue
    .line 90
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {p2}, LapJ;->a(LCn;)LapJ;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p0, p1}, LapJ;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(LCn;)LapJ;
    .locals 5

    .prologue
    .line 80
    invoke-static {}, LapJ;->values()[LapJ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 81
    iget-object v4, v0, LapJ;->a:LCn;

    invoke-virtual {p0, v4}, LCn;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    :goto_1
    return-object v0

    .line 80
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    sget-object v0, LapJ;->j:LapJ;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LapJ;
    .locals 1

    .prologue
    .line 18
    const-class v0, LapJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LapJ;

    return-object v0
.end method

.method public static values()[LapJ;
    .locals 1

    .prologue
    .line 18
    sget-object v0, LapJ;->a:[LapJ;

    invoke-virtual {v0}, [LapJ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LapJ;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 54
    sget v0, Lxe;->doc_list_empty_view:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 56
    sget v0, Lxc;->empty_list_message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 57
    iget v2, p0, LapJ;->b:I

    if-lez v2, :cond_0

    .line 58
    iget v2, p0, LapJ;->b:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 59
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    :goto_0
    sget v0, Lxc;->empty_list_message_details:I

    .line 65
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    iget v2, p0, LapJ;->c:I

    if-lez v2, :cond_1

    .line 67
    iget v2, p0, LapJ;->c:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 68
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    :goto_1
    sget v0, Lxc;->empty_list_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 74
    iget v2, p0, LapJ;->a:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    return-object v1

    .line 61
    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
