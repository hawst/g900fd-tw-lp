.class public LSC;
.super Ljava/lang/Object;
.source "ScaleGestureDetector.java"


# instance fields
.field private a:F

.field private final a:I

.field private final a:LSD;

.field private a:Z

.field private b:F

.field private final b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LSD;)V
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LSC;->b:Z

    .line 150
    iput-object p2, p0, LSC;->a:LSD;

    .line 151
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LSC;->a:I

    .line 152
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 288
    iget v0, p0, LSC;->a:F

    return v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, LSC;->a:Z

    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    .line 167
    iget-boolean v0, p0, LSC;->b:Z

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 267
    :goto_0
    return v0

    .line 171
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    .line 173
    const/4 v0, 0x1

    if-eq v7, v0, :cond_1

    const/4 v0, 0x3

    if-ne v7, v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 175
    :goto_1
    if-eqz v7, :cond_2

    if-eqz v0, :cond_5

    .line 179
    :cond_2
    iget-boolean v1, p0, LSC;->a:Z

    if-eqz v1, :cond_3

    .line 180
    iget-object v1, p0, LSC;->a:LSD;

    invoke-interface {v1, p0}, LSD;->a(LSC;)V

    .line 181
    const/4 v1, 0x0

    iput-boolean v1, p0, LSC;->a:Z

    .line 182
    const/4 v1, 0x0

    iput v1, p0, LSC;->e:F

    .line 185
    :cond_3
    if-eqz v0, :cond_5

    .line 186
    const/4 v0, 0x1

    goto :goto_0

    .line 173
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 190
    :cond_5
    const/4 v0, 0x6

    if-eq v7, v0, :cond_6

    const/4 v0, 0x5

    if-ne v7, v0, :cond_7

    :cond_6
    const/4 v0, 0x1

    move v6, v0

    .line 193
    :goto_2
    const/4 v0, 0x6

    if-ne v7, v0, :cond_8

    const/4 v0, 0x1

    move v5, v0

    .line 194
    :goto_3
    if-eqz v5, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 197
    :goto_4
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    .line 199
    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v3

    move v3, v11

    :goto_5
    if-ge v3, v4, :cond_b

    .line 200
    if-ne v0, v3, :cond_a

    .line 199
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 190
    :cond_7
    const/4 v0, 0x0

    move v6, v0

    goto :goto_2

    .line 193
    :cond_8
    const/4 v0, 0x0

    move v5, v0

    goto :goto_3

    .line 194
    :cond_9
    const/4 v0, -0x1

    goto :goto_4

    .line 201
    :cond_a
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    add-float/2addr v2, v8

    .line 202
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    add-float/2addr v1, v8

    goto :goto_6

    .line 204
    :cond_b
    if-eqz v5, :cond_c

    add-int/lit8 v3, v4, -0x1

    .line 205
    :goto_7
    int-to-float v5, v3

    div-float v8, v2, v5

    .line 206
    int-to-float v2, v3

    div-float v9, v1, v2

    .line 209
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 210
    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v5

    move v5, v11

    :goto_8
    if-ge v5, v4, :cond_e

    .line 211
    if-ne v0, v5, :cond_d

    .line 210
    :goto_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_c
    move v3, v4

    .line 204
    goto :goto_7

    .line 212
    :cond_d
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    sub-float/2addr v10, v8

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v2, v10

    .line 213
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    sub-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v1, v10

    goto :goto_9

    .line 215
    :cond_e
    int-to-float v0, v3

    div-float v0, v2, v0

    .line 216
    int-to-float v2, v3

    div-float/2addr v1, v2

    .line 221
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    .line 222
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    .line 223
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    .line 228
    iget-boolean v3, p0, LSC;->a:Z

    .line 229
    iput v8, p0, LSC;->a:F

    .line 230
    iput v9, p0, LSC;->b:F

    .line 231
    iget-boolean v4, p0, LSC;->a:Z

    if-eqz v4, :cond_10

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_f

    if-eqz v6, :cond_10

    .line 232
    :cond_f
    iget-object v4, p0, LSC;->a:LSD;

    invoke-interface {v4, p0}, LSD;->a(LSC;)V

    .line 233
    const/4 v4, 0x0

    iput-boolean v4, p0, LSC;->a:Z

    .line 234
    iput v2, p0, LSC;->e:F

    .line 236
    :cond_10
    if-eqz v6, :cond_11

    .line 237
    iput v0, p0, LSC;->f:F

    iput v0, p0, LSC;->h:F

    .line 238
    iput v1, p0, LSC;->g:F

    iput v1, p0, LSC;->i:F

    .line 239
    iput v2, p0, LSC;->c:F

    iput v2, p0, LSC;->d:F

    iput v2, p0, LSC;->e:F

    .line 241
    :cond_11
    iget-boolean v4, p0, LSC;->a:Z

    if-nez v4, :cond_13

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_13

    if-nez v3, :cond_12

    iget v3, p0, LSC;->e:F

    sub-float v3, v2, v3

    .line 242
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, LSC;->a:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_13

    .line 243
    :cond_12
    iput v0, p0, LSC;->f:F

    iput v0, p0, LSC;->h:F

    .line 244
    iput v1, p0, LSC;->g:F

    iput v1, p0, LSC;->i:F

    .line 245
    iput v2, p0, LSC;->c:F

    iput v2, p0, LSC;->d:F

    .line 246
    iget-object v3, p0, LSC;->a:LSD;

    invoke-interface {v3, p0}, LSD;->b(LSC;)Z

    move-result v3

    iput-boolean v3, p0, LSC;->a:Z

    .line 250
    :cond_13
    const/4 v3, 0x2

    if-ne v7, v3, :cond_15

    .line 251
    iput v0, p0, LSC;->f:F

    .line 252
    iput v1, p0, LSC;->g:F

    .line 253
    iput v2, p0, LSC;->c:F

    .line 255
    const/4 v0, 0x1

    .line 256
    iget-boolean v1, p0, LSC;->a:Z

    if-eqz v1, :cond_14

    .line 257
    iget-object v0, p0, LSC;->a:LSD;

    invoke-interface {v0, p0}, LSD;->a(LSC;)Z

    move-result v0

    .line 260
    :cond_14
    if-eqz v0, :cond_15

    .line 261
    iget v0, p0, LSC;->f:F

    iput v0, p0, LSC;->h:F

    .line 262
    iget v0, p0, LSC;->g:F

    iput v0, p0, LSC;->i:F

    .line 263
    iget v0, p0, LSC;->c:F

    iput v0, p0, LSC;->d:F

    .line 267
    :cond_15
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 302
    iget v0, p0, LSC;->b:F

    return v0
.end method

.method public c()F
    .locals 2

    .prologue
    .line 373
    iget v0, p0, LSC;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, LSC;->c:F

    iget v1, p0, LSC;->d:F

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method
