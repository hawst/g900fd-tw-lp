.class public abstract LaGz;
.super LaGN;
.source "EntryAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaGu;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, LaGN;-><init>()V

    .line 15
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LaGz;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 16
    return-void
.end method


# virtual methods
.method public final a(LaGM;)LaGu;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, LaGz;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, LaGz;->a(LaGM;)LaGu;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method protected abstract a(LaGu;)V
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, LaGu;

    invoke-virtual {p0, p1}, LaGz;->b(LaGu;)V

    return-void
.end method

.method public final b(LaGu;)V
    .locals 0

    .prologue
    .line 25
    if-nez p1, :cond_0

    .line 26
    invoke-virtual {p0}, LaGz;->a()V

    .line 30
    :goto_0
    return-void

    .line 28
    :cond_0
    invoke-virtual {p0, p1}, LaGz;->a(LaGu;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 34
    const-string v0, "%s[%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "EntryAsyncTask"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaGz;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
