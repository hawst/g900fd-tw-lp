.class public final LauT;
.super Ljava/lang/Object;
.source "TokenSource.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;
    .locals 1

    .prologue
    .line 132
    new-array v0, p1, [Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, LauT;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, LauT;->a(I)[Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;

    move-result-object v0

    return-object v0
.end method
