.class public LQF;
.super Ljava/lang/Object;
.source "ClientFlagsModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 32
    const-class v0, LQE;

    const-class v1, LQJ;

    .line 33
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 32
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 34
    return-void
.end method

.method provideClientFlag(LQt;)LQr;
    .locals 4
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 68
    .line 69
    new-instance v0, LQx;

    invoke-direct {v0}, LQx;-><init>()V

    .line 71
    :try_start_0
    invoke-interface {p1, v0}, LQt;->b(LQr;)V
    :try_end_0
    .catch LQu; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v1

    .line 73
    const-string v2, "ClientFlagsModule"

    const-string v3, "Unable to load cached client flags, will use defaults until next sync."

    invoke-static {v2, v1, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method provideClientFlagDatabase(LQv;)LQt;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 61
    return-object p1
.end method

.method provideClientFlagJsonParser()LQy;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 39
    new-instance v0, LQA;

    invoke-direct {v0}, LQA;-><init>()V

    return-object v0
.end method

.method provideClientFlagSynchronizer(LQD;)LQB;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 45
    return-object p1
.end method

.method provideClientFlagUriResolver(LbiP;)LQE;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQJ;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LQE;",
            ">;)",
            "LQE;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQE;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LQq;

    invoke-direct {v0}, LQq;-><init>()V

    goto :goto_0
.end method
