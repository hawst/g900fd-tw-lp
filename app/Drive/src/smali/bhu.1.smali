.class public Lbhu;
.super Lbhm;
.source "EditorActionBarHoneycomb.java"


# instance fields
.field private a:Lbhv;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lbhm;-><init>(Landroid/app/Activity;)V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lbhu;->a:Lbhv;

    .line 84
    return-void
.end method


# virtual methods
.method public a(LbgY;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lbhu;->a:Lbhv;

    if-nez v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {p1}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhu;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    goto :goto_0
.end method

.method public a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lbhu;->a:Lbhv;

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lbhu;->a:Lbhv;

    invoke-virtual {v0, p1}, Lbhv;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lbhu;->a:Lbhv;

    if-nez v0, :cond_0

    .line 107
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lbhu;->a:Lbhv;

    invoke-virtual {v0, p1}, Lbhv;->a(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lbhv;

    invoke-direct {v0, p0, p1}, Lbhv;-><init>(Lbhu;Landroid/view/Menu;)V

    iput-object v0, p0, Lbhu;->a:Lbhv;

    .line 89
    invoke-super {p0, p1}, Lbhm;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lbhu;->a:Lbhv;

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lbhu;->a:Lbhv;

    invoke-virtual {v0, p1}, Lbhv;->b(Z)V

    goto :goto_0
.end method
