.class public LaPe;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:I

.field private final a:J

.field private a:LaQw;

.field private final a:Landroid/os/Handler;

.field protected final a:Lcom/google/android/gms/internal/fc;

.field protected a:Z

.field private final b:I

.field private b:J

.field protected b:Z


# direct methods
.method public constructor <init>(LaQw;Lcom/google/android/gms/internal/fc;II)V
    .locals 10

    const-wide/16 v6, 0xc8

    const-wide/16 v8, 0x32

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v9}, LaPe;-><init>(LaQw;Lcom/google/android/gms/internal/fc;IIJJ)V

    return-void
.end method

.method public constructor <init>(LaQw;Lcom/google/android/gms/internal/fc;IIJJ)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p5, p0, LaPe;->a:J

    iput-wide p7, p0, LaPe;->b:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LaPe;->a:Landroid/os/Handler;

    iput-object p2, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    iput-object p1, p0, LaPe;->a:LaQw;

    iput-boolean v2, p0, LaPe;->a:Z

    iput-boolean v2, p0, LaPe;->b:Z

    iput p4, p0, LaPe;->a:I

    iput p3, p0, LaPe;->b:I

    return-void
.end method

.method static synthetic a(LaPe;)I
    .locals 1

    iget v0, p0, LaPe;->b:I

    return v0
.end method

.method static synthetic a(LaPe;)J
    .locals 4

    iget-wide v0, p0, LaPe;->b:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, LaPe;->b:J

    return-wide v0
.end method

.method static synthetic a(LaPe;)LaQw;
    .locals 1

    iget-object v0, p0, LaPe;->a:LaQw;

    return-object v0
.end method

.method static synthetic a(LaPe;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, LaPe;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(LaPe;)I
    .locals 1

    iget v0, p0, LaPe;->a:I

    return v0
.end method

.method static synthetic b(LaPe;)J
    .locals 2

    iget-wide v0, p0, LaPe;->b:J

    return-wide v0
.end method

.method static synthetic c(LaPe;)J
    .locals 2

    iget-wide v0, p0, LaPe;->a:J

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, LaPe;->a:Landroid/os/Handler;

    iget-wide v2, p0, LaPe;->a:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/dz;)V
    .locals 3

    new-instance v0, LaQH;

    iget-object v1, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    iget-object v2, p1, Lcom/google/android/gms/internal/dz;->e:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, LaQH;-><init>(LaPe;Lcom/google/android/gms/internal/fc;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, LaPe;->a(Lcom/google/android/gms/internal/dz;LaQH;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/dz;LaQH;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/fc;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    iget-object v1, p1, Lcom/google/android/gms/internal/dz;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v5

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/internal/dz;->b:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/internal/dz;->a:Ljava/lang/String;

    invoke-static {v1}, LaPS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaPe;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaPe;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, LaPe;->b:Z

    return v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaPe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LaPe;->a:LaQw;

    iget-object v1, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    invoke-interface {v0, v1}, LaQw;->a(Lcom/google/android/gms/internal/fc;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, LaPf;

    iget-object v1, p0, LaPe;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {v0, p0, v1}, LaPf;-><init>(LaPe;Landroid/webkit/WebView;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaPf;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
