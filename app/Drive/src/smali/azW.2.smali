.class final enum LazW;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LazW;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LazW;

.field private static final synthetic a:[LazW;

.field public static final enum b:LazW;

.field public static final enum c:LazW;

.field public static final enum d:LazW;

.field public static final enum e:LazW;

.field public static final enum f:LazW;

.field public static final enum g:LazW;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, LazW;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->a:LazW;

    new-instance v0, LazW;

    const-string v1, "CONNECTED_SERVICE"

    invoke-direct {v0, v1, v4}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->b:LazW;

    new-instance v0, LazW;

    const-string v1, "CONNECTED_LOCAL"

    invoke-direct {v0, v1, v5}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->c:LazW;

    new-instance v0, LazW;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v6}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->d:LazW;

    new-instance v0, LazW;

    const-string v1, "PENDING_CONNECTION"

    invoke-direct {v0, v1, v7}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->e:LazW;

    new-instance v0, LazW;

    const-string v1, "PENDING_DISCONNECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->f:LazW;

    new-instance v0, LazW;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LazW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LazW;->g:LazW;

    const/4 v0, 0x7

    new-array v0, v0, [LazW;

    sget-object v1, LazW;->a:LazW;

    aput-object v1, v0, v3

    sget-object v1, LazW;->b:LazW;

    aput-object v1, v0, v4

    sget-object v1, LazW;->c:LazW;

    aput-object v1, v0, v5

    sget-object v1, LazW;->d:LazW;

    aput-object v1, v0, v6

    sget-object v1, LazW;->e:LazW;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LazW;->f:LazW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LazW;->g:LazW;

    aput-object v2, v0, v1

    sput-object v0, LazW;->a:[LazW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LazW;
    .locals 1

    const-class v0, LazW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LazW;

    return-object v0
.end method

.method public static values()[LazW;
    .locals 1

    sget-object v0, LazW;->a:[LazW;

    invoke-virtual {v0}, [LazW;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LazW;

    return-object v0
.end method
