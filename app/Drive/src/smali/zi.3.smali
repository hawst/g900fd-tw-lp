.class public Lzi;
.super Ljava/lang/Object;
.source "LinkSharingCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Lzh;)V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0, p1}, Lzi;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaKR;

    move-result-object v0

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 237
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_offline:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 240
    :cond_2
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;

    move-result-object v0

    invoke-interface {v0}, Lacr;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 241
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_cannot_change:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 245
    :cond_3
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    iget-object v1, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Lacr;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 246
    new-instance v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;-><init>()V

    .line 248
    iget-object v1, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaGu;

    move-result-object v1

    iget-object v2, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;

    move-result-object v2

    new-instance v3, Lzj;

    invoke-direct {v3, p0}, Lzj;-><init>(Lzi;)V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;LaGu;Lacr;Ljava/lang/Runnable;)V

    .line 254
    iget-object v1, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LM;

    move-result-object v1

    iget-object v2, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a(LM;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_4
    new-instance v1, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;-><init>()V

    .line 257
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;

    move-result-object v0

    invoke-interface {v0}, Lacr;->a()Lqu;

    move-result-object v0

    invoke-virtual {v0}, Lqu;->a()Lqt;

    move-result-object v2

    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    .line 258
    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LbmF;

    move-result-object v0

    iget-object v4, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v4}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LbmF;

    move-result-object v4

    invoke-virtual {v4}, LbmF;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 257
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;Lqt;Ljava/lang/CharSequence;[Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lzi;->a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

    invoke-static {v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LM;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a(LM;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
