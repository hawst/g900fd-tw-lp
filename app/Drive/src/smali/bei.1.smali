.class public final Lbei;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# instance fields
.field private a:I

.field private a:LbdT;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private a:LbdY;

.field private a:LbdZ;

.field private a:Lbea;

.field private a:Lbec;

.field private a:Lbed;

.field private a:Lbeg;

.field private a:Lbeo;

.field private final a:Lbeq;

.field private a:Lber;

.field private a:LbfL;

.field private a:LbfP;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:I

.field private b:Lbed;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:I

.field private d:Z

.field private e:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private f:Z


# direct methods
.method constructor <init>(Lbeq;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x4e20

    const/4 v1, 0x1

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lbed;

    invoke-direct {v0}, Lbed;-><init>()V

    iput-object v0, p0, Lbei;->a:Lbed;

    .line 93
    new-instance v0, Lbed;

    invoke-direct {v0}, Lbed;-><init>()V

    iput-object v0, p0, Lbei;->b:Lbed;

    .line 101
    const/16 v0, 0xa

    iput v0, p0, Lbei;->a:I

    .line 123
    const/16 v0, 0x4000

    iput v0, p0, Lbei;->b:I

    .line 126
    iput-boolean v1, p0, Lbei;->a:Z

    .line 129
    iput-boolean v1, p0, Lbei;->b:Z

    .line 144
    iput v2, p0, Lbei;->c:I

    .line 150
    iput v2, p0, Lbei;->d:I

    .line 176
    iput-boolean v1, p0, Lbei;->c:Z

    .line 182
    iput-boolean v1, p0, Lbei;->d:Z

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbei;->e:Z

    .line 202
    sget-object v0, LbfP;->a:LbfP;

    iput-object v0, p0, Lbei;->a:LbfP;

    .line 209
    iput-object p1, p0, Lbei;->a:Lbeq;

    .line 210
    invoke-virtual {p0, p2}, Lbei;->a(Ljava/lang/String;)Lbei;

    .line 211
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lbei;->b:I

    return v0
.end method

.method public a()LbdT;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lbei;->a:LbdT;

    return-object v0
.end method

.method public a()LbdY;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lbei;->a:LbdY;

    return-object v0
.end method

.method public a()LbdZ;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lbei;->a:LbdZ;

    return-object v0
.end method

.method public a()Lbec;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lbei;->a:Lbec;

    return-object v0
.end method

.method public a()Lbed;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lbei;->a:Lbed;

    return-object v0
.end method

.method public a()Lbeg;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lbei;->a:Lbeg;

    return-object v0
.end method

.method public a(I)Lbei;
    .locals 1

    .prologue
    .line 456
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 457
    iput p1, p0, Lbei;->c:I

    .line 458
    return-object p0

    .line 456
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LbdY;)Lbei;
    .locals 1

    .prologue
    .line 257
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbdY;

    iput-object v0, p0, Lbei;->a:LbdY;

    .line 258
    return-object p0
.end method

.method public a(LbdZ;)Lbei;
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lbei;->a:LbdZ;

    .line 277
    return-object p0
.end method

.method public a(Lbea;)Lbei;
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lbei;->a:Lbea;

    .line 296
    return-object p0
.end method

.method public a(Lbec;)Lbei;
    .locals 0

    .prologue
    .line 564
    iput-object p1, p0, Lbei;->a:Lbec;

    .line 565
    return-object p0
.end method

.method public a(Lbeg;)Lbei;
    .locals 0

    .prologue
    .line 607
    iput-object p1, p0, Lbei;->a:Lbeg;

    .line 608
    return-object p0
.end method

.method public a(Lbeo;)Lbei;
    .locals 0

    .prologue
    .line 626
    iput-object p1, p0, Lbei;->a:Lbeo;

    .line 627
    return-object p0
.end method

.method public a(Lber;)Lbei;
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lbei;->a:Lber;

    .line 585
    return-object p0
.end method

.method public a(LbfL;)Lbei;
    .locals 0

    .prologue
    .line 672
    iput-object p1, p0, Lbei;->a:LbfL;

    .line 673
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbei;
    .locals 1

    .prologue
    .line 237
    if-eqz p1, :cond_0

    invoke-static {p1}, Lbeh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 238
    iput-object p1, p0, Lbei;->a:Ljava/lang/String;

    .line 239
    return-object p0

    .line 237
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Lbei;
    .locals 0

    .prologue
    .line 408
    iput-boolean p1, p0, Lbei;->a:Z

    .line 409
    return-object p0
.end method

.method public a()Lbel;
    .locals 22

    .prologue
    .line 835
    .line 836
    move-object/from16 v0, p0

    iget v2, v0, Lbei;->a:I

    if-ltz v2, :cond_11

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LbfN;->a(Z)V

    .line 837
    move-object/from16 v0, p0

    iget v2, v0, Lbei;->a:I

    .line 838
    move-object/from16 v0, p0

    iget-object v3, v0, Lbei;->a:LbdT;

    if-eqz v3, :cond_0

    .line 840
    move-object/from16 v0, p0

    iget-object v3, v0, Lbei;->a:LbdT;

    invoke-interface {v3}, LbdT;->a()V

    .line 842
    :cond_0
    const/4 v4, 0x0

    .line 845
    move-object/from16 v0, p0

    iget-object v3, v0, Lbei;->a:Ljava/lang/String;

    invoke-static {v3}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    move-object/from16 v0, p0

    iget-object v3, v0, Lbei;->a:LbdY;

    invoke-static {v3}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move v8, v2

    .line 850
    :goto_1
    if-eqz v4, :cond_1

    .line 851
    invoke-virtual {v4}, Lbel;->a()V

    .line 854
    :cond_1
    const/4 v7, 0x0

    .line 855
    const/4 v6, 0x0

    .line 858
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:Lbec;

    if-eqz v2, :cond_2

    .line 859
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:Lbec;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lbec;->b(Lbei;)V

    .line 862
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:LbdY;

    invoke-virtual {v2}, LbdY;->a()Ljava/lang/String;

    move-result-object v14

    .line 863
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:Lbeq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbei;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v14}, Lbeq;->a(Ljava/lang/String;Ljava/lang/String;)Lbet;

    move-result-object v15

    .line 864
    sget-object v16, Lbeq;->a:Ljava/util/logging/Logger;

    .line 865
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbei;->a:Z

    if-eqz v2, :cond_12

    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    move v13, v2

    .line 866
    :goto_2
    const/4 v3, 0x0

    .line 867
    const/4 v2, 0x0

    .line 869
    if-eqz v13, :cond_3

    .line 870
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 871
    const-string v4, "-------------- REQUEST  --------------"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LbfS;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    move-object/from16 v0, p0

    iget-object v4, v0, Lbei;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x20

    .line 873
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LbfS;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 876
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lbei;->b:Z

    if-eqz v4, :cond_3

    .line 877
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "curl -v --compressed"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 878
    move-object/from16 v0, p0

    iget-object v4, v0, Lbei;->a:Ljava/lang/String;

    const-string v5, "GET"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 879
    const-string v4, " -X "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 884
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lbei;->a:Lbed;

    invoke-virtual {v4}, Lbed;->e()Ljava/lang/String;

    move-result-object v4

    .line 885
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lbei;->f:Z

    if-nez v5, :cond_4

    .line 886
    if-nez v4, :cond_13

    .line 887
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbed;

    const-string v9, "Google-HTTP-Java-Client/1.19.0-rc-SNAPSHOT (gzip)"

    invoke-virtual {v5, v9}, Lbed;->l(Ljava/lang/String;)Lbed;

    .line 893
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbed;

    move-object/from16 v0, v16

    invoke-static {v5, v3, v2, v0, v15}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lbet;)V

    .line 894
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lbei;->f:Z

    if-nez v5, :cond_5

    .line 896
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbed;

    invoke-virtual {v5, v4}, Lbed;->l(Ljava/lang/String;)Lbed;

    .line 900
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:LbdZ;

    .line 901
    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lbei;->a:LbdZ;

    invoke-interface {v4}, LbdZ;->a()Z

    move-result v4

    if-eqz v4, :cond_14

    :cond_6
    const/4 v4, 0x1

    move v12, v4

    .line 902
    :goto_4
    if-eqz v5, :cond_a

    .line 905
    move-object/from16 v0, p0

    iget-object v4, v0, Lbei;->a:LbdZ;

    invoke-interface {v4}, LbdZ;->a()Ljava/lang/String;

    move-result-object v17

    .line 907
    if-eqz v13, :cond_23

    .line 908
    new-instance v4, LbfI;

    sget-object v9, Lbeq;->a:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    move-object/from16 v0, p0

    iget v11, v0, Lbei;->b:I

    invoke-direct {v4, v5, v9, v10, v11}, LbfI;-><init>(LbfR;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    .line 912
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbea;

    if-nez v5, :cond_15

    .line 913
    const/4 v5, 0x0

    .line 914
    move-object/from16 v0, p0

    iget-object v9, v0, Lbei;->a:LbdZ;

    invoke-interface {v9}, LbdZ;->a()J

    move-result-wide v10

    move-object/from16 v20, v5

    move-object v5, v4

    move-object/from16 v4, v20

    .line 921
    :goto_6
    if-eqz v13, :cond_8

    .line 922
    if-eqz v17, :cond_7

    .line 923
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Content-Type: "

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 924
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, LbfS;->a:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    if-eqz v2, :cond_7

    .line 926
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " -H \'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v18, "\'"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 929
    :cond_7
    const-wide/16 v18, 0x0

    cmp-long v9, v10, v18

    if-ltz v9, :cond_8

    .line 930
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Content-Length: "

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 931
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v18, LbfS;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 935
    :cond_8
    if-eqz v2, :cond_9

    .line 936
    const-string v9, " -d \'@-\'"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lbet;->b(Ljava/lang/String;)V

    .line 940
    invoke-virtual {v15, v4}, Lbet;->a(Ljava/lang/String;)V

    .line 941
    invoke-virtual {v15, v10, v11}, Lbet;->a(J)V

    .line 942
    invoke-virtual {v15, v5}, Lbet;->a(LbfR;)V

    .line 945
    :cond_a
    if-eqz v13, :cond_c

    .line 946
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 947
    if-eqz v2, :cond_c

    .line 948
    const-string v3, " -- \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 949
    const-string v3, "\'"

    const-string v4, "\'\"\'\"\'"

    invoke-virtual {v14, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 950
    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 951
    if-eqz v5, :cond_b

    .line 952
    const-string v3, " << $$$"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 954
    :cond_b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 960
    :cond_c
    if-eqz v12, :cond_17

    if-lez v8, :cond_17

    const/4 v2, 0x1

    .line 963
    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lbei;->c:I

    move-object/from16 v0, p0

    iget v4, v0, Lbei;->d:I

    invoke-virtual {v15, v3, v4}, Lbet;->a(II)V

    .line 965
    :try_start_0
    invoke-virtual {v15}, Lbet;->a()Lbeu;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 969
    :try_start_1
    new-instance v3, Lbel;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lbel;-><init>(Lbei;Lbeu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v3

    move-object v3, v6

    .line 993
    :goto_8
    if-eqz v4, :cond_1c

    :try_start_2
    invoke-virtual {v4}, Lbel;->a()Z

    move-result v5

    if-nez v5, :cond_1c

    .line 994
    const/4 v5, 0x0

    .line 995
    move-object/from16 v0, p0

    iget-object v6, v0, Lbei;->a:Lber;

    if-eqz v6, :cond_d

    .line 999
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lber;

    move-object/from16 v0, p0

    invoke-interface {v5, v0, v4, v2}, Lber;->a(Lbei;Lbel;Z)Z

    move-result v5

    .line 1001
    :cond_d
    if-nez v5, :cond_e

    .line 1002
    invoke-virtual {v4}, Lbel;->a()I

    move-result v6

    invoke-virtual {v4}, Lbel;->a()Lbed;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lbei;->a(ILbed;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1004
    const/4 v5, 0x1

    .line 1022
    :cond_e
    :goto_9
    and-int/2addr v2, v5

    .line 1024
    if-eqz v2, :cond_f

    .line 1025
    invoke-virtual {v4}, Lbel;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1033
    :cond_f
    :goto_a
    add-int/lit8 v5, v8, -0x1

    .line 1037
    if-eqz v4, :cond_10

    .line 1041
    :cond_10
    if-nez v2, :cond_22

    .line 1043
    if-nez v4, :cond_1f

    .line 1045
    throw v3

    .line 836
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 865
    :cond_12
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_2

    .line 889
    :cond_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbed;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Google-HTTP-Java-Client/1.19.0-rc-SNAPSHOT (gzip)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lbed;->l(Ljava/lang/String;)Lbed;

    goto/16 :goto_3

    .line 901
    :cond_14
    const/4 v4, 0x0

    move v12, v4

    goto/16 :goto_4

    .line 916
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbea;

    invoke-interface {v5}, Lbea;->a()Ljava/lang/String;

    move-result-object v9

    .line 917
    new-instance v10, Lbeb;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbea;

    invoke-direct {v10, v4, v5}, Lbeb;-><init>(LbfR;Lbea;)V

    .line 918
    if-eqz v12, :cond_16

    invoke-static {v10}, LbfD;->a(LbfR;)J

    move-result-wide v4

    :goto_b
    move-wide/from16 v20, v4

    move-object v4, v9

    move-object v5, v10

    move-wide/from16 v10, v20

    goto/16 :goto_6

    :cond_16
    const-wide/16 v4, -0x1

    goto :goto_b

    .line 960
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 972
    :catchall_0
    move-exception v3

    .line 973
    :try_start_3
    invoke-virtual {v4}, Lbeu;->a()Ljava/io/InputStream;

    move-result-object v4

    .line 974
    if-eqz v4, :cond_18

    .line 975
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 977
    :cond_18
    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 979
    :catch_0
    move-exception v3

    move-object v4, v7

    .line 980
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lbei;->e:Z

    if-nez v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbeg;

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:Lbeg;

    .line 981
    move-object/from16 v0, p0

    invoke-interface {v5, v0, v2}, Lbeg;->a(Lbei;Z)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 982
    :cond_19
    throw v3

    .line 986
    :cond_1a
    sget-object v5, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v6, "exception thrown while executing request"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 1005
    :cond_1b
    if-eqz v2, :cond_e

    :try_start_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lbei;->a:LbdT;

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    iget-object v6, v0, Lbei;->a:LbdT;

    .line 1006
    invoke-virtual {v4}, Lbel;->a()I

    move-result v7

    invoke-interface {v6, v7}, LbdT;->a(I)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1009
    move-object/from16 v0, p0

    iget-object v6, v0, Lbei;->a:LbdT;

    invoke-interface {v6}, LbdT;->a()J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-wide v6

    .line 1010
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-eqz v9, :cond_e

    .line 1012
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lbei;->a:LbfP;

    invoke-interface {v5, v6, v7}, LbfP;->a(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1016
    :goto_c
    const/4 v5, 0x1

    goto/16 :goto_9

    .line 1029
    :cond_1c
    if-nez v4, :cond_1d

    const/4 v5, 0x1

    :goto_d
    and-int/2addr v2, v5

    goto/16 :goto_a

    :cond_1d
    const/4 v5, 0x0

    goto :goto_d

    .line 1037
    :catchall_1
    move-exception v2

    if-eqz v4, :cond_1e

    .line 1038
    invoke-virtual {v4}, Lbel;->b()V

    :cond_1e
    throw v2

    .line 1048
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:Lbeo;

    if-eqz v2, :cond_20

    .line 1049
    move-object/from16 v0, p0

    iget-object v2, v0, Lbei;->a:Lbeo;

    invoke-interface {v2, v4}, Lbeo;->a(Lbel;)V

    .line 1052
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbei;->d:Z

    if-eqz v2, :cond_21

    invoke-virtual {v4}, Lbel;->a()Z

    move-result v2

    if-nez v2, :cond_21

    .line 1054
    :try_start_6
    new-instance v2, Lbem;

    invoke-direct {v2, v4}, Lbem;-><init>(Lbel;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1056
    :catchall_2
    move-exception v2

    invoke-virtual {v4}, Lbel;->b()V

    throw v2

    .line 1059
    :cond_21
    return-object v4

    .line 1013
    :catch_1
    move-exception v5

    goto :goto_c

    :cond_22
    move v8, v5

    goto/16 :goto_1

    :cond_23
    move-object v4, v5

    goto/16 :goto_5
.end method

.method public a()Lbeo;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lbei;->a:Lbeo;

    return-object v0
.end method

.method public a()Lbeq;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lbei;->a:Lbeq;

    return-object v0
.end method

.method public a()Lber;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lbei;->a:Lber;

    return-object v0
.end method

.method public final a()LbfL;
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lbei;->a:LbfL;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lbei;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lbei;->a:Z

    return v0
.end method

.method public a(ILbed;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1115
    invoke-virtual {p2}, Lbed;->c()Ljava/lang/String;

    move-result-object v0

    .line 1116
    invoke-virtual {p0}, Lbei;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Lbep;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1119
    new-instance v2, LbdY;

    iget-object v3, p0, Lbei;->a:LbdY;

    invoke-virtual {v3, v0}, LbdY;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    invoke-direct {v2, v0}, LbdY;-><init>(Ljava/net/URL;)V

    invoke-virtual {p0, v2}, Lbei;->a(LbdY;)Lbei;

    .line 1121
    const/16 v0, 0x12f

    if-ne p1, v0, :cond_0

    .line 1122
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Lbei;->a(Ljava/lang/String;)Lbei;

    .line 1124
    invoke-virtual {p0, v1}, Lbei;->a(LbdZ;)Lbei;

    .line 1127
    :cond_0
    iget-object v2, p0, Lbei;->a:Lbed;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbed;->b(Ljava/lang/String;)Lbed;

    .line 1128
    iget-object v2, p0, Lbei;->a:Lbed;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbed;->g(Ljava/lang/String;)Lbed;

    .line 1129
    iget-object v2, p0, Lbei;->a:Lbed;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbed;->h(Ljava/lang/String;)Lbed;

    .line 1130
    iget-object v2, p0, Lbei;->a:Lbed;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbed;->f(Ljava/lang/String;)Lbed;

    .line 1131
    iget-object v2, p0, Lbei;->a:Lbed;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbed;->i(Ljava/lang/String;)Lbed;

    .line 1132
    iget-object v0, p0, Lbei;->a:Lbed;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbed;->j(Ljava/lang/String;)Lbed;

    .line 1133
    const/4 v0, 0x1

    .line 1135
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lbei;->a:I

    return v0
.end method

.method public b()Lbed;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lbei;->b:Lbed;

    return-object v0
.end method

.method public b(I)Lbei;
    .locals 1

    .prologue
    .line 482
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 483
    iput p1, p0, Lbei;->d:I

    .line 484
    return-object p0

    .line 482
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)Lbei;
    .locals 0

    .prologue
    .line 729
    iput-boolean p1, p0, Lbei;->d:Z

    .line 730
    return-object p0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 691
    iget-boolean v0, p0, Lbei;->c:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 715
    iget-boolean v0, p0, Lbei;->d:Z

    return v0
.end method
