.class public final enum LbvL;
.super Ljava/lang/Enum;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbvL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbvL;

.field private static final synthetic a:[LbvL;

.field public static final enum b:LbvL;

.field public static final enum c:LbvL;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, LbvL;

    const-string v1, "MAPBINDER"

    invoke-direct {v0, v1, v2}, LbvL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvL;->a:LbvL;

    .line 38
    new-instance v0, LbvL;

    const-string v1, "MULTIBINDER"

    invoke-direct {v0, v1, v3}, LbvL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvL;->b:LbvL;

    .line 39
    new-instance v0, LbvL;

    const-string v1, "OPTIONALBINDER"

    invoke-direct {v0, v1, v4}, LbvL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvL;->c:LbvL;

    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [LbvL;

    sget-object v1, LbvL;->a:LbvL;

    aput-object v1, v0, v2

    sget-object v1, LbvL;->b:LbvL;

    aput-object v1, v0, v3

    sget-object v1, LbvL;->c:LbvL;

    aput-object v1, v0, v4

    sput-object v0, LbvL;->a:[LbvL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbvL;
    .locals 1

    .prologue
    .line 36
    const-class v0, LbvL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbvL;

    return-object v0
.end method

.method public static values()[LbvL;
    .locals 1

    .prologue
    .line 36
    sget-object v0, LbvL;->a:[LbvL;

    invoke-virtual {v0}, [LbvL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbvL;

    return-object v0
.end method
