.class public LPl;
.super Ljava/lang/Object;
.source "DetailDrawerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/view/View;

    move-result-object v0

    .line 215
    new-instance v1, LgA;

    iget-object v2, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    .line 216
    invoke-static {v2}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, LgA;-><init>(II)V

    .line 217
    invoke-static {}, LakQ;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    const v2, 0x800005

    iput v2, v1, LgA;->a:I

    .line 222
    :goto_0
    iget-object v2, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v2}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iget-object v1, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 226
    iget-object v1, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/view/View;

    move-result-object v1

    new-instance v2, LPm;

    invoke-direct {v2, p0, v0}, LPm;-><init>(LPl;Landroid/view/View;)V

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 238
    iget-object v0, p0, LPl;->a:Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/view/View;

    move-result-object v0

    new-instance v1, LPn;

    invoke-direct {v1, p0}, LPn;-><init>(LPl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 247
    return-void

    .line 220
    :cond_0
    const/4 v2, 0x5

    iput v2, v1, LgA;->a:I

    goto :goto_0
.end method
