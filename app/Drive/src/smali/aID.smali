.class public LaID;
.super Ljava/lang/Object;
.source "GuidCrypter.java"


# instance fields
.field private final a:LaIE;


# direct methods
.method public constructor <init>(LaIE;)V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIE;

    iput-object v0, p0, LaID;->a:LaIE;

    .line 193
    return-void
.end method

.method static a([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    const/16 v0, 0x8

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a([B[B)Z
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 267
    array-length v0, p2

    if-ne v0, v6, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    move v0, v2

    move v3, v2

    .line 269
    :goto_1
    if-ge v0, v6, :cond_1

    .line 270
    aget-byte v4, p1, v0

    aget-byte v5, p2, v0

    xor-int/2addr v4, v5

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 267
    goto :goto_0

    .line 272
    :cond_1
    if-nez v3, :cond_2

    :goto_2
    return v1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method static a(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 202
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Landroid/util/Base64;->decode([BI)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 203
    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 250
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 254
    invoke-virtual {p0, v1}, LaID;->a(I)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 255
    invoke-virtual {v3, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 256
    array-length v0, v3

    invoke-virtual {p0, v3, v2, v0}, LaID;->a([BII)[B

    move-result-object v4

    .line 257
    array-length v0, v4

    const/16 v5, 0x14

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 258
    const/4 v0, 0x2

    new-array v0, v0, [[B

    aput-object v4, v0, v2

    aput-object v3, v0, v1

    invoke-static {v0}, Lbsx;->a([[B)[B

    move-result-object v0

    invoke-static {v0}, LaID;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    move v0, v2

    .line 257
    goto :goto_0
.end method

.method a(I)Ljavax/crypto/Cipher;
    .locals 4

    .prologue
    .line 232
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v1, p0, LaID;->a:LaIE;

    invoke-virtual {v1}, LaIE;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 233
    iget-object v1, p0, LaID;->a:LaIE;

    invoke-virtual {v1}, LaIE;->a()Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 234
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, LbiT;->b(Z)V

    .line 235
    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 236
    invoke-virtual {v2, p1, v1, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 237
    return-object v2
.end method

.method a([BII)[B
    .locals 4

    .prologue
    .line 217
    :try_start_0
    const-string v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 218
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v2, p0, LaID;->a:LaIE;

    invoke-virtual {v2}, LaIE;->a()[B

    move-result-object v2

    const-string v3, "HmacSHA1"

    invoke-direct {v1, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 219
    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 220
    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Mac;->update([BII)V

    .line 221
    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 224
    :catch_1
    move-exception v0

    .line 225
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x14

    .line 281
    invoke-static {p1}, LaID;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 283
    array-length v1, v0

    add-int/lit8 v1, v1, -0x14

    invoke-virtual {p0, v0, v3, v1}, LaID;->a([BII)[B

    move-result-object v1

    .line 284
    invoke-direct {p0, v0, v1}, LaID;->a([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 285
    new-instance v0, Ljava/security/GeneralSecurityException;

    const-string v1, "Invalid string"

    invoke-direct {v0, v1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LaID;->a(I)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 289
    array-length v2, v0

    add-int/lit8 v2, v2, -0x14

    invoke-virtual {v1, v0, v3, v2}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v0

    .line 292
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 293
    :catch_0
    move-exception v0

    .line 294
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method
