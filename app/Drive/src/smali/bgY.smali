.class public LbgY;
.super Ljava/lang/Object;
.source "DocumentPage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field private a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field private a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

.field private a:Ljava/io/File;

.field private b:Ljava/io/File;

.field private c:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, LbgY;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/16 v0, 0x5a

    iput v0, p0, LbgY;->a:I

    .line 39
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 196
    iget v0, p0, LbgY;->a:F

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, LbgY;->a:I

    return v0
.end method

.method public a()J
    .locals 4

    .prologue
    .line 179
    const-wide/16 v0, 0x0

    .line 180
    iget-object v2, p0, LbgY;->a:Ljava/io/File;

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, p0, LbgY;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 183
    :cond_0
    iget-object v2, p0, LbgY;->b:Ljava/io/File;

    if-eqz v2, :cond_1

    .line 184
    iget-object v2, p0, LbgY;->b:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 186
    :cond_1
    iget-object v2, p0, LbgY;->c:Ljava/io/File;

    if-eqz v2, :cond_2

    .line 187
    iget-object v2, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 189
    :cond_2
    return-wide v0
.end method

.method public a(I)LbgY;
    .locals 0

    .prologue
    .line 164
    iput p1, p0, LbgY;->a:I

    .line 165
    return-object p0
.end method

.method public a(J)LbgY;
    .locals 1

    .prologue
    .line 236
    iput-wide p1, p0, LbgY;->a:J

    .line 237
    return-object p0
.end method

.method public a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 216
    return-object p0
.end method

.method public a(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)LbgY;
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 126
    return-object p0
.end method

.method public a(Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/io/File;)LbgY;
    .locals 5

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, LbgY;->a:F

    .line 53
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, LbgY;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Rectified low-res paths: %s\n%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 55
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 61
    :cond_0
    iput-object p2, p0, LbgY;->c:Ljava/io/File;

    .line 62
    return-object p0
.end method

.method public a(Ljava/io/File;)LbgY;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 75
    :cond_0
    iput-object p1, p0, LbgY;->a:Ljava/io/File;

    .line 76
    return-object p0
.end method

.method public a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    return-object v0
.end method

.method public a()Lcom/google/bionics/scanner/rectifier/Quadrilateral;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    return-object v0
.end method

.method public a()Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->loadBitmap(Ljava/io/File;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 254
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 256
    iput-object v1, p0, LbgY;->a:Ljava/io/File;

    .line 258
    :cond_0
    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 260
    iput-object v1, p0, LbgY;->b:Ljava/io/File;

    .line 262
    :cond_1
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 264
    iput-object v1, p0, LbgY;->c:Ljava/io/File;

    .line 266
    :cond_2
    iput-object v1, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 267
    const/16 v0, 0x5a

    iput v0, p0, LbgY;->a:I

    .line 268
    return-void
.end method

.method public b()F
    .locals 3

    .prologue
    .line 203
    invoke-virtual {p0}, LbgY;->a()F

    move-result v0

    .line 204
    iget v1, p0, LbgY;->a:I

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    iget v1, p0, LbgY;->a:I

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_1

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    div-float v0, v1, v0

    :cond_1
    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 247
    iget-wide v0, p0, LbgY;->a:J

    return-wide v0
.end method

.method public b(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)LbgY;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {v0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;-><init>(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    iput-object v0, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 138
    return-object p0
.end method

.method public b(Ljava/io/File;)LbgY;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    .line 101
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 104
    :cond_0
    iput-object p1, p0, LbgY;->b:Ljava/io/File;

    .line 105
    return-object p0
.end method

.method public b()Ljava/io/File;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LbgY;->c:Ljava/io/File;

    return-object v0
.end method

.method public c()Ljava/io/File;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, LbgY;->b:Ljava/io/File;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 290
    if-ne p0, p1, :cond_1

    .line 303
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    instance-of v2, p1, LbgY;

    if-nez v2, :cond_2

    move v0, v1

    .line 296
    goto :goto_0

    .line 299
    :cond_2
    check-cast p1, LbgY;

    .line 300
    iget-object v2, p0, LbgY;->a:Ljava/io/File;

    iget-object v3, p1, LbgY;->a:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LbgY;->b:Ljava/io/File;

    iget-object v3, p1, LbgY;->b:Ljava/io/File;

    .line 301
    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LbgY;->c:Ljava/io/File;

    iget-object v3, p1, LbgY;->c:Ljava/io/File;

    .line 302
    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    iget-object v3, p1, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 303
    invoke-virtual {v2, v3}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, LbgY;->a:J

    iget-wide v4, p1, LbgY;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, LbgY;->a:I

    iget v3, p1, LbgY;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    iget-object v3, p1, LbgY;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    if-ne v2, v3, :cond_3

    iget v2, p0, LbgY;->a:F

    iget v3, p1, LbgY;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 273
    .line 276
    iget-object v0, p0, LbgY;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 277
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LbgY;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 278
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LbgY;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LbgY;->a:J

    iget-wide v4, p0, LbgY;->a:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 281
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LbgY;->a:I

    add-int/2addr v0, v1

    .line 282
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LbgY;->a:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LbgY;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    return v0
.end method
