.class LWi;
.super LEC;
.source "CommonPreferencesInstaller.java"


# instance fields
.field final synthetic a:LWa;


# direct methods
.method constructor <init>(LWa;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, LWi;->a:LWa;

    invoke-direct {p0}, LEC;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 295
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, LWi;->a:LWa;

    iget-object v1, v1, LWa;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 296
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxh;->licenses:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 298
    invoke-static {v1, v0}, LamN;->a(Ljava/io/InputStream;Landroid/webkit/WebView;)V

    .line 300
    iget-object v1, p0, LWi;->a:LWa;

    iget-object v1, v1, LWa;->a:Landroid/app/Activity;

    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v1

    sget v2, Lxi;->prefs_legal:I

    .line 301
    invoke-virtual {v1, v2}, LEU;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    .line 302
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
