.class public LamF;
.super Ljava/lang/Object;
.source "RelativeDateFormatter.java"


# instance fields
.field private final a:J

.field private final a:Landroid/content/Context;

.field private final a:Landroid/text/format/Time;

.field private final a:Ljava/lang/String;

.field private final b:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/text/format/Time;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, LamF;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, LamF;->a:Landroid/text/format/Time;

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, LamF;->a:J

    .line 38
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, LamF;->b:Landroid/text/format/Time;

    .line 39
    sget v0, Lxi;->doclist_date_never_label:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LamF;->a:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public a(J)Ljava/lang/String;
    .locals 3

    .prologue
    const v0, 0x10a01

    .line 53
    iget-object v1, p0, LamF;->b:Landroid/text/format/Time;

    invoke-virtual {v1, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 56
    iget-object v1, p0, LamF;->b:Landroid/text/format/Time;

    invoke-static {v1}, Landroid/text/format/Time;->isEpoch(Landroid/text/format/Time;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget-object v0, p0, LamF;->a:Ljava/lang/String;

    .line 77
    :goto_0
    return-object v0

    .line 63
    :cond_0
    invoke-virtual {p0, p1, p2}, LamF;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    :cond_1
    :goto_1
    iget-object v1, p0, LamF;->a:Landroid/content/Context;

    invoke-static {v1, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_2
    iget-object v1, p0, LamF;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, LamF;->a:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-eq v1, v2, :cond_3

    .line 68
    const v0, 0x10a14

    goto :goto_1

    .line 69
    :cond_3
    iget-object v1, p0, LamF;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->yearDay:I

    iget-object v2, p0, LamF;->a:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    if-eq v1, v2, :cond_1

    .line 71
    const v0, 0x10a18

    goto :goto_1
.end method

.method a(J)Z
    .locals 5

    .prologue
    .line 44
    iget-wide v0, p0, LamF;->a:J

    const-wide/32 v2, 0x1499700

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
