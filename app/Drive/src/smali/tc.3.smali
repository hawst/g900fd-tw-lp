.class public Ltc;
.super LaGN;
.source "DocumentPreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "LaGA;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Landroid/os/Bundle;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    iput-object p2, p0, Ltc;->a:Landroid/content/Intent;

    iput-object p3, p0, Ltc;->a:Landroid/os/Bundle;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)LaGA;
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Ltc;->a:Landroid/content/Intent;

    const-string v1, "docListQuery"

    .line 314
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/DocListQuery;

    .line 315
    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    iget-object v1, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGA;

    move-result-object v0

    .line 320
    :goto_0
    iget-object v1, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltk;

    invoke-virtual {v1, v0}, Ltk;->a(LaGA;)V

    .line 321
    return-object v0

    .line 318
    :cond_0
    iget-object v1, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGM;Lcom/google/android/apps/docs/doclist/DocListQuery;)LaGA;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Ltc;->a(LaGM;)LaGA;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGA;)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Ltc;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    iget-object v1, p0, Ltc;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGA;Landroid/os/Bundle;)V

    .line 327
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 308
    check-cast p1, LaGA;

    invoke-virtual {p0, p1}, Ltc;->a(LaGA;)V

    return-void
.end method
