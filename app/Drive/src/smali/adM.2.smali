.class public final LadM;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LadF;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lady;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladg;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laec;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LadS;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladv;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LacV;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladf;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladu;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LadE;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladb;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ladi;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LadQ;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LadL;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laeb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 53
    iput-object p1, p0, LadM;->a:LbrA;

    .line 54
    const-class v0, LadF;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->a:Lbsk;

    .line 57
    const-class v0, Lady;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->b:Lbsk;

    .line 60
    const-class v0, Ladg;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->c:Lbsk;

    .line 63
    const-class v0, Laec;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->d:Lbsk;

    .line 66
    const-class v0, LadS;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->e:Lbsk;

    .line 69
    const-class v0, Ladv;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->f:Lbsk;

    .line 72
    const-class v0, LacV;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->g:Lbsk;

    .line 75
    const-class v0, Ladf;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->h:Lbsk;

    .line 78
    const-class v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->i:Lbsk;

    .line 81
    const-class v0, Ladu;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->j:Lbsk;

    .line 84
    const-class v0, LadE;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->k:Lbsk;

    .line 87
    const-class v0, Ladb;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->l:Lbsk;

    .line 90
    const-class v0, Ladi;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->m:Lbsk;

    .line 93
    const-class v0, LadQ;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->n:Lbsk;

    .line 96
    const-class v0, LadL;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->o:Lbsk;

    .line 99
    const-class v0, Laeb;

    invoke-static {v0, v2}, LadM;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LadM;->p:Lbsk;

    .line 102
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 17

    .prologue
    .line 179
    sparse-switch p1, :sswitch_data_0

    .line 451
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown binding ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :sswitch_0
    new-instance v4, LadF;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 184
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 182
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->B:Lbsk;

    .line 190
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->B:Lbsk;

    .line 188
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lamn;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->D:Lbsk;

    .line 196
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->D:Lbsk;

    .line 194
    invoke-static {v3, v5}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lakz;

    invoke-direct {v4, v1, v2, v3}, LadF;-><init>(LaGg;Lamn;Lakz;)V

    move-object v1, v4

    .line 449
    :goto_0
    return-object v1

    .line 203
    :sswitch_1
    new-instance v3, Lady;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 206
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->K:Lbsk;

    .line 204
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->k:Lbsk;

    .line 212
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->k:Lbsk;

    .line 210
    invoke-static {v2, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LadE;

    invoke-direct {v3, v1, v2}, Lady;-><init>(Lalo;LadE;)V

    move-object v1, v3

    .line 217
    goto :goto_0

    .line 219
    :sswitch_2
    new-instance v3, Ladg;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 222
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 220
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->D:Lbsk;

    .line 228
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->D:Lbsk;

    .line 226
    invoke-static {v2, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakz;

    invoke-direct {v3, v1, v2}, Ladg;-><init>(Landroid/content/Context;Lakz;)V

    move-object v1, v3

    .line 233
    goto :goto_0

    .line 235
    :sswitch_3
    new-instance v2, Laec;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 238
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lc;

    iget-object v3, v3, Lc;->b:Lbsk;

    .line 236
    invoke-static {v1, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, Laec;-><init>(Landroid/content/Context;)V

    move-object v1, v2

    .line 243
    goto/16 :goto_0

    .line 245
    :sswitch_4
    new-instance v1, LadS;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->m:Lbsk;

    .line 248
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 246
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladi;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->j:Lbsk;

    .line 254
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->j:Lbsk;

    .line 252
    invoke-static {v3, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGg;

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LahE;

    iget-object v4, v4, LahE;->h:Lbsk;

    .line 260
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->h:Lbsk;

    .line 258
    invoke-static {v4, v5}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LahB;

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LagN;

    iget-object v5, v5, LagN;->E:Lbsk;

    .line 266
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->E:Lbsk;

    .line 264
    invoke-static {v5, v6}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LagZ;

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LQH;

    iget-object v6, v6, LQH;->d:Lbsk;

    .line 272
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LadM;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LQH;

    iget-object v7, v7, LQH;->d:Lbsk;

    .line 270
    invoke-static {v6, v7}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LQr;

    move-object/from16 v0, p0

    iget-object v7, v0, LadM;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaHj;

    iget-object v7, v7, LaHj;->h:Lbsk;

    .line 278
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LadM;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaHj;

    iget-object v8, v8, LaHj;->h:Lbsk;

    .line 276
    invoke-static {v7, v8}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LaHr;

    invoke-direct/range {v1 .. v7}, LadS;-><init>(Ladi;LaGg;LahB;LagZ;LQr;LaHr;)V

    goto/16 :goto_0

    .line 285
    :sswitch_5
    new-instance v4, Ladv;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 288
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 286
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->h:Lbsk;

    .line 294
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->h:Lbsk;

    .line 292
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladf;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 300
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->m:Lbsk;

    .line 298
    invoke-static {v3, v5}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ladi;

    invoke-direct {v4, v1, v2, v3}, Ladv;-><init>(LaGM;Ladf;Ladi;)V

    move-object v1, v4

    .line 305
    goto/16 :goto_0

    .line 307
    :sswitch_6
    new-instance v5, LacV;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 310
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 308
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->h:Lbsk;

    .line 316
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->h:Lbsk;

    .line 314
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladf;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 322
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->m:Lbsk;

    .line 320
    invoke-static {v3, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ladi;

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 328
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 326
    invoke-static {v4, v6}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LtK;

    invoke-direct {v5, v1, v2, v3, v4}, LacV;-><init>(LaGM;Ladf;Ladi;LtK;)V

    move-object v1, v5

    .line 333
    goto/16 :goto_0

    .line 335
    :sswitch_7
    new-instance v5, Ladf;

    move-object/from16 v0, p0

    iget-object v1, v0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->c:Lbsk;

    .line 338
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->c:Lbsk;

    .line 336
    invoke-static {v1, v2}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ladg;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 344
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 342
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGM;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 350
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->S:Lbsk;

    .line 348
    invoke-static {v3, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaKM;

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 356
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LQH;

    iget-object v6, v6, LQH;->d:Lbsk;

    .line 354
    invoke-static {v4, v6}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LQr;

    invoke-direct {v5, v1, v2, v3, v4}, Ladf;-><init>(Ladg;LaGM;LaKM;LQr;)V

    move-object v1, v5

    .line 361
    goto/16 :goto_0

    .line 363
    :sswitch_8
    new-instance v1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    move-object/from16 v0, p0

    iget-object v2, v0, LadM;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->k:Lbsk;

    .line 366
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->k:Lbsk;

    .line 364
    invoke-static {v2, v3}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LadE;

    move-object/from16 v0, p0

    iget-object v3, v0, LadM;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->b:Lbsk;

    .line 372
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->b:Lbsk;

    .line 370
    invoke-static {v3, v4}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lady;

    move-object/from16 v0, p0

    iget-object v4, v0, LadM;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->j:Lbsk;

    .line 378
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->j:Lbsk;

    .line 376
    invoke-static {v4, v5}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaGg;

    move-object/from16 v0, p0

    iget-object v5, v0, LadM;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->B:Lbsk;

    .line 384
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->B:Lbsk;

    .line 382
    invoke-static {v5, v6}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lamn;

    move-object/from16 v0, p0

    iget-object v6, v0, LadM;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lc;

    iget-object v6, v6, Lc;->a:Lbsk;

    .line 390
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LadM;->a:LbrA;

    iget-object v7, v7, LbrA;->a:Lc;

    iget-object v7, v7, Lc;->a:Lbsk;

    .line 388
    invoke-static {v6, v7}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, LadM;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LalC;

    iget-object v7, v7, LalC;->N:Lbsk;

    .line 396
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LadM;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->N:Lbsk;

    .line 394
    invoke-static {v7, v8}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LamL;

    move-object/from16 v0, p0

    iget-object v8, v0, LadM;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->K:Lbsk;

    .line 402
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LadM;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LalC;

    iget-object v9, v9, LalC;->K:Lbsk;

    .line 400
    invoke-static {v8, v9}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lalo;

    move-object/from16 v0, p0

    iget-object v9, v0, LadM;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LQH;

    iget-object v9, v9, LQH;->d:Lbsk;

    .line 408
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LadM;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LQH;

    iget-object v10, v10, LQH;->d:Lbsk;

    .line 406
    invoke-static {v9, v10}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LQr;

    move-object/from16 v0, p0

    iget-object v10, v0, LadM;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LalC;

    iget-object v10, v10, LalC;->L:Lbsk;

    .line 414
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LadM;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LalC;

    iget-object v11, v11, LalC;->L:Lbsk;

    .line 412
    invoke-static {v10, v11}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LaKR;

    move-object/from16 v0, p0

    iget-object v11, v0, LadM;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LpG;

    iget-object v11, v11, LpG;->o:Lbsk;

    .line 420
    invoke-virtual {v11}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LadM;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LpG;

    iget-object v12, v12, LpG;->o:Lbsk;

    .line 418
    invoke-static {v11, v12}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lamx;

    move-object/from16 v0, p0

    iget-object v12, v0, LadM;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LahE;

    iget-object v12, v12, LahE;->l:Lbsk;

    .line 426
    invoke-virtual {v12}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, LadM;->a:LbrA;

    iget-object v13, v13, LbrA;->a:LahE;

    iget-object v13, v13, LahE;->l:Lbsk;

    .line 424
    invoke-static {v12, v13}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LahT;

    move-object/from16 v0, p0

    iget-object v13, v0, LadM;->a:LbrA;

    iget-object v13, v13, LbrA;->a:LMu;

    iget-object v13, v13, LMu;->b:Lbsk;

    .line 432
    invoke-virtual {v13}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LadM;->a:LbrA;

    iget-object v14, v14, LbrA;->a:LMu;

    iget-object v14, v14, LMu;->b:Lbsk;

    .line 430
    invoke-static {v13, v14}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LMs;

    move-object/from16 v0, p0

    iget-object v14, v0, LadM;->a:LbrA;

    iget-object v14, v14, LbrA;->a:LpG;

    iget-object v14, v14, LpG;->m:Lbsk;

    .line 438
    invoke-virtual {v14}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, LadM;->a:LbrA;

    iget-object v15, v15, LbrA;->a:LpG;

    iget-object v15, v15, LpG;->m:Lbsk;

    .line 436
    invoke-static {v14, v15}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LtK;

    move-object/from16 v0, p0

    iget-object v15, v0, LadM;->a:LbrA;

    iget-object v15, v15, LbrA;->a:LbiH;

    iget-object v15, v15, LbiH;->aW:Lbsk;

    .line 444
    invoke-virtual {v15}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LadM;->a:LbrA;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LbrA;->a:LbiH;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LbiH;->aW:Lbsk;

    move-object/from16 v16, v0

    .line 442
    invoke-static/range {v15 .. v16}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LbiP;

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;-><init>(LadE;Lady;LaGg;Lamn;Landroid/content/Context;LamL;Lalo;LQr;LaKR;Lamx;LahT;LMs;LtK;LbiP;)V

    goto/16 :goto_0

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        0x277 -> :sswitch_6
        0x50d -> :sswitch_0
        0x50e -> :sswitch_1
        0x510 -> :sswitch_2
        0x511 -> :sswitch_3
        0x512 -> :sswitch_5
        0x513 -> :sswitch_4
        0x514 -> :sswitch_7
        0x515 -> :sswitch_8
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 472
    sparse-switch p2, :sswitch_data_0

    .line 541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :sswitch_0
    check-cast p1, LadG;

    .line 476
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->f:Lbsk;

    .line 479
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladv;

    .line 476
    invoke-virtual {p1, v0}, LadG;->provideExposingDocumentUriCreator(Ladv;)Ladu;

    move-result-object v0

    .line 534
    :goto_0
    return-object v0

    .line 483
    :sswitch_1
    check-cast p1, LadG;

    .line 485
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->a:Lbsk;

    .line 488
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadF;

    .line 485
    invoke-virtual {p1, v0}, LadG;->provideFileInstanceHelper(LadF;)LadE;

    move-result-object v0

    goto :goto_0

    .line 492
    :sswitch_2
    check-cast p1, LadG;

    .line 494
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->ag:Lbsk;

    .line 497
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iget-object v1, p0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->m:Lbsk;

    .line 501
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ladi;

    .line 494
    invoke-virtual {p1, v0, v1}, LadG;->provideContentStorage(LbiP;Ladi;)Ladb;

    move-result-object v0

    goto :goto_0

    .line 505
    :sswitch_3
    check-cast p1, LadG;

    .line 507
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->i:Lbsk;

    .line 510
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 507
    invoke-virtual {p1, v0}, LadG;->provideDocumentFileManager(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Ladi;

    move-result-object v0

    goto :goto_0

    .line 514
    :sswitch_4
    check-cast p1, LadG;

    .line 516
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->e:Lbsk;

    .line 519
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadS;

    .line 516
    invoke-virtual {p1, v0}, LadG;->provideLocalFileManager(LadS;)LadQ;

    move-result-object v0

    goto :goto_0

    .line 523
    :sswitch_5
    check-cast p1, LadG;

    .line 525
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->m:Lbsk;

    .line 528
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    .line 525
    invoke-virtual {p1, v0}, LadG;->provideGarbageCollector(Ladi;)LadL;

    move-result-object v0

    goto :goto_0

    .line 532
    :sswitch_6
    check-cast p1, LadG;

    .line 534
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->d:Lbsk;

    .line 537
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laec;

    .line 534
    invoke-virtual {p1, v0}, LadG;->provideTempFileStore(Laec;)Laeb;

    move-result-object v0

    goto :goto_0

    .line 472
    nop

    :sswitch_data_0
    .sparse-switch
        0x4d -> :sswitch_3
        0xba -> :sswitch_2
        0x156 -> :sswitch_6
        0x22a -> :sswitch_0
        0x254 -> :sswitch_4
        0x291 -> :sswitch_5
        0x50f -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 120
    const-class v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x7b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LadM;->a(LbuP;LbuB;)V

    .line 123
    const-class v0, LadF;

    iget-object v1, p0, LadM;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 124
    const-class v0, Lady;

    iget-object v1, p0, LadM;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 125
    const-class v0, Ladg;

    iget-object v1, p0, LadM;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 126
    const-class v0, Laec;

    iget-object v1, p0, LadM;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 127
    const-class v0, LadS;

    iget-object v1, p0, LadM;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 128
    const-class v0, Ladv;

    iget-object v1, p0, LadM;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 129
    const-class v0, LacV;

    iget-object v1, p0, LadM;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 130
    const-class v0, Ladf;

    iget-object v1, p0, LadM;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 131
    const-class v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v1, p0, LadM;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 132
    const-class v0, Ladu;

    iget-object v1, p0, LadM;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 133
    const-class v0, LadE;

    iget-object v1, p0, LadM;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 134
    const-class v0, Ladb;

    iget-object v1, p0, LadM;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 135
    const-class v0, Ladi;

    iget-object v1, p0, LadM;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 136
    const-class v0, LadQ;

    iget-object v1, p0, LadM;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 137
    const-class v0, LadL;

    iget-object v1, p0, LadM;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 138
    const-class v0, Laeb;

    iget-object v1, p0, LadM;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, LadM;->a(Ljava/lang/Class;Lbsk;)V

    .line 139
    iget-object v0, p0, LadM;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x50d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 141
    iget-object v0, p0, LadM;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x50e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 143
    iget-object v0, p0, LadM;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x510

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 145
    iget-object v0, p0, LadM;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x511

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 147
    iget-object v0, p0, LadM;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x513

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 149
    iget-object v0, p0, LadM;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x512

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 151
    iget-object v0, p0, LadM;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x277

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 153
    iget-object v0, p0, LadM;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x514

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 155
    iget-object v0, p0, LadM;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x515

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 157
    iget-object v0, p0, LadM;->j:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x22a

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 159
    iget-object v0, p0, LadM;->k:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x50f

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 161
    iget-object v0, p0, LadM;->l:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0xba

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 163
    iget-object v0, p0, LadM;->m:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x4d

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 165
    iget-object v0, p0, LadM;->n:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x254

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 167
    iget-object v0, p0, LadM;->o:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x291

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 169
    iget-object v0, p0, LadM;->p:Lbsk;

    const-class v1, LadG;

    const/16 v2, 0x156

    invoke-virtual {p0, v1, v2}, LadM;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 171
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 458
    packed-switch p1, :pswitch_data_0

    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    .line 462
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    .line 463
    invoke-virtual {v0, p2}, LadM;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;)V

    .line 468
    return-void

    .line 458
    :pswitch_data_0
    .packed-switch 0x7b
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, LadM;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->m:Lbsk;

    .line 111
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LadM;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->m:Lbsk;

    .line 109
    invoke-static {v0, v1}, LadM;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    iput-object v0, p1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Ladi;

    .line 115
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method
