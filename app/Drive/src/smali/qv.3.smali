.class public enum Lqv;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lqv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lqv;

.field private static final synthetic a:[Lqv;

.field public static final enum b:Lqv;

.field public static final enum c:Lqv;

.field public static final enum d:Lqv;

.field public static final enum e:Lqv;

.field public static final enum f:Lqv;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 28
    new-instance v0, Lqv;

    const-string v1, "OWNER"

    const-string v2, "owner"

    sget v3, Lxi;->sharing_role_owner:I

    invoke-direct {v0, v1, v5, v2, v3}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->a:Lqv;

    .line 29
    new-instance v0, Lqv;

    const-string v1, "WRITER"

    const-string v2, "writer"

    sget v3, Lxi;->sharing_role_writer:I

    invoke-direct {v0, v1, v6, v2, v3}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->b:Lqv;

    .line 30
    new-instance v0, Lqv;

    const-string v1, "READER"

    const-string v2, "reader"

    sget v3, Lxi;->sharing_role_reader:I

    invoke-direct {v0, v1, v7, v2, v3}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->c:Lqv;

    .line 31
    new-instance v0, Lqv;

    const-string v1, "NONE"

    const-string v2, "none"

    sget v3, Lxi;->sharing_role_unknown:I

    invoke-direct {v0, v1, v8, v2, v3}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->d:Lqv;

    .line 32
    new-instance v0, Lqv;

    const-string v1, "NOACCESS"

    const-string v2, "noaccess"

    sget v3, Lxi;->sharing_role_no_access:I

    invoke-direct {v0, v1, v9, v2, v3}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->e:Lqv;

    .line 33
    new-instance v0, Lqw;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const/4 v3, 0x0

    sget v4, Lxi;->sharing_role_unknown:I

    invoke-direct {v0, v1, v2, v3, v4}, Lqw;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lqv;->f:Lqv;

    .line 27
    const/4 v0, 0x6

    new-array v0, v0, [Lqv;

    sget-object v1, Lqv;->a:Lqv;

    aput-object v1, v0, v5

    sget-object v1, Lqv;->b:Lqv;

    aput-object v1, v0, v6

    sget-object v1, Lqv;->c:Lqv;

    aput-object v1, v0, v7

    sget-object v1, Lqv;->d:Lqv;

    aput-object v1, v0, v8

    sget-object v1, Lqv;->e:Lqv;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lqv;->f:Lqv;

    aput-object v2, v0, v1

    sput-object v0, Lqv;->a:[Lqv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput-object p3, p0, Lqv;->a:Ljava/lang/String;

    .line 44
    iput p4, p0, Lqv;->a:I

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;ILqp;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lqv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lqv;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lqv;->a:I

    return v0
.end method

.method public static a(Ljava/lang/String;)Lqv;
    .locals 6

    .prologue
    .line 64
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-static {}, Lqv;->values()[Lqv;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 66
    iget-object v5, v0, Lqv;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 70
    :goto_1
    return-object v0

    .line 65
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_1
    sget-object v0, Lqv;->f:Lqv;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lqv;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lqv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lqv;

    return-object v0
.end method

.method public static values()[Lqv;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lqv;->a:[Lqv;

    invoke-virtual {v0}, [Lqv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqv;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lqv;->a:Ljava/lang/String;

    return-object v0
.end method
