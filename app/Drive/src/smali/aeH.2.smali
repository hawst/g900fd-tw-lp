.class public LaeH;
.super Ljava/lang/Object;
.source "AccountMetadataGDataParserFactory.java"

# interfaces
.implements LbwT;


# instance fields
.field private final a:Lbxr;


# direct methods
.method public constructor <init>(Lbxr;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LaeH;->a:Lbxr;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)LaeG;
    .locals 3

    .prologue
    .line 37
    :try_start_0
    iget-object v0, p0, LaeH;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    new-instance v1, LaeG;

    invoke-direct {v1, p1, v0}, LaeG;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Lbxk;

    const-string v2, "Could not create XmlPullParser"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Class;Ljava/io/InputStream;)LaeG;
    .locals 3

    .prologue
    .line 48
    const-class v0, LaeE;

    if-ne p1, v0, :cond_0

    .line 49
    invoke-virtual {p0, p2}, LaeH;->a(Ljava/io/InputStream;)LaeG;

    move-result-object v0

    return-object v0

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown entry class \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 52
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, LaeH;->a(Ljava/lang/Class;Ljava/io/InputStream;)LaeG;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbxb;)Lbxs;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot serialize"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/util/Enumeration;)Lbxs;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot serialize"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
