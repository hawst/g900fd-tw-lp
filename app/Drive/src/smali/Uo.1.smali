.class public LUo;
.super Ljava/lang/Object;
.source "OperationSyncReporters.java"


# static fields
.field public static final a:LaHy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, LUp;

    invoke-direct {v0}, LUp;-><init>()V

    sput-object v0, LUo;->a:LaHy;

    return-void
.end method

.method public static a(Landroid/content/Context;Lald;)LaHy;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-static {p0}, LUo;->a(Landroid/content/Context;)LbmL;

    move-result-object v0

    .line 33
    new-instance v1, LUq;

    invoke-direct {v1, v0, p1}, LUq;-><init>(LbmL;Lald;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)LbmL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LbmL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    .line 49
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lxi;->write_access_denied:I

    .line 50
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 51
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lxi;->operation_sync_error:I

    .line 52
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 53
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lxi;->operation_sync_network_error:I

    .line 54
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 55
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lxi;->operation_sync_error:I

    .line 56
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 57
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method
