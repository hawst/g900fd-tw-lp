.class public LEQ;
.super Ljava/lang/Object;
.source "DocumentOpenerErrorDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/net/Uri;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, LEQ;->a:Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    iput-object p2, p0, LEQ;->a:Landroid/net/Uri;

    iput-object p3, p0, LEQ;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 136
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, LEQ;->a:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 137
    iget-object v1, p0, LEQ;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 138
    iget-object v0, p0, LEQ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 139
    return-void
.end method
