.class public LarH;
.super Ljava/lang/Object;
.source "CascadingScaleAnimator.java"

# interfaces
.implements LarE;


# instance fields
.field private a:I

.field private final a:J

.field private a:Landroid/animation/AnimatorSet$Builder;

.field private final a:Landroid/animation/AnimatorSet;

.field private a:Z

.field private b:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LarH;->a:Landroid/animation/AnimatorSet;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, LarH;->a:I

    .line 28
    iput-wide p3, p0, LarH;->a:J

    .line 29
    invoke-virtual {p0, p1, p2}, LarH;->a(J)V

    .line 30
    return-void
.end method

.method private a(FFFFJ)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 48
    const-string v1, "scaleX"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 49
    new-array v1, v3, [F

    aput p1, v1, v4

    aput p2, v1, v5

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 51
    new-instance v1, Landroid/animation/ObjectAnimator;

    invoke-direct {v1}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 52
    const-string v2, "scaleY"

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 53
    new-array v2, v3, [F

    aput p3, v2, v4

    aput p4, v2, v5

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 55
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, LarH;->b:Landroid/animation/AnimatorSet;

    .line 56
    iget-object v2, p0, LarH;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, p5, p6}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 57
    iget-object v2, p0, LarH;->b:Landroid/animation/AnimatorSet;

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 58
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, LarH;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, LarH;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 89
    return-void
.end method

.method public a(J)V
    .locals 9

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, LarH;->a:Z

    move-object v1, p0

    move v4, v2

    move v5, v3

    move-wide v6, p1

    .line 34
    invoke-direct/range {v1 .. v7}, LarH;->a(FFFFJ)V

    .line 35
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 62
    iget-object v0, p0, LarH;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->clone()Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 63
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    .line 64
    iget v1, p0, LarH;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LarH;->a:I

    int-to-long v2, v1

    iget-wide v4, p0, LarH;->a:J

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 66
    iget-boolean v1, p0, LarH;->a:Z

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p1, v6}, Landroid/view/View;->setScaleX(F)V

    .line 68
    invoke-virtual {p1, v6}, Landroid/view/View;->setScaleY(F)V

    .line 74
    :goto_0
    iget-object v1, p0, LarH;->a:Landroid/animation/AnimatorSet$Builder;

    if-nez v1, :cond_1

    .line 75
    iget-object v1, p0, LarH;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    iput-object v0, p0, LarH;->a:Landroid/animation/AnimatorSet$Builder;

    .line 79
    :goto_1
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1, v7}, Landroid/view/View;->setScaleX(F)V

    .line 71
    invoke-virtual {p1, v7}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 77
    :cond_1
    iget-object v1, p0, LarH;->a:Landroid/animation/AnimatorSet$Builder;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1
.end method
