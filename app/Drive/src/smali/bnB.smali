.class final LbnB;
.super LblL;
.source "LinkedHashMultimap.java"

# interfaces
.implements LbnE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LblL",
        "<TK;TV;>;",
        "LbnE",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field a:LbnB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:LbnE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnE",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field b:LbnB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field b:LbnE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnE",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field c:LbnB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;ILbnB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;I",
            "LbnB",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0}, LblL;-><init>()V

    .line 172
    iput-object p1, p0, LbnB;->a:Ljava/lang/Object;

    .line 173
    iput-object p2, p0, LbnB;->b:Ljava/lang/Object;

    .line 174
    iput p3, p0, LbnB;->a:I

    .line 175
    iput-object p4, p0, LbnB;->a:LbnB;

    .line 176
    return-void
.end method


# virtual methods
.method public a()LbnB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, LbnB;->b:LbnB;

    return-object v0
.end method

.method public a()LbnE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbnE",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, LbnB;->a:LbnE;

    return-object v0
.end method

.method public a(LbnB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbnB",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 213
    iput-object p1, p0, LbnB;->c:LbnB;

    .line 214
    return-void
.end method

.method public a(LbnE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbnE",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 197
    iput-object p1, p0, LbnB;->a:LbnE;

    .line 198
    return-void
.end method

.method public b()LbnB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, LbnB;->c:LbnB;

    return-object v0
.end method

.method public b()LbnE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbnE",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, LbnB;->b:LbnE;

    return-object v0
.end method

.method public b(LbnB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbnB",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 217
    iput-object p1, p0, LbnB;->b:LbnB;

    .line 218
    return-void
.end method

.method public b(LbnE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbnE",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 201
    iput-object p1, p0, LbnB;->b:LbnE;

    .line 202
    return-void
.end method

.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, LbnB;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, LbnB;->b:Ljava/lang/Object;

    return-object v0
.end method
