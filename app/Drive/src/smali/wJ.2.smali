.class public LwJ;
.super Ljava/lang/Object;
.source "ManifestManagerImpl.java"

# interfaces
.implements LwH;


# instance fields
.field private final a:LaGg;

.field private final a:LaKR;

.field private final a:Ladi;

.field private final a:Lww;


# direct methods
.method public constructor <init>(LaGg;Lww;Ladi;LaKR;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, LwJ;->a:LaGg;

    .line 99
    iput-object p2, p0, LwJ;->a:Lww;

    .line 100
    iput-object p3, p0, LwJ;->a:Ladi;

    .line 101
    iput-object p4, p0, LwJ;->a:LaKR;

    .line 102
    return-void
.end method

.method private a(LwB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LaFR;
    .locals 5

    .prologue
    .line 258
    iget-object v0, p0, LwJ;->a:LaGg;

    invoke-interface {v0, p2, p3, p4, p5}, LaGg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LaFR;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, LaFR;->e()V

    .line 261
    invoke-interface {p1}, LwB;->a()LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwC;

    .line 262
    iget-object v3, p0, LwJ;->a:LaGg;

    invoke-interface {v0}, LwC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    .line 263
    invoke-interface {v0}, LwC;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v4, v0}, LaGr;->a(Ljava/io/File;Ljavax/crypto/SecretKey;)LaGr;

    move-result-object v0

    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v0

    .line 264
    iget-object v3, p0, LwJ;->a:LaGg;

    invoke-interface {v3, v1, v0}, LaGg;->a(LaFR;LaGp;)LaFS;

    move-result-object v0

    invoke-virtual {v0}, LaFS;->e()V

    goto :goto_0

    .line 266
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(LaGL;)LaGK;
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, LwJ;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaGL;)LaGK;

    move-result-object v0

    .line 190
    iget-object v1, p0, LwJ;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0}, LaGK;->a()LaGs;

    move-result-object v1

    invoke-virtual {p1}, LaGL;->a()LaGs;

    move-result-object v2

    invoke-virtual {v1, v2}, LaGs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 195
    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;LaFO;LaGs;LwL;Lyt;)LaGL;
    .locals 22

    .prologue
    .line 110
    invoke-interface/range {p4 .. p4}, LwL;->c()Ljava/lang/String;

    move-result-object v7

    .line 111
    invoke-interface/range {p4 .. p4}, LwL;->b()Ljava/lang/String;

    move-result-object v15

    .line 112
    invoke-interface/range {p4 .. p4}, LwL;->a()Z

    move-result v17

    .line 113
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, v17

    invoke-static {v0, v1, v15, v2, v3}, LaGL;->a(Ljava/lang/String;LaFO;Ljava/lang/String;LaGs;Z)LaGL;

    move-result-object v18

    .line 115
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LwJ;->a(LaGL;)LaGK;

    move-result-object v4

    .line 116
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LaGK;->a()Z

    move-result v5

    if-nez v5, :cond_0

    .line 117
    move-object/from16 v0, p0

    iget-object v5, v0, LwJ;->a:LaGg;

    invoke-virtual {v4}, LaGK;->a()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {v5, v8, v9}, LaGg;->a(J)LaFR;

    move-result-object v4

    .line 118
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LaFR;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v4, v18

    .line 156
    :goto_0
    return-object v4

    .line 122
    :catch_0
    move-exception v4

    .line 123
    const-string v5, "ManifestManagerImpl"

    const-string v6, "DB error fetching manifest for %s; recreating manifest."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-static {v5, v4, v6, v8}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 125
    :cond_0
    invoke-interface/range {p4 .. p4}, LwL;->a()Ljava/util/Set;

    move-result-object v8

    .line 126
    move-object/from16 v0, p0

    iget-object v4, v0, LwJ;->a:Lww;

    move-object/from16 v5, p2

    move-object/from16 v6, p1

    move-object/from16 v9, p5

    invoke-interface/range {v4 .. v9}, Lww;->a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Lyt;)LwB;

    move-result-object v6

    .line 128
    move-object/from16 v0, p0

    iget-object v4, v0, LwJ;->a:LaGg;

    invoke-interface {v4}, LaGg;->a()V

    .line 130
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LwJ;->a(LaGL;)LaGK;

    move-result-object v4

    .line 131
    invoke-interface/range {p4 .. p4}, LwL;->d()Ljava/lang/String;

    move-result-object v8

    .line 132
    invoke-interface/range {p4 .. p4}, LwL;->e()Ljava/lang/String;

    move-result-object v9

    .line 134
    invoke-interface/range {p4 .. p4}, LwL;->a()I

    move-result v10

    move-object/from16 v5, p0

    .line 133
    invoke-direct/range {v5 .. v10}, LwJ;->a(LwB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LaFR;

    move-result-object v5

    .line 135
    if-eqz v4, :cond_1

    .line 136
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, LaGK;->a(Z)V

    .line 137
    invoke-virtual {v5}, LaFR;->c()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, LaGK;->a(J)V

    .line 148
    :goto_1
    invoke-virtual {v4}, LaGK;->e()V

    .line 149
    move-object/from16 v0, p0

    iget-object v4, v0, LwJ;->a:LaGg;

    invoke-interface {v4}, LaGg;->c()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    move-object/from16 v0, p0

    iget-object v4, v0, LwJ;->a:LaGg;

    invoke-interface {v4}, LaGg;->b()V

    move-object/from16 v4, v18

    .line 156
    goto :goto_0

    .line 139
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v8, v0, LwJ;->a:LaGg;

    .line 141
    invoke-interface/range {p4 .. p4}, LwL;->a()Ljava/lang/String;

    move-result-object v11

    .line 142
    invoke-virtual {v5}, LaFR;->c()J

    move-result-wide v12

    new-instance v14, Ljava/util/Date;

    .line 143
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface/range {p4 .. p4}, LwL;->a()J

    move-result-wide v20

    add-long v4, v4, v20

    invoke-direct {v14, v4, v5}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v9, p1

    move-object v10, v15

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    .line 139
    invoke-interface/range {v8 .. v17}, LaGg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;LaFO;LaGs;Z)LaGK;
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    goto :goto_1

    .line 150
    :catch_1
    move-exception v4

    .line 151
    :try_start_3
    new-instance v5, Lwx;

    const-string v6, "DB error fetching manifest %s %s."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object v7, v8, v9

    .line 152
    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Lwx;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 154
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, LwJ;->a:LaGg;

    invoke-interface {v5}, LaGg;->b()V

    throw v4
.end method

.method public a(LaGL;)LwI;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 163
    :try_start_0
    invoke-virtual {p0, p1}, LwJ;->a(LaGL;)LaGK;

    move-result-object v3

    .line 164
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LaGK;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v7

    .line 180
    :goto_0
    return-object v0

    .line 167
    :cond_1
    iget-object v0, p0, LwJ;->a:LaGg;

    invoke-virtual {v3}, LaGK;->a()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, LaGg;->a(J)LaFR;

    move-result-object v6

    .line 168
    iget-object v0, p0, LwJ;->a:LaGg;

    invoke-interface {v0, v6}, LaGg;->a(LaFR;)Ljava/util/List;

    move-result-object v0

    .line 169
    iget-object v1, p0, LwJ;->a:Ladi;

    invoke-interface {v1, v0}, Ladi;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 170
    if-nez v1, :cond_2

    .line 172
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LaGK;->a(Z)V

    move-object v0, v7

    .line 173
    goto :goto_0

    .line 175
    :cond_2
    new-instance v0, LwK;

    invoke-virtual {v6}, LaFR;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, LaGK;->a()Ljava/lang/String;

    move-result-object v3

    .line 176
    invoke-virtual {v6}, LaFR;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, LaFR;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LaFR;->a()I

    move-result v6

    invoke-direct/range {v0 .. v6}, LwK;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    const-string v1, "ManifestManagerImpl"

    const-string v2, "DB error fetching manifest for %s; recreating manifest."

    new-array v3, v8, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 179
    invoke-virtual {p1}, LaGL;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 178
    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v7

    .line 180
    goto :goto_0
.end method
