.class LblJ;
.super Lbpm;
.source "AbstractMapBasedMultiset.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbpm",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LblI;

.field final synthetic a:Ljava/util/Map$Entry;


# direct methods
.method constructor <init>(LblI;Ljava/util/Map$Entry;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, LblJ;->a:LblI;

    iput-object p2, p0, LblJ;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, Lbpm;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, LblJ;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    invoke-virtual {v0}, Lbmc;->a()I

    move-result v1

    .line 106
    if-nez v1, :cond_0

    .line 107
    iget-object v0, p0, LblJ;->a:LblI;

    iget-object v0, v0, LblI;->a:LblH;

    invoke-static {v0}, LblH;->a(LblH;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, LblJ;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmc;

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Lbmc;->a()I

    move-result v0

    .line 112
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, LblJ;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
