.class public LaGO;
.super Ljava/lang/Object;
.source "ModelLoaderAsyncTaskActivityHandler.java"


# annotations
.annotation runtime LbuO;
.end annotation


# instance fields
.field private final a:LaGP;

.field private final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/app/Activity;",
            "LaGQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbiP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LaGP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-virtual {p1}, LbiP;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGP;

    iput-object v0, p0, LaGO;->a:LaGP;

    .line 124
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LaGO;->a:Ljava/util/WeakHashMap;

    .line 125
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;LaGN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LaGN",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-static {}, LamV;->a()V

    .line 129
    iget-object v0, p0, LaGO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGQ;

    .line 130
    if-nez v0, :cond_0

    .line 131
    new-instance v0, LaGQ;

    invoke-direct {v0, p1}, LaGQ;-><init>(Landroid/app/Activity;)V

    .line 132
    iget-object v1, p0, LaGO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    :cond_0
    invoke-virtual {v0, p2}, LaGQ;->b(LaGN;)V

    .line 135
    return-void
.end method

.method public a(Landroid/app/Activity;LaGT;)V
    .locals 2

    .prologue
    .line 147
    const-string v0, "ModelLoaderAsyncTaskActivityHandler"

    const-string v1, "handle modelLoader exception"

    invoke-static {v0, p2, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, LaGO;->a:LaGP;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, LaGO;->a:LaGP;

    invoke-interface {v0, p1, p2}, LaGP;->a(Landroid/app/Activity;LaGT;)V

    .line 151
    :cond_0
    return-void
.end method

.method public b(Landroid/app/Activity;LaGN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LaGN",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {}, LamV;->a()V

    .line 139
    iget-object v0, p0, LaGO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGQ;

    .line 140
    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-virtual {v0, p2}, LaGQ;->a(LaGN;)V

    goto :goto_0
.end method
