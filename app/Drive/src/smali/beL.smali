.class public LbeL;
.super LbdR;
.source "JsonHttpContent.java"


# instance fields
.field private final a:LbeO;

.field private final a:Ljava/lang/Object;

.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LbeO;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 67
    const-string v0, "application/json; charset=UTF-8"

    invoke-direct {p0, v0}, LbdR;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbeO;

    iput-object v0, p0, LbeL;->a:LbeO;

    .line 69
    invoke-static {p2}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LbeL;->a:Ljava/lang/Object;

    .line 70
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LbeL;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, LbeL;->a:Ljava/lang/String;

    .line 130
    return-object p0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, LbeL;->a:LbeO;

    invoke-virtual {p0}, LbeL;->a()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LbeO;->a(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)LbeP;

    move-result-object v0

    .line 74
    iget-object v1, p0, LbeL;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 75
    invoke-virtual {v0}, LbeP;->d()V

    .line 76
    iget-object v1, p0, LbeL;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LbeP;->a(Ljava/lang/String;)V

    .line 78
    :cond_0
    iget-object v1, p0, LbeL;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LbeP;->a(Ljava/lang/Object;)V

    .line 79
    iget-object v1, p0, LbeL;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 80
    invoke-virtual {v0}, LbeP;->e()V

    .line 82
    :cond_1
    invoke-virtual {v0}, LbeP;->a()V

    .line 83
    return-void
.end method
