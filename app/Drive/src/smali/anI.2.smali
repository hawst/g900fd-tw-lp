.class public LanI;
.super Ljava/lang/Object;
.source "BackgroundRequestFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<TK;",
            "LbsU",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<",
            "LbsU",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoo;Laop;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<TK;TV;>;",
            "Laop",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, LanJ;

    invoke-direct {v0, p0}, LanJ;-><init>(LanI;)V

    iput-object v0, p0, LanI;->a:Laoy;

    .line 23
    new-instance v0, LanK;

    invoke-direct {v0, p0}, LanK;-><init>(LanI;)V

    iput-object v0, p0, LanI;->b:Laoy;

    .line 39
    new-instance v0, LanL;

    invoke-direct {v0, p0, p2, p1}, LanL;-><init>(LanI;Laop;Laoo;)V

    iput-object v0, p0, LanI;->a:Laoo;

    .line 50
    return-void
.end method

.method static synthetic a(LanI;)Laoy;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, LanI;->a:Laoy;

    return-object v0
.end method

.method static synthetic b(LanI;)Laoy;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, LanI;->b:Laoy;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, LanI;->b:Laoy;

    .line 55
    invoke-static {v0}, Laou;->a(Laoy;)Laou;

    move-result-object v0

    .line 56
    iget-object v1, p0, LanI;->a:Laoy;

    .line 57
    invoke-static {v1}, Laou;->a(Laoy;)Laou;

    move-result-object v1

    .line 59
    iget-object v2, p0, LanI;->a:Laoo;

    invoke-interface {v2, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v2

    .line 60
    new-instance v3, LanM;

    invoke-direct {v3, p0, v0, v1}, LanM;-><init>(LanI;Laou;Laou;)V

    invoke-static {v2, v3}, LbsK;->a(LbsU;LbsJ;)V

    .line 72
    invoke-static {v2}, Lalv;->a(LbsU;)LbsU;

    move-result-object v2

    .line 73
    invoke-virtual {v0, v2}, Laou;->b(LbsU;)V

    .line 74
    invoke-virtual {v1, v2}, Laou;->b(LbsU;)V

    .line 75
    return-object v2
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 79
    return-void
.end method
