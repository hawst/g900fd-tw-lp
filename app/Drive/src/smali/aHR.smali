.class public LaHR;
.super Ljava/lang/Object;
.source "UnmodifiableDriveAppSet.java"

# interfaces
.implements LaHK;


# instance fields
.field private final a:LaHK;


# direct methods
.method private constructor <init>(LaHK;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHK;

    iput-object v0, p0, LaHR;->a:LaHK;

    .line 21
    return-void
.end method

.method public static a(LaHK;)LaHR;
    .locals 1

    .prologue
    .line 24
    instance-of v0, p0, LaHR;

    if-eqz v0, :cond_0

    .line 25
    check-cast p0, LaHR;

    .line 27
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LaHR;

    invoke-direct {v0, p0}, LaHR;-><init>(LaHK;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, LaHR;->a:LaHK;

    invoke-interface {v0}, LaHK;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, LaHR;->a:LaHK;

    invoke-interface {v0, p1}, LaHK;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, LaHR;->a:LaHK;

    invoke-interface {v0, p1}, LaHK;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LaHR;->a:LaHK;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
