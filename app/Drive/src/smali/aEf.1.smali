.class public final enum LaEf;
.super Ljava/lang/Enum;
.source "AccountTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEf;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEf;

.field private static final synthetic a:[LaEf;

.field public static final enum b:LaEf;

.field public static final enum c:LaEf;

.field public static final enum d:LaEf;

.field public static final enum e:LaEf;

.field public static final enum f:LaEf;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum g:LaEf;

.field public static final enum h:LaEf;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum i:LaEf;

.field public static final enum j:LaEf;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum k:LaEf;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum l:LaEf;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/16 v7, 0xe

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 45
    new-instance v0, LaEf;

    const-string v1, "ACCOUNT_HOLDER_NAME"

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountHolderName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    new-array v4, v9, [Ljava/lang/String;

    .line 46
    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 47
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 46
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->a:LaEf;

    .line 62
    new-instance v0, LaEf;

    const-string v1, "FORCE_FULL_SYNC"

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    const/16 v3, 0x3c

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 63
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x3d

    .line 64
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x3d

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 65
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x47

    .line 66
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x48

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 67
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x50

    .line 68
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x51

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 69
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x55

    .line 70
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x56

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 71
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x6a

    .line 72
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x6b

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 73
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x6e

    .line 74
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    const/16 v3, 0x6f

    new-instance v4, LaFG;

    const-string v5, "forceFullSync"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 75
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->b:LaEf;

    .line 77
    new-instance v0, LaEf;

    const-string v1, "LAST_SYNC_TIME"

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "lastSyncTime"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 78
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->c:LaEf;

    .line 80
    new-instance v0, LaEf;

    const-string v1, "FOLDER_SYNC_CLIP_TIME"

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "folderSyncClipTime"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 81
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->d:LaEf;

    .line 83
    new-instance v0, LaEf;

    const-string v1, "DOCUMENT_SYNC_CLIP_TIME"

    const/4 v2, 0x4

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "documentSyncClipTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 84
    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->e:LaEf;

    .line 86
    new-instance v0, LaEf;

    const-string v1, "__LEGACY_COLUMN_LAST_SYNC_ETAG"

    const/4 v2, 0x5

    .line 87
    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastSyncEtag"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 88
    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x3b

    .line 89
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->f:LaEf;

    .line 91
    new-instance v0, LaEf;

    const-string v1, "LAST_SYNC_CHANGE_STAMP"

    const/4 v2, 0x6

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastSyncChangeStamp"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 93
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    .line 92
    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->g:LaEf;

    .line 95
    new-instance v0, LaEf;

    const-string v1, "SYNC_IN_PROGRESS"

    const/4 v2, 0x7

    .line 99
    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "syncInProgress"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 100
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->h:LaEf;

    .line 106
    new-instance v0, LaEf;

    const-string v1, "LAST_SYNC_SEQUENCE_NUMBER"

    const/16 v2, 0x8

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x3b

    new-instance v5, LaFG;

    const-string v6, "lastSyncSequenceNumber"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 107
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    const-wide/16 v6, 0x0

    .line 108
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 107
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->i:LaEf;

    .line 110
    new-instance v0, LaEf;

    const-string v1, "OFFLINE_POLICY_ENABLED"

    const/16 v2, 0x9

    .line 117
    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x4e

    new-instance v5, LaFG;

    const-string v6, "offlinePolicyEnabled"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 118
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->j:LaEf;

    .line 120
    new-instance v0, LaEf;

    const-string v1, "LAST_OFFLINE_ENABLED_STATE"

    const/16 v2, 0xa

    .line 126
    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x53

    new-instance v5, LaFG;

    const-string v6, "lastOfflineEnabledState"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 127
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->k:LaEf;

    .line 133
    new-instance v0, LaEf;

    const-string v1, "LAST_FORCE_SYNC_LEVEL"

    const/16 v2, 0xb

    invoke-static {}, LaEe;->b()LaEe;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x57

    new-instance v5, LaFG;

    const-string v6, "lastForceSyncLevel"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 134
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEf;->l:LaEf;

    .line 44
    const/16 v0, 0xc

    new-array v0, v0, [LaEf;

    sget-object v1, LaEf;->a:LaEf;

    aput-object v1, v0, v9

    sget-object v1, LaEf;->b:LaEf;

    aput-object v1, v0, v8

    sget-object v1, LaEf;->c:LaEf;

    aput-object v1, v0, v10

    sget-object v1, LaEf;->d:LaEf;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, LaEf;->e:LaEf;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEf;->f:LaEf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEf;->g:LaEf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaEf;->h:LaEf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEf;->i:LaEf;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaEf;->j:LaEf;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaEf;->k:LaEf;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaEf;->l:LaEf;

    aput-object v2, v0, v1

    sput-object v0, LaEf;->a:[LaEf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 141
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEf;->a:LaFr;

    .line 142
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEf;
    .locals 1

    .prologue
    .line 44
    const-class v0, LaEf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEf;

    return-object v0
.end method

.method public static values()[LaEf;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LaEf;->a:[LaEf;

    invoke-virtual {v0}, [LaEf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEf;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LaEf;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, LaEf;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
