.class LamR;
.super Ljava/lang/Object;
.source "StringPartitionTree.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:I

.field final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LamR",
            "<TU;>;>;"
        }
    .end annotation
.end field

.field final a:Ljava/lang/String;

.field final b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LamQ",
            "<TU;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LamR",
            "<TU;>;>;",
            "Ljava/util/List",
            "<",
            "LamQ",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput p1, p0, LamR;->a:I

    .line 87
    iput-object p2, p0, LamR;->a:Ljava/lang/String;

    .line 88
    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LamR;->a:LbmF;

    .line 89
    if-nez p4, :cond_1

    :goto_1
    iput-object v1, p0, LamR;->b:LbmF;

    .line 91
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    .line 92
    invoke-static {p2}, LamO;->a(Ljava/lang/Object;)V

    .line 93
    iget-object v0, p0, LamR;->a:LbmF;

    invoke-static {v0}, LamO;->a(Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, LamR;->b:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v2

    :goto_2
    invoke-static {v0}, LbiT;->a(Z)V

    .line 105
    :goto_3
    return-void

    .line 88
    :cond_0
    invoke-static {p3}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {p4}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v1

    goto :goto_1

    :cond_2
    move v0, v3

    .line 94
    goto :goto_2

    .line 96
    :cond_3
    invoke-static {p1, v4}, LbiT;->a(II)I

    .line 97
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, LamR;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    if-ne v0, v4, :cond_4

    :goto_4
    invoke-static {v2}, LbiT;->a(Z)V

    .line 100
    iget-object v0, p0, LamR;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamR;

    .line 101
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_4
    move v2, v3

    .line 98
    goto :goto_4

    .line 103
    :cond_5
    iget-object v0, p0, LamR;->b:LbmF;

    invoke-static {v0}, LamO;->a(Ljava/lang/Object;)V

    goto :goto_3
.end method

.method synthetic constructor <init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;LamP;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3, p4}, LamR;-><init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 2

    .prologue
    .line 108
    iget v0, p0, LamR;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
