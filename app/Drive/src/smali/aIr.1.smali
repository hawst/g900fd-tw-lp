.class public LaIr;
.super Ljava/lang/Object;
.source "StorageFileDownloader.java"


# instance fields
.field private final a:LaGM;

.field private final a:Ladi;

.field private final a:LagG;

.field private final a:LagZ;

.field private final a:Lago;


# direct methods
.method public constructor <init>(LagG;LaGM;Lago;Ladi;LagZ;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, LaIr;->a:LagG;

    .line 44
    iput-object p2, p0, LaIr;->a:LaGM;

    .line 45
    iput-object p3, p0, LaIr;->a:Lago;

    .line 46
    iput-object p4, p0, LaIr;->a:Ladi;

    .line 47
    iput-object p5, p0, LaIr;->a:LagZ;

    .line 48
    return-void
.end method


# virtual methods
.method public a(LaGo;LacY;)LaGo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 60
    iget-object v0, p0, LaIr;->a:LagZ;

    invoke-interface {v0, p1}, LagZ;->d(LaGo;)V

    .line 62
    :try_start_0
    iget-object v0, p0, LaIr;->a:Ladi;

    invoke-interface {v0, p1, p2}, Ladi;->a(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 63
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 64
    if-nez v1, :cond_0

    .line 65
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "No resource found"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_6
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_7

    .line 88
    :catch_0
    move-exception v0

    .line 89
    throw v0

    .line 68
    :cond_0
    :try_start_1
    iget-object v0, p0, LaIr;->a:Lago;

    invoke-virtual {v0, p1, p2}, Lago;->a(LaGo;LacY;)Lagq;

    move-result-object v0

    .line 69
    if-nez v0, :cond_1

    .line 70
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "No download URI"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbxk; {:try_start_1 .. :try_end_1} :catch_1
    .catch LbwO; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4
    .catch LTr; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_6
    .catch LTt; {:try_start_1 .. :try_end_1} :catch_7

    .line 90
    :catch_1
    move-exception v0

    .line 91
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 92
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_1
    :try_start_2
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v3

    .line 75
    if-nez v3, :cond_2

    .line 76
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Unknown file mimetype"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_1
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4
    .catch LTr; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_6
    .catch LTt; {:try_start_2 .. :try_end_2} :catch_7

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 95
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_2
    :try_start_3
    iget-object v0, v0, Lagq;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object v0, p0, LaIr;->a:LagG;

    const/4 v5, 0x0

    move-object v4, p2

    invoke-interface/range {v0 .. v5}, LagG;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LacY;LagH;)V

    .line 82
    iget-object v0, p0, LaIr;->a:LaGM;

    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object p1

    .line 83
    if-nez p1, :cond_3

    .line 84
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "document disappeared"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbxk; {:try_start_3 .. :try_end_3} :catch_1
    .catch LbwO; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_4
    .catch LTr; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_6
    .catch LTt; {:try_start_3 .. :try_end_3} :catch_7

    .line 96
    :catch_3
    move-exception v0

    .line 97
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :catch_4
    move-exception v0

    .line 100
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 101
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :catch_5
    move-exception v0

    .line 103
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 104
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, LTr;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 105
    :catch_6
    move-exception v0

    .line 106
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 107
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 108
    :catch_7
    move-exception v0

    .line 109
    const-string v1, "StorageFileDownloader"

    const-string v2, "failed to download the file"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 110
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, LTt;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    :cond_3
    return-object p1
.end method
