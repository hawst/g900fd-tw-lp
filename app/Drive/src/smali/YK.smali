.class public abstract enum LYK;
.super Ljava/lang/Enum;
.source "PunchTransitionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYK;

.field private static final synthetic a:[LYK;

.field public static final enum b:LYK;


# instance fields
.field private final a:LYN;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, LYL;

    const-string v1, "DISABLED"

    sget-object v2, LYN;->b:LYN;

    invoke-direct {v0, v1, v3, v2}, LYL;-><init>(Ljava/lang/String;ILYN;)V

    sput-object v0, LYK;->a:LYK;

    .line 29
    new-instance v0, LYM;

    const-string v1, "ENABLED"

    sget-object v2, LYN;->a:LYN;

    invoke-direct {v0, v1, v4, v2}, LYM;-><init>(Ljava/lang/String;ILYN;)V

    sput-object v0, LYK;->b:LYK;

    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [LYK;

    sget-object v1, LYK;->a:LYK;

    aput-object v1, v0, v3

    sget-object v1, LYK;->b:LYK;

    aput-object v1, v0, v4

    sput-object v0, LYK;->a:[LYK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILYN;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LYN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, LYK;->a:LYN;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILYN;LYL;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, LYK;-><init>(Ljava/lang/String;ILYN;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYK;
    .locals 1

    .prologue
    .line 19
    const-class v0, LYK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYK;

    return-object v0
.end method

.method public static values()[LYK;
    .locals 1

    .prologue
    .line 19
    sget-object v0, LYK;->a:[LYK;

    invoke-virtual {v0}, [LYK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYK;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;LXz;LYO;Lbxw;LqK;)Landroid/view/View$OnTouchListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LXz;",
            "LYO;",
            "Lbxw",
            "<",
            "LXR;",
            ">;",
            "LqK;",
            ")",
            "Landroid/view/View$OnTouchListener;"
        }
    .end annotation
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, LYK;->a:LYN;

    sget-object v1, LYN;->a:LYN;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
