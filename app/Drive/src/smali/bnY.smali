.class LbnY;
.super Ljava/util/AbstractMap;
.source "MapMakerInternalMap.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:LboK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LboK",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/logging/Logger;

.field static final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final transient a:I

.field final a:J

.field final a:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final a:LbjA;

.field final a:LbnW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnW",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:LboA;

.field final transient a:Lboc;

.field transient a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LbnX",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field transient a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field final transient a:[Lboy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lboy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final transient b:I

.field final b:J

.field final b:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final b:LboA;

.field transient b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    const-class v0, LbnY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LbnY;->a:Ljava/util/logging/Logger;

    .line 586
    new-instance v0, LbnZ;

    invoke-direct {v0}, LbnZ;-><init>()V

    sput-object v0, LbnY;->a:LboK;

    .line 844
    new-instance v0, Lboa;

    invoke-direct {v0}, Lboa;-><init>()V

    sput-object v0, LbnY;->b:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(LbnN;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 195
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 196
    invoke-virtual {p1}, LbnN;->b()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LbnY;->c:I

    .line 198
    invoke-virtual {p1}, LbnN;->a()LboA;

    move-result-object v0

    iput-object v0, p0, LbnY;->a:LboA;

    .line 199
    invoke-virtual {p1}, LbnN;->b()LboA;

    move-result-object v0

    iput-object v0, p0, LbnY;->b:LboA;

    .line 201
    invoke-virtual {p1}, LbnN;->a()LbiD;

    move-result-object v0

    iput-object v0, p0, LbnY;->a:LbiD;

    .line 202
    iget-object v0, p0, LbnY;->b:LboA;

    invoke-virtual {v0}, LboA;->a()LbiD;

    move-result-object v0

    iput-object v0, p0, LbnY;->b:LbiD;

    .line 204
    iget v0, p1, LbnN;->c:I

    iput v0, p0, LbnY;->d:I

    .line 205
    invoke-virtual {p1}, LbnN;->b()J

    move-result-wide v0

    iput-wide v0, p0, LbnY;->a:J

    .line 206
    invoke-virtual {p1}, LbnN;->a()J

    move-result-wide v0

    iput-wide v0, p0, LbnY;->b:J

    .line 208
    iget-object v0, p0, LbnY;->a:LboA;

    invoke-virtual {p0}, LbnY;->b()Z

    move-result v1

    invoke-virtual {p0}, LbnY;->a()Z

    move-result v3

    invoke-static {v0, v1, v3}, Lboc;->a(LboA;ZZ)Lboc;

    move-result-object v0

    iput-object v0, p0, LbnY;->a:Lboc;

    .line 209
    invoke-virtual {p1}, LbnN;->a()LbjA;

    move-result-object v0

    iput-object v0, p0, LbnY;->a:LbjA;

    .line 211
    invoke-virtual {p1}, LbnN;->a()LbnW;

    move-result-object v0

    iput-object v0, p0, LbnY;->a:LbnW;

    .line 212
    iget-object v0, p0, LbnY;->a:LbnW;

    sget-object v1, Lbmp;->a:Lbmp;

    if-ne v0, v1, :cond_2

    invoke-static {}, LbnY;->a()Ljava/util/Queue;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LbnY;->a:Ljava/util/Queue;

    .line 216
    invoke-virtual {p1}, LbnN;->a()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 217
    invoke-virtual {p0}, LbnY;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    iget v1, p0, LbnY;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    move v1, v2

    move v3, v4

    .line 226
    :goto_1
    iget v5, p0, LbnY;->c:I

    if-ge v1, v5, :cond_3

    invoke-virtual {p0}, LbnY;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    mul-int/lit8 v5, v1, 0x2

    iget v6, p0, LbnY;->d:I

    if-gt v5, v6, :cond_3

    .line 227
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 228
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 212
    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_0

    .line 230
    :cond_3
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, LbnY;->b:I

    .line 231
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, LbnY;->a:I

    .line 233
    invoke-virtual {p0, v1}, LbnY;->a(I)[Lboy;

    move-result-object v3

    iput-object v3, p0, LbnY;->a:[Lboy;

    .line 235
    div-int v3, v0, v1

    .line 236
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_8

    .line 237
    add-int/lit8 v0, v3, 0x1

    .line 241
    :goto_2
    if-ge v2, v0, :cond_4

    .line 242
    shl-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 245
    :cond_4
    invoke-virtual {p0}, LbnY;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 247
    iget v0, p0, LbnY;->d:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 248
    iget v3, p0, LbnY;->d:I

    rem-int v1, v3, v1

    .line 249
    :goto_3
    iget-object v3, p0, LbnY;->a:[Lboy;

    array-length v3, v3

    if-ge v4, v3, :cond_7

    .line 250
    if-ne v4, v1, :cond_5

    .line 251
    add-int/lit8 v0, v0, -0x1

    .line 253
    :cond_5
    iget-object v3, p0, LbnY;->a:[Lboy;

    invoke-virtual {p0, v2, v0}, LbnY;->a(II)Lboy;

    move-result-object v5

    aput-object v5, v3, v4

    .line 249
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 256
    :cond_6
    :goto_4
    iget-object v0, p0, LbnY;->a:[Lboy;

    array-length v0, v0

    if-ge v4, v0, :cond_7

    .line 257
    iget-object v0, p0, LbnY;->a:[Lboy;

    const/4 v1, -0x1

    invoke-virtual {p0, v2, v1}, LbnY;->a(II)Lboy;

    move-result-object v1

    aput-object v1, v0, v4

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 260
    :cond_7
    return-void

    :cond_8
    move v0, v3

    goto :goto_2
.end method

.method static a(I)I
    .locals 3

    .prologue
    .line 1721
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 1722
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 1723
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 1724
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 1725
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1726
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static a()LboK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LboK",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 618
    sget-object v0, LbnY;->a:LboK;

    return-object v0
.end method

.method static a()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 841
    sget-object v0, Lbow;->a:Lbow;

    return-object v0
.end method

.method static a()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Queue",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 875
    sget-object v0, LbnY;->b:Ljava/util/Queue;

    return-object v0
.end method

.method static a(Lbox;Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1837
    invoke-interface {p0, p1}, Lbox;->a(Lbox;)V

    .line 1838
    invoke-interface {p1, p0}, Lbox;->b(Lbox;)V

    .line 1839
    return-void
.end method

.method static b(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1843
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    .line 1844
    invoke-interface {p0, v0}, Lbox;->a(Lbox;)V

    .line 1845
    invoke-interface {p0, v0}, Lbox;->b(Lbox;)V

    .line 1846
    return-void
.end method

.method static b(Lbox;Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1869
    invoke-interface {p0, p1}, Lbox;->c(Lbox;)V

    .line 1870
    invoke-interface {p1, p0}, Lbox;->d(Lbox;)V

    .line 1871
    return-void
.end method

.method static c(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1875
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    .line 1876
    invoke-interface {p0, v0}, Lbox;->c(Lbox;)V

    .line 1877
    invoke-interface {p0, v0}, Lbox;->d(Lbox;)V

    .line 1878
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1759
    iget-object v0, p0, LbnY;->a:LbiD;

    invoke-virtual {v0, p1}, LbiD;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1760
    invoke-static {v0}, LbnY;->a(I)I

    move-result v0

    return v0
.end method

.method a(I)Lboy;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lboy",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1791
    iget-object v0, p0, LbnY;->a:[Lboy;

    iget v1, p0, LbnY;->b:I

    ushr-int v1, p1, v1

    iget v2, p0, LbnY;->a:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method a(II)Lboy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lboy",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1795
    new-instance v0, Lboy;

    invoke-direct {v0, p0, p1, p2}, Lboy;-><init>(LbnY;II)V

    return-object v0
.end method

.method a(Lbox;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1804
    invoke-interface {p1}, Lbox;->a()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1815
    :cond_0
    :goto_0
    return-object v0

    .line 1807
    :cond_1
    invoke-interface {p1}, Lbox;->a()LboK;

    move-result-object v1

    invoke-interface {v1}, LboK;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1808
    if-eqz v1, :cond_0

    .line 1812
    invoke-virtual {p0}, LbnY;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p1}, LbnY;->a(Lbox;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 1815
    goto :goto_0
.end method

.method a()V
    .locals 4

    .prologue
    .line 1857
    :goto_0
    iget-object v0, p0, LbnY;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbnX;

    if-eqz v0, :cond_0

    .line 1859
    :try_start_0
    iget-object v1, p0, LbnY;->a:LbnW;

    invoke-interface {v1, v0}, LbnW;->a(LbnX;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1860
    :catch_0
    move-exception v0

    .line 1861
    sget-object v1, LbnY;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1864
    :cond_0
    return-void
.end method

.method a(LboK;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LboK",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1764
    invoke-interface {p1}, LboK;->a()Lbox;

    move-result-object v0

    .line 1765
    invoke-interface {v0}, Lbox;->a()I

    move-result v1

    .line 1766
    invoke-virtual {p0, v1}, LbnY;->a(I)Lboy;

    move-result-object v2

    invoke-interface {v0}, Lbox;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lboy;->a(Ljava/lang/Object;ILboK;)Z

    .line 1767
    return-void
.end method

.method a(Lbox;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1770
    invoke-interface {p1}, Lbox;->a()I

    move-result v0

    .line 1771
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lboy;->a(Lbox;I)Z

    .line 1772
    return-void
.end method

.method a()Z
    .locals 2

    .prologue
    .line 263
    iget v0, p0, LbnY;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lbox;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 1824
    iget-object v0, p0, LbnY;->a:LbjA;

    invoke-virtual {v0}, LbjA;->a()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, LbnY;->a(Lbox;J)Z

    move-result v0

    return v0
.end method

.method a(Lbox;J)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    .line 1832
    invoke-interface {p1}, Lbox;->a()J

    move-result-wide v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(I)[Lboy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lboy",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1882
    new-array v0, p1, [Lboy;

    return-object v0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, LbnY;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LbnY;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 4

    .prologue
    .line 271
    iget-wide v0, p0, LbnY;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 3453
    iget-object v1, p0, LbnY;->a:[Lboy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3454
    invoke-virtual {v3}, Lboy;->l()V

    .line 3453
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3456
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3349
    if-nez p1, :cond_0

    .line 3350
    const/4 v0, 0x0

    .line 3353
    :goto_0
    return v0

    .line 3352
    :cond_0
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3353
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lboy;->a(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    .line 3358
    if-nez p1, :cond_0

    .line 3359
    const/4 v0, 0x0

    .line 3392
    :goto_0
    return v0

    .line 3367
    :cond_0
    iget-object v7, p0, LbnY;->a:[Lboy;

    .line 3368
    const-wide/16 v4, -0x1

    .line 3369
    const/4 v0, 0x0

    move v6, v0

    move-wide v8, v4

    :goto_1
    const/4 v0, 0x3

    if-ge v6, v0, :cond_5

    .line 3370
    const-wide/16 v2, 0x0

    .line 3371
    array-length v10, v7

    const/4 v0, 0x0

    move-wide v4, v2

    move v2, v0

    :goto_2
    if-ge v2, v10, :cond_4

    aget-object v3, v7, v2

    .line 3374
    iget v0, v3, Lboy;->a:I

    .line 3376
    iget-object v11, v3, Lboy;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3377
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3378
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbox;

    :goto_4
    if-eqz v0, :cond_2

    .line 3379
    invoke-virtual {v3, v0}, Lboy;->a(Lbox;)Ljava/lang/Object;

    move-result-object v12

    .line 3380
    if-eqz v12, :cond_1

    iget-object v13, p0, LbnY;->b:LbiD;

    invoke-virtual {v13, p1, v12}, LbiD;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 3381
    const/4 v0, 0x1

    goto :goto_0

    .line 3378
    :cond_1
    invoke-interface {v0}, Lbox;->a()Lbox;

    move-result-object v0

    goto :goto_4

    .line 3377
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 3385
    :cond_3
    iget v0, v3, Lboy;->b:I

    int-to-long v0, v0

    add-long/2addr v4, v0

    .line 3371
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3387
    :cond_4
    cmp-long v0, v4, v8

    if-nez v0, :cond_6

    .line 3392
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 3369
    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-wide v8, v4

    goto :goto_1
.end method

.method d()Z
    .locals 4

    .prologue
    .line 275
    iget-wide v0, p0, LbnY;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, LbnY;->a:LboA;

    sget-object v1, LboA;->a:LboA;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3478
    iget-object v0, p0, LbnY;->b:Ljava/util/Set;

    .line 3479
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbom;

    invoke-direct {v0, p0}, Lbom;-><init>(LbnY;)V

    iput-object v0, p0, LbnY;->b:Ljava/util/Set;

    goto :goto_0
.end method

.method f()Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, LbnY;->b:LboA;

    sget-object v1, LboA;->a:LboA;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3328
    if-nez p1, :cond_0

    .line 3329
    const/4 v0, 0x0

    .line 3332
    :goto_0
    return-object v0

    .line 3331
    :cond_0
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3332
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lboy;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3293
    .line 3294
    iget-object v6, p0, LbnY;->a:[Lboy;

    move v0, v1

    move-wide v2, v4

    .line 3295
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 3296
    aget-object v7, v6, v0

    iget v7, v7, Lboy;->a:I

    if-eqz v7, :cond_1

    .line 3313
    :cond_0
    :goto_1
    return v1

    .line 3299
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, Lboy;->b:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3295
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3302
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 3303
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 3304
    aget-object v7, v6, v0

    iget v7, v7, Lboy;->a:I

    if-nez v7, :cond_0

    .line 3307
    aget-object v7, v6, v0

    iget v7, v7, Lboy;->b:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3303
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3309
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 3313
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3462
    iget-object v0, p0, LbnY;->a:Ljava/util/Set;

    .line 3463
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbov;

    invoke-direct {v0, p0}, Lbov;-><init>(LbnY;)V

    iput-object v0, p0, LbnY;->a:Ljava/util/Set;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3397
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3398
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3399
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3400
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lboy;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3412
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LbnY;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3415
    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3404
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3405
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3406
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3407
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lboy;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 3419
    if-nez p1, :cond_0

    .line 3420
    const/4 v0, 0x0

    .line 3423
    :goto_0
    return-object v0

    .line 3422
    :cond_0
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3423
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lboy;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3427
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3428
    :cond_0
    const/4 v0, 0x0

    .line 3431
    :goto_0
    return v0

    .line 3430
    :cond_1
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3431
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lboy;->a(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 3445
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3446
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3447
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3448
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lboy;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 3435
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3436
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3437
    if-nez p2, :cond_0

    .line 3438
    const/4 v0, 0x0

    .line 3441
    :goto_0
    return v0

    .line 3440
    :cond_0
    invoke-virtual {p0, p1}, LbnY;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3441
    invoke-virtual {p0, v0}, LbnY;->a(I)Lboy;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lboy;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 6

    .prologue
    .line 3318
    iget-object v1, p0, LbnY;->a:[Lboy;

    .line 3319
    const-wide/16 v2, 0x0

    .line 3320
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 3321
    aget-object v4, v1, v0

    iget v4, v4, Lboy;->a:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3323
    :cond_0
    invoke-static {v2, v3}, Lbsy;->b(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3470
    iget-object v0, p0, LbnY;->a:Ljava/util/Collection;

    .line 3471
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LboL;

    invoke-direct {v0, p0}, LboL;-><init>(LbnY;)V

    iput-object v0, p0, LbnY;->a:Ljava/util/Collection;

    goto :goto_0
.end method
