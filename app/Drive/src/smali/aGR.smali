.class public LaGR;
.super Ljava/lang/Object;
.source "ModelLoaderAsyncTaskRunner.java"


# instance fields
.field protected final a:LaGM;

.field private final a:LaGO;

.field protected final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;Laja;LaGO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaGO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, LaGR;->a:LaGM;

    .line 90
    iput-object p2, p0, LaGR;->a:Laja;

    .line 91
    iput-object p3, p0, LaGR;->a:LaGO;

    .line 92
    return-void
.end method

.method private a(LaGN;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaGN",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 96
    invoke-static {}, LamV;->a()V

    .line 97
    iget-object v0, p0, LaGR;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 98
    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 99
    const-string v0, "ModelLoaderAsyncTask"

    const-string v1, "No activity to invoke the async task. The task will be ignored: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 119
    :goto_0
    return-void

    :cond_0
    move-object v2, v0

    .line 103
    check-cast v2, Landroid/app/Activity;

    .line 104
    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const-string v0, "ModelLoaderAsyncTask"

    const-string v1, "Not calling task on finishing activity: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 109
    :cond_1
    if-eqz p2, :cond_2

    .line 110
    iget-object v0, p0, LaGR;->a:LaGO;

    invoke-virtual {v0, v2, p1}, LaGO;->a(Landroid/app/Activity;LaGN;)V

    .line 113
    :cond_2
    new-instance v0, LaGS;

    iget-object v4, p0, LaGR;->a:LaGO;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, LaGS;-><init>(LaGR;Landroid/app/Activity;LaGN;LaGO;Z)V

    new-array v1, v6, [Ljava/lang/Void;

    .line 117
    invoke-virtual {v0, v1}, LaGS;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public a(LaGN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaGN",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LaGR;->a(LaGN;Z)V

    .line 128
    return-void
.end method

.method public b(LaGN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaGN",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaGR;->a(LaGN;Z)V

    .line 136
    return-void
.end method
