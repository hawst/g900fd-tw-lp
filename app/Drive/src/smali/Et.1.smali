.class public LEt;
.super Ljava/lang/Object;
.source "GoogleDocsEntryCreator.java"


# instance fields
.field private final a:LTT;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LEs;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method constructor <init>(LtK;LTT;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LEt;->a:LtK;

    .line 42
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTT;

    iput-object v0, p0, LEt;->a:LTT;

    .line 44
    new-instance v0, LbmH;

    invoke-direct {v0}, LbmH;-><init>()V

    .line 47
    sget-object v1, LNl;->a:LNl;

    sget v2, Lxi;->create_new_folder:I

    invoke-direct {p0, v1, v2}, LEt;->a(LNl;I)LEs;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 48
    sget-object v1, LNl;->b:LNl;

    sget v2, Lxi;->create_new_kix_doc:I

    invoke-direct {p0, v1, v2}, LEt;->a(LNl;I)LEs;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 49
    sget-object v1, LNl;->e:LNl;

    sget v2, Lxi;->create_new_trix_doc:I

    invoke-direct {p0, v1, v2}, LEt;->a(LNl;I)LEs;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 50
    sget-object v1, LNl;->d:LNl;

    sget v2, Lxi;->create_new_punch_doc:I

    invoke-direct {p0, v1, v2}, LEt;->a(LNl;I)LEs;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 51
    sget-object v1, LNl;->c:LNl;

    sget v2, Lxi;->create_new_drawing_doc:I

    invoke-direct {p0, v1, v2}, LEt;->a(LNl;I)LEs;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 53
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LEt;->a:Ljava/util/List;

    .line 54
    return-void
.end method

.method private a(LNl;I)LEs;
    .locals 3

    .prologue
    .line 57
    new-instance v0, LEs;

    iget-object v1, p0, LEt;->a:LtK;

    iget-object v2, p0, LEt;->a:LTT;

    invoke-direct {v0, v1, v2, p1, p2}, LEs;-><init>(LtK;LTT;LNl;I)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LEs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, LEt;->a:Ljava/util/List;

    return-object v0
.end method
