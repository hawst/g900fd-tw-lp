.class final enum LaxR;
.super Ljava/lang/Enum;
.source "Priority.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaxR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaxR;

.field private static final synthetic a:[LaxR;

.field public static final enum b:LaxR;

.field public static final enum c:LaxR;

.field public static final enum d:LaxR;

.field public static final enum e:LaxR;

.field public static final enum f:LaxR;

.field public static final enum g:LaxR;

.field public static final enum h:LaxR;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, LaxR;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v3}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->a:LaxR;

    .line 6
    new-instance v0, LaxR;

    const-string v1, "DIMENSIONS"

    invoke-direct {v0, v1, v4}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->b:LaxR;

    .line 7
    new-instance v0, LaxR;

    const-string v1, "BITMAP"

    invoke-direct {v0, v1, v5}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->c:LaxR;

    .line 8
    new-instance v0, LaxR;

    const-string v1, "BITMAP_TILE"

    invoke-direct {v0, v1, v6}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->d:LaxR;

    .line 9
    new-instance v0, LaxR;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v7}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->e:LaxR;

    .line 10
    new-instance v0, LaxR;

    const-string v1, "SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->f:LaxR;

    .line 11
    new-instance v0, LaxR;

    const-string v1, "TEXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->g:LaxR;

    .line 12
    new-instance v0, LaxR;

    const-string v1, "FINALIZE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LaxR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaxR;->h:LaxR;

    .line 4
    const/16 v0, 0x8

    new-array v0, v0, [LaxR;

    sget-object v1, LaxR;->a:LaxR;

    aput-object v1, v0, v3

    sget-object v1, LaxR;->b:LaxR;

    aput-object v1, v0, v4

    sget-object v1, LaxR;->c:LaxR;

    aput-object v1, v0, v5

    sget-object v1, LaxR;->d:LaxR;

    aput-object v1, v0, v6

    sget-object v1, LaxR;->e:LaxR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LaxR;->f:LaxR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaxR;->g:LaxR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaxR;->h:LaxR;

    aput-object v2, v0, v1

    sput-object v0, LaxR;->a:[LaxR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaxR;
    .locals 1

    .prologue
    .line 4
    const-class v0, LaxR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaxR;

    return-object v0
.end method

.method public static values()[LaxR;
    .locals 1

    .prologue
    .line 4
    sget-object v0, LaxR;->a:[LaxR;

    invoke-virtual {v0}, [LaxR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaxR;

    return-object v0
.end method
