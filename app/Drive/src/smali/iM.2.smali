.class public LiM;
.super Landroid/view/ActionMode;
.source "SupportActionModeWrapper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field final a:Landroid/view/MenuInflater;

.field final a:Lmu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmu;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V

    .line 44
    iput-object p2, p0, LiM;->a:Lmu;

    .line 45
    new-instance v0, LiO;

    invoke-direct {v0, p1}, LiO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LiM;->a:Landroid/view/MenuInflater;

    .line 46
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()V

    .line 76
    return-void
.end method

.method public getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-static {v0}, Lju;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LiM;->a:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleOptionalHint()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->c()Z

    move-result v0

    return v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->b()V

    .line 71
    return-void
.end method

.method public isTitleOptional()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0}, Lmu;->b()Z

    move-result v0

    return v0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(Landroid/view/View;)V

    .line 111
    return-void
.end method

.method public setSubtitle(I)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->b(I)V

    .line 101
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(I)V

    .line 91
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->b(Ljava/lang/CharSequence;)V

    .line 61
    return-void
.end method

.method public setTitleOptionalHint(Z)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, LiM;->a:Lmu;

    invoke-virtual {v0, p1}, Lmu;->a(Z)V

    .line 126
    return-void
.end method
