.class public LHa;
.super Ljava/lang/Object;
.source "DriveAppAuthorizerImpl.java"

# interfaces
.implements LGZ;


# instance fields
.field private final a:Laeo;


# direct methods
.method public constructor <init>(Laeo;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, LHa;->a:Laeo;

    .line 31
    return-void
.end method

.method static synthetic a(I)LFV;
    .locals 1

    .prologue
    .line 24
    invoke-static {p0}, LHa;->b(I)LFV;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)LFV;
    .locals 1

    .prologue
    .line 34
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 35
    packed-switch p0, :pswitch_data_0

    .line 43
    sget-object v0, LFV;->h:LFV;

    :goto_1
    return-object v0

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 37
    :pswitch_0
    sget-object v0, LFV;->g:LFV;

    goto :goto_1

    .line 39
    :pswitch_1
    sget-object v0, LFV;->g:LFV;

    goto :goto_1

    .line 41
    :pswitch_2
    sget-object v0, LFV;->f:LFV;

    goto :goto_1

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(LFT;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v1, LHb;

    invoke-direct {v1, p0, p1}, LHb;-><init>(LHa;LFT;)V

    .line 64
    iget-object v2, p0, LHa;->a:Laeo;

    .line 65
    invoke-interface {v2, v1}, Laeo;->a(LaHy;)Laen;

    move-result-object v1

    .line 68
    invoke-interface {v1, p3}, Laen;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaJT;

    move-result-object v2

    .line 69
    if-nez v2, :cond_0

    .line 82
    :goto_0
    return-object v0

    .line 73
    :cond_0
    invoke-interface {v2, p2}, LaJT;->b(Ljava/lang/String;)V

    .line 74
    iget-object v3, p3, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-interface {v1, v2, v3}, Laen;->a(LaJT;LaFO;)LaJT;

    move-result-object v1

    .line 75
    if-nez v1, :cond_1

    .line 76
    sget-object v1, LFV;->h:LFV;

    invoke-interface {p1, v1, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 80
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://schemas.google.com/docs/2007#open-with-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-interface {v1, v0}, LaJT;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
