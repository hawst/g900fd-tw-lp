.class public LbrM;
.super Ljava/lang/Object;
.source "GellyConstantBindingBuilder.java"

# interfaces
.implements LbuR;
.implements LbuS;


# instance fields
.field private final a:LbrP;

.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/annotation/Annotation;


# direct methods
.method constructor <init>(LbrP;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbrP;

    iput-object v0, p0, LbrM;->a:LbrP;

    .line 22
    return-void
.end method

.method private a(Ljava/lang/Class;)Lbuv;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, LbrM;->a:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "unexpected: annotationType (%s) and annotation (%s) are both non-null"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LbrM;->a:Ljava/lang/Class;

    aput-object v5, v4, v1

    iget-object v1, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    aput-object v1, v4, v2

    invoke-static {v0, v3, v4}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, LbrM;->a:Ljava/lang/Class;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, LbrM;->a:Ljava/lang/Class;

    invoke-static {p1, v0}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 108
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 99
    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    if-eqz v0, :cond_3

    .line 106
    iget-object v0, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    invoke-static {p1, v0}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    goto :goto_1

    .line 108
    :cond_3
    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    goto :goto_1
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    iget-object v0, p0, LbrM;->a:Ljava/lang/Class;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "already annotated : %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, LbrM;->a:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "already annotated : %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LbrM;->a:Ljava/lang/annotation/Annotation;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 96
    return-void

    :cond_0
    move v0, v2

    .line 94
    goto :goto_0

    :cond_1
    move v0, v2

    .line 95
    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LbuS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbuS;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, LbrM;->a()V

    .line 27
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LbrM;->a:Ljava/lang/Class;

    .line 28
    return-object p0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, LbrM;->a:LbrP;

    const-class v1, Ljava/lang/String;

    invoke-direct {p0, v1}, LbrM;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LbrP;->a(Lbuv;Ljava/lang/Object;)V

    .line 41
    return-void
.end method
