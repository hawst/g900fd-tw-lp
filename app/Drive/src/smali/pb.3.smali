.class Lpb;
.super Ljava/lang/Object;
.source "Archives.java"


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;


# instance fields
.field private final a:Lph;

.field private final a:Lpi;

.field private final a:Lpw;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d, y k:m:s.S"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpb;->a:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method constructor <init>(Lph;Lpi;Lpw;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lpb;->a:Lph;

    .line 40
    iput-object p2, p0, Lpb;->a:Lpi;

    .line 41
    iput-object p3, p0, Lpb;->a:Lpw;

    .line 42
    return-void
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 130
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lpb;->a:Lph;

    invoke-virtual {v1}, Lph;->a()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0, v0}, Lpb;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    invoke-virtual {p0, p1}, Lpb;->a(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-object v0
.end method

.method a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget-object v0, p0, Lpb;->a:Lpi;

    invoke-interface {v0}, Lpi;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 54
    invoke-virtual {p0, v0}, Lpb;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_0
    return-object v1
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lpb;->a:Lph;

    invoke-virtual {v0, p1}, Lph;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 115
    const/4 v1, 0x0

    .line 117
    :try_start_0
    iget-object v0, p0, Lpb;->a:Lph;

    invoke-virtual {v0, p1}, Lph;->a(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    .line 118
    invoke-static {v2, v1}, Lbfn;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    invoke-direct {p0, v2}, Lpb;->a(Ljava/io/Closeable;)V

    .line 121
    invoke-direct {p0, v1}, Lpb;->a(Ljava/io/Closeable;)V

    .line 123
    return-void

    .line 120
    :catchall_0
    move-exception v0

    invoke-direct {p0, v2}, Lpb;->a(Ljava/io/Closeable;)V

    .line 121
    invoke-direct {p0, v1}, Lpb;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method a(Ljava/io/File;)Z
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    iget-object v2, p0, Lpb;->a:Lph;

    invoke-virtual {v2}, Lph;->a()Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 88
    if-nez v2, :cond_0

    .line 89
    iget-object v0, p0, Lpb;->a:Lpw;

    const-string v2, "Unable to get package info."

    invoke-virtual {v0, v2}, Lpw;->a(Ljava/lang/String;)V

    .line 101
    :goto_0
    return v1

    .line 94
    :cond_0
    :try_start_0
    iget-object v3, p0, Lpb;->a:Lpw;

    const-string v4, "Checking if extracted archive %s is up to date.\nExtracted file last modified: %s\nPackage last updated: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lpb;->a:Ljava/text/SimpleDateFormat;

    new-instance v8, Ljava/util/Date;

    .line 96
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Lpb;->a:Ljava/text/SimpleDateFormat;

    new-instance v8, Ljava/util/Date;

    iget-wide v10, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 97
    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 94
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lpw;->a(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    cmp-long v2, v4, v2

    if-ltz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 99
    :catch_0
    move-exception v0

    goto :goto_0
.end method
