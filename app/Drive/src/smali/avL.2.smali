.class public final LavL;
.super Ljava/lang/Object;
.source "SelectionBoundary.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    const/4 v2, 0x4

    new-array v2, v2, [I

    .line 61
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readIntArray([I)V

    .line 62
    new-instance v3, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    aget v4, v2, v1

    aget v5, v2, v0

    const/4 v6, 0x2

    aget v6, v2, v6

    const/4 v7, 0x3

    aget v2, v2, v7

    if-lez v2, :cond_0

    :goto_0
    invoke-direct {v3, v4, v5, v6, v0}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;-><init>(IIIZ)V

    return-object v3

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(I)[Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
    .locals 1

    .prologue
    .line 67
    new-array v0, p1, [Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, LavL;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, LavL;->a(I)[Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    return-object v0
.end method
