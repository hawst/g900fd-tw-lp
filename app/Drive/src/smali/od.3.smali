.class public Lod;
.super Ljava/lang/Object;
.source "GAServiceManager.java"

# interfaces
.implements LoO;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static a:Lod;


# instance fields
.field private a:I

.field private a:Landroid/content/Context;

.field private a:Landroid/os/Handler;

.field private a:LnU;

.field private a:LnV;

.field private volatile a:LnW;

.field private a:Loc;

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lod;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/16 v0, 0x708

    iput v0, p0, Lod;->a:I

    .line 45
    iput-boolean v1, p0, Lod;->a:Z

    .line 48
    iput-boolean v1, p0, Lod;->b:Z

    .line 52
    iput-boolean v1, p0, Lod;->c:Z

    .line 54
    new-instance v0, Loe;

    invoke-direct {v0, p0}, Loe;-><init>(Lod;)V

    iput-object v0, p0, Lod;->a:LnV;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lod;->d:Z

    .line 80
    return-void
.end method

.method static synthetic a(Lod;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lod;->a:I

    return v0
.end method

.method static synthetic a(Lod;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lod;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public static a()Lod;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lod;->a:Lod;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lod;

    invoke-direct {v0}, Lod;-><init>()V

    sput-object v0, Lod;->a:Lod;

    .line 76
    :cond_0
    sget-object v0, Lod;->a:Lod;

    return-object v0
.end method

.method static synthetic a(Lod;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lod;->b:Z

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Loc;

    invoke-direct {v0, p0}, Loc;-><init>(LoO;)V

    iput-object v0, p0, Lod;->a:Loc;

    .line 93
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 94
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lod;->a:Landroid/content/Context;

    iget-object v2, p0, Lod;->a:Loc;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    return-void
.end method

.method static synthetic b(Lod;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lod;->d:Z

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 99
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lod;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lof;

    invoke-direct {v2, p0}, Lof;-><init>(Lod;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lod;->a:Landroid/os/Handler;

    .line 117
    iget v0, p0, Lod;->a:I

    if-lez v0, :cond_0

    .line 118
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    iget-object v1, p0, Lod;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lod;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lod;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method declared-synchronized a()LnU;
    .locals 3

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lod;->a:LnU;

    if-nez v0, :cond_1

    .line 156
    iget-object v0, p0, Lod;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_0
    :try_start_1
    new-instance v0, LoK;

    iget-object v1, p0, Lod;->a:LnV;

    iget-object v2, p0, Lod;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, LoK;-><init>(LnV;Landroid/content/Context;)V

    iput-object v0, p0, Lod;->a:LnU;

    .line 163
    :cond_1
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 165
    invoke-direct {p0}, Lod;->c()V

    .line 167
    :cond_2
    iget-object v0, p0, Lod;->a:Loc;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lod;->c:Z

    if-eqz v0, :cond_3

    .line 168
    invoke-direct {p0}, Lod;->b()V

    .line 170
    :cond_3
    iget-object v0, p0, Lod;->a:LnU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lod;->a:LnW;

    if-nez v0, :cond_0

    .line 178
    const-string v0, "dispatch call queued.  Need to call GAServiceManager.getInstance().initialize()."

    invoke-static {v0}, LoD;->h(Ljava/lang/String;)I

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lod;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :cond_0
    :try_start_1
    invoke-static {}, Lou;->a()Lou;

    move-result-object v0

    sget-object v1, Lov;->S:Lov;

    invoke-virtual {v0, v1}, Lou;->a(Lov;)V

    .line 184
    iget-object v0, p0, Lod;->a:LnW;

    invoke-interface {v0}, LnW;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 190
    const-string v0, "Need to call initialize() and be in fallback mode to start dispatch."

    invoke-static {v0}, LoD;->h(Ljava/lang/String;)I

    .line 191
    iput p1, p0, Lod;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 195
    :cond_1
    :try_start_1
    invoke-static {}, Lou;->a()Lou;

    move-result-object v0

    sget-object v1, Lov;->T:Lov;

    invoke-virtual {v0, v1}, Lou;->a(Lov;)V

    .line 197
    iget-boolean v0, p0, Lod;->d:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lod;->b:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lod;->a:I

    if-lez v0, :cond_2

    .line 198
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lod;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 200
    :cond_2
    iput p1, p0, Lod;->a:I

    .line 201
    if-lez p1, :cond_0

    iget-boolean v0, p0, Lod;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lod;->b:Z

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    iget-object v1, p0, Lod;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lod;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Landroid/content/Context;LnW;)V
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lod;->a:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 135
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lod;->a:Landroid/content/Context;

    .line 137
    iget-object v0, p0, Lod;->a:LnW;

    if-nez v0, :cond_0

    .line 138
    iput-object p2, p0, Lod;->a:LnW;

    .line 139
    iget-boolean v0, p0, Lod;->a:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p2}, LnW;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lod;->d:Z

    invoke-virtual {p0, v0, p1}, Lod;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(ZZ)V
    .locals 4

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lod;->d:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lod;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-void

    .line 212
    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget v0, p0, Lod;->a:I

    if-lez v0, :cond_2

    .line 213
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lod;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 215
    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget v0, p0, Lod;->a:I

    if-lez v0, :cond_3

    .line 216
    iget-object v0, p0, Lod;->a:Landroid/os/Handler;

    iget-object v1, p0, Lod;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lod;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lod;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 219
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PowerSaveMode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_4

    if-nez p2, :cond_5

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LoD;->e(Ljava/lang/String;)I

    .line 221
    iput-boolean p1, p0, Lod;->d:Z

    .line 222
    iput-boolean p2, p0, Lod;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 219
    :cond_5
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
