.class public Laes;
.super Ljava/lang/Object;
.source "DocsListFeedClientImpl.java"

# interfaces
.implements Laer;
.implements Laeu;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LTd;

.field private a:Laet;

.field private final a:Laev;

.field private final a:LbwT;


# direct methods
.method public constructor <init>(LbwT;Laev;LTd;)V
    .locals 1
    .param p1    # LbwT;
        .annotation runtime Lbxv;
            a = "DocFeed"
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbwT;

    iput-object v0, p0, Laes;->a:LbwT;

    .line 50
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laev;

    iput-object v0, p0, Laes;->a:Laev;

    .line 51
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iput-object v0, p0, Laes;->a:LTd;

    .line 52
    return-void
.end method

.method private a(Ljava/lang/String;LaFO;)LaeF;
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Laes;->b(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeF;
    :try_end_0
    .catch Lbxa; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private a(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeF;
    .locals 1

    .prologue
    .line 87
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Laes;->b(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeF;
    :try_end_0
    .catch Lbxa; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a()Laet;
    .locals 4

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laes;->a:Laet;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Laet;

    iget-object v1, p0, Laes;->a:LbwT;

    iget-object v2, p0, Laes;->a:Laev;

    iget-object v3, p0, Laes;->a:LTd;

    invoke-direct {v0, v1, v2, v3}, Laet;-><init>(LbwT;Laev;LTd;)V

    iput-object v0, p0, Laes;->a:Laet;

    .line 58
    :cond_0
    iget-object v0, p0, Laes;->a:Laet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    const-string v0, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 141
    const-string v1, "root"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "folder:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 148
    :goto_0
    const-string v1, "showdeleted=true&showroot=true"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 144
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "document:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeF;
    .locals 4

    .prologue
    .line 102
    .line 103
    invoke-direct {p0}, Laes;->a()Laet;

    move-result-object v0

    const-class v1, LaeF;

    invoke-virtual {v0, v1, p1, p2, p3}, Laet;->a(Ljava/lang/Class;Ljava/lang/String;LaFO;Ljava/lang/String;)Lbxb;

    move-result-object v0

    .line 104
    instance-of v1, v0, LaeF;

    if-eqz v1, :cond_0

    .line 105
    check-cast v0, LaeF;

    return-object v0

    .line 107
    :cond_0
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected DocEntry, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Laes;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    .line 72
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-direct {p0, v0, v1}, Laes;->a(Ljava/lang/String;LaFO;)LaeF;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)LaeF;
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Laes;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    .line 97
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-direct {p0, v0, v1, p2}, Laes;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)LaeF;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbxb;LaFO;)Lbxb;
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Laes;->a()Laet;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Laet;->a(Lbxb;LaFO;)Lbxb;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;LaFO;Lbxb;)Lbxb;
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Laes;->a()Laet;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Laet;->a(Ljava/lang/String;LaFO;Lbxb;)Lbxb;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;LaFO;Ljava/lang/String;)Lbxj;
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Laes;->a()Laet;

    move-result-object v0

    const-class v1, LaeF;

    invoke-virtual {v0, v1, p1, p2, p3}, Laet;->a(Ljava/lang/Class;Ljava/lang/String;LaFO;Ljava/lang/String;)Lbxj;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Laes;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    .line 116
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-direct {p0, v0, v1}, Laes;->a(Ljava/lang/String;LaFO;)LaeF;

    move-result-object v0

    .line 117
    invoke-interface {v0}, LaJT;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;LaFO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Laes;->a()Laet;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Laet;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Laes;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
