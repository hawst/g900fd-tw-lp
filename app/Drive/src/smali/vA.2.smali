.class public LvA;
.super Ljava/lang/Object;
.source "VersionCheckResourcesModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method provideMarketUpgradeFlagString()Ljava/lang/String;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "marketFlag"
    .end annotation

    .prologue
    .line 32
    const-string v0, "upgradeUrl"

    return-object v0
.end method

.method provideTooOldCloseResourceInteger()Ljava/lang/Integer;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "tooOldClose"
    .end annotation

    .prologue
    .line 39
    sget v0, Lxi;->version_too_old_close:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method provideTooOldMessageResourceInteger()Ljava/lang/Integer;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "tooOldMessage"
    .end annotation

    .prologue
    .line 46
    sget v0, Lxi;->version_too_old:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method provideTooOldTitleResourceInteger()Ljava/lang/Integer;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "tooOldTitle"
    .end annotation

    .prologue
    .line 53
    sget v0, Lxi;->version_too_old_title:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method provideTooOldUpgradeResourceInteger()Ljava/lang/Integer;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "tooOldUpgrade"
    .end annotation

    .prologue
    .line 60
    sget v0, Lxi;->version_too_old_upgrade:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method provideVersionFlagString()Ljava/lang/String;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "versionFlag"
    .end annotation

    .prologue
    .line 25
    const-string v0, "minVersion"

    return-object v0
.end method
