.class LasT;
.super LasU;
.source "StoryShops.java"


# instance fields
.field private final a:Ladj;

.field final synthetic a:LasR;

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LasR;Ladj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 177
    iput-object p1, p0, LasT;->a:LasR;

    .line 178
    const-string v0, ""

    invoke-direct {p0, p1, v0, p3}, LasU;-><init>(LasR;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LasT;->a:Ljava/lang/String;

    .line 180
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    iput-object v0, p0, LasT;->a:Ladj;

    .line 181
    invoke-direct {p0}, LasT;->b()V

    .line 182
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 185
    iget-object v0, p0, LasT;->a:Ladj;

    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 188
    :try_start_0
    new-instance v2, Laee;

    iget-object v3, p0, LasT;->a:LasR;

    invoke-static {v3}, LasR;->a(LasR;)Lalo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Laee;-><init>(Lalo;Ljava/io/File;Z)V

    .line 189
    iget-object v0, p0, LasT;->a:LasR;

    invoke-static {v0}, LasR;->a(LasR;)Laeb;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, LadD;->a(Laeb;LadJ;Z)LadA;

    move-result-object v0

    .line 190
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(LadA;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LasT;->a:Landroid/net/Uri;

    .line 191
    iget-object v0, p0, LasT;->a:Landroid/net/Uri;

    iget-object v2, p0, LasT;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LasT;->b:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    const-string v2, "CachedStoryShop"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while registering the archive file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 199
    iget-object v0, p0, LasT;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    const-string v0, "CachedStoryShop"

    const-string v1, "Unregistering announce: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LasT;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 201
    iget-object v0, p0, LasT;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)V

    .line 204
    :cond_0
    iget-object v0, p0, LasT;->a:Ladj;

    invoke-interface {v0}, Ladj;->close()V

    .line 205
    return-void
.end method
