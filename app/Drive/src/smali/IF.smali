.class LIF;
.super Ljava/lang/Object;
.source "QuotaRangeSectionIndexerFactory.java"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LIF;->a:Landroid/content/Context;

    .line 26
    return-void
.end method

.method private static a(Ljava/lang/String;ZJ)LIO;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-nez p1, :cond_0

    move v0, v1

    .line 46
    :goto_0
    neg-long v4, p2

    .line 47
    new-instance v3, LIO;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v6}, LIL;->a([Ljava/lang/Object;)LIL;

    move-result-object v0

    invoke-direct {v3, p0, v0}, LIO;-><init>(Ljava/lang/String;LIL;)V

    return-object v3

    :cond_0
    move v0, v2

    .line 45
    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, LIF;->a:Landroid/content/Context;

    sget v1, Lxi;->fast_scroll_title_grouper_collections:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()[LIO;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 33
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 35
    invoke-direct {p0}, LIF;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v1, v2, v4, v5}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    const-string v1, "GB"

    const-wide v2, 0x10000000000L

    invoke-static {v1, v6, v2, v3}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    const-string v1, "MB"

    const-wide/32 v2, 0x40000000

    invoke-static {v1, v6, v2, v3}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    const-string v1, "KB"

    const-wide/32 v2, 0x100000

    invoke-static {v1, v6, v2, v3}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    const-string v1, "B"

    const-wide/16 v2, 0x400

    invoke-static {v1, v6, v2, v3}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    const-string v1, "----"

    const-wide/16 v2, 0x0

    invoke-static {v1, v6, v2, v3}, LIF;->a(Ljava/lang/String;ZJ)LIO;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    new-array v1, v6, [LIO;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIO;

    return-object v0
.end method


# virtual methods
.method public a(LDC;)Landroid/widget/SectionIndexer;
    .locals 2

    .prologue
    .line 51
    new-instance v0, LIN;

    invoke-direct {p0}, LIF;->a()[LIO;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LIN;-><init>(LDC;[LIw;)V

    return-object v0
.end method
