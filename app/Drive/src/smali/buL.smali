.class LbuL;
.super Ljava/lang/Object;
.source "Scopes.java"

# interfaces
.implements LbuE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbuE",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbuE;

.field final synthetic a:LbuK;

.field private volatile a:Ljava/lang/Object;


# direct methods
.method constructor <init>(LbuK;LbuE;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, LbuL;->a:LbuK;

    iput-object p2, p0, LbuL;->a:LbuE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, LbuL;->a:Ljava/lang/Object;

    if-nez v0, :cond_5

    .line 63
    const-class v1, Lbvp;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v0, p0, LbuL;->a:Ljava/lang/Object;

    if-nez v0, :cond_4

    .line 65
    iget-object v0, p0, LbuL;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    .line 68
    invoke-static {v0}, LbuJ;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    monitor-exit v1

    .line 87
    :cond_0
    :goto_0
    return-object v0

    .line 72
    :cond_1
    if-nez v0, :cond_2

    invoke-static {}, LbuJ;->a()Ljava/lang/Object;

    move-result-object v0

    .line 73
    :cond_2
    iget-object v2, p0, LbuL;->a:Ljava/lang/Object;

    if-eqz v2, :cond_3

    iget-object v2, p0, LbuL;->a:Ljava/lang/Object;

    if-eq v2, v0, :cond_3

    .line 74
    new-instance v0, LbuG;

    const-string v2, "Provider was reentrant while creating a singleton"

    invoke-direct {v0, v2}, LbuG;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 78
    :cond_3
    :try_start_1
    iput-object v0, p0, LbuL;->a:Ljava/lang/Object;

    .line 80
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :cond_5
    iget-object v0, p0, LbuL;->a:Ljava/lang/Object;

    .line 86
    invoke-static {}, LbuJ;->a()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    const-string v0, "%s[%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LbuL;->a:LbuE;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LbuJ;->a:LbuH;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
