.class public final Lbrw;
.super Ljava/lang/Object;
.source "Gelly.java"


# static fields
.field private static final a:Lbry;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "com.google.common.labs.inject.gelly.GellyStaticRuntime"

    invoke-static {v0}, Lbrw;->a(Ljava/lang/String;)Lbry;

    move-result-object v0

    .line 21
    if-nez v0, :cond_0

    .line 24
    const-string v0, "com.google.common.labs.inject.gelly.GellyGuiceRuntime"

    invoke-static {v0}, Lbrw;->a(Ljava/lang/String;)Lbry;

    move-result-object v0

    .line 26
    :cond_0
    if-nez v0, :cond_1

    .line 27
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "At least one gelly runtime must be there."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    sput-object v0, Lbrw;->a:Lbry;

    .line 30
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private static a(Ljava/lang/String;)Lbry;
    .locals 2

    .prologue
    .line 33
    const/4 v1, 0x0

    .line 35
    :try_start_0
    const-class v0, Lbrw;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbry;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 45
    :goto_0
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 44
    goto :goto_0

    .line 40
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 44
    goto :goto_0

    .line 42
    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;)Lbuu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;)",
            "Lbuu;"
        }
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lbrw;->a:Lbry;

    invoke-interface {v0, p0}, Lbry;->a(Ljava/lang/Iterable;)Lbuu;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a([LbuC;)Lbuu;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbrw;->a(Ljava/lang/Iterable;)Lbuu;

    move-result-object v0

    return-object v0
.end method
