.class public LSf;
.super Ljava/lang/Object;
.source "AutoBackupImpl.java"

# interfaces
.implements LSu;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:LbjP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjP",
            "<",
            "Ljava/lang/String;",
            "LaCV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LSf;->a:Landroid/content/Context;

    .line 46
    new-instance v0, LSg;

    invoke-direct {v0, p0}, LSg;-><init>(LSf;)V

    .line 54
    new-instance v1, LSh;

    invoke-direct {v1, p0}, LSh;-><init>(LSf;)V

    .line 86
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v2

    const-wide/16 v4, 0x1e

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 87
    invoke-virtual {v2, v4, v5, v3}, LbjG;->b(JLjava/util/concurrent/TimeUnit;)LbjG;

    move-result-object v2

    .line 88
    invoke-virtual {v2, v0}, LbjG;->a(Lblf;)LbjG;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v1}, LbjG;->a(LbjM;)LbjP;

    move-result-object v0

    iput-object v0, p0, LSf;->a:LbjP;

    .line 90
    return-void
.end method

.method private a(Ljava/lang/String;)LaCV;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, LSf;->a:LbjP;

    invoke-interface {v0, p1}, LbjP;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaCV;

    return-object v0
.end method

.method static synthetic a(LSf;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LSf;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LSf;)LbjP;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LSf;->a:LbjP;

    return-object v0
.end method

.method private a(LaCV;Lbtd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCV;",
            "Lbtd",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, LSk;

    invoke-direct {v0, p0, p2}, LSk;-><init>(LSf;Lbtd;)V

    invoke-interface {p1, v0}, LaCV;->a(LaCY;)V

    .line 105
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    const-string v0, "AutoBackupImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in queryHasAutoBackup for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 111
    invoke-direct {p0, p1}, LSf;->a(Ljava/lang/String;)LaCV;

    move-result-object v1

    .line 112
    invoke-direct {p0, v1, v0}, LSf;->a(LaCV;Lbtd;)V

    .line 114
    sget-object v2, Lbas;->a:Lbau;

    .line 115
    invoke-interface {v2, v1}, Lbau;->a(LaCV;)LaCZ;

    move-result-object v1

    .line 116
    new-instance v2, LSl;

    invoke-direct {v2, p0, v0}, LSl;-><init>(LSf;Lbtd;)V

    invoke-interface {v1, v2}, LaCZ;->a(LaDd;)V

    .line 127
    return-object v0
.end method

.method public b(Ljava/lang/String;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    const-string v0, "AutoBackupImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in getPromoActivityIntent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 134
    invoke-direct {p0, p1}, LSf;->a(Ljava/lang/String;)LaCV;

    move-result-object v1

    .line 135
    invoke-direct {p0, v1, v0}, LSf;->a(LaCV;Lbtd;)V

    .line 137
    sget-object v2, Lbas;->a:Lbau;

    invoke-interface {v2, v1}, Lbau;->c(LaCV;)LaCZ;

    move-result-object v1

    new-instance v2, LSm;

    invoke-direct {v2, p0, v0}, LSm;-><init>(LSf;Lbtd;)V

    .line 138
    invoke-interface {v1, v2}, LaCZ;->a(LaDd;)V

    .line 149
    return-object v0
.end method

.method public c(Ljava/lang/String;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    const-string v0, "AutoBackupImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in getSettingsActivityIntent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 156
    invoke-direct {p0, p1}, LSf;->a(Ljava/lang/String;)LaCV;

    move-result-object v1

    .line 157
    invoke-direct {p0, v1, v0}, LSf;->a(LaCV;Lbtd;)V

    .line 159
    sget-object v2, Lbas;->a:Lbau;

    invoke-interface {v2, v1}, Lbau;->b(LaCV;)LaCZ;

    move-result-object v1

    new-instance v2, LSn;

    invoke-direct {v2, p0, v0}, LSn;-><init>(LSf;Lbtd;)V

    .line 160
    invoke-interface {v1, v2}, LaCZ;->a(LaDd;)V

    .line 171
    return-object v0
.end method
