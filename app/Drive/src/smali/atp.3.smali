.class public Latp;
.super Ljava/lang/Object;
.source "WelcomeFragment.java"

# interfaces
.implements Lasw;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 184
    if-nez p1, :cond_0

    .line 185
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    .line 212
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    iget-object v1, v0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqK;

    const-string v2, "photoBackupAnnounce"

    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    .line 191
    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "gplusAutoBackupTurnedOn"

    .line 190
    :goto_1
    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)LatB;

    move-result-object v0

    invoke-interface {v0}, LatB;->a()V

    .line 196
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)LatB;

    move-result-object v0

    invoke-interface {v0}, LatB;->a()V

    .line 203
    :cond_1
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Latz;

    move-result-object v0

    invoke-static {v0}, Latz;->a(Latz;)V

    goto :goto_0

    .line 191
    :cond_2
    const-string v0, "onlyIncomingPhotosDialogDisplayed"

    goto :goto_1

    .line 206
    :cond_3
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    goto :goto_0

    .line 209
    :cond_4
    iget-object v0, p0, Latp;->a:Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqK;

    const-string v1, "photoBackupAnnounce"

    const-string v2, "autoBackupDialogDisplayed"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
