.class public Labw;
.super Ljava/lang/Object;
.source "ConfirmSharingDialogManager.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laby;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lue;


# direct methods
.method public constructor <init>(Ljava/util/Set;Landroid/app/Activity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Laby;",
            ">;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {p1}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Labw;->a:Ljava/util/List;

    .line 95
    check-cast p2, Lue;

    iput-object p2, p0, Labw;->a:Lue;

    .line 96
    return-void
.end method

.method private static a(Landroid/os/Bundle;)I
    .locals 1

    .prologue
    .line 84
    const-string v0, "confirmSharing_progress"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/os/Bundle;)LaGw;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 71
    const-string v0, "confirmSharing_plusMedia"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 72
    if-eq v0, v1, :cond_0

    invoke-static {v0}, LaGw;->a(I)LaGw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Labx;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Labw;->a:Lue;

    const-class v1, Labx;

    invoke-interface {v0, v1, p1}, Lue;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "confirmSharing_ownerDomain"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const-string v0, "confirmSharing_contactAddresses"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "confirmSharing_accountDomain"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "confirmSharing_entryName"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "confirmSharing_dialogTag"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "confirmSharing_listenerTag"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 116
    if-eqz p2, :cond_1

    .line 117
    invoke-static {p1}, Labw;->a(Landroid/os/Bundle;)I

    move-result v0

    .line 118
    add-int/lit8 v0, v0, 0x1

    .line 119
    iget-object v1, p0, Labw;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 120
    const-string v1, "confirmSharing_progress"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    iget-object v1, p0, Labw;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laby;

    invoke-interface {v0, p1}, Laby;->a(Landroid/os/Bundle;)V

    .line 128
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-static {p1}, Labw;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Labw;->a(Ljava/lang/String;)Labx;

    move-result-object v0

    invoke-interface {v0, p1}, Labx;->j(Landroid/os/Bundle;)V

    goto :goto_0

    .line 126
    :cond_1
    invoke-static {p1}, Labw;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Labw;->a(Ljava/lang/String;)Labx;

    move-result-object v0

    invoke-interface {v0, p1}, Labx;->k(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGw;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LaGw;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 102
    const-string v0, "confirmSharing_dialogTag"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v0, "confirmSharing_listenerTag"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v0, "confirmSharing_entryName"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v0, "confirmSharing_plusMedia"

    invoke-virtual {p4}, LaGw;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    const-string v0, "confirmSharing_ownerDomain"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "confirmSharing_accountDomain"

    invoke-virtual {v1, v0, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v2, "confirmSharing_contactAddresses"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 109
    invoke-interface {p7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 108
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 111
    const-string v0, "confirmSharing_progress"

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Labw;->a(Landroid/os/Bundle;Z)V

    .line 113
    return-void
.end method
