.class public LvN;
.super Ljava/lang/Object;
.source "CriterionSet.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LvN;->a:Ljava/util/List;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LvN;->a:Ljava/util/List;

    .line 50
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    iget-object v1, p0, LvN;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LvN;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, LvN;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_0
    return-object p0
.end method
