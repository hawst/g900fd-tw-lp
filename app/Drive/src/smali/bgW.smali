.class LbgW;
.super Ljava/lang/Object;
.source "BatchRectifier.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LbgY;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:LbgY;

.field private final a:LbgZ;


# direct methods
.method public constructor <init>(LbgY;LbgZ;I)V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p1, p0, LbgW;->a:LbgY;

    .line 198
    iput-object p2, p0, LbgW;->a:LbgZ;

    .line 199
    iput p3, p0, LbgW;->a:I

    .line 200
    return-void
.end method


# virtual methods
.method public a()LbgY;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 204
    iget-object v0, p0, LbgW;->a:LbgY;

    invoke-virtual {v0}, LbgY;->a()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 205
    invoke-static {}, LbgS;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Rectifying original file..."

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 207
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, LbgW;->a:LbgZ;

    iget-object v4, p0, LbgW;->a:LbgY;

    invoke-virtual {v4}, LbgY;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, LbgZ;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    iget-object v3, p0, LbgW;->a:LbgY;

    invoke-virtual {v3}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v3

    iget-object v4, p0, LbgW;->a:LbgY;

    .line 209
    invoke-virtual {v4}, LbgY;->a()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LbgW;->a:LbgY;

    .line 210
    invoke-virtual {v6}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v6

    iget v7, p0, LbgW;->a:I

    .line 208
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->rectifyJpeg(Ljava/lang/String;Ljava/lang/String;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;I)Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v3

    .line 211
    if-eqz v3, :cond_0

    .line 212
    iget-object v3, p0, LbgW;->a:LbgY;

    invoke-virtual {v3, v2}, LbgY;->b(Ljava/io/File;)LbgY;

    .line 213
    invoke-static {}, LbgS;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v2

    const-string v3, "Rectifying original file...DONE: %d ms"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    :goto_0
    iget-object v0, p0, LbgW;->a:LbgY;

    return-object v0

    .line 215
    :cond_0
    invoke-static {}, LbgS;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Rectification of original file failed!"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    :cond_1
    invoke-static {}, LbgS;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "Page has no input file. Nothing to do."

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, LbgW;->a()LbgY;

    move-result-object v0

    return-object v0
.end method
