.class public LalU;
.super Ljava/lang/Object;
.source "LockedFile.java"


# instance fields
.field private final a:Ljava/io/File;

.field private final a:Ljava/io/RandomAccessFile;

.field private final a:Ljava/nio/channels/FileLock;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, LalU;->a:Ljava/io/File;

    .line 32
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LalU;->a:Ljava/io/RandomAccessFile;

    .line 33
    iget-object v0, p0, LalU;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v0

    iput-object v0, p0, LalU;->a:Ljava/nio/channels/FileLock;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LalU;->a:Ljava/io/File;

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 41
    :try_start_0
    iget-object v0, p0, LalU;->a:Ljava/nio/channels/FileLock;

    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V

    .line 42
    iget-object v0, p0, LalU;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    iget-object v0, p0, LalU;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string v0, "LockedFile"

    const-string v1, "Error when releasing temp file lock."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
