.class public LacH;
.super Ljava/lang/Object;
.source "SharingUtilities.java"


# direct methods
.method public static a(Lacr;)LacD;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lqs;

    invoke-direct {v0}, Lqs;-><init>()V

    invoke-interface {p0}, Lacr;->a()Lqu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqs;->a(Lqu;)Lqs;

    move-result-object v0

    .line 44
    invoke-interface {p0}, Lacr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqs;->a(Ljava/lang/String;)Lqs;

    move-result-object v0

    invoke-virtual {v0}, Lqs;->a()Lqo;

    move-result-object v0

    .line 45
    new-instance v1, LacD;

    const/4 v2, 0x0

    new-instance v3, Laci;

    invoke-direct {v3, v0}, Laci;-><init>(Lqo;)V

    invoke-direct {v1, v2, v3}, LacD;-><init>(LabD;Laci;)V

    return-object v1
.end method

.method public static a(LacD;Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-virtual {p0}, LacD;->a()Lqo;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, LacD;->a()LabD;

    move-result-object v2

    .line 58
    if-eqz v2, :cond_1

    .line 59
    invoke-interface {v2}, LabD;->b()Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-interface {v2}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 64
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 65
    if-lez v1, :cond_0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 90
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v1

    .line 69
    sget-object v2, Lqx;->d:Lqx;

    if-ne v1, v2, :cond_3

    .line 70
    invoke-virtual {v0}, Lqo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    sget v0, Lxi;->sharing_option_anyone_with_link:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_2
    sget v0, Lxi;->sharing_option_anyone:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_3
    sget-object v2, Lqx;->c:Lqx;

    if-ne v1, v2, :cond_5

    .line 76
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-virtual {v0}, Lqo;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    sget v0, Lxi;->sharing_option_anyone_from_with_link:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_4
    sget v0, Lxi;->sharing_option_anyone_from:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_5
    invoke-virtual {v0}, Lqo;->a()Lqu;

    move-result-object v1

    sget-object v2, Lqu;->a:Lqu;

    if-eq v1, v2, :cond_6

    .line 84
    invoke-virtual {v0}, Lqo;->a()Lqu;

    move-result-object v1

    sget-object v2, Lqu;->n:Lqu;

    if-ne v1, v2, :cond_7

    .line 85
    :cond_6
    sget v0, Lxi;->sharing_option_specific_people:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_7
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 29
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 30
    if-ltz v0, :cond_0

    .line 31
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    .line 34
    :cond_0
    const-string v0, "SharingUtilities"

    const-string v1, "Account name does not include domain: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 35
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(LacD;Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    invoke-virtual {p0}, LacD;->a()Lqo;

    move-result-object v0

    .line 101
    sget-object v1, LacI;->a:[I

    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v2

    invoke-virtual {v2}, Lqx;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 129
    sget v0, Lxi;->sharing_option_unknown:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    :goto_0
    invoke-static {p0, p1}, LacH;->a(LacD;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const-string v0, ""

    .line 135
    :cond_0
    return-object v0

    .line 103
    :pswitch_0
    invoke-virtual {v0}, Lqo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    sget v0, Lxi;->sharing_option_anyone_with_link_description:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    sget v0, Lxi;->sharing_option_anyone_description:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_1
    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {v0}, Lqo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    sget v0, Lxi;->sharing_option_anyone_from_with_link_description:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :cond_2
    sget v0, Lxi;->sharing_option_anyone_from_description:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-virtual {p0}, LacD;->a()LabD;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_3

    .line 123
    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 125
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
