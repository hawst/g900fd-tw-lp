.class final Lbwi;
.super Lbwg;
.source "OptionalBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbwg",
        "<",
        "LbiP",
        "<",
        "LbuE",
        "<TT;>;>;>;"
    }
.end annotation


# instance fields
.field private a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LbuE",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final synthetic a:Lbwe;


# direct methods
.method private constructor <init>(Lbwe;)V
    .locals 1

    .prologue
    .line 342
    iput-object p1, p0, Lbwi;->a:Lbwe;

    .line 343
    invoke-static {p1}, Lbwe;->a(Lbwe;)Lbuv;

    move-result-object v0

    invoke-direct {p0, v0}, Lbwg;-><init>(Ljava/lang/Object;)V

    .line 344
    return-void
.end method

.method synthetic constructor <init>(Lbwe;LbvY;)V
    .locals 0

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lbwi;-><init>(Lbwe;)V

    return-void
.end method


# virtual methods
.method public a()LbiP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiP",
            "<",
            "LbuE",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 382
    iget-object v0, p0, Lbwi;->a:LbiP;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p0}, Lbwi;->a()LbiP;

    move-result-object v0

    return-object v0
.end method

.method a(Lbuu;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {v0, v1}, Lbwe;->a(Lbwe;Lcom/google/inject/Binder;)Lcom/google/inject/Binder;

    .line 348
    iget-object v0, p0, Lbwi;->a:Lbwe;

    iget-object v2, p0, Lbwi;->a:Lbwe;

    invoke-static {v2}, Lbwe;->b(Lbwe;)Lbuv;

    move-result-object v2

    invoke-interface {p1, v2}, Lbuu;->a(Lbuv;)Lbup;

    move-result-object v2

    invoke-static {v0, v2}, Lbwe;->a(Lbwe;Lbup;)Lbup;

    .line 349
    iget-object v0, p0, Lbwi;->a:Lbwe;

    iget-object v2, p0, Lbwi;->a:Lbwe;

    invoke-static {v2}, Lbwe;->c(Lbwe;)Lbuv;

    move-result-object v2

    invoke-interface {p1, v2}, Lbuu;->a(Lbuv;)Lbup;

    move-result-object v2

    invoke-static {v0, v2}, Lbwe;->b(Lbwe;Lbup;)Lbup;

    .line 350
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {v0}, Lbwe;->a(Lbwe;)Lbuv;

    move-result-object v0

    invoke-interface {p1, v0}, Lbuu;->a(Lbuv;)Lbup;

    move-result-object v0

    .line 352
    iget-object v2, p0, Lbwi;->a:Lbwe;

    invoke-static {v2}, Lbwe;->a(Lbwe;)Lbup;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 357
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {v0}, Lbwe;->a(Lbwe;)Lbup;

    move-result-object v0

    .line 368
    :goto_0
    if-eqz v0, :cond_2

    .line 369
    invoke-interface {v0}, Lbup;->a()LbuE;

    move-result-object v1

    invoke-static {v1}, LbiP;->a(Ljava/lang/Object;)LbiP;

    move-result-object v1

    iput-object v1, p0, Lbwi;->a:LbiP;

    .line 370
    iget-object v1, p0, Lbwi;->a:Lbwe;

    .line 371
    invoke-interface {v0}, Lbup;->a()Lbuv;

    move-result-object v2

    invoke-static {v2}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v2

    invoke-static {v2}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v2

    .line 370
    invoke-static {v1, v2}, Lbwe;->a(Lbwe;Ljava/util/Set;)Ljava/util/Set;

    .line 372
    iget-object v1, p0, Lbwi;->a:Lbwe;

    .line 373
    invoke-interface {v0}, Lbup;->a()Lbuv;

    move-result-object v0

    invoke-static {v0}, LbvX;->a(Lbuv;)Lbuv;

    move-result-object v0

    invoke-static {v0}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 372
    invoke-static {v1, v0}, Lbwe;->b(Lbwe;Ljava/util/Set;)Ljava/util/Set;

    .line 379
    :goto_1
    return-void

    .line 358
    :cond_0
    iget-object v2, p0, Lbwi;->a:Lbwe;

    invoke-static {v2}, Lbwe;->b(Lbwe;)Lbup;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 359
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {v0}, Lbwe;->b(Lbwe;)Lbup;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_1
    if-eqz v0, :cond_3

    .line 365
    iget-object v1, p0, Lbwi;->a:Lbwe;

    invoke-static {v1, v0}, Lbwe;->a(Lbwe;Lbup;)Lbup;

    goto :goto_0

    .line 375
    :cond_2
    invoke-static {}, LbiP;->a()LbiP;

    move-result-object v0

    iput-object v0, p0, Lbwi;->a:LbiP;

    .line 376
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v1

    invoke-static {v0, v1}, Lbwe;->a(Lbwe;Ljava/util/Set;)Ljava/util/Set;

    .line 377
    iget-object v0, p0, Lbwi;->a:Lbwe;

    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v1

    invoke-static {v0, v1}, Lbwe;->b(Lbwe;Ljava/util/Set;)Ljava/util/Set;

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
