.class public LaxS;
.super Ljava/lang/Object;
.source "FindInFileView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/widget/FindInFileView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/widget/FindInFileView;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->b(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 55
    :cond_0
    iget-object v0, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->clearFocus()V

    .line 56
    iget-object v0, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)LaxV;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    .line 58
    :goto_0
    iget-object v1, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)LaxV;

    move-result-object v1

    iget-object v2, p0, LaxS;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v2}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LaxV;->a(Ljava/lang/String;Z)Z

    .line 61
    :cond_1
    return-void

    .line 57
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
