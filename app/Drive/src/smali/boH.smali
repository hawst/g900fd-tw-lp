.class final LboH;
.super LboE;
.source "MapMakerInternalMap.java"

# interfaces
.implements Lbox;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LboE",
        "<TK;TV;>;",
        "Lbox",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILbox;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1056
    invoke-direct {p0, p1, p2, p3}, LboE;-><init>(Ljava/lang/Object;ILbox;)V

    .line 1061
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LboH;->a:J

    .line 1073
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboH;->b:Lbox;

    .line 1086
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboH;->c:Lbox;

    .line 1101
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboH;->d:Lbox;

    .line 1114
    invoke-static {}, LbnY;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, LboH;->e:Lbox;

    .line 1057
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1065
    iget-wide v0, p0, LboH;->a:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1070
    iput-wide p1, p0, LboH;->a:J

    .line 1071
    return-void
.end method

.method public a(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1083
    iput-object p1, p0, LboH;->b:Lbox;

    .line 1084
    return-void
.end method

.method public b()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1078
    iget-object v0, p0, LboH;->b:Lbox;

    return-object v0
.end method

.method public b(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1096
    iput-object p1, p0, LboH;->c:Lbox;

    .line 1097
    return-void
.end method

.method public c()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1091
    iget-object v0, p0, LboH;->c:Lbox;

    return-object v0
.end method

.method public c(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1111
    iput-object p1, p0, LboH;->d:Lbox;

    .line 1112
    return-void
.end method

.method public d()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1106
    iget-object v0, p0, LboH;->d:Lbox;

    return-object v0
.end method

.method public d(Lbox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1124
    iput-object p1, p0, LboH;->e:Lbox;

    .line 1125
    return-void
.end method

.method public e()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1119
    iget-object v0, p0, LboH;->e:Lbox;

    return-object v0
.end method
