.class LbmK;
.super LbmF;
.source "ImmutableList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LbmF",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final transient a:I

.field final synthetic a:LbmF;

.field final transient b:I


# direct methods
.method constructor <init>(LbmF;II)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, LbmK;->a:LbmF;

    invoke-direct {p0}, LbmF;-><init>()V

    .line 390
    iput p2, p0, LbmK;->a:I

    .line 391
    iput p3, p0, LbmK;->b:I

    .line 392
    return-void
.end method


# virtual methods
.method public a(II)LbmF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LbmF",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 405
    iget v0, p0, LbmK;->b:I

    invoke-static {p1, p2, v0}, LbiT;->a(III)V

    .line 406
    iget-object v0, p0, LbmK;->a:LbmF;

    iget v1, p0, LbmK;->a:I

    add-int/2addr v1, p1

    iget v2, p0, LbmK;->a:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, LbmF;->a(II)LbmF;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 399
    iget v0, p0, LbmK;->b:I

    invoke-static {p1, v0}, LbiT;->a(II)I

    .line 400
    iget-object v0, p0, LbmK;->a:LbmF;

    iget v1, p0, LbmK;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, LbmF;->a()Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0, p1}, LbmF;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, LbmK;->b:I

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1, p2}, LbmK;->a(II)LbmF;

    move-result-object v0

    return-object v0
.end method
