.class public final enum LapG;
.super Ljava/lang/Enum;
.source "DocListViewModeQuerier.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LapG;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LapG;

.field private static final synthetic a:[LapG;

.field public static final enum b:LapG;

.field public static final enum c:LapG;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, LapG;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LapG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapG;->a:LapG;

    .line 22
    new-instance v0, LapG;

    const-string v1, "SELECTION"

    invoke-direct {v0, v1, v3}, LapG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapG;->b:LapG;

    .line 23
    new-instance v0, LapG;

    const-string v1, "FILE_PICKER"

    invoke-direct {v0, v1, v4}, LapG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapG;->c:LapG;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [LapG;

    sget-object v1, LapG;->a:LapG;

    aput-object v1, v0, v2

    sget-object v1, LapG;->b:LapG;

    aput-object v1, v0, v3

    sget-object v1, LapG;->c:LapG;

    aput-object v1, v0, v4

    sput-object v0, LapG;->a:[LapG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LapG;
    .locals 1

    .prologue
    .line 17
    const-class v0, LapG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LapG;

    return-object v0
.end method

.method public static values()[LapG;
    .locals 1

    .prologue
    .line 17
    sget-object v0, LapG;->a:[LapG;

    invoke-virtual {v0}, [LapG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LapG;

    return-object v0
.end method
