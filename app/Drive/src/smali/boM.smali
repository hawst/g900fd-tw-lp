.class LboM;
.super Ljava/lang/ref/WeakReference;
.source "MapMakerInternalMap.java"

# interfaces
.implements Lbox;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TK;>;",
        "Lbox",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field volatile a:LboK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LboK",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1374
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1431
    invoke-static {}, LbnY;->a()LboK;

    move-result-object v0

    iput-object v0, p0, LboM;->a:LboK;

    .line 1375
    iput p3, p0, LboM;->a:I

    .line 1376
    iput-object p4, p0, LboM;->a:Lbox;

    .line 1377
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1444
    iget v0, p0, LboM;->a:I

    return v0
.end method

.method public a()J
    .locals 1

    .prologue
    .line 1386
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LboK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LboK",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1434
    iget-object v0, p0, LboM;->a:LboK;

    return-object v0
.end method

.method public a()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1448
    iget-object v0, p0, LboM;->a:Lbox;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1380
    invoke-virtual {p0}, LboM;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1390
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LboK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LboK",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1438
    iget-object v0, p0, LboM;->a:LboK;

    .line 1439
    iput-object p1, p0, LboM;->a:LboK;

    .line 1440
    invoke-interface {v0, p1}, LboK;->a(LboK;)V

    .line 1441
    return-void
.end method

.method public a(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1398
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1394
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1406
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1402
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1416
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1412
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1424
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Lbox;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1420
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
