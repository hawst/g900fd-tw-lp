.class Laps;
.super Landroid/os/AsyncTask;
.source "DocListLoadingManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LIK;

.field final synthetic a:LaFO;

.field final synthetic a:Lapn;

.field final synthetic a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# direct methods
.method constructor <init>(Lapn;LIK;LaFO;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Laps;->a:Lapn;

    iput-object p2, p0, Laps;->a:LIK;

    iput-object p3, p0, Laps;->a:LaFO;

    iput-object p4, p0, Laps;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 507
    const-string v0, "DocListLoader"

    const-string v1, "setInitialSortKind to %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Laps;->a:LIK;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 508
    iget-object v0, p0, Laps;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)LQV;

    move-result-object v0

    iget-object v1, p0, Laps;->a:LaFO;

    iget-object v2, p0, Laps;->a:LIK;

    iget-object v3, p0, Laps;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0, v1, v2, v3}, LQV;->a(LaFO;LIK;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 510
    const-string v0, "DocListLoader"

    const-string v1, "setInitialSortKind to %s: done"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Laps;->a:LIK;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 511
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 504
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Laps;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
