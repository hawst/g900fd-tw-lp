.class public abstract LbdR;
.super Ljava/lang/Object;
.source "AbstractHttpContent.java"

# interfaces
.implements LbdZ;


# instance fields
.field private a:J

.field private a:Lbeh;


# direct methods
.method protected constructor <init>(Lbeh;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LbdR;->a:J

    .line 56
    iput-object p1, p0, LbdR;->a:Lbeh;

    .line 57
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, LbdR;-><init>(Lbeh;)V

    .line 49
    return-void

    .line 48
    :cond_0
    new-instance v0, Lbeh;

    invoke-direct {v0, p1}, Lbeh;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LbdZ;)J
    .locals 2

    .prologue
    .line 136
    invoke-interface {p0}, LbdZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const-wide/16 v0, -0x1

    .line 139
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, LbfD;->a(LbfR;)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 64
    iget-wide v0, p0, LbdR;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, LbdR;->b()J

    move-result-wide v0

    iput-wide v0, p0, LbdR;->a:J

    .line 67
    :cond_0
    iget-wide v0, p0, LbdR;->a:J

    return-wide v0
.end method

.method public final a()Lbeh;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, LbdR;->a:Lbeh;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, LbdR;->a:Lbeh;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LbdR;->a:Lbeh;

    invoke-virtual {v0}, Lbeh;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, LbdR;->a:Lbeh;

    if-eqz v0, :cond_0

    iget-object v0, p0, LbdR;->a:Lbeh;

    invoke-virtual {v0}, Lbeh;->a()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lbfp;->a:Ljava/nio/charset/Charset;

    .line 101
    :goto_0
    return-object v0

    .line 100
    :cond_1
    iget-object v0, p0, LbdR;->a:Lbeh;

    .line 101
    invoke-virtual {v0}, Lbeh;->a()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method protected b()J
    .locals 2

    .prologue
    .line 117
    invoke-static {p0}, LbdR;->a(LbdZ;)J

    move-result-wide v0

    return-wide v0
.end method
