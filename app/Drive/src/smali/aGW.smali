.class public LaGW;
.super LaGm;
.source "PartialFeed.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaFc;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final a:Ljava/lang/Long;

.field private final a:Ljava/lang/String;

.field private b:J

.field private b:Ljava/lang/Long;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LaEz;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    invoke-static {}, LaFc;->a()LaFc;

    move-result-object v0

    sget-object v3, LaEG;->g:LaEG;

    invoke-virtual {v3}, LaEG;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, p1, v0, v3}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 48
    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 49
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    cmp-long v0, p8, v4

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 51
    invoke-static {p5, p6}, LaGW;->b(Ljava/lang/String;Ljava/lang/Long;)V

    .line 52
    iput-wide p2, p0, LaGW;->a:J

    .line 53
    iput-object p4, p0, LaGW;->a:Ljava/lang/String;

    .line 54
    iput-object p5, p0, LaGW;->b:Ljava/lang/String;

    .line 55
    iput-object p6, p0, LaGW;->b:Ljava/lang/Long;

    .line 56
    iput-object p7, p0, LaGW;->a:Ljava/lang/Long;

    .line 57
    iput-wide p8, p0, LaGW;->b:J

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    :cond_1
    move v1, v2

    .line 50
    goto :goto_1
.end method

.method public static a(LaEz;JLjava/lang/String;Ljava/lang/Long;)LaGW;
    .locals 11

    .prologue
    .line 37
    new-instance v0, LaGW;

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v9}, LaGW;-><init>(LaEz;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;J)V

    return-object v0
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaGW;
    .locals 10

    .prologue
    .line 157
    sget-object v0, LaFd;->a:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 158
    sget-object v0, LaFd;->b:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 159
    sget-object v0, LaFd;->c:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 160
    sget-object v0, LaFd;->d:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    .line 162
    if-nez v5, :cond_0

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v0, v0, v8

    if-nez v0, :cond_0

    .line 163
    const/4 v6, 0x0

    .line 166
    :cond_0
    sget-object v0, LaFd;->f:LaFd;

    .line 167
    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)J

    move-result-wide v8

    .line 169
    sget-object v0, LaFd;->e:LaFd;

    .line 170
    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v7

    .line 171
    new-instance v0, LaGW;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LaGW;-><init>(LaEz;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;J)V

    .line 173
    invoke-static {}, LaFc;->a()LaFc;

    move-result-object v1

    invoke-virtual {v1}, LaFc;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaGW;->c(J)V

    .line 174
    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    if-nez p0, :cond_0

    move v3, v0

    :goto_0
    if-nez p1, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid nextUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", clipTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 43
    return-void

    :cond_0
    move v3, v1

    .line 41
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, LaGW;->b:J

    return-wide v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, LaGW;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, LaGW;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 123
    iget-wide v0, p0, LaGW;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LaGW;->b:J

    .line 124
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 136
    sget-object v0, LaFd;->a:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaGW;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 137
    sget-object v0, LaFd;->b:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGW;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v0, LaFd;->c:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGW;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget-object v0, LaFd;->d:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGW;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 140
    sget-object v0, LaFd;->e:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGW;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    sget-object v0, LaFd;->f:LaFd;

    invoke-virtual {v0}, LaFd;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaGW;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 142
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 112
    invoke-static {p1, p2}, LaGW;->b(Ljava/lang/String;Ljava/lang/Long;)V

    .line 113
    iput-object p1, p0, LaGW;->b:Ljava/lang/String;

    .line 114
    iput-object p2, p0, LaGW;->b:Ljava/lang/Long;

    .line 115
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, LaGW;->a:Ljava/lang/String;

    iget-object v1, p0, LaGW;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LaGW;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, LaGW;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 146
    const-string v0, "PartialFeed[accountSqlId=%s, initialUri=%s, nextUri=%s, clipTime=%s, cachedSearchId=%s, sqlId=%s, numPagesRetrieved=%d]"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, LaGW;->a:J

    .line 148
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaGW;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaGW;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LaGW;->b:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LaGW;->a:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    .line 149
    invoke-virtual {p0}, LaGW;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-wide v4, p0, LaGW;->b:J

    .line 150
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 146
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
