.class public final enum LaEt;
.super Ljava/lang/Enum;
.source "CachedSearchTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEt;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEt;

.field private static final synthetic a:[LaEt;

.field public static final enum b:LaEt;

.field public static final enum c:LaEt;

.field public static final enum d:LaEt;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xe

    .line 40
    new-instance v0, LaEt;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LaEs;->b()LaEs;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 41
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 42
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 43
    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 41
    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaEt;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEt;->a:LaEt;

    .line 45
    new-instance v0, LaEt;

    const-string v1, "QUERY"

    invoke-static {}, LaEs;->b()LaEs;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "query"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 46
    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEt;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEt;->b:LaEt;

    .line 48
    new-instance v0, LaEt;

    const-string v1, "TIMESTAMP"

    invoke-static {}, LaEs;->b()LaEs;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "timestamp"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 49
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEt;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEt;->c:LaEt;

    .line 52
    new-instance v0, LaEt;

    const-string v1, "COMPLETED"

    invoke-static {}, LaEs;->b()LaEs;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "completed"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 53
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEt;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEt;->d:LaEt;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [LaEt;

    sget-object v1, LaEt;->a:LaEt;

    aput-object v1, v0, v7

    sget-object v1, LaEt;->b:LaEt;

    aput-object v1, v0, v8

    sget-object v1, LaEt;->c:LaEt;

    aput-object v1, v0, v9

    sget-object v1, LaEt;->d:LaEt;

    aput-object v1, v0, v10

    sput-object v0, LaEt;->a:[LaEt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEt;->a:LaFr;

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEt;
    .locals 1

    .prologue
    .line 39
    const-class v0, LaEt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEt;

    return-object v0
.end method

.method public static values()[LaEt;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LaEt;->a:[LaEt;

    invoke-virtual {v0}, [LaEt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEt;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LaEt;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LaEt;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
