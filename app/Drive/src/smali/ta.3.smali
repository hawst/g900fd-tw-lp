.class public Lta;
.super Ljava/lang/Object;
.source "DocumentOpenerActivityProxy.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "LaGu;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/ProgressDialog;

.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;Landroid/app/ProgressDialog;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    iput-object p2, p0, Lta;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iput-object p4, p0, Lta;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lta;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 89
    iget-object v0, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    iget-object v1, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iget-object v2, p0, Lta;->a:Landroid/content/Intent;

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V

    .line 90
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 85
    check-cast p1, LaGu;

    invoke-virtual {p0, p1}, Lta;->a(LaGu;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 94
    iget-object v0, p0, Lta;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 95
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    const-string v0, "DocumentOpenerActivityProxy"

    const-string v1, "User canceled entry lookup."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->finish()V

    .line 114
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-static {p1}, LUF;->a(Ljava/lang/Throwable;)LUF;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, LUF;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 102
    const-string v0, "DocumentOpenerActivityProxy"

    invoke-static {v0, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v0, LES;

    iget-object v1, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    .line 106
    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a()LM;

    move-result-object v1

    const/4 v2, 0x0

    check-cast v2, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iget-object v4, p0, Lta;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;

    sget v6, Lxi;->error_page_title:I

    .line 109
    invoke-virtual {v4, v6}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LES;-><init>(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0}, LES;->a()V

    goto :goto_0
.end method
