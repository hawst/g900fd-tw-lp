.class public Lbna;
.super Lbmx;
.source "ImmutableSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmx",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lbna;-><init>(I)V

    .line 542
    return-void
.end method

.method constructor <init>(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 544
    invoke-direct {p0}, Lbmx;-><init>()V

    .line 545
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "capacity must be >= 0 but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 546
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    .line 547
    iput v2, p0, Lbna;->a:I

    .line 548
    return-void

    :cond_0
    move v0, v2

    .line 545
    goto :goto_0
.end method


# virtual methods
.method public a()LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 638
    iget v0, p0, Lbna;->a:I

    iget-object v1, p0, Lbna;->a:[Ljava/lang/Object;

    invoke-static {v0, v1}, LbmY;->a(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 641
    invoke-virtual {v0}, LbmY;->size()I

    move-result v1

    iput v1, p0, Lbna;->a:I

    .line 642
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Iterable;)Lbmx;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lbna;->a(Ljava/lang/Iterable;)Lbna;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Lbmx;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/util/Iterator;)Lbmx;
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lbna;->a(Ljava/util/Iterator;)Lbna;

    move-result-object v0

    return-object v0
.end method

.method a(I)Lbna;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 555
    iget-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 556
    iget-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    iget-object v1, p0, Lbna;->a:[Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1, p1}, Lbna;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lbpv;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    .line 559
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lbna;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 609
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 610
    check-cast v0, Ljava/util/Collection;

    .line 611
    iget v1, p0, Lbna;->a:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lbna;->a(I)Lbna;

    .line 613
    :cond_0
    invoke-super {p0, p1}, Lbmx;->a(Ljava/lang/Iterable;)Lbmx;

    .line 614
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lbna;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 573
    iget v0, p0, Lbna;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lbna;->a(I)Lbna;

    .line 574
    iget-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    iget v1, p0, Lbna;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lbna;->a:I

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 575
    return-object p0
.end method

.method public a(Ljava/util/Iterator;)Lbna;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 628
    invoke-super {p0, p1}, Lbmx;->a(Ljava/util/Iterator;)Lbmx;

    .line 629
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)Lbna;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 589
    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 590
    aget-object v2, p1, v0

    invoke-static {v2, v0}, Lbpv;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 592
    :cond_0
    iget v0, p0, Lbna;->a:I

    array-length v2, p1

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lbna;->a(I)Lbna;

    .line 593
    iget-object v0, p0, Lbna;->a:[Ljava/lang/Object;

    iget v2, p0, Lbna;->a:I

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 594
    iget v0, p0, Lbna;->a:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lbna;->a:I

    .line 595
    return-object p0
.end method
