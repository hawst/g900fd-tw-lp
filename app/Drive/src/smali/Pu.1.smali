.class public LPu;
.super Ljava/lang/Object;
.source "MenuHelper.java"


# instance fields
.field private final a:LCM;

.field private final a:LPp;

.field private a:LaGu;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LvY;


# direct methods
.method public constructor <init>(LCM;LPp;Laja;LvY;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCM;",
            "LPp;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LvY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, LPu;->a:LCM;

    .line 44
    iput-object p2, p0, LPu;->a:LPp;

    .line 45
    iput-object p3, p0, LPu;->a:Laja;

    .line 46
    iput-object p4, p0, LPu;->a:LvY;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 50
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    .line 51
    iget-object v0, p0, LPu;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 52
    instance-of v2, v0, Landroid/app/Activity;

    invoke-static {v2}, LbiT;->a(Z)V

    .line 53
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 54
    sget v2, Lxf;->menu_detail:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 55
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    move v0, v1

    .line 56
    :goto_0
    if-ge v0, v2, :cond_0

    .line 57
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 58
    new-instance v3, LPv;

    invoke-direct {v3, p0, v1}, LPv;-><init>(LPu;Landroid/view/MenuItem;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Menu;LaGu;I)V
    .locals 3

    .prologue
    .line 119
    if-nez p2, :cond_1

    .line 132
    :cond_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, LPu;->a:LCM;

    invoke-virtual {v0, p2}, LCM;->a(LaGu;)LCI;

    move-result-object v0

    .line 124
    iput-object p2, p0, LPu;->a:LaGu;

    .line 125
    invoke-virtual {v0}, LCI;->a()Ljava/util/Set;

    move-result-object v0

    .line 127
    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 128
    invoke-interface {p1, p3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 129
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 130
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 127
    add-int/lit8 p3, p3, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, LPu;->a:LaGu;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, LPu;->a:LaGu;

    invoke-virtual {p0, p1, v0}, LPu;->a(Landroid/view/MenuItem;LaGu;)Z

    .line 71
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;LaGu;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 75
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-interface {p2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 78
    if-nez v2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 80
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_rename:I

    if-ne v3, v4, :cond_2

    .line 81
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->a(LaGu;)V

    :goto_1
    move v0, v1

    .line 115
    goto :goto_0

    .line 82
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_sharing:I

    if-ne v3, v4, :cond_3

    .line 83
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, v2}, LPp;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_1

    .line 84
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_delete:I

    if-ne v3, v4, :cond_4

    .line 85
    iget-object v0, p0, LPu;->a:LvY;

    invoke-interface {v0}, LvY;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 86
    iget-object v3, p0, LPu;->a:LPp;

    invoke-static {v2}, LaGD;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGD;

    move-result-object v2

    invoke-interface {v3, v2, v0}, LPp;->a(LaGD;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_1

    .line 87
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_untrash:I

    if-ne v3, v4, :cond_5

    .line 88
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, v2}, LPp;->c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_1

    .line 89
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_delete_forever:I

    if-ne v3, v4, :cond_6

    .line 90
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, v2}, LPp;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_1

    .line 91
    :cond_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_send:I

    if-ne v3, v4, :cond_7

    .line 92
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->d(LaGu;)V

    goto :goto_1

    .line 93
    :cond_7
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_send_link:I

    if-ne v3, v4, :cond_8

    .line 94
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->f(LaGu;)V

    goto :goto_1

    .line 95
    :cond_8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_print:I

    if-ne v3, v4, :cond_9

    .line 96
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->b(LaGu;)V

    goto :goto_1

    .line 97
    :cond_9
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_pin:I

    if-ne v3, v4, :cond_a

    .line 98
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2, v1}, LPp;->a(LaGu;Z)V

    goto :goto_1

    .line 99
    :cond_a
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_unpin:I

    if-ne v3, v4, :cond_b

    .line 100
    iget-object v2, p0, LPu;->a:LPp;

    invoke-interface {v2, p2, v0}, LPp;->a(LaGu;Z)V

    goto/16 :goto_1

    .line 101
    :cond_b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_move_to_folder:I

    if-ne v3, v4, :cond_c

    .line 102
    iget-object v0, p0, LPu;->a:LPp;

    invoke-static {v2}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v2

    invoke-interface {v0, v2}, LPp;->a(LbmY;)V

    goto/16 :goto_1

    .line 103
    :cond_c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_download:I

    if-ne v3, v4, :cond_d

    .line 104
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->e(LaGu;)V

    goto/16 :goto_1

    .line 105
    :cond_d
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_open_with:I

    if-ne v3, v4, :cond_e

    .line 106
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, p2}, LPp;->c(LaGu;)V

    goto/16 :goto_1

    .line 107
    :cond_e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_create_shortcut:I

    if-ne v3, v4, :cond_f

    .line 108
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, v2}, LPp;->d(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto/16 :goto_1

    .line 109
    :cond_f
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxc;->menu_dump_database:I

    if-ne v3, v4, :cond_0

    .line 110
    iget-object v0, p0, LPu;->a:LPp;

    invoke-interface {v0, v2}, LPp;->e(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto/16 :goto_1
.end method
