.class LbpY;
.super LblX;
.source "Sets.java"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LblX",
        "<TE;>;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Set;LbiU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TE;>;",
            "LbiU",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 847
    invoke-direct {p0, p1, p2}, LblX;-><init>(Ljava/util/Collection;LbiU;)V

    .line 848
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 852
    invoke-static {p0, p1}, LbpU;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 857
    invoke-static {p0}, LbpU;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method
