.class public Lbuv;
.super Ljava/lang/Object;
.source "Key.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Lbuy;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    sget-object v0, LbuA;->a:LbuA;

    iput-object v0, p0, Lbuv;->a:Lbuy;

    .line 121
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/Class;)LbuP;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lbvq;->a(LbuP;)LbuP;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:LbuP;

    .line 122
    invoke-direct {p0}, Lbuv;->a()I

    move-result v0

    iput v0, p0, Lbuv;->a:I

    .line 123
    invoke-direct {p0}, Lbuv;->a()Lbjv;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:Lbjv;

    .line 124
    return-void
.end method

.method private constructor <init>(LbuP;Lbuy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuP",
            "<TT;>;",
            "Lbuy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p2, p0, Lbuv;->a:Lbuy;

    .line 140
    invoke-static {p1}, Lbvq;->a(LbuP;)LbuP;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:LbuP;

    .line 141
    invoke-direct {p0}, Lbuv;->a()I

    move-result v0

    iput v0, p0, Lbuv;->a:I

    .line 142
    invoke-direct {p0}, Lbuv;->a()Lbjv;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:Lbjv;

    .line 143
    return-void
.end method

.method private constructor <init>(Ljava/lang/reflect/Type;Lbuy;)V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p2, p0, Lbuv;->a:Lbuy;

    .line 132
    invoke-static {p1}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbvq;->a(LbuP;)LbuP;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:LbuP;

    .line 133
    invoke-direct {p0}, Lbuv;->a()I

    move-result v0

    iput v0, p0, Lbuv;->a:I

    .line 134
    invoke-direct {p0}, Lbuv;->a()Lbjv;

    move-result-object v0

    iput-object v0, p0, Lbuv;->a:Lbjv;

    .line 135
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lbuv;->a:LbuP;

    invoke-virtual {v0}, LbuP;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lbuv;->a:Lbuy;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private a()Lbjv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbjv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    new-instance v0, Lbuw;

    invoke-direct {v0, p0}, Lbuw;-><init>(Lbuv;)V

    invoke-static {v0}, Lbjw;->a(Lbjv;)Lbjv;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbuv;)LbuP;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbuv;->a:LbuP;

    return-object v0
.end method

.method public static a(LbuP;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 287
    new-instance v0, Lbuv;

    sget-object v1, LbuA;->a:LbuA;

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(LbuP;Lbuy;)V

    return-object v0
.end method

.method public static a(LbuP;Ljava/lang/Class;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 295
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(LbuP;Lbuy;)V

    return-object v0
.end method

.method public static a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 303
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/annotation/Annotation;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(LbuP;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Lbuv;

    sget-object v1, LbuA;->a:LbuA;

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 251
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 258
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/annotation/Annotation;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/reflect/Type;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lbuv",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v0, Lbuv;

    sget-object v1, LbuA;->a:LbuA;

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbuv",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 273
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/annotation/Annotation;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Lbuv",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 280
    new-instance v0, Lbuv;

    invoke-static {p1}, Lbuv;->a(Ljava/lang/annotation/Annotation;)Lbuy;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method static synthetic a(Lbuv;)Lbuy;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbuv;->a:Lbuy;

    return-object v0
.end method

.method static a(Ljava/lang/Class;)Lbuy;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbuy;"
        }
    .end annotation

    .prologue
    .line 382
    invoke-static {p0}, LbuW;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 383
    invoke-static {v1}, LbuW;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    invoke-static {v1}, LbuW;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(Ljava/lang/annotation/Annotation;)Lbuy;

    move-result-object v0

    .line 390
    :goto_0
    return-object v0

    .line 387
    :cond_0
    const-string v0, "annotation type"

    invoke-static {v1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    invoke-static {v1}, Lbuv;->a(Ljava/lang/Class;)V

    .line 389
    invoke-static {v1}, Lbuv;->b(Ljava/lang/Class;)V

    .line 390
    new-instance v0, Lbuz;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbuz;-><init>(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)V

    goto :goto_0
.end method

.method static a(Ljava/lang/annotation/Annotation;)Lbuy;
    .locals 2

    .prologue
    .line 366
    const-string v0, "annotation"

    invoke-static {p0, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    invoke-interface {p0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 368
    invoke-static {v1}, Lbuv;->a(Ljava/lang/Class;)V

    .line 369
    invoke-static {v1}, Lbuv;->b(Ljava/lang/Class;)V

    .line 371
    invoke-static {v1}, LbuW;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    new-instance v0, Lbuz;

    invoke-direct {v0, v1, p0}, Lbuz;-><init>(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)V

    .line 375
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbux;

    invoke-static {p0}, LbuW;->a(Ljava/lang/annotation/Annotation;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-direct {v0, v1}, Lbux;-><init>(Ljava/lang/annotation/Annotation;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 396
    invoke-static {p0}, LbuW;->c(Ljava/lang/Class;)Z

    move-result v0

    const-string v1, "%s is not retained at runtime. Please annotate it with @Retention(RUNTIME)."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 398
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 396
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 399
    return-void
.end method

.method private static b(Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402
    invoke-static {p0}, LbuW;->e(Ljava/lang/Class;)Z

    move-result v0

    const-string v1, "%s is not a binding annotation. Please annotate it with @BindingAnnotation."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 404
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 402
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 405
    return-void
.end method


# virtual methods
.method public final a()LbuP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lbuv;->a:LbuP;

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lbuv;->a:Lbuy;

    invoke-interface {v0}, Lbuy;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lbuv;->a:Lbuy;

    invoke-interface {v0}, Lbuy;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public b(LbuP;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 333
    new-instance v0, Lbuv;

    iget-object v1, p0, Lbuv;->a:Lbuy;

    invoke-direct {v0, p1, v1}, Lbuv;-><init>(LbuP;Lbuy;)V

    return-object v0
.end method

.method public b(Ljava/lang/reflect/Type;)Lbuv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lbuv",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 323
    new-instance v0, Lbuv;

    iget-object v1, p0, Lbuv;->a:Lbuy;

    invoke-direct {v0, p1, v1}, Lbuv;-><init>(Ljava/lang/reflect/Type;Lbuy;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    if-ne p1, p0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    instance-of v2, p1, Lbuv;

    if-nez v2, :cond_2

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_2
    check-cast p1, Lbuv;

    .line 219
    iget-object v2, p0, Lbuv;->a:Lbuy;

    iget-object v3, p1, Lbuv;->a:Lbuy;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbuv;->a:LbuP;

    iget-object v3, p1, Lbuv;->a:LbuP;

    .line 220
    invoke-virtual {v2, v3}, LbuP;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lbuv;->a:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lbuv;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
