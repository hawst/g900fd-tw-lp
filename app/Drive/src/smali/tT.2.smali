.class public LtT;
.super Ljava/lang/Object;
.source "GetContentActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Lcom/google/android/apps/docs/app/GetContentActivity;

.field final synthetic a:Lcom/google/android/apps/docs/utils/ParcelableTask;

.field final synthetic b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/GetContentActivity;Lcom/google/android/apps/docs/utils/ParcelableTask;Landroid/content/Intent;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, LtT;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    iput-object p2, p0, LtT;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    iput-object p3, p0, LtT;->a:Landroid/content/Intent;

    iput-object p4, p0, LtT;->a:Landroid/net/Uri;

    iput-object p5, p0, LtT;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, LtT;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, LtT;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    iget-object v1, p0, LtT;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-interface {v0, v1}, Lcom/google/android/apps/docs/utils/ParcelableTask;->b(Laju;)V

    .line 191
    :cond_0
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iget-object v1, p0, LtT;->a:Landroid/content/Intent;

    iget-object v2, p0, LtT;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 192
    iget-object v0, p0, LtT;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    iget-object v2, p0, LtT;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 193
    iget-object v0, p0, LtT;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    const/4 v1, -0x1

    iget-object v2, p0, LtT;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/app/GetContentActivity;->setResult(ILandroid/content/Intent;)V

    .line 194
    iget-object v0, p0, LtT;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->finish()V

    .line 195
    return-void
.end method
