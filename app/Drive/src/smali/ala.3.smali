.class public Lala;
.super Ljava/lang/Object;
.source "DocumentOpenerUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 101
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lry;->z:Lry;

    invoke-interface {p2, v0}, LtK;->a(LtJ;)Z

    move-result v0

    .line 104
    invoke-interface {p1, p3}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v1

    .line 105
    if-nez v1, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    .line 110
    :cond_0
    invoke-interface {v1}, LaGu;->a()LaGn;

    move-result-object v2

    .line 111
    if-eqz v0, :cond_1

    sget-object v0, LaGn;->b:LaGn;

    invoke-virtual {v0, v2}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    invoke-static {p0, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;LaGu;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, p3, v0}, Lala;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;LaFO;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 127
    const-string v0, "application/vnd.google-apps"

    .line 128
    invoke-static {p1, p2, p3, v0}, Lala;->a(Landroid/net/Uri;LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 129
    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 130
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lala;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    .line 164
    const-string v1, "openOfflineVersion"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 169
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 150
    const-string v0, "null context"

    invoke-static {p0, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 154
    const-string v1, "editMode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 155
    return-object v0
.end method

.method public static a(Landroid/net/Uri;LaFO;Ljava/lang/String;LaGv;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p3}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lala;->a(Landroid/net/Uri;LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/net/Uri;LaFO;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v1, "accountName"

    invoke-static {p1}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "docListTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    return-object v0
.end method

.method public static a(LaGu;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    invoke-interface {p0}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-interface {p0}, LaGu;->g()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-interface {p0}, LaGu;->j()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-static {v0, v1, v2}, Lala;->a(LaGv;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaGv;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    sget-object v0, LaGv;->d:LaGv;

    sget-object v1, LaGv;->k:LaGv;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method
