.class public LaqA;
.super Ljava/lang/Object;
.source "PreviewPageFetcher.java"


# instance fields
.field private final a:LaGM;

.field private final a:LanP;

.field private final a:Laoh;

.field private final a:LapR;

.field private final a:LapZ;

.field private final a:LbsW;

.field private final a:LtK;


# direct methods
.method private constructor <init>(LaGM;LtK;LanR;LapZ;LapR;Laoh;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    invoke-static {v0}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    iput-object v0, p0, LaqA;->a:LbsW;

    .line 125
    iput-object p1, p0, LaqA;->a:LaGM;

    .line 126
    iput-object p2, p0, LaqA;->a:LtK;

    .line 127
    invoke-virtual {p3}, LanR;->a()LanP;

    move-result-object v0

    iput-object v0, p0, LaqA;->a:LanP;

    .line 128
    iput-object p4, p0, LaqA;->a:LapZ;

    .line 129
    iput-object p5, p0, LaqA;->a:LapR;

    .line 130
    iput-object p6, p0, LaqA;->a:Laoh;

    .line 131
    return-void
.end method

.method synthetic constructor <init>(LaGM;LtK;LanR;LapZ;LapR;Laoh;LaqB;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct/range {p0 .. p6}, LaqA;-><init>(LaGM;LtK;LanR;LapZ;LapR;Laoh;)V

    return-void
.end method

.method static synthetic a(LaqA;)LaGM;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LaqA;)LanP;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:LanP;

    return-object v0
.end method

.method static synthetic a(LaqA;)Laoh;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:Laoh;

    return-object v0
.end method

.method static synthetic a(LaqA;)LapR;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:LapR;

    return-object v0
.end method

.method static synthetic a(LaqA;)LapZ;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:LapZ;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGn;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LaGn;",
            ")",
            "LbsU",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, LaqA;->a:LanP;

    invoke-virtual {v0, p1}, LanP;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 180
    new-instance v1, LaqC;

    invoke-direct {v1, p0, p2, p1}, LaqC;-><init>(LaqA;LaGn;Lcom/google/android/apps/docs/utils/FetchSpec;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGv;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LaGv;",
            ")",
            "LbsU",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    invoke-static {}, Laou;->a()Laou;

    move-result-object v0

    .line 192
    iget-object v1, p0, LaqA;->a:Laoh;

    .line 193
    invoke-virtual {v1, p1}, Laoh;->a(Ljava/lang/Object;)LbsU;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Laou;->a(LbsU;)V

    .line 195
    new-instance v2, LaqD;

    invoke-direct {v2, p0, p2}, LaqD;-><init>(LaqA;LaGv;)V

    .line 196
    invoke-static {v1, v2}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v1

    .line 207
    invoke-virtual {v0, v1}, Laou;->b(LbsU;)V

    .line 208
    return-object v1
.end method

.method static synthetic a(LaqA;)LtK;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LaqA;->a:LtK;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, LaqA;->a:LanP;

    .line 154
    invoke-virtual {v0, p1}, LanP;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    .line 155
    new-instance v1, LaqB;

    invoke-direct {v1, p0}, LaqB;-><init>(LaqA;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbsF;)LbsU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, LaqA;->a:LbsW;

    new-instance v1, LaqF;

    invoke-direct {v1, p0, p1}, LaqF;-><init>(LaqA;Lcom/google/android/apps/docs/utils/FetchSpec;)V

    .line 213
    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 214
    invoke-static {v0}, LbsK;->a(LbsU;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGv;Ljava/lang/String;)LbsU;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LaGv;",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    const-string v0, "image/gif"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaqA;->a:LtK;

    sget-object v3, Lry;->ag:Lry;

    .line 136
    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-direct {p0, p1}, LaqA;->b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    .line 139
    :cond_0
    sget-object v0, LaGv;->b:LaGv;

    invoke-virtual {v0, p2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaqA;->a:LtK;

    sget-object v3, Lry;->ak:Lry;

    .line 140
    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 141
    :goto_1
    sget-object v3, LaGv;->i:LaGv;

    invoke-virtual {v3, p2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LaqA;->a:LtK;

    sget-object v4, Lry;->al:Lry;

    .line 142
    invoke-interface {v3, v4}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 143
    :goto_2
    if-nez v0, :cond_1

    if-eqz v1, :cond_4

    .line 144
    :cond_1
    invoke-direct {p0, p1, p2}, LaqA;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGv;)LbsU;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 140
    goto :goto_1

    :cond_3
    move v1, v2

    .line 142
    goto :goto_2

    .line 146
    :cond_4
    invoke-static {p3}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LaqA;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGn;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, LaqA;->a:LanP;

    invoke-virtual {v0}, LanP;->a()V

    .line 239
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, LaqA;->a:LanP;

    invoke-virtual {v0, p1, p2}, LanP;->a(J)V

    .line 219
    return-void
.end method
