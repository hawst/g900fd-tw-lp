.class public abstract Lbrj;
.super Ljava/lang/Object;
.source "CharSource.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/io/BufferedReader;
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lbrj;->a()Ljava/io/Reader;

    move-result-object v0

    .line 81
    instance-of v1, v0, Ljava/io/BufferedReader;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/io/BufferedReader;

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public abstract a()Ljava/io/Reader;
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 155
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v1

    .line 157
    :try_start_0
    invoke-virtual {p0}, Lbrj;->a()Ljava/io/BufferedReader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/BufferedReader;

    .line 158
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 162
    invoke-virtual {v1}, Lbrm;->close()V

    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    :try_start_1
    invoke-virtual {v1, v0}, Lbrm;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbrm;->close()V

    throw v0
.end method
