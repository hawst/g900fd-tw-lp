.class public final LadF;
.super Ljava/lang/Object;
.source "FileInstanceHelperImpl.java"

# interfaces
.implements LadE;


# instance fields
.field private final a:LaGg;

.field private final a:Lakz;

.field private final a:Lamn;


# direct methods
.method constructor <init>(LaGg;Lamn;Lakz;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, LadF;->a:LaGg;

    .line 34
    iput-object p2, p0, LadF;->a:Lamn;

    .line 35
    iput-object p3, p0, LadF;->a:Lakz;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;Z)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    if-eqz p2, :cond_1

    invoke-direct {p0}, LadF;->c()Ljava/io/File;

    move-result-object v0

    .line 77
    :cond_0
    :goto_0
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lamv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 80
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v0

    if-nez v0, :cond_2

    .line 81
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error creating randomized directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    invoke-direct {p0}, LadF;->e()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_2
    if-nez p2, :cond_4

    .line 86
    invoke-virtual {v1, v4, v3}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v4, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v0

    if-nez v0, :cond_4

    .line 87
    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error making randomized directory world-readable"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_4
    if-nez p1, :cond_6

    move-object v0, v1

    .line 107
    :cond_5
    return-object v0

    .line 96
    :cond_6
    invoke-static {p1}, Lalp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 101
    if-nez p2, :cond_5

    .line 102
    invoke-virtual {v0, v4, v3}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v1

    if-nez v1, :cond_5

    .line 103
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error making file world-readable"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static b(Ljava/io/File;)J
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 185
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 187
    if-nez v3, :cond_1

    .line 199
    :cond_0
    return-wide v0

    .line 191
    :cond_1
    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 192
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 193
    invoke-static {v5}, LadF;->b(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 191
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 195
    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v0, v6

    goto :goto_1
.end method

.method private c()Ljava/io/File;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, LadF;->a:Lamn;

    invoke-interface {v0}, Lamn;->b()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/io/File;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LadF;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/io/File;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, LadF;->a:Lamn;

    invoke-interface {v0}, Lamn;->c()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x1

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, LadF;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LadY;)J
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p1}, LadY;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/io/File;)J
    .locals 2

    .prologue
    .line 204
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LadF;->b(Ljava/io/File;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method public a()Ljava/io/File;
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LadF;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LadF;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, LadF;->a:Lamn;

    invoke-interface {v0}, Lamn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LadF;->a:Lakz;

    invoke-interface {v0}, Lakz;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGp;)V
    .locals 0

    .prologue
    .line 124
    invoke-virtual {p1}, LaGp;->e()V

    .line 125
    return-void
.end method

.method public a(Ladw;)Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, LadF;->a:LaGg;

    invoke-virtual {p1}, Ladw;->a()LaGp;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(LaGp;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/File;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 129
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    invoke-static {p1}, Lalp;->b(Ljava/io/File;)Z

    move-result v0

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 134
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 141
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    .line 142
    if-eqz v3, :cond_0

    .line 148
    :try_start_0
    invoke-direct {p0}, LadF;->c()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 156
    :goto_1
    :try_start_1
    invoke-direct {p0}, LadF;->d()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 162
    :goto_2
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    .line 163
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v1

    and-int/2addr v0, v1

    goto :goto_0

    .line 149
    :catch_0
    move-exception v2

    move v2, v1

    .line 151
    goto :goto_1

    .line 157
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public b()J
    .locals 4

    .prologue
    .line 181
    invoke-direct {p0}, LadF;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LadF;->b(Ljava/io/File;)J

    move-result-wide v0

    invoke-direct {p0}, LadF;->e()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, LadF;->b(Ljava/io/File;)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public b()Ljava/io/File;
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LadF;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 57
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LadF;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public b(LaGp;)V
    .locals 0

    .prologue
    .line 171
    invoke-virtual {p1}, LaGp;->f()V

    .line 172
    return-void
.end method
