.class public final LbjG;
.super Ljava/lang/Object;
.source "CacheBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final a:LbjA;

.field static final a:LbjO;

.field static final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<+",
            "LbjE;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/logging/Logger;

.field static final b:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LbjE;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:I

.field a:J

.field a:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field a:Lbkv;

.field a:Lblf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lblf",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field a:Lbln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbln",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field a:Z

.field b:I

.field b:J

.field b:LbiD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field b:LbjA;

.field b:Lbkv;

.field c:J

.field c:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<+",
            "LbjE;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field e:J


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 159
    new-instance v0, LbjH;

    invoke-direct {v0}, LbjH;-><init>()V

    invoke-static {v0}, Lbjw;->a(Ljava/lang/Object;)Lbjv;

    move-result-object v0

    sput-object v0, LbjG;->a:Lbjv;

    .line 176
    new-instance v1, LbjO;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    move-wide v10, v2

    move-wide v12, v2

    invoke-direct/range {v1 .. v13}, LbjO;-><init>(JJJJJJ)V

    sput-object v1, LbjG;->a:LbjO;

    .line 178
    new-instance v0, LbjI;

    invoke-direct {v0}, LbjI;-><init>()V

    sput-object v0, LbjG;->b:Lbjv;

    .line 199
    new-instance v0, LbjJ;

    invoke-direct {v0}, LbjJ;-><init>()V

    sput-object v0, LbjG;->a:LbjA;

    .line 207
    const-class v0, LbjG;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LbjG;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, LbjG;->a:Z

    .line 213
    iput v1, p0, LbjG;->a:I

    .line 214
    iput v1, p0, LbjG;->b:I

    .line 215
    iput-wide v2, p0, LbjG;->a:J

    .line 216
    iput-wide v2, p0, LbjG;->b:J

    .line 222
    iput-wide v2, p0, LbjG;->c:J

    .line 223
    iput-wide v2, p0, LbjG;->d:J

    .line 224
    iput-wide v2, p0, LbjG;->e:J

    .line 232
    sget-object v0, LbjG;->a:Lbjv;

    iput-object v0, p0, LbjG;->c:Lbjv;

    .line 235
    return-void
.end method

.method public static a()LbjG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjG",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    new-instance v0, LbjG;

    invoke-direct {v0}, LbjG;-><init>()V

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 798
    iget-wide v0, p0, LbjG;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 799
    return-void

    .line 798
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 802
    iget-object v2, p0, LbjG;->a:Lbln;

    if-nez v2, :cond_2

    .line 803
    iget-wide v2, p0, LbjG;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :goto_0
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 813
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 803
    goto :goto_0

    .line 805
    :cond_2
    iget-boolean v2, p0, LbjG;->a:Z

    if-eqz v2, :cond_4

    .line 806
    iget-wide v2, p0, LbjG;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :goto_2
    const-string v1, "weigher requires maximumWeight"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 808
    :cond_4
    iget-wide v0, p0, LbjG;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 809
    sget-object v0, LbjG;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method a()I
    .locals 2

    .prologue
    .line 333
    iget v0, p0, LbjG;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LbjG;->a:I

    goto :goto_0
.end method

.method a()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 484
    iget-wide v2, p0, LbjG;->c:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, LbjG;->d:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, LbjG;->a:Lbln;

    if-nez v0, :cond_2

    iget-wide v0, p0, LbjG;->a:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, LbjG;->b:J

    goto :goto_0
.end method

.method a()LbiD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, LbjG;->a:LbiD;

    invoke-virtual {p0}, LbjG;->a()Lbkv;

    move-result-object v1

    invoke-virtual {v1}, Lbkv;->a()LbiD;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiD;

    return-object v0
.end method

.method a(Z)LbjA;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, LbjG;->b:LbjA;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, LbjG;->b:LbjA;

    .line 703
    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, LbjA;->a()LbjA;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, LbjG;->a:LbjA;

    goto :goto_0
.end method

.method public a()LbjF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>()",
            "LbjF",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 792
    invoke-direct {p0}, LbjG;->b()V

    .line 793
    invoke-direct {p0}, LbjG;->a()V

    .line 794
    new-instance v0, Lbko;

    invoke-direct {v0, p0}, Lbko;-><init>(LbjG;)V

    return-object v0
.end method

.method public a(I)LbjG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    iget v0, p0, LbjG;->a:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "initial capacity was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, LbjG;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 327
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 328
    iput p1, p0, LbjG;->a:I

    .line 329
    return-object p0

    :cond_0
    move v0, v2

    .line 325
    goto :goto_0

    :cond_1
    move v1, v2

    .line 327
    goto :goto_1
.end method

.method public a(J)LbjG;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 394
    iget-wide v4, p0, LbjG;->a:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 396
    iget-wide v4, p0, LbjG;->b:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 398
    iget-object v0, p0, LbjG;->a:Lbln;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "maximum size can not be combined with weigher"

    invoke-static {v0, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 399
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_3

    :goto_3
    const-string v0, "maximum size must not be negative"

    invoke-static {v1, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 400
    iput-wide p1, p0, LbjG;->a:J

    .line 401
    return-object p0

    :cond_0
    move v0, v2

    .line 394
    goto :goto_0

    :cond_1
    move v0, v2

    .line 396
    goto :goto_1

    :cond_2
    move v0, v2

    .line 398
    goto :goto_2

    :cond_3
    move v1, v2

    .line 399
    goto :goto_3
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)LbjG;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 599
    iget-wide v4, p0, LbjG;->c:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 601
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 602
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LbjG;->c:J

    .line 603
    return-object p0

    :cond_0
    move v0, v2

    .line 599
    goto :goto_0

    :cond_1
    move v0, v2

    .line 601
    goto :goto_1
.end method

.method a(Lbkv;)LbjG;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkv;",
            ")",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, LbjG;->a:Lbkv;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LbjG;->a:Lbkv;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 516
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkv;

    iput-object v0, p0, LbjG;->a:Lbkv;

    .line 517
    return-object p0

    :cond_0
    move v0, v2

    .line 515
    goto :goto_0
.end method

.method public a(Lblf;)LbjG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "Lblf",
            "<-TK1;-TV1;>;)",
            "LbjG",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 730
    iget-object v0, p0, LbjG;->a:Lblf;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 735
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblf;

    iput-object v0, p0, LbjG;->a:Lblf;

    .line 736
    return-object p0

    .line 730
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbln;)LbjG;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "Lbln",
            "<-TK1;-TV1;>;)",
            "LbjG",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470
    iget-object v0, p0, LbjG;->a:Lbln;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 471
    iget-boolean v0, p0, LbjG;->a:Z

    if-eqz v0, :cond_0

    .line 472
    iget-wide v4, p0, LbjG;->a:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "weigher can not be combined with maximum size"

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v4, p0, LbjG;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 479
    :cond_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbln;

    iput-object v0, p0, LbjG;->a:Lbln;

    .line 480
    return-object p0

    :cond_1
    move v0, v2

    .line 470
    goto :goto_0

    :cond_2
    move v0, v2

    .line 472
    goto :goto_1
.end method

.method public a(LbjM;)LbjP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "LbjM",
            "<-TK1;TV1;>;)",
            "LbjP",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 775
    invoke-direct {p0}, LbjG;->b()V

    .line 776
    new-instance v0, Lbkn;

    invoke-direct {v0, p0, p1}, Lbkn;-><init>(LbjG;LbjM;)V

    return-object v0
.end method

.method a()Lbjv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbjv",
            "<+",
            "LbjE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 759
    iget-object v0, p0, LbjG;->c:Lbjv;

    return-object v0
.end method

.method a()Lbkv;
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, LbjG;->a:Lbkv;

    sget-object v1, Lbkv;->a:Lbkv;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkv;

    return-object v0
.end method

.method a()Lblf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>()",
            "Lblf",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 742
    iget-object v0, p0, LbjG;->a:Lblf;

    sget-object v1, LbjK;->a:LbjK;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblf;

    return-object v0
.end method

.method a()Lbln;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>()",
            "Lbln",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 493
    iget-object v0, p0, LbjG;->a:Lbln;

    sget-object v1, LbjL;->a:LbjL;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbln;

    return-object v0
.end method

.method b()I
    .locals 2

    .prologue
    .line 375
    iget v0, p0, LbjG;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LbjG;->b:I

    goto :goto_0
.end method

.method b()J
    .locals 4

    .prologue
    .line 607
    iget-wide v0, p0, LbjG;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LbjG;->c:J

    goto :goto_0
.end method

.method b()LbiD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbiD",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, LbjG;->b:LbiD;

    invoke-virtual {p0}, LbjG;->b()Lbkv;

    move-result-object v1

    invoke-virtual {v1}, Lbkv;->a()LbiD;

    move-result-object v1

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiD;

    return-object v0
.end method

.method public b()LbjG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 511
    sget-object v0, Lbkv;->c:Lbkv;

    invoke-virtual {p0, v0}, LbjG;->a(Lbkv;)LbjG;

    move-result-object v0

    return-object v0
.end method

.method public b(J)LbjG;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 430
    iget-wide v4, p0, LbjG;->b:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 432
    iget-wide v4, p0, LbjG;->a:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 434
    iput-wide p1, p0, LbjG;->b:J

    .line 435
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    :goto_2
    const-string v0, "maximum weight must not be negative"

    invoke-static {v1, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 436
    return-object p0

    :cond_0
    move v0, v2

    .line 430
    goto :goto_0

    :cond_1
    move v0, v2

    .line 432
    goto :goto_1

    :cond_2
    move v1, v2

    .line 435
    goto :goto_2
.end method

.method public b(JLjava/util/concurrent/TimeUnit;)LbjG;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 633
    iget-wide v4, p0, LbjG;->d:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LbjG;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 635
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 636
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LbjG;->d:J

    .line 637
    return-object p0

    :cond_0
    move v0, v2

    .line 633
    goto :goto_0

    :cond_1
    move v0, v2

    .line 635
    goto :goto_1
.end method

.method b(Lbkv;)LbjG;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbkv;",
            ")",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 570
    iget-object v0, p0, LbjG;->b:Lbkv;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Value strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LbjG;->b:Lbkv;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 571
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkv;

    iput-object v0, p0, LbjG;->b:Lbkv;

    .line 572
    return-object p0

    :cond_0
    move v0, v2

    .line 570
    goto :goto_0
.end method

.method b()Lbkv;
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, LbjG;->b:Lbkv;

    sget-object v1, Lbkv;->a:Lbkv;

    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkv;

    return-object v0
.end method

.method c()J
    .locals 4

    .prologue
    .line 641
    iget-wide v0, p0, LbjG;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LbjG;->d:J

    goto :goto_0
.end method

.method public c()LbjG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbjG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 542
    sget-object v0, Lbkv;->c:Lbkv;

    invoke-virtual {p0, v0}, LbjG;->b(Lbkv;)LbjG;

    move-result-object v0

    return-object v0
.end method

.method d()J
    .locals 4

    .prologue
    .line 681
    iget-wide v0, p0, LbjG;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LbjG;->e:J

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, -0x1

    const-wide/16 v6, -0x1

    .line 822
    invoke-static {p0}, LbiL;->a(Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 823
    iget v1, p0, LbjG;->a:I

    if-eq v1, v3, :cond_0

    .line 824
    const-string v1, "initialCapacity"

    iget v2, p0, LbjG;->a:I

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    .line 826
    :cond_0
    iget v1, p0, LbjG;->b:I

    if-eq v1, v3, :cond_1

    .line 827
    const-string v1, "concurrencyLevel"

    iget v2, p0, LbjG;->b:I

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    .line 829
    :cond_1
    iget-wide v2, p0, LbjG;->a:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 830
    const-string v1, "maximumSize"

    iget-wide v2, p0, LbjG;->a:J

    invoke-virtual {v0, v1, v2, v3}, LbiN;->a(Ljava/lang/String;J)LbiN;

    .line 832
    :cond_2
    iget-wide v2, p0, LbjG;->b:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 833
    const-string v1, "maximumWeight"

    iget-wide v2, p0, LbjG;->b:J

    invoke-virtual {v0, v1, v2, v3}, LbiN;->a(Ljava/lang/String;J)LbiN;

    .line 835
    :cond_3
    iget-wide v2, p0, LbjG;->c:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 836
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LbjG;->c:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 838
    :cond_4
    iget-wide v2, p0, LbjG;->d:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    .line 839
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LbjG;->d:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 841
    :cond_5
    iget-object v1, p0, LbjG;->a:Lbkv;

    if-eqz v1, :cond_6

    .line 842
    const-string v1, "keyStrength"

    iget-object v2, p0, LbjG;->a:Lbkv;

    invoke-virtual {v2}, Lbkv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 844
    :cond_6
    iget-object v1, p0, LbjG;->b:Lbkv;

    if-eqz v1, :cond_7

    .line 845
    const-string v1, "valueStrength"

    iget-object v2, p0, LbjG;->b:Lbkv;

    invoke-virtual {v2}, Lbkv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    .line 847
    :cond_7
    iget-object v1, p0, LbjG;->a:LbiD;

    if-eqz v1, :cond_8

    .line 848
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, LbiN;->a(Ljava/lang/Object;)LbiN;

    .line 850
    :cond_8
    iget-object v1, p0, LbjG;->b:LbiD;

    if-eqz v1, :cond_9

    .line 851
    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, LbiN;->a(Ljava/lang/Object;)LbiN;

    .line 853
    :cond_9
    iget-object v1, p0, LbjG;->a:Lblf;

    if-eqz v1, :cond_a

    .line 854
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, LbiN;->a(Ljava/lang/Object;)LbiN;

    .line 856
    :cond_a
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
