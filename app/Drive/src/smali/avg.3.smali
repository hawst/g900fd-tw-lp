.class public final enum Lavg;
.super Ljava/lang/Enum;
.source "DisplayType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavg;

.field private static final synthetic a:[Lavg;

.field public static final enum b:Lavg;

.field public static final enum c:Lavg;

.field public static final enum d:Lavg;

.field public static final enum e:Lavg;

.field public static final enum f:Lavg;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lavg;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v3}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->a:Lavg;

    .line 21
    new-instance v0, Lavg;

    const-string v1, "HTML"

    invoke-direct {v0, v1, v4}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->b:Lavg;

    .line 22
    new-instance v0, Lavg;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v5}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->c:Lavg;

    .line 23
    new-instance v0, Lavg;

    const-string v1, "PDF"

    invoke-direct {v0, v1, v6}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->d:Lavg;

    .line 24
    new-instance v0, Lavg;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v7}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->e:Lavg;

    .line 25
    new-instance v0, Lavg;

    const-string v1, "VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lavg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavg;->f:Lavg;

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [Lavg;

    sget-object v1, Lavg;->a:Lavg;

    aput-object v1, v0, v3

    sget-object v1, Lavg;->b:Lavg;

    aput-object v1, v0, v4

    sget-object v1, Lavg;->c:Lavg;

    aput-object v1, v0, v5

    sget-object v1, Lavg;->d:Lavg;

    aput-object v1, v0, v6

    sget-object v1, Lavg;->e:Lavg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lavg;->f:Lavg;

    aput-object v2, v0, v1

    sput-object v0, Lavg;->a:[Lavg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    return-void
.end method

.method public static a(Ljava/lang/String;)Lavg;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 31
    invoke-static {p0}, Lavi;->a(Ljava/lang/String;)Lavj;

    move-result-object v1

    .line 32
    if-nez v1, :cond_0

    .line 33
    const-string v1, "DisplayType"

    const-string v2, "ContentType has no family %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :goto_0
    return-object v0

    .line 37
    :cond_0
    sget-object v2, Lavh;->a:[I

    invoke-virtual {v1}, Lavj;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 53
    :cond_1
    const-string v1, "DisplayType"

    const-string v2, "Can\'t detect display type of %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 39
    :pswitch_0
    const-string v1, "application/pdf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    sget-object v0, Lavg;->d:Lavg;

    goto :goto_0

    .line 44
    :pswitch_1
    sget-object v0, Lavg;->a:Lavg;

    goto :goto_0

    .line 46
    :pswitch_2
    sget-object v0, Lavg;->c:Lavg;

    goto :goto_0

    .line 48
    :pswitch_3
    sget-object v0, Lavg;->e:Lavg;

    goto :goto_0

    .line 50
    :pswitch_4
    sget-object v0, Lavg;->f:Lavg;

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lavg;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lavg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavg;

    return-object v0
.end method

.method public static values()[Lavg;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lavg;->a:[Lavg;

    invoke-virtual {v0}, [Lavg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavg;

    return-object v0
.end method
