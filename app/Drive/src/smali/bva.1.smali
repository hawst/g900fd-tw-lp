.class Lbva;
.super Ljava/lang/Object;
.source "Annotations.java"


# instance fields
.field private a:LbjM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjM",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final a:LbjP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjP",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Lbvb;

    invoke-direct {v0, p0}, Lbvb;-><init>(Lbva;)V

    iput-object v0, p0, Lbva;->a:LbjM;

    .line 223
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->b()LbjG;

    move-result-object v0

    iget-object v1, p0, Lbva;->a:LbjM;

    .line 224
    invoke-virtual {v0, v1}, LbjG;->a(LbjM;)LbjP;

    move-result-object v0

    iput-object v0, p0, Lbva;->a:LbjP;

    .line 230
    iput-object p1, p0, Lbva;->a:Ljava/util/Collection;

    .line 231
    return-void
.end method

.method static synthetic a(Lbva;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lbva;->a:Ljava/util/Collection;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lbva;->a:LbjP;

    invoke-interface {v0, p1}, LbjP;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
