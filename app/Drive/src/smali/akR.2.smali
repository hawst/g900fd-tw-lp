.class public LakR;
.super Ljava/lang/Object;
.source "DiskCache.java"


# instance fields
.field private final a:F

.field private final a:I

.field private a:J

.field private final a:LakG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakG",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LakU;

.field private final a:Lalo;

.field private final a:Lbpi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpi",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "LakT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LakU;IF)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, LakS;

    invoke-direct {v0, p0}, LakS;-><init>(LakR;)V

    iput-object v0, p0, LakR;->a:LakG;

    .line 118
    invoke-static {}, Lbmr;->a()Lbmr;

    move-result-object v0

    iput-object v0, p0, LakR;->a:Lbpi;

    .line 119
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LakR;->a:Ljava/util/Set;

    .line 122
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LakR;->a:Ljava/util/Map;

    .line 124
    new-instance v0, Lalp;

    invoke-direct {v0}, Lalp;-><init>()V

    iput-object v0, p0, LakR;->a:Lalo;

    .line 128
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakU;

    iput-object v0, p0, LakR;->a:LakU;

    .line 129
    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 130
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MaxSizeRatio should be greater than 0 and less than 1. Value supplied: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 132
    iput p2, p0, LakR;->a:I

    .line 133
    iput p3, p0, LakR;->a:F

    .line 134
    invoke-direct {p0}, LakR;->a()J

    move-result-wide v0

    iput-wide v0, p0, LakR;->a:J

    .line 135
    return-void

    :cond_0
    move v0, v2

    .line 129
    goto :goto_0

    :cond_1
    move v1, v2

    .line 130
    goto :goto_1
.end method

.method private declared-synchronized a()J
    .locals 5

    .prologue
    .line 449
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 450
    :try_start_0
    invoke-direct {p0}, LakR;->a()Ljava/util/SortedSet;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakT;

    .line 451
    invoke-virtual {v0}, LakT;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 452
    goto :goto_0

    .line 453
    :cond_0
    monitor-exit p0

    return-wide v2

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LaFO;Ljava/lang/String;Ljava/lang/String;)LakD;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    monitor-enter p0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 219
    :goto_0
    if-eqz v1, :cond_1

    :try_start_0
    iget-object v0, p0, LakR;->a:LakU;

    invoke-virtual {v0, p1, p3, p2}, LakU;->a(LaFO;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 222
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not a regular file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 224
    invoke-static {v0, v1}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v2

    .line 225
    invoke-direct {p0}, LakR;->a()Ljava/util/SortedSet;

    move-result-object v3

    .line 226
    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 231
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/File;->setLastModified(J)Z

    .line 232
    invoke-static {v0, v1}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v1

    .line 233
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 234
    invoke-direct {p0, v0}, LakR;->a(Ljava/io/File;)V

    .line 235
    iget-object v1, p0, LakR;->a:LakG;

    invoke-static {v0, v1}, LakD;->a(Ljava/lang/Object;LakG;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 237
    :goto_2
    monitor-exit p0

    return-object v0

    .line 218
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 219
    :cond_1
    :try_start_1
    iget-object v0, p0, LakR;->a:LakU;

    .line 220
    invoke-virtual {v0, p1, p2}, LakU;->a(LaFO;Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 237
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LakU;IF)LakR;
    .locals 1

    .prologue
    .line 505
    new-instance v0, LakR;

    invoke-direct {v0, p0, p1, p2}, LakR;-><init>(LakU;IF)V

    return-object v0
.end method

.method private declared-synchronized a()Ljava/util/SortedSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<",
            "LakT;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 457
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LakR;->a:Ljava/util/SortedSet;

    if-nez v0, :cond_3

    .line 458
    iget-object v0, p0, LakR;->a:LakU;

    invoke-virtual {v0}, LakU;->a()LbmF;

    move-result-object v0

    .line 460
    invoke-static {}, LbpU;->a()Ljava/util/TreeSet;

    move-result-object v1

    iput-object v1, p0, LakR;->a:Ljava/util/SortedSet;

    .line 462
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 463
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 464
    iget-object v1, p0, LakR;->a:Ljava/util/SortedSet;

    const/4 v5, 0x0

    invoke-static {v0, v5}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 467
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 468
    array-length v1, v5

    if-gt v1, v2, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 470
    array-length v1, v5

    if-ne v1, v2, :cond_2

    .line 471
    iget-object v0, p0, LakR;->a:Ljava/util/SortedSet;

    const/4 v1, 0x0

    aget-object v1, v5, v1

    const/4 v5, 0x1

    invoke-static {v1, v5}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v1, v3

    .line 468
    goto :goto_1

    .line 474
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 479
    :cond_3
    iget-object v0, p0, LakR;->a:Ljava/util/SortedSet;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method private declared-synchronized a()V
    .locals 6

    .prologue
    .line 439
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LakR;->a()Ljava/util/SortedSet;

    move-result-object v1

    .line 440
    invoke-direct {p0}, LakR;->b()J

    move-result-wide v2

    long-to-float v0, v2

    iget v2, p0, LakR;->a:F

    mul-float/2addr v0, v2

    float-to-long v2, v0

    .line 441
    :goto_0
    invoke-interface {v1}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v4, p0, LakR;->a:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    invoke-interface {v1}, Ljava/util/SortedSet;->size()I

    move-result v0

    iget v4, p0, LakR;->a:I

    if-le v0, v4, :cond_1

    .line 442
    :cond_0
    invoke-interface {v1}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakT;

    .line 443
    invoke-interface {v1, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 444
    invoke-virtual {p0, v0}, LakR;->a(LakT;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 439
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 446
    :cond_1
    monitor-exit p0

    return-void
.end method

.method static synthetic a(LakR;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, LakR;->b(Ljava/io/File;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 509
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LakR;->a:Lbpi;

    invoke-interface {v0, p1}, Lbpi;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    monitor-exit p0

    return-void

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/io/File;LaFO;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 326
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-static {v3}, LbiT;->a(Z)V

    .line 327
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v3

    invoke-static {v3}, LbiT;->a(Z)V

    .line 330
    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 335
    :try_start_1
    invoke-direct {p0}, LakR;->a()V

    .line 337
    if-eqz p3, :cond_1

    .line 338
    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, LakR;->a:LakU;

    invoke-virtual {v2, p2, p3, p4}, LakU;->a(LaFO;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 340
    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 341
    invoke-static {v2, v0}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v1

    .line 342
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 343
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v6

    .line 344
    if-nez v6, :cond_3

    .line 345
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete cache file: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 357
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 358
    :goto_2
    :try_start_3
    const-string v2, "DiskCache"

    const-string v3, "Failed to store cache entry: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 359
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361
    :catchall_0
    move-exception v0

    move-object v2, v1

    .line 362
    :goto_3
    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 363
    const-string v1, "DiskCache"

    const-string v3, "Failed to delete file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 326
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v2

    .line 337
    goto :goto_0

    .line 338
    :cond_2
    :try_start_5
    iget-object v2, p0, LakR;->a:LakU;

    .line 339
    invoke-virtual {v2, p2, p4}, LakU;->a(LaFO;Ljava/lang/String;)Ljava/io/File;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v2

    goto :goto_1

    .line 347
    :cond_3
    :try_start_6
    iget-wide v6, p0, LakR;->a:J

    sub-long v4, v6, v4

    iput-wide v4, p0, LakR;->a:J

    .line 349
    :cond_4
    invoke-direct {p0, v3, v2}, LakR;->a(Ljava/io/File;Ljava/io/File;)V

    .line 350
    iget-wide v4, p0, LakR;->a:J

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, LakR;->a:J

    .line 351
    invoke-direct {p0}, LakR;->a()Ljava/util/SortedSet;

    move-result-object v3

    .line 352
    invoke-static {v2, v0}, LakT;->a(Ljava/io/File;Z)LakT;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 353
    if-eqz v1, :cond_5

    .line 354
    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 367
    :cond_5
    monitor-exit p0

    return-void

    .line 361
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catchall_3
    move-exception v0

    goto :goto_3

    .line 357
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private a(Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 530
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    .line 531
    if-nez v0, :cond_0

    .line 532
    const-string v0, "DiskCache"

    const-string v1, "Failed to move file: %s to: %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 533
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to move file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 526
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LakR;->a:Lbpi;

    invoke-interface {v0, p1}, Lbpi;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()J
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 485
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, LakR;->a:LakU;

    invoke-virtual {v1}, LakU;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 486
    invoke-static {}, LakQ;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 487
    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBlocksLong()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    mul-long/2addr v0, v2

    .line 489
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    goto :goto_0

    .line 485
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LakR;->a:Lbpi;

    invoke-interface {v0, p1}, Lbpi;->remove(Ljava/lang/Object;)Z

    .line 514
    invoke-direct {p0, p1}, LakR;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LakR;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 516
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    .line 517
    if-eqz v2, :cond_1

    .line 518
    iget-wide v2, p0, LakR;->a:J

    sub-long v0, v2, v0

    iput-wide v0, p0, LakR;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 523
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 520
    :cond_1
    :try_start_1
    const-string v0, "DiskCache"

    const-string v1, "Failed to delete cached file: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(LaFO;Ljava/lang/String;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Ljava/lang/String;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LakR;->a(LaFO;Ljava/lang/String;Ljava/lang/String;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/io/File;LaFO;Ljava/lang/String;)LakD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "LaFO;",
            "Ljava/lang/String;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, LakR;->a(Ljava/io/File;LaFO;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0, p2, p3}, LakR;->a(LaFO;Ljava/lang/String;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/io/File;LaFO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 321
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0, p3}, LakR;->a(Ljava/io/File;LaFO;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    monitor-exit p0

    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaFO;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 245
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, LakR;->a:LakU;

    invoke-virtual {v0, p1, p2}, LakU;->a(LaFO;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 249
    iget-object v1, p0, LakR;->a:Lalo;

    invoke-interface {v1, v0}, Lalo;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method declared-synchronized a(LakT;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 409
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LakT;->a()Ljava/io/File;

    move-result-object v1

    .line 410
    invoke-direct {p0, v1}, LakR;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 411
    iget-object v2, p0, LakR;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 414
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 415
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 416
    if-eqz v0, :cond_2

    .line 417
    iget-wide v4, p0, LakR;->a:J

    sub-long v2, v4, v2

    iput-wide v2, p0, LakR;->a:J

    .line 418
    invoke-virtual {p1}, LakT;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 419
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 420
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 424
    :cond_2
    :try_start_2
    const-string v2, "DiskCache"

    const-string v3, "Failed to delete cached file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
