.class public LWP;
.super LWN;
.source "BaseSlidePickerFragment.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iput-object p2, p0, LWP;->a:Landroid/os/Handler;

    invoke-direct {p0}, LWN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v0}, LYv;->a()V

    .line 152
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    invoke-virtual {v0}, LYb;->notifyDataSetChanged()V

    .line 153
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    invoke-interface {v0}, LXz;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    invoke-interface {v0}, LXz;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 136
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    invoke-interface {v0}, LXz;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    invoke-interface {v1}, LXz;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 137
    iget-object v1, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v1, v0}, LYv;->a(F)V

    .line 139
    :cond_0
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v0}, LYv;->a()V

    .line 140
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    invoke-virtual {v0}, LYb;->notifyDataSetChanged()V

    .line 141
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v0}, LYv;->a()V

    .line 146
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    invoke-virtual {v0}, LYb;->notifyDataSetChanged()V

    .line 147
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v0}, LYv;->a()V

    .line 158
    iget-object v0, p0, LWP;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    invoke-virtual {v0}, LYb;->notifyDataSetChanged()V

    .line 160
    iget-object v0, p0, LWP;->a:Landroid/os/Handler;

    new-instance v1, LWQ;

    invoke-direct {v1, p0}, LWQ;-><init>(LWP;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 166
    return-void
.end method
