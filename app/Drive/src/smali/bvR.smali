.class final enum LbvR;
.super Ljava/lang/Enum;
.source "Indexer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbvR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbvR;

.field private static final synthetic a:[LbvR;

.field public static final enum b:LbvR;

.field public static final enum c:LbvR;

.field public static final enum d:LbvR;

.field public static final enum e:LbvR;

.field public static final enum f:LbvR;

.field public static final enum g:LbvR;

.field public static final enum h:LbvR;

.field public static final enum i:LbvR;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    new-instance v0, LbvR;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v3}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->a:LbvR;

    .line 47
    new-instance v0, LbvR;

    const-string v1, "PROVIDER_INSTANCE"

    invoke-direct {v0, v1, v4}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->b:LbvR;

    .line 48
    new-instance v0, LbvR;

    const-string v1, "PROVIDER_KEY"

    invoke-direct {v0, v1, v5}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->c:LbvR;

    .line 49
    new-instance v0, LbvR;

    const-string v1, "LINKED_KEY"

    invoke-direct {v0, v1, v6}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->d:LbvR;

    .line 50
    new-instance v0, LbvR;

    const-string v1, "UNTARGETTED"

    invoke-direct {v0, v1, v7}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->e:LbvR;

    .line 51
    new-instance v0, LbvR;

    const-string v1, "CONSTRUCTOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->f:LbvR;

    .line 52
    new-instance v0, LbvR;

    const-string v1, "CONSTANT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->g:LbvR;

    .line 53
    new-instance v0, LbvR;

    const-string v1, "EXPOSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->h:LbvR;

    .line 54
    new-instance v0, LbvR;

    const-string v1, "PROVIDED_BY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LbvR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbvR;->i:LbvR;

    .line 45
    const/16 v0, 0x9

    new-array v0, v0, [LbvR;

    sget-object v1, LbvR;->a:LbvR;

    aput-object v1, v0, v3

    sget-object v1, LbvR;->b:LbvR;

    aput-object v1, v0, v4

    sget-object v1, LbvR;->c:LbvR;

    aput-object v1, v0, v5

    sget-object v1, LbvR;->d:LbvR;

    aput-object v1, v0, v6

    sget-object v1, LbvR;->e:LbvR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LbvR;->f:LbvR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbvR;->g:LbvR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LbvR;->h:LbvR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LbvR;->i:LbvR;

    aput-object v2, v0, v1

    sput-object v0, LbvR;->a:[LbvR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbvR;
    .locals 1

    .prologue
    .line 45
    const-class v0, LbvR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbvR;

    return-object v0
.end method

.method public static values()[LbvR;
    .locals 1

    .prologue
    .line 45
    sget-object v0, LbvR;->a:[LbvR;

    invoke-virtual {v0}, [LbvR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbvR;

    return-object v0
.end method
