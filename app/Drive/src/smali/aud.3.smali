.class Laud;
.super Ljava/lang/Object;
.source "ProjectorChrome.java"

# interfaces
.implements Lawf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lawf",
        "<",
        "LawH;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laua;

.field private a:Z


# direct methods
.method constructor <init>(Laua;)V
    .locals 1

    .prologue
    .line 143
    iput-object p1, p0, Laud;->a:Laua;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Laud;->a:Z

    return-void
.end method


# virtual methods
.method public a(LawH;LawH;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    iget v0, p1, LawH;->a:F

    iget v3, p2, LawH;->a:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    move v4, v1

    .line 152
    :goto_0
    if-eqz v4, :cond_0

    .line 153
    iget-object v0, p0, Laud;->a:Laua;

    iget-object v3, p0, Laud;->a:Laua;

    invoke-static {v3}, Laua;->a(Laua;)Laue;

    move-result-object v3

    iget v5, p2, LawH;->a:F

    float-to-double v6, v5

    invoke-static {v0, v3, v6, v7}, Laua;->a(Laua;Laue;D)V

    .line 156
    :cond_0
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->b(Laua;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Laud;->a:Laua;

    invoke-virtual {v0}, Laua;->a()V

    .line 209
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v4, v2

    .line 150
    goto :goto_0

    .line 162
    :cond_3
    iget v0, p2, LawH;->b:I

    if-ltz v0, :cond_1

    .line 166
    iget v0, p1, LawH;->b:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v3, p2, LawH;->b:I

    sub-int v5, v0, v3

    .line 167
    if-eqz v5, :cond_4

    .line 168
    if-lez v5, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Laud;->a:Z

    .line 171
    :cond_4
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->a(Laua;)Landroid/view/View;

    move-result-object v6

    .line 172
    if-eqz v6, :cond_a

    .line 173
    invoke-virtual {v6}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v7, v0

    .line 174
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->a(Laua;)I

    move-result v3

    .line 175
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->b(Laua;)I

    move-result v0

    .line 178
    if-eqz v4, :cond_6

    .line 179
    invoke-static {v7, v3, v0}, Lawb;->b(III)I

    move-result v0

    .line 180
    iget-object v1, p0, Laud;->a:Laua;

    invoke-static {v1, v0}, Laua;->a(Laua;I)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 168
    goto :goto_2

    .line 185
    :cond_6
    add-int v4, v7, v5

    .line 187
    invoke-static {v4, v3, v0}, Lawb;->a(III)I

    move-result v4

    .line 188
    int-to-float v5, v4

    invoke-virtual {v6, v5}, Landroid/view/View;->setY(F)V

    .line 191
    if-eq v4, v3, :cond_7

    if-ne v4, v0, :cond_8

    :cond_7
    move v2, v1

    .line 192
    :cond_8
    iget-boolean v1, p2, LawH;->a:Z

    if-eqz v1, :cond_1

    if-nez v2, :cond_1

    .line 193
    iget-boolean v1, p0, Laud;->a:Z

    if-eqz v1, :cond_9

    .line 194
    :goto_3
    iget-object v1, p0, Laud;->a:Laua;

    invoke-static {v1, v0}, Laua;->a(Laua;I)V

    goto :goto_1

    :cond_9
    move v0, v3

    .line 193
    goto :goto_3

    .line 198
    :cond_a
    if-nez v4, :cond_1

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_1

    .line 203
    iget-boolean v0, p0, Laud;->a:Z

    if-eqz v0, :cond_b

    .line 204
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->a(Laua;)V

    goto :goto_1

    .line 206
    :cond_b
    iget-object v0, p0, Laud;->a:Laua;

    invoke-static {v0}, Laua;->b(Laua;)V

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 143
    check-cast p1, LawH;

    check-cast p2, LawH;

    invoke-virtual {p0, p1, p2}, Laud;->a(LawH;LawH;)V

    return-void
.end method
