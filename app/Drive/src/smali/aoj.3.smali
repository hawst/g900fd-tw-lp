.class public Laoj;
.super Ljava/lang/Object;
.source "DocumentHttpIssuer.java"


# instance fields
.field private final a:LTh;


# direct methods
.method public constructor <init>(LTh;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Laoj;->a:LTh;

    .line 43
    return-void
.end method


# virtual methods
.method public a(LaFO;Landroid/net/Uri;Ljava/lang/String;LMP;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    .line 57
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    new-instance v0, Ljava/net/URI;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 58
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 60
    const v2, 0x1d4c0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 62
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 64
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 65
    iget-object v0, p0, Laoj;->a:LTh;

    invoke-interface {v0, v1}, LTh;->a(Lorg/apache/http/HttpRequest;)V

    .line 70
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "export"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "wise"

    .line 73
    :goto_0
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 75
    const-string v2, "If-Range"

    invoke-virtual {v1, v2, p3}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v2, "Range"

    invoke-virtual {p4}, LMP;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    iget-object v2, p0, Laoj;->a:LTh;

    const/4 v3, 0x5

    invoke-static {v2, p1, v1, v3, v0}, LTs;->a(LTh;LaFO;Lorg/apache/http/client/methods/HttpRequestBase;ILjava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Laoj;->a:LTh;

    invoke-interface {v0}, LTh;->b()V

    .line 85
    return-void
.end method
