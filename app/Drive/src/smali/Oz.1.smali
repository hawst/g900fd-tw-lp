.class final enum LOz;
.super Ljava/lang/Enum;
.source "PhotoBackupHelpCardFlow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LOz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LOz;

.field private static final synthetic a:[LOz;

.field public static final enum b:LOz;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, LOz;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2, v2}, LOz;-><init>(Ljava/lang/String;II)V

    sput-object v0, LOz;->a:LOz;

    .line 43
    new-instance v0, LOz;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v3, v3}, LOz;-><init>(Ljava/lang/String;II)V

    sput-object v0, LOz;->b:LOz;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [LOz;

    sget-object v1, LOz;->a:LOz;

    aput-object v1, v0, v2

    sget-object v1, LOz;->b:LOz;

    aput-object v1, v0, v3

    sput-object v0, LOz;->a:[LOz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, LOz;->a:I

    .line 49
    return-void
.end method

.method public static a(I)LOz;
    .locals 5

    .prologue
    .line 56
    invoke-static {}, LOz;->values()[LOz;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 57
    iget v4, v3, LOz;->a:I

    if-ne v4, p0, :cond_0

    .line 58
    return-object v3

    .line 56
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LOz;
    .locals 1

    .prologue
    .line 39
    const-class v0, LOz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LOz;

    return-object v0
.end method

.method public static values()[LOz;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LOz;->a:[LOz;

    invoke-virtual {v0}, [LOz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LOz;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, LOz;->a:I

    return v0
.end method
