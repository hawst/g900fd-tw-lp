.class public abstract LbmV;
.super Lbmv;
.source "ImmutableMultiset.java"

# interfaces
.implements Lbpi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmv",
        "<TE;>;",
        "Lbpi",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Lbmv;-><init>()V

    return-void
.end method

.method public static a()LbmV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LbmV",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lbmf;->a:Lbmf;

    return-object v0
.end method

.method private static a(Lbpi;)LbmV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbpi",
            "<+TE;>;)",
            "LbmV",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 193
    invoke-interface {p0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LbmV;->a(Ljava/util/Collection;)LbmV;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)LbmV;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LbmV",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 173
    instance-of v0, p0, LbmV;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 176
    check-cast v0, LbmV;

    .line 177
    invoke-virtual {v0}, LbmV;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    :goto_0
    return-object v0

    .line 182
    :cond_0
    instance-of v0, p0, Lbpi;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lbpk;->a(Ljava/lang/Iterable;)Lbpi;

    move-result-object v0

    .line 185
    :goto_1
    invoke-static {v0}, LbmV;->a(Lbpi;)LbmV;

    move-result-object v0

    goto :goto_0

    .line 182
    :cond_1
    invoke-static {p0}, LbnF;->a(Ljava/lang/Iterable;)LbnF;

    move-result-object v0

    goto :goto_1
.end method

.method static a(Ljava/util/Collection;)LbmV;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Lbpj",
            "<+TE;>;>;)",
            "LbmV",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 197
    .line 198
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v6

    .line 199
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 200
    invoke-interface {v0}, Lbpj;->a()I

    move-result v1

    .line 201
    if-lez v1, :cond_2

    .line 204
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v0, v8}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 205
    int-to-long v0, v1

    add-long/2addr v0, v2

    :goto_1
    move-wide v2, v0

    .line 207
    goto :goto_0

    .line 209
    :cond_0
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 210
    invoke-static {}, LbmV;->a()LbmV;

    move-result-object v0

    .line 212
    :goto_2
    return-object v0

    :cond_1
    new-instance v0, LbpH;

    invoke-virtual {v6}, LbmM;->a()LbmL;

    move-result-object v1

    invoke-static {v2, v3}, Lbsy;->b(J)I

    move-result v2

    invoke-direct {v0, v1, v2}, LbpH;-><init>(LbmL;I)V

    goto :goto_2

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 274
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()Lbqv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0}, LbmV;->b()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v0

    .line 236
    new-instance v1, LbmW;

    invoke-direct {v1, p0, v0}, LbmW;-><init>(LbmV;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, LbmV;->b()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 307
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 285
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 343
    iget-object v0, p0, LbmV;->a:LbmY;

    .line 344
    if-nez v0, :cond_0

    invoke-virtual {p0}, LbmV;->c()LbmY;

    move-result-object v0

    iput-object v0, p0, LbmV;->a:LbmY;

    :cond_0
    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 296
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method abstract c()LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lbpj",
            "<TE;>;>;"
        }
    .end annotation
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0, p1}, LbmV;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p0}, LbmV;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 312
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 327
    :goto_0
    return v0

    .line 315
    :cond_0
    instance-of v0, p1, Lbpi;

    if-eqz v0, :cond_4

    .line 316
    check-cast p1, Lbpi;

    .line 317
    invoke-virtual {p0}, LbmV;->size()I

    move-result v0

    invoke-interface {p1}, Lbpi;->size()I

    move-result v3

    if-eq v0, v3, :cond_1

    move v0, v2

    .line 318
    goto :goto_0

    .line 320
    :cond_1
    invoke-interface {p1}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 321
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, LbmV;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    if-eq v4, v0, :cond_2

    move v0, v2

    .line 322
    goto :goto_0

    :cond_3
    move v0, v1

    .line 325
    goto :goto_0

    :cond_4
    move v0, v2

    .line 327
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, LbmV;->b()LbmY;

    move-result-object v0

    invoke-static {v0}, LbpU;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, LbmV;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    invoke-virtual {p0}, LbmV;->b()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
