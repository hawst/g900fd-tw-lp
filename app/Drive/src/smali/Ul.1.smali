.class public LUl;
.super Ljava/lang/Object;
.source "OperationQueueMetadataChanger.java"

# interfaces
.implements LUi;


# static fields
.field private static final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaGg;

.field private final a:LaHE;

.field private final a:LaHr;

.field private final a:LaHz;

.field private final a:LahB;

.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, LUm;

    invoke-direct {v0}, LUm;-><init>()V

    sput-object v0, LUl;->a:LbiG;

    return-void
.end method

.method public constructor <init>(LaHz;LaHr;LaGg;LaHE;LahB;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, LUl;->a:LaHz;

    .line 61
    iput-object p2, p0, LUl;->a:LaHr;

    .line 62
    iput-object p3, p0, LUl;->a:LaGg;

    .line 63
    iput-object p4, p0, LUl;->a:LaHE;

    .line 64
    iput-object p5, p0, LUl;->a:LahB;

    .line 65
    iput-object p6, p0, LUl;->a:Ljava/util/concurrent/Executor;

    .line 66
    return-void
.end method

.method static synthetic a(LUl;)LaGg;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LUl;->a:LaGg;

    return-object v0
.end method

.method static synthetic a(LUl;)LahB;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LUl;->a:LahB;

    return-object v0
.end method

.method static synthetic a(LUl;LaGd;LaHy;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, LUl;->a(LaGd;LaHy;)V

    return-void
.end method

.method private a(LaGd;LaHy;)V
    .locals 4

    .prologue
    .line 175
    invoke-virtual {p1}, LaGd;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 177
    new-instance v0, LaHG;

    iget-object v1, p0, LUl;->a:LaGg;

    invoke-virtual {p1}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaHG;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    .line 178
    iget-object v1, p0, LUl;->a:LaHz;

    iget-object v2, p0, LUl;->a:LaHr;

    invoke-virtual {p1}, LaGd;->a()LaFM;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3, p2}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    .line 179
    return-void
.end method


# virtual methods
.method public a(LaGd;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 4

    .prologue
    .line 167
    invoke-virtual {p1}, LaGd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 169
    new-instance v0, LaHC;

    iget-object v1, p0, LUl;->a:LaGg;

    .line 170
    invoke-virtual {p1}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, LaHC;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 171
    iget-object v1, p0, LUl;->a:LaHz;

    iget-object v2, p0, LUl;->a:LaHr;

    invoke-virtual {p1}, LaGd;->a()LaFM;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3, p3}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    .line 172
    return-void

    .line 167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, LUl;->a:LaHz;

    invoke-virtual {v0, p1}, LaHz;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    .line 105
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 6

    .prologue
    .line 148
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, LUl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 151
    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v1, p0, LUl;->a:LaHz;

    iget-object v2, p0, LUl;->a:LaHr;

    new-instance v3, LaHH;

    iget-object v4, p0, LUl;->a:LaGg;

    .line 157
    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LaHH;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;)V

    invoke-virtual {v0}, LaGd;->a()LaFM;

    move-result-object v0

    .line 156
    invoke-virtual {v1, v2, v3, v0, p2}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "LaHy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, LUl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 131
    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p4, v0, v1}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 144
    :goto_0
    return-void

    .line 136
    :cond_0
    sget-object v1, LUl;->a:LbiG;

    .line 137
    invoke-static {p2, v1}, Lbnm;->a(Ljava/lang/Iterable;LbiG;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, LbmY;->a(Ljava/lang/Iterable;)LbmY;

    move-result-object v1

    .line 138
    sget-object v2, LUl;->a:LbiG;

    .line 139
    invoke-static {p3, v2}, Lbnm;->a(Ljava/lang/Iterable;LbiG;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v2}, LbmY;->a(Ljava/lang/Iterable;)LbmY;

    move-result-object v2

    .line 141
    new-instance v3, LaHB;

    iget-object v4, p0, LUl;->a:LaGg;

    .line 142
    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v5

    invoke-direct {v3, v4, v5, v1, v2}, LaHB;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LbmY;LbmY;)V

    .line 143
    iget-object v1, p0, LUl;->a:LaHz;

    iget-object v2, p0, LUl;->a:LaHr;

    invoke-virtual {v0}, LaGd;->a()LaFM;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0, p4}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 2

    .prologue
    .line 206
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, LUl;->a:Ljava/util/concurrent/Executor;

    new-instance v1, LUn;

    invoke-direct {v1, p0, p1, p3, p2}, LUn;-><init>(LUl;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 227
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 2

    .prologue
    .line 110
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    if-nez p2, :cond_0

    .line 114
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    .line 115
    :goto_0
    invoke-static {p3}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    .line 117
    invoke-virtual {p0, p1, v0, v1, p4}, LUl;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V

    .line 118
    return-void

    .line 114
    :cond_0
    invoke-static {p2}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;LaHy;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, LUl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 74
    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x2

    invoke-interface {p3, v0, v2}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 84
    :goto_0
    return-void

    .line 80
    :cond_0
    const/4 v1, 0x0

    invoke-interface {p3, v1, v2}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 81
    new-instance v1, LaHn;

    iget-object v2, p0, LUl;->a:LaGg;

    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v3

    invoke-direct {v1, v2, v3, p2}, LaHn;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 82
    iget-object v2, p0, LUl;->a:LaHz;

    iget-object v3, p0, LUl;->a:LaHr;

    .line 83
    invoke-virtual {v0}, LaGd;->a()LaFM;

    move-result-object v0

    sget-object v4, LUo;->a:LaHy;

    .line 82
    invoke-virtual {v2, v3, v1, v0, v4}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V
    .locals 4

    .prologue
    .line 89
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, LUl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v1, LaHF;

    iget-object v2, p0, LUl;->a:LaGg;

    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v3

    invoke-direct {v1, v2, v3, p2}, LaHF;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)V

    .line 99
    iget-object v2, p0, LUl;->a:LaHz;

    iget-object v3, p0, LUl;->a:LaHr;

    invoke-virtual {v0}, LaGd;->a()LaFM;

    move-result-object v0

    invoke-virtual {v2, v3, v1, v0, p3}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 183
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, LUl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 187
    if-nez v0, :cond_0

    .line 188
    invoke-interface {p2, v5, v4}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 201
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 193
    if-eqz v1, :cond_1

    iget-object v2, p0, LUl;->a:LaHE;

    const/4 v3, 0x1

    .line 194
    invoke-interface {v2, v1, p2, v3}, LaHE;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHy;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    const/4 v0, 0x2

    invoke-interface {p2, v0, v4}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 199
    :cond_1
    invoke-virtual {v0}, LaGd;->a()LaGe;

    move-result-object v0

    invoke-virtual {v0}, LaGe;->f()V

    .line 200
    invoke-interface {p2, v5, v4}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V
    .locals 2

    .prologue
    .line 234
    new-instance v0, LaGY;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-direct {v0, p2, v1}, LaGY;-><init>(ZLjava/util/Date;)V

    .line 235
    iget-object v1, p0, LUl;->a:LahB;

    invoke-interface {v1, p1, v0}, LahB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGY;)V

    .line 236
    return-void
.end method
