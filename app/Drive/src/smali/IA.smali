.class public LIA;
.super Ljava/lang/Object;
.source "GroupValueIndexer.java"

# interfaces
.implements LCx;


# instance fields
.field private final a:LIz;


# direct methods
.method public constructor <init>(LIz;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, LIA;->a:LIz;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, LIA;->a:LIz;

    invoke-interface {v0}, LIz;->getCount()I

    move-result v0

    return v0
.end method

.method public a(I)LIB;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 18
    invoke-virtual {p0}, LIA;->a()I

    move-result v4

    .line 19
    invoke-static {p1, v4}, LbiT;->a(II)I

    .line 22
    if-lez p1, :cond_3

    .line 23
    iget-object v1, p0, LIA;->a:LIz;

    add-int/lit8 v5, p1, -0x1

    invoke-interface {v1, v5}, LIz;->a(I)LIy;

    move-result-object v1

    .line 27
    :goto_0
    add-int/lit8 v4, v4, -0x1

    if-ge p1, v4, :cond_0

    .line 28
    iget-object v0, p0, LIA;->a:LIz;

    add-int/lit8 v4, p1, 0x1

    invoke-interface {v0, v4}, LIz;->a(I)LIy;

    move-result-object v0

    .line 33
    :cond_0
    iget-object v4, p0, LIA;->a:LIz;

    invoke-interface {v4, p1}, LIz;->a(I)LIy;

    move-result-object v4

    .line 34
    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 35
    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 36
    :goto_2
    invoke-interface {v4, v1, v2}, LIy;->a(ZZ)LIB;

    move-result-object v0

    return-object v0

    :cond_1
    move v1, v3

    .line 34
    goto :goto_1

    :cond_2
    move v2, v3

    .line 35
    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method
