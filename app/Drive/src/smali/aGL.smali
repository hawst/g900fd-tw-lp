.class public LaGL;
.super Ljava/lang/Object;
.source "ManifestSpec.java"


# instance fields
.field private final a:LaFO;

.field private final a:LaGs;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;LaFO;Ljava/lang/String;LaGs;Z)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGL;->a:Ljava/lang/String;

    .line 22
    iput-object p2, p0, LaGL;->a:LaFO;

    .line 23
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGL;->b:Ljava/lang/String;

    .line 24
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGs;

    iput-object v0, p0, LaGL;->a:LaGs;

    .line 25
    iput-boolean p5, p0, LaGL;->a:Z

    .line 26
    return-void
.end method

.method public static a(Ljava/lang/String;LaFO;Ljava/lang/String;LaGs;Z)LaGL;
    .locals 6

    .prologue
    .line 34
    new-instance v0, LaGL;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LaGL;-><init>(Ljava/lang/String;LaFO;Ljava/lang/String;LaGs;Z)V

    return-object v0
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LaGL;->a:LaFO;

    return-object v0
.end method

.method public a()LaGs;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LaGL;->a:LaGs;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, LaGL;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, LaGL;->a:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LaGL;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 64
    check-cast p1, LaGL;

    .line 65
    if-eqz p1, :cond_0

    iget-object v0, p0, LaGL;->a:Ljava/lang/String;

    iget-object v1, p1, LaGL;->a:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaGL;->a:LaFO;

    iget-object v1, p1, LaGL;->a:LaFO;

    .line 67
    invoke-static {v0, v1}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaGL;->b:Ljava/lang/String;

    iget-object v1, p1, LaGL;->b:Ljava/lang/String;

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaGL;->a:LaGs;

    iget-object v1, p1, LaGL;->a:LaGs;

    .line 69
    invoke-virtual {v0, v1}, LaGs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaGL;->a:Z

    iget-boolean v1, p1, LaGL;->a:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LaGL;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaGL;->a:LaFO;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LaGL;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LaGL;->a:LaGs;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LaGL;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    const-string v0, "ManifestSpec[%s, %s, %s, %s, isFastTrack: %s]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaGL;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaGL;->a:LaFO;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaGL;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LaGL;->a:LaGs;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, LaGL;->a:Z

    .line 77
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 75
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
