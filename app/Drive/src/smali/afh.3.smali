.class Lafh;
.super LaeT;
.source "PreparedSyncMoreImpl.java"


# instance fields
.field private final a:J

.field private final a:LSF;

.field private final a:LaFM;

.field private final a:LaGF;

.field private final a:LagS;

.field private final a:Lagy;

.field private final a:Landroid/content/SyncResult;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LaGW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaFM;Ljava/util/List;Lagy;LagS;Landroid/content/SyncResult;LSF;LaGF;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "Ljava/util/List",
            "<",
            "LaGW;",
            ">;",
            "Lagy;",
            "LagS;",
            "Landroid/content/SyncResult;",
            "LSF;",
            "LaGF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, LaeT;-><init>()V

    .line 58
    iput-object p1, p0, Lafh;->a:LaFM;

    .line 59
    const-wide/high16 v2, -0x8000000000000000L

    .line 60
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v4, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGW;

    .line 61
    invoke-virtual {v2}, LaGW;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    invoke-virtual {v2}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, LbiT;->a(Z)V

    .line 63
    invoke-virtual {v2}, LaGW;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 64
    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 65
    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-gtz v3, :cond_0

    .line 66
    const-string v3, "PreparedSyncMore"

    const-string v7, "Unexpectedly low clipTime in %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-static {v3, v7, v8}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-wide v2, v4

    move-wide v4, v2

    .line 69
    goto :goto_0

    .line 62
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 70
    :cond_2
    iput-wide v4, p0, Lafh;->a:J

    .line 71
    iput-object p2, p0, Lafh;->a:Ljava/util/List;

    .line 72
    iput-object p3, p0, Lafh;->a:Lagy;

    .line 73
    move-object/from16 v0, p4

    iput-object v0, p0, Lafh;->a:LagS;

    .line 74
    move-object/from16 v0, p5

    iput-object v0, p0, Lafh;->a:Landroid/content/SyncResult;

    .line 75
    move-object/from16 v0, p6

    iput-object v0, p0, Lafh;->a:LSF;

    .line 76
    move-object/from16 v0, p7

    iput-object v0, p0, Lafh;->a:LaGF;

    .line 77
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 141
    const-string v0, "PreparedSyncMore"

    const-string v1, "Error syncing more"

    invoke-static {v0, p1, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 142
    return-void
.end method


# virtual methods
.method public a(I)Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 101
    iget-object v0, p0, Lafh;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v8

    .line 102
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->a(LaFO;)V

    .line 104
    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 105
    iget-object v0, p0, Lafh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGW;

    .line 106
    new-instance v0, Lafv;

    iget-object v1, p0, Lafh;->a:LaFM;

    iget-object v2, p0, Lafh;->a:Lagy;

    iget-object v5, p0, Lafh;->a:LaGF;

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lafv;-><init>(LaFM;Lagy;LaGW;ILaGF;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch LQo; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    :try_start_1
    invoke-direct {p0, v0}, Lafh;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->b(LaFO;)V

    .line 137
    :goto_1
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_2
    return-object v0

    .line 109
    :cond_0
    :try_start_2
    invoke-static {v9}, Lais;->a(Ljava/util/Collection;)Laiq;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lafh;->a:LagS;

    invoke-interface {v1}, LagS;->a()Lagx;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lafh;->a:Landroid/content/SyncResult;

    invoke-interface {v0, v1, v2}, Laiq;->a(Lagx;Landroid/content/SyncResult;)V

    .line 112
    iget-object v2, p0, Lafh;->a:Landroid/content/SyncResult;

    invoke-interface {v1, v2}, Lagx;->a(Landroid/content/SyncResult;)V

    .line 113
    iget-object v1, p0, Lafh;->a:Landroid/content/SyncResult;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Laiq;->a(Landroid/content/SyncResult;Z)V

    .line 115
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v7

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafv;

    .line 116
    invoke-virtual {v0}, Lafv;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v6

    :goto_4
    or-int/2addr v0, v1

    move v1, v0

    .line 117
    goto :goto_3

    :cond_1
    move v0, v7

    .line 116
    goto :goto_4

    .line 119
    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_2
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_0
    .catch LbwW; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch LQo; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 135
    iget-object v1, p0, Lafh;->a:LSF;

    invoke-interface {v1, v8}, LSF;->b(LaFO;)V

    goto :goto_2

    .line 122
    :catch_1
    move-exception v0

    .line 123
    :try_start_3
    invoke-direct {p0, v0}, Lafh;->a(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 135
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->b(LaFO;)V

    goto :goto_1

    .line 124
    :catch_2
    move-exception v0

    .line 125
    :try_start_4
    invoke-direct {p0, v0}, Lafh;->a(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 135
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->b(LaFO;)V

    goto :goto_1

    .line 126
    :catch_3
    move-exception v0

    .line 127
    :try_start_5
    invoke-direct {p0, v0}, Lafh;->a(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 135
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->b(LaFO;)V

    goto :goto_1

    .line 128
    :catch_4
    move-exception v0

    .line 131
    const/4 v0, 0x1

    :try_start_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 135
    iget-object v1, p0, Lafh;->a:LSF;

    invoke-interface {v1, v8}, LSF;->b(LaFO;)V

    goto :goto_2

    .line 132
    :catch_5
    move-exception v0

    .line 133
    :try_start_7
    invoke-direct {p0, v0}, Lafh;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 135
    iget-object v0, p0, Lafh;->a:LSF;

    invoke-interface {v0, v8}, LSF;->b(LaFO;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lafh;->a:LSF;

    invoke-interface {v1, v8}, LSF;->b(LaFO;)V

    throw v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lafh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGW;

    .line 82
    invoke-virtual {v0}, LaGW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lafh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lafh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGW;

    .line 92
    invoke-virtual {v0}, LaGW;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 154
    const-string v1, "PreparedSyncMore[%s%d feeds, clipTime=%s, %s]"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 155
    invoke-virtual {p0}, Lafh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "running, "

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lafh;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-wide v4, p0, Lafh;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lafh;->a:LaFM;

    .line 156
    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v3

    aput-object v3, v2, v0

    .line 154
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 155
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
