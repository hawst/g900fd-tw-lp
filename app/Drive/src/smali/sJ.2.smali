.class public LsJ;
.super Ljava/lang/Object;
.source "DocumentCreatorIntentFactoryImpl.java"

# interfaces
.implements LsI;


# instance fields
.field a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaFO;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LsJ;->a:Laja;

    .line 38
    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 37
    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;LaGv;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 26
    if-nez p2, :cond_0

    .line 27
    iget-object v0, p0, LsJ;->a:Laja;

    .line 28
    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 27
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 30
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LsJ;->a:Laja;

    .line 31
    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 30
    invoke-static {v0, p2, p1, p3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;LaGv;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
