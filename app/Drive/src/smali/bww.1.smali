.class public final Lbww;
.super Ljava/lang/Object;
.source "InjectionPoint.java"


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/reflect/Member;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lbww;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lbww;->a:Ljava/util/logging/Logger;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/reflect/Member;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lbww;->a:Ljava/lang/reflect/Member;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lbww;->a:LbmF;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 186
    instance-of v0, p1, Lbww;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbww;->a:Ljava/lang/reflect/Member;

    move-object v0, p1

    check-cast v0, Lbww;

    iget-object v0, v0, Lbww;->a:Ljava/lang/reflect/Member;

    .line 187
    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbww;->a:LbuP;

    check-cast p1, Lbww;

    iget-object v1, p1, Lbww;->a:LbuP;

    .line 188
    invoke-virtual {v0, v1}, LbuP;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lbww;->a:Ljava/lang/reflect/Member;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lbww;->a:LbuP;

    invoke-virtual {v1}, LbuP;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lbww;->a:Ljava/lang/reflect/Member;

    invoke-static {v0}, LbvF;->a(Ljava/lang/reflect/Member;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
