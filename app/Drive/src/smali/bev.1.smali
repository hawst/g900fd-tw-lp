.class public Lbev;
.super LbdR;
.source "MultipartContent.java"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbew;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Lbeh;

    const-string v1, "multipart/related"

    invoke-direct {v0, v1}, Lbeh;-><init>(Ljava/lang/String;)V

    const-string v1, "boundary"

    const-string v2, "__END_OF_PART__"

    invoke-virtual {v0, v1, v2}, Lbeh;->a(Ljava/lang/String;Ljava/lang/String;)Lbeh;

    move-result-object v0

    invoke-direct {p0, v0}, LbdR;-><init>(Lbeh;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbev;->a:Ljava/util/ArrayList;

    .line 59
    return-void
.end method


# virtual methods
.method public a(Lbew;)Lbev;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lbev;->a:Ljava/util/ArrayList;

    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lbev;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "LbdZ;",
            ">;)",
            "Lbev;"
        }
    .end annotation

    .prologue
    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lbev;->a:Ljava/util/ArrayList;

    .line 174
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbdZ;

    .line 175
    new-instance v2, Lbew;

    invoke-direct {v2, v0}, Lbew;-><init>(LbdZ;)V

    invoke-virtual {p0, v2}, Lbev;->a(Lbew;)Lbev;

    goto :goto_0

    .line 177
    :cond_0
    return-object p0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 62
    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-virtual {p0}, Lbev;->a()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-direct {v5, p1, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    .line 63
    invoke-virtual {p0}, Lbev;->b()Ljava/lang/String;

    move-result-object v6

    .line 64
    iget-object v0, p0, Lbev;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbew;

    .line 65
    new-instance v2, Lbed;

    invoke-direct {v2}, Lbed;-><init>()V

    invoke-virtual {v2, v1}, Lbed;->a(Ljava/lang/String;)Lbed;

    move-result-object v8

    .line 66
    iget-object v2, v0, Lbew;->a:Lbed;

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, v0, Lbew;->a:Lbed;

    invoke-virtual {v8, v2}, Lbed;->a(Lbed;)V

    .line 69
    :cond_1
    invoke-virtual {v8, v1}, Lbed;->c(Ljava/lang/String;)Lbed;

    move-result-object v2

    .line 70
    invoke-virtual {v2, v1}, Lbed;->l(Ljava/lang/String;)Lbed;

    move-result-object v2

    .line 71
    invoke-virtual {v2, v1}, Lbed;->e(Ljava/lang/String;)Lbed;

    move-result-object v2

    .line 72
    invoke-virtual {v2, v1}, Lbed;->a(Ljava/lang/Long;)Lbed;

    move-result-object v2

    const-string v3, "Content-Transfer-Encoding"

    .line 73
    invoke-virtual {v2, v3, v1}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    .line 75
    iget-object v4, v0, Lbew;->a:LbdZ;

    .line 77
    if-eqz v4, :cond_5

    .line 78
    const-string v2, "Content-Transfer-Encoding"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "binary"

    aput-object v10, v3, v9

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    .line 79
    invoke-interface {v4}, LbdZ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lbed;->e(Ljava/lang/String;)Lbed;

    .line 80
    iget-object v2, v0, Lbew;->a:Lbea;

    .line 82
    if-nez v2, :cond_3

    .line 83
    invoke-interface {v4}, LbdZ;->a()J

    move-result-wide v2

    move-object v0, v4

    .line 90
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v4, v2, v10

    if-eqz v4, :cond_2

    .line 91
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v2}, Lbed;->a(Ljava/lang/Long;)Lbed;

    .line 95
    :cond_2
    :goto_2
    const-string v2, "--"

    invoke-virtual {v5, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v5, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 97
    const-string v2, "\r\n"

    invoke-virtual {v5, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 99
    invoke-static {v8, v1, v1, v5}, Lbed;->a(Lbed;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Ljava/io/Writer;)V

    .line 101
    if-eqz v0, :cond_0

    .line 102
    const-string v2, "\r\n"

    invoke-virtual {v5, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v5}, Ljava/io/Writer;->flush()V

    .line 104
    invoke-interface {v0, p1}, LbfR;->a(Ljava/io/OutputStream;)V

    .line 105
    const-string v0, "\r\n"

    invoke-virtual {v5, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 86
    :cond_3
    invoke-interface {v2}, Lbea;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lbed;->c(Ljava/lang/String;)Lbed;

    .line 87
    new-instance v0, Lbeb;

    invoke-direct {v0, v4, v2}, Lbeb;-><init>(LbfR;Lbea;)V

    .line 88
    invoke-static {v4}, LbdR;->a(LbdZ;)J

    move-result-wide v2

    goto :goto_1

    .line 109
    :cond_4
    const-string v0, "--"

    invoke-virtual {v5, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 110
    invoke-virtual {v5, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 111
    const-string v0, "--"

    invoke-virtual {v5, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 112
    const-string v0, "\r\n"

    invoke-virtual {v5, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v5}, Ljava/io/Writer;->flush()V

    .line 114
    return-void

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lbev;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbew;

    .line 119
    iget-object v0, v0, Lbew;->a:LbdZ;

    invoke-interface {v0}, LbdZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const/4 v0, 0x0

    .line 123
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0}, Lbev;->a()Lbeh;

    move-result-object v0

    const-string v1, "boundary"

    invoke-virtual {v0, v1}, Lbeh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
