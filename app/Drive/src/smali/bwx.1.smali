.class public final Lbwx;
.super Ljava/lang/Object;
.source "MembersInjectorLookup.java"

# interfaces
.implements Lbwu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbwu;"
    }
.end annotation


# instance fields
.field private a:LbuB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuB",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;


# virtual methods
.method public a()LbuP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuP",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lbwx;->a:LbuP;

    return-object v0
.end method

.method public a(LbuB;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lbwx;->a:LbuB;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "delegate already initialized"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 69
    const-string v0, "delegate"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuB;

    iput-object v0, p0, Lbwx;->a:LbuB;

    .line 70
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lbwx;->a:Ljava/lang/Object;

    return-object v0
.end method
