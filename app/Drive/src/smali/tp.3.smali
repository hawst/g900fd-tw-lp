.class public Ltp;
.super Ljava/lang/Object;
.source "DocumentPreviewMemoryLimits.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:I

.field private final a:J


# direct methods
.method public constructor <init>(LQr;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "maxRamForPreviewBitmapFraction"

    const-wide v2, 0x3fd3333340000000L    # 0.30000001192092896

    invoke-interface {p1, v0, v2, v3}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v0

    .line 30
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    .line 31
    long-to-double v4, v2

    mul-double/2addr v0, v4

    double-to-long v0, v0

    .line 38
    const-wide/16 v4, 0x2

    div-long/2addr v0, v4

    const-wide/16 v4, 0x4

    div-long/2addr v0, v4

    .line 40
    const-string v4, "DocumentPreviewMemoryLimits"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Max ram size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " offscreenPageLimit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " max image pixels: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iput v7, p0, Ltp;->a:I

    .line 45
    iput-wide v0, p0, Ltp;->a:J

    .line 46
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Ltp;->a:I

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Ltp;->a:J

    return-wide v0
.end method
