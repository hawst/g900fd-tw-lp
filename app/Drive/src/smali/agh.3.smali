.class Lagh;
.super Ljava/lang/Object;
.source "ContentSyncWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LZS;

.field private final a:LaKR;

.field private final a:LaKU;

.field private final a:LahK;

.field private final b:LZS;


# direct methods
.method public constructor <init>(LaKU;LahK;LZS;LZS;LaKR;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahK;

    iput-object v0, p0, Lagh;->a:LahK;

    .line 37
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKU;

    iput-object v0, p0, Lagh;->a:LaKU;

    .line 38
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p0, Lagh;->a:LZS;

    .line 39
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p0, Lagh;->b:LZS;

    .line 40
    iput-object p5, p0, Lagh;->a:LaKR;

    .line 41
    return-void
.end method

.method private a(LahF;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 72
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lagh;->a:LZS;

    invoke-interface {v0}, LZS;->b()V

    .line 79
    iget-object v0, p0, Lagh;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()LaKS;

    move-result-object v4

    .line 81
    :try_start_0
    new-instance v0, Lbjs;

    invoke-direct {v0}, Lbjs;-><init>()V

    .line 82
    invoke-virtual {v0}, Lbjs;->a()Lbjs;

    .line 84
    invoke-interface {p1}, LahF;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-interface {p1}, LahF;->i()V

    .line 86
    const-string v1, "ContentSyncWorker"

    const-string v5, "Syncing %s completed. Time taken: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 87
    invoke-virtual {v0, v8}, Lbjs;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    .line 86
    invoke-static {v1, v5, v6}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 90
    :cond_0
    :try_start_1
    iget-object v0, p0, Lagh;->b:LZS;

    invoke-interface {v0}, LZS;->c()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 117
    :cond_1
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    move v1, v2

    .line 92
    :goto_1
    :try_start_2
    invoke-interface {p1}, LahF;->a()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result v5

    if-eqz v5, :cond_3

    .line 94
    :try_start_3
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    move v2, v1

    :goto_2
    if-eqz v3, :cond_4

    .line 103
    invoke-interface {p1}, LahF;->c()V

    .line 114
    :cond_2
    :goto_3
    throw v0

    .line 96
    :cond_3
    :try_start_4
    invoke-interface {p1, v0}, LahF;->a(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 105
    if-nez v1, :cond_1

    .line 106
    iget-object v0, p0, Lagh;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()LaKS;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, LaKS;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    invoke-virtual {v4, v0}, LaKS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-interface {p1}, LahF;->d()V

    .line 111
    iget-object v0, p0, Lagh;->b:LZS;

    invoke-interface {v0}, LZS;->b()V

    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 100
    :goto_4
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 102
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 105
    :cond_4
    if-nez v2, :cond_2

    .line 106
    iget-object v1, p0, Lagh;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()LaKS;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, LaKS;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    invoke-virtual {v4, v1}, LaKS;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    invoke-interface {p1}, LahF;->d()V

    .line 111
    iget-object v1, p0, Lagh;->b:LZS;

    invoke-interface {v1}, LZS;->b()V

    goto :goto_3

    .line 102
    :catchall_2
    move-exception v0

    move v3, v2

    goto :goto_2

    :catchall_3
    move-exception v0

    move v10, v3

    move v3, v2

    move v2, v10

    goto :goto_2

    :catchall_4
    move-exception v0

    move v3, v2

    move v2, v1

    goto :goto_2

    .line 98
    :catch_2
    move-exception v0

    move v2, v3

    goto :goto_4

    .line 91
    :catch_3
    move-exception v0

    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 46
    :cond_0
    :goto_0
    iget-object v0, p0, Lagh;->a:LaKU;

    invoke-virtual {v0}, LaKU;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    :try_start_0
    iget-object v0, p0, Lagh;->a:LahK;

    invoke-interface {v0, v1}, LahK;->a(Ljava/lang/Thread;)LahF;

    move-result-object v2

    .line 50
    if-nez v2, :cond_2

    .line 60
    if-eqz v2, :cond_1

    .line 61
    invoke-interface {v2}, LahF;->j()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :cond_1
    return-void

    .line 53
    :cond_2
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    .line 61
    :try_start_2
    invoke-interface {v2}, LahF;->j()V

    :cond_3
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 64
    :catch_0
    move-exception v0

    .line 66
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_0

    .line 57
    :cond_4
    :try_start_3
    invoke-direct {p0, v2}, Lagh;->a(LahF;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    if-eqz v2, :cond_0

    .line 61
    :try_start_4
    invoke-interface {v2}, LahF;->j()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method
