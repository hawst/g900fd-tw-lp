.class public enum LCe;
.super Ljava/lang/Enum;
.source "DriveEntriesFilter.java"

# interfaces
.implements LCl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LCe;",
        ">;",
        "LCl;"
    }
.end annotation


# static fields
.field public static final enum a:LCe;

.field private static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LIK;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LCe;

.field public static final enum b:LCe;

.field private static final b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LIK;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum c:LCe;

.field public static final enum d:LCe;

.field public static final enum e:LCe;

.field public static final enum f:LCe;

.field public static final enum g:LCe;

.field public static final enum h:LCe;

.field public static final enum i:LCe;

.field public static final enum j:LCe;

.field public static final enum k:LCe;

.field public static final enum l:LCe;

.field public static final enum m:LCe;

.field public static final enum n:LCe;

.field public static final enum o:LCe;

.field public static final enum p:LCe;

.field public static final enum q:LCe;

.field public static final enum r:LCe;


# instance fields
.field private final a:I

.field private final a:LCm;

.field private final a:LCn;

.field private final a:LaeZ;

.field private final a:Ljava/lang/String;

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 43
    new-instance v0, LCe;

    const-string v1, "NONE"

    const/4 v2, 0x0

    sget-object v3, LCn;->l:LCn;

    const-string v4, "1=1"

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->slider_title_all_items:I

    const-string v8, "allItems"

    sget-object v9, LaeZ;->a:LaeZ;

    sget-object v10, LCl;->a:LbmY;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->a:LCe;

    .line 48
    new-instance v0, LCe;

    const-string v1, "STARRED"

    const/4 v2, 0x1

    sget-object v3, LCn;->g:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->v:LaES;

    .line 49
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<>0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_starred:I

    const-string v8, "starred"

    const-string v9, "starred"

    .line 51
    invoke-static {v9}, LaeZ;->b(Ljava/lang/String;)LaeZ;

    move-result-object v9

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_starred"

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->b:LCe;

    .line 54
    new-instance v0, LCf;

    const-string v1, "PINNED"

    const/4 v2, 0x2

    sget-object v3, LCn;->i:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->y:LaES;

    .line 55
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<>0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_pinned:I

    const-string v8, "pinned"

    sget-object v9, LaeZ;->d:LaeZ;

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_offline"

    invoke-direct/range {v0 .. v11}, LCf;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->c:LCe;

    .line 65
    new-instance v0, LCg;

    const-string v1, "UPLOADS"

    const/4 v2, 0x3

    sget-object v3, LCn;->h:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaFj;->f:LaFj;

    .line 66
    invoke-virtual {v5}, LaFj;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " <>0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_upload:I

    const-string v8, "browseUpload"

    sget-object v9, LaeZ;->d:LaeZ;

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_uploads"

    invoke-direct/range {v0 .. v11}, LCg;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->d:LCe;

    .line 81
    new-instance v0, LCe;

    const-string v1, "COLLECTIONS"

    const/4 v2, 0x4

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_folder:I

    const/4 v7, 0x0

    sget-object v8, LaeZ;->c:LaeZ;

    sget-object v9, LaGv;->a:LaGv;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->e:LCe;

    .line 84
    new-instance v0, LCe;

    const-string v1, "DOCUMENTS"

    const/4 v2, 0x5

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_kix:I

    const/4 v7, 0x0

    sget-object v8, LaeZ;->b:LaeZ;

    sget-object v9, LaGv;->b:LaGv;

    sget-object v10, LaGn;->d:LaGn;

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->f:LCe;

    .line 87
    new-instance v0, LCe;

    const-string v1, "SPREADSHEETS"

    const/4 v2, 0x6

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_trix:I

    const/4 v7, 0x0

    sget-object v8, LaeZ;->b:LaeZ;

    sget-object v9, LaGv;->i:LaGv;

    sget-object v10, LaGn;->e:LaGn;

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->g:LCe;

    .line 90
    new-instance v0, LCe;

    const-string v1, "PRESENTATIONS"

    const/4 v2, 0x7

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_punch:I

    const/4 v7, 0x0

    sget-object v8, LaeZ;->b:LaeZ;

    sget-object v9, LaGv;->g:LaGv;

    sget-object v10, LaGn;->f:LaGn;

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->h:LCe;

    .line 93
    new-instance v0, LCe;

    const-string v1, "DRAWINGS"

    const/16 v2, 0x8

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_drawing:I

    const/4 v7, 0x0

    sget-object v8, LaGv;->c:LaGv;

    .line 94
    invoke-static {v8}, LaeZ;->a(LaGv;)LaeZ;

    move-result-object v8

    sget-object v9, LaGv;->c:LaGv;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->i:LCe;

    .line 96
    new-instance v0, LCe;

    const-string v1, "PICTURES"

    const/16 v2, 0x9

    sget-object v3, LCn;->d:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->r:LaES;

    .line 97
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE \'image%\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_picture:I

    const/4 v8, 0x0

    sget-object v9, LaeZ;->b:LaeZ;

    const-string v10, "image/"

    .line 100
    invoke-static {v10}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->j:LCe;

    .line 101
    new-instance v0, LCe;

    const-string v1, "MOVIES"

    const/16 v2, 0xa

    sget-object v3, LCn;->d:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->r:LaES;

    .line 102
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE \'video%\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_movie:I

    const/4 v8, 0x0

    sget-object v9, LaeZ;->b:LaeZ;

    const-string v10, "video/"

    .line 105
    invoke-static {v10}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->k:LCe;

    .line 106
    new-instance v0, LCe;

    const-string v1, "PDF"

    const/16 v2, 0xb

    sget-object v3, LCn;->d:LCn;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v6, Lxi;->menu_show_pdf:I

    const/4 v7, 0x0

    sget-object v8, LaGv;->f:LaGv;

    .line 107
    invoke-static {v8}, LaeZ;->a(LaGv;)LaeZ;

    move-result-object v8

    sget-object v9, LaGv;->f:LaGv;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LCe;-><init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V

    sput-object v0, LCe;->l:LCe;

    .line 109
    new-instance v0, LCe;

    const-string v1, "TRASH"

    const/16 v2, 0xc

    sget-object v3, LCn;->j:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->x:LaES;

    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaGy;->d:LaGy;

    .line 110
    invoke-virtual {v5}, LaGy;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaES;->b:LaES;

    .line 111
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, LCm;->a:LCm;

    sget v7, Lxi;->menu_show_trash:I

    const-string v8, "trash"

    const-string v9, "trashed"

    .line 113
    invoke-static {v9}, LaeZ;->b(Ljava/lang/String;)LaeZ;

    move-result-object v9

    sget-object v10, LCl;->a:LbmY;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->m:LCe;

    .line 116
    new-instance v0, LCe;

    const-string v1, "OWNED_BY_ME"

    const/16 v2, 0xd

    sget-object v3, LCn;->c:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->b:LaES;

    .line 117
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 118
    invoke-static {}, LaER;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, LCm;->a:LCm;

    sget v7, Lxi;->menu_show_owned_by_me:I

    const-string v8, "ownedByMe"

    const-string v9, "mine"

    .line 120
    invoke-static {v9}, LaeZ;->b(Ljava/lang/String;)LaeZ;

    move-result-object v9

    sget-object v10, LCl;->a:LbmY;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->n:LCe;

    .line 123
    new-instance v0, LCh;

    const-string v1, "SHARED_WITH_ME"

    const/16 v2, 0xe

    sget-object v3, LCn;->f:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->i:LaES;

    .line 124
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IS NOT NULL AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaES;->c:LaES;

    .line 125
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<>?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, LCm;->a:LCm;

    sget v7, Lxi;->menu_incoming:I

    const-string v8, "sharedWithMe"

    sget-object v9, LaeZ;->a:LaeZ;

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_shared_with_me"

    invoke-direct/range {v0 .. v11}, LCh;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->o:LCe;

    .line 140
    new-instance v0, LCi;

    const-string v1, "OPENED_BY_ME"

    const/16 v2, 0xf

    sget-object v3, LCn;->e:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->h:LaES;

    .line 141
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IS NOT NULL"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 142
    invoke-static {}, LaER;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_show_recent:I

    const-string v8, "recentlyOpened"

    sget-object v9, LaeZ;->a:LaeZ;

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_recent"

    invoke-direct/range {v0 .. v11}, LCi;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->p:LCe;

    .line 157
    new-instance v0, LCj;

    const-string v1, "MY_DRIVE"

    const/16 v2, 0x10

    sget-object v3, LCn;->a:LCn;

    .line 158
    invoke-static {}, LaEu;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_my_drive:I

    const-string v8, "myDrive"

    .line 161
    invoke-static {}, LaeZ;->a()LaeZ;

    move-result-object v9

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_my_drive"

    invoke-direct/range {v0 .. v11}, LCj;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->q:LCe;

    .line 175
    new-instance v0, LCk;

    const-string v1, "INCOMING_PHOTOS"

    const/16 v2, 0x11

    sget-object v3, LCn;->k:LCn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaES;->H:LaES;

    .line 176
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaGw;->c:LaGw;

    .line 177
    invoke-virtual {v5}, LaGw;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaES;->t:LaES;

    .line 178
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<>\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaGv;->a:LaGv;

    invoke-virtual {v5}, LaGv;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget v7, Lxi;->menu_incoming:I

    const-string v8, "incomingPhotos"

    sget-object v9, Lafu;->a:Lafu;

    .line 182
    invoke-static {v9}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v9

    .line 181
    invoke-static {v9}, LaeZ;->a(LbmY;)LaeZ;

    move-result-object v9

    sget-object v10, LCl;->a:LbmY;

    const-string v11, "mobile_shared_with_me"

    invoke-direct/range {v0 .. v11}, LCk;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    sput-object v0, LCe;->r:LCe;

    .line 39
    const/16 v0, 0x12

    new-array v0, v0, [LCe;

    const/4 v1, 0x0

    sget-object v2, LCe;->a:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LCe;->b:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LCe;->c:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LCe;->d:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LCe;->e:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LCe;->f:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LCe;->g:LCe;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LCe;->h:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LCe;->i:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LCe;->j:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LCe;->k:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LCe;->l:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LCe;->m:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LCe;->n:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LCe;->o:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LCe;->p:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LCe;->q:LCe;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LCe;->r:LCe;

    aput-object v2, v0, v1

    sput-object v0, LCe;->a:[LCe;

    .line 197
    sget-object v0, LIK;->d:LIK;

    sget-object v1, LIK;->f:LIK;

    sget-object v2, LIK;->c:LIK;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LCe;->a:Ljava/util/EnumSet;

    .line 200
    sget-object v0, LIK;->b:LIK;

    sget-object v1, LIK;->c:LIK;

    sget-object v2, LIK;->d:LIK;

    sget-object v3, LIK;->f:LIK;

    sget-object v4, LIK;->g:LIK;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LCe;->b:Ljava/util/EnumSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCn;",
            "Ljava/lang/String;",
            "Z",
            "LCm;",
            "I",
            "Ljava/lang/String;",
            "LaeZ;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 241
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCn;

    iput-object v0, p0, LCe;->a:LCn;

    .line 242
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 243
    if-nez p5, :cond_0

    .line 244
    :goto_0
    iput-object v0, p0, LCe;->a:Ljava/lang/String;

    .line 245
    iput-object p6, p0, LCe;->a:LCm;

    .line 246
    iput p7, p0, LCe;->a:I

    .line 247
    iput-object p8, p0, LCe;->c:Ljava/lang/String;

    .line 248
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeZ;

    iput-object v0, p0, LCe;->a:LaeZ;

    .line 249
    iput-object p10, p0, LCe;->b:LbmY;

    .line 250
    iput-object p11, p0, LCe;->b:Ljava/lang/String;

    .line 251
    return-void

    .line 243
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->x:LaES;

    .line 244
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=0)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;LCf;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct/range {p0 .. p11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILCn;ZLCm;ILjava/lang/String;LaeZ;LaGv;LaGn;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCn;",
            "Z",
            "LCm;",
            "I",
            "Ljava/lang/String;",
            "LaeZ;",
            "LaGv;",
            "LaGn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 258
    invoke-static/range {p9 .. p10}, LCe;->a(LaGv;LaGn;)Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-static/range {p9 .. p10}, LCe;->a(LaGv;LaGn;)LbmY;

    move-result-object v10

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 258
    invoke-direct/range {v0 .. v11}, LCe;-><init>(Ljava/lang/String;ILCn;Ljava/lang/String;ZLCm;ILjava/lang/String;LaeZ;LbmY;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method private static a(LaGv;LaGn;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGv;",
            "LaGn;",
            ")",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    .line 266
    if-eqz p0, :cond_0

    .line 267
    invoke-virtual {p0}, LaGv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 270
    :cond_0
    if-eqz p1, :cond_1

    .line 271
    invoke-virtual {p1}, LaGn;->a()LbmY;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbna;->a(Ljava/lang/Iterable;)Lbna;

    .line 274
    :cond_1
    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method private static a(LaGv;LaGn;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaES;->t:LaES;

    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaGv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 281
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    invoke-virtual {p1}, LaGn;->a()LbmY;

    move-result-object v1

    invoke-static {v0, v1}, LCe;->a(LbmY;LbmY;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(LbmY;LbmY;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 293
    invoke-static {p0}, LDK;->a(LbmY;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 295
    invoke-virtual {p1}, LbmY;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 299
    sget-object v1, LaES;->r:LaES;

    .line 300
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    .line 299
    invoke-static {v1, p1}, LDK;->a(LaFr;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    .line 301
    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    sget-object v1, LaFL;->b:LaFL;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 305
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 292
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LCe;->a:Ljava/util/EnumSet;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LCe;
    .locals 1

    .prologue
    .line 39
    const-class v0, LCe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LCe;

    return-object v0
.end method

.method public static values()[LCe;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LCe;->a:[LCe;

    invoke-virtual {v0}, [LCe;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCe;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, LCe;->a:I

    return v0
.end method

.method public a()LCn;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, LCe;->a:LCn;

    return-object v0
.end method

.method public a()LIK;
    .locals 1

    .prologue
    .line 346
    sget-object v0, LIK;->c:LIK;

    return-object v0
.end method

.method public a()LaeZ;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, LCe;->a:LaeZ;

    return-object v0
.end method

.method public a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 3

    .prologue
    .line 337
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, LCe;->a:LCm;

    sget-object v2, LCm;->a:LCm;

    if-ne v1, v2, :cond_0

    .line 339
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-virtual {v0}, LaFO;->a()Ljava/lang/String;

    move-result-object v0

    .line 341
    :cond_0
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    iget-object v2, p0, LCe;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, LCe;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "LIK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    sget-object v0, LCe;->b:Ljava/util/EnumSet;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, LCe;->b:Ljava/lang/String;

    return-object v0
.end method
