.class public LaxT;
.super Ljava/lang/Object;
.source "FindInFileView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/widget/FindInFileView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/widget/FindInFileView;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, LaxT;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, LaxT;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)LaxV;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, LaxT;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)LaxV;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LaxV;->a(Ljava/lang/String;)Z

    .line 80
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    .line 81
    :goto_0
    iget-object v1, p0, LaxT;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/widget/FindInFileView;->a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 82
    iget-object v1, p0, LaxT;->a:Lcom/google/android/apps/viewer/widget/FindInFileView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/widget/FindInFileView;->b(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 83
    return-void

    .line 80
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
