.class public LapO;
.super LSE;
.source "GestureImageView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/GestureImageView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-direct {p0}, LSE;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;LapL;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0, p1}, LapO;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;)V

    return-void
.end method


# virtual methods
.method public a(LSC;)Z
    .locals 5

    .prologue
    .line 133
    iget-object v0, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)F

    move-result v0

    .line 135
    iget-object v1, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {p1}, LSC;->c()F

    move-result v2

    mul-float/2addr v2, v0

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;F)F

    move-result v1

    .line 137
    invoke-virtual {p1}, LSC;->a()F

    move-result v2

    .line 138
    invoke-virtual {p1}, LSC;->b()F

    move-result v3

    .line 140
    iget-object v4, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v4}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v4

    div-float v0, v1, v0

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(FFF)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 141
    iget-object v0, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)V

    .line 143
    iget-object v0, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)V

    .line 144
    iget-object v0, p0, LapO;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->invalidate()V

    .line 146
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
