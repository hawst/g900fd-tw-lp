.class public final enum LqH;
.super Ljava/lang/Enum;
.source "RocketEventTracker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LqH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LqH;

.field private static final synthetic a:[LqH;

.field public static final enum b:LqH;

.field public static final enum c:LqH;

.field public static final enum d:LqH;

.field public static final enum e:LqH;

.field public static final enum f:LqH;

.field public static final enum g:LqH;

.field public static final enum h:LqH;

.field public static final enum i:LqH;

.field public static final enum j:LqH;

.field public static final enum k:LqH;

.field public static final enum l:LqH;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    new-instance v0, LqH;

    const-string v1, "SHEETS_PROMO_GETAPP"

    const-string v2, "pr1g"

    invoke-direct {v0, v1, v4, v2}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->a:LqH;

    .line 46
    new-instance v0, LqH;

    const-string v1, "SHEETS_PROMO_DISMISS"

    const-string v2, "pr1d"

    invoke-direct {v0, v1, v5, v2}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->b:LqH;

    .line 47
    new-instance v0, LqH;

    const-string v1, "DOCS_PROMO_GETAPP"

    const-string v2, "pr2g"

    invoke-direct {v0, v1, v6, v2}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->c:LqH;

    .line 48
    new-instance v0, LqH;

    const-string v1, "DOCS_PROMO_DISMISS"

    const-string v2, "pr2d"

    invoke-direct {v0, v1, v7, v2}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->d:LqH;

    .line 49
    new-instance v0, LqH;

    const-string v1, "SLIDES_PROMO_GETAPP"

    const-string v2, "pr4g"

    invoke-direct {v0, v1, v8, v2}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->e:LqH;

    .line 50
    new-instance v0, LqH;

    const-string v1, "SLIDES_PROMO_DISMISS"

    const/4 v2, 0x5

    const-string v3, "pr4d"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->f:LqH;

    .line 51
    new-instance v0, LqH;

    const-string v1, "DRIVE_PROMO_GETAPP"

    const/4 v2, 0x6

    const-string v3, "pr3g"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->g:LqH;

    .line 52
    new-instance v0, LqH;

    const-string v1, "DRIVE_PROMO_DISMISS"

    const/4 v2, 0x7

    const-string v3, "pr3d"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->h:LqH;

    .line 53
    new-instance v0, LqH;

    const-string v1, "PROJECTOR_PREVIEW"

    const/16 v2, 0x8

    const-string v3, "pv"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->i:LqH;

    .line 54
    new-instance v0, LqH;

    const-string v1, "HOMESCREEN_SHOWN"

    const/16 v2, 0x9

    const-string v3, "hs"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->j:LqH;

    .line 55
    new-instance v0, LqH;

    const-string v1, "PROJECTOR_EDIT"

    const/16 v2, 0xa

    const-string v3, "ed"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->k:LqH;

    .line 56
    new-instance v0, LqH;

    const-string v1, "QUICK_OFFICE_OPEN"

    const/16 v2, 0xb

    const-string v3, "16"

    invoke-direct {v0, v1, v2, v3}, LqH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LqH;->l:LqH;

    .line 44
    const/16 v0, 0xc

    new-array v0, v0, [LqH;

    sget-object v1, LqH;->a:LqH;

    aput-object v1, v0, v4

    sget-object v1, LqH;->b:LqH;

    aput-object v1, v0, v5

    sget-object v1, LqH;->c:LqH;

    aput-object v1, v0, v6

    sget-object v1, LqH;->d:LqH;

    aput-object v1, v0, v7

    sget-object v1, LqH;->e:LqH;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LqH;->f:LqH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LqH;->g:LqH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LqH;->h:LqH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LqH;->i:LqH;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LqH;->j:LqH;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LqH;->k:LqH;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LqH;->l:LqH;

    aput-object v2, v0, v1

    sput-object v0, LqH;->a:[LqH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput-object p3, p0, LqH;->a:Ljava/lang/String;

    .line 62
    return-void
.end method

.method static synthetic a(LqH;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, LqH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LqH;
    .locals 1

    .prologue
    .line 44
    const-class v0, LqH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LqH;

    return-object v0
.end method

.method public static values()[LqH;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LqH;->a:[LqH;

    invoke-virtual {v0}, [LqH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LqH;

    return-object v0
.end method
