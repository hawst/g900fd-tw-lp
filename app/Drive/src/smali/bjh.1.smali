.class public final Lbjh;
.super Ljava/lang/Object;
.source "Randoms.java"


# static fields
.field private static final a:Ljava/security/SecureRandom;

.field private static final a:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lbjj;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbjj;-><init>(Lbji;)V

    sput-object v0, Lbjh;->a:Ljava/util/Random;

    .line 38
    invoke-static {}, Lbjh;->b()Ljava/security/SecureRandom;

    move-result-object v0

    sput-object v0, Lbjh;->a:Ljava/security/SecureRandom;

    return-void
.end method

.method public static a()Ljava/security/SecureRandom;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lbjh;->a:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private static b()Ljava/security/SecureRandom;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 133
    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    .line 134
    return-object v0
.end method
