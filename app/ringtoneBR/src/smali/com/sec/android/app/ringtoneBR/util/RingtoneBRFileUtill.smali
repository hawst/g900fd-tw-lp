.class public Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;
.super Ljava/lang/Object;
.source "RingtoneBRFileUtill.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public CheckSpaceAvilable(Ljava/lang/String;J)I
    .locals 10
    .param p1, "basePath"    # Ljava/lang/String;
    .param p2, "requiredSize"    # J

    .prologue
    .line 21
    const-wide/16 v0, 0x0

    .line 22
    .local v0, "availableBlocks":J
    const-wide/16 v2, 0x0

    .line 23
    .local v2, "blockSizeInBytes":J
    const-wide/16 v6, 0x0

    .line 25
    .local v6, "freeSpaceInBytes":J
    :try_start_0
    new-instance v5, Landroid/os/StatFs;

    invoke-direct {v5, p1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 26
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v0, v8

    .line 27
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    int-to-long v2, v8

    .line 28
    mul-long v6, v0, v2

    .line 35
    cmp-long v8, v6, p2

    if-gez v8, :cond_0

    .line 36
    const/4 v8, 0x2

    .line 38
    .end local v5    # "stat":Landroid/os/StatFs;
    :goto_0
    return v8

    .line 29
    :catch_0
    move-exception v4

    .line 30
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 32
    const/4 v8, 0x3

    goto :goto_0

    .line 38
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v5    # "stat":Landroid/os/StatFs;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public copyBackupFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 16
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "desPath"    # Ljava/lang/String;

    .prologue
    .line 84
    const-string v4, "RingtoneBRFileUtill"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "copyBackupFile srcPath="

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v13, " desPath="

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-wide/16 v6, 0x0

    .line 86
    .local v6, "fsize":J
    const/4 v9, 0x0

    .line 87
    .local v9, "fin":Ljava/io/FileInputStream;
    const/4 v11, 0x0

    .line 88
    .local v11, "fout":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 89
    .local v3, "inc":Ljava/nio/channels/FileChannel;
    const/4 v8, 0x0

    .line 92
    .local v8, "outc":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .local v10, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v12, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 95
    .end local v11    # "fout":Ljava/io/FileOutputStream;
    .local v12, "fout":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 96
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v8

    .line 98
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    .line 100
    const-wide/16 v4, 0x0

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    move-result-wide v4

    const-wide/16 v14, 0x0

    cmp-long v4, v4, v14

    if-nez v4, :cond_0

    .line 101
    const-string v4, "RingtoneBRFileUtill"

    const-string v5, "copyBackupFile inc.transferTo returns 0 byte."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 109
    :cond_0
    :try_start_3
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 110
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 111
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 112
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 117
    :goto_0
    const/4 v4, 0x1

    move-object v11, v12

    .end local v12    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "fout":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    :goto_1
    return v4

    .line 113
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v12    # "fout":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v10    # "fin":Ljava/io/FileInputStream;
    .end local v12    # "fout":Ljava/io/FileOutputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 105
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 106
    const/4 v4, 0x0

    .line 109
    :try_start_5
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 110
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 111
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 112
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 113
    :catch_2
    move-exception v2

    .line 114
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 108
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 109
    :goto_3
    :try_start_6
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 110
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 111
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 112
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 115
    :goto_4
    throw v4

    .line 113
    :catch_3
    move-exception v2

    .line 114
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 108
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v10    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v9, v10

    .end local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v12    # "fout":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v4

    move-object v11, v12

    .end local v12    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "fout":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    goto :goto_3

    .line 104
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v10    # "fin":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    move-object v9, v10

    .end local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v12    # "fout":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v2

    move-object v11, v12

    .end local v12    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "fout":Ljava/io/FileOutputStream;
    move-object v9, v10

    .end local v10    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public deleteAllFiles(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 60
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v4, "targetFolder":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 62
    .local v0, "childFile":[Ljava/io/File;
    array-length v3, v0

    .line 63
    .local v3, "size":I
    if-lez v3, :cond_1

    .line 64
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 65
    aget-object v5, v0, v2

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 66
    aget-object v5, v0, v2

    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "childFile":[Ljava/io/File;
    .end local v2    # "i":I
    .end local v3    # "size":I
    .end local v4    # "targetFolder":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 73
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method

.method public folderMemoryCheck(Ljava/lang/String;)J
    .locals 10
    .param p1, "a_path"    # Ljava/lang/String;

    .prologue
    .line 42
    const-wide/16 v6, 0x0

    .line 43
    .local v6, "totalMemory":J
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 45
    .local v2, "childFileList":[Ljava/io/File;
    if-nez v2, :cond_0

    .line 46
    const-wide/16 v8, 0x0

    .line 55
    :goto_0
    return-wide v8

    .line 48
    :cond_0
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v1, v0, v4

    .line 49
    .local v1, "childFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 50
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->folderMemoryCheck(Ljava/lang/String;)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 48
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 52
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    goto :goto_2

    .end local v1    # "childFile":Ljava/io/File;
    :cond_2
    move-wide v8, v6

    .line 55
    goto :goto_0
.end method

.method public makeDir(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "dir_path"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 80
    :cond_0
    return-object v0
.end method
