.class Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;
.super Ljava/lang/Object;
.source "RingtoneBRThread.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$OnScanCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->restoreRingtoneFiles(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field scanRecommedationTime:I

.field scanRingtonePath:Ljava/lang/String;

.field scanRingtoneType:I

.field final synthetic this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

.field final synthetic val$defaultRingtoneType:I

.field final synthetic val$finalDesPath:Ljava/lang/String;

.field final synthetic val$recommedationTime:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    iput-object p2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$finalDesPath:Ljava/lang/String;

    iput p3, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$defaultRingtoneType:I

    iput p4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$recommedationTime:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$finalDesPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtonePath:Ljava/lang/String;

    .line 291
    iget v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$defaultRingtoneType:I

    iput v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtoneType:I

    .line 292
    iget v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->val$recommedationTime:I

    iput v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRecommedationTime:I

    return-void
.end method


# virtual methods
.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtonePath:Ljava/lang/String;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtonePath:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    iget v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtoneType:I

    if-ne v2, v3, :cond_2

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    # getter for: Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3, p2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    # getter for: Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "recommendation_time"

    iget v4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRecommedationTime:I

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 302
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 304
    .local v1, "values":Landroid/content/ContentValues;
    :try_start_0
    const-string v2, "is_ringtone"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 310
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    # getter for: Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p2, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 312
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_1
    return-void

    .line 298
    :cond_2
    iget v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRingtoneType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    # getter for: Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x8

    invoke-static {v2, v3, p2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->this$0:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    # getter for: Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "recommendation_time_2"

    iget v4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;->scanRecommedationTime:I

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 305
    .restart local v1    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 306
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v2, "RingtoneBRThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 307
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 308
    .local v0, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "RingtoneBRThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UnsupportedOperationException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
