.class public Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;
.super Ljava/lang/Thread;
.source "RingtoneBRThread.java"


# static fields
.field public static EXTRA_ERR_CODE:Ljava/lang/String;

.field public static EXTRA_REQ_SIZE:Ljava/lang/String;

.field public static EXTRA_RESULT:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field public mHandler:Landroid/os/Handler;

.field private mIntent:Landroid/content/Intent;

.field mLoopEnable:Z

.field mOption:I

.field mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

.field mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

.field mSessionKey:Ljava/lang/String;

.field private mSource:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "RESULT"

    sput-object v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_RESULT:Ljava/lang/String;

    .line 44
    const-string v0, "ERR_CODE"

    sput-object v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_ERR_CODE:Ljava/lang/String;

    .line 45
    const-string v0, "REQ_SIZE"

    sput-object v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_REQ_SIZE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mOption:I

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mSessionKey:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mSource:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    .line 350
    new-instance v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$2;-><init>(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)V

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mHandler:Landroid/os/Handler;

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    .line 82
    new-instance v0, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    invoke-direct {v0}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    .line 83
    new-instance v0, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    invoke-direct {v0, p1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private backupRingtoneFiles(Ljava/lang/String;)V
    .locals 28
    .param p1, "basePath"    # Ljava/lang/String;

    .prologue
    .line 122
    const-string v25, "RingtoneBRThread"

    const-string v26, "backupRingtone"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;->getCursor()Landroid/database/Cursor;

    move-result-object v16

    .line 126
    .local v16, "ringtoneCursor":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 127
    .local v19, "strFullPath":Ljava/lang/String;
    const/16 v20, 0x0

    .line 128
    .local v20, "strUriString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-static/range {v25 .. v26}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v6

    .line 129
    .local v6, "defaultRingtoneUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-static/range {v25 .. v26}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v7

    .line 130
    .local v7, "defaultRingtoneUri2":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v26, "recommendation_time"

    const/16 v27, 0x0

    invoke-static/range {v25 .. v27}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v14

    .line 131
    .local v14, "recommendationTime":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v26, "recommendation_time_2"

    const/16 v27, 0x0

    invoke-static/range {v25 .. v27}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v15

    .line 132
    .local v15, "recommendationTime2":I
    const/16 v21, 0x0

    .line 133
    .local v21, "tempRecommendationTime":I
    const/4 v5, 0x0

    .line 134
    .local v5, "defaultRingtoneType":I
    const-wide/16 v22, 0x0

    .line 135
    .local v22, "totalSize":J
    const/16 v24, 0x0

    .line 137
    .local v24, "useSameDefaultRingtone":Z
    if-eqz v6, :cond_0

    if-eqz v7, :cond_0

    .line 138
    invoke-virtual {v6, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 140
    const/16 v24, 0x1

    .line 143
    :cond_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v17, "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v25

    if-nez v25, :cond_2

    .line 145
    const/16 v25, 0x1

    const/16 v26, 0x3

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    .line 224
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;->getUriFromCursor(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    .line 154
    if-nez v20, :cond_3

    .line 155
    const-string v25, "RingtoneBRThread"

    const-string v26, "ringtone uri = null"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/16 v25, 0x1

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v9

    .line 188
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 189
    const/16 v25, 0x1

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto :goto_0

    .line 159
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    sget-object v25, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 186
    :cond_4
    :goto_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v25

    if-nez v25, :cond_2

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->CheckSpaceAvilable(Ljava/lang/String;J)I

    move-result v4

    .line 195
    .local v4, "checkResult":I
    if-eqz v4, :cond_8

    .line 196
    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_0

    .line 162
    .end local v4    # "checkResult":I
    :cond_5
    :try_start_2
    const-string v25, "_data"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 164
    const-string v25, "_size"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    add-long v22, v22, v26

    .line 166
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 167
    const/4 v5, 0x1

    .line 168
    move/from16 v21, v14

    .line 176
    :goto_2
    sget-object v25, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 177
    .local v10, "fileName":Ljava/lang/String;
    new-instance v12, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-direct {v12, v0, v10, v1, v5}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 178
    .local v12, "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    const/16 v25, 0x1

    move/from16 v0, v25

    if-ne v5, v0, :cond_4

    if-eqz v24, :cond_4

    .line 181
    const/4 v5, 0x2

    .line 182
    move/from16 v21, v15

    .line 183
    new-instance v13, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-direct {v13, v0, v10, v1, v5}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 184
    .local v13, "object2":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 169
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .end local v13    # "object2":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    :cond_6
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v25

    if-eqz v25, :cond_7

    .line 170
    const/4 v5, 0x2

    .line 171
    move/from16 v21, v15

    goto :goto_2

    .line 173
    :cond_7
    const/4 v5, 0x0

    .line 174
    const/16 v21, 0x0

    goto :goto_2

    .line 200
    .restart local v4    # "checkResult":I
    :cond_8
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    .line 201
    .restart local v12    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1

    .line 204
    invoke-virtual {v12}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->getPath()Ljava/lang/String;

    move-result-object v18

    .line 205
    .local v18, "srcPath":Ljava/lang/String;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    sget-object v26, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v26

    add-int/lit8 v26, v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 206
    .local v8, "desPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->copyBackupFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_9

    .line 207
    const-string v25, "RingtoneBRThread"

    const-string v26, "Failed to write ringtonefiles"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/16 v25, 0x1

    const/16 v26, 0x3

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_0

    .line 214
    .end local v8    # "desPath":Ljava/lang/String;
    .end local v12    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .end local v18    # "srcPath":Ljava/lang/String;
    :cond_a
    :try_start_3
    new-instance v25, Lcom/sec/android/app/ringtoneBR/xml/GenerateXML;

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/ringtoneBR/xml/GenerateXML;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 223
    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_0

    .line 215
    :catch_1
    move-exception v9

    .line 217
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 218
    const/16 v25, 0x1

    const/16 v26, 0x3

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_0
.end method

.method private restoreRingtoneFiles(Ljava/lang/String;)V
    .locals 32
    .param p1, "basePath"    # Ljava/lang/String;

    .prologue
    .line 227
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "ringtone.xml"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 228
    .local v27, "xmlPath":Ljava/lang/String;
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "/Ringtones"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 229
    .local v19, "restoreRingtonePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->folderMemoryCheck(Ljava/lang/String;)J

    move-result-wide v16

    .line 230
    .local v16, "nBaseFolderTotalSize":J
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 233
    .local v26, "values_is_ringtone":Landroid/content/ContentValues;
    :try_start_0
    const-string v28, "is_ringtone"

    const-string v29, "1"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 239
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->makeDir(Ljava/lang/String;)Ljava/io/File;

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->CheckSpaceAvilable(Ljava/lang/String;J)I

    move-result v6

    .line 242
    .local v6, "checkResult":I
    if-eqz v6, :cond_1

    .line 243
    const/16 v28, 0x1

    const/16 v29, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    .line 334
    :cond_0
    :goto_1
    return-void

    .line 234
    .end local v6    # "checkResult":I
    :catch_0
    move-exception v10

    .line 235
    .local v10, "ex":Ljava/lang/IllegalArgumentException;
    const-string v28, "RingtoneBRThread"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "IllegalArgumentException occured :"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    .end local v10    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v10

    .line 237
    .local v10, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v28, "RingtoneBRThread"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "UnsupportedOperationException occured :"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v10}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 248
    .end local v10    # "ex":Ljava/lang/UnsupportedOperationException;
    .restart local v6    # "checkResult":I
    :cond_1
    :try_start_1
    new-instance v22, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 249
    .local v22, "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->getItemsList()Ljava/util/ArrayList;

    move-result-object v20

    .line 250
    .local v20, "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    .line 251
    .local v15, "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    move/from16 v28, v0

    if-eqz v28, :cond_0

    .line 254
    invoke-virtual {v15}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 255
    .local v8, "desPath":Ljava/lang/String;
    sget-object v28, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v28

    add-int/lit8 v28, v28, 0x1

    move/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 256
    .local v12, "fileName":Ljava/lang/String;
    const/16 v28, 0x0

    sget-object v29, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v29

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 257
    .local v11, "fileFolder":Ljava/lang/String;
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 258
    .local v23, "srcPath":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->getDefaultType()I

    move-result v7

    .line 259
    .local v7, "defaultRingtoneType":I
    invoke-virtual {v15}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->getRecommendationTime()I

    move-result v18

    .line 261
    .local v18, "recommedationTime":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRCursorUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;

    move-object/from16 v28, v0

    sget-object v29, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRCursorUtill;->getUriByPath(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v21

    .line 262
    .local v21, "ringtoneUri":Landroid/net/Uri;
    if-eqz v21, :cond_4

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    move-object/from16 v3, v29

    move-object/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 265
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v7, v0, :cond_3

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v29

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    const-string v29, "recommendation_time"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    goto/16 :goto_2

    .line 317
    .end local v7    # "defaultRingtoneType":I
    .end local v8    # "desPath":Ljava/lang/String;
    .end local v11    # "fileFolder":Ljava/lang/String;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .end local v18    # "recommedationTime":I
    .end local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .end local v21    # "ringtoneUri":Landroid/net/Uri;
    .end local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    .end local v23    # "srcPath":Ljava/lang/String;
    :catch_2
    move-exception v9

    .line 319
    .local v9, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v9}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    .line 320
    const/16 v28, 0x1

    const/16 v29, 0x1

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_1

    .line 268
    .end local v9    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    .restart local v7    # "defaultRingtoneType":I
    .restart local v8    # "desPath":Ljava/lang/String;
    .restart local v11    # "fileFolder":Ljava/lang/String;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .restart local v18    # "recommedationTime":I
    .restart local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .restart local v21    # "ringtoneUri":Landroid/net/Uri;
    .restart local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    .restart local v23    # "srcPath":Ljava/lang/String;
    :cond_3
    const/16 v28, 0x2

    move/from16 v0, v28

    if-ne v7, v0, :cond_2

    .line 269
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const/16 v29, 0x8

    move-object/from16 v0, v28

    move/from16 v1, v29

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    const-string v29, "recommendation_time_2"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_2

    .line 322
    .end local v7    # "defaultRingtoneType":I
    .end local v8    # "desPath":Ljava/lang/String;
    .end local v11    # "fileFolder":Ljava/lang/String;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .end local v18    # "recommedationTime":I
    .end local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .end local v21    # "ringtoneUri":Landroid/net/Uri;
    .end local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    .end local v23    # "srcPath":Ljava/lang/String;
    :catch_3
    move-exception v9

    .line 324
    .local v9, "e":Lorg/xml/sax/SAXException;
    invoke-virtual {v9}, Lorg/xml/sax/SAXException;->printStackTrace()V

    .line 325
    const/16 v28, 0x1

    const/16 v29, 0x1

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_1

    .line 273
    .end local v9    # "e":Lorg/xml/sax/SAXException;
    .restart local v7    # "defaultRingtoneType":I
    .restart local v8    # "desPath":Ljava/lang/String;
    .restart local v11    # "fileFolder":Ljava/lang/String;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .restart local v18    # "recommedationTime":I
    .restart local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .restart local v21    # "ringtoneUri":Landroid/net/Uri;
    .restart local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    .restart local v23    # "srcPath":Ljava/lang/String;
    :cond_4
    :try_start_3
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    .local v24, "tempFile":Ljava/io/File;
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v28

    if-nez v28, :cond_6

    .line 275
    new-instance v25, Ljava/io/File;

    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 276
    .local v25, "tempFolder":Ljava/io/File;
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v28

    if-nez v28, :cond_5

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->mkdirs()Z

    move-result v28

    if-nez v28, :cond_5

    .line 277
    const-string v28, "RingtoneBRThread"

    const-string v29, "Failed to make folder, use default ringtone folder"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget-object v29, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 281
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->copyBackupFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v28

    if-nez v28, :cond_6

    .line 282
    const-string v28, "RingtoneBRThread"

    const-string v29, "Failed to write ringtonefiles"

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const/16 v28, 0x1

    const/16 v29, 0x3

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    .line 286
    .end local v25    # "tempFolder":Ljava/io/File;
    :cond_6
    move-object v13, v8

    .line 287
    .local v13, "finalDesPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v28

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    aput-object v13, v29, v30

    const/16 v30, 0x0

    new-instance v31, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move/from16 v2, v18

    invoke-direct {v0, v1, v13, v7, v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread$1;-><init>(Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;Ljava/lang/String;II)V

    invoke-static/range {v28 .. v31}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    :try_end_3
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_2

    .line 327
    .end local v7    # "defaultRingtoneType":I
    .end local v8    # "desPath":Ljava/lang/String;
    .end local v11    # "fileFolder":Ljava/lang/String;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v13    # "finalDesPath":Ljava/lang/String;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "object":Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
    .end local v18    # "recommedationTime":I
    .end local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .end local v21    # "ringtoneUri":Landroid/net/Uri;
    .end local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    .end local v23    # "srcPath":Ljava/lang/String;
    .end local v24    # "tempFile":Ljava/io/File;
    :catch_4
    move-exception v9

    .line 329
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 330
    const/16 v28, 0x1

    const/16 v29, 0x1

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_1

    .line 333
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v20    # "ringtoneList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;>;"
    .restart local v22    # "ringtoneXmlParser":Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
    :cond_7
    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto/16 :goto_1
.end method

.method private sendResponse(III)V
    .locals 3
    .param p1, "result"    # I
    .param p2, "errorCode"    # I
    .param p3, "reqSize"    # I

    .prologue
    .line 337
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 338
    .local v0, "intent":Landroid/content/Intent;
    iget v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mOption:I

    if-nez v1, :cond_1

    .line 339
    const-string v1, "android.intent.action.RESPONSE_BACKUP_RINGTONE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 344
    sget-object v1, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_ERR_CODE:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 345
    sget-object v1, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->EXTRA_REQ_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 346
    const-string v1, "SOURCE"

    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mSource:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mContext:Landroid/content/Context;

    const-string v2, "com.wssnps.permission.COM_WSSNPS"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 348
    return-void

    .line 340
    :cond_1
    iget v1, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mOption:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 341
    const-string v1, "android.intent.action.RESPONSE_RESTORE_RINGTONE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    if-nez v2, :cond_1

    .line 90
    const-string v2, "RingtoneBRThread"

    const-string v3, "intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    const-string v3, "SAVE_PATH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "basePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    const-string v3, "ACTION"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 95
    .local v0, "action":I
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    const-string v3, "SESSION_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mSessionKey:Ljava/lang/String;

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    const-string v3, "SOURCE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mSource:Ljava/lang/String;

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.intent.action.REQUEST_BACKUP_RINGTONE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 100
    iput v5, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mOption:I

    .line 101
    if-nez v0, :cond_2

    .line 102
    iput-boolean v4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mRingtoneBRFileUtill:Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/ringtoneBR/util/RingtoneBRFileUtill;->deleteAllFiles(Ljava/lang/String;)V

    .line 106
    invoke-direct {p0, v1}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->backupRingtoneFiles(Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_2
    invoke-direct {p0, v4, v6, v5}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.intent.action.REQUEST_RESTORE_RINGTONE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    iput v4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mOption:I

    .line 112
    if-nez v0, :cond_4

    .line 113
    iput-boolean v4, p0, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mLoopEnable:Z

    .line 114
    invoke-direct {p0, v1}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->restoreRingtoneFiles(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_4
    invoke-direct {p0, v4, v6, v5}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->sendResponse(III)V

    goto :goto_0
.end method
