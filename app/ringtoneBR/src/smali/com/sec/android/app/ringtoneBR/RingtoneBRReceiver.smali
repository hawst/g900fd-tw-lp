.class public Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RingtoneBRReceiver.java"


# static fields
.field static mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x2

    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.intent.action.REQUEST_BACKUP_RINGTONE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.sec.android.intent.action.REQUEST_RESTORE_RINGTONE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    :cond_0
    const-string v2, "ACTION"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 25
    .local v1, "extAction":I
    if-ne v1, v4, :cond_2

    .line 26
    sget-object v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 27
    const-string v2, "RingtoneBRReceiver"

    const-string v3, "Cancel request"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    sget-object v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    iget-object v2, v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 35
    .end local v1    # "extAction":I
    :cond_1
    :goto_0
    return-void

    .line 31
    .restart local v1    # "extAction":I
    :cond_2
    new-instance v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    invoke-direct {v2, p1, p2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    sput-object v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    .line 32
    sget-object v2, Lcom/sec/android/app/ringtoneBR/RingtoneBRReceiver;->mKiesThread:Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;

    invoke-virtual {v2}, Lcom/sec/android/app/ringtoneBR/RingtoneBRThread;->start()V

    goto :goto_0
.end method
