.class public Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "RingtoneXmlParser.java"


# instance fields
.field private itemsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentElement:Ljava/lang/Boolean;

.field private mCurrentValue:Ljava/lang/String;

.field private mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 23
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentElement:Ljava/lang/Boolean;

    .line 24
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    .line 25
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    .line 26
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->itemsList:Ljava/util/ArrayList;

    .line 30
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    .line 31
    .local v2, "saxParserFactory":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    .line 32
    .local v1, "saxParser":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v3

    .line 33
    .local v3, "xmlReader":Lorg/xml/sax/XMLReader;
    invoke-interface {v3, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 34
    new-instance v0, Lorg/xml/sax/InputSource;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v4}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 35
    .local v0, "objSource":Lorg/xml/sax/InputSource;
    invoke-interface {v3, v0}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 36
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentElement:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    .line 81
    :cond_0
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentElement:Ljava/lang/Boolean;

    .line 64
    const-string v0, "name"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->setFilename(Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const-string v0, "path"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->setPath(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_2
    const-string v0, "recommend_time"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->setRecommendationTime(I)V

    goto :goto_0

    .line 70
    :cond_3
    const-string v0, "default_type"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->setDefaultType(I)V

    goto :goto_0
.end method

.method public getItemsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->itemsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 45
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentElement:Ljava/lang/Boolean;

    .line 46
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mCurrentValue:Ljava/lang/String;

    .line 48
    const-string v0, "file"

    .line 50
    .local v0, "objListTag":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    new-instance v1, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    invoke-direct {v1}, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->itemsList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtoneXmlParser;->mItem:Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    return-void
.end method
