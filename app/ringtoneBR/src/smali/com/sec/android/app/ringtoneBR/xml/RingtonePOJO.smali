.class public Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;
.super Ljava/lang/Object;
.source "RingtonePOJO.java"


# instance fields
.field private defaultType:I

.field private filename:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private recommandTime:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "recommandTime"    # I
    .param p4, "defaultType"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->path:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->filename:Ljava/lang/String;

    .line 16
    iput p3, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->recommandTime:I

    .line 17
    iput p4, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->defaultType:I

    .line 18
    return-void
.end method


# virtual methods
.method public getDefaultType()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->defaultType:I

    return v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->filename:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendationTime()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->recommandTime:I

    return v0
.end method

.method public setDefaultType(I)V
    .locals 0
    .param p1, "defaultType"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->defaultType:I

    .line 38
    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->filename:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->path:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setRecommendationTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/sec/android/app/ringtoneBR/xml/RingtonePOJO;->recommandTime:I

    .line 46
    return-void
.end method
