.class public abstract Landroid/support/v4/app/PagerAdapterShim;
.super Landroid/support/v4/view/PagerAdapter;
.source "PagerAdapterShim.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    return-void
.end method

.method protected static getFragmentSavedFragmentState(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 12
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    return-object v0
.end method

.method protected static getSavedStateInternalState(Landroid/support/v4/app/Fragment$SavedState;)Landroid/os/Bundle;
    .locals 1
    .param p0, "savedState"    # Landroid/support/v4/app/Fragment$SavedState;

    .prologue
    .line 16
    iget-object v0, p0, Landroid/support/v4/app/Fragment$SavedState;->mState:Landroid/os/Bundle;

    return-object v0
.end method
