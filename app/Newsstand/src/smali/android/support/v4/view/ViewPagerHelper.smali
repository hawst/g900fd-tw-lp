.class public Landroid/support/v4/view/ViewPagerHelper;
.super Ljava/lang/Object;
.source "ViewPagerHelper.java"


# direct methods
.method public static getChildViewPosition(Landroid/support/v4/view/ViewPager;Landroid/view/View;)Ljava/lang/Integer;
    .locals 2
    .param p0, "viewPager"    # Landroid/support/v4/view/ViewPager;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/ViewPager$ItemInfo;

    move-result-object v0

    .line 25
    .local v0, "ii":Landroid/support/v4/view/ViewPager$ItemInfo;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget v1, v0, Landroid/support/v4/view/ViewPager$ItemInfo;->position:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method
