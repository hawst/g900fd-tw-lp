.class public Landroid/support/v4/view/NSViewPager;
.super Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;
.source "NSViewPager.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private attemptedToPageInReverse:Z

.field private currentlyPagingPosition:I

.field private firstPageSelectedSent:Z

.field private ignoreChildHorizontalScrolling:Z

.field private isSettingCurrentItem:Z

.field private onPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Landroid/support/v4/view/NSViewPager;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Landroid/support/v4/view/NSViewPager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method private callOnFirstPageSelectedIfNeeded()V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->firstPageSelectedSent:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->firstPageSelectedSent:Z

    .line 129
    iget-object v0, p0, Landroid/support/v4/view/NSViewPager;->onPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Landroid/support/v4/view/NSViewPager;->onPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 133
    :cond_0
    return-void
.end method


# virtual methods
.method protected canScroll(Landroid/view/View;ZIII)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "checkV"    # Z
    .param p3, "dx"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    .line 76
    iget-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->ignoreChildHorizontalScrolling:Z

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 82
    .end local p1    # "v":Landroid/view/View;
    :goto_0
    return v0

    .line 79
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    if-eqz v0, :cond_1

    .line 80
    check-cast p1, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->canScroll(I)Z

    move-result v0

    goto :goto_0

    .line 82
    .restart local p1    # "v":Landroid/view/View;
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 174
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 175
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/view/NSViewPager;->attemptedToPageInReverse:Z

    .line 176
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    .line 178
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public getCurrentPageView()Landroid/view/View;
    .locals 5

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v1

    .line 43
    .local v1, "currentItem":I
    const/4 v0, 0x0

    .local v0, "child":I
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 44
    invoke-virtual {p0, v0}, Landroid/support/v4/view/NSViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 45
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p0, v3}, Landroid/support/v4/view/NSViewPager;->getChildViewPosition(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v2

    .line 46
    .local v2, "position":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 43
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 52
    .end local v2    # "position":Ljava/lang/Integer;
    .end local v3    # "view":Landroid/view/View;
    :goto_1
    return-object v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getPageViews()[Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 57
    new-array v6, v8, [Landroid/view/View;

    .line 58
    .local v6, "views":[Landroid/view/View;
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getChildCount()I

    move-result v1

    .line 59
    .local v1, "childCount":I
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getCurrentItem()I

    move-result v2

    .line 60
    .local v2, "currentItem":I
    const/4 v0, 0x0

    .local v0, "child":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 61
    invoke-virtual {p0, v0}, Landroid/support/v4/view/NSViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 62
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p0, v4}, Landroid/support/v4/view/NSViewPager;->getChildViewPosition(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v3

    .line 63
    .local v3, "position":Ljava/lang/Integer;
    if-nez v3, :cond_1

    .line 60
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sub-int/2addr v7, v2

    add-int/lit8 v5, v7, 0x1

    .line 67
    .local v5, "viewIndex":I
    if-ltz v5, :cond_0

    if-ge v5, v8, :cond_0

    .line 68
    aput-object v4, v6, v5

    goto :goto_1

    .line 71
    .end local v3    # "position":Ljava/lang/Integer;
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "viewIndex":I
    :cond_2
    return-object v6
.end method

.method public isSettingCurrentItem()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 138
    .local v0, "action":I
    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 141
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->isFakeDragging()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 142
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->endFakeDrag()V

    .line 147
    :cond_1
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 153
    :goto_0
    return v2

    .line 148
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Landroid/support/v4/view/NSViewPager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, " in NSViewPager.onInterceptTouchEvent"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 93
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onLayout(ZIIII)V

    .line 94
    invoke-direct {p0}, Landroid/support/v4/view/NSViewPager;->callOnFirstPageSelectedIfNeeded()V

    .line 95
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 161
    iget v0, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 162
    iput p1, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    .line 168
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onPageScrolled(IFI)V

    .line 169
    return-void

    .line 165
    :cond_1
    iget v0, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    if-eq v0, p1, :cond_0

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->attemptedToPageInReverse:Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, -0x1

    .line 185
    :try_start_0
    iget-boolean v4, p0, Landroid/support/v4/view/NSViewPager;->attemptedToPageInReverse:Z

    if-eqz v4, :cond_2

    .line 186
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getCurrentPageView()Landroid/view/View;

    move-result-object v0

    .line 187
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_2

    const/4 v4, -0x1

    .line 188
    invoke-virtual {v0, v4}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 191
    :cond_0
    iget v4, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    if-eq v4, v5, :cond_1

    .line 192
    const/4 v4, -0x1

    iput v4, p0, Landroid/support/v4/view/NSViewPager;->currentlyPagingPosition:I

    .line 193
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 194
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 204
    .end local v0    # "currentView":Landroid/view/View;
    :cond_1
    :goto_0
    return v2

    .line 200
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 201
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Landroid/support/v4/view/NSViewPager;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, " in NSViewPager.onTouchEvent"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    .line 204
    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v4/view/PagerAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/support/v4/view/NSViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->firstPageSelectedSent:Z

    .line 102
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 103
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "item"    # I

    .prologue
    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem:Z

    .line 108
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(I)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem:Z

    .line 110
    invoke-direct {p0}, Landroid/support/v4/view/NSViewPager;->callOnFirstPageSelectedIfNeeded()V

    .line 111
    return-void
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem:Z

    .line 116
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(IZ)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem:Z

    .line 118
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 87
    iput-object p1, p0, Landroid/support/v4/view/NSViewPager;->onPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 88
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 89
    return-void
.end method
