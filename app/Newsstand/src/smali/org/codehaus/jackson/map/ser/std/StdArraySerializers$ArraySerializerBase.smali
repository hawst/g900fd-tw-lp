.class public abstract Lorg/codehaus/jackson/map/ser/std/StdArraySerializers$ArraySerializerBase;
.super Lorg/codehaus/jackson/map/ser/std/ContainerSerializerBase;
.source "StdArraySerializers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/jackson/map/ser/std/StdArraySerializers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ArraySerializerBase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/codehaus/jackson/map/ser/std/ContainerSerializerBase",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final _property:Lorg/codehaus/jackson/map/BeanProperty;

.field protected final _valueTypeSerializer:Lorg/codehaus/jackson/map/TypeSerializer;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;Lorg/codehaus/jackson/map/TypeSerializer;Lorg/codehaus/jackson/map/BeanProperty;)V
    .locals 0
    .param p2, "vts"    # Lorg/codehaus/jackson/map/TypeSerializer;
    .param p3, "property"    # Lorg/codehaus/jackson/map/BeanProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lorg/codehaus/jackson/map/TypeSerializer;",
            "Lorg/codehaus/jackson/map/BeanProperty;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/codehaus/jackson/map/ser/std/StdArraySerializers$ArraySerializerBase;, "Lorg/codehaus/jackson/map/ser/std/StdArraySerializers$ArraySerializerBase<TT;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0, p1}, Lorg/codehaus/jackson/map/ser/std/ContainerSerializerBase;-><init>(Ljava/lang/Class;)V

    .line 47
    iput-object p2, p0, Lorg/codehaus/jackson/map/ser/std/StdArraySerializers$ArraySerializerBase;->_valueTypeSerializer:Lorg/codehaus/jackson/map/TypeSerializer;

    .line 48
    iput-object p3, p0, Lorg/codehaus/jackson/map/ser/std/StdArraySerializers$ArraySerializerBase;->_property:Lorg/codehaus/jackson/map/BeanProperty;

    .line 49
    return-void
.end method
