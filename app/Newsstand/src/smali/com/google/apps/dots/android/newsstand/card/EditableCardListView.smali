.class public Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;
.super Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
.source "EditableCardListView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private dragPointerId:I

.field private dragScrollSpeed:I

.field private final dragShadowPadding:Landroid/graphics/Point;

.field private draggedBitmap:Landroid/graphics/Bitmap;

.field private draggedImageView:Landroid/widget/ImageView;

.field private final draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private draggedView:Landroid/widget/FrameLayout;

.field private final draggedViewStartWindowPos:[I

.field private editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

.field private editedId:Ljava/lang/Object;

.field private final editedViewPadding:Landroid/graphics/Point;

.field private final hideRemoveViewRunnable:Ljava/lang/Runnable;

.field private isDragScrolling:Z

.field private mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

.field private pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

.field private final removeInterpolator:Landroid/view/animation/Interpolator;

.field private removeView:Landroid/view/View;

.field private removeViewAdded:Z

.field private final removeViewHeight:I

.field private removeViewHotspot:Landroid/view/View;

.field private startDragX:F

.field private startDragY:F

.field private final tempRect:Landroid/graphics/Rect;

.field private final updateOperationRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, -0x2

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->NORMAL:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    .line 68
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeInterpolator:Landroid/view/animation/Interpolator;

    .line 71
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    .line 72
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedViewPadding:Landroid/graphics/Point;

    .line 74
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->tempRect:Landroid/graphics/Rect;

    .line 76
    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    .line 77
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 98
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->editable_cardlist_remove:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    .line 99
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->remove_hotspot:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewHotspot:Landroid/view/View;

    .line 100
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewHeight:I

    .line 104
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateOperationRunnable:Ljava/lang/Runnable;

    .line 115
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$2;-><init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->hideRemoveViewRunnable:Ljava/lang/Runnable;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isOperationRemove()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateRemoveView(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateDraggableView(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isDragScrolling:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->scrollListIfNeeded()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    return-object v0
.end method

.method private addDraggedView(Landroid/view/View;)V
    .locals 12
    .param p1, "editedView"    # Landroid/view/View;

    .prologue
    const/4 v10, -0x2

    const v11, 0x3f99999a    # 1.2f

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 315
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    if-nez v6, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 316
    new-instance v6, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    .line 318
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v4

    .line 319
    .local v4, "wasPressed":Z
    if-eqz v4, :cond_0

    .line 320
    invoke-virtual {p1, v8}, Landroid/view/View;->setPressed(Z)V

    .line 322
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v6, v9

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    sub-int v5, v6, v9

    .line 324
    .local v5, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v9

    sub-int/2addr v6, v9

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    sub-int v2, v6, v9

    .line 325
    .local v2, "height":I
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v2, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedBitmap:Landroid/graphics/Bitmap;

    .line 326
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 327
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v0, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 328
    invoke-virtual {p1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 329
    if-eqz v4, :cond_1

    .line 330
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 332
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 333
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    const/16 v9, 0xe5

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 334
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v9, Lcom/google/android/apps/newsstanddev/R$drawable;->drag_shadow:I

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 335
    .local v1, "dragShadow":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 336
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    invoke-virtual {p1, v6}, Landroid/view/View;->getLocationInWindow([I)V

    .line 337
    new-instance v6, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    .line 338
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 340
    .local v3, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v9, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 341
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->tempRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v6}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 342
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->tempRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iput v9, v6, Landroid/graphics/Point;->x:I

    .line 343
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->tempRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iput v9, v6, Landroid/graphics/Point;->y:I

    .line 344
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v9, v6, v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int/2addr v9, v10

    aput v9, v6, v8

    .line 345
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v9, v6, v7

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    sub-int/2addr v9, v10

    aput v9, v6, v7

    .line 346
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v9, v6, v8

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    aput v9, v6, v8

    .line 347
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v9, v6, v7

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v10

    add-int/2addr v9, v10

    aput v9, v6, v7

    .line 348
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v8, v9, v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 349
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v7, v8, v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 350
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    neg-int v7, v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 351
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    neg-int v7, v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 352
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->addWindowOverlayView(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 353
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 354
    invoke-virtual {v6, v11}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 355
    invoke-virtual {v6, v11}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const-wide/16 v8, 0x64

    .line 356
    invoke-virtual {v6, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 357
    return-void

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "dragShadow":Landroid/graphics/drawable/Drawable;
    .end local v2    # "height":I
    .end local v3    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v4    # "wasPressed":Z
    .end local v5    # "width":I
    :cond_2
    move v6, v8

    .line 315
    goto/16 :goto_0
.end method

.method private clearDraggedView(Z)V
    .locals 12
    .param p1, "shrink"    # Z

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const-wide/16 v10, 0xc8

    const/4 v9, 0x0

    .line 428
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    if-eqz p1, :cond_0

    .line 430
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    .line 432
    .local v8, "oldDraggedView":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 433
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 434
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 435
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 436
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$4;

    invoke-direct {v0, p0, v8}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$4;-><init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Landroid/view/View;)V

    invoke-virtual {p0, v0, v10, v11}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 476
    .end local v8    # "oldDraggedView":Landroid/view/View;
    :goto_0
    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedBitmap:Landroid/graphics/Bitmap;

    .line 477
    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    .line 478
    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    .line 479
    iput-object v9, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    .line 480
    return-void

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getViewForId(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    .line 445
    .local v2, "placeHolderView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 447
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 448
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v0, v1

    .line 449
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    const/4 v3, 0x0

    aget v1, v1, v3

    sub-int/2addr v0, v1

    .line 450
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedViewPadding:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int v6, v0, v1

    .line 451
    .local v6, "offsetX":I
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragShadowPadding:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v1

    .line 452
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    const/4 v3, 0x1

    aget v1, v1, v3

    sub-int/2addr v0, v1

    .line 453
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedViewPadding:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int v7, v0, v1

    .line 454
    .local v7, "offsetY":I
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    .line 456
    .restart local v8    # "oldDraggedView":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 457
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 458
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-int v1, v6

    int-to-float v1, v1

    .line 459
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    neg-int v1, v7

    int-to-float v1, v1

    .line 460
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 461
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 464
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    sget-object v3, Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;->SHOW_SOURCE_HIDE_DESTINATION:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    const-wide/16 v4, 0xa0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->captureView(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;J)V

    .line 466
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$5;

    invoke-direct {v0, p0, v8}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$5;-><init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Landroid/view/View;)V

    invoke-virtual {p0, v0, v10, v11}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 473
    .end local v6    # "offsetX":I
    .end local v7    # "offsetY":I
    .end local v8    # "oldDraggedView":Landroid/view/View;
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->removeWindowOverlayView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    return-object v0
.end method

.method private getRemoveViewLayoutParams()Landroid/widget/RelativeLayout$LayoutParams;
    .locals 7

    .prologue
    .line 530
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 531
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 532
    .local v4, "window":Landroid/view/Window;
    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 533
    iget v3, v2, Landroid/graphics/Rect;->top:I

    .line 534
    .local v3, "statusBarHeight":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 535
    .local v0, "actionBarHeight":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v1, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 537
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v5, 0x0

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 538
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 539
    return-object v1
.end method

.method private static getViewForId(Landroid/view/View;Ljava/lang/Object;)Landroid/view/View;
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "id"    # Ljava/lang/Object;

    .prologue
    .line 292
    instance-of v6, p0, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v6, :cond_0

    move-object v2, p0

    .line 293
    check-cast v2, Lcom/google/android/libraries/bind/data/DataView;

    .line 294
    .local v2, "dataView":Lcom/google/android/libraries/bind/data/DataView;
    invoke-interface {v2}, Lcom/google/android/libraries/bind/data/DataView;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 295
    .local v1, "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 296
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 310
    .end local v1    # "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    .end local v2    # "dataView":Lcom/google/android/libraries/bind/data/DataView;
    .end local p0    # "view":Landroid/view/View;
    :goto_0
    return-object p0

    .line 300
    .restart local p0    # "view":Landroid/view/View;
    :cond_0
    instance-of v6, p0, Landroid/view/ViewGroup;

    if-eqz v6, :cond_2

    move-object v5, p0

    .line 301
    check-cast v5, Landroid/view/ViewGroup;

    .line 302
    .local v5, "viewGroup":Landroid/view/ViewGroup;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 303
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 304
    .local v0, "childView":Landroid/view/View;
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getViewForId(Landroid/view/View;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    .line 305
    .local v3, "foundView":Landroid/view/View;
    if-eqz v3, :cond_1

    move-object p0, v3

    .line 306
    goto :goto_0

    .line 302
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 310
    .end local v0    # "childView":Landroid/view/View;
    .end local v3    # "foundView":Landroid/view/View;
    .end local v4    # "i":I
    .end local v5    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private getViewForId(Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "id"    # Ljava/lang/Object;

    .prologue
    .line 288
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getViewForId(Landroid/view/View;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private handleEditOnTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 244
    :cond_0
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 228
    :pswitch_0
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragPointerId:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 229
    .local v0, "pointerIndex":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 230
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 231
    .local v1, "x":F
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 232
    .local v2, "y":F
    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateDraggedView(FF)V

    .line 233
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateDragScrollSpeed(F)V

    .line 234
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateEditOperation()V

    goto :goto_0

    .line 241
    .end local v0    # "pointerIndex":I
    .end local v1    # "x":F
    .end local v2    # "y":F
    :pswitch_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->NORMAL:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setMode(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;)V

    .line 242
    const/4 v3, 0x0

    goto :goto_1

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private isOperationRemove()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 125
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    iget v1, v1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    if-eq v1, v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 127
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/CardGroup;->getEditOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v1

    iget v1, v1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    if-ne v1, v0, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scrollListIfNeeded()V
    .locals 4

    .prologue
    .line 386
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isDragScrolling:Z

    if-nez v0, :cond_0

    .line 387
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->smoothScrollBy(II)V

    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isDragScrolling:Z

    .line 389
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$3;-><init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)V

    const-wide/16 v2, 0x190

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 397
    :cond_0
    return-void
.end method

.method private setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 5
    .param p1, "editOperation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 168
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardGroup;->getEditOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "setEditOperation: %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2, p1}, Lcom/google/android/libraries/bind/card/CardGroup;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 171
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 172
    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardGroup;->getEditOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    iget v2, v2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    if-ne v2, v0, :cond_1

    .line 173
    .local v0, "isRemove":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateRemoveView(Z)V

    .line 174
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateDraggableView(Z)V

    .line 176
    .end local v0    # "isRemove":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 172
    goto :goto_0
.end method

.method private setPendingEditionOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;J)V
    .locals 2
    .param p1, "editOperation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .param p2, "pendingOperationDelay"    # J

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateOperationRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 160
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateOperationRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 165
    :cond_0
    return-void
.end method

.method private updateDragScrollSpeed(F)V
    .locals 4
    .param p1, "fy"    # F

    .prologue
    .line 372
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 373
    .local v2, "y":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 374
    .local v0, "draggedViewHeight":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isOperationRemove()Z

    move-result v1

    .line 375
    .local v1, "isRemove":Z
    if-nez v1, :cond_0

    if-ge v2, v0, :cond_0

    .line 376
    sub-int v3, v2, v0

    div-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    .line 382
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->scrollListIfNeeded()V

    .line 383
    return-void

    .line 377
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getHeight()I

    move-result v3

    sub-int/2addr v3, v0

    if-le v2, v3, :cond_1

    .line 378
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getHeight()I

    move-result v3

    sub-int/2addr v3, v2

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    goto :goto_0

    .line 380
    :cond_1
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    goto :goto_0
.end method

.method private updateDraggableView(Z)V
    .locals 3
    .param p1, "isRemove"    # Z

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->editable_cardlist_text_highlight:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 525
    .local v0, "removeColor":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 526
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilter(IZ)Landroid/graphics/ColorFilter;

    move-result-object v1

    .line 525
    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 527
    return-void

    .line 526
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateDraggedView(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 360
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragX:F

    sub-float v0, p1, v2

    .line 361
    .local v0, "dx":F
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragY:F

    sub-float v1, p2, v2

    .line 362
    .local v1, "dy":F
    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "updateDraggedView - dx: %f, dy: %f"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v3, v3, v6

    int-to-float v3, v3

    add-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 365
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedViewStartWindowPos:[I

    aget v3, v3, v7

    int-to-float v3, v3

    add-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 366
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    neg-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 367
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    neg-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 368
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedView:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    return-void
.end method

.method private updateEditOperation()V
    .locals 10

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isOperationRemove()Z

    move-result v6

    .line 404
    .local v6, "wasRemove":Z
    const/4 v1, 0x0

    .line 406
    .local v1, "isRemove":Z
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewHotspot:Landroid/view/View;

    invoke-static {v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->doViewsIntersect(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 407
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    invoke-static {v7}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->remove(Ljava/lang/Object;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    .line 408
    .local v2, "newOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    const/4 v1, 0x1

    .line 423
    :goto_0
    if-eq v1, v6, :cond_2

    const-wide/16 v8, 0x32

    :goto_1
    invoke-direct {p0, v2, v8, v9}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setPendingEditionOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;J)V

    .line 425
    return-void

    .line 411
    .end local v2    # "newOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    :cond_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v3, v7, v8

    .line 412
    .local v3, "screenX":I
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->draggedImageView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v4, v7, v8

    .line 413
    .local v4, "screenY":I
    const-class v7, Lcom/google/android/libraries/bind/data/DataView;

    .line 414
    invoke-static {p0, v3, v4, v7}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewAtScreenPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/DataView;

    .line 415
    .local v0, "dataView":Lcom/google/android/libraries/bind/data/DataView;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v7, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->findDataViewIndex(Lcom/google/android/libraries/bind/data/DataView;)I

    move-result v5

    .line 417
    .local v5, "viewIndex":I
    const/4 v7, -0x1

    if-ne v5, v7, :cond_1

    .line 418
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    invoke-static {v7}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->unchanged(Ljava/lang/Object;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    .restart local v2    # "newOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    goto :goto_0

    .line 420
    .end local v2    # "newOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    :cond_1
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    invoke-static {v7, v5}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->move(Ljava/lang/Object;I)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    .restart local v2    # "newOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    goto :goto_0

    .line 423
    .end local v0    # "dataView":Lcom/google/android/libraries/bind/data/DataView;
    .end local v3    # "screenX":I
    .end local v4    # "screenY":I
    .end local v5    # "viewIndex":I
    :cond_2
    const-wide/16 v8, 0x12c

    goto :goto_1
.end method

.method private updateRemoveView(Z)V
    .locals 1
    .param p1, "isRemove"    # Z

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setPressed(Z)V

    .line 521
    return-void
.end method


# virtual methods
.method public hideRemoveView()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xc8

    .line 510
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    .line 511
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    .line 512
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 513
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeInterpolator:Landroid/view/animation/Interpolator;

    .line 514
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 515
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 516
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->hideRemoveViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 517
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 484
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->NORMAL:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setMode(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;)V

    .line 485
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewAdded:Z

    if-eqz v0, :cond_0

    .line 486
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->removeWindowOverlayView(Landroid/view/View;)V

    .line 487
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewAdded:Z

    .line 489
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->onDetachedFromWindow()V

    .line 490
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 180
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$6;->$SwitchMap$com$google$apps$dots$android$newsstand$card$EditableCardListView$Mode:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 182
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 192
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 194
    :goto_1
    return v0

    .line 184
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragX:F

    .line 186
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragY:F

    .line 187
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragPointerId:I

    .line 188
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Grabbed start pos"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 194
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->handleEditOnTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 182
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 203
    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$6;->$SwitchMap$com$google$apps$dots$android$newsstand$card$EditableCardListView$Mode:[I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 221
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 205
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 207
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragX:F

    float-to-int v2, v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->startDragY:F

    float-to-int v3, v3

    const-class v4, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-static {p0, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewAtScreenPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .line 209
    .local v0, "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    if-eqz v0, :cond_1

    .line 210
    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->startEditingIfPossible()Z

    move-result v1

    .line 211
    .local v1, "startedEditing":Z
    if-eqz v1, :cond_0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 212
    check-cast v0, Landroid/view/View;

    .end local v0    # "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 219
    .end local v1    # "startedEditing":Z
    :cond_0
    :goto_0
    return v1

    .line 217
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 219
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->handleEditOnTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setMode(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;)V
    .locals 7
    .param p1, "mode"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 248
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    if-ne p1, v1, :cond_0

    .line 281
    :goto_0
    return-void

    .line 251
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "setMode: %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-virtual {v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    if-eq v1, p1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 253
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    .line 254
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$6;->$SwitchMap$com$google$apps$dots$android$newsstand$card$EditableCardListView$Mode:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 256
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 257
    const/4 v0, 0x0

    .line 258
    .local v0, "isRemove":Z
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardGroup;->getEditOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 259
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isOperationRemove()Z

    move-result v0

    .line 260
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/CardGroup;->allowEditOperation()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 261
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/CardGroup;->commitEditOperation()V

    .line 268
    :goto_2
    const-wide/16 v4, 0x0

    invoke-direct {p0, v6, v4, v5}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setPendingEditionOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;J)V

    .line 269
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 270
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->clearDraggedView(Z)V

    .line 271
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->hideRemoveView()V

    .line 272
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateRemoveView(Z)V

    .line 273
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->dragScrollSpeed:I

    goto :goto_0

    .end local v0    # "isRemove":Z
    :cond_1
    move v1, v3

    .line 252
    goto :goto_1

    .line 263
    .restart local v0    # "isRemove":Z
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/CardGroup;->onOperationDisallowed()V

    goto :goto_2

    .line 266
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v1, v6}, Lcom/google/android/libraries/bind/card/CardGroup;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    goto :goto_2

    .line 278
    .end local v0    # "isRemove":Z
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showRemoveView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 493
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->hideRemoveViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 494
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewAdded:Z

    if-nez v0, :cond_0

    .line 495
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getRemoveViewLayoutParams()Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->addWindowOverlayView(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 496
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 497
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 498
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeViewAdded:Z

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 501
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeView:Landroid/view/View;

    .line 502
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 503
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 504
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->removeInterpolator:Landroid/view/animation/Interpolator;

    .line 505
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    .line 506
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 507
    return-void
.end method

.method public startEditing(Landroid/view/View;Lcom/google/android/libraries/bind/card/CardGroup;ILjava/lang/Object;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cardGroup"    # Lcom/google/android/libraries/bind/card/CardGroup;
    .param p3, "position"    # I
    .param p4, "editedId"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->mode:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->NORMAL:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    if-eq v4, v5, :cond_0

    .line 153
    :goto_0
    return v2

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->showRemoveView()V

    .line 138
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->draggable:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "draggableView":Landroid/view/View;
    if-nez v0, :cond_1

    .line 140
    move-object v0, p1

    .line 142
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedViewPadding:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 143
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedViewPadding:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 144
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->addDraggedView(Landroid/view/View;)V

    .line 145
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    if-nez v4, :cond_2

    move v2, v3

    :cond_2
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 147
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 148
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedId:Ljava/lang/Object;

    .line 149
    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;->EDIT:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setMode(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$Mode;)V

    .line 150
    invoke-static {p4}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->unchanged(Ljava/lang/Object;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v1

    .line 151
    .local v1, "editOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 152
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    move v2, v3

    .line 153
    goto :goto_0
.end method
