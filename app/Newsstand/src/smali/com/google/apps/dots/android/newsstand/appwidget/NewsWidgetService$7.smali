.class Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$7;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "NewsWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->fetchDataListIfNecessary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$7;->this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 296
    iget-boolean v1, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService$7;->this$0:Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 298
    .local v0, "context":Landroid/content/Context;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateAllWidgets(Landroid/content/Context;Z)V

    .line 300
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    return-void
.end method
