.class final Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState$1;
.super Ljava/lang/Object;
.source "HeaderEditionFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    const-class v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 68
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-class v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 69
    .local v1, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    invoke-direct {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 74
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    move-result-object v0

    return-object v0
.end method
