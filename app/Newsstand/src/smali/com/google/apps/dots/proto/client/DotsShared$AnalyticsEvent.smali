.class public final Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnalyticsEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;,
        Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;


# instance fields
.field private action_:Ljava/lang/String;

.field public additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

.field public additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

.field public analyticsEventId:Ljava/lang/String;

.field private appFamilyId_:Ljava/lang/String;

.field private appFamilyName_:Ljava/lang/String;

.field private appId_:Ljava/lang/String;

.field private appName_:Ljava/lang/String;

.field private bitField0_:I

.field private category_:Ljava/lang/String;

.field private created_:J

.field public customPrefixStrings:[Ljava/lang/String;

.field private experiments_:Ljava/lang/String;

.field private isOffline_:Z

.field private isPaid_:Z

.field private label_:Ljava/lang/String;

.field private page_:I

.field private postId_:Ljava/lang/String;

.field private postTitle_:Ljava/lang/String;

.field private screen_:Ljava/lang/String;

.field private sectionId_:Ljava/lang/String;

.field private sectionName_:Ljava/lang/String;

.field private storeType_:I

.field private testId_:Ljava/lang/String;

.field private userSubscriptionType_:I

.field private value_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 742
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 962
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    .line 965
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    .line 987
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    .line 1009
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    .line 1031
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    .line 1053
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    .line 1072
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    .line 1091
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    .line 1113
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    .line 1135
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    .line 1157
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    .line 1179
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    .line 1201
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    .line 1223
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    .line 1245
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    .line 1267
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    .line 1286
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    .line 1305
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    .line 1324
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    .line 1343
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    .line 1362
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    .line 1384
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    .line 1406
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    .line 1409
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 1412
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 742
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 5

    .prologue
    .line 1449
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1453
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_0

    .line 1454
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    .line 1456
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 1457
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 1458
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 1459
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 1460
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1458
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1450
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 1451
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 1464
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 1465
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 1466
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 1467
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v3, v3, v2

    if-eqz v3, :cond_3

    .line 1468
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1466
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1472
    .end local v2    # "i":I
    :cond_4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1477
    if-ne p1, p0, :cond_1

    .line 1504
    :cond_0
    :goto_0
    return v1

    .line 1478
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1479
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 1480
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1481
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1482
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1483
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1484
    :goto_5
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1487
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1488
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1489
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1490
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1491
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1492
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1493
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1494
    :goto_d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1500
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1501
    :goto_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    .line 1502
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 1503
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 1504
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 1480
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    .line 1481
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    .line 1482
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    .line 1483
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    .line 1484
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    .line 1487
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    .line 1488
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    .line 1489
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    .line 1490
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    .line 1491
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    .line 1492
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    .line 1493
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    .line 1494
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    .line 1500
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    .line 1501
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f
.end method

.method public getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppFamilyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    return-object v0
.end method

.method public getExperiments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsOffline()Z
    .locals 1

    .prologue
    .line 1345
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 1269
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    return v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1225
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPostTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getScreen()Ljava/lang/String;
    .locals 1

    .prologue
    .line 967
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1642
    const/4 v2, 0x0

    .line 1643
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    .line 1644
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1645
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_0

    .line 1646
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    .line 1647
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1649
    :cond_0
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_1

    .line 1650
    const/4 v4, 0x3

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    .line 1651
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1653
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_2

    .line 1654
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    .line 1655
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1657
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_3

    .line 1658
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    .line 1659
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1661
    :cond_3
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_4

    .line 1662
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    .line 1663
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1665
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_5

    .line 1666
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    .line 1667
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1669
    :cond_5
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_6

    .line 1670
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    .line 1671
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1673
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_7

    .line 1674
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    .line 1675
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1677
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    if-eqz v4, :cond_8

    .line 1678
    const/16 v4, 0xa

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    .line 1679
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 1681
    :cond_8
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-eqz v4, :cond_a

    .line 1682
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_a

    aget-object v1, v5, v4

    .line 1683
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    if-eqz v1, :cond_9

    .line 1684
    const/16 v7, 0xb

    .line 1685
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 1682
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1689
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :cond_a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_b

    .line 1690
    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    .line 1691
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1693
    :cond_b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_c

    .line 1694
    const/16 v4, 0xd

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    .line 1695
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1697
    :cond_c
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_e

    .line 1698
    const/4 v0, 0x0

    .line 1699
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_d

    aget-object v1, v5, v4

    .line 1701
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 1699
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1703
    .end local v1    # "element":Ljava/lang/String;
    :cond_d
    add-int/2addr v2, v0

    .line 1704
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 1706
    .end local v0    # "dataSize":I
    :cond_e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_f

    .line 1707
    const/16 v4, 0x10

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    .line 1708
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1710
    :cond_f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_10

    .line 1711
    const/16 v4, 0x11

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    .line 1712
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1714
    :cond_10
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_11

    .line 1715
    const/16 v4, 0x12

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    .line 1716
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1718
    :cond_11
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_12

    .line 1719
    const/16 v4, 0x13

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    .line 1720
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1722
    :cond_12
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    if-eqz v4, :cond_13

    .line 1723
    const/16 v4, 0x15

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    .line 1724
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 1726
    :cond_13
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_14

    .line 1727
    const/16 v4, 0x17

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    .line 1728
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1730
    :cond_14
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    if-eqz v4, :cond_15

    .line 1731
    const/16 v4, 0x19

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    .line 1732
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1734
    :cond_15
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v5, 0x80000

    and-int/2addr v4, v5

    if-eqz v4, :cond_16

    .line 1735
    const/16 v4, 0x1b

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    .line 1736
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1738
    :cond_16
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_17

    .line 1739
    const/16 v4, 0x1d

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    .line 1740
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1742
    :cond_17
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    if-eqz v4, :cond_18

    .line 1743
    const/16 v4, 0x1e

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    .line 1744
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 1746
    :cond_18
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-eqz v4, :cond_1a

    .line 1747
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_1a

    aget-object v1, v4, v3

    .line 1748
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    if-eqz v1, :cond_19

    .line 1749
    const/16 v6, 0x1f

    .line 1750
    invoke-static {v6, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v2, v6

    .line 1747
    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1754
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    :cond_1a
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->cachedSize:I

    .line 1755
    return v2
.end method

.method public getStoreType()I
    .locals 1

    .prologue
    .line 1288
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    return v0
.end method

.method public getTestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1364
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserSubscriptionType()I
    .locals 1

    .prologue
    .line 1326
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 1055
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    return-wide v0
.end method

.method public hasAppFamilyId()Z
    .locals 1

    .prologue
    .line 1104
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAppId()Z
    .locals 1

    .prologue
    .line 1148
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExperiments()Z
    .locals 2

    .prologue
    .line 1397
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsOffline()Z
    .locals 2

    .prologue
    .line 1353
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPage()Z
    .locals 1

    .prologue
    .line 1277
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPostId()Z
    .locals 1

    .prologue
    .line 1236
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasScreen()Z
    .locals 1

    .prologue
    .line 978
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSectionId()Z
    .locals 1

    .prologue
    .line 1192
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStoreType()Z
    .locals 2

    .prologue
    .line 1296
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTestId()Z
    .locals 2

    .prologue
    .line 1375
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserSubscriptionType()Z
    .locals 2

    .prologue
    .line 1334
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 1063
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1508
    const/16 v1, 0x11

    .line 1509
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 1510
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 1511
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 1512
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 1513
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 1514
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 1515
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 1516
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 1517
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 1518
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    .line 1519
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 1520
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 1521
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 1522
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    .line 1523
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    .line 1524
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    if-nez v2, :cond_f

    move v2, v3

    :goto_c
    add-int v1, v6, v2

    .line 1525
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    add-int v1, v2, v6

    .line 1526
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    add-int v1, v2, v6

    .line 1527
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    if-eqz v2, :cond_10

    move v2, v4

    :goto_d
    add-int v1, v6, v2

    .line 1528
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    add-int v1, v2, v6

    .line 1529
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    if-eqz v6, :cond_11

    :goto_e
    add-int v1, v2, v4

    .line 1530
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_f
    add-int v1, v4, v2

    .line 1531
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    if-nez v2, :cond_13

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 1532
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    if-nez v2, :cond_14

    mul-int/lit8 v1, v1, 0x1f

    .line 1538
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-nez v2, :cond_16

    mul-int/lit8 v1, v1, 0x1f

    .line 1544
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-nez v2, :cond_18

    mul-int/lit8 v1, v1, 0x1f

    .line 1550
    :cond_2
    return v1

    .line 1510
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 1511
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 1512
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 1513
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 1514
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 1517
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 1518
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 1519
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 1520
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 1521
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 1522
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 1523
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 1524
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    :cond_10
    move v2, v5

    .line 1527
    goto/16 :goto_d

    :cond_11
    move v4, v5

    .line 1529
    goto/16 :goto_e

    .line 1530
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 1531
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 1534
    :cond_14
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1535
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_15

    move v2, v3

    :goto_12
    add-int v1, v4, v2

    .line 1534
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 1535
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_12

    .line 1540
    .end local v0    # "i":I
    :cond_16
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1541
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v2, v2, v0

    if-nez v2, :cond_17

    move v2, v3

    :goto_14
    add-int v1, v4, v2

    .line 1540
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1541
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->hashCode()I

    move-result v2

    goto :goto_14

    .line 1546
    .end local v0    # "i":I
    :cond_18
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1547
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v2, v2, v0

    if-nez v2, :cond_19

    move v2, v3

    :goto_16
    add-int v1, v4, v2

    .line 1546
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 1547
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->hashCode()I

    move-result v2

    goto :goto_16
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1764
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1768
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1769
    :sswitch_0
    return-object p0

    .line 1774
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    goto :goto_0

    .line 1778
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    .line 1779
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1783
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    .line 1784
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1788
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    .line 1789
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1793
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    .line 1794
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1798
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    .line 1799
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1803
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    .line 1804
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1808
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    .line 1809
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1813
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    .line 1814
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto :goto_0

    .line 1818
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    .line 1819
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1823
    :sswitch_b
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1824
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-nez v6, :cond_2

    move v1, v5

    .line 1825
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 1826
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-eqz v6, :cond_1

    .line 1827
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1829
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 1830
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 1831
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;-><init>()V

    aput-object v7, v6, v1

    .line 1832
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1833
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1830
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1824
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v1, v6

    goto :goto_1

    .line 1836
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;-><init>()V

    aput-object v7, v6, v1

    .line 1837
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1841
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    .line 1842
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1846
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    .line 1847
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1851
    :sswitch_e
    const/16 v6, 0x7a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1852
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v1, v6

    .line 1853
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1854
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1855
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    .line 1856
    :goto_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 1857
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 1858
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1856
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1861
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 1865
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    .line 1866
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1870
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    .line 1871
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1875
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    .line 1876
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1880
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    .line 1881
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v7, 0x100000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1885
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v6

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    .line 1886
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1890
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    .line 1891
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1895
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    .line 1896
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v7, 0x40000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1900
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    .line 1901
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1905
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    .line 1906
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1910
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1911
    .local v4, "temp":I
    if-eq v4, v8, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_5

    const/4 v6, 0x3

    if-eq v4, v6, :cond_5

    const/4 v6, 0x4

    if-ne v4, v6, :cond_6

    .line 1915
    :cond_5
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    .line 1916
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    goto/16 :goto_0

    .line 1918
    :cond_6
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    goto/16 :goto_0

    .line 1923
    .end local v4    # "temp":I
    :sswitch_19
    const/16 v6, 0xfa

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1924
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-nez v6, :cond_8

    move v1, v5

    .line 1925
    .restart local v1    # "i":I
    :goto_4
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 1926
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-eqz v6, :cond_7

    .line 1927
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1929
    :cond_7
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 1930
    :goto_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 1931
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;-><init>()V

    aput-object v7, v6, v1

    .line 1932
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1933
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1930
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1924
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v1, v6

    goto :goto_4

    .line 1936
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;-><init>()V

    aput-object v7, v6, v1

    .line 1937
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1764
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa8 -> :sswitch_13
        0xb8 -> :sswitch_14
        0xc8 -> :sswitch_15
        0xda -> :sswitch_16
        0xea -> :sswitch_17
        0xf0 -> :sswitch_18
        0xfa -> :sswitch_19
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 738
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    return-object v0
.end method

.method public setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1014
    if-nez p1, :cond_0

    .line 1015
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1017
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    .line 1018
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1019
    return-object p0
.end method

.method public setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1096
    if-nez p1, :cond_0

    .line 1097
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1099
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    .line 1100
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1101
    return-object p0
.end method

.method public setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1118
    if-nez p1, :cond_0

    .line 1119
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1121
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    .line 1122
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1123
    return-object p0
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1140
    if-nez p1, :cond_0

    .line 1141
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1143
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    .line 1144
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1145
    return-object p0
.end method

.method public setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1162
    if-nez p1, :cond_0

    .line 1163
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1165
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    .line 1166
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1167
    return-object p0
.end method

.method public setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 992
    if-nez p1, :cond_0

    .line 993
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 995
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    .line 996
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 997
    return-object p0
.end method

.method public setCreated(J)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1077
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    .line 1078
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1079
    return-object p0
.end method

.method public setExperiments(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1389
    if-nez p1, :cond_0

    .line 1390
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1392
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    .line 1393
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1394
    return-object p0
.end method

.method public setIsOffline(Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1348
    iput-boolean p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    .line 1349
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1350
    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1036
    if-nez p1, :cond_0

    .line 1037
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1039
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    .line 1040
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1041
    return-object p0
.end method

.method public setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1272
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    .line 1273
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1274
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1228
    if-nez p1, :cond_0

    .line 1229
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1231
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    .line 1232
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1233
    return-object p0
.end method

.method public setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1250
    if-nez p1, :cond_0

    .line 1251
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1253
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    .line 1254
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1255
    return-object p0
.end method

.method public setScreen(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 970
    if-nez p1, :cond_0

    .line 971
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 973
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    .line 974
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 975
    return-object p0
.end method

.method public setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1184
    if-nez p1, :cond_0

    .line 1185
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1187
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    .line 1188
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1189
    return-object p0
.end method

.method public setSectionName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1206
    if-nez p1, :cond_0

    .line 1207
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1209
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    .line 1210
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1211
    return-object p0
.end method

.method public setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1291
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    .line 1292
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1293
    return-object p0
.end method

.method public setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1329
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    .line 1330
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1331
    return-object p0
.end method

.method public setValue(J)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1058
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    .line 1059
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    .line 1060
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1555
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1556
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_0

    .line 1557
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->action_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1559
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_1

    .line 1560
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->created_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1562
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_2

    .line 1563
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1565
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_3

    .line 1566
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appName_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1568
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_4

    .line 1569
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1571
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_5

    .line 1572
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->sectionName_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1574
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_6

    .line 1575
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1577
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_7

    .line 1578
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->postTitle_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1580
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_8

    .line 1581
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->page_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 1583
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    if-eqz v2, :cond_a

    .line 1584
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_a

    aget-object v0, v3, v2

    .line 1585
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    if-eqz v0, :cond_9

    .line 1586
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1584
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1590
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_b

    .line 1591
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1593
    :cond_b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_c

    .line 1594
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->appFamilyName_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1596
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 1597
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->customPrefixStrings:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_d

    aget-object v0, v3, v2

    .line 1598
    .local v0, "element":Ljava/lang/String;
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1597
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1601
    .end local v0    # "element":Ljava/lang/String;
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_e

    .line 1602
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->category_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1604
    :cond_e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_f

    .line 1605
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->label_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1607
    :cond_f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_10

    .line 1608
    const/16 v2, 0x12

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->value_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1610
    :cond_10
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    if-eqz v2, :cond_11

    .line 1611
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->experiments_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1613
    :cond_11
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_12

    .line 1614
    const/16 v2, 0x15

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->storeType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 1616
    :cond_12
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_13

    .line 1617
    const/16 v2, 0x17

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isPaid_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1619
    :cond_13
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    if-eqz v2, :cond_14

    .line 1620
    const/16 v2, 0x19

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->isOffline_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1622
    :cond_14
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    if-eqz v2, :cond_15

    .line 1623
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->testId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1625
    :cond_15
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_16

    .line 1626
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->screen_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1628
    :cond_16
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_17

    .line 1629
    const/16 v2, 0x1e

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->userSubscriptionType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1631
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    if-eqz v2, :cond_19

    .line 1632
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_19

    aget-object v0, v2, v1

    .line 1633
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    if-eqz v0, :cond_18

    .line 1634
    const/16 v4, 0x1f

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1632
    :cond_18
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1638
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    :cond_19
    return-void
.end method
