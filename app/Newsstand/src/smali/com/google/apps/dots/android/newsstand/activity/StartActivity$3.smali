.class Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startOrResumeFlow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->loadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->access$200(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->retryButton:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->access$300(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 243
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->completeFlow(Z)V

    .line 237
    return-void
.end method
