.class public interface abstract Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;
.super Ljava/lang/Object;
.source "BridgeEventHandler.java"


# virtual methods
.method public abstract bind(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract gotoPage(I)V
.end method

.method public abstract onLayoutChange(IZII)V
.end method

.method public abstract onScriptLoad()V
.end method

.method public abstract openAudio(Ljava/lang/String;)V
.end method

.method public abstract openDrawer(Ljava/lang/String;IZ)V
.end method

.method public abstract openOriginalUrl(Ljava/lang/String;)V
.end method

.method public abstract openPlayStoreDoc(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract pauseAudio()V
.end method

.method public abstract requestPurchase(Ljava/lang/String;)V
.end method

.method public abstract requestPurchaseInContext(IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract switchToApp(Ljava/lang/String;)V
.end method

.method public abstract switchToArticle(Ljava/lang/String;)V
.end method

.method public abstract switchToSection(Ljava/lang/String;)V
.end method

.method public abstract switchToToc()V
.end method

.method public abstract toggleActionBar()V
.end method

.method public abstract toggleFullScreen()V
.end method

.method public abstract watchVideo(Ljava/lang/String;)V
.end method
