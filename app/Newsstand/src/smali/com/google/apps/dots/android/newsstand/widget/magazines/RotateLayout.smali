.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;
.super Landroid/view/ViewGroup;
.source "RotateLayout.java"


# instance fields
.field private final matrix:Landroid/graphics/Matrix;

.field private orientation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    .line 29
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 88
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 89
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getWidth()I

    move-result v1

    .line 90
    .local v1, "w":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getHeight()I

    move-result v0

    .line 91
    .local v0, "h":I
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    sparse-switch v2, :sswitch_data_0

    .line 105
    :goto_0
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v4, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 106
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 107
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 108
    return-void

    .line 93
    :sswitch_0
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 96
    :sswitch_1
    int-to-float v2, v0

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 99
    :sswitch_2
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 102
    :sswitch_3
    int-to-float v2, v1

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getWidth()I

    move-result v3

    .line 64
    .local v3, "w":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getHeight()I

    move-result v0

    .line 65
    .local v0, "h":I
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    sparse-switch v4, :sswitch_data_0

    .line 79
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 80
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/util/MotionEventUtil;->transform(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 81
    .local v2, "transformedEvent":Landroid/view/MotionEvent;
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 82
    .local v1, "handled":Z
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 83
    return v1

    .line 67
    .end local v1    # "handled":Z
    .end local v2    # "transformedEvent":Landroid/view/MotionEvent;
    :sswitch_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v6, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto :goto_0

    .line 70
    :sswitch_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    neg-int v5, v0

    int-to-float v5, v5

    invoke-virtual {v4, v6, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto :goto_0

    .line 73
    :sswitch_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    neg-int v5, v3

    int-to-float v5, v5

    neg-int v6, v0

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto :goto_0

    .line 76
    :sswitch_3
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->matrix:Landroid/graphics/Matrix;

    neg-int v5, v3

    int-to-float v5, v5

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto :goto_0

    .line 65
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    return v0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 3
    .param p1, "location"    # [I
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 132
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "change"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 50
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 51
    .local v0, "child":Landroid/view/View;
    sub-int v2, p4, p2

    .line 52
    .local v2, "width":I
    sub-int v1, p5, p3

    .line 53
    .local v1, "height":I
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_1

    .line 54
    invoke-virtual {v0, v4, v4, v2, v1}, Landroid/view/View;->layout(IIII)V

    .line 59
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "height":I
    .end local v2    # "width":I
    :cond_0
    :goto_0
    return-void

    .line 56
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "height":I
    .restart local v2    # "width":I
    :cond_1
    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 112
    const/4 v2, 0x0

    .local v2, "w":I
    const/4 v1, 0x0

    .line 113
    .local v1, "h":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 114
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "child":Landroid/view/View;
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_1

    .line 116
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->measureChild(Landroid/view/View;II)V

    .line 117
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 118
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 125
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    :goto_0
    invoke-virtual {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->setMeasuredDimension(II)V

    .line 126
    return-void

    .line 120
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    invoke-virtual {p0, v0, p2, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->measureChild(Landroid/view/View;II)V

    .line 121
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 122
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    goto :goto_0
.end method

.method public setOrientation(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 33
    rem-int/lit16 p1, p1, 0x168

    .line 34
    if-gez p1, :cond_0

    .line 35
    add-int/lit16 p1, p1, 0x168

    .line 37
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    if-eq v0, p1, :cond_1

    .line 38
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->orientation:I

    .line 39
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RotateLayout;->requestLayout()V

    .line 41
    :cond_1
    return-void
.end method
