.class public final Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "ReadStateList.java"


# static fields
.field public static final DK_IS_METERED_READ:I

.field public static final DK_POST_ID:I

.field public static final DK_POST_READ_STATE:I


# instance fields
.field private final edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadStateList_postId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_POST_ID:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadStateList_postReadState:I

    sput v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_POST_READ_STATE:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadStateList_isMeteredRead:I

    sput v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_IS_METERED_READ:I

    return-void
.end method

.method public static getReadState(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 42
    sget v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_POST_READ_STATE:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    return-object v0
.end method

.method public static getReadStatesMap(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;J)Ljava/util/Map;
    .locals 7
    .param p0, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .param p1, "meteredDuration"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            "J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v2

    .line 86
    .local v2, "serverNow":J
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 87
    .local v1, "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v6, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList$1;-><init>(Ljava/util/Map;JJ)V

    invoke-virtual {v6, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 116
    return-object v1
.end method

.method public static isMeteredRead(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 2
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 46
    sget v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->DK_IS_METERED_READ:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public static lookupMeteredDuration(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)J
    .locals 8
    .param p0, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 69
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v1

    .line 72
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    if-eqz v1, :cond_0

    .line 73
    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 74
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasMeteredPolicy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getMeteredPolicy()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->getPeriodDuration(Lcom/google/apps/dots/proto/client/DotsShared$Period;)J

    move-result-wide v2

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getMeteredPolicy()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->getStartTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 75
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 80
    .end local v0    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .end local v1    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 3
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-static {p2, v2}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->lookupMeteredDuration(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)J

    move-result-wide v0

    .line 65
    .local v0, "meteredDuration":J
    invoke-static {p3, v0, v1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->getReadStatesMap(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;J)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2
.end method
