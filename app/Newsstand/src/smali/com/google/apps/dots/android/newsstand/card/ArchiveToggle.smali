.class public Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;
.super Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;
.source "ArchiveToggle.java"


# static fields
.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_SHOW_DEFAULT:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArchiveToggle_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_ON_CLICK_LISTENER:I

    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArchiveToggle_showDefault:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_SHOW_DEFAULT:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->archive_toggle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->LAYOUT:I

    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_SHOW_DEFAULT:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public getAlternateAccessibleMessageId()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->archive_hide_button_description:I

    return v0
.end method

.method public getAlternateImageResourceId()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_archive_hide:I

    return v0
.end method

.method public getAlternateTextColor()I
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_text_normal:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getAlternateTextMessageId()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->magazines_toggle_button_hide_archive:I

    return v0
.end method

.method public getDefaultAccessibleMessageId()I
    .locals 1

    .prologue
    .line 50
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->archive_show_button_description:I

    return v0
.end method

.method public getDefaultImageResourceId()I
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_archive_show:I

    return v0
.end method

.method public getDefaultTextColor()I
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->action_message_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getDefaultTextMessageId()I
    .locals 1

    .prologue
    .line 45
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->magazines_toggle_button_show_archive:I

    return v0
.end method

.method public getImageViewId()I
    .locals 1

    .prologue
    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toggle_image_view:I

    return v0
.end method

.method public getNSTextViewId()I
    .locals 1

    .prologue
    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toggle_text_view:I

    return v0
.end method

.method public getShowDefaultDataKey()I
    .locals 1

    .prologue
    .line 85
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_SHOW_DEFAULT:I

    return v0
.end method
