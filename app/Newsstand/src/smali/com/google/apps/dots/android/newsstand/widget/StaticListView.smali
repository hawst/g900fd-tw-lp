.class public Lcom/google/apps/dots/android/newsstand/widget/StaticListView;
.super Landroid/widget/LinearLayout;
.source "StaticListView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ConditionalSizeViewGroup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;
    }
.end annotation


# instance fields
.field private adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

.field private final dataSetObserver:Landroid/database/DataSetObserver;

.field private forceWrapHeight:Z

.field private onSizeChangedListener:Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setOrientation(I)V

    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/StaticListView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->dataSetObserver:Landroid/database/DataSetObserver;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/StaticListView;)Lcom/google/android/libraries/bind/data/DataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/StaticListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/StaticListView;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->fillViewsFromAdapter()V

    return-void
.end method

.method private fillViewsFromAdapter()V
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->recycleAllViews()V

    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 106
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getView(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->addView(Landroid/view/View;)V

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->requestLayout()V

    .line 109
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->invalidate()V

    .line 110
    return-void
.end method

.method private getView(I)Landroid/view/View;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private recycleAllViews()V
    .locals 3

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 121
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 122
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->removeViewAt(I)V

    .line 123
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/view/ViewHeap;->recycle(Landroid/view/View;)V

    .line 120
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 125
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->recycleAllViews()V

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 96
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 70
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->forceWrapHeight:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getChildCount()I

    move-result v5

    if-lez v5, :cond_1

    .line 71
    const/4 v4, 0x0

    .line 72
    .local v4, "totalHeight":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getPaddingRight()I

    move-result v6

    sub-int v0, v5, v6

    .line 73
    .local v0, "desiredWidth":I
    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 74
    .local v2, "itemWidth":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 75
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 76
    .local v3, "listItemView":Landroid/view/View;
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Landroid/view/View;->measure(II)V

    .line 77
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v3    # "listItemView":Landroid/view/View;
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 80
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0, v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->setMeasuredDimension(II)V

    .line 82
    .end local v0    # "desiredWidth":I
    .end local v1    # "i":I
    .end local v2    # "itemWidth":I
    .end local v4    # "totalHeight":I
    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->onSizeChangedListener:Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->onSizeChangedListener:Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;->onSizeChanged()V

    .line 90
    :cond_0
    return-void
.end method

.method public setAdapter(Lcom/google/android/libraries/bind/data/DataAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/libraries/bind/data/DataAdapter;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 47
    if-nez p1, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->recycleAllViews()V

    .line 51
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 52
    if-eqz p1, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->adapter:Lcom/google/android/libraries/bind/data/DataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->fillViewsFromAdapter()V

    .line 56
    :cond_1
    return-void
.end method

.method public setForceWrapHeight(Z)V
    .locals 0
    .param p1, "forceWrapHeight"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->forceWrapHeight:Z

    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->requestLayout()V

    .line 65
    return-void
.end method

.method public setOnSizeChangedListener(Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;)V
    .locals 0
    .param p1, "onSizeChangedListener"    # Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/StaticListView;->onSizeChangedListener:Lcom/google/apps/dots/android/newsstand/widget/StaticListView$OnSizeChangedListener;

    .line 60
    return-void
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method
