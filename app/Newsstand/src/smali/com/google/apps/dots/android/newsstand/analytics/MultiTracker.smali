.class public Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;
.super Ljava/lang/Object;
.source "MultiTracker.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final internalTrackers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final publisherTrackers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "internalTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    .local p2, "publisherTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->internalTrackers:Ljava/util/List;

    .line 22
    invoke-static {p2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->publisherTrackers:Ljava/util/List;

    .line 23
    return-void
.end method

.method private sendEventToTrackersList(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    .locals 7
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "trackingId"    # Ljava/lang/String;
    .param p4, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;",
            ">;",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "trackers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;

    .line 42
    .local v0, "t":Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;
    if-nez p2, :cond_1

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;->supportsAccountlessEvents()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44
    :cond_1
    :try_start_0
    invoke-interface {v0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;->trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "thrown":Ljava/lang/Throwable;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "error tracking event %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p4, v5, v6

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    .end local v0    # "t":Lcom/google/apps/dots/android/newsstand/analytics/NSTracker;
    .end local v1    # "thrown":Ljava/lang/Throwable;
    :cond_2
    return-void
.end method


# virtual methods
.method public trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p4, "sendToPublishers"    # Z

    .prologue
    .line 27
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 28
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "trackEvent(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p3, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->internalTrackers:Ljava/util/List;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->sendEventToTrackersList(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V

    .line 31
    if-eqz p4, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->publisherTrackers:Ljava/util/List;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->sendEventToTrackersList(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)V

    .line 34
    :cond_0
    return-void
.end method
