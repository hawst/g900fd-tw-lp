.class Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;
.super Ljava/lang/Object;
.source "NSActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProgressDialogRunnable"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private loadingDialog:Landroid/app/ProgressDialog;

.field private final title:Ljava/lang/String;


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->loadingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->loadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 160
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->loadingDialog:Landroid/app/ProgressDialog;

    .line 162
    :cond_0
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->context:Landroid/content/Context;

    .line 163
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->context:Landroid/content/Context;

    const-string v1, ""

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->title:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->loadingDialog:Landroid/app/ProgressDialog;

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->context:Landroid/content/Context;

    .line 151
    return-void
.end method
