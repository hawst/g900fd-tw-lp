.class Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;
.super Ljava/lang/Object;
.source "ForegroundProvider.java"


# static fields
.field private static final IS_HC_OR_ABOVE:Z

.field private static final IS_JBMR1_OR_ABOVE:Z


# instance fields
.field private foregroundBoundsChanged:Z

.field private foregroundDrawable:Landroid/graphics/drawable/Drawable;

.field private foregroundPaddingBottom:I

.field private foregroundPaddingLeft:I

.field private foregroundPaddingRight:I

.field private foregroundPaddingTop:I

.field private final overlayBounds:Landroid/graphics/Rect;

.field private final selfBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->IS_HC_OR_ABOVE:Z

    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->IS_JBMR1_OR_ABOVE:Z

    return-void

    :cond_0
    move v0, v2

    .line 25
    goto :goto_0

    :cond_1
    move v1, v2

    .line 27
    goto :goto_1
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingLeft:I

    .line 32
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingTop:I

    .line 33
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingRight:I

    .line 34
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingBottom:I

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->selfBounds:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->overlayBounds:Landroid/graphics/Rect;

    .line 38
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundBoundsChanged:Z

    return-void
.end method


# virtual methods
.method public draw(Landroid/view/ViewGroup;Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 137
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 139
    .local v6, "foreground":Landroid/graphics/drawable/Drawable;
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundBoundsChanged:Z

    if-eqz v0, :cond_0

    .line 140
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundBoundsChanged:Z

    .line 141
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->selfBounds:Landroid/graphics/Rect;

    .line 142
    .local v3, "selfBounds":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->overlayBounds:Landroid/graphics/Rect;

    .line 144
    .local v4, "overlayBounds":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    .line 145
    .local v8, "w":I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    .line 147
    .local v7, "h":I
    invoke-virtual {v3, v1, v1, v8, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 157
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->IS_JBMR1_OR_ABOVE:Z

    if-eqz v0, :cond_2

    .line 158
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutDirection()I

    move-result v5

    .line 159
    .local v5, "layoutDirection":I
    const/16 v0, 0x77

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 160
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 159
    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/GravityCompat;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 165
    .end local v5    # "layoutDirection":I
    :goto_0
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 168
    .end local v3    # "selfBounds":Landroid/graphics/Rect;
    .end local v4    # "overlayBounds":Landroid/graphics/Rect;
    .end local v7    # "h":I
    .end local v8    # "w":I
    :cond_0
    invoke-virtual {v6, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 170
    .end local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void

    .line 163
    .restart local v3    # "selfBounds":Landroid/graphics/Rect;
    .restart local v4    # "overlayBounds":Landroid/graphics/Rect;
    .restart local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "h":I
    .restart local v8    # "w":I
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->overlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 118
    :cond_0
    return-void
.end method

.method protected drawableStateChanged(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 112
    :cond_0
    return-void
.end method

.method public initialize(Landroid/view/ViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 41
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010109

    aput v3, v2, v4

    invoke-virtual {p2, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 43
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 44
    .local v1, "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->setForeground(Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)V

    .line 47
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 103
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 106
    :cond_0
    return-void
.end method

.method protected onLayout()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundBoundsChanged:Z

    .line 129
    return-void
.end method

.method protected onSizeChanged()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundBoundsChanged:Z

    .line 133
    return-void
.end method

.method public setForeground(Landroid/view/ViewGroup;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, 0x0

    .line 51
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v1, p2, :cond_3

    .line 52
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 54
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 57
    :cond_0
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 58
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingLeft:I

    .line 59
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingTop:I

    .line 60
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingRight:I

    .line 61
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingBottom:I

    .line 63
    if-eqz p2, :cond_4

    .line 64
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    .line 65
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 66
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 69
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 70
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 71
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingLeft:I

    .line 72
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingTop:I

    .line 73
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingRight:I

    .line 74
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundPaddingBottom:I

    .line 79
    .end local v0    # "padding":Landroid/graphics/Rect;
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 80
    invoke-virtual {p1}, Landroid/view/ViewGroup;->invalidate()V

    .line 82
    :cond_3
    return-void

    .line 77
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 121
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 122
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 123
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 125
    .end local v0    # "isVisible":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 122
    goto :goto_0
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/ForegroundProvider;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
