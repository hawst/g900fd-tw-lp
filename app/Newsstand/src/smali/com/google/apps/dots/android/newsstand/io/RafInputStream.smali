.class public Lcom/google/apps/dots/android/newsstand/io/RafInputStream;
.super Ljava/io/InputStream;
.source "RafInputStream.java"


# instance fields
.field private final limit:J

.field private markPosition:J

.field private position:J

.field private final raf:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;JJ)V
    .locals 8
    .param p1, "raf"    # Ljava/io/RandomAccessFile;
    .param p2, "offset"    # J
    .param p4, "size"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    .line 22
    add-long v0, p2, p4

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    .line 23
    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    .line 24
    invoke-virtual {p1, p2, p3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 25
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    .line 26
    .local v6, "fileLength":J
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 27
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    move-wide v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;-><init>(JJJ)V

    throw v1

    .line 29
    :cond_0
    return-void
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    return-void
.end method

.method public mark(I)V
    .locals 2
    .param p1, "readlimit"    # I

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->markPosition:J

    .line 44
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 54
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 65
    :goto_0
    return v0

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->read()I

    move-result v0

    .line 60
    .local v0, "read":I
    if-ne v0, v1, :cond_1

    .line 61
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;

    const-string v2, "Attempted to read byte %d off the end of file with length %d."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    .line 62
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 61
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    :cond_1
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 10
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 75
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 91
    :goto_0
    return v0

    .line 79
    :cond_0
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    int-to-long v4, p3

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 80
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    sub-long/2addr v2, v4

    long-to-int p3, v2

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    .line 85
    .local v0, "read":I
    if-ne v0, v1, :cond_2

    .line 86
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;

    const-string v2, "Attempted to read a region [%d, %d) off the end of file with length %d."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    .line 87
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    int-to-long v8, p3

    add-long/2addr v6, v8

    .line 88
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 86
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_2
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    goto :goto_0
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->markPosition:J

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->markPosition:J

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 50
    return-void
.end method

.method public skip(J)J
    .locals 7
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    add-long/2addr v2, p1

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 105
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->limit:J

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    sub-long p1, v2, v4

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->raf:Ljava/io/RandomAccessFile;

    long-to-int v3, p1

    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    move-result v2

    int-to-long v0, v2

    .line 108
    .local v0, "skipped":J
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;->position:J

    .line 109
    return-wide v0
.end method
