.class Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "ArticleTail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->updateCardsList(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

.field final synthetic val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final synthetic val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$useDarkTheme:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->val$useDarkTheme:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 143
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 132
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail$3;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
