.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 779
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 782
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v2, "%s: syncing deps"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v5

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 783
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 784
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 785
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 786
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 784
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageOrAttachmentTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 790
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getPreviewAttachmentId()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    sget-object v11, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v7, v1

    move v10, v5

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v6 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 792
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$10;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
