.class Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache$1;
.super Landroid/support/v4/util/LruCache;
.source "StoreCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
        "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
        "<*>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .param p2, "x0"    # I

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected sizeOf(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)I
    .locals 1
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 22
    .local p2, "value":Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;, "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem<*>;"
    iget v0, p2, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->sizeKb:I

    return v0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache$1;->sizeOf(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;)I

    move-result v0

    return v0
.end method
