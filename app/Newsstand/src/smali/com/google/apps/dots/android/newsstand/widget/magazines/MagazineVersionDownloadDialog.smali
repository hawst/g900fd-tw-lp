.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "MagazineVersionDownloadDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field protected static final EXTRA_EDITION:Ljava/lang/String; = "MagazineVersionDownloadDialog_edition"

.field protected static final EXTRA_IS_ARCHIVED:Ljava/lang/String; = "MagazineVersionDownloadDialog_isArchived"

.field protected static final EXTRA_JUST_PIN_DONT_SYNC:Ljava/lang/String; = "MagazineVersionDownloadDialog_justPinDontSync"

.field private static final EXTRA_PINNED_VERSION:Ljava/lang/String; = "MagazineVersionDownloadDialog_pinnedVersion"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private isArchived:Z

.field private justPinDontSync:Z

.field private pinnedVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 47
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    return p1
.end method

.method public static show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 1
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "isArchived"    # Z

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZZ)V

    .line 75
    return-void
.end method

.method public static show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZZ)V
    .locals 3
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "isArchived"    # Z
    .param p3, "justPinDontSync"    # Z

    .prologue
    .line 83
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;-><init>()V

    .line 87
    .local v1, "dialog":Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "MagazineVersionDownloadDialog_edition"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 89
    const-string v2, "MagazineVersionDownloadDialog_isArchived"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    if-eqz p3, :cond_0

    .line 91
    const-string v2, "MagazineVersionDownloadDialog_justPinDontSync"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->setArguments(Landroid/os/Bundle;)V

    .line 95
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->TAG:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 96
    return-void
.end method


# virtual methods
.method protected handleExtras(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 50
    if-nez p1, :cond_0

    .line 62
    :goto_0
    return-void

    .line 53
    :cond_0
    const-string v0, "MagazineVersionDownloadDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "MagazineVersionDownloadDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 56
    :cond_1
    const-string v0, "MagazineVersionDownloadDialog_isArchived"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->isArchived:Z

    .line 58
    const-string v0, "MagazineVersionDownloadDialog_justPinDontSync"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->justPinDontSync:Z

    .line 59
    const-string v2, "MagazineVersionDownloadDialog_pinnedVersion"

    .line 60
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;->getDefaultToLiteMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 59
    :goto_1
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 60
    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->justPinDontSync:Z

    if-eqz v0, :cond_1

    .line 133
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->pin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 134
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfDownloadLater()V

    .line 143
    :goto_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->isArchived:Z

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->removeEditionFromArchive(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 147
    :cond_0
    return-void

    .line 137
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 138
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 139
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    .line 140
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 100
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->handleExtras(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 103
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v4, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_version_download_body:I

    const/4 v5, 0x0

    .line 104
    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 106
    .local v1, "linear":Landroid/widget/LinearLayout;
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->version_spinner:I

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    .line 107
    .local v2, "spinner":Landroid/widget/Spinner;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    if-ne v4, v3, :cond_0

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 108
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 123
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->magazine_version_title:I

    .line 124
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 125
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->magazine_version_download_button:I

    .line 126
    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 127
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 107
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    const-string v0, "MagazineVersionDownloadDialog_edition"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 67
    const-string v0, "MagazineVersionDownloadDialog_isArchived"

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->isArchived:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 68
    const-string v0, "MagazineVersionDownloadDialog_justPinDontSync"

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->justPinDontSync:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    const-string v0, "MagazineVersionDownloadDialog_pinnedVersion"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineVersionDownloadDialog;->pinnedVersion:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 71
    return-void
.end method
