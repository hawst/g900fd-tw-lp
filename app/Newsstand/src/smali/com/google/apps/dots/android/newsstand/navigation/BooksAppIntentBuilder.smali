.class public Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "BooksAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final URI_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private addToMyEbooks:Z

.field private promptBeforeAdding:Z

.field private referrer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 25
    const-string v0, "^https?://(play|books).google.com/e?books/reader(\\?|/).*"

    .line 26
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->URI_PATTERN:Ljava/util/regex/Pattern;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 28
    const-string v0, "newsstand"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->referrer:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->addToMyEbooks:Z

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->promptBeforeAdding:Z

    .line 34
    return-void
.end method

.method public static isBooksAppUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 37
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->URI_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 52
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.apps.books"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 53
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v1, "authAccount"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 55
    return-object v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 63
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->backendDocId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 64
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->activity:Landroid/app/Activity;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->books_app_uri:I

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->backendDocId:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 68
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "building view item intent with uri %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->di(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 71
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.google.android.apps.books"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v2, "authAccount"

    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 75
    const-string v2, "books:referrer"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->referrer:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v2, "books:addToMyEBooks"

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->addToMyEbooks:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    const-string v2, "books:promptBeforeAdding"

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->promptBeforeAdding:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    return-object v0

    .line 66
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->webUri:Landroid/net/Uri;

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0
.end method

.method public setAddToMyEbooks(Z)Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->addToMyEbooks:Z

    .line 42
    return-object p0
.end method

.method public setPromptBeforeAdding(Z)Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;->promptBeforeAdding:Z

    .line 47
    return-object p0
.end method
