.class public Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;
.super Ljava/lang/Object;
.source "WebViewBridge.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private bridgeEventHandler:Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "bridgeEventHandler"    # Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->bridgeEventHandler:Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;

    .line 23
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->bridgeEventHandler:Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;

    return-object v0
.end method

.method private safePost(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->bridgeEventHandler:Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 310
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 251
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$20;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$20;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 257
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->bridgeEventHandler:Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;

    .line 28
    return-void
.end method

.method public evaluatedJavascript(Ljava/lang/String;J)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;
    .param p2, "requestCode"    # J
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 317
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$21;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$21;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;JLjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 326
    return-void
.end method

.method public getPostsForSectionAsync(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;
    .param p2, "max"    # I
    .param p3, "requireImages"    # Z
    .param p4, "callbackKey"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 290
    return-void
.end method

.method public getSectionsAsync(Ljava/lang/String;)V
    .locals 0
    .param p1, "callbackKey"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 283
    return-void
.end method

.method public gotoPage(I)V
    .locals 1
    .param p1, "pageNumber"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$3;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$3;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;I)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 59
    return-void
.end method

.method public isFullScreen()Z
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method public notifyIframeUnload()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 303
    return-void
.end method

.method public onLayoutChange(IZII)V
    .locals 6
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$2;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$2;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;IZII)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method

.method public onScriptLoad()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$1;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 38
    return-void
.end method

.method public openAudio(Ljava/lang/String;)V
    .locals 1
    .param p1, "audioUri"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$12;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$12;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 150
    return-void
.end method

.method public openDrawer(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "restrictToSingleField"    # Z
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$10;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$10;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;IZ)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 130
    return-void
.end method

.method public openOriginalUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 113
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$9;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$9;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.method public openPlayStoreDoc(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "docTypeValue"    # I
    .param p2, "backendDocId"    # Ljava/lang/String;
    .param p3, "parentBackendDocId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 207
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$17;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$17;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 213
    return-void
.end method

.method public pauseAudio()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 154
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$13;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$13;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 160
    return-void
.end method

.method public requestPurchase(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 174
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$15;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$15;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 180
    return-void
.end method

.method public requestPurchaseInContext(IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "backendId"    # I
    .param p2, "docTypeValue"    # I
    .param p3, "backendDocId"    # Ljava/lang/String;
    .param p4, "fullDocId"    # Ljava/lang/String;
    .param p5, "offerType"    # I
    .param p6, "parentBackendDocId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 190
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$16;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$16;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 202
    return-void
.end method

.method public sendAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 218
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$18;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$18;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 224
    return-void
.end method

.method public sendCustomAnalyticsEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "pageIndex"    # I
    .param p6, "dimensionsMetricsJson"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 233
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$19;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$19;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 244
    return-void
.end method

.method public showCreatePost(Ljava/lang/String;)V
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 271
    return-void
.end method

.method public showEditPost(Ljava/lang/String;)V
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 277
    return-void
.end method

.method public showGotoMenu()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 265
    return-void
.end method

.method public switchToApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$6;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$6;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 89
    return-void
.end method

.method public switchToArticle(Ljava/lang/String;)V
    .locals 1
    .param p1, "postId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$5;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$5;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 79
    return-void
.end method

.method public switchToSection(Ljava/lang/String;)V
    .locals 1
    .param p1, "sectionId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$7;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$7;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 99
    return-void
.end method

.method public switchToToc()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$8;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$8;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 109
    return-void
.end method

.method public toggleActionBar()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$4;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 69
    return-void
.end method

.method public toggleFullScreen()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 164
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$14;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$14;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 170
    return-void
.end method

.method public watchVideo(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$11;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge$11;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->safePost(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method
