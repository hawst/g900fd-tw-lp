.class public Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.source "BrowserActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 26
    return-void
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 33
    .local v2, "i":Landroid/content/Intent;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "onCreate(%s)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "url":Ljava/lang/String;
    const-string v4, "content"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "content":Ljava/lang/String;
    const-string v4, "contentType"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "contentType":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    .line 39
    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 40
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->loadUrl(Ljava/lang/String;)V

    .line 45
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->setContentView(Landroid/view/View;)V

    .line 46
    return-void

    .line 42
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v4, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 68
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->finish()V

    .line 70
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->onPause()V

    .line 51
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onPause()V

    .line 52
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onResume()V

    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;->webView:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->onResume()V

    .line 58
    return-void
.end method
