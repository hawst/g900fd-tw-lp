.class public Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;
.super Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;
.source "OnDeviceAppFilter.java"


# instance fields
.field protected final pinnedEditionCache:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    .line 20
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;->pinnedEditionCache:Ljava/util/Set;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 1
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    .line 20
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;->pinnedEditionCache:Ljava/util/Set;

    .line 28
    return-void
.end method


# virtual methods
.method protected includeItem(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 46
    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 47
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;->pinnedEditionCache:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public onPreFilter()V
    .locals 5

    .prologue
    .line 35
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceFilter;->onPreFilter()V

    .line 36
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v2

    .line 37
    .local v2, "pinnedList":Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;->pinnedEditionCache:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 38
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 39
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 40
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;->pinnedEditionCache:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    return-void
.end method
