.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;
.source "NativeBodyDotsWidget.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataView;
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
.implements Lcom/google/apps/dots/android/newsstand/widget/DotsWidget;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

.field private final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private backgroundColor:Ljava/lang/Integer;

.field private builtViewHierarchy:Z

.field private dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

.field private eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

.field private final handler:Landroid/os/Handler;

.field private final inLiteMode:Z

.field private isLaidOut:Z

.field private loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

.field private nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

.field private pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

.field private pendingEdgeDirection:Ljava/lang/Integer;

.field private pendingPageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

.field private pendingPageLocationFromSavedPageLocation:Z

.field private post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field private postIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;"
        }
    .end annotation
.end field

.field private scrolledToEdge:Z

.field private scrolledToPageLocation:Z

.field private section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

.field private statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V
    .locals 2
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "inLiteMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->handler:Landroid/os/Handler;

    .line 75
    new-instance v0, Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 82
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 85
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    .line 89
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z

    .line 90
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToEdge:Z

    .line 104
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->inLiteMode:Z

    .line 105
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->colorHelper()Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getSectionBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->backgroundColor:Ljava/lang/Integer;

    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->backgroundColor:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 107
    const/high16 v0, -0x1000000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->backgroundColor:Ljava/lang/Integer;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->backgroundColor:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->setBackgroundColor(I)V

    .line 110
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/android/libraries/bind/data/DataViewHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->buildViewHierarchy(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .param p2, "x2"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    return-object v0
.end method

.method private buildViewHierarchy(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;)V
    .locals 6
    .param p1, "nativeBodies"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->readyToBuildViewHierarchy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    :goto_0
    return-void

    .line 258
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->postIndex:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 259
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 260
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildHierarchy()V

    .line 262
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 265
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getPagingScroller()Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    .line 266
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v1, :cond_1

    .line 277
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->builtViewHierarchy:Z

    .line 280
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->onLoadComplete()V

    goto :goto_0
.end method

.method private static keepOneChild(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;I)V
    .locals 3
    .param p0, "parent"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p1, "keptChildIndex"    # I

    .prologue
    .line 245
    const/4 v1, 0x1

    new-array v0, v1, [Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 246
    .local v0, "justOneChild":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v2, v2, p1

    aput-object v2, v0, v1

    .line 247
    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 248
    return-void
.end method

.method private maybeRemoveRedundantPrintPage()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 192
    const/4 v5, 0x0

    .line 193
    .local v5, "rightHasCorrespondingText":Z
    const/4 v3, 0x0

    .line 196
    .local v3, "leftHasCorrespondingText":Z
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    const-string v10, "altFormat"

    invoke-static {v7, v10}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v1

    .line 197
    .local v1, "altFormatItem":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-eqz v1, :cond_2

    .line 198
    iget-object v10, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v11, v10

    move v7, v8

    :goto_0
    if-ge v7, v11, :cond_2

    aget-object v6, v10, v7

    .line 199
    .local v6, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAltFormat()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    move-result-object v0

    .line 205
    .local v0, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v12

    if-ne v12, v9, :cond_0

    .line 206
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v12

    if-nez v12, :cond_1

    .line 207
    const/4 v3, 0x1

    .line 198
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v12

    if-ne v12, v9, :cond_0

    .line 209
    const/4 v5, 0x1

    goto :goto_1

    .line 215
    .end local v0    # "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .end local v6    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_2
    if-ne v3, v5, :cond_4

    .line 242
    :cond_3
    :goto_2
    return-void

    .line 225
    :cond_4
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v7

    iput-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 228
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 229
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->getPortraitNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->getRootPart()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v7

    iget-object v4, v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 230
    .local v4, "portraitRootChildren":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    array-length v7, v4

    if-ne v7, v9, :cond_5

    aget-object v7, v4, v8

    .line 231
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v7

    if-ne v7, v13, :cond_5

    aget-object v7, v4, v8

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v7, v7

    if-ne v7, v13, :cond_5

    .line 233
    aget-object v10, v4, v8

    if-eqz v5, :cond_6

    move v7, v8

    :goto_3
    invoke-static {v10, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->keepOneChild(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;I)V

    .line 235
    :cond_5
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 236
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->getLandscapeNativeBody()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBody;->getRootPart()Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    move-result-object v7

    iget-object v2, v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    .line 237
    .local v2, "landscapeRootChildren":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    array-length v7, v2

    if-ne v7, v9, :cond_3

    aget-object v7, v2, v8

    .line 238
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getType()I

    move-result v7

    if-ne v7, v13, :cond_3

    aget-object v7, v2, v8

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v7, v7

    if-ne v7, v13, :cond_3

    .line 240
    aget-object v7, v2, v8

    if-eqz v5, :cond_7

    :goto_4
    invoke-static {v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->keepOneChild(Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;I)V

    goto :goto_2

    .end local v2    # "landscapeRootChildren":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_6
    move v7, v9

    .line 233
    goto :goto_3

    .restart local v2    # "landscapeRootChildren":[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    :cond_7
    move v8, v9

    .line 240
    goto :goto_4
.end method

.method private readyToBuildViewHierarchy()Z
    .locals 1

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->builtViewHierarchy:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->isLaidOut:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    .line 286
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestBuildViewHierarchyIfReady()V
    .locals 2

    .prologue
    .line 293
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->readyToBuildViewHierarchy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 305
    :cond_0
    return-void
.end method

.method private scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V
    .locals 3
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .param p2, "fromSavedPageLocation"    # Z

    .prologue
    const/4 v2, 0x1

    .line 412
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, v1, :cond_2

    .line 416
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingPageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .line 417
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingPageLocationFromSavedPageLocation:Z

    goto :goto_0

    .line 419
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_3

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToEdge:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    const/4 v1, -0x1

    .line 427
    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->canScrollHorizontally(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    .line 428
    invoke-interface {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->canScrollHorizontally(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 429
    :cond_3
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z

    .line 430
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    goto :goto_0
.end method

.method private updateEditionActivity()V
    .locals 0

    .prologue
    .line 453
    return-void
.end method


# virtual methods
.method public canScrollHorizontally(I)Z
    .locals 1
    .param p1, "direction"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearDataOnDetach(Z)V
    .locals 1
    .param p1, "clearDataOnDetach"    # Z

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach(Z)Z

    .line 583
    return-void
.end method

.method public getBackgroundColor()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->backgroundColor:Ljava/lang/Integer;

    return-object v0
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getCurrentPage()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 576
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getPageCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getPageFraction()F
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getCurrentPageFraction()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadDelayedContents(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "optLoadedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->isNotLoadedOrFailed()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 310
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadedRunnable(Ljava/lang/Runnable;)V

    .line 311
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->onLoadStart(Ljava/lang/String;)V

    .line 315
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadIfPossible()V

    .line 316
    return-void
.end method

.method public loadIfPossible()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 122
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 123
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    sget v3, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 124
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/DataViewHelper;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 125
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 130
    .local v1, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    .line 132
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    .line 133
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 134
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v2, v3

    .line 131
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    .line 130
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method protected notifyArticlePageChanged(Z)V
    .locals 4
    .param p1, "userDriven"    # Z

    .prologue
    .line 472
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v3, :cond_0

    .line 473
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getCurrentPageFraction()F

    move-result v1

    .line 474
    .local v1, "pageFraction":F
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getPageCount()I

    move-result v0

    .line 475
    .local v0, "pageCount":I
    if-lez v0, :cond_1

    add-int/lit8 v3, v0, -0x1

    int-to-float v3, v3

    mul-float/2addr v3, v1

    :goto_0
    float-to-int v2, v3

    .line 476
    .local v2, "pageNumber":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v3, p0, v2, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticlePageChanged(Landroid/view/View;IIZ)V

    .line 478
    .end local v0    # "pageCount":I
    .end local v1    # "pageFraction":F
    .end local v2    # "pageNumber":I
    :cond_0
    return-void

    .line 475
    .restart local v0    # "pageCount":I
    .restart local v1    # "pageFraction":F
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected notifyArticleScrolled(Z)V
    .locals 6
    .param p1, "userDriven"    # Z

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    .line 467
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getScrollOffset()I

    move-result v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->getScrollRange()I

    move-result v3

    const/4 v4, 0x1

    move-object v1, p0

    move v5, p1

    .line 466
    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleScrolled(Landroid/view/View;IIIZ)V

    .line 469
    :cond_0
    return-void
.end method

.method protected notifyArticleUnhandledClick()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleUnhandledClick(Landroid/view/View;)V

    .line 484
    :cond_0
    return-void
.end method

.method notifyFinishLoading()V
    .locals 3

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_1

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 323
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingEdgeDirection:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingEdgeDirection:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToEdge(I)V

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingPageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingPageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingPageLocationFromSavedPageLocation:Z

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_4

    .line 330
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->onLoadComplete()V

    .line 334
    :cond_4
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 336
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getPageFractionFutureForPost(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)V

    .line 335
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public onArticleAvailable(Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/util/Map;Lcom/google/apps/dots/proto/client/DotsShared$Form;)V
    .locals 5
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            ")V"
        }
    .end annotation

    .prologue
    .line 166
    .local p3, "postIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v0, :cond_0

    .line 183
    :goto_0
    return-void

    .line 170
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 171
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 172
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->postIndex:Ljava/util/Map;

    .line 173
    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->magazine_article_accessibility_warning:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 175
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 174
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 176
    const-string v0, "nativeBody"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getNativeBodies(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->nativeBodies:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 177
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->inLiteMode:Z

    if-eqz v0, :cond_1

    .line 178
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->maybeRemoveRedundantPrintPage()V

    .line 181
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->updateEditionActivity()V

    .line 182
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->requestBuildViewHierarchyIfReady()V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 379
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onAttachedToWindow()V

    .line 380
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onAttachedToWindow()V

    .line 381
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, v1, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadIfPossible()V

    .line 571
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 368
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onDetachedFromWindow()V

    .line 369
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->stop()V

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->eventDispatcher:Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 374
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onDetachedFromWindow()V

    .line 375
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 391
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onFinishTemporaryDetach()V

    .line 392
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onFinishTemporaryDetach()V

    .line 393
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 457
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onLayout(ZIIII)V

    .line 458
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->isLaidOut:Z

    .line 459
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->requestBuildViewHierarchyIfReady()V

    .line 460
    return-void
.end method

.method public onLoadComplete()V
    .locals 2

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 549
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onMeasure(II)V

    .line 398
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onMeasure(II)V

    .line 399
    return-void
.end method

.method public onPageChanged()V
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->notifyArticlePageChanged(Z)V

    .line 521
    return-void
.end method

.method public onScrolled()V
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->notifyArticleScrolled(Z)V

    .line 516
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 385
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RootPartView;->onStartTemporaryDetach()V

    .line 386
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onStartTemporaryDetach()V

    .line 387
    return-void
.end method

.method public onUnhandledClick()V
    .locals 0

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->notifyArticleUnhandledClick()V

    .line 507
    return-void
.end method

.method public onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
    .locals 1
    .param p1, "flingDirection"    # Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    if-eqz v0, :cond_0

    .line 526
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->LEFT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    if-ne p1, v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->navigateForward()V

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 528
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->RIGHT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    if-ne p1, v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;->navigateBackward()V

    goto :goto_0
.end method

.method public onZoomAttempt()V
    .locals 0

    .prologue
    .line 537
    return-void
.end method

.method public scrollToEdge(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToEdge:Z

    .line 438
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, v1, :cond_1

    .line 439
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pendingEdgeDirection:Ljava/lang/Integer;

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;->scrollToEdge(I)V

    goto :goto_0
.end method

.method public scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 1
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 409
    return-void
.end method

.method public setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V
    .locals 0
    .param p1, "articleEventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 114
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->dataViewHelper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 554
    return-void
.end method

.method public setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
    .locals 1
    .param p1, "eventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadStateEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 364
    return-void
.end method

.method public setStatusListener(Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->statusListener:Lcom/google/apps/dots/android/newsstand/widget/DotsWidgetStatusListener;

    .line 404
    return-void
.end method
