.class public Lcom/google/apps/dots/android/newsstand/async/TaskQueue;
.super Ljava/lang/Object;
.source "TaskQueue.java"


# instance fields
.field public final maxConcurrentTasks:I

.field private final semaphore:Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

.field private final tasks:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxConcurrentTasks"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->tasks:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 20
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->maxConcurrentTasks:I

    .line 21
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->semaphore:Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/async/TaskQueue;)Ljava/util/concurrent/PriorityBlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->tasks:Ljava/util/concurrent/PriorityBlockingQueue;

    return-object v0
.end method


# virtual methods
.method public addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->tasks:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 31
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/async/Task;->next()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 33
    .local v0, "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->semaphore:Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->sameThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/TaskQueue;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->with(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 41
    return-object v0
.end method
