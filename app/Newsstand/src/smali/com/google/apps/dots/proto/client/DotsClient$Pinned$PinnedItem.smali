.class public final Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PinnedItem"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;


# instance fields
.field private bitField0_:I

.field private edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

.field private lastSyncProgressAtFailure_:F

.field private lastSyncStarted_:J

.field private lastSynced_:J

.field private pinId_:I

.field private pinnedVersion_:I

.field private snapshotId_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 800
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 829
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    .line 848
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    .line 867
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    .line 886
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    .line 905
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    .line 924
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    .line 800
    return-void
.end method


# virtual methods
.method public clearLastSyncProgressAtFailure()Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1

    .prologue
    .line 918
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    .line 919
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 920
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 3

    .prologue
    .line 959
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 963
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v2, :cond_0

    .line 964
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 966
    :cond_0
    return-object v0

    .line 960
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    :catch_0
    move-exception v1

    .line 961
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 971
    if-ne p1, p0, :cond_1

    .line 974
    :cond_0
    :goto_0
    return v1

    .line 972
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 973
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 974
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_3

    :goto_1
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-object v0
.end method

.method public getLastSyncProgressAtFailure()F
    .locals 1

    .prologue
    .line 907
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    return v0
.end method

.method public getLastSyncStarted()J
    .locals 2

    .prologue
    .line 888
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    return-wide v0
.end method

.method public getLastSynced()J
    .locals 2

    .prologue
    .line 831
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    return-wide v0
.end method

.method public getPinId()I
    .locals 1

    .prologue
    .line 869
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    return v0
.end method

.method public getPinnedVersion()I
    .locals 1

    .prologue
    .line 926
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1023
    const/4 v0, 0x0

    .line 1024
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v1, :cond_0

    .line 1025
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1026
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1028
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 1029
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    .line 1030
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1032
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 1033
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    .line 1034
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1036
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 1037
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    .line 1038
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1040
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 1041
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    .line 1042
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 1045
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    .line 1046
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 1049
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    .line 1050
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    :cond_6
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->cachedSize:I

    .line 1053
    return v0
.end method

.method public getSnapshotId()I
    .locals 1

    .prologue
    .line 850
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    return v0
.end method

.method public hasEdition()Z
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastSyncProgressAtFailure()Z
    .locals 1

    .prologue
    .line 915
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastSyncStarted()Z
    .locals 1

    .prologue
    .line 896
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastSynced()Z
    .locals 1

    .prologue
    .line 839
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSnapshotId()Z
    .locals 1

    .prologue
    .line 858
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 984
    const/16 v0, 0x11

    .line 985
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 986
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 987
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 988
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    add-int v0, v1, v2

    .line 989
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    add-int v0, v1, v2

    .line 990
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 991
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 992
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    add-int v0, v1, v2

    .line 993
    return v0

    .line 986
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1062
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1066
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1067
    :sswitch_0
    return-object p0

    .line 1072
    :sswitch_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v2, :cond_1

    .line 1073
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1075
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1079
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    .line 1080
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1084
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    .line 1085
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1089
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    .line 1090
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1094
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    .line 1095
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1099
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    .line 1100
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1104
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1105
    .local v1, "temp":I
    if-eqz v1, :cond_2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 1107
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    .line 1108
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    goto :goto_0

    .line 1110
    :cond_3
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    goto :goto_0

    .line 1062
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 796
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    move-result-object v0

    return-object v0
.end method

.method public setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 815
    if-nez p1, :cond_0

    .line 816
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 818
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 819
    return-object p0
.end method

.method public setLastSyncProgressAtFailure(F)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 910
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    .line 911
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 912
    return-object p0
.end method

.method public setLastSynced(J)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 834
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    .line 835
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 836
    return-object p0
.end method

.method public setPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 872
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    .line 873
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 874
    return-object p0
.end method

.method public setPinnedVersion(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 929
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    .line 930
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 931
    return-object p0
.end method

.method public setSnapshotId(I)Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 853
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    .line 854
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    .line 855
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 998
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v0, :cond_0

    .line 999
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1001
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 1002
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSynced_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1004
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 1005
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->snapshotId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1007
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 1008
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1010
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 1011
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncStarted_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1013
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 1014
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->lastSyncProgressAtFailure_:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 1016
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 1017
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->pinnedVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1019
    :cond_6
    return-void
.end method
