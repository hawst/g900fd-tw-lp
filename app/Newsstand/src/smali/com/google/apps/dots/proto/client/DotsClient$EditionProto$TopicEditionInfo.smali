.class public final Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TopicEditionInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;


# instance fields
.field private clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2437
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    .locals 3

    .prologue
    .line 2469
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2473
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_0

    .line 2474
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 2476
    :cond_0
    return-object v0

    .line 2470
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    :catch_0
    move-exception v1

    .line 2471
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2434
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2481
    if-ne p1, p0, :cond_1

    .line 2484
    :cond_0
    :goto_0
    return v1

    .line 2482
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2483
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    .line 2484
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getClientEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1

    .prologue
    .line 2443
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2503
    const/4 v0, 0x0

    .line 2504
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v1, :cond_0

    .line 2505
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 2506
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2508
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->cachedSize:I

    .line 2509
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2488
    const/16 v0, 0x11

    .line 2489
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2490
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 2491
    return v0

    .line 2490
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2517
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2518
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2522
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2523
    :sswitch_0
    return-object p0

    .line 2528
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v1, :cond_1

    .line 2529
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 2531
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2518
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2434
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2496
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v0, :cond_0

    .line 2497
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->clientEntity_:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2499
    :cond_0
    return-void
.end method
