.class Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "AudioControlBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setupButtons(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isPlaying:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->pauseAudio(Landroid/content/Context;)V

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->playAudio(Landroid/content/Context;)V

    goto :goto_0
.end method
