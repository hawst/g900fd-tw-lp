.class Lcom/google/apps/dots/android/newsstand/sync/Pinner$2;
.super Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;
.source "Pinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Pinner;-><init>(Lcom/google/apps/dots/android/newsstand/events/EventNotifier;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;Lcom/google/apps/dots/android/newsstand/server/ServerUris;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected onEventAsync(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getAccount(Ljava/util/Map;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 93
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$2;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedCurationsAndMigratePinnedEntityTopics(Landroid/accounts/Account;)V
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$100(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;)V

    .line 94
    return-void
.end method
