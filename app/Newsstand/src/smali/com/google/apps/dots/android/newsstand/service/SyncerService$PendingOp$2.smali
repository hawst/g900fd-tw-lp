.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;
.super Ljava/lang/Object;
.source "SyncerService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->startSync()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

.field final synthetic val$nextSnapshotId:I

.field final synthetic val$pinId:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Ljava/lang/Integer;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$pinId:Ljava/lang/Integer;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$nextSnapshotId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 175
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Sync failed for %s, pinId=%s, snapshot=%s. Exception: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$pinId:Ljava/lang/Integer;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$nextSnapshotId:I

    .line 176
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->progress:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->markFailed(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;F)V

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->remove()V

    .line 179
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 168
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Synced %s, pinId=%s, snapshot=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$pinId:Ljava/lang/Integer;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$nextSnapshotId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->val$nextSnapshotId:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->markSynced(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->remove()V

    .line 171
    return-void
.end method
