.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$1;
.super Ljava/lang/Object;
.source "MyMagazinesGroupingFilter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->subTransformByAppFamily(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 2
    .param p1, "lhs"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "rhs"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 174
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    .line 175
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 171
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    check-cast p2, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$1;->compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    return v0
.end method
