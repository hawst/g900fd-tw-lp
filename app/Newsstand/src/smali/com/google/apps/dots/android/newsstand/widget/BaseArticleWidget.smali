.class public abstract Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;
.source "BaseArticleWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

.field protected articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

.field protected form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

.field protected idToAdTemplate:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation
.end field

.field private isTextView:Z

.field protected post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field private final postIndex:I

.field private final random:Ljava/util/Random;

.field private final readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

.field protected section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

.field protected subscriptions:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

.field private unzoomableMessageShown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 7
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "postIndex"    # I
    .param p4, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p5, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p6, "useVerticalLayout"    # Z

    .prologue
    .line 100
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 101
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->readingActivityTracker()Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->readingActivity:Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    .line 102
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->postIndex:I

    .line 103
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->random:Ljava/util/Random;

    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->subscriptions:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 105
    return-void
.end method

.method private getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;
    .locals 1
    .param p1, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .param p2, "isPhone"    # Z
    .param p3, "showGoogleSold"    # Z

    .prologue
    .line 390
    if-eqz p1, :cond_1

    .line 391
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Z)Ljava/lang/String;

    move-result-object v0

    .line 394
    :goto_1
    return-object v0

    .line 391
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    goto :goto_0

    .line 394
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private loadPostData()V
    .locals 2

    .prologue
    .line 193
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 215
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$1;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 216
    return-void
.end method

.method private static putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "objectNode"    # Lorg/codehaus/jackson/node/ObjectNode;
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 383
    if-eqz p2, :cond_0

    .line 384
    invoke-virtual {p0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :cond_0
    return-void
.end method

.method private static safeGetPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;
    .locals 1
    .param p0, "settings"    # Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .prologue
    .line 375
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 376
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdSystem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private setZoomableFromTemplate()V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isZoomableFromTemplate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setZoomable(Z)V

    .line 257
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->destroy()V

    .line 177
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 178
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 179
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 180
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    goto :goto_0
.end method

.method protected fixNavigatorOnline()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "dots.androidOffline = %s;"

    new-array v4, v0, [Ljava/lang/Object;

    .line 242
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 241
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->executeStatements([Ljava/lang/String;)V

    .line 243
    return-void

    :cond_0
    move v0, v1

    .line 242
    goto :goto_0
.end method

.method protected getAdBlockData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 16
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p2, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 272
    sget-object v11, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v11}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    .line 274
    .local v2, "adBlockData":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-object v2

    .line 278
    :cond_1
    new-instance v9, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;

    const/4 v11, 0x0

    invoke-direct {v9, v11}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;-><init>(Z)V

    .line 279
    .local v9, "serializer":Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;
    invoke-virtual/range {p3 .. p3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v7

    .line 281
    .local v7, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v11

    sget-object v12, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v11, v12, :cond_7

    const/4 v5, 0x1

    .line 282
    .local v5, "isPhone":Z
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->hasGoogleSoldAds()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 283
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAlwaysShowGoogleSoldAds()Z

    move-result v11

    if-nez v11, :cond_2

    .line 284
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getGoogleSoldAds()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->getPercent()F

    move-result v11

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_8

    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xf

    if-le v11, v12, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->random:Ljava/util/Random;

    .line 287
    invoke-virtual {v11}, Ljava/util/Random;->nextFloat()F

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getGoogleSoldAds()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->getPercent()F

    move-result v12

    cmpg-float v11, v11, v12

    if-gtz v11, :cond_8

    :cond_2
    const/4 v10, 0x1

    .line 291
    .local v10, "showGoogleSold":Z
    :goto_2
    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Integer;

    const/4 v12, 0x0

    .line 292
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v13

    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->safeGetPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    .line 293
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v13

    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->safeGetPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    .line 294
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v13

    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->safeGetPubAdSystem(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    .line 292
    invoke-static {v11}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v8

    .line 295
    .local v8, "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 296
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    const/4 v11, 0x5

    .line 297
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    const/4 v6, 0x1

    .line 299
    .local v6, "onlyGoogleManaged":Z
    :goto_3
    sget-object v11, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v12, "getAdBlockData(): isPhone? %b showGoogleSold? %b onlyGoogleManaged? %b"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    .line 300
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    aput-object v15, v13, v14

    .line 299
    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    const-string v11, "adFrequencySettings"

    .line 304
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getAdFrequency()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v12

    .line 303
    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 306
    const-string v11, "interstitialAdSettings"

    .line 307
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v12

    .line 306
    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 308
    const-string v11, "interstitialAdTemplate"

    .line 309
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v5, v10}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v12

    .line 308
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v11, "leaderboardAdSettings"

    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v12

    .line 311
    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 313
    const-string v11, "leaderboardAdTemplate"

    .line 314
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v5, v10}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v12

    .line 313
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string v11, "mrectAdSettings"

    .line 317
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/apps/dots/android/newsstand/model/AdSettingsJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v12

    .line 316
    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 318
    const-string v11, "mrectAdTemplate"

    .line 319
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v5, v10}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;ZZ)Ljava/lang/String;

    move-result-object v12

    .line 318
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v11, "showEmptyAds"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v12

    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 324
    const-string v11, "postId"

    iget-object v12, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string v11, "appId"

    iget-object v12, v7, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v11, "appFamilyId"

    .line 328
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getAppFamilyId()Ljava/lang/String;

    move-result-object v12

    .line 327
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v11, "languagePref"

    .line 330
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getPreferredLanguage()Ljava/lang/String;

    move-result-object v12

    .line 329
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v11, "contentLanguage"

    .line 332
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getLanguageCode()Ljava/lang/String;

    move-result-object v12

    .line 331
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v12, "deviceType"

    if-eqz v5, :cond_a

    const-string v11, "phone"

    :goto_4
    invoke-static {v2, v12, v11}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v11, "platform"

    const-string v12, "android"

    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v11, "subscriptionType"

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->subscriptions:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 338
    invoke-virtual {v12, v13}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;->toString()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v12, v13}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    .line 337
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    if-eqz v10, :cond_3

    .line 344
    const-string v11, "showGoogleSold"

    invoke-virtual {v2, v11, v10}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 348
    :cond_3
    if-nez v10, :cond_4

    const/4 v11, 0x5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 349
    :cond_4
    const-string v11, "topic"

    const-string v12, ","

    .line 350
    invoke-static {v12}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-static {v13}, Lcom/google/common/primitives/Ints;->asList([I)Ljava/util/List;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v12

    .line 349
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_5
    if-nez v10, :cond_6

    if-eqz v6, :cond_0

    .line 355
    :cond_6
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    .line 356
    .local v1, "account":Landroid/accounts/Account;
    sget-object v11, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v12, "account is %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v1, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    if-eqz v1, :cond_0

    .line 358
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v3

    .line 359
    .local v3, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->hasDemographics()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 360
    sget-object v11, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v12, "config is %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v3, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 361
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getDemographics()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    move-result-object v4

    .line 362
    .local v4, "demographics":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    if-eqz v4, :cond_0

    .line 363
    sget-object v11, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v12, "demographics is %s %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getAgeRange()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getGender()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    const-string v11, "ageRange"

    .line 365
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getAgeRange()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    .line 364
    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v11, "gender"

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->getGender()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v11, v12}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIfExists(Lorg/codehaus/jackson/node/ObjectNode;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 281
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v3    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .end local v4    # "demographics":Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    .end local v5    # "isPhone":Z
    .end local v6    # "onlyGoogleManaged":Z
    .end local v8    # "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v10    # "showGoogleSold":Z
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 287
    .restart local v5    # "isPhone":Z
    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 297
    .restart local v8    # "pubAdSystems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v10    # "showGoogleSold":Z
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 333
    .restart local v6    # "onlyGoogleManaged":Z
    :cond_a
    const-string v11, "tablet"

    goto/16 :goto_4
.end method

.method public getAdTemplate(Lcom/google/apps/dots/proto/client/DotsShared$AdContent;Z)Ljava/lang/String;
    .locals 4
    .param p1, "content"    # Lcom/google/apps/dots/proto/client/DotsShared$AdContent;
    .param p2, "isPhone"    # Z

    .prologue
    const/4 v1, 0x0

    .line 399
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdSystem()I

    move-result v2

    if-nez v2, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-object v1

    .line 401
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 402
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->idToAdTemplate:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 403
    .local v0, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 405
    .end local v0    # "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getPhoneTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getTabletTemplate()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getEstimatedPages()I
    .locals 2

    .prologue
    .line 235
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected getPost()Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    return-object v0
.end method

.method protected getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v0, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getRolesBlockData(Lcom/google/apps/dots/proto/client/DotsShared$RoleList;)Lorg/codehaus/jackson/node/ArrayNode;
    .locals 5
    .param p1, "roles"    # Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .prologue
    .line 264
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v0

    .line 265
    .local v0, "roleBlockData":Lorg/codehaus/jackson/node/ArrayNode;
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->roleId:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 266
    .local v1, "roleId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/node/ArrayNode;->add(Ljava/lang/String;)V

    .line 265
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 268
    .end local v1    # "roleId":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public handleOnWebViewClientReady()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->fixNavigatorOnline()V

    .line 163
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->populateJsonStore()V

    .line 164
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setTemplateProperties()V

    .line 165
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->loadPostData()V

    .line 167
    :cond_0
    return-void
.end method

.method public isZoomableFromTemplate()Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getZoomable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public landscape()Z
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getHeight()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "optionalArgs"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 220
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->scriptReady:Z

    if-eqz v2, :cond_0

    const-string v2, "pageview"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    aget-object v2, p2, v5

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 222
    .local v0, "pageNumber":I
    const-string v1, "dots.layout.triggerPageView && dots.layout.triggerPageView(%s);"

    .line 223
    .local v1, "stmt":Ljava/lang/String;
    new-array v2, v3, [Ljava/lang/String;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->executeStatements([Ljava/lang/String;)V

    .line 231
    .end local v0    # "pageNumber":I
    .end local v1    # "stmt":Ljava/lang/String;
    :goto_0
    return-void

    .line 224
    :cond_0
    const-string v2, "pause"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onPause()V

    goto :goto_0

    .line 226
    :cond_1
    const-string v2, "resume"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 227
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->onResume()V

    goto :goto_0

    .line 229
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onArticleAvailable(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Ljava/util/Map;Z)V
    .locals 4
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .param p2, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p4, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .param p5, "articleTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p7, "useLegacyLayout"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p6, "idToAdTemplate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;->setData(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    .line 121
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 122
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 123
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 124
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->form:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 125
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->articleTemplate:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 126
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->idToAdTemplate:Ljava/util/Map;

    .line 130
    if-eqz p7, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v0, v3, :cond_1

    .line 131
    :cond_0
    invoke-virtual {p0, p5}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setBackgroundColorFromTemplate(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)V

    .line 133
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setZoomableFromTemplate()V

    .line 134
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget$ArticleDotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setDotsWebViewClient(Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;)V

    .line 135
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->unzoomableMessageShown:Z

    .line 136
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isTextView:Z

    .line 137
    if-eqz p2, :cond_2

    .line 138
    iget-object v0, p2, Lcom/google/apps/dots/proto/client/DotsShared$Section;->features:[I

    .line 139
    invoke-static {v0}, Lcom/google/common/primitives/Ints;->asList([I)Ljava/util/List;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 138
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setNavigationTouchpointsEnabled(Z)V

    .line 140
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hasCorrespondingImageSectionId()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isTextView:Z

    .line 142
    :cond_2
    invoke-virtual {p0, p7}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->setUseLegacyLayout(Z)V

    .line 144
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 139
    goto :goto_0
.end method

.method protected onZoomAttempt()V
    .locals 3

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->zoomable:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->isTextView:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->unzoomableMessageShown:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->useLegacyLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->zooming_is_unavailable:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->unzoomableMessageShown:Z

    .line 154
    :cond_0
    return-void
.end method

.method protected populateJsonStore()V
    .locals 2

    .prologue
    .line 184
    const-string v0, "postIndex"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->postIndex:I

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIntoJsonStore(Ljava/lang/String;I)V

    .line 185
    const-string v0, "appId"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method protected setTemplateProperties()V
    .locals 0

    .prologue
    .line 190
    return-void
.end method
