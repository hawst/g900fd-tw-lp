.class public final Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ObjectIdProto"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;


# instance fields
.field private bitField0_:I

.field private externalIdIsTopicId_:Z

.field private externalId_:[B

.field private parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

.field private preferOriginalTransformationHint_:Z

.field private preferZoomTransformationHint_:Z

.field private targetTranslationLanguage_:Ljava/lang/String;

.field public type:I

.field private uniqueId_:J

.field private uuidLeastSignificantBits_:J

.field private uuidMostSignificantBits_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15925
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 15926
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 15951
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    .line 15954
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    .line 15973
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    .line 15992
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    .line 16014
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    .line 16052
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    .line 16071
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    .line 16090
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    .line 16109
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 15926
    return-void
.end method


# virtual methods
.method public clearTargetTranslationLanguage()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 1

    .prologue
    .line 16125
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 16126
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    .line 16127
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 3

    .prologue
    .line 16150
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16154
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-eqz v2, :cond_0

    .line 16155
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 16157
    :cond_0
    return-object v0

    .line 16151
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :catch_0
    move-exception v1

    .line 16152
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 15922
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16162
    if-ne p1, p0, :cond_1

    .line 16174
    :cond_0
    :goto_0
    return v1

    .line 16163
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 16164
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 16165
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    .line 16168
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-nez v3, :cond_3

    .line 16170
    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 16174
    goto :goto_0

    .line 16168
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 16170
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 16174
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getExternalId()[B
    .locals 1

    .prologue
    .line 15994
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    return-object v0
.end method

.method public getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 1

    .prologue
    .line 16035
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    return-object v0
.end method

.method public getPreferOriginalTransformationHint()Z
    .locals 1

    .prologue
    .line 16054
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    return v0
.end method

.method public getPreferZoomTransformationHint()Z
    .locals 1

    .prologue
    .line 16073
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 16232
    const/4 v0, 0x0

    .line 16233
    .local v0, "size":I
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    .line 16234
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16235
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16236
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    .line 16237
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16239
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16240
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    .line 16241
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16243
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 16244
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    .line 16245
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 16247
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-eqz v1, :cond_3

    .line 16248
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 16249
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16251
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 16252
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    .line 16253
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16255
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 16256
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    .line 16257
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16259
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 16260
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    .line 16261
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16263
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 16264
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    .line 16265
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16267
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 16268
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 16269
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16271
    :cond_8
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->cachedSize:I

    .line 16272
    return v0
.end method

.method public getTargetTranslationLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16111
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public hasExternalId()Z
    .locals 1

    .prologue
    .line 16005
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasParentId()Z
    .locals 1

    .prologue
    .line 16045
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTargetTranslationLanguage()Z
    .locals 1

    .prologue
    .line 16122
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/16 v10, 0x20

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 16178
    const/16 v1, 0x11

    .line 16179
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 16180
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    add-int v1, v2, v6

    .line 16181
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 16182
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 16183
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 16189
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 16190
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 16191
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    if-eqz v2, :cond_3

    move v2, v4

    :goto_1
    add-int v1, v6, v2

    .line 16192
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    if-eqz v2, :cond_4

    move v2, v4

    :goto_2
    add-int v1, v6, v2

    .line 16193
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    if-eqz v6, :cond_5

    :goto_3
    add-int v1, v2, v4

    .line 16194
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    if-nez v4, :cond_6

    :goto_4
    add-int v1, v2, v3

    .line 16195
    return v1

    .line 16185
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 16186
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    aget-byte v6, v6, v0

    add-int v1, v2, v6

    .line 16185
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 16190
    .end local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_3
    move v2, v5

    .line 16191
    goto :goto_1

    :cond_4
    move v2, v5

    .line 16192
    goto :goto_2

    :cond_5
    move v4, v5

    .line 16193
    goto :goto_3

    .line 16194
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0xa

    .line 16280
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 16281
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 16285
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 16286
    :sswitch_0
    return-object p0

    .line 16291
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 16292
    .local v1, "temp":I
    if-eq v1, v4, :cond_1

    if-eqz v1, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0xb

    if-eq v1, v2, :cond_1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    const/16 v2, 0xe

    if-eq v1, v2, :cond_1

    const/16 v2, 0xf

    if-eq v1, v2, :cond_1

    const/16 v2, 0x10

    if-eq v1, v2, :cond_1

    const/16 v2, 0x11

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12

    if-eq v1, v2, :cond_1

    const/16 v2, 0x13

    if-ne v1, v2, :cond_2

    .line 16309
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    goto :goto_0

    .line 16311
    :cond_2
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    goto :goto_0

    .line 16316
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    .line 16317
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto :goto_0

    .line 16321
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    .line 16322
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto :goto_0

    .line 16326
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    .line 16327
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto :goto_0

    .line 16331
    :sswitch_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-nez v2, :cond_3

    .line 16332
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 16334
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 16338
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    .line 16339
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto/16 :goto_0

    .line 16343
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    .line 16344
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto/16 :goto_0

    .line 16348
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    .line 16349
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto/16 :goto_0

    .line 16353
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    .line 16354
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto/16 :goto_0

    .line 16358
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 16359
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    goto/16 :goto_0

    .line 16281
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15922
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    return-object v0
.end method

.method public setTargetTranslationLanguage(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 16114
    if-nez p1, :cond_0

    .line 16115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16117
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    .line 16118
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    .line 16119
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16200
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 16201
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16202
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidMostSignificantBits_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 16204
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16205
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uuidLeastSignificantBits_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 16207
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 16208
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalId_:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 16210
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    if-eqz v0, :cond_3

    .line 16211
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->parentId_:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16213
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 16214
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->uniqueId_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 16216
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 16217
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferOriginalTransformationHint_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 16219
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 16220
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->preferZoomTransformationHint_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 16222
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 16223
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->externalIdIsTopicId_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 16225
    :cond_7
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    .line 16226
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->targetTranslationLanguage_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16228
    :cond_8
    return-void
.end method
