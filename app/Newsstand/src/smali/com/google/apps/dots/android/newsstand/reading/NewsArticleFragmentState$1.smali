.class final Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState$1;
.super Ljava/lang/Object;
.source "NewsArticleFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "postId":Ljava/lang/String;
    const-class v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 81
    .local v3, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-class v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 84
    .local v0, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 85
    .local v2, "postIndex":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 86
    const/4 v2, 0x0

    .line 88
    :cond_0
    new-instance v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    invoke-direct {v4, v1, v3, v0, v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V

    return-object v4
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 93
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    move-result-object v0

    return-object v0
.end method
