.class public final Lcom/google/apps/dots/android/newsstand/util/MotionEventUtil;
.super Ljava/lang/Object;
.source "MotionEventUtil.java"


# direct methods
.method public static transform(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "event"    # Landroid/view/MotionEvent;
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 8
    invoke-static {p0}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 9
    .local v0, "newEvent":Landroid/view/MotionEvent;
    invoke-virtual {v0, p1}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 10
    return-object v0
.end method
