.class public abstract Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;
.super Lcom/google/android/play/onboard/OnboardSimpleQuizPage;
.source "NSOnboardQuizPage.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public static addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 6
    .param p0, "pageList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 81
    const-string v4, "quizCurations"

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    .line 84
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 85
    .local v0, "curationsPage":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v5, "quizCurations"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 86
    sget v4, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$2;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$2;-><init>()V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v2, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v2}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 95
    .local v2, "magsPage":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v5, "quizMagazineOffers"

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 96
    sget v4, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$3;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$3;-><init>()V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 102
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    new-instance v3, Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v4

    iget v4, v4, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    invoke-direct {v3, v4, v1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    .line 105
    .local v3, "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/libraries/bind/data/DataChange;->get(ZZ)Lcom/google/android/libraries/bind/data/DataChange;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 107
    .end local v0    # "curationsPage":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v2    # "magsPage":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    :cond_0
    return-void
.end method

.method protected static getItemContentDescription(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "itemTitle"    # Ljava/lang/String;
    .param p1, "selected"    # Z

    .prologue
    .line 71
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->onboard_quiz_selected_description:I

    .line 74
    .local v0, "contentDescriptionResId":I
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 71
    .end local v0    # "contentDescriptionResId":I
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->onboard_quiz_not_selected_description:I

    goto :goto_0
.end method


# virtual methods
.method protected getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->onboard_button_done:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;Lcom/google/android/play/onboard/OnboardHostControl;)V

    .line 59
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public onEnterPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->onEnterPage(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->sendAnalyticsEvent()V

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/GATracker;->flushAnalyticsEvents()V

    .line 53
    return-void
.end method

.method protected abstract sendAnalyticsEvent()V
.end method

.method protected setUpAdapter()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->setUpAdapter()V

    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 46
    return-void
.end method
