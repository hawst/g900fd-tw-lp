.class public Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "DotsWebChromeClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->TAG:Ljava/lang/String;

    .line 29
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 36
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method


# virtual methods
.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 6
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    const/4 v5, 0x1

    .line 54
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->TAG:Ljava/lang/String;

    const-string v1, "%s %s:%d: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->messageLevel()Landroid/webkit/ConsoleMessage$MessageLevel;

    move-result-object v4

    aput-object v4, v2, v3

    .line 57
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->sourceId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 56
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->messageLevel()Landroid/webkit/ConsoleMessage$MessageLevel;

    move-result-object v0

    sget-object v1, Landroid/webkit/ConsoleMessage$MessageLevel;->ERROR:Landroid/webkit/ConsoleMessage$MessageLevel;

    if-ne v0, v1, :cond_0

    .line 60
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->onJsError(Landroid/webkit/ConsoleMessage;)V

    .line 63
    :cond_0
    return v5
.end method

.method public onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 5
    .param p1, "parentView"    # Landroid/webkit/WebView;
    .param p2, "isDialog"    # Z
    .param p3, "isUserGesture"    # Z
    .param p4, "resultMsg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 106
    if-eqz p3, :cond_0

    .line 111
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v4, "passing window creation to temp web view"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 113
    .local v0, "tempWebView":Landroid/webkit/WebView;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient$TempWebViewClient;-><init>(Landroid/webkit/WebView;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 114
    iget-object v1, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/webkit/WebView$WebViewTransport;

    .line 115
    .local v1, "transport":Landroid/webkit/WebView$WebViewTransport;
    invoke-virtual {v1, v0}, Landroid/webkit/WebView$WebViewTransport;->setWebView(Landroid/webkit/WebView;)V

    .line 116
    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    .line 117
    const/4 v2, 0x1

    .line 119
    .end local v0    # "tempWebView":Landroid/webkit/WebView;
    .end local v1    # "transport":Landroid/webkit/WebView$WebViewTransport;
    :cond_0
    return v2
.end method

.method public onHideCustomView()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideCustomView()V

    .line 50
    return-void
.end method

.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method public onJsBeforeUnload(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public onJsError(Landroid/webkit/ConsoleMessage;)V
    .locals 0
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 68
    return-void
.end method

.method public onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "defaultValue"    # Ljava/lang/String;
    .param p5, "result"    # Landroid/webkit/JsPromptResult;

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "callback"    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    .prologue
    const/4 v3, -0x2

    .line 40
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 41
    .local v0, "fullscreenView":Landroid/widget/FrameLayout;
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 42
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebChromeClient;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v1, v0, p2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->showCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 45
    return-void
.end method
