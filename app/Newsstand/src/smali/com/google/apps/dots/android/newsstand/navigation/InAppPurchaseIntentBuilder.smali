.class public Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "InAppPurchaseIntentBuilder.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private analyticsDimensions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private analyticsMetrics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private backendDocId:Ljava/lang/String;

.field private backendId:Ljava/lang/Integer;

.field private defaultOfferType:Ljava/lang/Integer;

.field private docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field private fullDocId:Ljava/lang/String;

.field private isHouseAd:Z

.field private offerFilter:Ljava/lang/String;

.field private offerType:Ljava/lang/Integer;

.field private final optConversionAnalyticsEventProvider:Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

.field private parentBackendDocId:Ljava/lang/String;

.field private showPurchaseToast:Z

.field private uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "optConversionAnalyticsEventProvider"    # Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 69
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsDimensions:Ljava/util/Map;

    .line 70
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsMetrics:Ljava/util/Map;

    .line 79
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->optConversionAnalyticsEventProvider:Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    .line 80
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->tryLoggingConversionEvent()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->showPurchaseToast:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->getPurchaseSuccessMessageId()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Lcom/google/apps/dots/android/newsstand/util/DocType;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendDocId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->parentBackendDocId:Ljava/lang/String;

    return-object v0
.end method

.method private getPurchaseSuccessMessageId()I
    .locals 2

    .prologue
    .line 292
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$2;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 310
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success:I

    :goto_0
    return v0

    .line 294
    :pswitch_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success_books:I

    goto :goto_0

    .line 299
    :pswitch_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success_movies:I

    goto :goto_0

    .line 301
    :pswitch_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success_apps:I

    goto :goto_0

    .line 305
    :pswitch_3
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success_newsstand:I

    goto :goto_0

    .line 308
    :pswitch_4
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->iap_success_music:I

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public static isInAppPurchaseSupported(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.vending"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 89
    :goto_0
    if-eqz v0, :cond_1

    .line 90
    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const v3, 0x4c7c14b

    if-lt v2, v3, :cond_0

    const/4 v1, 0x1

    .line 94
    :cond_0
    :goto_1
    return v1

    .line 93
    :cond_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Play Store package information not found. Should never happen"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 86
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static isInAppPurchaseUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 98
    const-string v0, "play.google.com"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static final isNewsstandPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pathSegment"    # Ljava/lang/String;

    .prologue
    .line 241
    const-string v0, "magazines"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "newsstand"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryLoggingConversionEvent()V
    .locals 6

    .prologue
    .line 282
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->optConversionAnalyticsEventProvider:Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->optConversionAnalyticsEventProvider:Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->fullDocId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isHouseAd:Z

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsDimensions:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsMetrics:Ljava/util/Map;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;->get(Ljava/lang/String;ZLjava/util/Map;Ljava/util/Map;)Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;

    move-result-object v0

    .line 285
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;
    if-eqz v0, :cond_0

    .line 286
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;->track(Z)V

    .line 289
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;
    :cond_0
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 256
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendId:Ljava/lang/Integer;

    const-string v2, "Tried to build an IAP intent with no backend id"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    const-string v2, "Tried to build an IAP intent with no doc type"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendDocId:Ljava/lang/String;

    const-string v2, "Tried to build an IAP intent with no backend doc id"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->fullDocId:Ljava/lang/String;

    const-string v2, "Tried to build an IAP intent with no full doc id"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->defaultOfferType:Ljava/lang/Integer;

    const-string v2, "Tried to build an IAP intent with no offer type"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.PURCHASE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    const-string v1, "backend"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 266
    const-string v1, "document_type"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/util/DocType;->protoValue:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 267
    const-string v1, "backend_docid"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendDocId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const-string v1, "full_docid"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->fullDocId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v2, "offer_type"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerType:Ljava/lang/Integer;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->defaultOfferType:Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 270
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->uri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 271
    const-string v1, "referral_url"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerFilter:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 274
    const-string v1, "offer_filter"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerFilter:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    :cond_1
    const-string v1, "authAccount"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    return-object v0

    .line 269
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerType:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "backendDocId"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 143
    return-object p0
.end method

.method public setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 1
    .param p1, "backendId"    # I

    .prologue
    .line 102
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendId:Ljava/lang/Integer;

    .line 103
    return-object p0
.end method

.method public setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 2
    .param p1, "docType"    # Lcom/google/apps/dots/android/newsstand/util/DocType;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 108
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$2;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 117
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->defaultOfferType:Ljava/lang/Integer;

    .line 119
    :goto_0
    return-object p0

    .line 114
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->defaultOfferType:Ljava/lang/Integer;

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "fullDocId"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->fullDocId:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public setIsHouseAd(Z)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isHouseAd:Z

    .line 251
    return-object p0
.end method

.method public setOfferFilter(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "offerFilter"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerFilter:Ljava/lang/String;

    .line 138
    return-object p0
.end method

.method public setOfferType(Lcom/google/apps/dots/android/newsstand/util/OfferType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 1
    .param p1, "offerType"    # Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .prologue
    .line 128
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    if-eq p1, v0, :cond_0

    .line 129
    iget v0, p1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoValue:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerType:Ljava/lang/Integer;

    .line 133
    :goto_0
    return-object p0

    .line 131
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerType:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public setOfferType(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "offerType"    # Ljava/lang/Integer;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->offerType:Ljava/lang/Integer;

    .line 124
    return-object p0
.end method

.method public setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "parentBackendDocId"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->parentBackendDocId:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public setPlayStoreUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x1

    const/4 v11, 0x6

    const/4 v10, 0x3

    const/4 v9, 0x2

    .line 158
    sget-object v4, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "setPlayStoreUri(%s)"

    new-array v6, v12, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isInAppPurchaseUri(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 160
    new-instance v4, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;

    const-string v5, "Not a valid play store URL"

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    throw v4

    .line 162
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "path":[Ljava/lang/String;
    array-length v4, v2

    if-ge v4, v13, :cond_1

    .line 164
    new-instance v4, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;

    const-string v5, "Path is too short."

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    throw v4

    .line 166
    :cond_1
    const-string v4, "id"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 168
    new-instance v4, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;

    const-string v5, "No product ID found in Play Store URL"

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    throw v4

    .line 171
    :cond_2
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->uri:Landroid/net/Uri;

    .line 173
    const-string v4, "apps"

    aget-object v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "details"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 174
    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 175
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->ANDROID_APP:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 176
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 177
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 225
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsDimensions:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseDimensions(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 226
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->analyticsMetrics:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/NSAnalyticsParams;->parseMetrics(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 227
    return-object p0

    .line 178
    :cond_3
    const-string v4, "books"

    aget-object v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "details"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 179
    invoke-virtual {p0, v12}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 180
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->OCEAN_BOOK:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 181
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 182
    const-string v5, "book-"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto :goto_0

    :cond_4
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 183
    :cond_5
    aget-object v4, v2, v9

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isNewsstandPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "details"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 184
    const-string v4, "cdid"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "cdid":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 186
    invoke-virtual {p0, v11}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 187
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE_ISSUE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 188
    const/16 v4, 0x2d

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 189
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 190
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    .line 192
    :cond_6
    invoke-virtual {p0, v11}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 193
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 194
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 195
    const-string v5, "magazine-"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    :cond_7
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 197
    .end local v0    # "cdid":Ljava/lang/String;
    :cond_8
    aget-object v4, v2, v9

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->isNewsstandPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "news"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 198
    invoke-virtual {p0, v11}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 199
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 200
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 201
    const-string v5, "newsedition-"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    :cond_9
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 202
    :cond_a
    const-string v4, "music"

    aget-object v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "album"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 203
    const-string v4, "tid"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "tid":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 205
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 206
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_SONG:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 207
    const/16 v4, 0x2d

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 208
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 209
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    .line 211
    :cond_b
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 212
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->MUSIC_ALBUM:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 213
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 214
    const-string v5, "album-"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_4
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    :cond_c
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 216
    .end local v3    # "tid":Ljava/lang/String;
    :cond_d
    const-string v4, "movies"

    aget-object v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "details"

    aget-object v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 217
    invoke-virtual {p0, v13}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 218
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/DocType;->YOUTUBE_MOVIE:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 219
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 220
    const-string v5, "movie-"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_5
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    goto/16 :goto_0

    :cond_e
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 222
    :cond_f
    new-instance v4, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;

    const-string v5, "Not a supported product type"

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/exception/UriNotAllowedException;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    throw v4
.end method

.method public setShowPurchaseToast(Z)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 245
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->showPurchaseToast:Z

    .line 246
    return-object p0
.end method

.method public start(Z)V
    .locals 6
    .param p1, "addToBackstack"    # Z

    .prologue
    .line 317
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->activity:Landroid/app/Activity;

    instance-of v2, v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v2, :cond_0

    .line 318
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->activity:Landroid/app/Activity;

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 319
    .local v1, "nsActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    const/16 v2, 0x66

    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;-><init>(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setResultHandlerForActivityCode(ILcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;)V

    .line 343
    .end local v1    # "nsActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    :cond_0
    const/16 v2, 0x66

    invoke-virtual {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->startForResult(IZ)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v3, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    const-string v4, "market://details?id="

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->fullDocId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 346
    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v2

    const/4 v3, 0x1

    .line 347
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setUseDirectPurchase(Z)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v2

    .line 348
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    goto :goto_0

    .line 345
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
