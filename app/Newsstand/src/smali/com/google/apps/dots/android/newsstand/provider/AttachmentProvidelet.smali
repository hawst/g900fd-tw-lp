.class public Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;
.super Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;
.source "AttachmentProvidelet.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->context:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;)Landroid/content/res/AssetFileDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getNotFoundImageAssetDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private getAttachmentFD(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    .line 112
    .local v3, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    if-eqz p2, :cond_0

    .line 115
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 116
    invoke-virtual {v4, p2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 118
    .local v0, "givenTransformRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 119
    .local v1, "givenTransformResponse":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    new-instance v4, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;

    invoke-direct {v4, p0, v3, p1}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$2;-><init>(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 132
    .end local v0    # "givenTransformRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v1    # "givenTransformResponse":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    .local v2, "responseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->extractAFD(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4

    .line 129
    .end local v2    # "responseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, p1, v5}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .restart local v2    # "responseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    goto :goto_0
.end method

.method private getNotFoundImageAssetDescriptor()Landroid/content/res/AssetFileDescriptor;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getNotFoundImagePath()Ljava/lang/String;

    move-result-object v6

    .line 157
    .local v6, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    .line 158
    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 159
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    return-object v0
.end method


# virtual methods
.method public getContentType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 50
    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method public getNotFoundImagePath()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "attachment_not_found.png"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 137
    .local v2, "notFoundFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->context:Landroid/content/Context;

    .line 139
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->image_not_found:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 141
    .local v3, "notFound":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bitmap"

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x4b

    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->writeBitmap(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v3    # "notFound":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 147
    .restart local v3    # "notFound":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v6

    .line 148
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Error writing not found bitmap"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v6, v1, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method public openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
    .locals 11
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .param p4, "contentProvider"    # Landroid/content/ContentProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 57
    sget-object v6, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "requesting: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p2, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    packed-switch p1, :pswitch_data_0

    .line 77
    const/4 v1, 0x0

    .line 78
    .local v1, "attachmentId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 82
    .local v4, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->isValidAttachmentIdOrUrl(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 84
    invoke-direct {p0, v1, v4}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getAttachmentFD(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 86
    .local v0, "attachmentFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/content/res/AssetFileDescriptor;>;"
    invoke-virtual {p2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v6

    const-string v7, "noplaceholder"

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 87
    .local v2, "noPlaceholder":Z
    if-nez v2, :cond_1

    .line 89
    new-instance v6, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$1;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet$1;-><init>(Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;)V

    invoke-static {v0, v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 100
    :cond_1
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->isNoPumpFd(Landroid/net/Uri;)Z

    move-result v6

    .line 99
    invoke-static {v0, v6}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    return-object v6

    .line 64
    .end local v0    # "attachmentFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/content/res/AssetFileDescriptor;>;"
    .end local v1    # "attachmentId":Ljava/lang/String;
    .end local v2    # "noPlaceholder":Z
    .end local v4    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :pswitch_0
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 65
    .restart local v1    # "attachmentId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 66
    .restart local v4    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    goto :goto_0

    .line 68
    .end local v1    # "attachmentId":Ljava/lang/String;
    .end local v4    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :pswitch_1
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 69
    .local v3, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 70
    .restart local v1    # "attachmentId":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 71
    .local v5, "transformString":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform;->parse(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v4

    .line 72
    .restart local v4    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    if-nez v4, :cond_0

    .line 73
    sget-object v6, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Failed to parse transform: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v5, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    .end local v3    # "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "transformString":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Detected invalid attachmentId: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v1, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    new-instance v6, Ljava/io/FileNotFoundException;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
