.class Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;
.super Ljava/lang/Object;
.source "Upgrade.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->clearMagazinesData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldMagazinesBlobStores()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$600(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    .line 193
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldMagazinesDatabases()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$700(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    .line 194
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteSyncedAssets()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$800(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$900(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "hasClearedMagazinesData"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 196
    return-void
.end method
