.class final Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "ArchiveUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->removeEditionFromArchive(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 113
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-static {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->removeEditionFromArchive(Landroid/app/Activity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 115
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 110
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil$3;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
