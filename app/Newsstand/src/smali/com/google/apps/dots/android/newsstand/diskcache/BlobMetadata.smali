.class public Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
.super Ljava/lang/Object;
.source "BlobMetadata.java"


# instance fields
.field public final eTag:Ljava/lang/String;

.field public final expiration:Ljava/lang/Long;

.field public final lastModified:Ljava/lang/Long;

.field public final readTime:J

.field public final writeTime:J


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1
    .param p1, "readTime"    # J
    .param p3, "writeTime"    # J
    .param p5, "eTag"    # Ljava/lang/String;
    .param p6, "lastModified"    # Ljava/lang/Long;
    .param p7, "expiration"    # Ljava/lang/Long;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->readTime:J

    .line 16
    iput-wide p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->writeTime:J

    .line 17
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->eTag:Ljava/lang/String;

    .line 18
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    .line 19
    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->expiration:Ljava/lang/Long;

    .line 20
    return-void
.end method
