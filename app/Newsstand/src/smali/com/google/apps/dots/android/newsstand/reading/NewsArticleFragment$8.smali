.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;
.super Ljava/lang/Object;
.source "NewsArticleFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onAttach(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    :goto_0
    return-void

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->init()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    .line 416
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadArticle()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    goto :goto_0
.end method
