.class public Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NSGCMBroadcastReceiver.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private ackGcmMessage(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "gcmMessageId"    # Ljava/lang/String;
    .param p3, "gcmRegId"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsClient()Lcom/google/apps/dots/android/newsstand/http/NSClient;

    move-result-object v0

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 139
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getGcmAckUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [B

    .line 138
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private beepIfNeeded(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$bool;->beep_on_gcm:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    sget v1, Lcom/google/android/apps/newsstanddev/R$raw;->beep:I

    invoke-static {p1, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    .line 109
    .local v0, "mp":Landroid/media/MediaPlayer;
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 110
    new-instance v1, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver$1;-><init>(Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 117
    .end local v0    # "mp":Landroid/media/MediaPlayer;
    :cond_0
    return-void
.end method

.method private lookupAccountsForUserId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backendUserId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 125
    .local v1, "allGoogleAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 126
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v2

    .line 127
    .local v2, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 128
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 131
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :goto_0
    return-object v3

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    sget-object v10, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "Received GCM intent %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;->toString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    const/4 v10, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->setResultCode(I)V

    .line 46
    invoke-direct/range {p0 .. p1}, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->beepIfNeeded(Landroid/content/Context;)V

    .line 48
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "nsPayload"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 49
    .local v8, "payload":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 50
    sget-object v10, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "Empty GCM payload"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v6

    .line 56
    .local v6, "messageBytes":[B
    :try_start_0
    invoke-static {v6}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->parseFrom([B)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;

    move-result-object v9

    .line 57
    .local v9, "syncMessage":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    sget-object v10, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "Payload: %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->toStringer(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/Object;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->getUserId()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10}, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->lookupAccountsForUserId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 60
    .local v3, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_2

    .line 61
    sget-object v10, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "Couldn\'t find Account for user"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 65
    .local v2, "account":Landroid/accounts/Account;
    iget-object v12, v9, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->operation:[I

    array-length v13, v12

    const/4 v10, 0x0

    :goto_2
    if-ge v10, v13, :cond_4

    aget v7, v12, v10

    .line 66
    .local v7, "operation":I
    new-instance v14, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {v14, v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setAccount(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v14

    const v15, 0x927c0

    .line 68
    invoke-virtual {v14, v15}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->wakeful(I)Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    .line 70
    .local v5, "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    packed-switch v7, :pswitch_data_0

    .line 65
    :cond_3
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 72
    :pswitch_0
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncMyNews()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 101
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v3    # "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .end local v5    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .end local v7    # "operation":I
    .end local v9    # "syncMessage":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    :catch_0
    move-exception v4

    .line 102
    .local v4, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    sget-object v10, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v11, "Trouble parsing GCM proto"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v4, v11, v12}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    .end local v4    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    .restart local v2    # "account":Landroid/accounts/Account;
    .restart local v3    # "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .restart local v5    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .restart local v7    # "operation":I
    .restart local v9    # "syncMessage":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
    :pswitch_1
    :try_start_1
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncMyCurations()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_3

    .line 78
    :pswitch_2
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncMyMagazines([B)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_3

    .line 81
    :pswitch_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_3

    .line 83
    new-instance v14, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;

    const-string v15, "requesting full sync on GCM"

    .line 84
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v16

    invoke-direct/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {v14}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/SyncStartEvent;->track()V

    .line 85
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->requestOneTimeSync(Landroid/accounts/Account;)V

    goto :goto_3

    .line 89
    :pswitch_4
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncConfig()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_3

    .line 97
    .end local v5    # "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .end local v7    # "operation":I
    :cond_4
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;->getMessageId()Ljava/lang/String;

    move-result-object v10

    .line 98
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v12

    const-string v13, "gcmRegistrationId"

    invoke-virtual {v12, v13}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 96
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v10, v12}, Lcom/google/apps/dots/android/newsstand/gcm/NSGCMBroadcastReceiver;->ackGcmMessage(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
