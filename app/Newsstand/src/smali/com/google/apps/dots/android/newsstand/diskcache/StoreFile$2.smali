.class Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$2;
.super Ljava/lang/Object;
.source "StoreFile.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->compact()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .prologue
    .line 945
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)I
    .locals 4
    .param p1, "a"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p2, "b"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 948
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/common/primitives/Longs;->compare(JJ)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 945
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    check-cast p2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$2;->compare(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)I

    move-result v0

    return v0
.end method
