.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$4;
.super Ljava/lang/Object;
.source "NativeBodyBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->initEventDispatcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/net/Uri;)V
    .locals 1
    .param p1, "event"    # Landroid/net/Uri;

    .prologue
    .line 216
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->isHttp(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->show(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/net/Uri;)V

    .line 219
    :cond_0
    return-void
.end method
