.class Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;
.super Ljava/lang/Object;
.source "AttachmentViewCache.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->fetchCachedBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

.field final synthetic val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 7
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->access$900(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;)Ljava/util/Map;

    move-result-object v2

    monitor-enter v2

    .line 243
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->FAILED:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$002(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .line 244
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->access$1000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v3, "Failed to load bitmap - state: %s, key: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 245
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    .line 244
    invoke-virtual {v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .line 247
    .local v0, "callback":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;->onCachedBitmapMissing()V

    goto :goto_0

    .line 249
    .end local v0    # "callback":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    return-void
.end method

.method public onSuccess(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 226
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->access$900(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;)Ljava/util/Map;

    move-result-object v2

    monitor-enter v2

    .line 227
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$502(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 228
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v3

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->hasAlpha:Z
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$702(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Z)Z

    .line 229
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$002(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .line 230
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->access$1000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v3, "Loaded bitmap - state: %s, key: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 231
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    .line 230
    invoke-virtual {v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .line 234
    .local v0, "callback":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    invoke-interface {v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;->onCachedBitmapReady(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V

    goto :goto_0

    .line 237
    .end local v0    # "callback":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 236
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->val$cachedBitmap:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->readyListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 237
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 223
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;->onSuccess(Landroid/graphics/Bitmap;)V

    return-void
.end method
