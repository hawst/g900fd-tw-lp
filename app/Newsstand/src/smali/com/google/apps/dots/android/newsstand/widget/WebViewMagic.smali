.class public Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;
.super Ljava/lang/Object;
.source "WebViewMagic.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

.field private centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

.field private enabled:Z

.field private getZoomControllerMethod:Ljava/lang/reflect/Method;

.field private mInZoomOverviewField:Ljava/lang/reflect/Field;

.field private mProviderField:Ljava/lang/reflect/Field;

.field private mZoomManagerField:Ljava/lang/reflect/Field;

.field private setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

.field private final webView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/webkit/WebView;)V
    .locals 1
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    .line 33
    return-void
.end method

.method private initCenterFitRectJB()V
    .locals 6

    .prologue
    .line 148
    :try_start_0
    const-class v1, Landroid/webkit/WebView;

    const-string v2, "mProvider"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    .line 149
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 153
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 154
    .local v0, "mProvider":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "centerFitRect"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/graphics/Rect;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    .line 159
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 162
    .end local v0    # "mProvider":Ljava/lang/Object;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private initCenterFitRectTakingInt4()V
    .locals 5

    .prologue
    .line 119
    :try_start_0
    const-class v0, Landroid/webkit/WebView;

    const-string v1, "centerFitRect"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initCenterFitRectTakingRect()V
    .locals 5

    .prologue
    .line 134
    :try_start_0
    const-class v0, Landroid/webkit/WebView;

    const-string v1, "centerFitRect"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/graphics/Rect;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    .line 135
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 138
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initGetZoomButtonsController()V
    .locals 3

    .prologue
    .line 47
    :try_start_0
    const-class v0, Landroid/webkit/WebView;

    const-string v1, "getZoomButtonsController"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->getZoomControllerMethod:Ljava/lang/reflect/Method;

    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->getZoomControllerMethod:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->getZoomControllerMethod:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initInZoomOverview()V
    .locals 2

    .prologue
    .line 171
    :try_start_0
    const-class v0, Landroid/webkit/WebView;

    const-string v1, "mInZoomOverview"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mInZoomOverviewField:Ljava/lang/reflect/Field;

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mInZoomOverviewField:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mInZoomOverviewField:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initReflectedMethods()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initGetZoomButtonsController()V

    .line 181
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initSetZoomOverviewWidthPreJB()V

    .line 182
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initSetZoomOverviewWidthJB()V

    .line 183
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initCenterFitRectTakingInt4()V

    .line 184
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initCenterFitRectTakingRect()V

    .line 185
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initCenterFitRectJB()V

    .line 186
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initInZoomOverview()V

    .line 187
    return-void
.end method

.method private initSetZoomOverviewWidthJB()V
    .locals 7

    .prologue
    .line 86
    :try_start_0
    const-class v2, Landroid/webkit/WebView;

    const-string v3, "mProvider"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    .line 87
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 91
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 92
    .local v0, "mProvider":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mZoomManager"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    .line 96
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 100
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 101
    .local v1, "zoomManager":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 105
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setZoomOverviewWidth"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    .line 106
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 109
    .end local v0    # "mProvider":Ljava/lang/Object;
    .end local v1    # "zoomManager":Ljava/lang/Object;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private initSetZoomOverviewWidthPreJB()V
    .locals 6

    .prologue
    .line 62
    :try_start_0
    const-class v1, Landroid/webkit/WebView;

    const-string v2, "mZoomManager"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    .line 63
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 67
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    .local v0, "zoomManager":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setZoomOverviewWidth"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    .line 73
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    .end local v0    # "zoomManager":Ljava/lang/Object;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static setWebSettingsProperty(Landroid/webkit/WebSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "settings"    # Landroid/webkit/WebSettings;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 279
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setProperty"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 280
    .local v1, "m":Ljava/lang/reflect/Method;
    if-nez v1, :cond_0

    .line 291
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 283
    .restart local v1    # "m":Ljava/lang/reflect/Method;
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    .line 285
    const/4 v2, 0x2

    :try_start_1
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 286
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->TAG:Ljava/lang/String;

    const-string v3, "Error on WebSettings.setProperty:\n"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 289
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method private tryGetZoomManager()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    if-nez v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 194
    :goto_0
    return-object v1

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 194
    .local v0, "mProvider":Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 36
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    if-nez v0, :cond_0

    .line 37
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->initReflectedMethods()V

    .line 39
    :cond_0
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    .line 40
    return-void
.end method

.method public tryCenterFitRect(Landroid/graphics/Rect;)Z
    .locals 9
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 249
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    if-nez v4, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v2

    .line 255
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mInZoomOverviewField:Ljava/lang/reflect/Field;

    if-eqz v4, :cond_2

    .line 256
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mInZoomOverviewField:Ljava/lang/reflect/Field;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 258
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_4

    .line 259
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    if-eqz v2, :cond_3

    .line 260
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mProviderField:Ljava/lang/reflect/Field;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 261
    .local v1, "mProvider":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .end local v1    # "mProvider":Ljava/lang/Object;
    :goto_1
    move v2, v3

    .line 274
    goto :goto_0

    .line 263
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Rect:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 271
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->TAG:Ljava/lang/String;

    const-string v4, "Error on center fit rect:\n"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 265
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_0

    .line 266
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->centerFitRectMethod_Int4:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p1, Landroid/graphics/Rect;->left:I

    .line 267
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v7, p1, Landroid/graphics/Rect;->right:I

    iget v8, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    iget v8, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 266
    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public tryDismissZoomController()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    if-nez v3, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v2

    .line 207
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->getZoomControllerMethod:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_0

    .line 210
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->getZoomControllerMethod:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->webView:Landroid/webkit/WebView;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 211
    .local v1, "zoomController":Ljava/lang/Object;
    instance-of v3, v1, Landroid/widget/ZoomButtonsController;

    if-eqz v3, :cond_2

    .line 212
    check-cast v1, Landroid/widget/ZoomButtonsController;

    .end local v1    # "zoomController":Ljava/lang/Object;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->TAG:Ljava/lang/String;

    const-string v4, "Error retrieving zoom controller:\n"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public trySetZoomOverviewWidth(I)Z
    .locals 8
    .param p1, "width"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 225
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->enabled:Z

    if-nez v4, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v2

    .line 229
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->mZoomManagerField:Ljava/lang/reflect/Field;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_0

    .line 232
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->tryGetZoomManager()Ljava/lang/Object;

    move-result-object v1

    .line 233
    .local v1, "zoomManager":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 236
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setZoomOverviewWidthMethod:Ljava/lang/reflect/Method;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 241
    goto :goto_0

    .line 237
    .end local v1    # "zoomManager":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
