.class public abstract Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter2;
.super Ljava/lang/Object;
.source "MergeList.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/data/MergeList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MergeFilter2"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract apply(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/Snapshot;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/Snapshot;",
            "Lcom/google/android/libraries/bind/data/Snapshot;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end method

.method public final varargs transform([Lcom/google/android/libraries/bind/data/Snapshot;)Ljava/util/List;
    .locals 4
    .param p1, "sources"    # [Lcom/google/android/libraries/bind/data/Snapshot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/libraries/bind/data/Snapshot;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 36
    aget-object v0, p1, v2

    aget-object v1, p1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter2;->apply(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/Snapshot;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 35
    goto :goto_0
.end method
