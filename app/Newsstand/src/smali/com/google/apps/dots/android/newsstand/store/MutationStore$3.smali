.class Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;
.super Ljava/lang/Object;
.source "MutationStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLogFile(Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$100(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Landroid/accounts/Account;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 130
    .local v0, "mutLogFile":Ljava/io/File;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->lockSpace:Lcom/google/common/cache/LoadingCache;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$700(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/common/cache/LoadingCache;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/common/cache/LoadingCache;->getUnchecked(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->STORE_MUTATION:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v2, p0, v3, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/io/File;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/store/AsyncLock;->with(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$3;->apply(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
