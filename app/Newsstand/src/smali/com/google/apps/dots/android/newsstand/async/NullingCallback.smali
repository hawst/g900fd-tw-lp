.class public abstract Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.super Ljava/lang/Object;
.source "NullingCallback.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/NullingCallback;, "Lcom/google/apps/dots/android/newsstand/async/NullingCallback<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/NullingCallback;, "Lcom/google/apps/dots/android/newsstand/async/NullingCallback<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;->onSuccess(Ljava/lang/Object;)V

    .line 17
    return-void
.end method

.method public abstract onSuccess(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
