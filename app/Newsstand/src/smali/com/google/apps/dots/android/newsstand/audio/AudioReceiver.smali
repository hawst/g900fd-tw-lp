.class public abstract Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;
.super Ljava/lang/Object;
.source "AudioReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;
    }
.end annotation


# instance fields
.field private final broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final handler:Landroid/os/Handler;

.field private registered:Z

.field private resultReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->handler:Landroid/os/Handler;

    .line 22
    new-instance v0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->registered:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected abstract onReceiveUpdate(Landroid/os/Bundle;)V
.end method

.method public register(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 35
    .local v0, "appContext":Landroid/content/Context;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->registered:Z

    if-nez v1, :cond_0

    .line 36
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->resultReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;

    .line 37
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.UPDATE_STATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 39
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->resultReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->getAudioStatus(Landroid/content/Context;Landroid/os/ResultReceiver;)V

    .line 40
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->registered:Z

    .line 42
    :cond_0
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 46
    .local v0, "appContext":Landroid/content/Context;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->registered:Z

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 48
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->registered:Z

    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 50
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->resultReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;->destroy()V

    .line 51
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->resultReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver$AudioResultReceiver;

    .line 53
    :cond_0
    return-void
.end method
