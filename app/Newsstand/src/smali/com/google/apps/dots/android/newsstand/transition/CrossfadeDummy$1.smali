.class Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;
.super Lcom/google/android/play/animation/BaseAnimatorListener;
.source "CrossfadeDummy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

.field final synthetic val$alphaAnimator:Landroid/animation/Animator;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;Landroid/animation/Animator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->this$0:Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->val$alphaAnimator:Landroid/animation/Animator;

    invoke-direct {p0}, Lcom/google/android/play/animation/BaseAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->val$alphaAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->val$alphaAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 114
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->this$0:Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    # getter for: Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeOutView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->access$000(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy$1;->this$0:Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;

    # getter for: Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->fadeOutView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;->access$000(Lcom/google/apps/dots/android/newsstand/transition/CrossfadeDummy;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    :cond_0
    return-void
.end method
