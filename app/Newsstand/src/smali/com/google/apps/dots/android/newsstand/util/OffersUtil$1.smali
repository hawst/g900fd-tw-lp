.class final Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "OffersUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->modifyOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$accept:Z

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# direct methods
.method constructor <init>(ZLcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$accept:Z

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 188
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$accept:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 189
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 190
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/OffersUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 191
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start(Z)V

    .line 193
    :cond_0
    return-void
.end method
