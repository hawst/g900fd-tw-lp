.class public Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "NewsstandAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MAGAZINE_URI:Landroid/net/Uri;

.field private static final NEWS_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 18
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    .line 19
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "play.google.com"

    .line 20
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "magazines"

    .line 21
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "reader"

    .line 22
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    .line 25
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "issue"

    .line 26
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->MAGAZINE_URI:Landroid/net/Uri;

    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "news"

    .line 30
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->NEWS_URI:Landroid/net/Uri;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 35
    return-void
.end method

.method private basicIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->basicIntent()Landroid/content/Intent;

    move-result-object v0

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 41
    const-string v1, "authAccount"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 42
    return-object v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->basicIntent()Landroid/content/Intent;

    move-result-object v0

    .line 48
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 49
    .local v1, "uri":Landroid/net/Uri;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 58
    :goto_0
    if-nez v1, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 64
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_1
    return-object v0

    .line 52
    .restart local v0    # "intent":Landroid/content/Intent;
    :pswitch_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->MAGAZINE_URI:Landroid/net/Uri;

    .line 53
    goto :goto_0

    .line 55
    :pswitch_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->NEWS_URI:Landroid/net/Uri;

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->backendDocId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 62
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Sending intent URL %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    const-string v2, "authAccount"

    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_1

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
