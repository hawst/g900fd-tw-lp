.class public Lcom/google/apps/dots/android/newsstand/http/HttpModule;
.super Ljava/lang/Object;
.source "HttpModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;
    }
.end annotation


# static fields
.field private static final CONNECT_TIMEOUT_MS:I = 0x1388

.field private static final MAX_REDIRECTS:I = 0x6

.field private static final READ_TIMEOUT_MS:I = 0x1388

.field private static final USER_AGENT_TEMPLATE:Ljava/lang/String; = "%s/%s (Linux; U; Android %s; %s-%s; %s/%s Build/%s; Density/%d; gzip) %s/%s"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/gms/http/GoogleHttpClient;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->createGoogleHttpClient()Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/http/HttpModule;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createGoogleHttpClient()Lcom/google/android/gms/http/GoogleHttpClient;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$NSGoogleHttpClient;-><init>()V

    return-object v0
.end method

.method private static getUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 53
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->user_agent:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "ua":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/shared/DeviceCategory;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "deviceCategory":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s/%s (Linux; U; Android %s; %s-%s; %s/%s Build/%s; Density/%d; gzip) %s/%s"

    const/16 v4, 0xb

    new-array v7, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v7, v4

    const/4 v4, 0x1

    .line 60
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v4, 0x2

    sget-object v8, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v8, v7, v4

    const/4 v8, 0x3

    .line 62
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    aput-object v4, v7, v8

    const/4 v8, 0x4

    .line 63
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    aput-object v4, v7, v8

    const/4 v4, 0x5

    aput-object v0, v7, v4

    const/4 v4, 0x6

    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v8, v7, v4

    const/4 v4, 0x7

    sget-object v8, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v8, v7, v4

    const/16 v4, 0x8

    .line 67
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDensityDpi()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    const/16 v4, 0x9

    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    const/16 v4, 0xa

    .line 69
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionCode(Landroid/content/Context;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    .line 57
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, "userAgent":Ljava/lang/String;
    return-object v3

    .line 62
    .end local v3    # "userAgent":Ljava/lang/String;
    :cond_0
    const-string v4, "en"

    goto :goto_0

    .line 63
    :cond_1
    const-string v4, ""

    goto :goto_1
.end method

.method public static provideHttpClient(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;",
            "Lcom/google/apps/dots/android/newsstand/preference/Preferences;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$2;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static provideWebviewHttpClient(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/http/HttpModule$1;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
