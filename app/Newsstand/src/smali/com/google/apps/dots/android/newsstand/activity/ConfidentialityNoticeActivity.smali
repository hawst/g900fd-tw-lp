.class public Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.source "ConfidentialityNoticeActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;-><init>(Z)V

    .line 33
    return-void
.end method

.method public static shouldShow()Z
    .locals 8

    .prologue
    .line 55
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    .line 56
    .local v2, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    const-string v3, "confidentialityAcknowledgedTime"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 57
    .local v0, "lastTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/32 v6, 0x240c8400

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected createView()Landroid/view/View;
    .locals 7

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 62
    .local v1, "inflater":Landroid/view/LayoutInflater;
    sget v5, Lcom/google/android/apps/newsstanddev/R$layout;->confidentiality_notice_activity:I

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 64
    .local v4, "view":Landroid/view/View;
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->accept_button:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    .local v0, "acceptButton":Landroid/widget/Button;
    const-string v5, "Acknowledge"

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 66
    new-instance v5, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity$1;-><init>(Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->title_textbox:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 76
    .local v3, "reminderTitle":Landroid/widget/TextView;
    const-string v5, "Google Confidential"

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->content_textbox:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 79
    .local v2, "reminderContent":Landroid/widget/TextView;
    const-string v5, "IMPORTANT: Access to this app is restricted to Googlers only. \nDo NOT share this app nor its contents (e.g., screenshots) with anyone.\n\nBy downloading, installing, and/or using the app, you:\n\n1. Agree to maintain the confidentiality of the app; this includes:\n    a. NOT forwarding or in any way sharing the specifics of the app with anyone\n    b. NOT using the app in areas where external users (i.e., non-Googlers and non-family members) may view the app\n\n2. Agree that access and use of the app may be monitored via your username or other identifiers\n\n\nNote: You will be shown this screen once a week."

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-object v4
.end method

.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->setResult(I)V

    .line 103
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->finish()V

    .line 104
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->createView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->setContentView(Landroid/view/View;)V

    .line 45
    return-void
.end method

.method protected setShown()V
    .locals 4

    .prologue
    .line 96
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "confidentialityAcknowledgedTime"

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 96
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 98
    return-void
.end method
