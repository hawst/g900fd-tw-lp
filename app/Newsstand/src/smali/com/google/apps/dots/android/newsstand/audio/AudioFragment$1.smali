.class Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;
.super Ljava/lang/Object;
.source "AudioFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->showOnScrollEnabled()Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    .line 100
    :goto_0
    return v1

    .line 55
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 56
    .local v0, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 58
    :pswitch_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->NEUTRAL:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$202(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    .line 59
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupScroll:Z
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$302(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    .line 60
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    goto :goto_0

    .line 63
    :pswitch_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupScroll:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 64
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F
    invoke-static {v3, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$502(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F

    .line 65
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->setupScroll:Z
    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$302(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    .line 68
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->UP:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    if-eq v3, v4, :cond_3

    .line 69
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollUpY:F
    invoke-static {v3, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$602(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F

    .line 70
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->UP:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$202(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    .line 71
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    .line 73
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    if-eq v3, v4, :cond_4

    .line 74
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollDownY:F
    invoke-static {v3, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$702(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F

    .line 75
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$202(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    .line 76
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    .line 79
    :cond_4
    sget-object v3, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$3;->$SwitchMap$com$google$apps$dots$android$newsstand$audio$AudioFragment$ScrollDirection:[I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->scrollDirection:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$ScrollDirection;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 97
    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->lastScrollY:F
    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$502(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;F)F

    goto/16 :goto_0

    .line 81
    :pswitch_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollUpY:F
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->minScrollToToggleControlsDp:F
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v4

    sub-float/2addr v3, v4

    cmpg-float v3, v0, v3

    if-gez v3, :cond_5

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 82
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hide()V

    .line 83
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    goto :goto_1

    .line 87
    :pswitch_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->firstScrollDownY:F
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->minScrollToToggleControlsDp:F
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)F

    move-result v4

    add-float/2addr v3, v4

    cmpl-float v3, v0, v3

    if-lez v3, :cond_5

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 88
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->audioControlBar:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;)Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show()V

    .line 89
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->resetScrollDirection:Z
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;->access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;Z)Z

    goto :goto_1

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 79
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
