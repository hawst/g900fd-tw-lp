.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "TitleIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

.field private calendar:Ljava/util/Calendar;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

.field private final onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

.field private title:Ljava/lang/String;

.field private final titleAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    const-string v1, "TitleIssuesFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->title_issues_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->title:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->titleAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->title:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->calendar:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private setupAdapter()V
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 133
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->calendar:Ljava/util/Calendar;

    .line 134
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 126
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 127
    return-void
.end method

.method private updateAdapter()V
    .locals 13

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    iget-object v0, v9, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    .line 161
    .local v0, "appFamilyId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->titleAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 162
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->titleAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v8

    .line 163
    .local v8, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v9

    invoke-virtual {v9, v8, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    new-instance v10, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$2;

    invoke-direct {v10, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)V

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 173
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    move-result-object v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->onDeviceFilter:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    .line 174
    invoke-virtual {v9, v10}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 175
    invoke-virtual {v9, v10}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    .line 177
    .local v4, "filteredList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v4, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->filterForAppFamily(Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    .line 178
    .local v3, "dateFilteredAppFamilyList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->magazineTileFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;

    invoke-virtual {v3, v9}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v5

    .line 180
    .local v5, "issueCardList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;

    invoke-direct {v6, p0, v5}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 222
    .local v6, "issueGroup":Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 223
    .local v7, "numCols":I
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 227
    new-instance v9, Lcom/google/android/libraries/bind/data/StaticDataProvider;

    .line 228
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    sget v12, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_offline:I

    .line 227
    invoke-static {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/libraries/bind/data/StaticDataProvider;-><init>(Lcom/google/android/libraries/bind/data/Data;)V

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->setEmptyRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 231
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v2, v9}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 232
    .local v2, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-virtual {v2, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 234
    sget-object v9, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v2, v9}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 236
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 237
    .local v1, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v9, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 239
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_0

    .line 241
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    const/4 v10, 0x2

    new-instance v11, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$4;

    invoke-direct {v11, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)V

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnChildrenAddedListener(ILjava/lang/Runnable;)V

    .line 248
    :cond_0
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 266
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$5;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    iget-object v0, v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    .line 253
    .local v0, "appFamily":Ljava/lang/String;
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 254
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "help_context_key"

    const-string v3, "mobile_magazine_source"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v2, "appFamilyId"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return-object v1
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 79
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->setupHeaderListLayout()V

    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 81
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->setupAdapter()V

    .line 82
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;)V
    .locals 5
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 138
    if-nez p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    if-eqz p2, :cond_2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    .line 142
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    :cond_2
    move v0, v2

    .line 143
    .local v0, "appFamilyChanged":Z
    :goto_1
    if-eqz p2, :cond_3

    iget-boolean v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    iget-boolean v4, p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    if-eq v3, v4, :cond_4

    :cond_3
    move v1, v2

    .line 146
    .local v1, "filterChanged":Z
    :cond_4
    if-eqz v0, :cond_5

    .line 147
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->appFamilyId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->clearAppFamilyFromNotifications(Landroid/content/Context;Ljava/lang/String;)V

    .line 149
    :cond_5
    if-eqz v1, :cond_6

    .line 150
    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    iget-boolean v3, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragmentState;->hideArchive:Z

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;-><init>(Z)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->archiveFilter:Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;

    .line 152
    :cond_6
    if-nez v1, :cond_7

    if-eqz v0, :cond_0

    .line 153
    :cond_7
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->updateAdapter()V

    goto :goto_0

    .end local v0    # "appFamilyChanged":Z
    .end local v1    # "filterChanged":Z
    :cond_8
    move v0, v1

    .line 142
    goto :goto_1
.end method
