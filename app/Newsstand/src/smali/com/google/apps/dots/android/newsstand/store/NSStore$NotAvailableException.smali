.class public Lcom/google/apps/dots/android/newsstand/store/NSStore$NotAvailableException;
.super Ljava/io/IOException;
.source "NSStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotAvailableException"
.end annotation


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 65
    return-void
.end method
