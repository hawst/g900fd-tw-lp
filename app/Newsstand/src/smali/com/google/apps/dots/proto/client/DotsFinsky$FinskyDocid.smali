.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FinskyDocid"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;


# instance fields
.field public backend:I

.field public backendDocid:Ljava/lang/String;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 13
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    .line 16
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    .locals 3

    .prologue
    .line 33
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    return-object v0

    .line 34
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    :catch_0
    move-exception v1

    .line 35
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    .line 45
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 47
    goto :goto_0

    .line 45
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    .line 47
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    .local v0, "size":I
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    .line 70
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    .line 72
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->cachedSize:I

    .line 76
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 51
    const/16 v0, 0x11

    .line 52
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 53
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    add-int v0, v1, v2

    .line 54
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    add-int v0, v1, v2

    .line 55
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 56
    return v0

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 85
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 89
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 95
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    goto :goto_0

    .line 99
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    goto :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    goto :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 62
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 63
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocid;->backendDocid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 64
    return-void
.end method
