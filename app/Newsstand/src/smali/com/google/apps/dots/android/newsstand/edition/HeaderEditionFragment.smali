.class public Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;
.super Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
.source "HeaderEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private connectivityListener:Ljava/lang/Runnable;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

.field private final keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

.field private final shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

.field private subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

.field private final translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "HeaderEditionFragment_state"

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;-><init>(Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    .line 69
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    .line 71
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateEditionSummary()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateHeroView()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateActionBarTitle()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateShareParams()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method private edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 208
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method

.method private isSubscribed()Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateActionBarTitle()V
    .locals 4

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 381
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    .line 382
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTitleHint()Ljava/lang/String;

    move-result-object v2

    .line 381
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->updateContentDescriptionForActivityContentView(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 386
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 387
    .local v0, "appContext":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 402
    return-void
.end method

.method private updateControls()V
    .locals 5

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 264
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 263
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 265
    return-void
.end method

.method private updateEditionSummary()V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 269
    return-void
.end method

.method private updateHeroView()V
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 374
    return-void
.end method

.method private updateShareParams()V
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 330
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 338
    return-void
.end method

.method private updateTabState()V
    .locals 3

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->showTabs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 293
    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getHeaderHeight()I

    move-result v2

    .line 292
    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setTabMode(II)V

    .line 298
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 296
    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getHeaderHeight()I

    move-result v2

    .line 295
    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setTabMode(II)V

    goto :goto_0
.end method


# virtual methods
.method protected alwaysShowActionBarText()Z
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x1

    return v0
.end method

.method protected currentPageIndex()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 223
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 224
    .local v1, "sectionEdition":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    if-nez v1, :cond_1

    move v0, v2

    .line 232
    :cond_0
    :goto_0
    return v0

    .line 227
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 228
    .local v0, "logicalPosition":I
    if-ne v0, v3, :cond_0

    .line 229
    sget-object v4, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Unable to find page for section edition: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    .line 230
    goto :goto_0
.end method

.method protected dataListId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Object;
    .locals 2
    .param p1, "pageEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 215
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_0

    .line 216
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p1    # "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    .restart local p1    # "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 304
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 303
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method protected getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 314
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 131
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->plain_edition_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 133
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onDestroyView()V

    .line 102
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onDestroyView()V

    .line 103
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 104
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onDestroyView()V

    .line 105
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 165
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_add_edition:I

    if-ne v2, v3, :cond_1

    .line 166
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->account()Landroid/accounts/Account;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 168
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_remove_edition:I

    if-ne v2, v3, :cond_2

    .line 169
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->account()Landroid/accounts/Account;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->unsubscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 173
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_refresh:I

    if-ne v2, v3, :cond_3

    .line 174
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->refreshCurrentEditionWithSpinner()V

    goto :goto_0

    .line 176
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_mini_cards:I

    if-ne v2, v3, :cond_4

    .line 177
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->toggleCompactMode()Z

    move-result v0

    .line 178
    .local v0, "isEnabled":Z
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 180
    .end local v0    # "isEnabled":Z
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_7

    .line 181
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 183
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->isSubscribed()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 184
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 185
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    .line 190
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    goto :goto_0

    .line 187
    :cond_6
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    goto :goto_1

    .line 193
    :cond_7
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method protected onPageListChanged()V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateTabState()V

    .line 288
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 143
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 144
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 146
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 147
    .local v0, "sectionEdition":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_refresh:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v0, :cond_2

    .line 148
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->supportsRefresh()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 147
    :goto_1
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 149
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_mini_cards:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->useCompactMode()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v1

    .line 150
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isCompactModeAvailable()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 152
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$2;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;Landroid/view/Menu;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 148
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onViewCreated(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->image_header_logo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->round_topic_header_logo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    .line 89
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onViewCreated()V

    .line 90
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .line 91
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 97
    return-void
.end method

.method protected pageFragment(I)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 237
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/HostedPlainEditionFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/HostedPlainEditionFragment;-><init>()V

    .line 238
    .local v0, "plainEditionFragment":Lcom/google/apps/dots/android/newsstand/edition/HostedPlainEditionFragment;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_ID:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 239
    .local v1, "sectionId":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .line 240
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 239
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/HostedPlainEditionFragment;->setInitialState(Landroid/os/Parcelable;)V

    .line 241
    return-object v0
.end method

.method protected pageList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 3

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->sectionList(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$3;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    instance-of v1, p2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 110
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    .line 111
    .local v0, "fragmentState":Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v3, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 112
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;

    invoke-direct {v3, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V

    .line 113
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    .line 111
    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 115
    .end local v0    # "fragmentState":Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected showTabs()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 412
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v1, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    move-result-object v0

    return-object v0
.end method

.method protected stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    .locals 5
    .param p1, "logicalPosition"    # I

    .prologue
    .line 422
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 425
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 427
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_ID:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    .line 428
    .local v2, "sectionId":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 430
    .end local v1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "sectionId":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected updateErrorView()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->refreshErrorView()Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 310
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V

    return-void
.end method

.method protected bridge synthetic updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    .prologue
    .line 247
    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 248
    .local v0, "editionChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 249
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 250
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateEditionSummary()V

    .line 251
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updatePageList()V

    .line 252
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateHeroView()V

    .line 253
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateActionBarTitle()V

    .line 254
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateControls()V

    .line 255
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateShareParams()V

    .line 256
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 257
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateErrorView()V

    .line 259
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V

    .line 260
    return-void

    .line 247
    .end local v0    # "editionChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
