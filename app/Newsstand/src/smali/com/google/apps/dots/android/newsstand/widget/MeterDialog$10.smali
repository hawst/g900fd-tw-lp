.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getInAppClickHandler(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field final synthetic val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

.field final synthetic val$offerType:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 599
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offerType:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/16 v7, 0x66

    const/4 v8, 0x0

    .line 602
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isOffline:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$500(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 603
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->wait_until_online:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 604
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 647
    :goto_0
    return-void

    .line 607
    :cond_0
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->offer_line:I

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 608
    .local v2, "offerLine":Landroid/view/View;
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->offer_line_progress:I

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 609
    .local v3, "progress":Landroid/view/View;
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 610
    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 611
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    move-object v1, p2

    .line 613
    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 614
    .local v1, "nsActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;

    invoke-direct {v5, p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v7, v5}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setResultHandlerForActivityCode(ILcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;)V

    .line 630
    const/4 v4, 0x0

    .line 631
    .local v4, "provider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    instance-of v5, p2, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    if-eqz v5, :cond_2

    move-object v5, p2

    .line 632
    check-cast v5, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    move-result-object v4

    .line 635
    :cond_2
    new-instance v5, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    invoke-direct {v5, p2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;)V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 636
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getBackendId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendId(I)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 637
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getDocType()I

    move-result v6

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/DocType;->forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 638
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getBackendDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offer:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 639
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getFullDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setFullDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->val$offerType:I

    .line 640
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->setOfferType(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    move-result-object v5

    .line 641
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 644
    .local v0, "inAppPurchaseIntent":Landroid/content/Intent;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredPaymentFlowScreen;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$10;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredPaymentFlowScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredPaymentFlowScreen;->track(Z)V

    .line 646
    invoke-virtual {v1, v0, v7}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
