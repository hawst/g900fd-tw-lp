.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

.field final synthetic val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

.field final synthetic val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/concurrent/Executor;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;->val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;->val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 212
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_BUTTON_TEXT:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    sget v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    sget v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 215
    .local v0, "shelfType":I
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 216
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;->val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 222
    .end local v0    # "shelfType":I
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 217
    .restart local v0    # "shelfType":I
    :cond_1
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 218
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$5;->val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method
