.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
.super Ljava/lang/Object;
.source "AttachmentViewCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReadyListener"
.end annotation


# virtual methods
.method public abstract onCachedBitmapMissing()V
.end method

.method public abstract onCachedBitmapReady(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
.end method
