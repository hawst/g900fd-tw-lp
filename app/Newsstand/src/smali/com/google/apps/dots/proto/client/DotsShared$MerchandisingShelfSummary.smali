.class public final Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MerchandisingShelfSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;


# instance fields
.field private bitField0_:I

.field private storePath_:Ljava/lang/String;

.field private subtitle_:Ljava/lang/String;

.field private title_:Ljava/lang/String;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24799
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24800
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 24815
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    .line 24834
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    .line 24856
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    .line 24878
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    .line 24800
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    .locals 3

    .prologue
    .line 24913
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24917
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    return-object v0

    .line 24914
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    :catch_0
    move-exception v1

    .line 24915
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24796
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24922
    if-ne p1, p0, :cond_1

    .line 24928
    :cond_0
    :goto_0
    return v1

    .line 24923
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 24924
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .line 24925
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24926
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 24927
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 24928
    goto :goto_0

    .line 24925
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    .line 24926
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    .line 24927
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    .line 24928
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 24959
    const/4 v0, 0x0

    .line 24960
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 24961
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    .line 24962
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24964
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 24965
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    .line 24966
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24968
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 24969
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    .line 24970
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24972
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 24973
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    .line 24974
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24976
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->cachedSize:I

    .line 24977
    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24858
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24836
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 24817
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    return v0
.end method

.method public hasSubtitle()Z
    .locals 1

    .prologue
    .line 24869
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 1

    .prologue
    .line 24847
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 24932
    const/16 v0, 0x11

    .line 24933
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 24934
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    add-int v0, v1, v3

    .line 24935
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 24936
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 24937
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 24938
    return v0

    .line 24935
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 24936
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 24937
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24985
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 24986
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 24990
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 24991
    :sswitch_0
    return-object p0

    .line 24996
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 24997
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 25004
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    .line 25005
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    goto :goto_0

    .line 25007
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    goto :goto_0

    .line 25012
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    .line 25013
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    goto :goto_0

    .line 25017
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    .line 25018
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    goto :goto_0

    .line 25022
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    .line 25023
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    goto :goto_0

    .line 24986
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24796
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24943
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 24944
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 24946
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 24947
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24949
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 24950
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->subtitle_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24952
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 24953
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->storePath_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 24955
    :cond_3
    return-void
.end method
