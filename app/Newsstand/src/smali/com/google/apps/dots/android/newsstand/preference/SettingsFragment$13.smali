.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

.field final synthetic val$customGucUrlPref:Landroid/preference/EditTextPreference;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/EditTextPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;->val$customGucUrlPref:Landroid/preference/EditTextPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 436
    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Changing GUC URL to: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 437
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setCustomGucUrl(Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;->val$customGucUrlPref:Landroid/preference/EditTextPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # setter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->processRestartRequired:Z
    invoke-static {v0, v4}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$302(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Z)Z

    .line 440
    return v3
.end method
