.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "MagazineReadingFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LiteModeTransitionDelegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1354
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;

    .prologue
    .line 1354
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;-><init>()V

    return-void
.end method

.method private liteModeSharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Z)Landroid/transition/Transition;
    .locals 3
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "isEntering"    # Z

    .prologue
    .line 1376
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->intrinsic_card_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1378
    .local v0, "inset":I
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v0, p2}, Lcom/google/apps/dots/android/newsstand/transition/CrossFadeExpandoHelper;->sharedElementEnterTransition(Landroid/view/View;IZ)Landroid/transition/Transition;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 1366
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->liteModeSharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Z)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 1371
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->liteModeSharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Z)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 1417
    const/4 v0, 0x2

    return v0
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 1412
    const-wide/16 v0, 0x190

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1385
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1386
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 1406
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setVisibility(I)V

    .line 1407
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1408
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 1391
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 1392
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setVisibility(I)V

    .line 1393
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 1398
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 1399
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setVisibility(I)V

    .line 1400
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1401
    return-void
.end method

.method protected bridge synthetic onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->onSharedElementReturnTransitionStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 1359
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->magazine_reading_activity_hero:I

    .line 1360
    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 1361
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1353
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    return-void
.end method
