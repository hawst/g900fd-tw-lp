.class Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;
.super Ljava/lang/Object;
.source "MagazinesUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MagazineWidgetInfo"
.end annotation


# instance fields
.field public final appId:Ljava/lang/String;

.field public final coverAttachmentId:Ljava/lang/String;

.field public final offerId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "coverAttachmentId"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "coverAttachmentId"    # Ljava/lang/String;
    .param p3, "offerId"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->appId:Ljava/lang/String;

    .line 135
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->coverAttachmentId:Ljava/lang/String;

    .line 136
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->offerId:Ljava/lang/String;

    .line 137
    return-void
.end method
