.class public Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.source "CardTopicListItem.java"


# static fields
.field public static final DK_IS_READ:I

.field public static final DK_PRIMARY_IMAGE:I

.field public static final DK_SOURCE_NAME:I

.field public static final DK_STORY_ON_CLICK_LISTENER:I

.field public static final DK_TITLE:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I

.field public static final LAYOUTS_COMPACT:[I

.field public static final LAYOUT_SELECTOR:Lcom/google/android/libraries/bind/data/DataProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/data/DataProperty",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicListItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_TITLE:I

    .line 19
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicListItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_SOURCE_NAME:I

    .line 21
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicListItem_storyOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_STORY_ON_CLICK_LISTENER:I

    .line 23
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicListItem_primaryImage:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_PRIMARY_IMAGE:I

    .line 25
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardTopicListItem_isRead:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_IS_READ:I

    .line 27
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->LAYOUT_SELECTOR:Lcom/google/android/libraries/bind/data/DataProperty;

    .line 34
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->EQUALITY_FIELDS:[I

    .line 38
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->LAYOUTS:[I

    .line 42
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item_compact:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->LAYOUTS_COMPACT:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method
