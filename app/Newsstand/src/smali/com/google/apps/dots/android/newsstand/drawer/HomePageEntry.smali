.class public Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;
.super Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
.source "HomePageEntry.java"


# instance fields
.field private final activeIconResId:I

.field public final homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

.field private final iconResId:I

.field private final showDownloadedOnly:Z


# direct methods
.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;IIZ)V
    .locals 1
    .param p1, "homePage"    # Lcom/google/apps/dots/android/newsstand/home/HomePage;
    .param p2, "iconResId"    # I
    .param p3, "activeIconResId"    # I
    .param p4, "showDownloadedOnly"    # Z

    .prologue
    .line 25
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->HOME_PAGE:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;)V

    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 27
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->iconResId:I

    .line 28
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->activeIconResId:I

    .line 29
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->showDownloadedOnly:Z

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 88
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 89
    check-cast v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 90
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 92
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getActiveIconResId()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->activeIconResId:I

    return v0
.end method

.method public getActiveTextColorResId()I
    .locals 1

    .prologue
    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material_dark:I

    return v0
.end method

.method public getIconResId()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->iconResId:I

    return v0
.end method

.method public getPrimaryAction(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .locals 8
    .param p1, "active"    # Z
    .param p2, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->getIconResId()I

    move-result v2

    .line 35
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->getActiveIconResId()I

    move-result v3

    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->getActiveTextColorResId()I

    move-result v4

    const/4 v6, 0x1

    new-instance v7, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry$1;

    invoke-direct {v7, p0, p2}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry$1;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    move v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->hashCode()I

    move-result v0

    return v0
.end method

.method public onClick(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    .line 57
    return-void
.end method

.method public showDownloadedOnly()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->showDownloadedOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 102
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{type: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
