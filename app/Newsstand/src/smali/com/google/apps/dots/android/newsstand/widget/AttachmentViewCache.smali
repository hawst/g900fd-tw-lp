.class public Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;
.super Ljava/lang/Object;
.source "AttachmentViewCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;,
        Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field private final inUseBitmaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;)V
    .locals 1
    .param p1, "bitmapPool"    # Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    .line 95
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 96
    return-void
.end method

.method static synthetic access$1000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    return-object v0
.end method

.method private createCachedBitmap(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .locals 2
    .param p1, "cacheKey"    # Ljava/lang/Object;

    .prologue
    .line 213
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;-><init>(Ljava/lang/Object;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;)V

    .line 214
    .local v0, "cachedBitmap":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    return-object v0
.end method

.method private evictBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
    .locals 6
    .param p1, "cachedBitmap"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$200(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 200
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroy()V

    .line 202
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$500(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v3

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$500(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 204
    const/4 v0, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$502(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 205
    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->hasAlpha:Z
    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$702(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Z)Z

    .line 207
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->EVICTED:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$002(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .line 208
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Evicted bitmap - state: %s, key: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    move-result-object v5

    aput-object v5, v4, v2

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    return-void

    :cond_1
    move v0, v2

    .line 199
    goto :goto_0
.end method

.method private fetchCachedBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V
    .locals 9
    .param p1, "cachedBitmap"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p2, "attachmentId"    # Ljava/lang/String;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p4, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 220
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Loading bitmap - state: %s, key: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    move-result-object v8

    aput-object v8, v7, v4

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {p1, v5}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$002(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    .line 223
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V

    .line 253
    .local v1, "bitmapCallback":Lcom/google/common/util/concurrent/FutureCallback;, "Lcom/google/common/util/concurrent/FutureCallback<Landroid/graphics/Bitmap;>;"
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->cacheKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$600(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getCachedBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 254
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 256
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 257
    invoke-interface {v1, v0}, Lcom/google/common/util/concurrent/FutureCallback;->onSuccess(Ljava/lang/Object;)V

    .line 265
    :goto_1
    return-void

    :cond_0
    move v3, v4

    .line 256
    goto :goto_0

    .line 260
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v3

    .line 261
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    .line 260
    invoke-virtual {v3, v4, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 262
    .local v2, "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 263
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$400(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method

.method private findBestKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Ljava/lang/Object;
    .locals 11
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p3, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    const/4 v10, 0x1

    .line 109
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->makeKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Ljava/lang/Object;

    move-result-object v4

    .line 110
    .local v4, "key":Ljava/lang/Object;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v5, v4}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->isInCache(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 131
    .end local v4    # "key":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v4

    .line 113
    .restart local v4    # "key":Ljava/lang/Object;
    :cond_1
    iget v5, p2, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-lez v5, :cond_0

    iget v5, p2, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-lez v5, :cond_0

    .line 115
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>(Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 116
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    const/4 v3, -0x1

    .local v3, "dw":I
    :goto_1
    if-gt v3, v10, :cond_0

    .line 117
    iget v5, p2, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    add-int/2addr v5, v3

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 118
    const/4 v2, -0x1

    .local v2, "dh":I
    :goto_2
    if-gt v2, v10, :cond_5

    .line 119
    if-nez v3, :cond_3

    if-nez v2, :cond_3

    .line 118
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 122
    :cond_3
    iget v5, p2, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    add-int/2addr v5, v2

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 123
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v5

    invoke-direct {p0, p1, v5, p3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->makeKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Ljava/lang/Object;

    move-result-object v1

    .line 124
    .local v1, "candidate":Ljava/lang/Object;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->isInCache(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 125
    :cond_4
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Found better key with dw: %d, dh: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v1

    .line 126
    goto :goto_0

    .line 116
    .end local v1    # "candidate":Ljava/lang/Object;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private makeKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Ljava/lang/Object;
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p3, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .prologue
    .line 99
    invoke-static {p1, p2, p3}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBitmap(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .locals 5
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;
    .param p3, "decodeOptions"    # Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    .param p4, "listener"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 144
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "cacheKey":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    monitor-enter v3

    .line 147
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->findBestKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Ljava/lang/Object;

    move-result-object v0

    .line 148
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    .line 149
    .local v1, "cachedBitmap":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    if-nez v1, :cond_0

    .line 150
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->createCachedBitmap(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;

    move-result-object v1

    .line 152
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$2;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$AttachmentViewCache$CachedBitmap$LoadState:[I

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->loadState:Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap$LoadState;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 174
    :cond_1
    :goto_0
    # operator++ for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$208(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I

    .line 175
    monitor-exit v3

    return-object v1

    .line 155
    :pswitch_0
    if-eqz p4, :cond_2

    .line 156
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->registerReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    invoke-static {v1, p4}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    .line 159
    :cond_2
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->fetchCachedBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V

    goto :goto_0

    .line 176
    .end local v0    # "cacheKey":Ljava/lang/Object;
    .end local v1    # "cachedBitmap":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 162
    .restart local v0    # "cacheKey":Ljava/lang/Object;
    .restart local v1    # "cachedBitmap":Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    :pswitch_1
    if-eqz p4, :cond_1

    .line 163
    :try_start_1
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->registerReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    invoke-static {v1, p4}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$100(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    goto :goto_0

    .line 167
    :pswitch_2
    if-eqz p4, :cond_1

    .line 168
    invoke-interface {p4, v1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;->onCachedBitmapReady(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public releaseBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    .locals 2
    .param p1, "cachedBitmap"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;
    .param p2, "listener"    # Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->inUseBitmaps:Ljava/util/Map;

    monitor-enter v1

    .line 187
    :try_start_0
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$200(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 188
    if-eqz p2, :cond_0

    .line 189
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->clearReadyListener(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$300(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$ReadyListener;)V

    .line 191
    :cond_0
    # operator-- for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$210(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I

    .line 192
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->refCount:I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;->access$200(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)I

    move-result v0

    if-nez v0, :cond_1

    .line 193
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache;->evictBitmap(Lcom/google/apps/dots/android/newsstand/widget/AttachmentViewCache$CachedBitmap;)V

    .line 195
    :cond_1
    monitor-exit v1

    .line 196
    return-void

    .line 187
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
