.class Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;
.super Ljava/lang/Object;
.source "WebDataProvidelet.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        "Landroid/content/res/AssetFileDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;

.field final synthetic val$fieldId:Ljava/lang/String;

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$sectionId:Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->this$0:Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$fieldId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$path:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$uri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$sectionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 64
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$fieldId:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v1

    .line 65
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getInlineFrame()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v3

    iget-object v5, v3, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v6, v5

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v2, v5, v3

    .line 67
    .local v2, "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$path:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->getUri()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 68
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "attachmentId":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v5, "Mapping URI %s to attachment %s"

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$uri:Landroid/net/Uri;

    aput-object v7, v6, v4

    aput-object v0, v6, v9

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    # invokes: Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->getAttachmentFD(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->access$100(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 78
    .end local v2    # "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :goto_1
    return-object v3

    .line 66
    .end local v0    # "attachmentId":Ljava/lang/String;
    .restart local v2    # "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 76
    .end local v2    # "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$sectionId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$path:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->hashString(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "-"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    .restart local v0    # "attachmentId":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v5, "Mapping URI %s to attachment %s"

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->val$uri:Landroid/net/Uri;

    aput-object v7, v6, v4

    aput-object v0, v6, v9

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    # invokes: Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->getAttachmentFD(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->access$100(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
