.class Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "PlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateShareParams()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 440
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v1

    .line 439
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 441
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 436
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
