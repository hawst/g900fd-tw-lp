.class Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;
.super Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;
.source "ViewPropertyTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ColorFilterTransition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Z)V
    .locals 1
    .param p1, "originValue"    # Ljava/lang/Integer;
    .param p2, "endValue"    # Ljava/lang/Integer;
    .param p3, "isEntering"    # Z

    .prologue
    .line 93
    new-instance v0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;-><init>(Ljava/lang/Integer;)V

    invoke-direct {p0, v0, p1, p3}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;Ljava/lang/Object;Z)V

    .line 110
    return-void
.end method


# virtual methods
.method protected createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/animation/Animator;
    .locals 3
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "startValue"    # Ljava/lang/Integer;
    .param p4, "endValue"    # Ljava/lang/Integer;

    .prologue
    .line 115
    move-object v1, p2

    check-cast v1, Landroid/widget/ImageView;

    .line 116
    .local v1, "imageView":Landroid/widget/ImageView;
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 117
    .local v0, "colorFilter":Landroid/animation/ValueAnimator;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;

    invoke-direct {v2, p0, p3, p4, v1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 132
    return-object v0

    .line 116
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected bridge synthetic createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 90
    check-cast p3, Ljava/lang/Integer;

    check-cast p4, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;->createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method
