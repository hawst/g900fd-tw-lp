.class public Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private articleFractionToRestoreTo:F

.field articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

.field audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

.field private bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

.field private connectivityListener:Ljava/lang/Runnable;

.field errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

.field private fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field private hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

.field private lastLoadFailedThrowable:Ljava/lang/Throwable;

.field loadingView:Landroid/view/View;

.field meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field newsArticleParentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

.field overlayView:Landroid/view/View;

.field private renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

.field private renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final retryRunnable:Ljava/lang/Runnable;

.field private rootView:Landroid/view/View;

.field private final shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

.field private final translateMenuHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

.field private webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 116
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->setStrictModeLimits(Ljava/lang/Class;I)V

    .line 117
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x0

    const-string v1, "NewsArticleFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_article_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    .line 97
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->translateMenuHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    .line 105
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->retryRunnable:Ljava/lang/Runnable;

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setUserVisibleHint(Z)V

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->init()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadArticle()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    return-object v0
.end method

.method static synthetic access$1100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->account()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->translateMenuHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->account()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->account()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupRenderSource()V

    .line 206
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupWebView()V

    .line 207
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupArticleTail()V

    .line 208
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupMenus()V

    .line 209
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setupMeterDialog()V

    .line 210
    return-void
.end method

.method private loadArticle()V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadArticle()V

    .line 334
    return-void
.end method

.method private markPostAsReadIfNeeded()V
    .locals 3

    .prologue
    .line 372
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isLoadComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 377
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 389
    .end local v0    # "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    :cond_0
    return-void
.end method

.method private sendMeterDialogAnalyticsEventIfNecessary()V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isSetup()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredDialogShownEvent;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getPostId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredDialogShownEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 396
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredDialogShownEvent;->track(Z)V

    .line 399
    :cond_0
    return-void
.end method

.method private setupArticleTail()V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->newsArticleParentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->setup(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/reading/RenderSource;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 281
    return-void
.end method

.method private setupMenus()V
    .locals 4

    .prologue
    .line 285
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onDestroyView()V

    .line 287
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    .line 289
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    .line 292
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 293
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    const/4 v1, 0x2

    new-array v2, v1, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 295
    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x1

    .line 296
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    aput-object v1, v2, v3

    .line 294
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    .line 293
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 309
    return-void
.end method

.method private setupMeterDialog()V
    .locals 3

    .prologue
    .line 314
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 315
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 330
    return-void
.end method

.method private setupRenderSource()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 214
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 216
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 217
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 223
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 226
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->setResourceLoadFailedListener(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;)V

    .line 234
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 235
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 236
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->optPostIndex:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSourceScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    .line 235
    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$RenderSourceFactory;->newsRenderSource(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 237
    return-void

    .end local v0    # "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    :cond_1
    move v1, v2

    .line 219
    goto :goto_0
.end method

.method private setupWebView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 240
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setVisibility(I)V

    .line 243
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->startDestroy()V

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 251
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 253
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setRenderSource(Lcom/google/apps/dots/android/newsstand/reading/RenderSource;)V

    .line 254
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$NewsRenderDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$1;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setRenderDelegate(Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;)V

    .line 255
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadBaseHtml(Landroid/accounts/Account;)V

    .line 258
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 259
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 260
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setVisibility(I)V

    .line 261
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadingView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->newsArticleParentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setupNewsArticleTail(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    .line 267
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->rootView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v1, v2, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 271
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getUserVisibleHint()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setUserVisibleHint(Z)V

    .line 273
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->articleFractionToRestoreTo:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->articleFractionToRestoreTo:F

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setInitialPageFraction(F)V

    .line 275
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->articleFractionToRestoreTo:F

    .line 277
    :cond_1
    return-void
.end method


# virtual methods
.method protected getDisableA11yMode()I
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x2

    return v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 559
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 560
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "help_context_key"

    const-string v5, "mobile_news_object"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v5, "editionInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v5, "editionPostId"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    if-eqz v4, :cond_0

    .line 567
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 568
    .local v2, "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 570
    :try_start_0
    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 571
    .local v1, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    .line 572
    .local v3, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    if-eqz v3, :cond_0

    .line 573
    const-string v4, "shareUrl"

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getShareUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    const-string v4, "article_title"

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 583
    .end local v1    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .end local v2    # "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    .end local v3    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    :goto_0
    return-object v0

    .line 578
    .restart local v2    # "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    :catch_0
    move-exception v4

    goto :goto_0

    .line 576
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method protected getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    .locals 4

    .prologue
    .line 589
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    .line 590
    .local v1, "postId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v2, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 591
    .local v2, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 593
    .local v0, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$14;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    return-object v3
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 403
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 404
    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audio_fragment:I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->audioFragment:Lcom/google/apps/dots/android/newsstand/audio/AudioFragment;

    .line 408
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "articleFontSizev2"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 422
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onAttach(Landroid/app/Activity;)V

    .line 423
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 178
    if-eqz p1, :cond_0

    const-string v0, "NewsArticleFragment_pageFraction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "NewsArticleFragment_pageFraction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->articleFractionToRestoreTo:F

    .line 181
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onCreate(Landroid/os/Bundle;)V

    .line 182
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 463
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 464
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->news_article_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 465
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->startDestroy()V

    .line 150
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->bookmarkHelper:Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/BookmarkMenuHelper;->onDestroyView()V

    .line 153
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 154
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->setResourceLoadFailedListener(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;)V

    .line 157
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V

    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 162
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 165
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 166
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->fontSizePrefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 431
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDetach()V

    .line 432
    return-void
.end method

.method protected onLoadFailed(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lastLoadFailedThrowable:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lastLoadFailedThrowable:Ljava/lang/Throwable;

    .line 348
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 349
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lastLoadFailedThrowable:Ljava/lang/Throwable;

    .line 350
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->updateErrorView()V

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadingView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 354
    return-void
.end method

.method protected onLoadFinished()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 341
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->markPostAsReadIfNeeded()V

    .line 342
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->sendMeterDialogAnalyticsEventIfNecessary()V

    .line 343
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 510
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 511
    .local v0, "itemId":I
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_save:I

    if-ne v0, v1, :cond_1

    .line 512
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$11;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 554
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_1
    return v1

    .line 522
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_unsave:I

    if-ne v0, v1, :cond_2

    .line 523
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 533
    :cond_2
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_see_original_article:I

    if-ne v0, v1, :cond_3

    .line 534
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$13;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$13;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 545
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 547
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v1, :cond_4

    .line 548
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->goUpHierarchy(Landroid/app/Activity;)V

    .line 551
    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    .line 552
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 469
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 471
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 478
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Landroid/view/Menu;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 495
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Landroid/view/Menu;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 505
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "NewsArticleFragment_pageFraction"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPageFraction()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 174
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->rootView:Landroid/view/View;

    move-object v0, p1

    .line 132
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->newsArticleParentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .line 133
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->meter_dialog:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 134
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->article_tail:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .line 135
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->overlay:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->overlayView:Landroid/view/View;

    .line 136
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->loading:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadingView:Landroid/view/View;

    .line 137
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->load_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    .line 139
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 145
    return-void
.end method

.method public setHidingActionBarArticleEventDelegate(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;)V
    .locals 0
    .param p1, "hidingActionBarArticleEventDelegate"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    .line 127
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 436
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 437
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setUserVisibleHint(Z)V

    .line 440
    :cond_0
    if-eqz p1, :cond_1

    .line 441
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->markPostAsReadIfNeeded()V

    .line 442
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->sendMeterDialogAnalyticsEventIfNecessary()V

    .line 444
    :cond_1
    return-void
.end method

.method protected toggleActionBar()V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->hidingActionBarArticleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;->onClick()V

    .line 459
    :cond_0
    return-void
.end method

.method protected updateErrorView()V
    .locals 5

    .prologue
    .line 358
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    .line 359
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->lastLoadFailedThrowable:Ljava/lang/Throwable;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->retryRunnable:Ljava/lang/Runnable;

    .line 358
    invoke-static {v2, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 362
    .local v0, "errorViewData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 368
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->configure(Lcom/google/android/libraries/bind/data/Data;)V

    .line 369
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 71
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;)V
    .locals 2
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->init()V

    .line 199
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->loadArticle()V

    .line 201
    :cond_1
    return-void
.end method
