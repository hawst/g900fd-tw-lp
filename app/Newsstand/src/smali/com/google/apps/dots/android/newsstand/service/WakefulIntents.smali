.class public Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;
.super Ljava/lang/Object;
.source "WakefulIntents.java"


# static fields
.field private static final activeWakeLocks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/PowerManager$WakeLock;",
            ">;"
        }
    .end annotation
.end field

.field private static nextId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    .line 16
    const/4 v0, 0x1

    sput v0, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    return-void
.end method

.method public static completeWakefulIntent(Landroid/content/Intent;)Z
    .locals 5
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    const-string v4, "com.google.apps.dots.wakelockid"

    invoke-virtual {p0, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 39
    .local v0, "id":I
    if-nez v0, :cond_0

    .line 49
    :goto_0
    return v2

    .line 42
    :cond_0
    sget-object v4, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    monitor-enter v4

    .line 43
    :try_start_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager$WakeLock;

    .line 44
    .local v1, "wl":Landroid/os/PowerManager$WakeLock;
    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 46
    sget-object v2, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 47
    monitor-exit v4

    move v2, v3

    goto :goto_0

    .line 49
    :cond_1
    monitor-exit v4

    move v2, v3

    goto :goto_0

    .line 50
    .end local v1    # "wl":Landroid/os/PowerManager$WakeLock;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static makeWakeful(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "lockTimeoutMs"    # I

    .prologue
    .line 19
    sget-object v4, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    monitor-enter v4

    .line 20
    :try_start_0
    sget v0, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    .line 21
    .local v0, "id":I
    sget v3, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    .line 22
    sget v3, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    if-gtz v3, :cond_0

    .line 23
    const/4 v3, 0x1

    sput v3, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->nextId:I

    .line 26
    :cond_0
    const-string v3, "com.google.apps.dots.wakelockid"

    invoke-virtual {p1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 28
    const-string v3, "power"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 29
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v3, 0x1

    .line 30
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x5

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "wake:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 29
    invoke-virtual {v1, v3, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    .line 31
    .local v2, "wl":Landroid/os/PowerManager$WakeLock;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 32
    int-to-long v6, p2

    invoke-virtual {v2, v6, v7}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 33
    sget-object v3, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->activeWakeLocks:Landroid/util/SparseArray;

    invoke-virtual {v3, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 34
    monitor-exit v4

    .line 35
    return-void

    .line 34
    .end local v0    # "id":I
    .end local v1    # "pm":Landroid/os/PowerManager;
    .end local v2    # "wl":Landroid/os/PowerManager$WakeLock;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
