.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "TopicEditionScreen.java"


# instance fields
.field private final entity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

.field private final page:I


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;I)V
    .locals 1
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;
    .param p2, "page"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 21
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->entity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->page:I

    .line 23
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 55
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;

    .line 57
    .local v0, "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->page:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 58
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 60
    .end local v0    # "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 28
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->page:I

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 30
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 45
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 35
    const-string v0, "[Topic] - "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->entity:Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method
