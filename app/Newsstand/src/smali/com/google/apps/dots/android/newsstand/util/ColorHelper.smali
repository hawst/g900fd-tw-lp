.class public Lcom/google/apps/dots/android/newsstand/util/ColorHelper;
.super Ljava/lang/Object;
.source "ColorHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final ONBOARD_ACCENT_COLORS:[I

.field private static final RSS_DRAWABLE_IDS:[I

.field private static final TOPIC_COLORS:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field private static final random:Ljava/util/Random;


# instance fields
.field private final util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 25
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->random:Ljava/util/Random;

    .line 76
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BROWN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->RED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->NAVY:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->ORANGE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE1:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->PURPLE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->AQUA:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->MAROON:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE2:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->LIGHT_GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->TOPIC_COLORS:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 90
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_blue:I

    aput v1, v0, v3

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_green:I

    aput v1, v0, v4

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_orange:I

    aput v1, v0, v5

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_purple:I

    aput v1, v0, v6

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_red:I

    aput v1, v0, v7

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->RSS_DRAWABLE_IDS:[I

    .line 98
    sget-object v0, Lcom/google/android/play/onboard/InterstitialOverlay;->DEFAULT_ACCENT_COLORS_RES_IDS:[I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->ONBOARD_ACCENT_COLORS:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;)V
    .locals 0
    .param p1, "util"    # Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    .line 105
    return-void
.end method

.method public static applyAlpha(II)I
    .locals 4
    .param p0, "srcColor"    # I
    .param p1, "alpha"    # I

    .prologue
    .line 136
    .line 137
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    mul-int/2addr v0, p1

    div-int/lit16 v0, v0, 0xff

    .line 138
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 139
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 140
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 136
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static getColorFilter(IZ)Landroid/graphics/ColorFilter;
    .locals 12
    .param p0, "color"    # I
    .param p1, "strict"    # Z

    .prologue
    .line 199
    shr-int/lit8 v10, p0, 0x18

    and-int/lit16 v10, v10, 0xff

    int-to-float v10, v10

    const/high16 v11, 0x437f0000    # 255.0f

    div-float v0, v10, v11

    .line 200
    .local v0, "a":F
    shr-int/lit8 v10, p0, 0x10

    and-int/lit16 v10, v10, 0xff

    int-to-float v10, v10

    const/high16 v11, 0x437f0000    # 255.0f

    div-float v9, v10, v11

    .line 201
    .local v9, "r":F
    shr-int/lit8 v10, p0, 0x8

    and-int/lit16 v10, v10, 0xff

    int-to-float v10, v10

    const/high16 v11, 0x437f0000    # 255.0f

    div-float v7, v10, v11

    .line 202
    .local v7, "g":F
    and-int/lit16 v10, p0, 0xff

    int-to-float v10, v10

    const/high16 v11, 0x437f0000    # 255.0f

    div-float v3, v10, v11

    .line 203
    .local v3, "b":F
    const/16 v10, 0x14

    new-array v4, v10, [F

    const/4 v10, 0x0

    aput v9, v4, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v4, v10

    const/4 v10, 0x2

    const/4 v11, 0x0

    aput v11, v4, v10

    const/4 v10, 0x3

    const/4 v11, 0x0

    aput v11, v4, v10

    const/4 v10, 0x4

    const/4 v11, 0x0

    aput v11, v4, v10

    const/4 v10, 0x5

    const/4 v11, 0x0

    aput v11, v4, v10

    const/4 v10, 0x6

    aput v7, v4, v10

    const/4 v10, 0x7

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0x8

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0x9

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0xa

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0xb

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0xc

    aput v3, v4, v10

    const/16 v10, 0xd

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0xe

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0xf

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0x10

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0x11

    const/4 v11, 0x0

    aput v11, v4, v10

    const/16 v10, 0x12

    const/high16 v11, 0x3f800000    # 1.0f

    aput v11, v4, v10

    const/16 v10, 0x13

    const/4 v11, 0x0

    aput v11, v4, v10

    .line 208
    .local v4, "colorArray":[F
    new-instance v5, Landroid/graphics/ColorMatrix;

    invoke-direct {v5, v4}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 209
    .local v5, "colorMatrix":Landroid/graphics/ColorMatrix;
    if-eqz p1, :cond_0

    .line 210
    new-instance v10, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v10, v5}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 225
    :goto_0
    return-object v10

    .line 211
    :cond_0
    const/high16 v10, 0x3f800000    # 1.0f

    cmpg-float v10, v0, v10

    if-gez v10, :cond_1

    const/4 v10, 0x0

    cmpl-float v10, v0, v10

    if-ltz v10, :cond_1

    .line 212
    const/16 v10, 0x14

    new-array v1, v10, [F

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v0

    aput v11, v1, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v1, v10

    const/4 v10, 0x2

    const/4 v11, 0x0

    aput v11, v1, v10

    const/4 v10, 0x3

    const/4 v11, 0x0

    aput v11, v1, v10

    const/4 v10, 0x4

    mul-float v11, v0, v9

    aput v11, v1, v10

    const/4 v10, 0x5

    const/4 v11, 0x0

    aput v11, v1, v10

    const/4 v10, 0x6

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v0

    aput v11, v1, v10

    const/4 v10, 0x7

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0x8

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0x9

    mul-float v11, v0, v7

    aput v11, v1, v10

    const/16 v10, 0xa

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0xb

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0xc

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v0

    aput v11, v1, v10

    const/16 v10, 0xd

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0xe

    mul-float v11, v0, v3

    aput v11, v1, v10

    const/16 v10, 0xf

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0x10

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0x11

    const/4 v11, 0x0

    aput v11, v1, v10

    const/16 v10, 0x12

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v0

    aput v11, v1, v10

    const/16 v10, 0x13

    aput v0, v1, v10

    .line 217
    .local v1, "alphaArray":[F
    new-instance v2, Landroid/graphics/ColorMatrix;

    invoke-direct {v2, v1}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 218
    .local v2, "alphaMatrix":Landroid/graphics/ColorMatrix;
    new-instance v10, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v10, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    goto :goto_0

    .line 221
    .end local v1    # "alphaArray":[F
    .end local v2    # "alphaMatrix":Landroid/graphics/ColorMatrix;
    :cond_1
    new-instance v8, Landroid/graphics/ColorMatrix;

    invoke-direct {v8}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 222
    .local v8, "grayscale":Landroid/graphics/ColorMatrix;
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 223
    move-object v6, v8

    .line 224
    .local v6, "finalMatrix":Landroid/graphics/ColorMatrix;
    invoke-virtual {v6, v5}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 225
    new-instance v10, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v10, v6}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    goto/16 :goto_0
.end method

.method public static getColorFilterPercentBlack(IIZ)Landroid/graphics/ColorFilter;
    .locals 11
    .param p0, "color"    # I
    .param p1, "percentBlack"    # I
    .param p2, "grayScale"    # Z

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v7, 0x437f0000    # 255.0f

    const/4 v9, 0x0

    .line 364
    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 365
    .local v3, "finalMatrix":Landroid/graphics/ColorMatrix;
    if-eqz p2, :cond_0

    .line 366
    invoke-virtual {v3, v9}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 369
    :cond_0
    shr-int/lit8 v6, p0, 0x10

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    div-float v5, v6, v7

    .line 370
    .local v5, "r":F
    shr-int/lit8 v6, p0, 0x8

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    div-float v4, v6, v7

    .line 371
    .local v4, "g":F
    and-int/lit16 v6, p0, 0xff

    int-to-float v6, v6

    div-float v0, v6, v7

    .line 372
    .local v0, "b":F
    int-to-float v6, p1

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v6, v7

    sub-float v1, v10, v6

    .line 373
    .local v1, "blackMultiplier":F
    new-instance v2, Landroid/graphics/ColorMatrix;

    const/16 v6, 0x14

    new-array v6, v6, [F

    const/4 v7, 0x0

    mul-float v8, v5, v1

    aput v8, v6, v7

    const/4 v7, 0x1

    aput v9, v6, v7

    const/4 v7, 0x2

    aput v9, v6, v7

    const/4 v7, 0x3

    aput v9, v6, v7

    const/4 v7, 0x4

    aput v9, v6, v7

    const/4 v7, 0x5

    aput v9, v6, v7

    const/4 v7, 0x6

    mul-float v8, v4, v1

    aput v8, v6, v7

    const/4 v7, 0x7

    aput v9, v6, v7

    const/16 v7, 0x8

    aput v9, v6, v7

    const/16 v7, 0x9

    aput v9, v6, v7

    const/16 v7, 0xa

    aput v9, v6, v7

    const/16 v7, 0xb

    aput v9, v6, v7

    const/16 v7, 0xc

    mul-float v8, v0, v1

    aput v8, v6, v7

    const/16 v7, 0xd

    aput v9, v6, v7

    const/16 v7, 0xe

    aput v9, v6, v7

    const/16 v7, 0xf

    aput v9, v6, v7

    const/16 v7, 0x10

    aput v9, v6, v7

    const/16 v7, 0x11

    aput v9, v6, v7

    const/16 v7, 0x12

    aput v10, v6, v7

    const/16 v7, 0x13

    aput v9, v6, v7

    invoke-direct {v2, v6}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 379
    .local v2, "colorMatrix":Landroid/graphics/ColorMatrix;
    invoke-virtual {v3, v2}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 380
    new-instance v6, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v6, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    return-object v6
.end method

.method public static getColorPercentBlack(II)I
    .locals 8
    .param p0, "color"    # I
    .param p1, "percentBlack"    # I

    .prologue
    .line 391
    shr-int/lit8 v4, p0, 0x10

    and-int/lit16 v3, v4, 0xff

    .line 392
    .local v3, "r":I
    shr-int/lit8 v4, p0, 0x8

    and-int/lit16 v2, v4, 0xff

    .line 393
    .local v2, "g":I
    and-int/lit16 v0, p0, 0xff

    .line 394
    .local v0, "b":I
    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v5, p1

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    sub-float v1, v4, v5

    .line 396
    .local v1, "blackMultiplier":F
    const/16 v4, 0xff

    int-to-float v5, v3

    mul-float/2addr v5, v1

    float-to-int v5, v5

    int-to-float v6, v2

    mul-float/2addr v6, v1

    float-to-int v6, v6

    int-to-float v7, v0

    mul-float/2addr v7, v1

    float-to-int v7, v7

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    return v4
.end method

.method public static getColorResIdForEditionId(Ljava/lang/String;)I
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 272
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->TOPIC_COLORS:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->TOPIC_COLORS:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->colorResId:I

    return v0
.end method

.method public static getOnboardAccentColor(Ljava/lang/String;)I
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 283
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->ONBOARD_ACCENT_COLORS:[I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->ONBOARD_ACCENT_COLORS:[I

    array-length v2, v2

    rem-int/2addr v1, v2

    aget v0, v0, v1

    return v0
.end method

.method public static getRssDrawableId(Ljava/lang/String;)I
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 276
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->RSS_DRAWABLE_IDS:[I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->RSS_DRAWABLE_IDS:[I

    array-length v2, v2

    rem-int/2addr v1, v2

    aget v0, v0, v1

    return v0
.end method

.method public static isTransparent(I)Z
    .locals 1
    .param p0, "color"    # I

    .prologue
    .line 118
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseQuietly(Ljava/lang/String;I)I
    .locals 2
    .param p0, "colorString"    # Ljava/lang/String;
    .param p1, "defaultColor"    # I

    .prologue
    .line 260
    move v0, p1

    .line 261
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    :try_start_0
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 268
    :cond_0
    :goto_0
    return v0

    .line 264
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getSectionBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Ljava/lang/Integer;
    .locals 3
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 159
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hasDisplayOptions()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getDisplayOptions()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->hasDisplayTemplate()Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    :cond_0
    const/4 v1, 0x0

    .line 164
    :goto_0
    return-object v1

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->util:Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getDisplayOptions()Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Section$DisplayOptions;->getDisplayTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    .line 164
    .local v0, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getTemplateBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public getTemplateBackgroundColor(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;)Ljava/lang/Integer;
    .locals 2
    .param p1, "template"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .prologue
    .line 168
    if-eqz p1, :cond_0

    .line 169
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->hasBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "bgColor":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->parseBackgroundColor(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 169
    .end local v0    # "bgColor":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseBackgroundColor(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .param p1, "backgroundColor"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 175
    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    :goto_0
    return-object v1

    .line 175
    :cond_0
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method
