.class public Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "MagazineEditionIntentBuilder.java"


# instance fields
.field private final intent:Landroid/content/Intent;

.field private ratio:F


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 25
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    move-object v0, p2

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .line 26
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;->getDefaultToLiteModeForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;)Z

    move-result v0

    invoke-direct {v1, p2, v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 25
    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "liteMode"    # Z

    .prologue
    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-direct {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "liteMode"    # Z
    .param p4, "initialPostId"    # Ljava/lang/String;
    .param p5, "initialPageNumber"    # Ljava/lang/Integer;

    .prologue
    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/MagazineEditionActivity;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "MagazineEditionFragment_state"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;->initialPostId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "MagazineEditionFragment_centerOnInitialPostId"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->ratio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "MagazineEditionFragment_heroAspectRatio"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->ratio:F

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 55
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public setTransitionHeroElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
    .locals 3
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->activity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->expando_hero:I

    .line 59
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 64
    .local v0, "ratio":F
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->intent:Landroid/content/Intent;

    const-string v2, "MagazineEditionFragment_heroAspectRatio"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 67
    :cond_0
    return-object p0
.end method
