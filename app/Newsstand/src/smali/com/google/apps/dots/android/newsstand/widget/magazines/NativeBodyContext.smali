.class public interface abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
.super Ljava/lang/Object;
.source "NativeBodyContext.java"


# virtual methods
.method public abstract getAppId()Ljava/lang/String;
.end method

.method public abstract getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
.end method

.method public abstract getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
.end method

.method public abstract getLetterboxScale()F
.end method

.method public abstract onDestroyed(Landroid/view/View;)V
.end method
