.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "MagazineTileFilter.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 31
    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 7
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 37
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->LAYOUTS:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 38
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 41
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_TITLE:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_ISSUE_NAME:I

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 42
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_ICON_ID:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_ATTACHMENT_ID:I

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 43
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_ICON_ID:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_TRANSITION_NAME:I

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 44
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_CONTENT_DESCRIPTION:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CONTENT_DESCRIPTION:I

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 45
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 46
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_HEIGHT_TO_WIDTH_RATIO:I

    const/high16 v5, 0x3fc00000    # 1.5f

    .line 47
    invoke-static {v0, v5}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 46
    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 48
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 49
    .local v2, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_SOURCE_TRANSITION_NAME:I

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 50
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;

    invoke-direct {v5, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineTileFilter;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 63
    .end local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    return-object p1
.end method
