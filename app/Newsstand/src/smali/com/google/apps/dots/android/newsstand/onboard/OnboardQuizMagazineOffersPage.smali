.class public Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;
.super Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;
.source "OnboardQuizMagazineOffersPage.java"


# instance fields
.field private quizItemdataList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 169
    const/4 v0, 0x2

    return v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_quiz_magazine_offers_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNumColumns()I
    .locals 4

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 119
    .local v1, "isPortrait":Z
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 120
    .local v0, "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$2;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 126
    if-eqz v1, :cond_2

    const/4 v2, 0x2

    :goto_1
    return v2

    .line 118
    .end local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v1    # "isPortrait":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 122
    .restart local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v1    # "isPortrait":Z
    :pswitch_0
    if-eqz v1, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :cond_1
    const/4 v2, 0x5

    goto :goto_1

    .line 126
    :cond_2
    const/4 v2, 0x4

    goto :goto_1

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->quizItemdataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList()Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->EQUALITY_FIELDS:[I

    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->quizItemdataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->quizItemdataList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method protected getSelectedMagazineOffers()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 137
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 138
    .local v2, "selectedOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 139
    .local v0, "item":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    sget v3, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_SUMMARY:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v0    # "item":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return-object v2
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizMagazineOffersPage;->getSelectedMagazineOffers()Ljava/util/List;

    move-result-object v2

    .line 150
    .local v2, "selectedOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 151
    .local v0, "encodedOffers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 152
    .local v1, "selectedOffer":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    .end local v1    # "selectedOffer":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    :cond_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_MAGAZINE_OFFERS:Ljava/lang/String;

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 155
    return-void
.end method

.method protected sendAnalyticsEvent()V
    .locals 2

    .prologue
    .line 159
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizMagazineOffersScreen;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizMagazineOffersScreen;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizMagazineOffersScreen;->track(Z)V

    .line 160
    return-void
.end method
