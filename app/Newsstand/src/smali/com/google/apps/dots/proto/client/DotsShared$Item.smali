.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item;


# instance fields
.field private bitField0_:I

.field public fieldId:Ljava/lang/String;

.field public origin:I

.field private safeType_:I

.field public type:I

.field public value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10949
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10950
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 15673
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    .line 15676
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    .line 15679
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    .line 15698
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15701
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    .line 10950
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .locals 5

    .prologue
    .line 15718
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15722
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 15723
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15724
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 15725
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 15726
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v4

    aput-object v4, v3, v2

    .line 15724
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 15719
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 15720
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 15730
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 10946
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15735
    if-ne p1, p0, :cond_1

    .line 15741
    :cond_0
    :goto_0
    return v1

    .line 15736
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 15737
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 15738
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15741
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 15738
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSafeType()I
    .locals 1

    .prologue
    .line 15681
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 15780
    const/4 v1, 0x0

    .line 15781
    .local v1, "size":I
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    .line 15782
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 15783
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    .line 15784
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 15785
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v2, :cond_1

    .line 15786
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 15787
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    .line 15788
    const/4 v5, 0x3

    .line 15789
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 15786
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 15793
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_1
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    .line 15794
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 15795
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 15796
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    .line 15797
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 15799
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->cachedSize:I

    .line 15800
    return v1
.end method

.method public hasSafeType()Z
    .locals 1

    .prologue
    .line 15689
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 15746
    const/16 v1, 0x11

    .line 15747
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 15748
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 15749
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    add-int v1, v2, v4

    .line 15750
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    add-int v1, v2, v4

    .line 15751
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 15757
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    add-int v1, v2, v3

    .line 15758
    return v1

    .line 15748
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 15753
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 15754
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 15753
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 15754
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .locals 12
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 15808
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 15809
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 15813
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 15814
    :sswitch_0
    return-object p0

    .line 15819
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    goto :goto_0

    .line 15823
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 15824
    .local v4, "temp":I
    if-eq v4, v8, :cond_1

    if-eq v4, v9, :cond_1

    if-eq v4, v10, :cond_1

    if-eq v4, v11, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/4 v6, 0x6

    if-eq v4, v6, :cond_1

    const/4 v6, 0x7

    if-eq v4, v6, :cond_1

    const/16 v6, 0x8

    if-eq v4, v6, :cond_1

    const/16 v6, 0x9

    if-eq v4, v6, :cond_1

    const/16 v6, 0xa

    if-eq v4, v6, :cond_1

    const/16 v6, 0xb

    if-eq v4, v6, :cond_1

    const/16 v6, 0xc

    if-eq v4, v6, :cond_1

    const/16 v6, 0xd

    if-eq v4, v6, :cond_1

    const/16 v6, 0xe

    if-eq v4, v6, :cond_1

    const/16 v6, 0xf

    if-ne v4, v6, :cond_2

    .line 15839
    :cond_1
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    goto :goto_0

    .line 15841
    :cond_2
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    goto :goto_0

    .line 15846
    .end local v4    # "temp":I
    :sswitch_3
    const/16 v6, 0x1a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 15847
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-nez v6, :cond_4

    move v1, v5

    .line 15848
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15849
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v6, :cond_3

    .line 15850
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 15852
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15853
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 15854
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    aput-object v7, v6, v1

    .line 15855
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 15856
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 15853
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 15847
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v1, v6

    goto :goto_1

    .line 15859
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    aput-object v7, v6, v1

    .line 15860
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15864
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 15865
    .restart local v4    # "temp":I
    if-eq v4, v8, :cond_6

    if-eq v4, v9, :cond_6

    if-eq v4, v10, :cond_6

    if-eq v4, v11, :cond_6

    const/4 v6, 0x5

    if-eq v4, v6, :cond_6

    const/16 v6, 0xa

    if-eq v4, v6, :cond_6

    const/4 v6, 0x6

    if-eq v4, v6, :cond_6

    const/4 v6, 0x7

    if-eq v4, v6, :cond_6

    const/16 v6, 0x8

    if-eq v4, v6, :cond_6

    const/16 v6, 0x9

    if-ne v4, v6, :cond_7

    .line 15875
    :cond_6
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    goto/16 :goto_0

    .line 15877
    :cond_7
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    goto/16 :goto_0

    .line 15882
    .end local v4    # "temp":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 15883
    .restart local v4    # "temp":I
    if-eq v4, v8, :cond_8

    if-eq v4, v9, :cond_8

    if-eq v4, v10, :cond_8

    if-eq v4, v11, :cond_8

    const/4 v6, 0x5

    if-eq v4, v6, :cond_8

    const/4 v6, 0x6

    if-eq v4, v6, :cond_8

    const/4 v6, 0x7

    if-eq v4, v6, :cond_8

    const/16 v6, 0x8

    if-eq v4, v6, :cond_8

    const/16 v6, 0x9

    if-eq v4, v6, :cond_8

    const/16 v6, 0xa

    if-eq v4, v6, :cond_8

    const/16 v6, 0xb

    if-eq v4, v6, :cond_8

    const/16 v6, 0xc

    if-eq v4, v6, :cond_8

    const/16 v6, 0xd

    if-eq v4, v6, :cond_8

    const/16 v6, 0xe

    if-eq v4, v6, :cond_8

    const/16 v6, 0xf

    if-ne v4, v6, :cond_9

    .line 15898
    :cond_8
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    .line 15899
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->bitField0_:I

    goto/16 :goto_0

    .line 15901
    :cond_9
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    goto/16 :goto_0

    .line 15809
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10946
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15763
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 15764
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 15765
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-eqz v1, :cond_1

    .line 15766
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 15767
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    .line 15768
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15766
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 15772
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_1
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 15773
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 15774
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->safeType_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 15776
    :cond_2
    return-void
.end method
