.class Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "StartActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->checkOnboardFlagAndStartOrResumeFlow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "optionalResult"    # Ljava/lang/Boolean;

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    # setter for: Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->access$102(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 196
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startOrResumeFlow()V

    .line 197
    return-void

    .line 195
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 190
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;->onSuccess(Ljava/lang/Boolean;)V

    return-void
.end method
