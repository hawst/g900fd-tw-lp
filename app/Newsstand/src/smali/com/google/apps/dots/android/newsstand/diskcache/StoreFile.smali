.class Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
.super Ljava/lang/Object;
.source "StoreFile.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final appendLock:Ljava/lang/Object;

.field private blobsChannel:Ljava/nio/channels/FileChannel;

.field private final blobsFile:Ljava/io/File;

.field private blobsRaf:Ljava/io/RandomAccessFile;

.field private final cacheBlobsDir:Ljava/io/File;

.field private dirty:Z

.field private entryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;

.field private final manifestFile:Ljava/io/File;

.field private final permBlobsDir:Ljava/io/File;

.field private storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 4
    .param p1, "manifestFile"    # Ljava/io/File;
    .param p2, "blobsFile"    # Ljava/io/File;
    .param p3, "permBlobsDir"    # Ljava/io/File;
    .param p4, "cacheBlobsDir"    # Ljava/io/File;

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 83
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    .line 94
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    .line 95
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    .line 96
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->permBlobsDir:Ljava/io/File;

    .line 97
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->cacheBlobsDir:Ljava/io/File;

    .line 98
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Creating %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method static isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z
    .locals 1
    .param p0, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLocation()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPinned(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z
    .locals 1
    .param p0, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;
    .locals 4
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 239
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 240
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "filename":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLocation()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->permBlobsDir:Ljava/io/File;

    .line 242
    .local v1, "isolatedDir":Ljava/io/File;
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2

    .line 241
    .end local v1    # "isolatedDir":Ljava/io/File;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->cacheBlobsDir:Ljava/io/File;

    goto :goto_0
.end method

.method private readManifest()V
    .locals 31
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    const/16 v19, 0x0

    .line 248
    .local v19, "success":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    move-object/from16 v24, v0

    monitor-enter v24
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 249
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    move-object/from16 v25, v0

    monitor-enter v25
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 250
    const/16 v22, 0x0

    :try_start_2
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 251
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 252
    new-instance v22, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-direct/range {v22 .. v22}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 258
    .local v6, "blobFileLength":J
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v5

    .line 259
    .local v5, "cacheIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v18

    .line 261
    .local v18, "permIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    const/4 v9, 0x0

    .line 263
    .local v9, "fis":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 264
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .local v10, "fis":Ljava/io/FileInputStream;
    :try_start_4
    new-instance v22, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    invoke-direct/range {v22 .. v22}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;-><init>()V

    const/16 v23, 0x2000

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->readFromStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/InputStream;I)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v14

    check-cast v14, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    .line 267
    .local v14, "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    const/4 v11, 0x0

    .line 268
    .local v11, "flush":Z
    iget-object v0, v14, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v27, v0

    const/16 v22, 0x0

    move/from16 v23, v22

    :goto_0
    move/from16 v0, v23

    move/from16 v1, v27

    if-ge v0, v1, :cond_1

    aget-object v4, v26, v23

    .line 270
    .local v4, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v16

    .line 271
    .local v16, "offset":J
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v20

    .line 272
    .local v20, "size":J
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v12

    .line 274
    .local v12, "isolated":Z
    const-wide/16 v28, 0x0

    cmp-long v22, v16, v28

    if-ltz v22, :cond_0

    const-wide/16 v28, 0x0

    cmp-long v22, v20, v28

    if-gez v22, :cond_4

    .line 276
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->clear()V

    .line 277
    new-instance v22, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-direct/range {v22 .. v22}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .line 278
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->clear()V

    .line 279
    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 280
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 281
    sget-object v22, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v23, "Found invalid store file manifest entry."

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    .end local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v12    # "isolated":Z
    .end local v16    # "offset":J
    .end local v20    # "size":J
    :cond_1
    if-eqz v11, :cond_2

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->flush()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 318
    :cond_2
    const/16 v22, 0x1

    :try_start_5
    move/from16 v0, v22

    invoke-static {v10, v0}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    move-object v9, v10

    .line 321
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "flush":Z
    .end local v14    # "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->permBlobsDir:Ljava/io/File;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->reconcileSpuriousBlobFiles(Ljava/io/File;Ljava/util/Set;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->cacheBlobsDir:Ljava/io/File;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->reconcileSpuriousBlobFiles(Ljava/io/File;Ljava/util/Set;)V

    .line 323
    monitor-exit v25
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 324
    :try_start_6
    monitor-exit v24
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 325
    const/16 v19, 0x1

    .line 329
    if-nez v19, :cond_3

    .line 330
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->close(Z)V

    .line 333
    :cond_3
    return-void

    .line 283
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "flush":Z
    .restart local v12    # "isolated":Z
    .restart local v14    # "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .restart local v16    # "offset":J
    .restart local v20    # "size":J
    :cond_4
    if-nez v12, :cond_6

    add-long v28, v16, v20

    cmp-long v22, v28, v6

    if-lez v22, :cond_6

    .line 288
    const/16 v22, 0x1

    :try_start_7
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 289
    const/4 v11, 0x1

    .line 290
    sget-object v22, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v28, "Ignoring invalid blob entry %s."

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    aput-object v4, v29, v30

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    :cond_5
    :goto_2
    add-int/lit8 v22, v23, 0x1

    move/from16 v23, v22

    goto/16 :goto_0

    .line 292
    :cond_6
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getKey()[B

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v13

    .line 293
    .local v13, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v13, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 294
    .local v15, "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 297
    if-nez v15, :cond_8

    const/16 v22, 0x1

    :goto_3
    invoke-static/range {v22 .. v22}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 299
    if-eqz v12, :cond_5

    .line 300
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLocation()I

    move-result v22

    const/16 v28, 0x1

    move/from16 v0, v22

    move/from16 v1, v28

    if-ne v0, v1, :cond_9

    .line 301
    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_2

    .line 311
    .end local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v11    # "flush":Z
    .end local v12    # "isolated":Z
    .end local v13    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v14    # "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .end local v15    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v16    # "offset":J
    .end local v20    # "size":J
    :catch_0
    move-exception v22

    move-object v9, v10

    .line 318
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :goto_4
    const/16 v22, 0x1

    :try_start_8
    move/from16 v0, v22

    invoke-static {v9, v0}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    goto/16 :goto_1

    .line 323
    .end local v5    # "cacheIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    .end local v6    # "blobFileLength":J
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "permIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    :catchall_0
    move-exception v22

    monitor-exit v25
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v22

    .line 324
    :catchall_1
    move-exception v22

    monitor-exit v24
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v22
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 329
    :catchall_2
    move-exception v22

    if-nez v19, :cond_7

    .line 330
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->close(Z)V

    :cond_7
    throw v22

    .line 297
    .restart local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v5    # "cacheIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    .restart local v6    # "blobFileLength":J
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "flush":Z
    .restart local v12    # "isolated":Z
    .restart local v13    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .restart local v14    # "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .restart local v15    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v16    # "offset":J
    .restart local v18    # "permIsolated":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    .restart local v20    # "size":J
    :cond_8
    const/16 v22, 0x0

    goto :goto_3

    .line 303
    :cond_9
    :try_start_b
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    goto :goto_2

    .line 313
    .end local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v11    # "flush":Z
    .end local v12    # "isolated":Z
    .end local v13    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v14    # "manifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .end local v15    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v16    # "offset":J
    .end local v20    # "size":J
    :catch_1
    move-exception v8

    move-object v9, v10

    .line 315
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v8, "e":Ljava/io/IOException;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :goto_5
    const/16 v22, 0x1

    :try_start_c
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 316
    sget-object v22, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 318
    const/16 v22, 0x1

    :try_start_d
    move/from16 v0, v22

    invoke-static {v9, v0}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    goto/16 :goto_1

    .end local v8    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v22

    :goto_6
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-static {v9, v0}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    throw v22
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catchall_4
    move-exception v22

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .line 313
    :catch_2
    move-exception v8

    goto :goto_5

    .line 311
    :catch_3
    move-exception v22

    goto :goto_4
.end method

.method private reconcileSpuriousBlobFiles(Ljava/io/File;Ljava/util/Set;)V
    .locals 12
    .param p1, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "manifest":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;>;"
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 341
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->listFilenamesNonNull(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v5

    .line 342
    .local v5, "onDiskFiles":[Ljava/lang/String;
    array-length v6, v5

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 362
    :cond_0
    return-void

    .line 347
    :cond_1
    invoke-static {v5}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    .line 348
    .local v4, "onDiskFilenames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v1

    .line 349
    .local v1, "expectedOnDiskFilenames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 350
    .local v3, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "encoded":Ljava/lang/String;
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    .end local v0    # "encoded":Ljava/lang/String;
    .end local v3    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :cond_2
    invoke-static {v4, v1}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/common/collect/Sets$SetView;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 354
    .local v2, "filename":Ljava/lang/String;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Removing spurious isolated blob file %s"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v2, v9, v11

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 357
    .end local v2    # "filename":Ljava/lang/String;
    :cond_3
    invoke-static {v1, v4}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/common/collect/Sets$SetView;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 358
    .restart local v2    # "filename":Ljava/lang/String;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Removing spurious isolated blob entry %s"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v2, v9, v11

    invoke-virtual {v6, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 359
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameDecode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-virtual {v8, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 360
    iput-boolean v10, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    goto :goto_2
.end method

.method private static setPinSnapshot(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;II)V
    .locals 5
    .param p0, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p1, "pinId"    # I
    .param p2, "snapshotId"    # I

    .prologue
    .line 493
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 494
    .local v0, "pin":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->getPinId()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 495
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->setSnapshotId(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 504
    :goto_1
    return-void

    .line 493
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 500
    .end local v0    # "pin":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :cond_1
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;-><init>()V

    .line 501
    .restart local v0    # "pin":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->setPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 502
    invoke-virtual {v0, p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->setSnapshotId(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 503
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-static {v1, v0}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    goto :goto_1
.end method

.method static shouldIsolate(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z
    .locals 4
    .param p0, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v0

    const-wide/32 v2, 0x10000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private splitBetween(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 7
    .param p1, "lower"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "upper"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    const/4 v5, 0x0

    .line 111
    invoke-virtual {p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v4

    if-gez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 113
    const/4 v0, 0x0

    .line 114
    .local v0, "commonPrefixLength":I
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->size()I

    move-result v4

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->size()I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 115
    .local v3, "shorterLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->get(I)B

    move-result v4

    invoke-virtual {p2, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->get(I)B

    move-result v6

    if-ne v4, v6, :cond_1

    .line 116
    add-int/lit8 v0, v0, 0x1

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "commonPrefixLength":I
    .end local v1    # "i":I
    .end local v3    # "shorterLength":I
    :cond_0
    move v4, v5

    .line 111
    goto :goto_0

    .line 120
    .restart local v0    # "commonPrefixLength":I
    .restart local v1    # "i":I
    .restart local v3    # "shorterLength":I
    :cond_1
    add-int/lit8 v4, v0, 0x1

    new-array v2, v4, [B

    .line 121
    .local v2, "result":[B
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes()[B

    move-result-object v4

    array-length v6, v2

    invoke-static {v4, v5, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v4

    return-object v4
.end method

.method private takeIsolatedFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Ljava/io/File;)V
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "srcBlobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 844
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v4

    .line 845
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 846
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 848
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v1

    .line 849
    .local v1, "newFile":Ljava/io/File;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Taking blob %s from %s to %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p3, v6, v7

    const/4 v7, 0x2

    aput-object v1, v6, v7

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 850
    invoke-static {p3, v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->move(Ljava/io/File;Ljava/io/File;)V

    .line 851
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v0

    .line 853
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 855
    :try_start_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 856
    .local v2, "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v3, v2, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->replace(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 857
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 858
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 859
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 860
    return-void

    .line 858
    .end local v2    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3

    .line 859
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v1    # "newFile":Ljava/io/File;
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3
.end method

.method private static toMillis(J)J
    .locals 2
    .param p0, "seconds"    # J

    .prologue
    .line 106
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method private static toSeconds(J)J
    .locals 2
    .param p0, "millis"    # J

    .prologue
    .line 102
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    return-wide v0
.end method

.method private unpin(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Landroid/util/SparseIntArray;I)Z
    .locals 12
    .param p1, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p2, "pinSnapshotIds"    # Landroid/util/SparseIntArray;
    .param p3, "defaultSnapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 523
    const/4 v0, 0x0

    .line 524
    .local v0, "changed":Z
    iget-object v8, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-static {v8}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v6

    .line 525
    .local v6, "pinList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 526
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 527
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 528
    .local v5, "pin":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->getPinId()I

    move-result v8

    invoke-virtual {p2, v8, p3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v7

    .line 529
    .local v7, "snapshotId":I
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->getSnapshotId()I

    move-result v8

    if-ge v8, v7, :cond_0

    .line 530
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 531
    const/4 v0, 0x1

    goto :goto_0

    .line 534
    .end local v5    # "pin":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    .end local v7    # "snapshotId":I
    :cond_1
    if-eqz v0, :cond_2

    .line 536
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v8, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 537
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    invoke-interface {v6, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    iput-object v8, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 538
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v8, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 540
    iget-object v8, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v8, v8

    if-nez v8, :cond_2

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 541
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLocation()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    const/4 v8, 0x1

    :goto_1
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 543
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getKey()[B

    move-result-object v8

    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v2

    .line 544
    .local v2, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v4

    .line 545
    .local v4, "oldLocation":Ljava/io/File;
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setLocation(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 546
    invoke-direct {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v3

    .line 547
    .local v3, "newLocation":Ljava/io/File;
    sget-object v8, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "Moving isolated file %s --> %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    const/4 v11, 0x1

    aput-object v3, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->move(Ljava/io/File;Ljava/io/File;)V

    .line 554
    .end local v2    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v3    # "newLocation":Ljava/io/File;
    .end local v4    # "oldLocation":Ljava/io/File;
    :cond_2
    return v0

    .line 541
    :cond_3
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private writeByteChannel(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Ljava/nio/channels/ReadableByteChannel;)V
    .locals 12
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "srcBlobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .param p3, "byteChannel"    # Ljava/nio/channels/ReadableByteChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 818
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v8

    .line 819
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 820
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    .line 821
    .local v2, "dstBlobOffset":J
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    .line 823
    .local v4, "srcBlobSize":J
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 825
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    add-long v10, v2, v4

    invoke-virtual {v0, v10, v11}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 827
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setOffset(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v6

    .line 829
    .local v6, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 831
    :try_start_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v0, p1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 832
    .local v7, "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v0, v7, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->replace(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 833
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 834
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 836
    return-void

    .line 834
    .end local v7    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 835
    .end local v2    # "dstBlobOffset":J
    .end local v4    # "srcBlobSize":J
    .end local v6    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method private writeManifest()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 409
    :try_start_0
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;-><init>()V

    .line 410
    .local v0, "diskManifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->setVersion(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;

    .line 411
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    iput-object v5, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 412
    const/4 v4, 0x0

    .line 413
    .local v4, "i":I
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 414
    .local v1, "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v7, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;->entry:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    aput-object v1, v7, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 415
    add-int/lit8 v4, v4, 0x1

    .line 416
    goto :goto_0

    .line 418
    .end local v1    # "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_0
    const/4 v2, 0x0

    .line 420
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    const/4 v7, 0x0

    invoke-direct {v3, v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->writeToStream(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 423
    const/4 v5, 0x1

    :try_start_3
    invoke-static {v3, v5}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    .line 425
    monitor-exit v6

    .line 426
    return-void

    .line 423
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    :goto_1
    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    throw v5

    .line 425
    .end local v0    # "diskManifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "i":I
    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 423
    .restart local v0    # "diskManifest":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    :catchall_2
    move-exception v5

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method close(Z)V
    .locals 6
    .param p1, "writeback"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    :try_start_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    if-eqz p1, :cond_1

    .line 369
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Deleting empty StoreFile %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 372
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete()V

    .line 373
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 392
    :goto_0
    return-void

    .line 375
    :cond_0
    :try_start_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->flush()V

    .line 379
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 383
    :try_start_4
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 387
    :goto_1
    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    .line 390
    :cond_2
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 391
    :try_start_6
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 390
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 384
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public compact()V
    .locals 32
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    move-object/from16 v29, v0

    monitor-enter v29

    .line 939
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    move-object/from16 v30, v0

    monitor-enter v30
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 940
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v16

    .line 941
    .local v16, "initialSize":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 944
    .local v26, "startTimeMs":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v25

    .line 945
    .local v25, "sortedByOffset":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$2;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V

    move-object/from16 v0, v25

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    move-object/from16 v23, v0

    .line 956
    .local v23, "oldEntryMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-object/from16 v24, v0

    .line 957
    .local v24, "oldStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 958
    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .line 961
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_0

    .line 962
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3

    .line 1048
    .end local v16    # "initialSize":J
    .end local v23    # "oldEntryMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .end local v24    # "oldStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    .end local v25    # "sortedByOffset":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .end local v26    # "startTimeMs":J
    :catchall_0
    move-exception v3

    monitor-exit v30
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3

    .line 1053
    :catchall_1
    move-exception v3

    monitor-exit v29
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 965
    .restart local v16    # "initialSize":J
    .restart local v23    # "oldEntryMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .restart local v24    # "oldStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    .restart local v25    # "sortedByOffset":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .restart local v26    # "startTimeMs":J
    :cond_0
    const/16 v28, 0x0

    .line 966
    .local v28, "tempFile":Ljava/io/File;
    const/16 v20, 0x0

    .line 967
    .local v20, "newBlobsRaf":Ljava/io/RandomAccessFile;
    const/4 v14, 0x0

    .line 970
    .local v14, "newBlobsChannel":Ljava/nio/channels/FileChannel;
    :try_start_3
    const-string v6, "___"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    .line 971
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string v6, ".c"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    .line 970
    invoke-static {v3, v6, v7}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v28

    .line 972
    new-instance v21, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 973
    .end local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .local v21, "newBlobsRaf":Ljava/io/RandomAccessFile;
    :try_start_4
    invoke-virtual/range {v21 .. v21}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v14

    .line 976
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_1
    :goto_1
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 977
    .local v2, "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 981
    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 983
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v4

    .line 984
    .local v4, "oldOffset":J
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->shouldIsolate(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 986
    iget-object v3, v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v3, v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setLocation(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 987
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clearOffset()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 990
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getKey()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v19

    .line 991
    .local v19, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v15

    .line 992
    .local v15, "isolatedFile":Ljava/io/File;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Isolating %s to %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v19, v7, v9

    const/4 v9, 0x1

    aput-object v15, v7, v9

    invoke-virtual {v3, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 993
    invoke-virtual {v15}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 994
    new-instance v18, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    move-object/from16 v0, v18

    invoke-direct {v0, v15, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 995
    .local v18, "isolatedRaf":Ljava/io/RandomAccessFile;
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v8

    .line 997
    .local v8, "isolatedChannel":Ljava/nio/channels/FileChannel;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v6

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 999
    :try_start_6
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 1000
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    .line 1009
    .end local v8    # "isolatedChannel":Ljava/nio/channels/FileChannel;
    .end local v15    # "isolatedFile":Ljava/io/File;
    .end local v18    # "isolatedRaf":Ljava/io/RandomAccessFile;
    .end local v19    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :goto_3
    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 1038
    .end local v2    # "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v4    # "oldOffset":J
    :catchall_2
    move-exception v3

    move-object/from16 v20, v21

    .end local v21    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .restart local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    :goto_4
    if-eqz v14, :cond_2

    .line 1039
    :try_start_7
    invoke-virtual {v14}, Ljava/nio/channels/FileChannel;->close()V

    .line 1041
    :cond_2
    if-eqz v20, :cond_3

    .line 1042
    invoke-virtual/range {v20 .. v20}, Ljava/io/RandomAccessFile;->close()V

    .line 1044
    :cond_3
    if-eqz v28, :cond_4

    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1045
    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->delete()Z

    :cond_4
    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 971
    :cond_5
    :try_start_8
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto/16 :goto_0

    .line 1038
    :catchall_3
    move-exception v3

    goto :goto_4

    .line 986
    .end local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .restart local v2    # "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v4    # "oldOffset":J
    .restart local v21    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    :cond_6
    const/4 v3, 0x2

    goto :goto_2

    .line 999
    .restart local v8    # "isolatedChannel":Ljava/nio/channels/FileChannel;
    .restart local v15    # "isolatedFile":Ljava/io/File;
    .restart local v18    # "isolatedRaf":Ljava/io/RandomAccessFile;
    .restart local v19    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :catchall_4
    move-exception v3

    :try_start_9
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V

    .line 1000
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    throw v3

    .line 1005
    .end local v8    # "isolatedChannel":Ljava/nio/channels/FileChannel;
    .end local v15    # "isolatedFile":Ljava/io/File;
    .end local v18    # "isolatedRaf":Ljava/io/RandomAccessFile;
    .end local v19    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :cond_7
    invoke-virtual {v14}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setOffset(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 1006
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v12

    move-wide v10, v4

    invoke-virtual/range {v9 .. v14}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    goto :goto_3

    .line 1013
    .end local v2    # "entry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v4    # "oldOffset":J
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1014
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Ljava/nio/channels/FileChannel;->force(Z)V

    .line 1017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    move-object/from16 v22, v0

    .line 1019
    .local v22, "oldBlobs":Ljava/io/RandomAccessFile;
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 1020
    const/16 v20, 0x0

    .line 1021
    .end local v21    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .restart local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    :try_start_a
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    .line 1022
    const/4 v14, 0x0

    .line 1023
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 1024
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .line 1025
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1028
    :try_start_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 1034
    .end local v22    # "oldBlobs":Ljava/io/RandomAccessFile;
    :goto_5
    :try_start_c
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Successful compact %s KB --> %s KB, took %s ms"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-wide/16 v10, 0x400

    div-long v10, v16, v10

    .line 1035
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v10}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    const-wide/16 v12, 0x400

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x2

    .line 1036
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v10, v10, v26

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v9

    .line 1034
    invoke-virtual {v3, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 1038
    if-eqz v14, :cond_9

    .line 1039
    :try_start_d
    invoke-virtual {v14}, Ljava/nio/channels/FileChannel;->close()V

    .line 1041
    :cond_9
    if-eqz v20, :cond_a

    .line 1042
    invoke-virtual/range {v20 .. v20}, Ljava/io/RandomAccessFile;->close()V

    .line 1044
    :cond_a
    if-eqz v28, :cond_b

    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1045
    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->delete()Z

    .line 1048
    :cond_b
    monitor-exit v30
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1052
    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->flush()V

    .line 1053
    monitor-exit v29
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1054
    return-void

    .line 1029
    .restart local v22    # "oldBlobs":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v3

    goto :goto_5

    .end local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .end local v22    # "oldBlobs":Ljava/io/RandomAccessFile;
    .restart local v21    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    :cond_c
    move-object/from16 v20, v21

    .end local v21    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    .restart local v20    # "newBlobsRaf":Ljava/io/RandomAccessFile;
    goto :goto_5
.end method

.method public computeSplitPoint()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 9

    .prologue
    .line 142
    const/4 v5, 0x0

    .line 143
    .local v5, "splitPoint":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v7

    .line 144
    const/4 v4, 0x0

    .line 145
    .local v4, "lastKey":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    const/4 v2, 0x0

    .line 146
    .local v2, "index":I
    :try_start_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 147
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    new-instance v6, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$1;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V

    invoke-static {v0, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 154
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 155
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 156
    .local v3, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    if-eqz v4, :cond_0

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    iget v8, v8, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    div-int/lit8 v8, v8, 0x2

    if-ne v2, v8, :cond_0

    .line 157
    invoke-direct {p0, v4, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->splitBetween(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v5

    .line 159
    :cond_0
    move-object v4, v3

    .line 160
    add-int/lit8 v2, v2, 0x1

    .line 161
    goto :goto_0

    .line 162
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .end local v3    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :cond_1
    monitor-exit v7

    return-object v5

    .line 163
    .end local v0    # "entries":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public contains(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 2
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 429
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 431
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method delete()V
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 196
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    .line 209
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 210
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->permBlobsDir:Ljava/io/File;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->cacheBlobsDir:Ljava/io/File;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    .line 213
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 214
    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 215
    return-void

    .line 213
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0

    .line 214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 203
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public delete(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 4
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    const/4 v1, 0x1

    .line 863
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 864
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 865
    .local v0, "removedEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 866
    if-eqz v0, :cond_1

    .line 867
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 868
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 870
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 871
    monitor-exit v2

    .line 873
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 874
    .end local v0    # "removedEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 396
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    if-eqz v0, :cond_0

    .line 397
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->writeManifest()V

    .line 398
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 400
    :cond_0
    monitor-exit v1

    .line 401
    return-void

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAssetFileDescriptor(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Landroid/content/res/AssetFileDescriptor;
    .locals 12
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
        }
    .end annotation

    .prologue
    .line 630
    const/4 v8, 0x0

    .line 631
    .local v8, "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v11

    .line 632
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    .local v7, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v7, :cond_3

    .line 634
    const/4 v1, 0x0

    .line 635
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    const/4 v6, 0x0

    .line 637
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_1
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 638
    invoke-direct {p0, p1, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v10

    .line 639
    .local v10, "isolatedFile":Ljava/io/File;
    const/high16 v2, 0x10000000

    invoke-static {v10, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 640
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v2

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 641
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    const-wide/16 v2, 0x0

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_1
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 657
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "isolatedFile":Ljava/io/File;
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    :goto_0
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 659
    if-eqz v0, :cond_2

    .line 660
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setReadTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 661
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    .line 643
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v10    # "isolatedFile":Ljava/io/File;
    :cond_0
    :try_start_3
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Couldn\'t find isolated blob %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    goto :goto_0

    .line 646
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v10    # "isolatedFile":Ljava/io/File;
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->verifyBlobEntryRangeInBlobsFile(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 647
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    const/high16 v3, 0x10000000

    invoke-static {v2, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 648
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v2

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_3
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    goto :goto_0

    .line 650
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    :catch_0
    move-exception v9

    .line 652
    .local v9, "e":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    move-object v8, v9

    move-object v0, v6

    .line 655
    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    goto :goto_0

    .line 653
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v9    # "e":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    .restart local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    :catch_1
    move-exception v2

    move-object v0, v6

    .end local v6    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    goto :goto_0

    .line 663
    :cond_2
    :try_start_4
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    .line 664
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 667
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_3
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 668
    if-eqz v8, :cond_4

    .line 669
    throw v8

    .line 667
    .end local v7    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 671
    .restart local v7    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_4
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getDiskBlob(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .locals 13
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
        }
    .end annotation

    .prologue
    .line 578
    const/4 v7, 0x0

    .line 579
    .local v7, "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v12

    .line 580
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    .local v6, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v6, :cond_3

    .line 582
    const/4 v11, 0x0

    .line 583
    .local v11, "raf":Ljava/io/RandomAccessFile;
    const/4 v8, 0x0

    .line 585
    .local v8, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :try_start_1
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 586
    invoke-direct {p0, p1, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v10

    .line 587
    .local v10, "isolatedFile":Ljava/io/File;
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, v10, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588
    .end local v11    # "raf":Ljava/io/RandomAccessFile;
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 589
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    const-wide/16 v2, 0x0

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;-><init>(Ljava/io/RandomAccessFile;JJ)V
    :try_end_2
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 605
    .end local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .end local v10    # "isolatedFile":Ljava/io/File;
    .local v0, "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :goto_0
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 607
    if-eqz v0, :cond_2

    .line 608
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setReadTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 609
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0

    .line 591
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v10    # "isolatedFile":Ljava/io/File;
    :cond_0
    :try_start_4
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Couldn\'t find isolated blob %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v8

    .end local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    goto :goto_0

    .line 594
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "isolatedFile":Ljava/io/File;
    .restart local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v11    # "raf":Ljava/io/RandomAccessFile;
    :cond_1
    :try_start_5
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->verifyBlobEntryRangeInBlobsFile(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 595
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    const-string v3, "r"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 596
    .end local v11    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    :try_start_6
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;-><init>(Ljava/io/RandomAccessFile;JJ)V
    :try_end_6
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .end local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    goto :goto_0

    .line 598
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v11    # "raf":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v9

    move-object v1, v11

    .line 600
    .end local v11    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    .local v9, "e":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    :goto_1
    move-object v7, v9

    move-object v0, v8

    .line 603
    .end local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    goto :goto_0

    .line 601
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "e":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    .restart local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v11    # "raf":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v2

    move-object v1, v11

    .end local v11    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    :goto_2
    move-object v0, v8

    .end local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .restart local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    goto :goto_0

    .line 611
    :cond_2
    :try_start_7
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 612
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 615
    .end local v0    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    :cond_3
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 616
    if-eqz v7, :cond_4

    .line 617
    throw v7

    .line 615
    .end local v6    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v2

    :try_start_8
    monitor-exit v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v2

    .line 619
    .restart local v6    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_4
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 601
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "diskBlob":Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :catch_2
    move-exception v2

    goto :goto_2

    .line 598
    :catch_3
    move-exception v9

    goto :goto_1
.end method

.method public getMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .locals 12
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 703
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v9

    .line 704
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 705
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_3

    .line 706
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .line 707
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getReadTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v2

    .line 708
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getWriteTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v4

    .line 709
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->hasETag()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getETag()[B

    move-result-object v6

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->decodeUtf8([B)Ljava/lang/String;

    move-result-object v6

    .line 710
    :goto_0
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->hasLastModified()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLastModified()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 711
    :goto_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->hasExpiration()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getExpiration()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    :cond_0
    invoke-direct/range {v1 .. v8}, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;-><init>(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    monitor-exit v9

    return-object v1

    :cond_1
    move-object v6, v8

    .line 709
    goto :goto_0

    :cond_2
    move-object v7, v8

    .line 710
    goto :goto_1

    .line 713
    :cond_3
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 714
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 713
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getStats()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 127
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    new-instance v1, Ljava/io/IOException;

    const-string v3, "StoreFile closed"

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 138
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 130
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->clone()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 133
    .local v0, "clonedStats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    :try_start_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileSize:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    :goto_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0

    .line 134
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method isOpen()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method open()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v1

    .line 223
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :try_start_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    const-string v4, "rw"

    invoke-direct {v0, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    .line 226
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    .line 227
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 228
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->readManifest()V

    .line 230
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 231
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 230
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public pin(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;II)V
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "pinId"    # I
    .param p3, "snapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 463
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 464
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 465
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_1

    .line 466
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getLocation()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 468
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v2

    .line 469
    .local v2, "oldLocation":Ljava/io/File;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setLocation(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 470
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v1

    .line 471
    .local v1, "newLocation":Ljava/io/File;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Moving isolated file %s --> %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->move(Ljava/io/File;Ljava/io/File;)V

    .line 476
    .end local v1    # "newLocation":Ljava/io/File;
    .end local v2    # "oldLocation":Ljava/io/File;
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 478
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 479
    invoke-static {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->setPinSnapshot(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;II)V

    .line 480
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->addBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 481
    monitor-exit v4

    return-void

    .line 483
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 483
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .locals 6
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "metadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 719
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 720
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 721
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_3

    .line 722
    iget-wide v4, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->readTime:J

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setReadTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 723
    iget-wide v4, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->writeTime:J

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setWriteTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 725
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->eTag:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 726
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->eTag:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->encodeUtf8(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setETag([B)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 730
    :goto_0
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 731
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setLastModified(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 735
    :goto_1
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->expiration:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 736
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->expiration:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setExpiration(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 740
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 741
    monitor-exit v2

    return-void

    .line 728
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clearETag()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    goto :goto_0

    .line 743
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 733
    .restart local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clearLastModified()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    goto :goto_1

    .line 738
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clearExpiration()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    goto :goto_2

    .line 743
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 744
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public splitFrom(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    .locals 18
    .param p1, "splitKey"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "otherManifestFile"    # Ljava/io/File;
    .param p3, "otherBlobsFile"    # Ljava/io/File;
    .param p4, "permBlobsDir"    # Ljava/io/File;
    .param p5, "cacheBlobsDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 884
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v13

    .line 885
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 886
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v12

    invoke-static {v12}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 887
    new-instance v11, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    invoke-direct {v11, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    .line 890
    .local v11, "splitStoreFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    .line 893
    .local v6, "endPosition":J
    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 895
    :try_start_2
    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 896
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 897
    .local v9, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 898
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 899
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 900
    .local v10, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 901
    .local v4, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v12

    if-ltz v12, :cond_0

    .line 902
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 903
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v12

    invoke-direct {v11, v10, v4, v12}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->takeIsolatedFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Ljava/io/File;)V

    .line 912
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    .line 913
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v12, v4}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 914
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 917
    .end local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .end local v9    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    .end local v10    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :catch_0
    move-exception v8

    .line 919
    .local v8, "ioe":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete()V

    .line 920
    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 923
    .end local v8    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    :try_start_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v15, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    throw v12

    .line 925
    .end local v6    # "endPosition":J
    .end local v11    # "splitStoreFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    :catchall_1
    move-exception v12

    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v12

    .line 926
    :catchall_2
    move-exception v12

    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v12

    .line 908
    .restart local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .restart local v6    # "endPosition":J
    .restart local v9    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    .restart local v10    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .restart local v11    # "splitStoreFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    :cond_1
    :try_start_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v12, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 909
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-direct {v11, v10, v4, v12}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->writeByteChannel(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Ljava/nio/channels/ReadableByteChannel;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 923
    .end local v4    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .end local v10    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v12, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 925
    monitor-exit v14
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 926
    :try_start_8
    monitor-exit v13
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 928
    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->flush()V

    .line 929
    return-object v11
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1058
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "manifestFile"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->manifestFile:Ljava/io/File;

    .line 1059
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "blobsFile"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    .line 1060
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "permBlobsDir"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->permBlobsDir:Ljava/io/File;

    .line 1061
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "cacheBlobsDir"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->cacheBlobsDir:Ljava/io/File;

    .line 1062
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "open"

    .line 1063
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "dirty"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 1064
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 1065
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public touch(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 10
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 438
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v4

    .line 439
    :try_start_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 440
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-eqz v0, :cond_1

    .line 441
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 442
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 446
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v1

    .line 447
    .local v1, "isolatedFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 448
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-virtual {v5, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->removeBlob(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 449
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 450
    monitor-exit v4

    move v2, v3

    .line 456
    .end local v1    # "isolatedFile":Ljava/io/File;
    :goto_0
    return v2

    .line 453
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setReadTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 454
    monitor-exit v4

    goto :goto_0

    .line 458
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 456
    .restart local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v3

    goto :goto_0
.end method

.method public unpinAll(Landroid/util/SparseIntArray;I)Z
    .locals 5
    .param p1, "pinSnapshotIds"    # Landroid/util/SparseIntArray;
    .param p2, "defaultSnapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 511
    const/4 v1, 0x0

    .line 512
    .local v1, "changed":Z
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 513
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 514
    .local v0, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    invoke-direct {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->unpin(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Landroid/util/SparseIntArray;I)Z

    move-result v4

    or-int/2addr v1, v4

    .line 515
    goto :goto_0

    .line 516
    .end local v0    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_0
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    or-int/2addr v2, v1

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 517
    monitor-exit v3

    .line 518
    return v1

    .line 517
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method protected verifyBlobEntryRangeInBlobsFile(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V
    .locals 10
    .param p1, "blobEntry"    # Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
        }
    .end annotation

    .prologue
    .line 682
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getOffset()J

    move-result-wide v2

    .line 683
    .local v2, "offset":J
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v8

    .line 684
    .local v8, "size":J
    add-long v4, v2, v8

    .line 685
    .local v4, "limit":J
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 686
    .local v6, "fileLength":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 687
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getKey()[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;-><init>(Ljava/lang/String;JJJ)V

    throw v0

    .line 690
    :cond_1
    return-void
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
    .locals 18
    .param p1, "visitor"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    const/4 v12, 0x0

    .line 168
    .local v12, "entries":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v14

    .line 169
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v12

    .line 170
    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 176
    .local v13, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v15

    .line 177
    :try_start_1
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 178
    .local v3, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 179
    .local v11, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getReadTime()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v4

    .line 180
    .local v4, "readTime":J
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getWriteTime()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toMillis(J)J

    move-result-wide v6

    .line 181
    .local v6, "writeTime":J
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->getSize()J

    move-result-wide v8

    .line 182
    .local v8, "size":J
    iget-object v2, v11, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->pin:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    array-length v2, v2

    if-lez v2, :cond_0

    const/4 v10, 0x1

    .line 183
    .local v10, "pinned":Z
    :goto_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v2, p1

    .line 184
    invoke-interface/range {v2 .. v10}, Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;JJJZ)V

    goto :goto_0

    .line 170
    .end local v3    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v4    # "readTime":J
    .end local v6    # "writeTime":J
    .end local v8    # "size":J
    .end local v10    # "pinned":Z
    .end local v11    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 182
    .restart local v3    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .restart local v4    # "readTime":J
    .restart local v6    # "writeTime":J
    .restart local v8    # "size":J
    .restart local v11    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .restart local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    :cond_0
    const/4 v10, 0x0

    goto :goto_1

    .line 183
    .end local v3    # "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v4    # "readTime":J
    .end local v6    # "writeTime":J
    .end local v8    # "size":J
    .end local v11    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 186
    .end local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    :cond_1
    return-void
.end method

.method public writeStream(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/InputStream;)V
    .locals 16
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 756
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v6

    .line 757
    .local v6, "bytePool":Lcom/google/apps/dots/android/newsstand/util/BytePool;
    const/high16 v11, 0x10000

    invoke-virtual {v6, v11}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v7

    .line 758
    .local v7, "bytes":[B
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 763
    .local v3, "blobsBuffer":Ljava/nio/ByteBuffer;
    const/4 v11, 0x0

    :try_start_0
    array-length v12, v7

    move-object/from16 v0, p2

    invoke-static {v0, v7, v11, v12}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I

    move-result v10

    .line 764
    .local v10, "readCount":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->appendLock:Ljava/lang/Object;

    monitor-enter v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 768
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v11

    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 769
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v11}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 772
    .local v4, "blobOffset":J
    :goto_0
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 773
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 774
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v11, v3}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 776
    array-length v11, v7

    if-ge v10, v11, :cond_1

    .line 784
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->lock:Ljava/lang/Object;

    monitor-enter v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 785
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 788
    .local v9, "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    if-nez v9, :cond_2

    new-instance v2, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;-><init>()V

    .line 791
    .local v2, "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :goto_1
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isIsolated(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 792
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isolatedBlobFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)Ljava/io/File;

    move-result-object v8

    .line 793
    .local v8, "isolatedFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 794
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clearLocation()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 797
    .end local v8    # "isolatedFile":Ljava/io/File;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setKey([B)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v11

    .line 798
    invoke-virtual {v11, v4, v5}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setOffset(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->blobsChannel:Ljava/nio/channels/FileChannel;

    .line 799
    invoke-virtual {v14}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v14

    sub-long/2addr v14, v4

    invoke-virtual {v11, v14, v15}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setSize(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v11

    .line 800
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-static {v14, v15}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->toSeconds(J)J

    move-result-wide v14

    invoke-virtual {v11, v14, v15}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->setWriteTime(J)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    .line 803
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->entryMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v11, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->storeFileStats:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    invoke-virtual {v11, v9, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->replace(Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;)V

    .line 805
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->dirty:Z

    .line 806
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 807
    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 809
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    .line 811
    return-void

    .line 780
    .end local v2    # "blobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    .end local v9    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_1
    const/4 v11, 0x0

    :try_start_4
    array-length v13, v7

    move-object/from16 v0, p2

    invoke-static {v0, v7, v11, v13}, Lcom/google/common/io/ByteStreams;->read(Ljava/io/InputStream;[BII)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v10

    goto/16 :goto_0

    .line 788
    .restart local v9    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :cond_2
    :try_start_5
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;

    move-result-object v2

    goto :goto_1

    .line 806
    .end local v9    # "oldBlobEntry":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
    :catchall_0
    move-exception v11

    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v11

    .line 807
    .end local v4    # "blobOffset":J
    :catchall_1
    move-exception v11

    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 809
    .end local v10    # "readCount":I
    :catchall_2
    move-exception v11

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V

    throw v11
.end method
