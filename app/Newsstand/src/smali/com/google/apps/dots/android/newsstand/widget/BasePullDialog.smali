.class public abstract Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;
.super Landroid/widget/FrameLayout;
.source "BasePullDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;
    }
.end annotation


# static fields
.field private static final ARTICLE_TO_DIALOG_SCROLL_RATIO:F = 2.0f

.field private static final DROP_CARD_OVERHANG_DP:I = 0x0

.field private static final MAX_DISTANCE_TO_TRIGGER_DP:I = 0xc8

.field private static final MIN_DISTANCE_TO_TRIGGER_DP:I = 0x64

.field public static final TRANSLATE_DURATION:J = 0x12cL

.field private static final TRIGGER_SHRINK_DURATION_IN_MILLIS:I = 0xfa


# instance fields
.field private actionTriggerBar:Landroid/view/View;

.field protected final animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private articleScrollMaxY:F

.field private articleScrollMinY:F

.field protected bodyView:Landroid/widget/FrameLayout;

.field protected density:F

.field private downPosition:I

.field protected enableAccess:Z

.field protected headerView:Landroid/widget/FrameLayout;

.field protected initialEventY:F

.field private initialSnapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

.field private initialTranslationY:F

.field protected initialized:Z

.field protected isHidden:Z

.field private lastMaxArticleY:F

.field private mAccelerateInterpolator:Landroid/view/animation/Interpolator;

.field private mDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private mDistanceToTriggerMeterDp:F

.field private offscreenPosition:I

.field protected overlayView:Landroid/view/View;

.field protected final setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field protected snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

.field private trackingArticleScrollMovement:Z

.field private upPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x42c80000    # 100.0f

    const v4, 0x3ecccccd    # 0.4f

    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialSnapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 37
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 54
    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mDistanceToTriggerMeterDp:F

    .line 55
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 56
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mDecelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 58
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    .line 59
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialized:Z

    .line 60
    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->enableAccess:Z

    .line 65
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 92
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 93
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    sget v3, Lcom/google/android/apps/newsstanddev/R$layout;->pull_dialog:I

    invoke-virtual {v1, v3, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 94
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->dialog_header:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->headerView:Landroid/widget/FrameLayout;

    .line 95
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->dialog_body:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->bodyView:Landroid/widget/FrameLayout;

    .line 96
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->action_trigger:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    .line 98
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 99
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->density:F

    .line 103
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->density:F

    div-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    div-float v2, v3, v4

    .line 104
    .local v2, "threshold":F
    const/high16 v3, 0x43480000    # 200.0f

    .line 105
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mDistanceToTriggerMeterDp:F

    .line 106
    return-void
.end method

.method private cancelProgressTrigger()V
    .locals 4

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->trackingArticleScrollMovement:Z

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 234
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 235
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 236
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 238
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->trackingArticleScrollMovement:Z

    .line 239
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onResetTrigger()V

    .line 240
    return-void
.end method

.method private fadeOverlay()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 356
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v1, v2, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 360
    .local v0, "overlayAnimator":Landroid/view/ViewPropertyAnimator;
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 361
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v1, v3, :cond_3

    const/4 v1, 0x0

    .line 362
    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 363
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    .line 364
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 366
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 361
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1
.end method

.method private getSnapPointY()I
    .locals 2

    .prologue
    .line 318
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$3;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$BasePullDialog$SnapPoint:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 325
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->offscreenPosition:I

    :goto_0
    return v0

    .line 320
    :pswitch_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    goto :goto_0

    .line 322
    :pswitch_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private snapToPoint()V
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapToPoint(Landroid/animation/Animator$AnimatorListener;)V

    .line 300
    return-void
.end method

.method private startProgressTrigger()V
    .locals 2

    .prologue
    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->trackingArticleScrollMovement:Z

    .line 226
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 227
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 229
    return-void
.end method

.method private updateOverlayAlpha()V
    .locals 5

    .prologue
    .line 293
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 295
    .local v0, "alpha":F
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 296
    return-void
.end method


# virtual methods
.method protected doneLayout()Z
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x1

    return v0
.end method

.method public handleArticleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 166
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v5, v6, :cond_1

    :cond_0
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->enableAccess:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    if-eqz v5, :cond_2

    .line 221
    :cond_1
    :goto_0
    return v7

    .line 171
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 172
    .local v4, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 174
    :pswitch_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->startProgressTrigger()V

    .line 175
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMaxY:F

    .line 176
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMinY:F

    .line 177
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    .line 178
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getTranslationY()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialTranslationY:F

    .line 179
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->lastMaxArticleY:F

    goto :goto_0

    .line 183
    :pswitch_1
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->trackingArticleScrollMovement:Z

    if-eqz v5, :cond_3

    .line 184
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->cancelProgressTrigger()V

    .line 186
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapToPoint()V

    goto :goto_0

    .line 189
    :pswitch_2
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->trackingArticleScrollMovement:Z

    if-eqz v5, :cond_4

    .line 190
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->lastMaxArticleY:F

    sub-float v2, v4, v5

    .line 191
    .local v2, "verticalDistance":F
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->density:F

    div-float v3, v2, v5

    .line 192
    .local v3, "verticalDistanceDp":F
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mDecelerateInterpolator:Landroid/view/animation/Interpolator;

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->mDistanceToTriggerMeterDp:F

    div-float v6, v3, v6

    invoke-interface {v5, v6}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 195
    .local v0, "scaleX":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v0, v5

    if-lez v5, :cond_7

    .line 196
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->hide(Z)V

    .line 201
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->triggerUpdate(F)V

    .line 204
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->lastMaxArticleY:F

    sub-float v5, v4, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v1, v5, v6

    .line 205
    .local v1, "translation":F
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialTranslationY:F

    add-float/2addr v5, v1

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialTranslationY:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setTranslationY(F)V

    .line 209
    .end local v0    # "scaleX":F
    .end local v1    # "translation":F
    .end local v2    # "verticalDistance":F
    .end local v3    # "verticalDistanceDp":F
    :cond_4
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMaxY:F

    cmpl-float v5, v4, v5

    if-lez v5, :cond_5

    .line 210
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMaxY:F

    .line 211
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMinY:F

    .line 213
    :cond_5
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMinY:F

    cmpg-float v5, v4, v5

    if-gez v5, :cond_6

    .line 214
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMinY:F

    .line 215
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->articleScrollMaxY:F

    .line 217
    :cond_6
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->lastMaxArticleY:F

    cmpl-float v5, v4, v5

    if-lez v5, :cond_1

    .line 218
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->lastMaxArticleY:F

    goto/16 :goto_0

    .line 198
    .restart local v0    # "scaleX":F
    .restart local v2    # "verticalDistance":F
    .restart local v3    # "verticalDistanceDp":F
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->actionTriggerBar:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setScaleX(F)V

    goto :goto_1

    .line 172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public hide(Z)V
    .locals 1
    .param p1, "userDriven"    # Z

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    if-eqz v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    .line 269
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->cancelProgressTrigger()V

    .line 270
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->OFFSCREEN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 271
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onHide(Z)V

    .line 273
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapToPoint(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public isHidden()Z
    .locals 1

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setupScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 113
    return-void
.end method

.method protected onHide(Z)V
    .locals 0
    .param p1, "userDriven"    # Z

    .prologue
    .line 286
    return-void
.end method

.method protected onHideComplete()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 117
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 118
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->headerView:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    .line 119
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    div-int/lit8 v0, v0, 0x3

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    .line 120
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getBottom()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->offscreenPosition:I

    .line 121
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialized:Z

    if-nez v0, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->doneLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialized:Z

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setYToSnapPoint()V

    .line 127
    :cond_1
    return-void
.end method

.method protected onResetTrigger()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 388
    return-void
.end method

.method protected onSnapComplete()V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 135
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->enableAccess:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    if-eqz v1, :cond_1

    .line 136
    :cond_0
    const/4 v1, 0x0

    .line 156
    :goto_0
    return v1

    .line 138
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    sub-float v0, v1, v2

    .line 139
    .local v0, "translationSinceActionDown":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 156
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 141
    :pswitch_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialSnapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 142
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getTranslationY()F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialTranslationY:F

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    goto :goto_1

    .line 147
    :pswitch_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x41700000    # 15.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 148
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialSnapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    if-ne v1, v2, :cond_3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    :goto_2
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 150
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapToPoint()V

    goto :goto_1

    .line 148
    :cond_3
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    goto :goto_2

    .line 153
    :pswitch_2
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    :goto_3
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 154
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialTranslationY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->initialEventY:F

    sub-float/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setTranslationY(F)V

    goto :goto_1

    .line 153
    :cond_4
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    goto :goto_3

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected reset(Z)V
    .locals 1
    .param p1, "enableAccess"    # Z

    .prologue
    const/4 v0, 0x0

    .line 252
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setVisibility(I)V

    .line 253
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->isHidden:Z

    .line 255
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setEnableAccess(Z)V

    .line 258
    if-nez p1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 259
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setYToSnapPoint()V

    .line 260
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->updateAccess()V

    .line 261
    return-void

    .line 258
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->DOWN:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    goto :goto_0
.end method

.method public setEnableAccess(Z)V
    .locals 0
    .param p1, "enableAccess"    # Z

    .prologue
    .line 370
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->enableAccess:Z

    .line 371
    return-void
.end method

.method public setOverlayView(Landroid/view/View;)V
    .locals 1
    .param p1, "overlayView"    # Landroid/view/View;

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    .line 375
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 376
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 244
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 248
    :cond_0
    return-void
.end method

.method public setYToSnapPoint()V
    .locals 2

    .prologue
    .line 303
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$3;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$BasePullDialog$SnapPoint:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 314
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->onSnapComplete()V

    .line 315
    return-void

    .line 305
    :pswitch_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->offscreenPosition:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setY(F)V

    goto :goto_0

    .line 308
    :pswitch_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->downPosition:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setY(F)V

    goto :goto_0

    .line 311
    :pswitch_2
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->upPosition:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->setY(F)V

    goto :goto_0

    .line 303
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected snapToPoint(Landroid/animation/Animator$AnimatorListener;)V
    .locals 8
    .param p1, "optAnimatorListener"    # Landroid/animation/Animator$AnimatorListener;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v6, 0x12c

    .line 331
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->getSnapPointY()I

    move-result v1

    .line 332
    .local v1, "endPointY":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->fadeOverlay()V

    .line 333
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 334
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 335
    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    .line 336
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;)V

    .line 343
    .local v2, "snapCompleteRunnable":Ljava/lang/Runnable;
    invoke-virtual {p0, v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 344
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_0

    .line 345
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 347
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    invoke-static {v3, v0, p1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V

    .line 348
    return-void
.end method

.method protected triggerUpdate(F)V
    .locals 3
    .param p1, "update"    # F

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog;->overlayView:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    const v2, 0x3dcccccd    # 0.1f

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 384
    return-void
.end method

.method protected updateAccess()V
    .locals 0

    .prologue
    .line 380
    return-void
.end method
