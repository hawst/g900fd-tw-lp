.class Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;
.super Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;
.source "NSCardListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NSCardShuffleEvaluator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-direct {p0}, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$NSCardShuffleEvaluator;-><init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V

    return-void
.end method


# virtual methods
.method public isAnimationGroup(Landroid/view/ViewGroup;)Z
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 53
    instance-of v0, p1, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;

    if-nez v0, :cond_0

    .line 54
    invoke-super {p0, p1}, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;->isAnimationGroup(Landroid/view/ViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
