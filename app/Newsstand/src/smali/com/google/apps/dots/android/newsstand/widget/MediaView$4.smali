.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;
.super Ljava/lang/Object;
.source "MediaView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initTextureView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 489
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$602(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Z)Z

    .line 494
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->bindSurface()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$700(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    .line 495
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$800(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$900()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "onSurfaceTextureAvailable(): auto-starting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    .line 500
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$602(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Z)Z

    .line 513
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->unbindSurface()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    .line 514
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 508
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 504
    return-void
.end method
