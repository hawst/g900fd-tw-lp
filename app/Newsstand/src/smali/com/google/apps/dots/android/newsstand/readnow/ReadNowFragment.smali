.class public Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;
.super Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
.source "ReadNowFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private coloredOverlay:Landroid/view/View;

.field private currentEditionForHeader:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private currentHero:Landroid/view/View;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private fadeOverlayAnimator:Landroid/animation/Animator;

.field private headerAnimationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private heroAnimator:Landroid/animation/Animator;

.field private heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

.field private ripple:Landroid/animation/AnimatorSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>()V

    const-string v1, "ReadNowFragment_state"

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerAnimationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 88
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->DK_PAGER_TITLE:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->DK_PAGER_TITLE:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateImageRotatorColor()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateHero()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->startRippleAnimation()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeOutOverlay()V

    return-void
.end method

.method private fadeInOverlayAnimator()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 352
    const/4 v1, 0x1

    const-wide/16 v2, 0x190

    invoke-direct {p0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->overlayFadeAnimator(ZJ)Landroid/animation/Animator;

    move-result-object v0

    .line 353
    .local v0, "colorFade":Landroid/animation/Animator;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 359
    return-object v0
.end method

.method private fadeOutOverlay()V
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeOverlayAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeOverlayAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 346
    :cond_0
    const/4 v0, 0x0

    const-wide/16 v2, 0x190

    invoke-direct {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->overlayFadeAnimator(ZJ)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeOverlayAnimator:Landroid/animation/Animator;

    .line 347
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerAnimationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeOverlayAnimator:Landroid/animation/Animator;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    .line 349
    :cond_1
    return-void
.end method

.method private overlayFadeAnimator(ZJ)Landroid/animation/Animator;
    .locals 6
    .param p1, "fadeIn"    # Z
    .param p2, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 363
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .line 364
    .local v0, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    const-string v3, "alpha"

    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 365
    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getAlpha()I

    move-result v5

    aput v5, v4, v2

    const/4 v5, 0x1

    if-eqz p1, :cond_0

    const/16 v2, 0xff

    :cond_0
    aput v2, v4, v5

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 366
    .local v1, "colorFade":Landroid/animation/ValueAnimator;
    invoke-virtual {v1, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 367
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 368
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/animation/PlayInterpolators;->slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 370
    :cond_1
    return-object v1
.end method

.method private startRippleAnimation()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 295
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getHeroElementVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 297
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateImageRotatorColor()V

    .line 298
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 328
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    .line 305
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 309
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateOverlayColor()V

    .line 310
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    .line 311
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 324
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/Animator;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->fadeInOverlayAnimator()Landroid/animation/Animator;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x1

    .line 325
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroView:Landroid/view/View;

    invoke-static {v3, v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->getCenteredCircularReveal(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/animation/Animator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 326
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    invoke-static {v3, v4, v6}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->getScaleReveal(Landroid/content/Context;Landroid/view/View;Z)Landroid/animation/Animator;

    move-result-object v3

    aput-object v3, v1, v2

    .line 324
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 327
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerAnimationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->ripple:Landroid/animation/AnimatorSet;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    goto :goto_0
.end method

.method private updateControls()V
    .locals 5

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 271
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 270
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 272
    return-void
.end method

.method private updateEditionSummary()V
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 220
    return-void
.end method

.method private updateHeader()V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentEditionForHeader:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentEditionForHeader:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 224
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentEditionForHeader:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 230
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 231
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 235
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->getScaleReveal(Landroid/content/Context;Landroid/view/View;Z)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    .line 236
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerAnimationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroAnimator:Landroid/animation/Animator;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    goto :goto_0

    .line 244
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateHero()V

    goto :goto_0
.end method

.method private updateHero()V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 267
    return-void
.end method

.method private updateImageRotatorColor()V
    .locals 3

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateImageRotatorColor(I)V

    .line 375
    return-void
.end method

.method private updateImageRotatorColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 378
    .line 379
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$integer;->subscribed_topic_black_percentage:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    const/4 v2, 0x1

    .line 378
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilterPercentBlack(IIZ)Landroid/graphics/ColorFilter;

    move-result-object v0

    .line 380
    .local v0, "colorFilter":Landroid/graphics/ColorFilter;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 381
    return-void
.end method

.method private updateOverlayColor()V
    .locals 5

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    .line 333
    .local v0, "color":I
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 334
    .local v1, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    .line 335
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->topic_image_rotator_overlay_color:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 334
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ColorDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 337
    const/16 v2, 0x55

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 338
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 339
    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 124
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->image_rotator_background:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->backgroundView:Landroid/view/View;

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->backgroundView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->header_image_rotator:I

    .line 126
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    .line 129
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->image_rotator_overlay_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->backgroundView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->colored_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->coloredOverlay:Landroid/view/View;

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->backgroundView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 132
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 118
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_hero:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroView:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 120
    return-void
.end method

.method protected currentPageIndex()I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 155
    .local v1, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 156
    .local v0, "logicalPosition":I
    if-ne v0, v2, :cond_0

    .line 157
    sget-object v3, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to find page for edition: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 160
    .end local v0    # "logicalPosition":I
    :cond_0
    return v0
.end method

.method protected dataListId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Object;
    .locals 0
    .param p1, "pageEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 195
    return-object p1
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 276
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 277
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_read_now"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v1, "editionInfo"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected onPageListChanged()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateEditionSummary()V

    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateHeader()V

    .line 149
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateControls()V

    .line 150
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onViewCreated(Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->image_header_logo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->round_topic_header_logo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    .line 113
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->setupActionBar(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 114
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->currentHero:Landroid/view/View;

    goto :goto_0
.end method

.method protected pageFragment(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 200
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;-><init>()V

    .line 201
    .local v1, "plainEditionFragment":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->DK_EDITION:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 202
    .local v0, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setInitialState(Landroid/os/Parcelable;)V

    .line 204
    return-object v1
.end method

.method protected pageList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 3

    .prologue
    .line 166
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$1;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 138
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    check-cast p2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    .line 139
    .end local p2    # "state":Landroid/os/Parcelable;
    invoke-virtual {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setReadNowFragmentState(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    .line 140
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 137
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .restart local p2    # "state":Landroid/os/Parcelable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    move-result-object v0

    return-object v0
.end method

.method protected stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    .locals 3
    .param p1, "logicalPosition"    # I

    .prologue
    .line 284
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 285
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 286
    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/topics/CurationSubscriptionsList;->DK_EDITION:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 287
    .local v1, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 289
    .end local v1    # "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;-><init>()V

    goto :goto_0
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;)V

    return-void
.end method

.method protected bridge synthetic updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;

    .prologue
    .line 209
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V

    .line 210
    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 211
    .local v0, "pageChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 212
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateEditionSummary()V

    .line 213
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateHeader()V

    .line 214
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowFragment;->updateControls()V

    .line 216
    :cond_1
    return-void

    .line 210
    .end local v0    # "pageChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
