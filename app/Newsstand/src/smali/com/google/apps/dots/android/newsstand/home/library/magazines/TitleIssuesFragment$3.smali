.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;
.super Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;
.source "TitleIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->updateAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method


# virtual methods
.method protected getComparatorForSortingClusters()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;)V

    return-object v0
.end method

.method protected getStringToClusterOn(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/String;
    .locals 7
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 195
    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 196
    .local v2, "millis":J
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->calendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 197
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->calendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;)Ljava/util/Calendar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 201
    .local v4, "year":I
    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_TITLE:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "issueTitle":Ljava/lang/String;
    add-int/lit8 v5, v4, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "nextYear":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 206
    .end local v1    # "nextYear":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "nextYear":Ljava/lang/String;
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "rowData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "stringToClusterOn"    # Ljava/lang/String;
    .param p3, "isFirstHeader"    # Z

    .prologue
    .line 213
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/ClusteredGridGroup;->makeShelfHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 214
    .local v0, "shelfHeader":Lcom/google/android/libraries/bind/data/Data;
    if-eqz p3, :cond_0

    .line 215
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TEXT_COLOR:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->play_header_list_banner_text_color:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 216
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfHeader;->DK_TITLE_TRANSITION_NAME:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/TitleIssuesFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 218
    :cond_0
    return-object v0
.end method
