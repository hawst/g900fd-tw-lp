.class public abstract Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "NSActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;,
        Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;
    }
.end annotation


# static fields
.field private static final CUSTOM_VIEW_PARAMS:Landroid/view/ViewGroup$LayoutParams;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static currentActivity:Landroid/app/Activity;


# instance fields
.field protected actionBarBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private final activityResultHandlers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

.field private currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

.field private customView:Landroid/view/View;

.field private customViewCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field public final handler:Landroid/os/Handler;

.field private isTransitioningToExit:Z

.field private orientationEventListener:Landroid/view/OrientationEventListener;

.field private overlayViewHelper:Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

.field private progressDialogRunnable:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;

.field private rootView:Landroid/view/View;

.field public final stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private textToSpeech:Landroid/speech/tts/TextToSpeech;

.field private textToSpeechInitialized:Z

.field private final utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 72
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 105
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->CUSTOM_VIEW_PARAMS:Landroid/view/ViewGroup$LayoutParams;

    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 1
    .param p1, "isUserActivity"    # Z

    .prologue
    .line 166
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 94
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CONSTRUCTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 122
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 128
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->handler:Landroid/os/Handler;

    .line 130
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityResultHandlers:Landroid/util/SparseArray;

    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->overlayViewHelper:Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

    .line 167
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 168
    return-void

    .line 167
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeechInitialized:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/libraries/bind/collections/RingBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    return-object p1
.end method

.method private addActionableToastBar(Landroid/view/View;)V
    .locals 3
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 266
    instance-of v2, p1, Landroid/view/ViewGroup;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    move-object v0, p1

    .line 268
    check-cast v0, Landroid/view/ViewGroup;

    .line 270
    .local v0, "rootViewGroup":Landroid/view/ViewGroup;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;-><init>(Landroid/content/Context;)V

    .line 273
    .local v1, "toastBar":Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->actionable_toast_bar:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->setId(I)V

    .line 275
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 276
    return-void
.end method

.method private destroyTextToSpeech()V
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 586
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 589
    :cond_0
    return-void
.end method

.method public static getCurrentActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 492
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private initTextToSpeech()V
    .locals 2

    .prologue
    .line 564
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isBlindAccessibilityEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-direct {v0, p0, v1}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 581
    :cond_0
    return-void
.end method


# virtual methods
.method public addWindowOverlayView(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "layoutParams"    # Landroid/widget/RelativeLayout$LayoutParams;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->overlayViewHelper:Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->addWindowOverlayView(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 758
    return-void
.end method

.method public configureOrientationPreference(ZZ)V
    .locals 6
    .param p1, "hasLandscape"    # Z
    .param p2, "hasPortrait"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 615
    .line 616
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "accelerometer_rotation"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_1

    move v1, v2

    .line 620
    .local v1, "rotationLocked":Z
    :goto_0
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    .line 621
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-eq v4, v5, :cond_0

    .line 622
    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->needed_to_rotate_orientation:I

    invoke-static {p0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 624
    :cond_0
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setRequestedOrientation(I)V

    .line 635
    :goto_2
    return-void

    .end local v1    # "rotationLocked":Z
    :cond_1
    move v1, v3

    .line 616
    goto :goto_0

    .line 617
    :catch_0
    move-exception v0

    .line 618
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const/4 v1, 0x0

    .restart local v1    # "rotationLocked":Z
    goto :goto_0

    .line 624
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    const/4 v3, 0x6

    goto :goto_1

    .line 626
    :cond_3
    if-nez p1, :cond_6

    if-eqz p2, :cond_6

    .line 627
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-eq v3, v4, :cond_4

    .line 628
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->needed_to_rotate_orientation:I

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 630
    :cond_4
    if-eqz v1, :cond_5

    :goto_3
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setRequestedOrientation(I)V

    goto :goto_2

    :cond_5
    const/4 v2, 0x7

    goto :goto_3

    .line 633
    :cond_6
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setRequestedOrientation(I)V

    goto :goto_2
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 742
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->finish()V

    .line 743
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isTransitioningToExit:Z

    .line 744
    return-void
.end method

.method protected abstract getActionBarDisplayOptions()I
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    return-object v0
.end method

.method protected getContextForSearchView()Landroid/content/Context;
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 296
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    .line 299
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentDisplayOrientationDegrees()I
    .locals 3

    .prologue
    .line 660
    const-string v2, "window"

    .line 661
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 662
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 664
    .local v1, "screenOrientation":I
    packed-switch v1, :pswitch_data_0

    .line 667
    const/4 v2, 0x0

    .line 673
    :goto_0
    return v2

    .line 669
    :pswitch_0
    const/16 v2, 0x5a

    goto :goto_0

    .line 671
    :pswitch_1
    const/16 v2, 0xb4

    goto :goto_0

    .line 673
    :pswitch_2
    const/16 v2, 0x10e

    goto :goto_0

    .line 664
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract getHelpFeedbackInfo()Landroid/os/Bundle;
.end method

.method public getManagedMediaPlayer(Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    .locals 4
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;

    .prologue
    const/4 v1, 0x0

    .line 685
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isActivityResumed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 686
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->getInstance(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    move-result-object v0

    .line 688
    .local v0, "mediaPlayer":Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 689
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 690
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$5;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$5;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setOnReleaseListener(Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;)V

    .line 705
    .end local v0    # "mediaPlayer":Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    return-object v1

    .line 699
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v2, :cond_1

    .line 700
    sget-object v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Found unreleased MediaPlayer while not in foreground"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 701
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->release()V

    .line 702
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    goto :goto_0
.end method

.method public hideCustomView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 421
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 423
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customView:Landroid/view/View;

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customViewCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customViewCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 427
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customViewCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 429
    :cond_1
    return-void
.end method

.method public hideLoadingDialog()V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->progressDialogRunnable:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->progressDialogRunnable:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 392
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->progressDialogRunnable:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;->hide()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->progressDialogRunnable:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ProgressDialogRunnable;

    .line 395
    :cond_0
    return-void
.end method

.method public isActivityResumed()Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityStarted()Z
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCustomViewShowing()Z
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTransitioningToExit()Z
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isTransitioningToExit:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 364
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 366
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityResultHandlers:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;

    .line 367
    .local v0, "handler":Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;
    if-eqz v0, :cond_0

    .line 368
    invoke-interface {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;->onActivityResult(IILandroid/content/Intent;)V

    .line 370
    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 497
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onAttachedToWindow()V

    .line 498
    sput-object p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentActivity:Landroid/app/Activity;

    .line 499
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 218
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onCreate: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CREATED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 220
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setVolumeControlStream(I)V

    .line 221
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->initTextToSpeech()V

    .line 222
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 223
    return-void
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 473
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onDestroy: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 474
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->DESTROYED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 475
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isCustomViewShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideCustomView()V

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->overlayViewHelper:Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->onDestroy()V

    .line 479
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 480
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->destroyTextToSpeech()V

    .line 481
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isCustomViewShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideCustomView()V

    .line 506
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDetachedFromWindow()V

    .line 507
    const/4 v0, 0x0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentActivity:Landroid/app/Activity;

    .line 508
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 433
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isCustomViewShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideCustomView()V

    .line 435
    const/4 v0, 0x1

    .line 437
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 173
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->removeBackstackExtra(Landroid/content/Intent;)V

    .line 174
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setIntent(Landroid/content/Intent;)V

    .line 175
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 538
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 539
    .local v0, "itemId":I
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_search:I

    if-ne v0, v3, :cond_0

    .line 541
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->start()V

    .line 560
    :goto_0
    return v2

    .line 543
    :cond_0
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_settings:I

    if-ne v0, v3, :cond_1

    .line 544
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/SettingsIntentBuilder;->start()V

    goto :goto_0

    .line 546
    :cond_1
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_helpfeedback:I

    if-ne v0, v3, :cond_2

    .line 547
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->launchHelpFeedback(Landroid/app/Activity;)V

    goto :goto_0

    .line 549
    :cond_2
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_share:I

    if-ne v0, v3, :cond_3

    .line 552
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 553
    .local v1, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->startActivity(Landroid/content/Intent;)V

    .line 557
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat;->setThreadPolicyDelayed(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 560
    .end local v1    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    :cond_3
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 442
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPause: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 443
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 444
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isCustomViewShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideCustomView()V

    .line 447
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 448
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->hideLoadingDialog()V

    .line 451
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->release()V

    .line 453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->currentMediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_2

    .line 458
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 460
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 242
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->RESUMED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 243
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 244
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onResume: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 250
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 227
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->STARTED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 228
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 229
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStart: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 231
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/NSApplication;->setVisible(Z)V

    .line 232
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setUpActionBar()V

    .line 234
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 235
    new-instance v0, Landroid/app/ActivityManager$TaskDescription;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->launcher_app_name:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 236
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material_dark:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 235
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 238
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 464
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStop: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;->CREATED:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityState:Lcom/google/apps/dots/android/newsstand/activity/NSActivity$ActivityState;

    .line 466
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/NSApplication;->setVisible(Z)V

    .line 467
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 468
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    .line 469
    return-void
.end method

.method protected removeBackstackExtra(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 179
    const-string v0, "addToBackStack"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public removeOrientationListener(Landroid/view/OrientationEventListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/OrientationEventListener;

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-ne p1, v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 655
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 657
    :cond_0
    return-void
.end method

.method public removeWindowOverlayView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->overlayViewHelper:Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/OverlayViewHelper;->removeWindowOverlayView(Landroid/view/View;)V

    .line 762
    return-void
.end method

.method public setActionBarBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 709
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->actionBarBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 710
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 711
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 731
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 732
    return-void
.end method

.method public setContentView(I)V
    .locals 1
    .param p1, "layoutResID"    # I

    .prologue
    .line 254
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->setContentView(I)V

    .line 255
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    .line 257
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->addActionableToastBar(Landroid/view/View;)V

    .line 258
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 280
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->setContentView(Landroid/view/View;)V

    .line 281
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    .line 282
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 286
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 287
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    .line 288
    return-void
.end method

.method public setOrientationListener(Landroid/view/OrientationEventListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/OrientationEventListener;

    .prologue
    .line 641
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 645
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 646
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 647
    return-void
.end method

.method public setResultHandlerForActivityCode(ILcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "handler"    # Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->activityResultHandlers:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 360
    return-void
.end method

.method protected setUpActionBar()V
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 312
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getActionBarDisplayOptions()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(I)V

    .line 315
    :cond_0
    return-void
.end method

.method public showCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "callback"    # Landroid/webkit/WebChromeClient$CustomViewCallback;

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isCustomViewShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    .line 404
    :cond_0
    if-eqz p2, :cond_1

    .line 406
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$2;

    invoke-direct {v1, p0, p2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$2;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 418
    :cond_1
    :goto_0
    return-void

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->rootView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->CUSTOM_VIEW_PARAMS:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customView:Landroid/view/View;

    .line 417
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->customViewCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    goto :goto_0
.end method

.method public speak(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 604
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeechInitialized:Z

    if-eqz v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public supportFinishAfterTransition()V
    .locals 1

    .prologue
    .line 736
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isTransitioningToExit:Z

    .line 737
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->supportFinishAfterTransition()V

    .line 738
    return-void
.end method
