.class public Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;
.super Ljava/lang/Object;
.source "PageFractionHelper.java"


# instance fields
.field private final account:Landroid/accounts/Account;

.field private final postId:Ljava/lang/String;

.field private queuedPageFraction:F

.field private final storePageFractionRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "postId"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->account:Landroid/accounts/Account;

    .line 26
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->postId:Ljava/lang/String;

    .line 27
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->storePageFractionRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->storePageFraction()V

    return-void
.end method

.method private storePageFraction()V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->account:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->postId:Ljava/lang/String;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->queuedPageFraction:F

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->setPageFractionForPost(Landroid/accounts/Account;Ljava/lang/String;F)V

    .line 46
    return-void
.end method


# virtual methods
.method public queueStorePageFraction(F)V
    .locals 4
    .param p1, "pageFraction"    # F

    .prologue
    .line 40
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->queuedPageFraction:F

    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->storePageFractionRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v2, 0xfa

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 42
    return-void
.end method
