.class public Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;
.super Ljava/lang/Object;
.source "NSCookiePolicy.java"

# interfaces
.implements Ljava/net/CookiePolicy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$CheckAdTrackingSettingReceiver;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final uris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->uris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$CheckAdTrackingSettingReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$CheckAdTrackingSettingReceiver;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.apps.dots.android.newsstand.NSApplication.action.APPLICATION_VISIBLE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->checkIsLimitAdTrackingEnabled(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->checkIsLimitAdTrackingEnabled(Landroid/content/Context;)V

    return-void
.end method

.method private checkIsLimitAdTrackingEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 56
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CACHE_WARMUP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 75
    return-void
.end method


# virtual methods
.method public isAcceptingCookies()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldAccept(Ljava/net/URI;Ljava/net/HttpCookie;)Z
    .locals 1
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "cookie"    # Ljava/net/HttpCookie;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isAcceptingCookies()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->uris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->isGoogleAdHost(Ljava/net/URI;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
