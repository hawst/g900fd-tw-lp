.class final Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;
.super Ljava/lang/Object;
.source "MagazinesUserContentService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMagazinesForWidget(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v3, 0x0

    .line 372
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failure getting a ClientConfig, we will not fetch MyOffers or MyMagazines."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    # setter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetching:Z
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$702(Z)Z

    .line 374
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    .locals 1
    .param p1, "config"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;->val$context:Landroid/content/Context;

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMyOffersInternal(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$600(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    .line 368
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 364
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$6;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    return-void
.end method
