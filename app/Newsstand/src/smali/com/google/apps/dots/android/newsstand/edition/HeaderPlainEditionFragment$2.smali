.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateActionBarTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 177
    if-eqz p1, :cond_1

    .line 178
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->val$context:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "editionTitle":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->updateContentDescriptionForActivityContentView(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 187
    .end local v0    # "editionTitle":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTitleHint()Ljava/lang/String;

    move-result-object v1

    .line 183
    .local v1, "editionTitleHint":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
