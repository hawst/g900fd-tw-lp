.class Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$2;
.super Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.source "LibraryPageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$2;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->buildLibraryPageList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
