.class Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;
.source "CuratedTopicList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;->visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;->this$0:Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    const-string v0, "[Topic] - "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;->this$0:Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 39
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected ignoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;->this$0:Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIgnoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    return-object v0
.end method
