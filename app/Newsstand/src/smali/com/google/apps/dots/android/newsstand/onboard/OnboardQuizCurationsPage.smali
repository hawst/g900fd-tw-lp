.class public Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;
.super Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;
.source "OnboardQuizCurationsPage.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;


# instance fields
.field private backPressGoesToTutorial:Z

.field private final lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private quizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

.field private showMagazineOffersNext:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    .line 61
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Lcom/google/android/play/onboard/OnboardHostControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mModifiedByUser:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->mModifiedByUser:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getCurationSubscriptions()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v5

    iget-object v2, v5, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 162
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 164
    .local v4, "subscriptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 165
    .local v1, "item":Lcom/google/android/libraries/bind/data/Data;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    .line 166
    invoke-virtual {v1, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    sget v6, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    .line 167
    invoke-virtual {v1, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    sget v7, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_SUMMARY:I

    .line 168
    invoke-virtual {v1, v7}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v0, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    .line 169
    .local v0, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;

    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_DEFAULT_ON:I

    .line 170
    invoke-virtual {v1, v5, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v5

    sget v6, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    .line 171
    invoke-virtual {v1, v6, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v6

    invoke-direct {v3, v0, v5, v6}, Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    .line 172
    .local v3, "subscription":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    .end local v0    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .end local v1    # "item":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "subscription":Lcom/google/apps/dots/android/newsstand/onboard/EditionSubscriptionInfo;
    :cond_0
    return-object v4
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 4
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 215
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    .line 216
    .local v0, "info":Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 217
    new-instance v1, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    .line 229
    :goto_0
    return-object v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    new-instance v1, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    .line 221
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->play_onboard_button_next:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_chevron_end_wht_24dp:I

    .line 222
    invoke-virtual {v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$3;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$3;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    goto :goto_0

    .line 229
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    goto :goto_0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->showMagazineOffersNext:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    const/4 v0, 0x2

    .line 210
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->onboard_quiz_curations_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNumColumns()I
    .locals 5

    .prologue
    const/4 v2, 0x4

    .line 143
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v3, v4, :cond_1

    const/4 v1, 0x1

    .line 144
    .local v1, "isPortrait":Z
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 145
    .local v0, "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$4;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 151
    if-eqz v1, :cond_0

    const/4 v2, 0x3

    :cond_0
    :goto_1
    return v2

    .line 143
    .end local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v1    # "isPortrait":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 147
    .restart local v0    # "deviceCategory":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v1    # "isPortrait":Z
    :pswitch_0
    if-nez v1, :cond_0

    const/4 v2, 0x5

    goto :goto_1

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->quizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 95
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->defaultSubscriptionsList()Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->EQUALITY_FIELDS:[I

    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$2;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->quizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->quizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->onAttachedToWindow()V

    .line 66
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 67
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->rawOffersList()Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 68
    .local v0, "freeMagazineOffers":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->whenDataListFirstRefreshed(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;Lcom/google/android/libraries/bind/data/DataList;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 76
    return-void
.end method

.method public onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->backPressGoesToTutorial:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 81
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->onDetachedFromWindow()V

    .line 82
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 87
    sget-object v3, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_SEQUENCE_TYPE:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 88
    .local v0, "sequenceType":I
    sget-object v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v4, "sequenceType = %d"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    if-nez v0, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->backPressGoesToTutorial:Z

    .line 90
    return-void

    :cond_0
    move v1, v2

    .line 89
    goto :goto_0
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 181
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->STATE_CURATION_SUBS:Ljava/lang/String;

    .line 182
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizCurationsPage;->getCurationSubscriptions()Ljava/util/ArrayList;

    move-result-object v1

    .line 181
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 183
    return-void
.end method

.method protected sendAnalyticsEvent()V
    .locals 2

    .prologue
    .line 187
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizCurationsScreen;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizCurationsScreen;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizCurationsScreen;->track(Z)V

    .line 188
    return-void
.end method
