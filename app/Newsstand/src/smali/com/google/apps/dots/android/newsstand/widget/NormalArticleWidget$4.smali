.class Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;
.super Ljava/lang/Object;
.source "NormalArticleWidget.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->resetJsonStore()V

    .line 415
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->storeLayoutParameters()V

    .line 416
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->notifyOnLoadStart()V

    .line 417
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq v0, v1, :cond_1

    .line 418
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->applyPageFractionAfterLayout:Z

    .line 422
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->currPage:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->articleState:Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget$ArticleState;->pageFraction:F

    .line 423
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setupWebView(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    goto :goto_0
.end method
