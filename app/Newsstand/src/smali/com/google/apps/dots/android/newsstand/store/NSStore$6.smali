.class Lcom/google/apps/dots/android/newsstand/store/NSStore$6;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/server/Transform;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 390
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failed to get original transform for %s. Exception: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 395
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/server/Transform;)V
    .locals 3
    .param p1, "originalTransform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/NSStore;->canFallBackToOriginalTransform(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;)Z
    invoke-static {v0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1200(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->val$combinedResponseFuture:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Failing so that client-side transform can proceed"

    invoke-direct {v1, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 386
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 379
    check-cast p1, Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;->onSuccess(Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    return-void
.end method
