.class public final enum Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
.super Ljava/lang/Enum;
.source "ColorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ColorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TopicColor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum AQUA:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum BLUE1:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum BLUE2:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum BROWN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum DEFAULT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum DEFAULT_UNSUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum LIGHT_GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum MAROON:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum NAVY:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum ORANGE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum PURPLE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

.field public static final enum RED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;


# instance fields
.field public final accentColorResId:I

.field public final colorResId:I

.field public final textColorResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "BROWN"

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->card_background_brown:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_brown_dark:I

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BROWN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "GREEN"

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->card_background_green:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_green_dark:I

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "NAVY"

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->card_background_navy:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_navy_dark:I

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->NAVY:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "BLUE1"

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->card_background_blue1:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_blue1_dark:I

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE1:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "BLUE2"

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->card_background_blue2:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_blue2_dark:I

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE2:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 36
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "AQUA"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_aqua:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_aqua_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->AQUA:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 37
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "PURPLE"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_purple:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_purple_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->PURPLE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "MAROON"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_maroon:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_maroon_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->MAROON:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "RED"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_red:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_red_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->RED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "ORANGE"

    const/16 v2, 0x9

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_orange:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_orange_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->ORANGE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 41
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "LIGHT_GREEN"

    const/16 v2, 0xa

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_light_green:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_background_light_green_dark:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->LIGHT_GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 42
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "DEFAULT_UNSUBSCRIBED"

    const/16 v2, 0xb

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_label_color_default:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->card_label_color_default_dark:I

    sget v5, Lcom/google/android/apps/newsstanddev/R$color;->card_tag_label_color_unsubscribed:I

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_UNSUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 44
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    const-string v1, "DEFAULT_SUBSCRIBED"

    const/16 v2, 0xc

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->NAVY:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 30
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BROWN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->NAVY:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE1:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->BLUE2:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->AQUA:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->PURPLE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->MAROON:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->RED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->ORANGE:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->LIGHT_GREEN:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_UNSUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 6
    .param p3, "colorResId"    # I
    .param p4, "accentColorResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 58
    const v5, 0x106000b

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;IIII)V

    .line 59
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p3, "colorResId"    # I
    .param p4, "accentColorResId"    # I
    .param p5, "textColorResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->colorResId:I

    .line 53
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->accentColorResId:I

    .line 54
    iput p5, p0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->textColorResId:I

    .line 55
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;)V
    .locals 6
    .param p3, "other"    # Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    iget v3, p3, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->colorResId:I

    iget v4, p3, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->accentColorResId:I

    iget v5, p3, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->textColorResId:I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;-><init>(Ljava/lang/String;IIII)V

    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    return-object v0
.end method
