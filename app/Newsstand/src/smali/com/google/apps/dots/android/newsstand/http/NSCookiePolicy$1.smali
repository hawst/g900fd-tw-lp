.class Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;
.super Ljava/lang/Object;
.source "NSCookiePolicy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->checkIsLimitAdTrackingEnabled(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 61
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v1

    .line 62
    .local v1, "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$100(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 63
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "limit ad tracking set to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->isLimitAdTrackingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$100(Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_3

    .line 73
    .end local v1    # "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Error initializing cookie policy: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 67
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Error initializing cookie policy: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    .end local v0    # "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    :catch_2
    move-exception v0

    .line 69
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Error initializing cookie policy: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 71
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Error initializing cookie policy: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
