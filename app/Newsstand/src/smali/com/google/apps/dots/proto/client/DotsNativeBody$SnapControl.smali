.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SnapControl"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;


# instance fields
.field private bitField0_:I

.field private type_:I

.field private unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1905
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1906
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1915
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    .line 1906
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 3

    .prologue
    .line 1964
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1968
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v2, :cond_0

    .line 1969
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1971
    :cond_0
    return-object v0

    .line 1965
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :catch_0
    move-exception v1

    .line 1966
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1902
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1976
    if-ne p1, p0, :cond_1

    .line 1980
    :cond_0
    :goto_0
    return v1

    .line 1977
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1978
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1979
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 1980
    goto :goto_0

    .line 1979
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1980
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2003
    const/4 v0, 0x0

    .line 2004
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2005
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    .line 2006
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2008
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v1, :cond_1

    .line 2009
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 2010
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2012
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->cachedSize:I

    .line 2013
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1917
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    return v0
.end method

.method public getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 1

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    return-object v0
.end method

.method public hasUnzoomedPoint()Z
    .locals 1

    .prologue
    .line 1946
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1984
    const/16 v0, 0x11

    .line 1985
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1986
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    add-int v0, v1, v2

    .line 1987
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 1988
    return v0

    .line 1987
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2021
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2022
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2026
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2027
    :sswitch_0
    return-object p0

    .line 2032
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2033
    .local v1, "temp":I
    if-nez v1, :cond_1

    .line 2034
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    .line 2035
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    goto :goto_0

    .line 2037
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    goto :goto_0

    .line 2042
    .end local v1    # "temp":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v2, :cond_2

    .line 2043
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 2045
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2022
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1902
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v0

    return-object v0
.end method

.method public setType(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1920
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    .line 1921
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    .line 1922
    return-object p0
.end method

.method public setUnzoomedPoint(Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .prologue
    .line 1939
    if-nez p1, :cond_0

    .line 1940
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1942
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1943
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1993
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1994
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1996
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v0, :cond_1

    .line 1997
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->unzoomedPoint_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1999
    :cond_1
    return-void
.end method
