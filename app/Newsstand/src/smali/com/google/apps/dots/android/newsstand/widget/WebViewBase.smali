.class public abstract Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;
.super Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;
.source "WebViewBase.java"


# static fields
.field private static final JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

.field private static didSetRenderPriority:Z


# instance fields
.field private destroyAllowed:Z

.field private final jsErrorMatcher:Ljava/util/regex/Matcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, ".*(Syntax|Type|Reference)Error.*"

    .line 28
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->didSetRenderPriority:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;-><init>(Landroid/content/Context;)V

    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->jsErrorMatcher:Ljava/util/regex/Matcher;

    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->initWebViewSettings()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->jsErrorMatcher:Ljava/util/regex/Matcher;

    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->initWebViewSettings()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->JS_ERROR_PATTERN:Ljava/util/regex/Pattern;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->jsErrorMatcher:Ljava/util/regex/Matcher;

    .line 46
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->initWebViewSettings()V

    .line 47
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->destroyAllowed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;Landroid/webkit/ConsoleMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;
    .param p1, "x1"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onJsError(Landroid/webkit/ConsoleMessage;)V

    return-void
.end method

.method private onJsError(Landroid/webkit/ConsoleMessage;)V
    .locals 6
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 152
    const/4 v1, 0x0

    .line 154
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "messageText":Ljava/lang/String;
    const-string v2, "dots is not defined"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    new-instance v1, Ljava/net/SocketException;

    .end local v1    # "t":Ljava/lang/Throwable;
    const-string v2, "Failed to load layout engine."

    invoke-direct {v1, v2}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    .line 164
    .restart local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->shouldFailOnJsError()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onLayoutFailed(Ljava/lang/Throwable;)V

    .line 167
    :cond_1
    return-void

    .line 159
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->jsErrorMatcher:Ljava/util/regex/Matcher;

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    new-instance v1, Lcom/google/apps/dots/android/newsstand/exception/LayoutEngineException;

    .end local v1    # "t":Ljava/lang/Throwable;
    const-string v2, "%s, line %d: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->sourceId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 161
    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    .line 160
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/exception/LayoutEngineException;-><init>(Ljava/lang/String;)V

    .restart local v1    # "t":Ljava/lang/Throwable;
    goto :goto_0
.end method


# virtual methods
.method public final destroy()V
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->destroyAllowed:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 97
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->destroy()V

    .line 98
    return-void
.end method

.method protected initWebViewSettings()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 57
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 58
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 59
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 60
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 61
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 62
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 64
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 65
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 67
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 68
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 69
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 71
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v1, v2, :cond_0

    .line 72
    const-string v1, "use_minimal_memory"

    const-string v2, "false"

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setWebSettingsProperty(Landroid/webkit/WebSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 85
    sget-boolean v1, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->didSetRenderPriority:Z

    if-nez v1, :cond_1

    .line 87
    sput-boolean v3, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->didSetRenderPriority:Z

    .line 88
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 90
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onAttachedToWindow()V

    .line 139
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 140
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    if-eqz v0, :cond_0

    .line 141
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$3;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 149
    :cond_0
    return-void
.end method

.method protected abstract onLayoutFailed(Ljava/lang/Throwable;)V
.end method

.method protected onReallyDestroy()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method protected abstract shouldFailOnJsError()Z
.end method

.method public startDestroy()V
    .locals 4

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->stopLoading()V

    .line 116
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onPause()V

    .line 117
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    return-void
.end method
