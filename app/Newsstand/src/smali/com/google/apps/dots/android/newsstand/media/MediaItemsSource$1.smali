.class final Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$1;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "MediaItemsSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v0, 0x0

    .line 54
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    .line 55
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    sget v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_IS_METERED:I

    .line 56
    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 8
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 65
    .local v1, "audioDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 66
    .local v2, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v6, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 67
    .local v4, "postId":Ljava/lang/String;
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 68
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 72
    .local v0, "audioData":Lcom/google/android/libraries/bind/data/Data;
    sget v6, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_ID:I

    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->audioId(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 74
    sget v6, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    invoke-virtual {v0, v6, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 75
    sget v6, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_INDEX:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 76
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "audioData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "index":I
    .end local v4    # "postId":Ljava/lang/String;
    :cond_1
    return-object v1
.end method
