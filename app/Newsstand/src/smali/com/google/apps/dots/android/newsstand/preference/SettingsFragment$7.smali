.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupOpenSourcePreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 277
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->open_source_licenses_url:I

    .line 278
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->start()V

    .line 280
    const/4 v0, 0x1

    return v0
.end method
