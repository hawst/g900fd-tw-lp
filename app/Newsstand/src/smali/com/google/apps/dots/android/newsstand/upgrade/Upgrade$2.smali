.class Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;
.super Ljava/lang/Object;
.source "Upgrade.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->upgradeDiskCacheIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->moveDiskCacheDir(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$200(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/io/File;)V

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    # invokes: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->moveDiskCacheDir(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$200(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$300(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->set(Ljava/lang/Object;)Z

    .line 83
    return-void

    .line 79
    :catch_0
    move-exception v0

    goto :goto_0
.end method
