.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState$1;
.super Ljava/lang/Object;
.source "MyMagazinesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 56
    .local v0, "showArchive":Z
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;-><init>(Z)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 61
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    move-result-object v0

    return-object v0
.end method
