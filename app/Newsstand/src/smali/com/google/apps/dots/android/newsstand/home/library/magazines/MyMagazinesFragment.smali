.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "MyMagazinesFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final GROUPING_FILTER_EQUALITY_FIELDS:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final archiveListener:Landroid/view/View$OnClickListener;

.field private cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field emptyViewData:Lcom/google/android/libraries/bind/data/Data;

.field errorViewData:Lcom/google/android/libraries/bind/data/Data;

.field private prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

.field rowDataList:Lcom/google/android/libraries/bind/data/DataList;

.field rowDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

.field private final updateHeaderCardsRunnable:Ljava/lang/Runnable;

.field private updateScrollPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_UPDATED:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR_MONTH_KEY:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->GROUPING_FILTER_EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;-><init>(Z)V

    const-string v1, "MyMagazinesFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->my_magazines_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 78
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->errorViewData:Lcom/google/android/libraries/bind/data/Data;

    .line 79
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->emptyViewData:Lcom/google/android/libraries/bind/data/Data;

    .line 81
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->archiveListener:Landroid/view/View$OnClickListener;

    .line 88
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateScrollPosition:I

    .line 109
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->toggleShowArchive()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateErrorViewData()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateCardBuilder()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateViewPosition()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateHeaderCards(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateMagazines()V

    return-void
.end method

.method private findToggleIcon()I
    .locals 5

    .prologue
    .line 240
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getFirstVisiblePosition()I

    move-result v1

    .line 241
    .local v1, "firstVisible":I
    move v2, v1

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 242
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 244
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 245
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget v4, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->LAYOUT:I

    if-ne v3, v4, :cond_0

    .line 249
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "i":I
    :goto_1
    return v2

    .line 241
    .restart local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 249
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private getFilteredMagazineGroupList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 5

    .prologue
    .line 215
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    move-result-object v2

    .line 216
    .local v2, "source":Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/SubscriptionsList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 219
    .local v1, "onDeviceList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 220
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;Landroid/content/res/Resources;)V

    .line 221
    .local v0, "groupingFilter":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->GROUPING_FILTER_EQUALITY_FIELDS:[I

    sget v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_GROUPING_ID:I

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    return-object v3
.end method

.method private getMonitoredDataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 2

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getFilteredMagazineGroupList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 270
    .local v0, "dataList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 271
    return-object v0
.end method

.method private getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 304
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$6;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    return-object v0
.end method

.method private hasErrorMessage()Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->errorViewData:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshIfNeeded()V
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 449
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 448
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 450
    return-void
.end method

.method private setShowArchive(Z)V
    .locals 7
    .param p1, "showArchive"    # Z

    .prologue
    const/4 v6, 0x1

    .line 253
    if-eqz p1, :cond_0

    .line 254
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->shiftShowArchiveOnUpdate()Z

    .line 256
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "setShowArchive called with %s"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;-><init>(Z)V

    .line 258
    .local v0, "newState":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    invoke-virtual {p0, v0, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 259
    return-void
.end method

.method private setupAdapter(Lcom/google/android/libraries/bind/card/CardListBuilder;)V
    .locals 4
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;

    .prologue
    .line 276
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 277
    .local v1, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 278
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    move-result-object v0

    .line 279
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 280
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 283
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setTag(Ljava/lang/Object;)V

    .line 284
    return-void
.end method

.method private shiftShowArchiveOnUpdate()Z
    .locals 2

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->findToggleIcon()I

    move-result v0

    .line 228
    .local v0, "toggleIconIndex":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 231
    add-int/lit8 v1, v0, 0x2

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateScrollPosition:I

    .line 232
    const/4 v1, 0x1

    .line 234
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private toggleShowArchive()V
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->isArchiveShowing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->setShowArchive(Z)V

    .line 445
    return-void

    .line 444
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCardBuilder()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 401
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->removeAll()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 403
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 404
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 405
    .local v1, "magazineGroup":Lcom/google/android/libraries/bind/data/Data;
    sget v6, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    .line 406
    invoke-virtual {v1, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/DataList;

    .line 409
    .local v2, "magazineIssues":Lcom/google/android/libraries/bind/data/DataList;
    if-nez v2, :cond_0

    .line 410
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->createHideShowIconData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    .line 411
    .local v5, "toggleButton":Lcom/google/android/libraries/bind/data/Data;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v7, "TOGGLE_ARCHIVE_CONTROL"

    invoke-virtual {v6, v7, v5}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 403
    .end local v5    # "toggleButton":Lcom/google/android/libraries/bind/data/Data;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-direct {v3, v2, v1, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;)V

    .line 415
    .local v3, "magazineRow":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;->getRowId()Ljava/lang/String;

    move-result-object v4

    .line 416
    .local v4, "rowId":Ljava/lang/String;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Row ID:  %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v7, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    goto :goto_1

    .line 420
    .end local v1    # "magazineGroup":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "magazineIssues":Lcom/google/android/libraries/bind/data/DataList;
    .end local v3    # "magazineRow":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGridGroup;
    .end local v4    # "rowId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateErrorMessageView()V

    .line 421
    invoke-direct {p0, v9}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateHeaderCards(Z)V

    .line 422
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 423
    return-void
.end method

.method private updateErrorMessageView()V
    .locals 3

    .prologue
    .line 297
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->hasErrorMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v1, "ERROR_MESSAGE"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->errorViewData:Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 299
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Appending error message"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    :cond_0
    return-void
.end method

.method private updateErrorViewData()V
    .locals 4

    .prologue
    .line 291
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 292
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/DataAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 291
    invoke-static {v1, v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeSpecificErrorCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->errorViewData:Lcom/google/android/libraries/bind/data/Data;

    .line 293
    return-void
.end method

.method private updateHeaderCards(Z)V
    .locals 8
    .param p1, "clearFirst"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 318
    const/4 v1, 0x1

    .line 320
    .local v1, "emptyRowAllowed":Z
    const/4 v3, 0x0

    .line 321
    .local v3, "onlineCardVisible":Z
    const/4 v2, 0x0

    .line 323
    .local v2, "offlineCardVisible":Z
    if-eqz p1, :cond_0

    .line 325
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v7, "OFFLINE_WARM_WELCOME"

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->remove(Ljava/lang/String;)Z

    .line 326
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v7, "ONLINE_WARM_WELCOME"

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->remove(Ljava/lang/String;)Z

    .line 327
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v7, "EMPTY_LIST_MESSAGE"

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->remove(Ljava/lang/String;)Z

    .line 330
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->hasErrorMessage()Z

    move-result v6

    if-nez v6, :cond_4

    move v0, v4

    .line 332
    .local v0, "emptyPage":Z
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isOnDeviceOnly()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v6

    if-nez v6, :cond_5

    .line 333
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v4

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v4, v6}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z

    move-result v2

    .line 341
    :goto_1
    if-eqz v3, :cond_7

    .line 342
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v6, "ONLINE_WARM_WELCOME"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getOnlineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->prepend(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 343
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Prepending online card"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    const/4 v0, 0x0

    .line 350
    :cond_2
    :goto_2
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Prepending library spacer"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v4, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 354
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 355
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Appending empty message."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isOnDeviceOnly()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getOfflineEmptyRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    :goto_3
    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->emptyViewData:Lcom/google/android/libraries/bind/data/Data;

    .line 358
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v5, "EMPTY_LIST_MESSAGE"

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->emptyViewData:Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 360
    :cond_3
    return-void

    .end local v0    # "emptyPage":Z
    :cond_4
    move v0, v5

    .line 330
    goto :goto_0

    .line 336
    .restart local v0    # "emptyPage":Z
    :cond_5
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_MAGAZINES_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z

    move-result v3

    .line 338
    if-nez v3, :cond_6

    move v1, v4

    :goto_4
    goto :goto_1

    :cond_6
    move v1, v5

    goto :goto_4

    .line 345
    :cond_7
    if-eqz v2, :cond_2

    .line 346
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    const-string v6, "OFFLINE_WARM_WELCOME"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getOfflineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->prepend(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 347
    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Prepending offline card"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    const/4 v0, 0x0

    goto :goto_2

    .line 357
    :cond_8
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getOnlineEmptyRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    goto :goto_3
.end method

.method private updateMagazines()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 427
    return-void
.end method

.method private updateViewPosition()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 390
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateScrollPosition:I

    if-eq v0, v2, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateScrollPosition:I

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->smoothScrollToPosition(I)V

    .line 392
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateScrollPosition:I

    .line 394
    :cond_0
    return-void
.end method


# virtual methods
.method public createHideShowIconData()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 199
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->LAYOUT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 200
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 201
    sget v2, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_SHOW_DEFAULT:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->isArchiveShowing()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 202
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ArchiveToggle;->DK_ON_CLICK_LISTENER:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->archiveListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 203
    return-object v0

    .line 201
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 485
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 501
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 502
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_my_magazines"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    return-object v0
.end method

.method protected getOfflineEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 367
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_offline:I

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getOfflineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleMyMagazinesOfflineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getOnlineEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeMyMagazinesOnlineEmptyRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getOnlineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleMyMagazinesOnlineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public isArchiveShowing()Z
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    .line 263
    .local v0, "currentState":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 480
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 166
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 167
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->my_magazines_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 168
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 467
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 468
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->dismiss()V

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 473
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 475
    :cond_1
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 476
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_manage_subscriptions:I

    if-ne v1, v2, :cond_0

    .line 173
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/ManageSubscriptionsIntentBuilder;->start()V

    .line 182
    :goto_0
    return v0

    .line 175
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_sync_on_device:I

    if-ne v1, v2, :cond_1

    .line 176
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->refreshMyMagazinesCollectionAndStartSync()V

    goto :goto_0

    .line 178
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_sort:I

    if-ne v1, v2, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->showSortByDialog()V

    goto :goto_0

    .line 182
    :cond_2
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 454
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 455
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 456
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->refreshIfNeeded()V

    .line 458
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateMagazines()V

    .line 459
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 7
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 127
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 128
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getMonitoredDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->rowDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 129
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 130
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->setupAdapter(Lcom/google/android/libraries/bind/card/CardListBuilder;)V

    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    .line 146
    .local v0, "connectivityRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->refreshIfNeeded()V

    .line 150
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "downloadedOnly"

    aput-object v5, v3, v4

    const-string v4, "myMagazinesSortField"

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 161
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->setHasOptionsMenu(Z)V

    .line 162
    return-void
.end method

.method refreshMyMagazinesCollectionAndStartSync()V
    .locals 5

    .prologue
    .line 430
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyMagazines(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 431
    .local v0, "collectionUri":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v3, v0, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 432
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 433
    .local v1, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 434
    .local v2, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$7;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 441
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 383
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->setUserVisibleHint(Z)V

    .line 384
    if-eqz p1, :cond_0

    .line 385
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->refreshIfNeeded()V

    .line 387
    :cond_0
    return-void
.end method

.method protected showSortByDialog()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->sortByDialog:Lcom/google/apps/dots/android/newsstand/home/library/magazines/SortByDialogFragment;

    const-string v2, "sortby"

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 192
    :cond_1
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 57
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    .prologue
    .line 490
    if-nez p1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Update views called"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 494
    if-eqz p2, :cond_2

    invoke-virtual {p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->updateMagazines()V

    goto :goto_0
.end method
