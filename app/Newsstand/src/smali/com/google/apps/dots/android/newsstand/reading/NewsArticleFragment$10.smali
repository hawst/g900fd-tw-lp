.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

.field final synthetic val$menu:Landroid/view/Menu;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;Landroid/view/Menu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->val$menu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 499
    if-eqz p1, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->translateMenuHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->val$menu:Landroid/view/Menu;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .line 501
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    .line 500
    invoke-virtual {v1, v2, p1, v0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 503
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 496
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$10;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
