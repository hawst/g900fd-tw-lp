.class Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->setUpAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getEmptyMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;->query:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->initialEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$500(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->noResultsEmptyMessageData:Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$600(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method
