.class abstract Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;
.super Ljava/lang/Object;
.source "BaseTraversal.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;


# instance fields
.field private requestedFinish:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final finish()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;->requestedFinish:Z

    .line 15
    return-void
.end method

.method final requestedFinish()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/BaseTraversal;->requestedFinish:Z

    return v0
.end method
