.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;
.super Ljava/lang/Object;
.source "EditionPagerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    .prologue
    .line 329
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;"
    const/4 v3, 0x0

    .line 335
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->currentPageIndex()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$400(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "INVALID_PAGE_INDEX returned by currentPageIndex(), resetting fragment state."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 344
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onPageListChanged()V

    .line 341
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updatePage()V

    .line 342
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateImageRotator()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    goto :goto_0
.end method
