.class public abstract Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "MoreReloadoIntentBuilder.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 38
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method


# virtual methods
.method public addSharedElement(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "transitionName"    # Ljava/lang/String;

    .prologue
    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 79
    invoke-virtual {p1, p2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 82
    :cond_0
    return-object p0
.end method

.method protected addSharedElements(Ljava/util/Collection;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "views":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 87
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    invoke-virtual {v0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    goto :goto_0

    .line 91
    .end local v0    # "view":Landroid/view/View;
    :cond_1
    return-object p0
.end method

.method public enableReloadoTransition(Landroid/view/ViewGroup;Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
    .locals 6
    .param p1, "shelfHeader"    # Landroid/view/ViewGroup;
    .param p2, "cardClass"    # Ljava/lang/Class;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 47
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 50
    const-class v4, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    .line 51
    invoke-static {p1, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 52
    .local v1, "listView":Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;

    invoke-direct {v4, p0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;-><init>(Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;Ljava/lang/Class;)V

    invoke-static {v1, p1, v4}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->viewsUnderShelfHeader(Landroid/widget/ListView;Landroid/view/ViewGroup;Lcom/google/common/base/Predicate;)Ljava/util/List;

    move-result-object v2

    .line 61
    .local v2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->shelf_header_button:I

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "button":Landroid/view/View;
    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->expando_splash:I

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 63
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->shelf_header_title:I

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 67
    .local v3, "title":Landroid/view/View;
    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 68
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->addSharedElements(Ljava/util/Collection;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->addSystemUiAsSharedElements()V

    .line 74
    .end local v0    # "button":Landroid/view/View;
    .end local v1    # "listView":Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .end local v2    # "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v3    # "title":Landroid/view/View;
    :cond_0
    return-object p0
.end method
