.class Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;
.super Ljava/lang/Object;
.source "NSActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->initTextToSpeech()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 565
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 570
    if-nez p1, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->textToSpeechInitialized:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->access$002(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)Z

    .line 572
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->access$100(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 573
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->access$100(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->speak(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity$4;->this$0:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->utteranceQueue:Lcom/google/android/libraries/bind/collections/RingBuffer;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->access$100(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->clear()V

    .line 578
    :cond_1
    return-void
.end method
