.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FlipperDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;


# instance fields
.field private bitField0_:I

.field private initialState_:I

.field private loopAround_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3308
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3309
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3314
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    .line 3333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    .line 3309
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    .locals 3

    .prologue
    .line 3363
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3367
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    return-object v0

    .line 3364
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    :catch_0
    move-exception v1

    .line 3365
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3305
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3372
    if-ne p1, p0, :cond_1

    .line 3375
    :cond_0
    :goto_0
    return v1

    .line 3373
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3374
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    .line 3375
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getInitialState()I
    .locals 1

    .prologue
    .line 3316
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    return v0
.end method

.method public getLoopAround()Z
    .locals 1

    .prologue
    .line 3335
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3399
    const/4 v0, 0x0

    .line 3400
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3401
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    .line 3402
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3404
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3405
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    .line 3406
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3408
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->cachedSize:I

    .line 3409
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 3380
    const/16 v0, 0x11

    .line 3381
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3382
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    add-int v0, v1, v2

    .line 3383
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 3384
    return v0

    .line 3383
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3417
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3418
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3422
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3423
    :sswitch_0
    return-object p0

    .line 3428
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    .line 3429
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    goto :goto_0

    .line 3433
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    .line 3434
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    goto :goto_0

    .line 3418
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3305
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3389
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3390
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->initialState_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3392
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3393
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$FlipperDetails;->loopAround_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3395
    :cond_1
    return-void
.end method
