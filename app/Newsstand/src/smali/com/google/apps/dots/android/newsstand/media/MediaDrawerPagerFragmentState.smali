.class public Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
.super Ljava/lang/Object;
.source "MediaDrawerPagerFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field public final expandedCaptionVisible:Z

.field public final mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

.field public final postId:Ljava/lang/String;

.field public final restrictToSingleField:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "mediaItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "restrictToSingleField"    # Z
    .param p4, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 24
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    .line 25
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    .line 26
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "restrictToSingleField"    # Z
    .param p4, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p5, "expandedCaptionVisible"    # Z

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 33
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    .line 34
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    .line 35
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 36
    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    .line 37
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 47
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    .line 49
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    .line 50
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 55
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    .line 61
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 60
    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 41
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{postId: %s, mediaItem: %s, restrictToSingleField: %s}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    .line 42
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 41
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->mediaItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 75
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->expandedCaptionVisible:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    return-void

    :cond_0
    move v0, v2

    .line 73
    goto :goto_0

    :cond_1
    move v1, v2

    .line 75
    goto :goto_1
.end method
