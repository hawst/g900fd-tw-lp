.class public Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
.super Ljava/lang/Object;
.source "StoreMutation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;
    }
.end annotation


# instance fields
.field public final action:Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

.field public final id:Ljava/lang/String;

.field public priority:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->BATCH:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 18
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->id:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->action:Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 20
    return-void
.end method


# virtual methods
.method public priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    .locals 0
    .param p1, "priority"    # Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 24
    return-object p0
.end method
