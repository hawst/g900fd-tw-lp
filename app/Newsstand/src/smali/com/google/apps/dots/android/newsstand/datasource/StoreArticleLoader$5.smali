.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->val$formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 210
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->val$postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 211
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->getUnchecked(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 209
    invoke-virtual {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getLegacyTemplate(Lcom/google/apps/dots/proto/client/DotsShared$Form;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;->apply(Ljava/lang/Object;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method
