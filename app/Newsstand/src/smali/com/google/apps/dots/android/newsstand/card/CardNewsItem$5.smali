.class final Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardNewsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$analyticsScreenName:Ljava/lang/String;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$analyticsScreenName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 322
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$analyticsScreenName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;->track(Z)V

    .line 323
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasAppName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->setTitleHint(Ljava/lang/String;)V

    .line 326
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 327
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    .line 329
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v1

    .line 328
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 331
    return-void
.end method
