.class public abstract Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;
.super Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;
.source "CollectionCardBuilderList.java"


# instance fields
.field protected final cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

.field private final selfObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    sget v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;-><init>(Landroid/content/Context;I)V

    .line 24
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->selfObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;)Lcom/google/android/libraries/bind/data/DataObserver;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->selfObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    return-object v0
.end method


# virtual methods
.method public cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    return-object v0
.end method

.method public rawCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public readingList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected refreshBuilder()V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->updateWithRefreshException(Lcom/google/android/libraries/bind/data/DataException;)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardBuilderList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->bulkReplace(Ljava/util/Collection;)V

    goto :goto_0
.end method
