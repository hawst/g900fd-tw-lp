.class Lcom/google/apps/dots/android/newsstand/http/NSClient$2;
.super Ljava/lang/Object;
.source "NSClient.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/NSClient;->getHttpClient(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$100(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 213
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSClient;->clientPool:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$100(Lcom/google/apps/dots/android/newsstand/http/NSClient;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->val$account:Landroid/accounts/Account;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    monitor-exit v1

    .line 215
    return-void

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;)V
    .locals 0
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    .prologue
    .line 208
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 206
    check-cast p1, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$2;->onSuccess(Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;)V

    return-void
.end method
