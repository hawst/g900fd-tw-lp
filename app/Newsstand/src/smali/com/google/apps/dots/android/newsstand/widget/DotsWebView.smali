.class public abstract Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
.super Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;
.source "DotsWebView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/EventSupport;


# static fields
.field private static final DOTS_BRIDGE:Ljava/lang/String; = "dots_bridge"

.field private static final JSON_DATA_ATTACHMENT_BASE_URL:Ljava/lang/String; = "attachmentBaseUrl"

.field private static final JSON_DATA_CLIENT_PLATFORM:Ljava/lang/String; = "clientPlatform"

.field private static final JSON_DATA_CLIENT_PLATFORM_VERSION:Ljava/lang/String; = "clientPlatformVersion"

.field private static final JSON_DATA_CLIENT_PRODUCT:Ljava/lang/String; = "clientProduct"

.field private static final JSON_DATA_CLIENT_VERSION:Ljava/lang/String; = "clientVersion"

.field private static final JSON_DATA_CONTENT:Ljava/lang/String; = "content"

.field private static final JSON_DATA_ENABLED_LAB_IDS:Ljava/lang/String; = "enabledLabIds"

.field private static final JSON_DATA_IS_PHONE:Ljava/lang/String; = "isPhone"

.field private static final JSON_DATA_LAYOUT_ENGINE_VERSION:Ljava/lang/String; = "layoutEngineVersion"

.field private static final JSON_DATA_SERVER_BASE_URL:Ljava/lang/String; = "serverBaseUrl"

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static didSetRenderPriority:Z


# instance fields
.field protected activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field protected asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field protected bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

.field private contentWidth:I

.field private destroyOnDetach:Z

.field private dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

.field private gestureDetector:Landroid/view/GestureDetector;

.field protected httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

.field private isAttached:Z

.field protected isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isDoubleTapDown:Z

.field protected isTouchDown:Z

.field private jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

.field protected lastScrollToX:I

.field protected lastScrollToY:I

.field private minZoomScale:F

.field protected final motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

.field protected final originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

.field protected pendingNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private scale:F

.field private scaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field protected scriptReady:Z

.field protected scroller:Landroid/widget/Scroller;

.field protected touchDownPage:I

.field private useLegacyLayout:Z

.field protected final useVerticalLayout:Z

.field protected final viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

.field protected zoomable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->didSetRenderPriority:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p3, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "useVerticalLayout"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 130
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->pendingNotifications:Ljava/util/List;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->destroyOnDetach:Z

    .line 99
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scriptReady:Z

    .line 104
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 113
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    .line 114
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->minZoomScale:F

    .line 115
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->contentWidth:I

    .line 119
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDoubleTapDown:Z

    .line 120
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 123
    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    .line 131
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScale()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    .line 132
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 133
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 134
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 135
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    .line 136
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    .line 137
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->init(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    .line 138
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDoubleTapDown:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->handleDoubleTap(II)Z

    move-result v0

    return v0
.end method

.method private static buildLoadContentJavaScript(IILorg/codehaus/jackson/node/ObjectNode;)Ljava/lang/String;
    .locals 5
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "jsonStore"    # Lorg/codehaus/jackson/node/ObjectNode;

    .prologue
    .line 76
    invoke-virtual {p2}, Lorg/codehaus/jackson/node/ObjectNode;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "serializedJsonStore":Ljava/lang/String;
    const-string v1, "dots.loadContent(%s, %s, %s);"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private calculateClosestPage()I
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 573
    .line 574
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 573
    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->calculateClosestPage(FF)I

    move-result v0

    return v0
.end method

.method private calculateClosestPage(FF)I
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 567
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    div-float v0, p2, v0

    .line 568
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContentHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getPageCountFromScroll()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 569
    :goto_0
    return v0

    .line 568
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    div-float v0, p1, v0

    .line 569
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContentWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getPageCountFromScroll()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method private ensureScriptReady()V
    .locals 2

    .prologue
    .line 789
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scriptReady:Z

    if-nez v0, :cond_0

    .line 790
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Method depends on inline script being fully loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 792
    :cond_0
    return-void
.end method

.method private getMinZoomScale()F
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->minZoomScale:F

    return v0
.end method

.method private handleDoubleTap(II)Z
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 639
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v17

    add-int v17, v17, p1

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v18

    add-int v18, v18, p2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->calculateClosestPage(FF)I

    move-result v6

    .line 640
    .local v6, "closestPage":I
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getMinZoomScale()F

    move-result v11

    .line 641
    .local v11, "minZoomScale":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v13, v17, v11

    .line 642
    .local v13, "pageWidthContent":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v12, v17, v11

    .line 643
    .local v12, "pageHeightContent":F
    int-to-float v0, v6

    move/from16 v17, v0

    mul-float v17, v17, v13

    move/from16 v0, v17

    float-to-int v7, v0

    .line 644
    .local v7, "closestPageLeft":I
    add-int/lit8 v17, v6, 0x1

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v13

    move/from16 v0, v17

    float-to-int v8, v0

    .line 646
    .local v8, "closestPageRight":I
    const/high16 v17, 0x3fc00000    # 1.5f

    mul-float v14, v11, v17

    .line 647
    .local v14, "zoomInOutThreshold":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    move/from16 v17, v0

    cmpl-float v17, v17, v14

    if-lez v17, :cond_0

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    move-object/from16 v17, v0

    new-instance v18, Landroid/graphics/Rect;

    const/16 v19, 0x0

    float-to-int v0, v12

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v7, v1, v8, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual/range {v17 .. v18}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->tryCenterFitRect(Landroid/graphics/Rect;)Z

    .line 669
    :goto_0
    const/16 v17, 0x1

    return v17

    .line 651
    :cond_0
    const v17, 0x3ff9999a    # 1.95f

    mul-float v16, v11, v17

    .line 653
    .local v16, "zoomScale":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v17

    add-int v17, v17, p1

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    move/from16 v18, v0

    div-float v4, v17, v18

    .line 654
    .local v4, "centerXAtZoom":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v17

    add-int v17, v17, p2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    move/from16 v18, v0

    div-float v5, v17, v18

    .line 655
    .local v5, "centerYAtZoom":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v17, v17, v16

    const/high16 v18, 0x3f000000    # 0.5f

    mul-float v10, v17, v18

    .line 656
    .local v10, "halfWidthAtZoom":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v17, v17, v16

    const/high16 v18, 0x3f000000    # 0.5f

    mul-float v9, v17, v18

    .line 657
    .local v9, "halfHeightAtZoom":F
    new-instance v15, Landroid/graphics/Rect;

    sub-float v17, v4, v10

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    sub-float v18, v5, v9

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    add-float v19, v4, v10

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    add-float v20, v5, v9

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v15, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 661
    .local v15, "zoomRect":Landroid/graphics/Rect;
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v7, :cond_1

    .line 662
    add-int/lit8 v17, v7, 0x1

    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 664
    :cond_1
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v0, v8, :cond_2

    .line 665
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    sub-int v17, v8, v17

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 667
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->tryCenterFitRect(Landroid/graphics/Rect;)Z

    goto/16 :goto_0
.end method

.method private ignoreEvent()Z
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    return v0
.end method

.method private init(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 6
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;-><init>(Landroid/webkit/WebView;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    .line 180
    new-instance v2, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    .line 183
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useHttpContentService()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->httpContentService()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;

    move-result-object v0

    .line 184
    .local v0, "httpContentService":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
    :goto_0
    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;->bind()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 188
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->startWebView()V

    .line 190
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 191
    .local v1, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 192
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 193
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 194
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 195
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 196
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 198
    sget-object v2, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 199
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 201
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 202
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 203
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 205
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v2, v3, :cond_1

    .line 206
    const-string v2, "use_minimal_memory"

    const-string v3, "false"

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setWebSettingsProperty(Landroid/webkit/WebSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_1
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;)V

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 218
    sget-boolean v2, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->didSetRenderPriority:Z

    if-nez v2, :cond_2

    .line 220
    sput-boolean v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->didSetRenderPriority:Z

    .line 221
    sget-object v2, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 225
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->resetJsonStore()V

    .line 227
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$2;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 235
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setVerticalScrollBarEnabled(Z)V

    .line 236
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 239
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->makeBridge(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .line 242
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    const-string v3, "dots_bridge"

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setFocusable(Z)V

    .line 247
    new-instance v2, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$3;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;)V

    invoke-direct {v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->gestureDetector:Landroid/view/GestureDetector;

    .line 260
    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$4;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;)V

    invoke-direct {v2, v3, v4}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 272
    return-void

    .line 183
    .end local v0    # "httpContentService":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
    .end local v1    # "settings":Landroid/webkit/WebSettings;
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private isAlmostZoomedOut()Z
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getMinZoomScale()F

    move-result v1

    div-float/2addr v0, v1

    const v1, 0x3f8ccccd    # 1.1f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private putIntoJsonStore(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 752
    .local p2, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 753
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v1

    .line 754
    .local v1, "node":Lorg/codehaus/jackson/node/ArrayNode;
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 755
    .local v0, "entry":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lorg/codehaus/jackson/node/ArrayNode;->add(Ljava/lang/String;)V

    goto :goto_0

    .line 757
    .end local v0    # "entry":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v2, p1, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 758
    return-void
.end method

.method private snapToClosestPage()V
    .locals 1

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->calculateClosestPage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->snapToPage(I)Z

    .line 579
    return-void
.end method


# virtual methods
.method public canZoomIn()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public canZoomOut()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public computeScroll()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 410
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->computeScroll()V

    .line 411
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useLegacyLayout()Z

    move-result v4

    if-nez v4, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isAlmostZoomedOut()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 419
    :cond_2
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v4, :cond_6

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->lastScrollToY:I

    .line 420
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v5

    if-eq v4, v5, :cond_5

    .line 421
    .local v0, "scrollChanged":Z
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isTouchDown:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    if-nez v3, :cond_0

    .line 422
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    .line 423
    .local v1, "x":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    .line 424
    .local v2, "y":I
    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scrollTo(II)V

    goto :goto_0

    .end local v0    # "scrollChanged":Z
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_5
    move v0, v3

    .line 420
    goto :goto_1

    :cond_6
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->lastScrollToX:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v5

    if-ne v4, v5, :cond_3

    move v0, v3

    goto :goto_1
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->unbind()V

    .line 349
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 353
    const-string v0, "dots_bridge"

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 355
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->clearReferences()V

    .line 356
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    .line 357
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setDotsWebViewClient(Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;)V

    .line 363
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->stopLoading()V

    .line 366
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onPause()V

    .line 370
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 371
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    .line 372
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    .line 373
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->gestureDetector:Landroid/view/GestureDetector;

    .line 374
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 375
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    .line 377
    :try_start_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public destroyOnDetach(Z)V
    .locals 1
    .param p1, "destroyOnDetach"    # Z

    .prologue
    .line 309
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->destroyOnDetach:Z

    .line 310
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isAttached:Z

    if-nez v0, :cond_0

    .line 311
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->postDestroy()V

    .line 313
    :cond_0
    return-void
.end method

.method public varargs executeStatements([Ljava/lang/String;)V
    .locals 6
    .param p1, "statements"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 683
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->ensureScriptReady()V

    .line 687
    array-length v2, p1

    if-eqz v2, :cond_0

    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 689
    .local v0, "sb":Ljava/lang/StringBuilder;
    array-length v4, p1

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v1, p1, v2

    .line 690
    .local v1, "statement":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 691
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Statement is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 693
    :cond_2
    const-string v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 694
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Missing semicolon: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 696
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 698
    .end local v1    # "statement":Ljava/lang/String;
    :cond_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v2, v4, :cond_6

    .line 701
    const-string v2, "javascript"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 704
    :cond_6
    const-string v2, "javascript:"

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getContentWidth()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->contentWidth:I

    return v0
.end method

.method protected getCurrentPage()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method protected getPageCount()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method public getPageCountFromScroll()I
    .locals 3

    .prologue
    .line 586
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v1, :cond_0

    .line 587
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->computeVerticalScrollExtent()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    mul-float/2addr v1, v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getMinZoomScale()F

    move-result v2

    div-float v0, v1, v2

    .line 588
    .local v0, "scrollExtent":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->computeVerticalScrollRange()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 591
    :goto_0
    return v1

    .line 590
    .end local v0    # "scrollExtent":F
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->computeHorizontalScrollExtent()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    mul-float/2addr v1, v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getMinZoomScale()F

    move-result v2

    div-float v0, v1, v2

    .line 591
    .restart local v0    # "scrollExtent":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->computeHorizontalScrollRange()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0
.end method

.method protected getScrollOffset(I)I
    .locals 3
    .param p1, "page"    # I

    .prologue
    .line 600
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v0

    .line 601
    .local v0, "dimension":I
    :goto_0
    int-to-float v1, v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getMinZoomScale()F

    move-result v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    mul-float/2addr v1, v2

    int-to-float v2, p1

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1

    .line 600
    .end local v0    # "dimension":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public loadContent()V
    .locals 4

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->buildLoadContentJavaScript(IILorg/codehaus/jackson/node/ObjectNode;)Ljava/lang/String;

    move-result-object v0

    .line 766
    .local v0, "loadClause":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->executeStatements([Ljava/lang/String;)V

    .line 770
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    .line 771
    return-void
.end method

.method protected abstract makeBridge(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;
.end method

.method public varargs notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "optionalArgs"    # [Ljava/lang/Object;

    .prologue
    .line 813
    const-string v0, "focus"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 814
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onFocus()V

    .line 818
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scriptReady:Z

    if-eqz v0, :cond_3

    .line 819
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    if-eqz v0, :cond_1

    .line 820
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->notify(Ljava/lang/String;)V

    .line 825
    :cond_1
    :goto_1
    return-void

    .line 815
    :cond_2
    const-string v0, "blur"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->onBlur()V

    goto :goto_0

    .line 823
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->pendingNotifications:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 317
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onAttachedToWindow()V

    .line 318
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->enterWebView()V

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isAttached:Z

    .line 320
    return-void
.end method

.method protected onBlur()V
    .locals 1

    .prologue
    .line 802
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setFocusable(Z)V

    .line 803
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 324
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onDetachedFromWindow()V

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isAttached:Z

    .line 326
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->destroyOnDetach:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->postDestroy()V

    .line 329
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewTracker()Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->exitWebView()V

    .line 330
    return-void
.end method

.method protected onFocus()V
    .locals 1

    .prologue
    .line 797
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->setFocusable(Z)V

    .line 798
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->requestFocus()Z

    .line 799
    return-void
.end method

.method protected onJsError(Landroid/webkit/ConsoleMessage;)V
    .locals 0
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 809
    return-void
.end method

.method public onLayoutChange(IZII)V
    .locals 2
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    .line 777
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->minZoomScale:F

    .line 778
    mul-int v0, p3, p1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->contentWidth:I

    .line 779
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->onLayoutChange(IZII)V

    .line 782
    :cond_0
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 2
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isTouchDown:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getOverScrollMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 484
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onOverScrolled(IIZZ)V

    .line 486
    :cond_1
    return-void
.end method

.method public onScriptLoad()V
    .locals 3

    .prologue
    .line 714
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scriptReady:Z

    .line 715
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    if-eqz v1, :cond_0

    .line 716
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->onReady()V

    .line 718
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    if-eqz v1, :cond_1

    .line 719
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->pendingNotifications:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 720
    .local v0, "eventName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;->notify(Ljava/lang/String;)V

    goto :goto_0

    .line 723
    .end local v0    # "eventName":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->pendingNotifications:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 724
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 7
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 445
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onScrollChanged(IIII)V

    .line 448
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v1, v5

    .line 449
    .local v1, "halfWidth":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v0, v5

    .line 450
    .local v0, "halfHeight":F
    int-to-float v5, p3

    add-float/2addr v5, v1

    int-to-float v6, p4

    add-float/2addr v6, v0

    invoke-direct {p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->calculateClosestPage(FF)I

    move-result v2

    .line 451
    .local v2, "lastPage":I
    int-to-float v5, p1

    add-float/2addr v5, v1

    int-to-float v6, p2

    add-float/2addr v6, v0

    invoke-direct {p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->calculateClosestPage(FF)I

    move-result v3

    .line 452
    .local v3, "page":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getPageCount()I

    move-result v4

    .line 453
    .local v4, "pageCount":I
    if-eq v3, v2, :cond_0

    if-ge v3, v4, :cond_0

    .line 454
    const/4 v5, 0x1

    invoke-virtual {p0, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->updateCurrentPage(IIZ)V

    .line 456
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    .line 490
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    .line 555
    :goto_0
    return v2

    .line 494
    :cond_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v5, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 495
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v5, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 496
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onStartTouchEvent(Landroid/view/MotionEvent;)V

    .line 497
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDoubleTapDown:Z

    .line 498
    .local v3, "wasDoubleTapDown":Z
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDoubleTapDown:Z

    if-eqz v5, :cond_1

    .line 500
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 502
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 540
    :cond_2
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->onEndTouchEvent(Landroid/view/MotionEvent;)V

    .line 545
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContentWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getPageCount()I

    move-result v7

    div-int/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->trySetZoomOverviewWidth(I)Z

    .line 546
    const/4 v2, 0x0

    .line 550
    .local v2, "superHandled":Z
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 554
    :goto_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->tryDismissZoomController()Z

    goto :goto_0

    .line 507
    .end local v2    # "superHandled":Z
    :pswitch_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isTouchDown:Z

    .line 508
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getCurrentPage()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->touchDownPage:I

    .line 509
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->isFinished()Z

    move-result v5

    if-nez v5, :cond_2

    .line 510
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    goto :goto_1

    .line 515
    :pswitch_2
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isTouchDown:Z

    .line 516
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDoubleTapDown:Z

    .line 517
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->isFinished()Z

    move-result v5

    if-nez v5, :cond_3

    .line 518
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    .line 520
    :cond_3
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    if-eqz v5, :cond_4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isAlmostZoomedOut()Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    if-nez v3, :cond_5

    .line 522
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getFlingDirection()Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    move-result-object v0

    .line 523
    .local v0, "direction":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v5, :cond_6

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->DOWN:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    :goto_3
    if-ne v5, v0, :cond_7

    .line 524
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->touchDownPage:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->snapToPage(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 525
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 535
    .end local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    :cond_5
    :goto_4
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getDidScrollX(Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->motionHelper:Lcom/google/apps/dots/android/newsstand/util/MotionHelper;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/util/MotionHelper;->getDidScrollY(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    .line 523
    .restart local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    :cond_6
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->RIGHT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    goto :goto_3

    .line 527
    :cond_7
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v5, :cond_8

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->UP:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    :goto_5
    if-ne v5, v0, :cond_9

    .line 528
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->touchDownPage:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->snapToPage(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 529
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_4

    .line 527
    :cond_8
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;->LEFT:Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;

    goto :goto_5

    .line 532
    :cond_9
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->snapToClosestPage()V

    goto :goto_4

    .line 551
    .end local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;
    .restart local v2    # "superHandled":Z
    :catch_0
    move-exception v1

    .line 552
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Suppressing exception from WebView onTouchEvent"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v5, v1, v6, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 502
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 560
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->ignoreEvent()Z

    move-result v0

    return v0
.end method

.method protected onZoomAttempt()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public postDestroy()V
    .locals 1

    .prologue
    .line 333
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$5;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->post(Ljava/lang/Runnable;)Z

    .line 339
    return-void
.end method

.method public putIntoJsonStore(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 732
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 733
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 734
    return-void
.end method

.method public putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 727
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    return-void
.end method

.method public putIntoJsonStore(Ljava/lang/String;Lorg/codehaus/jackson/node/ArrayNode;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lorg/codehaus/jackson/node/ArrayNode;

    .prologue
    .line 747
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 749
    return-void
.end method

.method public putIntoJsonStore(Ljava/lang/String;Lorg/codehaus/jackson/node/ObjectNode;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lorg/codehaus/jackson/node/ObjectNode;

    .prologue
    .line 742
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 743
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 744
    return-void
.end method

.method public putIntoJsonStore(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 737
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 738
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Z)V

    .line 739
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 464
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected resetJsonStore()V
    .locals 5

    .prologue
    .line 280
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    .line 281
    .local v1, "uris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    sget-object v2, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v2}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->jsonStore:Lorg/codehaus/jackson/node/ObjectNode;

    .line 282
    const-string v2, "serverBaseUrl"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 283
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getBaseUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 282
    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->httpHandle:Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;

    .line 286
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;->getExportedContentBaseUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "attachmentBaseUrl":Ljava/lang/String;
    :goto_0
    const-string v2, "attachmentBaseUrl"

    invoke-virtual {p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v3, "isPhone"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/shared/DeviceCategory;->isTablet()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Z)V

    .line 290
    const-string v2, "deviceCategory"

    .line 291
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/shared/DeviceCategory;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 290
    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v2, "clientPlatform"

    const-string v3, "android"

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v2, "clientPlatformVersion"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;I)V

    .line 295
    const-string v2, "clientProduct"

    const-string v3, "ns"

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string v2, "clientVersion"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v2, "layoutEngineVersion"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;I)V

    .line 299
    const-string v2, "content"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string v2, "useVerticalLayout"

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Z)V

    .line 301
    const-string v2, "enabledLabIds"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getEnabledLabIds()Ljava/util/Set;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/util/Collection;)V

    .line 302
    return-void

    .line 287
    .end local v0    # "attachmentBaseUrl":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$Attachments;->exportedContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 289
    .restart local v0    # "attachmentBaseUrl":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public scrollTo(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    .line 435
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v1, :cond_0

    move p1, v0

    .end local p1    # "x":I
    :cond_0
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->lastScrollToX:I

    .line 436
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v1, :cond_3

    move v1, p2

    :goto_0
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->lastScrollToY:I

    .line 437
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->lastScrollToX:I

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v2, :cond_2

    :cond_1
    move v0, p2

    :cond_2
    invoke-super {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->scrollTo(II)V

    .line 438
    return-void

    :cond_3
    move v1, v0

    .line 436
    goto :goto_0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 1
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 761
    const-string v0, "content"

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->putIntoJsonStore(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    return-void
.end method

.method public setDotsWebViewClient(Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;)V
    .locals 1
    .param p1, "dotsClient"    # Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    .prologue
    .line 386
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NoHorizontalScrollWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 387
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 388
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->dotsClient:Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    .line 389
    if-eqz p1, :cond_0

    .line 390
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->setWebviewUserAgent(Ljava/lang/String;)V

    .line 391
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scriptReady:Z

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->onReady()V

    .line 395
    :cond_0
    return-void
.end method

.method protected setUseLegacyLayout(Z)V
    .locals 0
    .param p1, "useLegacyLayout"    # Z

    .prologue
    .line 630
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useLegacyLayout:Z

    .line 631
    return-void
.end method

.method public setZoomable(Z)V
    .locals 2
    .param p1, "zoomable"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->zoomable:Z

    .line 143
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 144
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 145
    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setEnableSmoothTransition(Z)V

    .line 146
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 147
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->webViewMagic:Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewMagic;->setEnabled(Z)V

    .line 148
    return-void
.end method

.method public snapToPage(I)Z
    .locals 8
    .param p1, "page"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 609
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useLegacyLayout()Z

    move-result v6

    if-nez v6, :cond_1

    .line 622
    :cond_0
    :goto_0
    return v4

    .line 612
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getPageCount()I

    move-result v2

    .line 613
    .local v2, "pageCount":I
    if-ltz p1, :cond_0

    if-ge p1, v2, :cond_0

    .line 614
    invoke-virtual {p0, p1, v2, v5}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->updateCurrentPage(IIZ)V

    .line 615
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollOffset(I)I

    move-result v3

    .line 616
    .local v3, "target":I
    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v6, :cond_2

    move v0, v4

    .line 617
    .local v0, "dx":I
    :goto_1
    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useVerticalLayout:Z

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v4

    sub-int v1, v3, v4

    .line 618
    .local v1, "dy":I
    :goto_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollY()I

    move-result v7

    invoke-virtual {v4, v6, v7, v0, v1}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 619
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->postInvalidate()V

    move v4, v5

    .line 620
    goto :goto_0

    .line 616
    .end local v0    # "dx":I
    .end local v1    # "dy":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->getScrollX()I

    move-result v6

    sub-int v0, v3, v6

    goto :goto_1

    .restart local v0    # "dx":I
    :cond_3
    move v1, v4

    .line 617
    goto :goto_2
.end method

.method protected updateCurrentPage(IIZ)V
    .locals 0
    .param p1, "page"    # I
    .param p2, "pageCount"    # I
    .param p3, "userDriven"    # Z

    .prologue
    .line 93
    return-void
.end method

.method public updateScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 398
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->scale:F

    .line 399
    return-void
.end method

.method protected useHttpContentService()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method protected useLegacyLayout()Z
    .locals 1

    .prologue
    .line 626
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->useLegacyLayout:Z

    return v0
.end method
