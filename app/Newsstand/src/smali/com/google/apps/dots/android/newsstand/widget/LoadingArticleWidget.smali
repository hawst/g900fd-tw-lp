.class public Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
.super Landroid/widget/FrameLayout;
.source "LoadingArticleWidget.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;


# instance fields
.field private articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

.field private final articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

.field private final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private connectivityListener:Ljava/lang/Runnable;

.field private final downArrow:Landroid/widget/ImageView;

.field private final errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

.field private gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private interceptedTouchDownY:F

.field private isInterceptingScroll:Z

.field protected lastThrowable:Ljava/lang/Throwable;

.field private final loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

.field private final loadingView:Lcom/google/android/libraries/bind/widget/LoadingView;

.field private final meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field private final overlayView:Landroid/view/View;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final widget:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 54
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "linkWidget"    # Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v2, 0x0

    .line 68
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "normalArticleWidget"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v3, 0x0

    .line 59
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "normalArticleWidget"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;
    .param p3, "nativeBodyDotsWidget"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    .param p4, "linkWidget"    # Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;
    .param p5, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    .line 74
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->loading_article_widget:I

    invoke-static {p1, v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 76
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->loading:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/widget/LoadingView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadingView:Lcom/google/android/libraries/bind/widget/LoadingView;

    .line 77
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->load_error:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->meter_dialog:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 79
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->article_tail:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->overlay:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->overlayView:Landroid/view/View;

    .line 81
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->down_arrow:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->downArrow:Landroid/widget/ImageView;

    .line 82
    invoke-static {p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/util/ObjectUtil;->coalesce(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->widget:Landroid/view/View;

    .line 83
    if-eqz p2, :cond_1

    .line 84
    invoke-virtual {p2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 85
    invoke-virtual {p2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 86
    invoke-virtual {p0, p2, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->addView(Landroid/view/View;I)V

    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->updateErrorView()V

    .line 97
    return-void

    .line 87
    :cond_1
    if-eqz p3, :cond_2

    .line 88
    invoke-virtual {p3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 89
    invoke-virtual {p3, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 90
    invoke-virtual {p0, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 91
    :cond_2
    if-eqz p4, :cond_0

    .line 92
    invoke-virtual {p4, p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 93
    invoke-virtual {p4, p0}, Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 94
    invoke-virtual {p0, p4, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nativeBodyDotsWidget"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v2, 0x0

    .line 64
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/widget/LinkWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)Lcom/google/apps/dots/android/newsstand/card/ActionMessage;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)Lcom/google/android/libraries/bind/widget/LoadingView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadingView:Lcom/google/android/libraries/bind/widget/LoadingView;

    return-object v0
.end method


# virtual methods
.method public canScrollHorizontally(I)Z
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v0

    return v0
.end method

.method public getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    return-object v0
.end method

.method protected getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)V

    return-object v0
.end method

.method public getWidget()Landroid/view/View;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->widget:Landroid/view/View;

    return-object v0
.end method

.method public loadDelayedContents(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "optLoadedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v0

    .line 141
    .local v0, "loadState":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$5;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$DelayedContentWidget$LoadState:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 152
    :goto_0
    return-void

    .line 144
    :pswitch_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadedRunnable(Ljava/lang/Runnable;)V

    .line 145
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->loadDelayedContents(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onArticleOverscrolled(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "top"    # Z

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleOverscrolled(Landroid/view/View;Z)V

    .line 269
    :cond_0
    return-void
.end method

.method public onArticlePageChanged(Landroid/view/View;IIZ)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "page"    # I
    .param p3, "pageCount"    # I
    .param p4, "userDriven"    # Z

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticlePageChanged(Landroid/view/View;IIZ)V

    .line 276
    :cond_0
    return-void
.end method

.method public onArticleScrolled(Landroid/view/View;IIIZ)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scrollOffset"    # I
    .param p3, "scrollRange"    # I
    .param p4, "scrollDelta"    # I
    .param p5, "userDriven"    # Z

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleScrolled(Landroid/view/View;IIIZ)V

    .line 262
    :cond_0
    return-void
.end method

.method public onArticleUnhandledClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticleUnhandledClick(Landroid/view/View;)V

    .line 283
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 292
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 293
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 294
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->connectivityListener:Ljava/lang/Runnable;

    .line 300
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 305
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 306
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->connectivityListener:Ljava/lang/Runnable;

    .line 309
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 310
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 174
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    if-eqz v5, :cond_0

    .line 175
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 176
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v5, v0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getHitRect(Landroid/graphics/Rect;)V

    .line 177
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 205
    .end local v0    # "rect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return v3

    .line 180
    .restart local v0    # "rect":Landroid/graphics/Rect;
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->interceptedTouchDownY:F

    .line 181
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v4, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 185
    :pswitch_1
    iput v7, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->interceptedTouchDownY:F

    .line 186
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->isInterceptingScroll:Z

    .line 187
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v4, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 192
    :pswitch_2
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->isInterceptingScroll:Z

    if-eqz v5, :cond_1

    move v3, v4

    .line 193
    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->interceptedTouchDownY:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-int v1, v5

    .line 197
    .local v1, "scrollDist":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 198
    .local v2, "touchSlop":I
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->interceptedTouchDownY:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_0

    if-le v1, v2, :cond_0

    .line 199
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->isInterceptingScroll:Z

    move v3, v4

    .line 200
    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onLoadStateChanged(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;)V

    .line 231
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$5;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$DelayedContentWidget$LoadState:[I

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 253
    :goto_0
    :pswitch_0
    return-void

    .line 233
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadingView:Lcom/google/android/libraries/bind/widget/LoadingView;

    const/16 v1, 0xc8

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)V

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeOut(Landroid/view/View;ILjava/lang/Runnable;)V

    goto :goto_0

    .line 243
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->lastThrowable:Ljava/lang/Throwable;

    if-eq v0, p3, :cond_0

    .line 244
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->lastThrowable:Ljava/lang/Throwable;

    .line 245
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->updateErrorView()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->setVisibility(I)V

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x1

    .line 220
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V
    .locals 0
    .param p1, "articleEventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    .line 125
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 136
    :cond_0
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/FilteredDataRow;)V
    .locals 2
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/FilteredDataRow;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->widget:Landroid/view/View;

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->widget:Landroid/view/View;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->downArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->setupDownArrow(Landroid/widget/ImageView;)V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->widget:Landroid/view/View;

    check-cast v0, Lcom/google/android/libraries/bind/data/DataView;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/bind/data/DataView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 164
    return-void
.end method

.method public setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V
    .locals 1
    .param p1, "eventHandler"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadStateEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 226
    return-void
.end method

.method protected updateErrorView()V
    .locals 5

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->lastThrowable:Ljava/lang/Throwable;

    .line 101
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v4

    .line 100
    invoke-static {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 104
    .local v0, "errorViewData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 110
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->errorView:Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->configure(Lcom/google/android/libraries/bind/data/Data;)V

    .line 111
    return-void
.end method
