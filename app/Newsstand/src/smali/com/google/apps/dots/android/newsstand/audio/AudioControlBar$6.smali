.class Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioControlBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updateInfo(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 3
    .param p1, "result"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 214
    if-nez p1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->titleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 220
    .local v0, "summary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->titleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 211
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
