.class final Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState$1;
.super Ljava/lang/Object;
.source "PlainEditionFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    const-class v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 66
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    move-result-object v1

    .line 67
    .local v1, "headerType":Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    invoke-direct {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 72
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    move-result-object v0

    return-object v0
.end method
