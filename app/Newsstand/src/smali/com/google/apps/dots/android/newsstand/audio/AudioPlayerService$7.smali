.class Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->val$index:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 5
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 534
    if-nez p1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 545
    :goto_0
    return-void

    .line 538
    :cond_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->getAudioItemsList(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;

    move-result-object v0

    .line 539
    .local v0, "audioItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/media/AudioItem;>;"
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->val$index:I

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->val$index:I

    if-ltz v1, :cond_1

    .line 540
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->val$index:I

    .line 541
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1400(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v4

    .line 540
    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    invoke-static {v2, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 543
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 531
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
