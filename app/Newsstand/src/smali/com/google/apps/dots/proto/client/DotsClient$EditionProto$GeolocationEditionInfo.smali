.class public final Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GeolocationEditionInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;


# instance fields
.field private bitField0_:I

.field private location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

.field private zoom_:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2554
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2555
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2579
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    .line 2555
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    .locals 3

    .prologue
    .line 2609
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2613
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v2, :cond_0

    .line 2614
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 2616
    :cond_0
    return-object v0

    .line 2610
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    :catch_0
    move-exception v1

    .line 2611
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2551
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2621
    if-ne p1, p0, :cond_1

    .line 2624
    :cond_0
    :goto_0
    return v1

    .line 2622
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2623
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    .line 2624
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2648
    const/4 v0, 0x0

    .line 2649
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v1, :cond_0

    .line 2650
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 2651
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2653
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2654
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    .line 2655
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 2657
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->cachedSize:I

    .line 2658
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2629
    const/16 v0, 0x11

    .line 2630
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2631
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 2632
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 2633
    return v0

    .line 2631
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2666
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2667
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2671
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2672
    :sswitch_0
    return-object p0

    .line 2677
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v1, :cond_1

    .line 2678
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 2680
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2684
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    .line 2685
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->bitField0_:I

    goto :goto_0

    .line 2667
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2551
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2638
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v0, :cond_0

    .line 2639
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2641
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2642
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$GeolocationEditionInfo;->zoom_:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 2644
    :cond_1
    return-void
.end method
