.class Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;
.super Ljava/lang/Object;
.source "Upgrade.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

.field final synthetic val$currentStoreRootName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;->this$0:Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;->val$currentStoreRootName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 115
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "store"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;->val$currentStoreRootName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    # getter for: Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Deleting old DiskCache dir: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    .line 121
    :cond_0
    return v3
.end method
