.class public Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "NewsWidgetProvider.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static isConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 30
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->isConnected:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static configureNavigationButtons(Landroid/content/Context;ILandroid/widget/RemoteViews;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p3, "showNavigationIcons"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v3, 0x8

    .line 257
    const/16 v0, 0x8

    .line 258
    .local v0, "navigationIconVisibilityState":I
    if-eqz p3, :cond_0

    .line 259
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->next:I

    const-string v2, "NewsWidgetService_nextAction"

    .line 260
    invoke-static {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdatePendingIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 259
    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 261
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->previous:I

    const-string v2, "NewsWidgetService_previousAction"

    .line 262
    invoke-static {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdatePendingIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 261
    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 264
    const/4 v0, 0x0

    .line 266
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->next_icon:I

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->previous_icon:I

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 269
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->next_progress:I

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 270
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->previous_progress:I

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 271
    return-void
.end method

.method private static convertCellsToDp(I)I
    .locals 1
    .param p0, "cells"    # I

    .prologue
    .line 351
    mul-int/lit8 v0, p0, 0x46

    add-int/lit8 v0, v0, -0x1e

    return v0
.end method

.method private static getUpdateIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 320
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 322
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 328
    return-object v0
.end method

.method private static getUpdatePendingIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 333
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdateIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 334
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    .line 335
    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 336
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    return-object v1
.end method

.method private handleSamsungResize(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 85
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    const-string v4, "widgetId"

    .line 89
    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 90
    .local v0, "appWidgetId":I
    const-string v4, "widgetspanx"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 91
    .local v2, "widgetSpanX":I
    const-string v4, "widgetspany"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 93
    .local v3, "widgetSpanY":I
    if-eqz v0, :cond_0

    if-lez v2, :cond_0

    if-lez v3, :cond_0

    .line 95
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 96
    .local v1, "newOptions":Landroid/os/Bundle;
    const-string v4, "appWidgetMinHeight"

    .line 97
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->convertCellsToDp(I)I

    move-result v5

    .line 96
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v4, "appWidgetMinWidth"

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->convertCellsToDp(I)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidgetOptions(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static isWidgetInstalled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 362
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 363
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 364
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 365
    .local v0, "appWidgetIds":[I
    array-length v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static refreshAllWidgets(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 305
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 308
    .local v0, "appWidgetIds":[I
    array-length v2, v0

    if-lez v2, :cond_0

    .line 309
    const/4 v2, 0x0

    aget v2, v0, v2

    const-string v3, "NewsWidgetService_refreshAction"

    const/4 v4, 0x1

    invoke-static {p0, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 311
    :cond_0
    return-void
.end method

.method public static restartAllWidgets(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 314
    const/4 v1, 0x0

    const-string v2, "NewsWidgetService_cancelAction"

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdateIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 315
    .local v0, "cancelIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 316
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateAllWidgets(Landroid/content/Context;Z)V

    .line 317
    return-void
.end method

.method private static showEmptyMessage(Landroid/content/Context;ILjava/lang/String;Landroid/app/PendingIntent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v3, 0x0

    .line 169
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 170
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->useCompactLayout(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget_compact:I

    :goto_0
    invoke-direct {v0, v2, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 173
    .local v0, "rv":Landroid/widget/RemoteViews;
    invoke-static {p0, p1, v0, v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->configureNavigationButtons(Landroid/content/Context;ILandroid/widget/RemoteViews;Z)V

    .line 175
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->widget_article_layout:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 176
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->widget_empty_layout:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 177
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->empty_view_text:I

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 179
    if-eqz p3, :cond_0

    .line 180
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->widget_empty_layout:I

    invoke-virtual {v0, v1, p3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 183
    :cond_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 184
    return-void

    .line 170
    .end local v0    # "rv":Landroid/widget/RemoteViews;
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget:I

    goto :goto_0
.end method

.method public static showLoadingView(Landroid/content/Context;ILandroid/accounts/Account;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 128
    if-eqz p2, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showOfflineMessage(Landroid/content/Context;I)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->loading:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showEmptyMessage(Landroid/content/Context;ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static showNavigationProgressIndicator(Landroid/content/Context;IZ)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "isNavigationToNext"    # Z

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 199
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 200
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->useCompactLayout(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget_compact:I

    :goto_0
    invoke-direct {v1, v3, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 203
    .local v1, "rv":Landroid/widget/RemoteViews;
    if-eqz p2, :cond_1

    .line 204
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->next_progress:I

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 205
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->next_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 206
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->previous_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    :goto_1
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 214
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, p1, v1}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 215
    return-void

    .line 200
    .end local v0    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v1    # "rv":Landroid/widget/RemoteViews;
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget:I

    goto :goto_0

    .line 208
    .restart local v1    # "rv":Landroid/widget/RemoteViews;
    :cond_1
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->previous_progress:I

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 209
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->previous_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 210
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->next_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method public static showNoPostsFound(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 139
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->widget_no_posts:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewsWidgetService_refreshAction"

    .line 140
    invoke-static {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdatePendingIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 139
    invoke-static {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showEmptyMessage(Landroid/content/Context;ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 141
    return-void
.end method

.method public static showOfflineMessage(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 147
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 148
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 149
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showTapToSignIn(Landroid/content/Context;I)V

    .line 157
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->content_error_domode_downloaded_not_synced:I

    .line 155
    .local v1, "messageResId":I
    :goto_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "NewsWidgetService_refreshAction"

    .line 156
    invoke-static {p0, p1, v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdatePendingIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 155
    invoke-static {p0, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showEmptyMessage(Landroid/content/Context;ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 152
    .end local v1    # "messageResId":I
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->widget_offline_not_downloaded:I

    goto :goto_1
.end method

.method public static showRefreshProgressIndicator(Landroid/content/Context;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 227
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 228
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->useCompactLayout(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget_compact:I

    :goto_0
    invoke-direct {v1, v3, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 231
    .local v1, "rv":Landroid/widget/RemoteViews;
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->empty_view_text:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->loading:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 233
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->next_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 234
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->previous_icon:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 237
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->next_progress:I

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 238
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->previous_progress:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 239
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->widget_empty_layout:I

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->widget_article_layout:I

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 242
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 243
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, p1, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 244
    return-void

    .line 228
    .end local v0    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v1    # "rv":Landroid/widget/RemoteViews;
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->news_widget:I

    goto :goto_0
.end method

.method public static showTapToSignIn(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 116
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 117
    .local v1, "startIntent":Landroid/content/Intent;
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    .line 118
    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 119
    .local v0, "startClickIntent":Landroid/app/PendingIntent;
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->widget_tap_to_sign_in:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v2, v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showEmptyMessage(Landroid/content/Context;ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 121
    return-void
.end method

.method public static updateAllWidgets(Landroid/content/Context;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resetPosition"    # Z

    .prologue
    .line 292
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 293
    .local v2, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 294
    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 295
    .local v1, "appWidgetIds":[I
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget v0, v1, v3

    .line 296
    .local v0, "appWidgetId":I
    const-string v5, "NewsWidgetService_updateAction"

    invoke-static {p0, v0, v5, p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 295
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "appWidgetId":I
    :cond_0
    return-void
.end method

.method public static updateWidget(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "resetPosition"    # Z

    .prologue
    .line 281
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->getUpdateIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 282
    .local v0, "serviceIntent":Landroid/content/Intent;
    if-eqz p3, :cond_0

    .line 283
    const-string v1, "NewsWidgetService_articlePosition"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 286
    return-void
.end method

.method public static useCompactLayout(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 341
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 342
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v0

    .line 343
    .local v0, "options":Landroid/os/Bundle;
    const-string v2, "appWidgetMinHeight"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->convertCellsToDp(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    const/4 v1, 0x1

    .line 345
    .end local v0    # "options":Landroid/os/Bundle;
    :cond_0
    return v1
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 108
    const-string v0, "NewsWidgetService_updateAction"

    const/4 v1, 0x0

    invoke-static {p1, p3, v0, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 109
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51
    sget-object v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Widget got intent: %s"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    sget-boolean v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->isConnected:Z

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v3

    if-eq v2, v3, :cond_0

    .line 56
    sget-boolean v2, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->isConnected:Z

    if-nez v2, :cond_1

    :goto_0
    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->isConnected:Z

    .line 57
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateAllWidgets(Landroid/content/Context;Z)V

    .line 68
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 69
    return-void

    :cond_1
    move v0, v1

    .line 56
    goto :goto_0

    .line 59
    :cond_2
    const-string v0, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->handleSamsungResize(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1

    .line 63
    :cond_3
    const-string v0, "com.google.apps.dots.android.newsstand.appwidget.REFRESH"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 64
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->refreshAllWidgets(Landroid/content/Context;)V

    goto :goto_1

    .line 65
    :cond_4
    const-string v0, "com.google.apps.dots.android.newsstand.appwidget.POKE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateAllWidgets(Landroid/content/Context;Z)V

    goto :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    .line 74
    .local v1, "noAccount":Z
    :goto_0
    array-length v4, p3

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    aget v0, p3, v3

    .line 75
    .local v0, "appWidgetId":I
    if-eqz v1, :cond_1

    .line 76
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->showTapToSignIn(Landroid/content/Context;I)V

    .line 74
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "appWidgetId":I
    .end local v1    # "noAccount":Z
    :cond_0
    move v1, v2

    .line 73
    goto :goto_0

    .line 78
    .restart local v0    # "appWidgetId":I
    .restart local v1    # "noAccount":Z
    :cond_1
    const-string v5, "NewsWidgetService_updateAction"

    invoke-static {p1, v0, v5, v2}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;Z)V

    goto :goto_2

    .line 81
    .end local v0    # "appWidgetId":I
    :cond_2
    return-void
.end method
