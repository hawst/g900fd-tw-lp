.class final Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState$1;
.super Ljava/lang/Object;
.source "MagazineEditionFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 79
    const-class v5, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 80
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x1

    .line 81
    .local v1, "inLiteMode":Z
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 82
    .local v4, "initialPostId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 83
    .local v3, "initialPageNumberInt":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_1

    const/4 v2, 0x0

    .line 85
    .local v2, "initialPageNumber":Ljava/lang/Integer;
    :goto_1
    new-instance v5, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    invoke-direct {v5, v0, v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V

    return-object v5

    .line 80
    .end local v1    # "inLiteMode":Z
    .end local v2    # "initialPageNumber":Ljava/lang/Integer;
    .end local v3    # "initialPageNumberInt":I
    .end local v4    # "initialPostId":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 84
    .restart local v1    # "inLiteMode":Z
    .restart local v3    # "initialPageNumberInt":I
    .restart local v4    # "initialPostId":Ljava/lang/String;
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 91
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragmentState;

    move-result-object v0

    return-object v0
.end method
