.class public Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat;
.super Ljava/lang/Object;
.source "StrictModeCompat.java"


# direct methods
.method public static setThreadPolicyDelayed(Landroid/os/StrictMode$ThreadPolicy;)V
    .locals 4
    .param p0, "policy"    # Landroid/os/StrictMode$ThreadPolicy;

    .prologue
    .line 21
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat$1;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 27
    return-void
.end method
