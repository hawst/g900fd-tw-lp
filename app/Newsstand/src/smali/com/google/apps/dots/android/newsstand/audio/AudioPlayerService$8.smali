.class Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;
.super Ljava/lang/Object;
.source "AudioPlayerService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotification()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 631
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdate()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1900(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 645
    return-void
.end method

.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 3
    .param p1, "result"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 634
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 635
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 636
    .local v0, "summary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1602(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Ljava/lang/String;)Ljava/lang/String;

    .line 637
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updatePublisherInfo()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1700(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 638
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->postUpdateNotification()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1800(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 640
    .end local v0    # "summary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 631
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
