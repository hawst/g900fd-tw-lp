.class public final Lcom/google/apps/dots/android/newsstand/model/nodes/Nodes;
.super Ljava/lang/Object;
.source "Nodes.java"


# direct methods
.method public static getSummary(Ljava/lang/Class;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasCollectionSummary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 20
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    goto :goto_0

    .line 24
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasSectionSummary()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasPostSummary()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 33
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 34
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasWebPageSummary()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 37
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getWebPageSummary()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 38
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getWebPageSummary()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_5
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasClusterSummary()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 41
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getClusterSummary()Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 42
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getClusterSummary()Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_6
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasExploreGroupSummary()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 45
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 46
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v0

    goto/16 :goto_0

    .line 48
    :cond_7
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasMerchandisingShelfSummary()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 49
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getMerchandisingShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 50
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getMerchandisingShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    goto/16 :goto_0

    .line 52
    :cond_8
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasOfferSummary()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 53
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 54
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    goto/16 :goto_0

    .line 56
    :cond_9
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasGeoLocationSummary()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 57
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getGeoLocationSummary()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 58
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getGeoLocationSummary()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v0

    goto/16 :goto_0

    .line 60
    :cond_a
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasReadNowEditionShelfSummary()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 61
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getReadNowEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 62
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getReadNowEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v0

    goto/16 :goto_0

    .line 65
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
