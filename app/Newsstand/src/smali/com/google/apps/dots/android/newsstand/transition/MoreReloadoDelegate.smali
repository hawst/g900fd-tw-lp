.class public abstract Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "MoreReloadoDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
        ">",
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private moreButtonDummy:Landroid/widget/ImageView;

.field private final titleLocInWindow:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    return-void
.end method

.method private findSnapshot(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p3, "lookupTransitionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "snapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v2, 0x0

    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 253
    .local v1, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 254
    invoke-interface {v1, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 255
    .local v0, "index":I
    if-ltz v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    :cond_0
    return-object v2
.end method


# virtual methods
.method protected abstract cardClass()Ljava/lang/Class;
.end method

.method protected createEnterTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->windowTransition(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->createEnterTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->sharedElementTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Z)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->sharedElementTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Z)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 196
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    const/4 v0, 0x2

    return v0
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 191
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    const-wide/16 v0, 0x1c2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getFallbackReturnTransition()Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 201
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    new-instance v0, Landroid/transition/Explode;

    invoke-direct {v0}, Landroid/transition/Explode;-><init>()V

    return-object v0
.end method

.method protected handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;

    move-result-object v0

    .line 135
    .local v0, "splashView":Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisibleOnScreen(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const/4 v1, 0x0

    invoke-interface {p3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 141
    invoke-interface {p3}, Ljava/util/Map;->clear()V

    .line 142
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->getFallbackReturnTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->setReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method protected handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 173
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 174
    .local v1, "titleSnapshotIndex":I
    if-ltz v1, :cond_0

    .line 175
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 176
    .local v0, "title":Landroid/view/View;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 182
    .end local v0    # "title":Landroid/view/View;
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    aget v3, v3, v5

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setRight(I)V

    .line 183
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    aget v3, v3, v6

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBottom(I)V

    .line 184
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLeft(I)V

    .line 185
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->titleLocInWindow:[I

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTop(I)V

    .line 186
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 187
    return-void

    .line 178
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unable to find title in snapshots"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterSetSharedElementEnd(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementEnd(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    .line 152
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-direct {p0, p2, p4, v1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->findSnapshot(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 153
    .local v0, "moreButtonSnapshot":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;

    move-result-object v1

    .line 159
    invoke-static {v0}, Lcom/google/android/play/transition/PlayTransitionUtil;->viewBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 157
    invoke-static {v1, v2}, Lcom/google/android/play/transition/PlayTransitionUtil;->centerViewOnRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 166
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 163
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 164
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->getFallbackReturnTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->setReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected abstract headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/android/play/headerlist/PlayHeaderListLayout;"
        }
    .end annotation
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    const/4 v3, -0x2

    .line 58
    invoke-super {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->prepareViewsForTransition(Ljava/lang/Object;)V

    .line 60
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->expando_splash:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTransitionName(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->addView(Landroid/view/View;)V

    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 68
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->reloado_actionbar:I

    .line 69
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 72
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    return-void
.end method

.method protected sharedElementTransition(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;Z)Landroid/transition/TransitionSet;
    .locals 13
    .param p2, "isEntering"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)",
            "Landroid/transition/TransitionSet;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    const/4 v8, 0x2

    const/4 v12, 0x0

    const/4 v7, 0x1

    .line 100
    new-instance v6, Landroid/transition/Fade;

    invoke-direct {v6}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 102
    .local v0, "actionBarFade":Landroid/transition/Transition;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;

    invoke-direct {v6}, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;-><init>()V

    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    .line 103
    invoke-virtual {p1, v9}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/apps/dots/android/newsstand/transition/ChangeTextView;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    move-result-object v2

    .line 105
    .local v2, "changeTitle":Landroid/transition/Transition;
    new-instance v9, Lcom/google/android/play/transition/CircularReveal;

    if-eqz p2, :cond_0

    move v6, v7

    :goto_0
    invoke-direct {v9, v6}, Lcom/google/android/play/transition/CircularReveal;-><init>(I)V

    .line 107
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v9, v6}, Lcom/google/android/play/transition/CircularReveal;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v5

    .line 110
    .local v5, "revealSplash":Landroid/transition/Transition;
    invoke-static {v12, p2}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;->fadeAlpha(ZZ)Landroid/transition/Transition;

    move-result-object v6

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    .line 111
    invoke-virtual {v6, v9}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v6

    const-wide/16 v10, 0xe1

    .line 112
    invoke-virtual {v6, v10, v11}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    move-result-object v3

    .line 114
    .local v3, "fadeButton":Landroid/transition/Transition;
    new-instance v6, Landroid/transition/ChangeBounds;

    invoke-direct {v6}, Landroid/transition/ChangeBounds;-><init>()V

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    .line 115
    invoke-virtual {v6, v9}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v6

    .line 116
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v4

    .line 119
    .local v4, "moveButton":Landroid/transition/Transition;
    new-instance v6, Landroid/transition/ChangeBounds;

    invoke-direct {v6}, Landroid/transition/ChangeBounds;-><init>()V

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->moreButtonDummy:Landroid/widget/ImageView;

    .line 120
    invoke-virtual {v6, v9, v7}, Landroid/transition/ChangeBounds;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    move-result-object v6

    .line 121
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v9

    invoke-virtual {v6, v9, v7}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    move-result-object v6

    .line 122
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v6, v9, v7}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    move-result-object v1

    .line 123
    .local v1, "changeBounds":Landroid/transition/Transition;
    new-instance v6, Landroid/transition/SidePropagation;

    invoke-direct {v6}, Landroid/transition/SidePropagation;-><init>()V

    invoke-virtual {v1, v6}, Landroid/transition/Transition;->setPropagation(Landroid/transition/TransitionPropagation;)V

    .line 126
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v6

    const-wide/16 v10, 0x1c2

    const/4 v9, 0x6

    new-array v9, v9, [Landroid/transition/Transition;

    aput-object v0, v9, v12

    aput-object v1, v9, v7

    aput-object v5, v9, v8

    const/4 v7, 0x3

    aput-object v2, v9, v7

    const/4 v7, 0x4

    aput-object v3, v9, v7

    const/4 v7, 0x5

    aput-object v4, v9, v7

    invoke-static {v6, v10, v11, v9}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->aggregate(Landroid/view/animation/Interpolator;J[Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v6

    return-object v6

    .end local v1    # "changeBounds":Landroid/transition/Transition;
    .end local v3    # "fadeButton":Landroid/transition/Transition;
    .end local v4    # "moveButton":Landroid/transition/Transition;
    .end local v5    # "revealSplash":Landroid/transition/Transition;
    :cond_0
    move v6, v8

    .line 105
    goto :goto_0
.end method

.method protected splashView(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 224
    .line 225
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->background_container:I

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 226
    .local v0, "background":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected final titleInSharedElements(Ljava/util/List;Ljava/util/List;)Landroid/widget/TextView;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/widget/TextView;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getStringResource(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "transitionName":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 212
    .local v0, "index":I
    if-ltz v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 213
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public windowTransition(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;
    .locals 4
    .param p1, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 81
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;, "Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate<TT;>;"
    new-instance v1, Landroid/transition/Slide;

    invoke-direct {v1}, Landroid/transition/Slide;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/transition/MoreReloadoDelegate;->cardClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/Slide;->addTarget(Ljava/lang/Class;)Landroid/transition/Transition;

    move-result-object v0

    .line 83
    .local v0, "slide":Landroid/transition/Transition;
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/transition/Transition;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v1

    .line 84
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v1

    const-wide/16 v2, 0x1c2

    .line 85
    invoke-virtual {v1, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v1

    return-object v1
.end method
