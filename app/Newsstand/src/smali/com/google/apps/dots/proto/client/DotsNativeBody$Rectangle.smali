.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rectangle"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;


# instance fields
.field private bitField0_:I

.field private height_:I

.field private width_:I

.field private x_:I

.field private y_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 159
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 164
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    .line 183
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    .line 202
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    .line 221
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    .line 159
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    .locals 3

    .prologue
    .line 253
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    return-object v0

    .line 254
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    if-ne p1, p0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v1

    .line 263
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 264
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 265
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 299
    const/4 v0, 0x0

    .line 300
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 301
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    .line 302
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 305
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    .line 306
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 309
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    .line 310
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 313
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    .line 314
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->cachedSize:I

    .line 317
    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 272
    const/16 v0, 0x11

    .line 273
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 274
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    add-int v0, v1, v2

    .line 275
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    add-int v0, v1, v2

    .line 276
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    add-int v0, v1, v2

    .line 277
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    add-int v0, v1, v2

    .line 278
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 326
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 330
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    :sswitch_0
    return-object p0

    .line 336
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    .line 337
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    goto :goto_0

    .line 341
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    .line 342
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    goto :goto_0

    .line 346
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    .line 347
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    goto :goto_0

    .line 351
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    .line 352
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    goto :goto_0

    .line 326
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 284
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->x_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 286
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 287
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->y_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 289
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 290
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 292
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 293
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 295
    :cond_3
    return-void
.end method
