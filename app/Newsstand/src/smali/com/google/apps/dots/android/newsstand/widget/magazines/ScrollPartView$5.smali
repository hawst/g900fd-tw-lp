.class Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;
.super Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;
.source "ScrollPartView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->initGestureDetectors()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

.field final synthetic val$temp:Landroid/graphics/Matrix;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .prologue
    .line 801
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->val$temp:Landroid/graphics/Matrix;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOnDoubleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 897
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerDoubleTapZoomInGesture:Z
    invoke-static {v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2202(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z

    .line 899
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->handleDoubleTapAt(FF)V
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;FF)V

    .line 900
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->onZoomAttempt()V

    .line 902
    :cond_0
    return v3
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "vx"    # F
    .param p4, "vy"    # F

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    neg-float v1, p3

    neg-float v2, p4

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNearestControlInDirection(FFF)Z

    .line 892
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "dx"    # F
    .param p4, "dy"    # F

    .prologue
    .line 804
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->scaleDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/view/ScaleGestureDetector;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v5

    if-nez v5, :cond_5

    .line 805
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransform(Landroid/graphics/Matrix;)V

    .line 806
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->val$temp:Landroid/graphics/Matrix;

    neg-float v6, p3

    neg-float v7, p4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 807
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->val$temp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setTransform(Landroid/graphics/Matrix;)V

    .line 809
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    const/4 v1, 0x1

    .line 810
    .local v1, "isScrollingHorizontally":Z
    :goto_0
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    const/4 v2, 0x1

    .line 812
    .local v2, "isScrollingVertically":Z
    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    if-eqz v1, :cond_8

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .line 813
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForLeftBoundary()Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForRightBoundary()Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_0
    const/4 v5, 0x1

    .line 812
    :goto_2
    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstLeftOrRight:Z
    invoke-static {v6, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$802(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z

    .line 814
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    if-eqz v2, :cond_9

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .line 815
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForTopBoundary()Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didAdjustScrollForBottomBoundary()Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_1
    const/4 v5, 0x1

    .line 814
    :goto_3
    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstTopOrBottom:Z
    invoke-static {v6, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z

    .line 817
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstLeftOrRight:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isScrollingAgainstTopOrBottom:Z
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 818
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    const/4 v6, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->hasScrolledInternally:Z
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1402(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z

    .line 821
    :cond_2
    const/4 v3, 0x0

    .line 822
    .local v3, "pulled":Z
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isHorizontallyScrollable()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 823
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)[F

    move-result-object v5

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    int-to-float v6, v6

    div-float v0, v5, v6

    .line 824
    .local v0, "horizPull":F
    const/4 v5, 0x0

    cmpg-float v5, v0, v5

    if-gez v5, :cond_a

    .line 825
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->leftEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v5

    neg-float v6, v0

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v5

    or-int/2addr v3, v5

    .line 830
    .end local v0    # "horizPull":F
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isVerticallyScrollable()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 831
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->transformConstraintCorrection:[F
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)[F

    move-result-object v5

    const/4 v6, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getHeight()I

    move-result v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 832
    .local v4, "vertPull":F
    const/4 v5, 0x0

    cmpg-float v5, v4, v5

    if-gez v5, :cond_b

    .line 833
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v5

    neg-float v6, v4

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v5

    or-int/2addr v3, v5

    .line 838
    .end local v4    # "vertPull":F
    :cond_4
    :goto_5
    if-eqz v3, :cond_5

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->willNotDraw()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 839
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->setWillNotDraw(Z)V

    .line 842
    .end local v1    # "isScrollingHorizontally":Z
    .end local v2    # "isScrollingVertically":Z
    .end local v3    # "pulled":Z
    :cond_5
    const/4 v5, 0x0

    return v5

    .line 809
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 810
    .restart local v1    # "isScrollingHorizontally":Z
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 813
    .restart local v2    # "isScrollingVertically":Z
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 815
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 826
    .restart local v0    # "horizPull":F
    .restart local v3    # "pulled":Z
    :cond_a
    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-lez v5, :cond_3

    .line 827
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->rightEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1700(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v5

    or-int/2addr v3, v5

    goto :goto_4

    .line 834
    .end local v0    # "horizPull":F
    .restart local v4    # "vertPull":F
    :cond_b
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-lez v5, :cond_4

    .line 835
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$1900(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v5

    or-int/2addr v3, v5

    goto :goto_5
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->onUnhandledClick()V

    .line 884
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 847
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->isInGesture:Z
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 875
    :cond_0
    :goto_0
    return v3

    .line 851
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->enableGutterTap:Z
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 856
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3e19999a    # 0.15f

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    move v0, v2

    .line 857
    .local v0, "leftGutterTap":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3f59999a    # 0.85f

    mul-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    move v1, v2

    .line 858
    .local v1, "rightGutterTap":Z
    :goto_2
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    .line 859
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->getTransformScale()F

    move-result v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->isUnzoomedScale(F)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 860
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->didTriggerPageFlipInGesture:Z
    invoke-static {v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->access$2102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;Z)Z

    .line 864
    if-eqz v0, :cond_5

    .line 865
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToPreviousPage()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .end local v0    # "leftGutterTap":Z
    .end local v1    # "rightGutterTap":Z
    :cond_3
    move v0, v3

    .line 856
    goto :goto_1

    .restart local v0    # "leftGutterTap":Z
    :cond_4
    move v1, v3

    .line 857
    goto :goto_2

    .line 869
    .restart local v1    # "rightGutterTap":Z
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollPartView;->snapToNextPage()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0
.end method
