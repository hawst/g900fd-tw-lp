.class public Lcom/google/apps/dots/android/newsstand/store/Version;
.super Ljava/lang/Object;
.source "Version.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/Version;",
        ">;"
    }
.end annotation


# static fields
.field public static final ANY:Lcom/google/apps/dots/android/newsstand/store/Version;

.field public static final LATEST:Lcom/google/apps/dots/android/newsstand/store/Version;


# instance fields
.field public final localMutationCount:I

.field public final serverVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 10
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7fffffff

    invoke-direct {v0, v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(JI)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/Version;->LATEST:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 11
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(JI)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/Version;->ANY:Lcom/google/apps/dots/android/newsstand/store/Version;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "serverVersion"    # J

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(JI)V

    .line 20
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1
    .param p1, "serverVersion"    # J
    .param p3, "localMutationCount"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    .line 24
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    .line 25
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/apps/dots/android/newsstand/store/Version;)I
    .locals 6
    .param p1, "version"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 37
    invoke-static {}, Lcom/google/common/collect/ComparisonChain;->start()Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    iget-wide v4, p1, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    .line 38
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/common/collect/ComparisonChain;->compare(JJ)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    iget v2, p1, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ComparisonChain;->compare(II)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/google/common/collect/ComparisonChain;->result()I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/Version;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/Version;->compareTo(Lcom/google/apps/dots/android/newsstand/store/Version;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 45
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/store/Version;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 47
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/store/Version;
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    iget-wide v4, v0, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 50
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/store/Version;
    :cond_0
    return v1
.end method

.method public newerThan(Lcom/google/apps/dots/android/newsstand/store/Version;)Z
    .locals 1
    .param p1, "otherVersion"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/Version;->compareTo(Lcom/google/apps/dots/android/newsstand/store/Version;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->serverVersion:J

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/store/Version;->localMutationCount:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
