.class public Lcom/google/apps/dots/android/newsstand/widget/RatingsView;
.super Lcom/google/android/play/layout/StarRatingBar;
.source "RatingsView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private bindRatingKey:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/StarRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RatingsView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 26
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 27
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RatingsView_bindRating:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->bindRatingKey:Ljava/lang/Integer;

    .line 28
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 30
    :cond_0
    return-void
.end method

.method private getRatingContentDescription()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->star_rating:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->getRating()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->getRating()F

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    rem-float/2addr v6, v7

    invoke-static {v6, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-lez v6, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    :cond_0
    add-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v3, v4

    .line 40
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setRating(F)V
    .locals 1
    .param p1, "rating"    # F

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 35
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->getRatingContentDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 36
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->bindRatingKey:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 47
    if-nez p1, :cond_1

    const/high16 v0, 0x800000

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->setRating(F)V

    .line 49
    :cond_0
    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RatingsView;->bindRatingKey:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method
