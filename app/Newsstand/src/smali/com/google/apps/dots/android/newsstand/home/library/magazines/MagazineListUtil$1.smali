.class final Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$1;
.super Ljava/lang/Object;
.source "MagazineListUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I
    .locals 5
    .param p1, "data1"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "data2"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 40
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 41
    .local v0, "publicationDate1":J
    sget v4, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_PUBLICATION_DATE:I

    invoke-virtual {p2, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 43
    .local v2, "publicationDate2":J
    invoke-static {v2, v3, v0, v1}, Lcom/google/common/primitives/Longs;->compare(JJ)I

    move-result v4

    return v4
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    check-cast p2, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil$1;->compare(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v0

    return v0
.end method
