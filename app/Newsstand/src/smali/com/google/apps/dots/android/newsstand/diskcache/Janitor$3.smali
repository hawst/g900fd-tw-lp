.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->statFs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 6
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 171
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getStats()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-result-object v0

    .line 172
    .local v0, "stats":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1000(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->totalCacheOnDiskSize()J

    move-result-wide v4

    add-long/2addr v2, v4

    # setter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1002(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;J)J

    .line 173
    return-void
.end method
