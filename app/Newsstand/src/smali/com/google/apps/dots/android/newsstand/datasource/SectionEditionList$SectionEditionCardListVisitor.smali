.class public Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList$SectionEditionCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;
.source "SectionEditionList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SectionEditionCardListVisitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList$SectionEditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "[Section]"

    return-object v0
.end method

.method protected ignoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 3

    .prologue
    .line 51
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList$SectionEditionCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;

    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/datasource/SectionEditionList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 52
    .local v1, "sectionEdition":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 53
    .local v0, "editionForSection":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v2, :cond_0

    .line 54
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .end local v0    # "editionForSection":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIgnoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v2

    .line 56
    :goto_0
    return-object v2

    .restart local v0    # "editionForSection":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
