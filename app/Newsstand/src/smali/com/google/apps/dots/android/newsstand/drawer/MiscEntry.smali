.class public abstract Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
.super Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
.source "MiscEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;
    }
.end annotation


# instance fields
.field public final miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;


# direct methods
.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;)V
    .locals 1
    .param p1, "miscType"    # Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    .prologue
    .line 20
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->MISC:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;)V

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 36
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;

    .line 38
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 40
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
    :cond_0
    return v1
.end method

.method public abstract getLabelResId()I
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->getLabelResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{type: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
