.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;
.super Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/events/AsyncEventObserver;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected onEventAsync(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden:Z

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateMeteredCount()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    .line 172
    :cond_0
    return-void
.end method
