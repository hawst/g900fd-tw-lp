.class public final Lcom/google/apps/dots/proto/client/DotsShared$Application;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Application"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Application;


# instance fields
.field private adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

.field private adTemplateSelection_:Ljava/lang/String;

.field private analyticsId_:Ljava/lang/String;

.field private appFamilyId_:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field private bitField0_:I

.field private category_:I

.field private checkoutId_:Ljava/lang/String;

.field private countryCode_:Ljava/lang/String;

.field private created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private description_:Ljava/lang/String;

.field private dynamicIconAttachmentId_:Ljava/lang/String;

.field private externalId_:Ljava/lang/String;

.field private googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

.field private headerBackgroundColor_:Ljava/lang/String;

.field private iconAttachmentId_:Ljava/lang/String;

.field private iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

.field private languageCode_:Ljava/lang/String;

.field private leadCurationEntityId_:Ljava/lang/String;

.field private leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

.field private longShareUrl_:Ljava/lang/String;

.field public mediaLibraryAttachmentIds:[Ljava/lang/String;

.field private meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

.field public meteredSectionIds:[Ljava/lang/String;

.field private mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

.field private name_:Ljava/lang/String;

.field public orderedSectionId:[Ljava/lang/String;

.field public previewAttachmentId:[Ljava/lang/String;

.field public previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private privacyPolicy_:Ljava/lang/String;

.field private promoIconAttachmentId_:Ljava/lang/String;

.field private publicationDate_:J

.field private shortShareUrl_:Ljava/lang/String;

.field private splashAttachmentId_:Ljava/lang/String;

.field private storeType_:I

.field private translationCode_:Ljava/lang/String;

.field private translationEnabled_:Z

.field private untranslatedAppFamilyId_:Ljava/lang/String;

.field private untranslatedAppId_:Ljava/lang/String;

.field private updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private useHeaderBackgroundImages_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3626
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Application;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Application;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3627
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3632
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    .line 3635
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    .line 3657
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    .line 3679
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 3701
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    .line 3761
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    .line 3783
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    .line 3805
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    .line 3846
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    .line 3868
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    .line 3890
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    .line 3912
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    .line 4010
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    .line 4032
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    .line 4051
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    .line 4070
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    .line 4073
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4076
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    .line 4098
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    .line 4120
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    .line 4142
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    .line 4164
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    .line 4187
    const-string v0, "und"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    .line 4209
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    .line 4231
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    .line 4251
    const-string v0, "US"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    .line 4273
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    .line 4295
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    .line 4315
    const-string v0, "#000000"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    .line 4337
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    .line 4356
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    .line 4359
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    .line 3627
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .locals 5

    .prologue
    .line 4431
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4435
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_0

    .line 4436
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4438
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_1

    .line 4439
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4441
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_2

    .line 4442
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4444
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 4445
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    .line 4447
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v3, :cond_4

    .line 4448
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4450
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v3, :cond_5

    .line 4451
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4453
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v3, :cond_6

    .line 4454
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4456
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-eqz v3, :cond_7

    .line 4457
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    .line 4459
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-eqz v3, :cond_8

    .line 4460
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .line 4462
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 4463
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    .line 4465
    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 4466
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4467
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-ge v2, v3, :cond_b

    .line 4468
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v3, v3, v2

    if-eqz v3, :cond_a

    .line 4469
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    aput-object v4, v3, v2

    .line 4467
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4432
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 4433
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 4473
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_c

    .line 4474
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    .line 4476
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v3, :cond_d

    .line 4477
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 4479
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 4480
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    .line 4482
    :cond_e
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3623
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Application;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4487
    if-ne p1, p0, :cond_1

    .line 4530
    :cond_0
    :goto_0
    return v1

    .line 4488
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 4489
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    .line 4490
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Application;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4491
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4492
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4493
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4494
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 4495
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 4496
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4497
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4498
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4499
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 4500
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4501
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4502
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4503
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    .line 4504
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_3

    .line 4505
    :goto_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_3

    .line 4506
    :goto_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v3, :cond_3

    .line 4507
    :goto_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-nez v3, :cond_15

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-nez v3, :cond_3

    .line 4508
    :goto_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-nez v3, :cond_16

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-nez v3, :cond_3

    .line 4509
    :goto_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    if-nez v3, :cond_17

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4510
    :goto_14
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    .line 4513
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4514
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_18

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4515
    :goto_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_19

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4516
    :goto_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    if-nez v3, :cond_1a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4517
    :goto_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    if-nez v3, :cond_1b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4518
    :goto_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    if-nez v3, :cond_1c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4519
    :goto_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_1d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4520
    :goto_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_1e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4521
    :goto_1b
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    if-nez v3, :cond_1f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4523
    :goto_1c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    .line 4524
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_20

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_3

    .line 4525
    :goto_1d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_21

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 4527
    :goto_1e
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    .line 4529
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    if-nez v3, :cond_22

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 4530
    goto/16 :goto_0

    .line 4490
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    .line 4491
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    .line 4492
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 4493
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    .line 4494
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4495
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4496
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    .line 4497
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    .line 4498
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    .line 4499
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4500
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    .line 4501
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    .line 4502
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    .line 4503
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    .line 4504
    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4505
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4506
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4507
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    .line 4508
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_12

    :cond_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .line 4509
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_13

    :cond_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    .line 4510
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_14

    .line 4514
    :cond_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    .line 4515
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_15

    :cond_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    .line 4516
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_16

    :cond_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    .line 4517
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_17

    :cond_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    .line 4518
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_18

    :cond_1c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    .line 4519
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_19

    :cond_1d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    .line 4520
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1a

    :cond_1e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    .line 4521
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1b

    :cond_1f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    .line 4523
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1c

    .line 4524
    :cond_20
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 4525
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1d

    :cond_21
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    .line 4527
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1e

    .line 4529
    :cond_22
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    .line 4530
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAdFrequency()Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;
    .locals 1

    .prologue
    .line 3993
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    return-object v0
.end method

.method public getAppFamilyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3659
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    return-object v0
.end method

.method public getGoogleSoldAds()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    .locals 1

    .prologue
    .line 3974
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    return-object v0
.end method

.method public getIconAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3807
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .locals 1

    .prologue
    .line 3917
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    return-object v0
.end method

.method public getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .locals 1

    .prologue
    .line 3936
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    return-object v0
.end method

.method public getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .locals 1

    .prologue
    .line 3955
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3763
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 4744
    const/4 v2, 0x0

    .line 4745
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    .line 4746
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4747
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_0

    .line 4748
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4749
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4751
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_1

    .line 4752
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4753
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4755
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_2

    .line 4756
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    .line 4757
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4759
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_4

    .line 4760
    const/4 v0, 0x0

    .line 4761
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v1, v5, v4

    .line 4763
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 4761
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 4765
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v2, v0

    .line 4766
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 4768
    .end local v0    # "dataSize":I
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_5

    .line 4769
    const/16 v4, 0x13

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    .line 4770
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4772
    :cond_5
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    if-eqz v4, :cond_6

    .line 4773
    const/16 v4, 0x17

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    .line 4774
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4776
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_7

    .line 4777
    const/16 v4, 0x18

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    .line 4778
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 4780
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_8

    .line 4781
    const/16 v4, 0x1a

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    .line 4782
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4784
    :cond_8
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_9

    .line 4785
    const/16 v4, 0x1c

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    .line 4786
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4788
    :cond_9
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_a

    .line 4789
    const/16 v4, 0x1d

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    .line 4790
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4792
    :cond_a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_b

    .line 4793
    const/16 v4, 0x28

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    .line 4794
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4796
    :cond_b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_c

    .line 4797
    const/16 v4, 0x2b

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    .line 4798
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4800
    :cond_c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_d

    .line 4801
    const/16 v4, 0x2d

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    .line 4802
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4804
    :cond_d
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    if-eqz v4, :cond_e

    .line 4805
    const/16 v4, 0x30

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    .line 4806
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4808
    :cond_e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_f

    .line 4809
    const/16 v4, 0x31

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    .line 4810
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4812
    :cond_f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    if-eqz v4, :cond_10

    .line 4813
    const/16 v4, 0x34

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    .line 4814
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4816
    :cond_10
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_11

    .line 4817
    const/16 v4, 0x44

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    .line 4818
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4820
    :cond_11
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    if-eqz v4, :cond_12

    .line 4821
    const/16 v4, 0x4e

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    .line 4822
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4824
    :cond_12
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x80000

    and-int/2addr v4, v5

    if-eqz v4, :cond_13

    .line 4825
    const/16 v4, 0x53

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    .line 4826
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4828
    :cond_13
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v4, :cond_14

    .line 4829
    const/16 v4, 0x55

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4830
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4832
    :cond_14
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v4, :cond_15

    .line 4833
    const/16 v4, 0x56

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4834
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4836
    :cond_15
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_16

    .line 4837
    const/16 v4, 0x57

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    .line 4838
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 4840
    :cond_16
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_17

    .line 4841
    const/16 v4, 0x58

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    .line 4842
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4844
    :cond_17
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x200000

    and-int/2addr v4, v5

    if-eqz v4, :cond_18

    .line 4845
    const/16 v4, 0x59

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    .line 4846
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4848
    :cond_18
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v4, :cond_1a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_1a

    .line 4849
    const/4 v0, 0x0

    .line 4850
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_19

    aget-object v1, v5, v4

    .line 4852
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 4850
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 4854
    .end local v1    # "element":Ljava/lang/String;
    :cond_19
    add-int/2addr v2, v0

    .line 4855
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 4857
    .end local v0    # "dataSize":I
    :cond_1a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_1b

    .line 4858
    const/16 v4, 0x5e

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    .line 4859
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4861
    :cond_1b
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v4, :cond_1d

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_1d

    .line 4862
    const/4 v0, 0x0

    .line 4863
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_1c

    aget-object v1, v5, v4

    .line 4865
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 4863
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 4867
    .end local v1    # "element":Ljava/lang/String;
    :cond_1c
    add-int/2addr v2, v0

    .line 4868
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 4870
    .end local v0    # "dataSize":I
    :cond_1d
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-eqz v4, :cond_1e

    .line 4871
    const/16 v4, 0x64

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    .line 4872
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4874
    :cond_1e
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v4, :cond_1f

    .line 4875
    const/16 v4, 0x65

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 4876
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4878
    :cond_1f
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v4, :cond_20

    .line 4879
    const/16 v4, 0x66

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 4880
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4882
    :cond_20
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x400000

    and-int/2addr v4, v5

    if-eqz v4, :cond_21

    .line 4883
    const/16 v4, 0x67

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    .line 4884
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4886
    :cond_21
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_22

    .line 4887
    const/16 v4, 0x68

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 4888
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4890
    :cond_22
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-eqz v4, :cond_23

    .line 4891
    const/16 v4, 0x69

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .line 4892
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4894
    :cond_23
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x800000

    and-int/2addr v4, v5

    if-eqz v4, :cond_24

    .line 4895
    const/16 v4, 0x6b

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    .line 4896
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4898
    :cond_24
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v5, 0x1000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_25

    .line 4899
    const/16 v4, 0x6c

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    .line 4900
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 4902
    :cond_25
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    if-eqz v4, :cond_27

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_27

    .line 4903
    const/4 v0, 0x0

    .line 4904
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_26

    aget-object v1, v5, v4

    .line 4906
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 4904
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 4908
    .end local v1    # "element":Ljava/lang/String;
    :cond_26
    add-int/2addr v2, v0

    .line 4909
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 4911
    .end local v0    # "dataSize":I
    :cond_27
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_28

    .line 4912
    const/16 v4, 0x6e

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    .line 4913
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4915
    :cond_28
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_29

    .line 4916
    const/16 v4, 0x6f

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 4917
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4919
    :cond_29
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_2b

    .line 4920
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v5, v4

    :goto_4
    if-ge v3, v5, :cond_2b

    aget-object v1, v4, v3

    .line 4921
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v1, :cond_2a

    .line 4922
    const/16 v6, 0x74

    .line 4923
    invoke-static {v6, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v2, v6

    .line 4920
    :cond_2a
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 4927
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_2b
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v4, 0x2000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_2c

    .line 4928
    const/16 v3, 0x7b

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    .line 4929
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4931
    :cond_2c
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->cachedSize:I

    .line 4932
    return v2
.end method

.method public hasGoogleSoldAds()Z
    .locals 1

    .prologue
    .line 3984
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInterstitialAdSettings()Z
    .locals 1

    .prologue
    .line 3927
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeaderboardAdSettings()Z
    .locals 1

    .prologue
    .line 3946
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMrectAdSettings()Z
    .locals 1

    .prologue
    .line 3965
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4534
    const/16 v1, 0x11

    .line 4535
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 4536
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 4537
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 4538
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 4539
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 4540
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    if-nez v2, :cond_9

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 4541
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_a

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 4542
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_b

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    .line 4543
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 4544
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 4545
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 4546
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_f

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    .line 4547
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_10

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    .line 4548
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_11

    move v2, v3

    :goto_c
    add-int v1, v6, v2

    .line 4549
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_d
    add-int v1, v6, v2

    .line 4550
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    if-nez v2, :cond_13

    mul-int/lit8 v1, v1, 0x1f

    .line 4556
    :cond_0
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v2, :cond_15

    move v2, v3

    :goto_e
    add-int v1, v6, v2

    .line 4557
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v2, :cond_16

    move v2, v3

    :goto_f
    add-int v1, v6, v2

    .line 4558
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v2, :cond_17

    move v2, v3

    :goto_10
    add-int v1, v6, v2

    .line 4559
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-nez v2, :cond_18

    move v2, v3

    :goto_11
    add-int v1, v6, v2

    .line 4560
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-nez v2, :cond_19

    move v2, v3

    :goto_12
    add-int v1, v6, v2

    .line 4561
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    if-nez v2, :cond_1a

    move v2, v3

    :goto_13
    add-int v1, v6, v2

    .line 4562
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    const/16 v10, 0x20

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 4563
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    add-int v1, v2, v6

    .line 4564
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    if-nez v2, :cond_1b

    mul-int/lit8 v1, v1, 0x1f

    .line 4570
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_1d

    mul-int/lit8 v1, v1, 0x1f

    .line 4576
    :cond_2
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    if-nez v2, :cond_1f

    move v2, v3

    :goto_14
    add-int v1, v6, v2

    .line 4577
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    if-nez v2, :cond_20

    move v2, v3

    :goto_15
    add-int v1, v6, v2

    .line 4578
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    if-nez v2, :cond_21

    move v2, v3

    :goto_16
    add-int v1, v6, v2

    .line 4579
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    if-nez v2, :cond_22

    move v2, v3

    :goto_17
    add-int v1, v6, v2

    .line 4580
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    if-nez v2, :cond_23

    move v2, v3

    :goto_18
    add-int v1, v6, v2

    .line 4581
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_24

    move v2, v3

    :goto_19
    add-int v1, v6, v2

    .line 4582
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    if-nez v2, :cond_25

    move v2, v3

    :goto_1a
    add-int v1, v6, v2

    .line 4583
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    if-eqz v2, :cond_26

    move v2, v4

    :goto_1b
    add-int v1, v6, v2

    .line 4584
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    if-nez v2, :cond_27

    move v2, v3

    :goto_1c
    add-int v1, v6, v2

    .line 4585
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    if-nez v2, :cond_28

    mul-int/lit8 v1, v1, 0x1f

    .line 4591
    :cond_3
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v2, :cond_2a

    move v2, v3

    :goto_1d
    add-int v1, v6, v2

    .line 4592
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    add-int v1, v2, v6

    .line 4593
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v2, :cond_2b

    move v2, v3

    :goto_1e
    add-int v1, v6, v2

    .line 4594
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    if-eqz v6, :cond_2c

    :goto_1f
    add-int v1, v2, v4

    .line 4595
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    if-nez v2, :cond_2d

    mul-int/lit8 v1, v1, 0x1f

    .line 4601
    :cond_4
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    if-nez v4, :cond_2f

    :goto_20
    add-int v1, v2, v3

    .line 4602
    return v1

    .line 4536
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 4537
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 4538
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 4539
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 4540
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 4541
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 4542
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 4543
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 4544
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 4545
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 4546
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 4547
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 4548
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 4549
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 4552
    :cond_13
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_21
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 4553
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v3

    :goto_22
    add-int v1, v6, v2

    .line 4552
    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    .line 4553
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_22

    .line 4556
    .end local v0    # "i":I
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 4557
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 4558
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 4559
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->hashCode()I

    move-result v2

    goto/16 :goto_11

    .line 4560
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;->hashCode()I

    move-result v2

    goto/16 :goto_12

    .line 4561
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_13

    .line 4566
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_23
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 4567
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_1c

    move v2, v3

    :goto_24
    add-int v1, v6, v2

    .line 4566
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 4567
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_24

    .line 4572
    .end local v0    # "i":I
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_25
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4573
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    if-nez v2, :cond_1e

    move v2, v3

    :goto_26
    add-int v1, v6, v2

    .line 4572
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 4573
    :cond_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_26

    .line 4576
    .end local v0    # "i":I
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_14

    .line 4577
    :cond_20
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_15

    .line 4578
    :cond_21
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_16

    .line 4579
    :cond_22
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_17

    .line 4580
    :cond_23
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_18

    .line 4581
    :cond_24
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_19

    .line 4582
    :cond_25
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1a

    :cond_26
    move v2, v5

    .line 4583
    goto/16 :goto_1b

    .line 4584
    :cond_27
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1c

    .line 4587
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_27
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 4588
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_29

    move v2, v3

    :goto_28
    add-int v1, v6, v2

    .line 4587
    add-int/lit8 v0, v0, 0x1

    goto :goto_27

    .line 4588
    :cond_29
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_28

    .line 4591
    .end local v0    # "i":I
    :cond_2a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->hashCode()I

    move-result v2

    goto/16 :goto_1d

    .line 4593
    :cond_2b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1e

    :cond_2c
    move v4, v5

    .line 4594
    goto/16 :goto_1f

    .line 4597
    :cond_2d
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_29
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 4598
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2e

    move v2, v3

    :goto_2a
    add-int v1, v4, v2

    .line 4597
    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    .line 4598
    :cond_2e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2a

    .line 4601
    .end local v0    # "i":I
    :cond_2f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto/16 :goto_20
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 4940
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4941
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4945
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 4946
    :sswitch_0
    return-object p0

    .line 4951
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    goto :goto_0

    .line 4955
    :sswitch_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v6, :cond_1

    .line 4956
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4958
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4962
    :sswitch_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v6, :cond_2

    .line 4963
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 4965
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4969
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    .line 4970
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto :goto_0

    .line 4974
    :sswitch_5
    const/16 v6, 0x52

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4975
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v1, v6

    .line 4976
    .local v1, "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 4977
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4978
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    .line 4979
    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 4980
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 4981
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4979
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4984
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto :goto_0

    .line 4988
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    .line 4989
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 4993
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    .line 4994
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 4998
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    .line 4999
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5003
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    .line 5004
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5008
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    .line 5009
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5013
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 5014
    .local v4, "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    if-eq v4, v9, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_4

    const/4 v6, 0x4

    if-eq v4, v6, :cond_4

    const/4 v6, 0x5

    if-eq v4, v6, :cond_4

    const/4 v6, 0x6

    if-eq v4, v6, :cond_4

    const/16 v6, 0xa

    if-eq v4, v6, :cond_4

    const/16 v6, 0x23

    if-eq v4, v6, :cond_4

    const/16 v6, 0xd

    if-eq v4, v6, :cond_4

    const/4 v6, 0x7

    if-eq v4, v6, :cond_4

    const/16 v6, 0x8

    if-eq v4, v6, :cond_4

    const/16 v6, 0xb

    if-eq v4, v6, :cond_4

    const/16 v6, 0xc

    if-eq v4, v6, :cond_4

    const/16 v6, 0x9

    if-eq v4, v6, :cond_4

    const/16 v6, 0xe

    if-eq v4, v6, :cond_4

    const/16 v6, 0xf

    if-eq v4, v6, :cond_4

    const/16 v6, 0x10

    if-eq v4, v6, :cond_4

    const/16 v6, 0x11

    if-eq v4, v6, :cond_4

    const/16 v6, 0x12

    if-eq v4, v6, :cond_4

    const/16 v6, 0x13

    if-eq v4, v6, :cond_4

    const/16 v6, 0x14

    if-eq v4, v6, :cond_4

    const/16 v6, 0x15

    if-eq v4, v6, :cond_4

    const/16 v6, 0x16

    if-eq v4, v6, :cond_4

    const/16 v6, 0x17

    if-eq v4, v6, :cond_4

    const/16 v6, 0x18

    if-eq v4, v6, :cond_4

    const/16 v6, 0x19

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1a

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1b

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1c

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1d

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1e

    if-eq v4, v6, :cond_4

    const/16 v6, 0x1f

    if-eq v4, v6, :cond_4

    const/16 v6, 0x20

    if-eq v4, v6, :cond_4

    const/16 v6, 0x21

    if-eq v4, v6, :cond_4

    const/16 v6, 0x22

    if-eq v4, v6, :cond_4

    const/16 v6, 0x24

    if-eq v4, v6, :cond_4

    const/16 v6, 0x25

    if-ne v4, v6, :cond_5

    .line 5052
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    .line 5053
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5055
    :cond_5
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    goto/16 :goto_0

    .line 5060
    .end local v4    # "temp":I
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    .line 5061
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5065
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    .line 5066
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5070
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    .line 5071
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5075
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    .line 5076
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5080
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    .line 5081
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5085
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    .line 5086
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5090
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    .line 5091
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5095
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    .line 5096
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x40000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5100
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    .line 5101
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5105
    :sswitch_15
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v6, :cond_6

    .line 5106
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 5108
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5112
    :sswitch_16
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v6, :cond_7

    .line 5113
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 5115
    :cond_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5119
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    .line 5120
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x100000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5124
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    .line 5125
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5129
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    .line 5130
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x200000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5134
    :sswitch_1a
    const/16 v6, 0x2d2

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5135
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v1, v6

    .line 5136
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 5137
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5138
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    .line 5139
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_8

    .line 5140
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 5141
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5139
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5144
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 5148
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    .line 5149
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5153
    :sswitch_1c
    const/16 v6, 0x2fa

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5154
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v1, v6

    .line 5155
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 5156
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5157
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    .line 5158
    :goto_3
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 5159
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 5160
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5158
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5163
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 5167
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_1d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-nez v6, :cond_a

    .line 5168
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    .line 5170
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5174
    :sswitch_1e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v6, :cond_b

    .line 5175
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 5177
    :cond_b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5181
    :sswitch_1f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-nez v6, :cond_c

    .line 5182
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .line 5184
    :cond_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5188
    :sswitch_20
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 5189
    .restart local v4    # "temp":I
    if-eqz v4, :cond_d

    if-eq v4, v8, :cond_d

    if-ne v4, v9, :cond_e

    .line 5192
    :cond_d
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    .line 5193
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x400000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5195
    :cond_e
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    goto/16 :goto_0

    .line 5200
    .end local v4    # "temp":I
    :sswitch_21
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_f

    .line 5201
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 5203
    :cond_f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5207
    :sswitch_22
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-nez v6, :cond_10

    .line 5208
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    .line 5210
    :cond_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5214
    :sswitch_23
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    .line 5215
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x800000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5219
    :sswitch_24
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    .line 5220
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x1000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5224
    :sswitch_25
    const/16 v6, 0x36a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5225
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v1, v6

    .line 5226
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 5227
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5228
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    .line 5229
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_11

    .line 5230
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 5231
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5229
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 5234
    :cond_11
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    goto/16 :goto_0

    .line 5238
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_26
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    .line 5239
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5243
    :sswitch_27
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 5244
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 5248
    :sswitch_28
    const/16 v6, 0x3a2

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5249
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v6, :cond_13

    move v1, v5

    .line 5250
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 5251
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v6, :cond_12

    .line 5252
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5254
    :cond_12
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 5255
    :goto_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_14

    .line 5256
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 5257
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 5258
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5255
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 5249
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_13
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v1, v6

    goto :goto_5

    .line 5261
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_14
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    aput-object v7, v6, v1

    .line 5262
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5266
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :sswitch_29
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    .line 5267
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v7, 0x2000000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    goto/16 :goto_0

    .line 4941
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x52 -> :sswitch_5
        0x9a -> :sswitch_6
        0xba -> :sswitch_7
        0xc0 -> :sswitch_8
        0xd2 -> :sswitch_9
        0xe2 -> :sswitch_a
        0xe8 -> :sswitch_b
        0x142 -> :sswitch_c
        0x15a -> :sswitch_d
        0x16a -> :sswitch_e
        0x182 -> :sswitch_f
        0x18a -> :sswitch_10
        0x1a2 -> :sswitch_11
        0x222 -> :sswitch_12
        0x272 -> :sswitch_13
        0x29a -> :sswitch_14
        0x2aa -> :sswitch_15
        0x2b2 -> :sswitch_16
        0x2b8 -> :sswitch_17
        0x2c2 -> :sswitch_18
        0x2ca -> :sswitch_19
        0x2d2 -> :sswitch_1a
        0x2f2 -> :sswitch_1b
        0x2fa -> :sswitch_1c
        0x322 -> :sswitch_1d
        0x32a -> :sswitch_1e
        0x332 -> :sswitch_1f
        0x338 -> :sswitch_20
        0x342 -> :sswitch_21
        0x34a -> :sswitch_22
        0x35a -> :sswitch_23
        0x360 -> :sswitch_24
        0x36a -> :sswitch_25
        0x372 -> :sswitch_26
        0x37a -> :sswitch_27
        0x3a2 -> :sswitch_28
        0x3da -> :sswitch_29
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3623
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Application;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 4607
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4608
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_0

    .line 4609
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4611
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_1

    .line 4612
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4614
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 4615
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->name_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4617
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 4618
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->orderedSectionId:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 4619
    .local v0, "element":Ljava/lang/String;
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4618
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4622
    .end local v0    # "element":Ljava/lang/String;
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    .line 4623
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->externalId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4625
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_5

    .line 4626
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->analyticsId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4628
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_6

    .line 4629
    const/16 v2, 0x18

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->publicationDate_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 4631
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_7

    .line 4632
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->description_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4634
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_8

    .line 4635
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->shortShareUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4637
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_9

    .line 4638
    const/16 v2, 0x1d

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->category_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4640
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_a

    .line 4641
    const/16 v2, 0x28

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4643
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_b

    .line 4644
    const/16 v2, 0x2b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->checkoutId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4646
    :cond_b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_c

    .line 4647
    const/16 v2, 0x2d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->splashAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4649
    :cond_c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_d

    .line 4650
    const/16 v2, 0x30

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->longShareUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4652
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_e

    .line 4653
    const/16 v2, 0x31

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4655
    :cond_e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_f

    .line 4656
    const/16 v2, 0x34

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->privacyPolicy_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4658
    :cond_f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_10

    .line 4659
    const/16 v2, 0x44

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->dynamicIconAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4661
    :cond_10
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    if-eqz v2, :cond_11

    .line 4662
    const/16 v2, 0x4e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4664
    :cond_11
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    if-eqz v2, :cond_12

    .line 4665
    const/16 v2, 0x53

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4667
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v2, :cond_13

    .line 4668
    const/16 v2, 0x55

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->interstitialAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4670
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v2, :cond_14

    .line 4671
    const/16 v2, 0x56

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leaderboardAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4673
    :cond_14
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    if-eqz v2, :cond_15

    .line 4674
    const/16 v2, 0x57

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->translationEnabled_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4676
    :cond_15
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_16

    .line 4677
    const/16 v2, 0x58

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adTemplateSelection_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4679
    :cond_16
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    if-eqz v2, :cond_17

    .line 4680
    const/16 v2, 0x59

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->countryCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4682
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 4683
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_18

    aget-object v0, v3, v2

    .line 4684
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v5, 0x5a

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4683
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4687
    .end local v0    # "element":Ljava/lang/String;
    :cond_18
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_19

    .line 4688
    const/16 v2, 0x5e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->promoIconAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4690
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 4691
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_1a

    aget-object v0, v3, v2

    .line 4692
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v5, 0x5f

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4691
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4695
    .end local v0    # "element":Ljava/lang/String;
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-eqz v2, :cond_1b

    .line 4696
    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->googleSoldAds_:Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4698
    :cond_1b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v2, :cond_1c

    .line 4699
    const/16 v2, 0x65

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4701
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    if-eqz v2, :cond_1d

    .line 4702
    const/16 v2, 0x66

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mrectAdSettings_:Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4704
    :cond_1d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1e

    .line 4705
    const/16 v2, 0x67

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->storeType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4707
    :cond_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_1f

    .line 4708
    const/16 v2, 0x68

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4710
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    if-eqz v2, :cond_20

    .line 4711
    const/16 v2, 0x69

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->adFrequency_:Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4713
    :cond_20
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    if-eqz v2, :cond_21

    .line 4714
    const/16 v2, 0x6b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->headerBackgroundColor_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4716
    :cond_21
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_22

    .line 4717
    const/16 v2, 0x6c

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->useHeaderBackgroundImages_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4719
    :cond_22
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    if-eqz v2, :cond_23

    .line 4720
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_23

    aget-object v0, v3, v2

    .line 4721
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v5, 0x6d

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4720
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4724
    .end local v0    # "element":Ljava/lang/String;
    :cond_23
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_24

    .line 4725
    const/16 v2, 0x6e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4727
    :cond_24
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_25

    .line 4728
    const/16 v2, 0x6f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4730
    :cond_25
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_27

    .line 4731
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewImage:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_27

    aget-object v0, v2, v1

    .line 4732
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    if-eqz v0, :cond_26

    .line 4733
    const/16 v4, 0x74

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4731
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4737
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_27
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->bitField0_:I

    const/high16 v2, 0x2000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_28

    .line 4738
    const/16 v1, 0x7b

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->leadCurationEntityId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4740
    :cond_28
    return-void
.end method
