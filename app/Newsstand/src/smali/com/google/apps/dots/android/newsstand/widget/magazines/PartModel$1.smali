.class Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;
.super Ljava/lang/Object;
.source "PartModel.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;->configureAnalyticsEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

.field final synthetic val$appId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;->val$appId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/net/Uri;)V
    .locals 6
    .param p1, "event"    # Landroid/net/Uri;

    .prologue
    .line 58
    const-string v4, "category"

    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "category":Ljava/lang/String;
    const-string v4, "action"

    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "action":Ljava/lang/String;
    const-string v4, "label"

    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "label":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/PartModel$1;->val$appId:Ljava/lang/String;

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->tryFindIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "appFamilyId":Ljava/lang/String;
    return-void
.end method
