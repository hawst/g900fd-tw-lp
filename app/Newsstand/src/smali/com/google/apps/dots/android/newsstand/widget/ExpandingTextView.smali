.class public Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;
.super Lcom/google/apps/dots/android/newsstand/widget/NSTextView;
.source "ExpandingTextView.java"


# instance fields
.field protected maxLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 28
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 42
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_0

    .line 43
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 44
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->getLineHeight()I

    move-result v2

    div-int v1, v0, v2

    .line 46
    .local v1, "newMaxLines":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->maxLines:I

    if-eq v1, v2, :cond_0

    .line 47
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->setMaxLines(I)V

    .line 50
    .end local v0    # "height":I
    .end local v1    # "newMaxLines":I
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->onMeasure(II)V

    .line 51
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0
    .param p1, "maxLines"    # I

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setMaxLines(I)V

    .line 33
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ExpandingTextView;->maxLines:I

    .line 34
    return-void
.end method
