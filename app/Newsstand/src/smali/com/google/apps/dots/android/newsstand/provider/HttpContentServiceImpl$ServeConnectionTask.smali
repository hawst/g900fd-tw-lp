.class Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "HttpContentServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServeConnectionTask"
.end annotation


# instance fields
.field private requestToken:Ljava/lang/String;

.field private socket:Ljava/net/Socket;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Lcom/google/apps/dots/android/newsstand/async/Queue;)V
    .locals 0
    .param p2, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    .line 216
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    .line 217
    return-void
.end method

.method private checkPrefixedRequestToken(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "encodedPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 261
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 262
    .local v1, "skip":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v4, v1, 0x10

    add-int/lit8 v4, v4, 0x1

    if-lt v3, v4, :cond_0

    .line 263
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v3, v1, 0x10

    .line 264
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2f

    if-eq v3, v4, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v2

    .line 267
    :cond_1
    add-int/lit8 v3, v1, 0x10

    invoke-virtual {p2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "requestToken":Ljava/lang/String;
    monitor-enter p0

    .line 269
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$000(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 270
    monitor-exit p0

    goto :goto_0

    .line 273
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 272
    :cond_2
    :try_start_1
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->requestToken:Ljava/lang/String;

    .line 273
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private handleConnection(Ljava/net/Socket;)V
    .locals 34
    .param p1, "socket"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    const/16 v16, 0x0

    .line 280
    .local v16, "fd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    new-instance v8, Lorg/apache/http/impl/DefaultHttpServerConnection;

    invoke-direct {v8}, Lorg/apache/http/impl/DefaultHttpServerConnection;-><init>()V

    .line 281
    .local v8, "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    new-instance v3, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v3}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v3}, Lorg/apache/http/impl/DefaultHttpServerConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    .line 282
    invoke-virtual {v8}, Lorg/apache/http/impl/DefaultHttpServerConnection;->receiveRequestHeader()Lorg/apache/http/HttpRequest;

    move-result-object v21

    .line 286
    .local v21, "request":Lorg/apache/http/HttpRequest;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->appContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$600(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v23

    .line 288
    .local v23, "resolver":Landroid/content/ContentResolver;
    :try_start_1
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 289
    .local v22, "requestUri":Landroid/net/Uri;
    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v11

    .line 290
    .local v11, "encodedPath":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Request URI: %s, path: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v22, v5, v6

    const/4 v6, 0x1

    aput-object v11, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    if-nez v11, :cond_1

    .line 294
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381
    .end local v11    # "encodedPath":Ljava/lang/String;
    .end local v22    # "requestUri":Landroid/net/Uri;
    :catch_0
    move-exception v10

    .line 383
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "File not found."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v10, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    new-instance v24, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v3, Lorg/apache/http/ProtocolVersion;

    const-string v4, "HTTP"

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    const/16 v4, 0x194

    const-string v5, "File not found."

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 386
    .local v24, "response":Lorg/apache/http/HttpResponse;
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    const-string v4, "File not found."

    invoke-direct {v3, v4}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 390
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v3, "Connection"

    const-string v4, "close"

    move-object/from16 v0, v24

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;->sendResponseHeader(Lorg/apache/http/HttpResponse;)V

    .line 392
    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/apache/http/impl/DefaultHttpServerConnection;->sendResponseEntity(Lorg/apache/http/HttpResponse;)V

    .line 393
    invoke-virtual {v8}, Lorg/apache/http/impl/DefaultHttpServerConnection;->close()V
    :try_end_2
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 409
    .end local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v21    # "request":Lorg/apache/http/HttpRequest;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    .end local v24    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    :goto_1
    return-void

    .line 295
    .restart local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v11    # "encodedPath":Ljava/lang/String;
    .restart local v21    # "request":Lorg/apache/http/HttpRequest;
    .restart local v22    # "requestUri":Landroid/net/Uri;
    .restart local v23    # "resolver":Landroid/content/ContentResolver;
    :cond_1
    :try_start_3
    const-string v3, "/w"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->checkPrefixedRequestToken(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 296
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/WebDataContentProvider;->contentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "/w"

    .line 297
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 296
    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "noPumpFd"

    const-string v5, "true"

    .line 298
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 320
    .local v9, "contentUri":Landroid/net/Uri;
    :cond_2
    :goto_2
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Translated to content URI: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v9, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    const-wide/16 v28, -0x1

    .line 324
    .local v28, "startByte":J
    const-wide/16 v14, -0x1

    .line 325
    .local v14, "endByte":J
    const-string v3, "Range"

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Lorg/apache/http/HttpRequest;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_4

    aget-object v17, v4, v3

    .line 326
    .local v17, "header":Lorg/apache/http/Header;
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rangePattern:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$700()Ljava/util/regex/Pattern;

    move-result-object v6

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v20

    .line 327
    .local v20, "rangeMatcher":Ljava/util/regex/Matcher;
    invoke-virtual/range {v20 .. v20}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 328
    const/4 v6, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v25

    .line 329
    .local v25, "start":Ljava/lang/String;
    const/4 v6, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    .line 331
    .local v12, "end":Ljava/lang/String;
    :try_start_4
    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v28

    .line 332
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 333
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 335
    const-wide/16 v6, 0x1

    add-long/2addr v14, v6

    .line 337
    :cond_3
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v6

    const-string v7, "Parsed range %s - %s"

    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    aput-object v33, v31, v32

    move-object/from16 v0, v31

    invoke-virtual {v6, v7, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 346
    .end local v12    # "end":Ljava/lang/String;
    .end local v17    # "header":Lorg/apache/http/Header;
    .end local v20    # "rangeMatcher":Ljava/util/regex/Matcher;
    .end local v25    # "start":Ljava/lang/String;
    :cond_4
    :try_start_5
    const-string v3, "r"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v16

    .line 347
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v26

    .line 348
    .local v26, "sourceLength":J
    const-wide/16 v4, 0x0

    cmp-long v3, v4, v28

    if-gtz v3, :cond_a

    cmp-long v3, v28, v26

    if-gez v3, :cond_a

    .line 350
    cmp-long v3, v28, v14

    if-gtz v3, :cond_9

    cmp-long v3, v14, v26

    if-gtz v3, :cond_9

    .line 358
    :goto_4
    new-instance v2, Landroid/content/res/AssetFileDescriptor;

    .line 359
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v4

    add-long v4, v4, v28

    sub-long v6, v14, v28

    invoke-direct/range {v2 .. v7}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    .line 362
    .local v2, "rangeFd":Landroid/content/res/AssetFileDescriptor;
    new-instance v13, Lorg/apache/http/entity/InputStreamEntity;

    .line 363
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-direct {v13, v3, v4, v5}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 366
    .local v13, "entity":Lorg/apache/http/entity/InputStreamEntity;
    const-string v3, "application/octet-stream"

    invoke-virtual {v13, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 367
    new-instance v24, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v3, Lorg/apache/http/ProtocolVersion;

    const-string v4, "HTTP"

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    const/16 v4, 0xce

    const-string v5, "OK"

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 368
    .restart local v24    # "response":Lorg/apache/http/HttpResponse;
    const-string v3, "Content-Range"

    const-string v4, "bytes %s-%s/%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 369
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide/16 v32, 0x1

    sub-long v32, v14, v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 368
    move-object/from16 v0, v24

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    move-object/from16 v0, v24

    invoke-interface {v0, v13}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 394
    .end local v2    # "rangeFd":Landroid/content/res/AssetFileDescriptor;
    .end local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v9    # "contentUri":Landroid/net/Uri;
    .end local v11    # "encodedPath":Ljava/lang/String;
    .end local v13    # "entity":Lorg/apache/http/entity/InputStreamEntity;
    .end local v14    # "endByte":J
    .end local v21    # "request":Lorg/apache/http/HttpRequest;
    .end local v22    # "requestUri":Landroid/net/Uri;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    .end local v24    # "response":Lorg/apache/http/HttpResponse;
    .end local v26    # "sourceLength":J
    .end local v28    # "startByte":J
    :catch_1
    move-exception v3

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto/16 :goto_1

    .line 299
    .restart local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v11    # "encodedPath":Ljava/lang/String;
    .restart local v21    # "request":Lorg/apache/http/HttpRequest;
    .restart local v22    # "requestUri":Landroid/net/Uri;
    .restart local v23    # "resolver":Landroid/content/ContentResolver;
    :cond_5
    :try_start_6
    const-string v3, "/e"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->checkPrefixedRequestToken(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 300
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$Attachments;->exportedContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "/e"

    .line 301
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 300
    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "noPumpFd"

    const-string v5, "true"

    .line 302
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .restart local v9    # "contentUri":Landroid/net/Uri;
    goto/16 :goto_2

    .line 303
    .end local v9    # "contentUri":Landroid/net/Uri;
    :cond_6
    const-string v3, "/c"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->checkPrefixedRequestToken(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 304
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "content"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 305
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "/c"

    .line 306
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 305
    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "noPumpFd"

    const-string v5, "true"

    .line 307
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 310
    .restart local v9    # "contentUri":Landroid/net/Uri;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/provider/Contract;->match(Landroid/net/Uri;)I

    move-result v18

    .line 311
    .local v18, "match":I
    const/4 v3, 0x3

    move/from16 v0, v18

    if-eq v0, v3, :cond_2

    const/4 v3, 0x4

    move/from16 v0, v18

    if-eq v0, v3, :cond_2

    const/4 v3, 0x6

    move/from16 v0, v18

    if-eq v0, v3, :cond_2

    .line 314
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 396
    .end local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v9    # "contentUri":Landroid/net/Uri;
    .end local v11    # "encodedPath":Ljava/lang/String;
    .end local v18    # "match":I
    .end local v21    # "request":Lorg/apache/http/HttpRequest;
    .end local v22    # "requestUri":Landroid/net/Uri;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    :catch_2
    move-exception v3

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto/16 :goto_1

    .line 317
    .restart local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v11    # "encodedPath":Ljava/lang/String;
    .restart local v21    # "request":Lorg/apache/http/HttpRequest;
    .restart local v22    # "requestUri":Landroid/net/Uri;
    .restart local v23    # "resolver":Landroid/content/ContentResolver;
    :cond_7
    :try_start_7
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 398
    .end local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v11    # "encodedPath":Ljava/lang/String;
    .end local v21    # "request":Lorg/apache/http/HttpRequest;
    .end local v22    # "requestUri":Landroid/net/Uri;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    :catch_3
    move-exception v10

    .line 399
    .local v10, "e":Ljava/io/IOException;
    :try_start_8
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto/16 :goto_1

    .line 339
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .restart local v9    # "contentUri":Landroid/net/Uri;
    .restart local v11    # "encodedPath":Ljava/lang/String;
    .restart local v12    # "end":Ljava/lang/String;
    .restart local v14    # "endByte":J
    .restart local v17    # "header":Lorg/apache/http/Header;
    .restart local v20    # "rangeMatcher":Ljava/util/regex/Matcher;
    .restart local v21    # "request":Lorg/apache/http/HttpRequest;
    .restart local v22    # "requestUri":Landroid/net/Uri;
    .restart local v23    # "resolver":Landroid/content/ContentResolver;
    .restart local v25    # "start":Ljava/lang/String;
    .restart local v28    # "startByte":J
    :catch_4
    move-exception v19

    .line 340
    .local v19, "nfe":Ljava/lang/NumberFormatException;
    const-wide/16 v28, -0x1

    .line 341
    const-wide/16 v14, -0x1

    .line 325
    .end local v12    # "end":Ljava/lang/String;
    .end local v19    # "nfe":Ljava/lang/NumberFormatException;
    .end local v25    # "start":Ljava/lang/String;
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 353
    .end local v17    # "header":Lorg/apache/http/Header;
    .end local v20    # "rangeMatcher":Ljava/util/regex/Matcher;
    .restart local v26    # "sourceLength":J
    :cond_9
    move-wide/from16 v14, v26

    goto/16 :goto_4

    .line 374
    :cond_a
    :try_start_9
    new-instance v13, Lorg/apache/http/entity/InputStreamEntity;

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v3

    move-wide/from16 v0, v26

    invoke-direct {v13, v3, v0, v1}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 375
    .restart local v13    # "entity":Lorg/apache/http/entity/InputStreamEntity;
    const-string v3, "application/octet-stream"

    invoke-virtual {v13, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 376
    new-instance v24, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v3, Lorg/apache/http/ProtocolVersion;

    const-string v4, "HTTP"

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    const/16 v4, 0xc8

    const-string v5, "OK"

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 377
    .restart local v24    # "response":Lorg/apache/http/HttpResponse;
    const-string v3, "Content-Length"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, v24

    invoke-interface {v0, v13}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/apache/http/ConnectionClosedException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lorg/apache/http/HttpException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 400
    .end local v8    # "connection":Lorg/apache/http/impl/DefaultHttpServerConnection;
    .end local v9    # "contentUri":Landroid/net/Uri;
    .end local v11    # "encodedPath":Ljava/lang/String;
    .end local v13    # "entity":Lorg/apache/http/entity/InputStreamEntity;
    .end local v14    # "endByte":J
    .end local v21    # "request":Lorg/apache/http/HttpRequest;
    .end local v22    # "requestUri":Landroid/net/Uri;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    .end local v24    # "response":Lorg/apache/http/HttpResponse;
    .end local v26    # "sourceLength":J
    .end local v28    # "startByte":J
    :catch_5
    move-exception v10

    .line 401
    .local v10, "e":Lorg/apache/http/HttpException;
    :try_start_a
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto/16 :goto_1

    .line 402
    .end local v10    # "e":Lorg/apache/http/HttpException;
    :catch_6
    move-exception v30

    .line 403
    .local v30, "tr":Ljava/lang/Throwable;
    :try_start_b
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 405
    if-eqz v16, :cond_0

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto/16 :goto_1

    .line 405
    .end local v30    # "tr":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_b

    .line 406
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_b
    throw v3
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;->cancel()V

    .line 230
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$400(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 231
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$300(Ljava/net/Socket;)V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    .line 233
    return-void
.end method

.method protected doInBackground()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 243
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$500(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/net/ServerSocket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    .line 244
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->handleConnection(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 248
    monitor-enter p0

    .line 249
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->requestToken:Ljava/lang/String;

    .line 250
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$300(Ljava/net/Socket;)V

    .line 252
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    .line 253
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 250
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 248
    monitor-enter p0

    .line 249
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->requestToken:Ljava/lang/String;

    .line 250
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 251
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$300(Ljava/net/Socket;)V

    .line 252
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    .line 253
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 250
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1

    .line 248
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v1

    monitor-enter p0

    .line 249
    const/4 v2, 0x0

    :try_start_6
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->requestToken:Ljava/lang/String;

    .line 250
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 251
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$300(Ljava/net/Socket;)V

    .line 252
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    .line 253
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 255
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    :cond_1
    throw v1

    .line 250
    :catchall_3
    move-exception v1

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v1
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->this$0:Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$400(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 238
    return-void
.end method

.method public revokeToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "requestToken"    # Ljava/lang/String;

    .prologue
    .line 220
    monitor-enter p0

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->requestToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->socket:Ljava/net/Socket;

    # invokes: Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->access$300(Ljava/net/Socket;)V

    .line 224
    :cond_0
    monitor-exit p0

    .line 225
    return-void

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
