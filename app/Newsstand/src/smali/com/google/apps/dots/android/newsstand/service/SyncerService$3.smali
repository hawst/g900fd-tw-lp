.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;
.super Ljava/lang/Object;
.source "SyncerService.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService;->syncMyMagazinesLibrary(Landroid/accounts/Account;[B)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

.field final synthetic val$newMagazineAppIds:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/Set;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->val$newMagazineAppIds:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/Object;
    .locals 3
    .param p1, "allMags"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    const/4 v2, 0x0

    .line 448
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->val$newMagazineAppIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    :goto_0
    return-object v2

    .line 452
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 442
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$3;->apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
