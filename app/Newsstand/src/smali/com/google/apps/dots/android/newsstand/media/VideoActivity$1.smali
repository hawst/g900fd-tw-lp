.class Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "VideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupVideoItem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "messages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v2, 0x0

    .line 115
    .local v2, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    const/4 v0, 0x0

    .line 118
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-eqz p1, :cond_2

    .line 119
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 120
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v3, :cond_1

    .line 121
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 119
    .restart local v2    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v3, :cond_0

    .line 123
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .restart local v0    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    goto :goto_1

    .line 129
    .end local v1    # "i":I
    :cond_2
    if-nez v2, :cond_4

    .line 130
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->video_error:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 131
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 148
    :cond_3
    :goto_2
    return-void

    .line 134
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    # setter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$002(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 135
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->supportInvalidateOptionsMenu()V

    .line 136
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->getFirstVideoItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    move-result-object v4

    # setter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$102(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;Lcom/google/apps/dots/android/newsstand/media/VideoItem;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    .line 137
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->videoItem:Lcom/google/apps/dots/android/newsstand/media/VideoItem;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$100(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/android/newsstand/media/VideoItem;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 138
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # invokes: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->setupPlayer()V
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$200(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)V

    .line 142
    :cond_5
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$000(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 145
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$300(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    .line 146
    # getter for: Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->access$000(Lcom/google/apps/dots/android/newsstand/media/VideoActivity;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v5

    .line 145
    invoke-static {v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareParamsForPost(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 147
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/VideoActivity$1;->this$0:Lcom/google/apps/dots/android/newsstand/media/VideoActivity;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/media/VideoActivity;->supportInvalidateOptionsMenu()V

    goto :goto_2
.end method
