.class public Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;
.super Ljava/lang/Object;
.source "DateFormatUtil.java"


# direct methods
.method public static getLocalizedYear(I)Ljava/lang/String;
    .locals 4
    .param p0, "year"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 20
    if-lez p0, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 21
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 22
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0, v3, v2}, Ljava/util/Calendar;->set(III)V

    .line 23
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .end local v0    # "calendar":Ljava/util/Calendar;
    :cond_0
    move v1, v3

    .line 20
    goto :goto_0
.end method

.method public static getLocalizedYearAndMonth(Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 7
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "year"    # I
    .param p2, "month"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 31
    if-lez p1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 32
    if-lez p2, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 34
    .local v0, "calendar":Ljava/util/Calendar;
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1, v2}, Ljava/util/Calendar;->set(III)V

    .line 36
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->year_and_month:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "yyyy"

    aput-object v6, v5, v3

    const-string v3, "MMM"

    aput-object v3, v5, v2

    .line 37
    invoke-virtual {p0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .end local v0    # "calendar":Ljava/util/Calendar;
    :cond_0
    move v1, v3

    .line 31
    goto :goto_0

    :cond_1
    move v1, v3

    .line 32
    goto :goto_1
.end method

.method public static getLocalizedYearAndMonth(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 7
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "millis"    # J

    .prologue
    .line 45
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 46
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 47
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->year_and_month:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "yyyy"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "MMM"

    aput-object v5, v3, v4

    .line 48
    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLocalizedYearAndMonthRange(Landroid/content/res/Resources;III)Ljava/lang/String;
    .locals 11
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "year"    # I
    .param p2, "startMonth"    # I
    .param p3, "endMonth"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 58
    if-lez p1, :cond_0

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 59
    if-lez p2, :cond_1

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 60
    if-lez p3, :cond_2

    move v6, v7

    :goto_2
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 61
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "MMM"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-direct {v2, v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 62
    .local v2, "monthFormat":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 63
    .local v3, "startCalendar":Ljava/util/Calendar;
    add-int/lit8 v6, p2, -0x1

    invoke-virtual {v3, p1, v6, v7}, Ljava/util/Calendar;->set(III)V

    .line 64
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 65
    .local v4, "startMonthString":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 66
    .local v0, "endCalendar":Ljava/util/Calendar;
    add-int/lit8 v6, p3, -0x1

    invoke-virtual {v0, p1, v6, v7}, Ljava/util/Calendar;->set(III)V

    .line 67
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "endMonthString":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy"

    .line 69
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v6, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 70
    .local v5, "yearString":Ljava/lang/String;
    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->year_and_month_range:I

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v5, v9, v8

    aput-object v4, v9, v7

    const/4 v7, 0x2

    aput-object v1, v9, v7

    invoke-virtual {p0, v6, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    return-object v6

    .end local v0    # "endCalendar":Ljava/util/Calendar;
    .end local v1    # "endMonthString":Ljava/lang/String;
    .end local v2    # "monthFormat":Ljava/text/SimpleDateFormat;
    .end local v3    # "startCalendar":Ljava/util/Calendar;
    .end local v4    # "startMonthString":Ljava/lang/String;
    .end local v5    # "yearString":Ljava/lang/String;
    :cond_0
    move v6, v8

    .line 58
    goto :goto_0

    :cond_1
    move v6, v8

    .line 59
    goto :goto_1

    :cond_2
    move v6, v8

    .line 60
    goto :goto_2
.end method

.method public static getLocalizedYearAndOptionalMonthRange(Landroid/content/res/Resources;III)Ljava/lang/String;
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "year"    # I
    .param p2, "startMonth"    # I
    .param p3, "endMonth"    # I

    .prologue
    .line 80
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 81
    if-nez p2, :cond_1

    .line 83
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYear(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_1
    return-object v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_1
    if-nez p3, :cond_2

    .line 86
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYearAndMonth(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 89
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/util/DateFormatUtil;->getLocalizedYearAndMonthRange(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
