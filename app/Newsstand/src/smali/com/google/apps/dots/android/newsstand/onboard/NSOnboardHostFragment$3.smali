.class Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;
.super Ljava/lang/Object;
.source "NSOnboardHostFragment.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->submitQuizResultsAndCompleteOnboardFlow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v3, 0x0

    .line 306
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->hideOverlay()V

    .line 309
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3, p1, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 310
    .local v0, "errorData":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 311
    .local v1, "errorMessage":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 312
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->exitOnboardFlow()V

    .line 301
    return-void
.end method
