.class public Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "MusicAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 29
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.music"

    .line 30
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 31
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v1, "authAccount"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 33
    return-object v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.PLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "storeId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;->backendDocId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    const-string v1, "authAccount"

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 24
    return-object v0
.end method
