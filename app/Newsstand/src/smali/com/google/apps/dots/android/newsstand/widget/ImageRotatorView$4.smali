.class Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "ImageRotatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshNow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

.field final synthetic val$animateImage:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->val$animateImage:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 158
    if-eqz p1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->val$animateImage:Z

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->swapImage(Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$500(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Landroid/graphics/Bitmap;Z)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->scheduleImageRefresh()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$700(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$700(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$702(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 168
    :cond_2
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 155
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;->onSuccess(Landroid/graphics/Bitmap;)V

    return-void
.end method
