.class public Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;
.super Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;
.source "GroupRowTwoColumnSingleCardLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method private updateShouldShrinkAndEnlarge()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 85
    const/4 v1, 0x1

    .line 86
    .local v1, "childCanShrink":Z
    const/4 v0, 0x1

    .line 88
    .local v0, "childCanEnlarge":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 89
    invoke-static {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getContainedCardAt(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v2

    .line 90
    .local v2, "childView":Landroid/view/View;
    if-eqz v2, :cond_2

    instance-of v4, v2, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    if-eqz v4, :cond_2

    .line 91
    if-eqz v1, :cond_0

    move-object v4, v2

    check-cast v4, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canShrink()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v5

    .line 92
    :goto_1
    if-eqz v0, :cond_1

    check-cast v2, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    .end local v2    # "childView":Landroid/view/View;
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canEnlarge()Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v5

    .line 88
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .restart local v2    # "childView":Landroid/view/View;
    :cond_0
    move v1, v6

    .line 91
    goto :goto_1

    .end local v2    # "childView":Landroid/view/View;
    :cond_1
    move v0, v6

    .line 92
    goto :goto_2

    .line 95
    .restart local v2    # "childView":Landroid/view/View;
    :cond_2
    const/4 v1, 0x0

    .line 96
    const/4 v0, 0x0

    .line 100
    .end local v2    # "childView":Landroid/view/View;
    :cond_3
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldShrink:Z

    .line 101
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldEnlarge:Z

    .line 102
    return-void
.end method


# virtual methods
.method public arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I
    .locals 5
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->getNumCards()I

    move-result v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, p3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 69
    .local v1, "numCardsToFill":I
    new-array v2, v1, [I

    .line 71
    .local v2, "returnArray":[I
    if-lez v1, :cond_0

    .line 72
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 73
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 74
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 73
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getResizedLayoutId(ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 75
    const/4 v4, 0x0

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v4

    .line 80
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "returnArray":[I
    :goto_0
    return-object v2

    .restart local v2    # "returnArray":[I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected getDesiredHeightMeasureSpec(I)I
    .locals 2
    .param p1, "desiredRowHeight"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->updateShouldShrinkAndEnlarge()V

    .line 48
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldShrink()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, -0x80000000

    .line 49
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 53
    .local v0, "returnSpec":I
    :goto_0
    return v0

    .line 50
    .end local v0    # "returnSpec":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldEnlarge()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 51
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 52
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method protected getNumCards()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldEnlarge:Z

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;->shouldShrink:Z

    return v0
.end method
