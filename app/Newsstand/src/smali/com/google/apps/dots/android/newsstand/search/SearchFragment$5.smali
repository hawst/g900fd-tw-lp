.class Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->setUpAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/search/SearchFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 155
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    .line 156
    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->adapter:Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$000(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$SearchDataAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/search/SearchFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/search/SearchFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->refreshRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/search/SearchFragment;->access$400(Lcom/google/apps/dots/android/newsstand/search/SearchFragment;)Ljava/lang/Runnable;

    move-result-object v3

    .line 155
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
