.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "ArticleSeenEvent.java"


# instance fields
.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private sectionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 32
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 33
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 37
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 144
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 145
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;

    .line 146
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 147
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 148
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 150
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 4
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 96
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const-string v2, "Article Seen"

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 97
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 98
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->sectionId:Ljava/lang/String;

    .line 99
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 100
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 101
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 102
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 103
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 104
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 105
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 109
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->sectionName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->sectionName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setSectionName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 113
    :cond_0
    const-string v2, "MiniMode"

    .line 114
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->useCompactMode()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    .line 113
    invoke-static {p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 118
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "readingEditionAppId":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 120
    const-string v2, "ReadFromAppId"

    invoke-static {p1, v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_1
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 134
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 43
    sget-object v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 84
    const/4 v2, 0x0

    .line 87
    .local v2, "screen":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 45
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_0
    const-string v2, "ReadNow"

    .line 46
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 48
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_1
    const-string v5, "%s - %s"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v4, "[Topic]"

    aput-object v4, v6, v7

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .line 49
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v8

    .line 48
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 52
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_2
    const-string v5, "%s - %s"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v4, "[Topic]"

    aput-object v4, v6, v7

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 53
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v8

    .line 52
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 56
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_3
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getSectionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;

    move-result-object v3

    .line 57
    .local v3, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->sectionName:Ljava/lang/String;

    .line 58
    const-string v4, "%s %s - %s"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, "[Section]"

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 59
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->sectionName:Ljava/lang/String;

    aput-object v6, v5, v9

    .line 58
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 60
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 62
    .end local v2    # "screen":Ljava/lang/String;
    .end local v3    # "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    :pswitch_4
    const-string v2, "Bookmarks"

    .line 63
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 65
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_5
    const-string v2, "Search"

    .line 66
    .restart local v2    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 68
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_6
    const-string v4, "%s %s - %s"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, "[Magazine - TOC]"

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 69
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppFamilyName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    .line 68
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 70
    .restart local v2    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 74
    .end local v2    # "screen":Ljava/lang/String;
    :pswitch_7
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "postId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v4, v5, v1, v6}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 75
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 77
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 78
    new-instance v5, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v6, "Could not find post for postId = "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-direct {v5, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 80
    :cond_1
    const-string v4, "%s %s - %s"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, "[Related]"

    aput-object v6, v5, v7

    .line 81
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    .line 80
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .restart local v2    # "screen":Ljava/lang/String;
    goto/16 :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 129
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method
