.class public Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
.super Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;
.source "CollectionDataAdapter.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 1
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$1;-><init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method


# virtual methods
.method public getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter$2;-><init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;)V

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/data/NSBindingDataAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v0

    .line 30
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 33
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    :cond_0
    return-object v0
.end method
