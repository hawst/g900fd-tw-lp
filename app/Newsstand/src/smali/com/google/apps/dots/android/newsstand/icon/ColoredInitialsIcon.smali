.class public Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;
.super Ljava/lang/Object;
.source "ColoredInitialsIcon.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/icon/IconSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/icon/IconSource",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;",
        ">;"
    }
.end annotation


# instance fields
.field private color:I

.field private initials:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "color"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->initials:Ljava/lang/String;

    .line 16
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->color:I

    .line 17
    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;)V
    .locals 2
    .param p1, "roundTopicLogo"    # Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    .prologue
    .line 21
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->apply(Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;II)V

    .line 22
    return-void
.end method

.method public apply(Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;II)V
    .locals 1
    .param p1, "roundTopicLogo"    # Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    .param p2, "iconWidth"    # I
    .param p3, "iconHeight"    # I

    .prologue
    .line 31
    invoke-virtual {p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setMinimumWidth(I)V

    .line 32
    invoke-virtual {p1, p3}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setMinimumHeight(I)V

    .line 33
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->initials:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setInitials(Ljava/lang/CharSequence;)V

    .line 34
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;->color:I

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setBackgroundColor(I)V

    .line 35
    return-void
.end method
