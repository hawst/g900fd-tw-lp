.class public Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;
.super Ljava/lang/Object;
.source "NewFeatureCardHelper.java"


# static fields
.field private static final CARD_FREQUENCY_IN_MILLIS:J

.field public static final DK_NEW_FEATURE_CARD_EXCLUDE_DEVICES_LIST:I

.field public static final DK_NEW_FEATURE_CARD_IMAGE:I

.field public static final DK_NEW_FEATURE_CARD_SHOW_PREF_KEY:I

.field public static final DK_NEW_FEATURE_CARD_TEXT:I

.field public static final DK_NEW_FEATURE_CARD_TITLE:I

.field private static instance:Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

.field private static newFeatureCardData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private static newFeatureCardPreferenceKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    const-wide/32 v0, 0xea60

    .line 28
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->new_feature_card_fequency_minutes:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->CARD_FREQUENCY_IN_MILLIS:J

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NewFeatureCard_image:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_IMAGE:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NewFeatureCard_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TITLE:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NewFeatureCard_text:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TEXT:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NewFeatureCard_showPrefKey:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_SHOW_PREF_KEY:I

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->NewFeatureCard_excludeDevicesList:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_EXCLUDE_DEVICES_LIST:I

    .line 41
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardPreferenceKeys:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 58
    .local v1, "compactModeCard":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_minicards_scaleable:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->feature_card_text_mini_cards:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->mini_cards:I

    const-string v5, "compactModeNewFeatureCard"

    sget-object v0, Lcom/google/apps/dots/shared/DeviceCategory;->NORMAL_TABLET:Lcom/google/apps/dots/shared/DeviceCategory;

    .line 63
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    move-object v0, p0

    .line 58
    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;Ljava/util/List;)Lcom/google/android/libraries/bind/data/Data;

    .line 65
    new-instance v3, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v3}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 66
    .local v3, "translationCard":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_translate_scaleable:I

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->feature_card_text_translation:I

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->feature_card_title_translation:I

    const-string v7, "translationNewFeatureCard"

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    .line 72
    new-instance v5, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v5}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 73
    .local v5, "widgetCard":Lcom/google/android/libraries/bind/data/Data;
    sget v6, Lcom/google/android/apps/newsstanddev/R$drawable;->illo_wwc_widget_scaleable:I

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->feature_card_text_widget:I

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->feature_card_title_widget:I

    const-string v9, "widgetNewFeatureCard"

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    .line 80
    invoke-static {v1, v3, v5}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardData:Ljava/util/List;

    .line 85
    return-void
.end method

.method private addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "imageResKey"    # I
    .param p3, "textResKey"    # I
    .param p4, "titleResKey"    # I
    .param p5, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;Ljava/util/List;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method private addCardData(Lcom/google/android/libraries/bind/data/Data;IIILjava/lang/String;Ljava/util/List;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "imageResKey"    # I
    .param p3, "textResKey"    # I
    .param p4, "titleResKey"    # I
    .param p5, "preferenceKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/Data;",
            "III",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/shared/DeviceCategory;",
            ">;)",
            "Lcom/google/android/libraries/bind/data/Data;"
        }
    .end annotation

    .prologue
    .line 169
    .local p6, "optExcludeDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/shared/DeviceCategory;>;"
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 170
    .local v0, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    .line 171
    .local v1, "primaryKey":Ljava/util/UUID;
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 173
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    invoke-virtual {p1, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 175
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_IMAGE:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 176
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TEXT:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 177
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_TITLE:I

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 178
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_SHOW_PREF_KEY:I

    invoke-virtual {p1, v2, p5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 179
    if-eqz p6, :cond_0

    .line 180
    sget v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_EXCLUDE_DEVICES_LIST:I

    invoke-virtual {p1, v2, p6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 183
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardPreferenceKeys:Ljava/util/List;

    invoke-interface {v2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    return-object p1
.end method

.method public static getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->instance:Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->instance:Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    .line 47
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->instance:Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    return-object v0
.end method

.method private shouldShow(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 5
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v3, 0x0

    .line 143
    sget v4, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_EXCLUDE_DEVICES_LIST:I

    .line 144
    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 145
    .local v1, "ineligibleDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/shared/DeviceCategory;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 146
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    if-eqz v1, :cond_1

    .line 147
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v3

    .line 152
    :cond_1
    sget v4, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->DK_NEW_FEATURE_CARD_SHOW_PREF_KEY:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "prefKey":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 154
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0
.end method


# virtual methods
.method public clearDismissibleNewFeaturePreferences()V
    .locals 4

    .prologue
    .line 199
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    .line 200
    .local v1, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getFeatureCardPreferenceKeys()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 201
    .local v0, "preferenceKey":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 203
    .end local v0    # "preferenceKey":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public dismissCard(Ljava/lang/String;)V
    .locals 4
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 92
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLastNewFeatureCardDismissTime(J)V

    .line 93
    return-void
.end method

.method public getFeatureCardPreferenceKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardPreferenceKeys:Ljava/util/List;

    return-object v0
.end method

.method public getNewFeatureCardToShow()Lcom/google/android/libraries/bind/data/Data;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLastNewFeatureCardDismissTime()J

    move-result-wide v4

    .line 123
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-wide v8, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->CARD_FREQUENCY_IN_MILLIS:J

    sub-long/2addr v6, v8

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    move-object v1, v2

    .line 132
    :goto_0
    return-object v1

    .line 127
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 128
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->shouldShow(Lcom/google/android/libraries/bind/data/Data;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->newFeatureCardData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0

    .line 127
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 132
    goto :goto_0
.end method

.method public makeSetFeatureCardShownRunnable(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "preferenceKey"    # Ljava/lang/String;

    .prologue
    .line 96
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;Ljava/lang/String;)V

    return-object v0
.end method
