.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "EditableLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->commitNewsEdit(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

.field final synthetic val$isPurchased:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->val$isPurchased:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->account()Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Landroid/accounts/Account;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->val$isPurchased:Z

    const/4 v3, 0x1

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    .line 387
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 382
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$9;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
