.class public final Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GoogleSoldAdSettings"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;


# instance fields
.field private bitField0_:I

.field private percent_:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3511
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3512
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3517
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    .line 3512
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    .locals 3

    .prologue
    .line 3546
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3550
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    return-object v0

    .line 3547
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    :catch_0
    move-exception v1

    .line 3548
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3508
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->clone()Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3555
    if-ne p1, p0, :cond_1

    .line 3558
    :cond_0
    :goto_0
    return v1

    .line 3556
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3557
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    .line 3558
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getPercent()F
    .locals 1

    .prologue
    .line 3519
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3577
    const/4 v0, 0x0

    .line 3578
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3579
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    .line 3580
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 3582
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->cachedSize:I

    .line 3583
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 3562
    const/16 v0, 0x11

    .line 3563
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 3564
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 3565
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3592
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3596
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3597
    :sswitch_0
    return-object p0

    .line 3602
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    .line 3603
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->bitField0_:I

    goto :goto_0

    .line 3592
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3508
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3570
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3571
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;->percent_:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 3573
    :cond_0
    return-void
.end method
