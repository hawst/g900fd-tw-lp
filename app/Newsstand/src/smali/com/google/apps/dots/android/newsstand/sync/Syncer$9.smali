.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Application;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Application;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 739
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 742
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v3, "%s: syncing deps"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v4

    invoke-virtual {v0, v3, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 743
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 744
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getAppFamilyId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppFamilySummaryAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v0, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 745
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 747
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    iget-object v8, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->previewAttachmentId:[Ljava/lang/String;

    array-length v9, v8

    move v6, v4

    :goto_0
    if-ge v6, v9, :cond_0

    aget-object v2, v8, v6

    .line 748
    .local v2, "previewAttachmentId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 747
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 751
    .end local v2    # "previewAttachmentId":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->hasInterstitialAdSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 752
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdFormatSettingsTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V
    invoke-static {v0, v1, v3, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V

    .line 754
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->hasLeaderboardAdSettings()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 755
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdFormatSettingsTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V
    invoke-static {v0, v1, v3, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V

    .line 757
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->hasMrectAdSettings()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 758
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addAdFormatSettingsTasks(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V
    invoke-static {v0, v1, v3, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;Ljava/lang/Object;)V

    .line 760
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Application;->mediaLibraryAttachmentIds:[Ljava/lang/String;

    array-length v11, v3

    move v0, v4

    :goto_1
    if-ge v0, v11, :cond_4

    aget-object v7, v3, v0

    .line 761
    .local v7, "mediaLibraryAttachmentId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->val$app:Lcom/google/apps/dots/proto/client/DotsShared$Application;

    sget-object v10, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v6, v1

    move v9, v4

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v5 .. v10}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 760
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 764
    .end local v7    # "mediaLibraryAttachmentId":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$9;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
