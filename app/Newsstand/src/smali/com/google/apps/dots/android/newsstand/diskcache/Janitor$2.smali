.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 107
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Starting for %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    const-string v1, "janitor"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    move-result v0

    .line 111
    .local v0, "traceRestorePoint":I
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->statFs()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$300(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    .line 113
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 114
    const-string v1, "janitor-unpin"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 115
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->globalUnpin(Z)V

    .line 116
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 118
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 119
    const-string v1, "janitor-evict"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 120
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->evictOldBlobs()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$400(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    .line 121
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 123
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 124
    const-string v1, "janitor-split"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 125
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->splitStoreFiles()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$500(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    .line 126
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 128
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 129
    const-string v1, "janitor-compact"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 130
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->compactStoreFiles()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$600(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    .line 131
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 133
    sget-object v1, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 134
    const-string v1, "janitor-close"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 135
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->closeStoreFiles()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$700(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    .line 143
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$800(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 144
    :try_start_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    const/4 v3, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->requested:Z
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$902(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Z)Z

    .line 145
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 146
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Finished for %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    return-void

    .line 137
    :catch_0
    move-exception v1

    .line 140
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection(I)V

    throw v1

    .line 145
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
