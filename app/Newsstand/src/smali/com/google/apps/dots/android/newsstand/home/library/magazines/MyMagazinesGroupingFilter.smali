.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;
.super Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;
.source "MyMagazinesGroupingFilter.java"


# static fields
.field public static final DK_GROUPING_ID:I

.field public static final DK_MAGAZINE_ISSUES:I


# instance fields
.field fragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

.field resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->AppFamilyList_magazineIssues:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    .line 48
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MyMagazinesFragment_groupArchiveId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_GROUPING_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;
    .param p2, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;
    .param p3, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceAppFilter;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    .line 55
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->fragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    .line 56
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->resources:Landroid/content/res/Resources;

    .line 57
    return-void
.end method

.method private bucketMagazineDates(Ljava/util/Calendar;Ljava/util/List;)Ljava/util/Map;
    .locals 10
    .param p1, "calendar"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 212
    .local p2, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v6

    .line 213
    .local v6, "yearMonths":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 214
    .local v0, "inputData":Lcom/google/android/libraries/bind/data/Data;
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getYear(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v4

    .line 215
    .local v4, "year":I
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getMonth(Ljava/util/Calendar;Lcom/google/android/libraries/bind/data/Data;)I

    move-result v1

    .line 216
    .local v1, "month":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 217
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 218
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 219
    .local v5, "yearMonth":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    .end local v5    # "yearMonth":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 222
    .local v2, "monthIssuesCount":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 223
    .local v3, "monthMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 228
    .end local v2    # "monthIssuesCount":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    .end local v3    # "monthMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    .line 229
    .restart local v2    # "monthIssuesCount":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 230
    .restart local v3    # "monthMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 235
    .end local v0    # "inputData":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "month":I
    .end local v2    # "monthIssuesCount":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    .end local v3    # "monthMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v4    # "year":I
    :cond_2
    return-object v6
.end method

.method private convertDataToMagazineTile(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;
    .locals 6
    .param p1, "sourceData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 337
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/bind/data/Data;-><init>(Lcom/google/android/libraries/bind/data/Data;)V

    .line 339
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->LAYOUTS:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 340
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 343
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_TITLE:I

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_ISSUE_NAME:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 344
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_ICON_ID:I

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_ATTACHMENT_ID:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 345
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_ICON_ID:I

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_TRANSITION_NAME:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 346
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_CONTENT_DESCRIPTION:I

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CONTENT_DESCRIPTION:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 347
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 348
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_COVER_HEIGHT_TO_WIDTH_RATIO:I

    const/high16 v4, 0x3fc00000    # 1.5f

    .line 349
    invoke-static {v0, v4}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 348
    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 350
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 351
    .local v2, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_SOURCE_TRANSITION_NAME:I

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 352
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazineItem;->DK_CLICK_LISTENER:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$2;

    invoke-direct {v4, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 364
    return-object v1
.end method

.method private createToggleButton(Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "isShowingArchive"    # Z

    .prologue
    .line 380
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 381
    .local v0, "blankLine":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_GROUPING_ID:I

    const-string v3, "emptyRow"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 383
    if-eqz p1, :cond_0

    .line 384
    const-string v1, "Showing Archive"

    .line 388
    .local v1, "showingState":Ljava/lang/String;
    :goto_0
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 389
    return-object v0

    .line 386
    .end local v1    # "showingState":Ljava/lang/String;
    :cond_0
    const-string v1, "Hiding Archive"

    .restart local v1    # "showingState":Ljava/lang/String;
    goto :goto_0
.end method

.method private generateDateGroup(Ljava/util/List;ILjava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 10
    .param p2, "year"    # I
    .param p3, "archiveStateAppend"    # Ljava/lang/String;
    .param p4, "usePartYear"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;>;>;I",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/libraries/bind/data/Data;"
        }
    .end annotation

    .prologue
    .local p1, "months":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 302
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 303
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 304
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v5, v8}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 306
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 308
    .local v2, "monthEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    const-string v5, "%d-%d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v8, v6

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 309
    .local v4, "yearMonthId":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR_MONTH_KEY:I

    invoke-virtual {v1, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 310
    sget v7, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_GROUPING_ID:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v8, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v1, v7, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 313
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 314
    .local v0, "cumulativeMagazineList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 316
    .local v3, "monthLoopEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .end local v0    # "cumulativeMagazineList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "monthEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    .end local v3    # "monthLoopEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    .end local v4    # "yearMonthId":Ljava/lang/String;
    :cond_0
    move v5, v7

    .line 302
    goto :goto_0

    .line 310
    .restart local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "monthEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    .restart local v4    # "yearMonthId":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 318
    .restart local v0    # "cumulativeMagazineList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_2
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    sget v7, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_ID:I

    .line 319
    invoke-direct {p0, v7, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->getMagazineDataList(ILjava/util/List;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v7

    .line 318
    invoke-virtual {v1, v5, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 322
    if-eqz p4, :cond_3

    .line 323
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_MONTH:I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v5, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 325
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v6, :cond_3

    .line 326
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "monthEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    check-cast v2, Ljava/util/Map$Entry;

    .line 327
    .restart local v2    # "monthEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    sget v5, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_END_MONTH:I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 330
    :cond_3
    return-object v1
.end method

.method private generateDateRowData(Ljava/util/Map;ILjava/lang/String;)Ljava/util/List;
    .locals 17
    .param p2, "numColumns"    # I
    .param p3, "archiveStateAppend"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;>;>;I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "yearMonths":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 241
    .local v10, "outList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    mul-int/lit8 v5, p2, 0x1

    .line 242
    .local v5, "maxGroupSize":I
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 246
    .local v12, "year":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    const/4 v2, 0x0

    .line 247
    .local v2, "alreadyOutputPartYear":Z
    const/4 v3, 0x0

    .line 248
    .local v3, "cumulativeNumIssues":I
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 249
    .local v9, "monthsList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    .line 250
    .local v8, "monthSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    const/4 v7, 0x0

    .line 251
    .local v7, "monthIndex":I
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 252
    .local v6, "month":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    add-int/2addr v13, v3

    if-le v13, v5, :cond_4

    .line 255
    if-nez v2, :cond_1

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v13

    add-int/lit8 v16, v7, 0x1

    move/from16 v0, v16

    if-le v13, v0, :cond_2

    :cond_1
    const/4 v11, 0x1

    .line 256
    .local v11, "usePartYear":Z
    :goto_2
    if-nez v3, :cond_3

    .line 258
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v9, v13, v1, v11}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->generateDateGroup(Ljava/util/List;ILjava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    .line 261
    .local v4, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 273
    :goto_3
    const/4 v2, 0x1

    .line 280
    .end local v4    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v11    # "usePartYear":Z
    :goto_4
    add-int/lit8 v7, v7, 0x1

    .line 281
    goto :goto_1

    .line 255
    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    .line 265
    .restart local v11    # "usePartYear":Z
    :cond_3
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v9, v13, v1, v11}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->generateDateGroup(Ljava/util/List;ILjava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    .line 267
    .restart local v4    # "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 270
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_3

    .line 277
    .end local v4    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v11    # "usePartYear":Z
    :cond_4
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    add-int/2addr v3, v13

    goto :goto_4

    .line 285
    .end local v6    # "month":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;"
    :cond_5
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 286
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v9, v13, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->generateDateGroup(Ljava/util/List;ILjava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    .line 288
    .restart local v4    # "data":Lcom/google/android/libraries/bind/data/Data;
    sget v13, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->DK_YEAR:I

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v4, v13, v15}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 289
    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 292
    .end local v2    # "alreadyOutputPartYear":Z
    .end local v3    # "cumulativeNumIssues":I
    .end local v4    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v7    # "monthIndex":I
    .end local v8    # "monthSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    .end local v9    # "monthsList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    .end local v12    # "year":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    :cond_6
    return-object v10
.end method

.method private getArchiveSublist(Ljava/util/List;Z)Ljava/util/List;
    .locals 5
    .param p2, "keepArchived"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "sourceList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    .line 108
    .local v1, "subList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 109
    .local v0, "entry":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    if-ne v3, p2, :cond_0

    .line 110
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    .end local v0    # "entry":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return-object v1
.end method

.method private getMagazineDataList(ILjava/util/List;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 5
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)",
            "Lcom/google/android/libraries/bind/data/DataList;"
        }
    .end annotation

    .prologue
    .line 370
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 371
    .local v1, "magazineTileList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 372
    .local v2, "sourceData":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->convertDataToMagazineTile(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 373
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 375
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "sourceData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazineListUtil;->getMagazineSortComparator()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 376
    new-instance v3, Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {v3, p1, v1}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    return-object v3
.end method

.method private subTransformByAppFamily(Ljava/util/List;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 122
    .local v9, "outList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 178
    :goto_0
    return-object v9

    .line 125
    :cond_0
    const/4 v5, 0x0

    .line 126
    .local v5, "currentAppFamilyId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 127
    .local v11, "previousRow":Lcom/google/android/libraries/bind/data/Data;
    const/4 v8, 0x0

    .line 128
    .local v8, "issueCount":I
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 130
    .local v6, "currentRowMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/libraries/bind/data/Data;

    sget v13, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    const/4 v14, 0x0

    .line 131
    invoke-virtual {v12, v13, v14}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v12

    if-eqz v12, :cond_2

    const-string v4, "_archived"

    .line 134
    .local v4, "archiveStateAppend":Ljava/lang/String;
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/bind/data/Data;

    .line 135
    .local v7, "inputRow":Lcom/google/android/libraries/bind/data/Data;
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_ID:I

    invoke-virtual {v7, v12}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "appFamilyId":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 138
    if-eqz v11, :cond_1

    .line 139
    sget v12, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    sget v14, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_ID:I

    .line 140
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->getMagazineDataList(ILjava/util/List;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v14

    .line 139
    invoke-virtual {v11, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 141
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NUM_ISSUES:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 145
    :cond_1
    new-instance v6, Ljava/util/LinkedList;

    .end local v6    # "currentRowMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 146
    .restart local v6    # "currentRowMagazines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v10, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v10}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 148
    .local v10, "outputRow":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    move-object v5, v2

    .line 150
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_SUMMARY:I

    invoke-virtual {v7, v12}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 151
    .local v3, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    sget v14, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_GROUPING_ID:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_3

    invoke-virtual {v15, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    :goto_3
    invoke-virtual {v10, v14, v12}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 152
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_ID:I

    invoke-virtual {v10, v12, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 153
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_APP_FAMILY_SUMMARY:I

    invoke-virtual {v10, v12, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 154
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NAME:I

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 155
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_ICON_ID:I

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 156
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_DESCRIPTION:I

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getDescription()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 157
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_UPDATED:I

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getUpdateTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v10, v12, v14}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 158
    move-object v11, v10

    .line 159
    const/4 v8, 0x1

    .line 160
    goto/16 :goto_2

    .line 131
    .end local v2    # "appFamilyId":Ljava/lang/String;
    .end local v3    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .end local v4    # "archiveStateAppend":Ljava/lang/String;
    .end local v7    # "inputRow":Lcom/google/android/libraries/bind/data/Data;
    .end local v10    # "outputRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    const-string v4, "_not_archived"

    goto/16 :goto_1

    .line 151
    .restart local v2    # "appFamilyId":Ljava/lang/String;
    .restart local v3    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .restart local v4    # "archiveStateAppend":Ljava/lang/String;
    .restart local v7    # "inputRow":Lcom/google/android/libraries/bind/data/Data;
    .restart local v10    # "outputRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 161
    .end local v3    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .end local v10    # "outputRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_4
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 166
    .end local v2    # "appFamilyId":Ljava/lang/String;
    .end local v7    # "inputRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_5
    if-eqz v11, :cond_6

    .line 167
    sget v12, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->DK_MAGAZINE_ISSUES:I

    sget v13, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_ID:I

    .line 168
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v6}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->getMagazineDataList(ILjava/util/List;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v13

    .line 167
    invoke-virtual {v11, v12, v13}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 169
    sget v12, Lcom/google/apps/dots/android/newsstand/datasource/AppFamilyList;->DK_NUM_ISSUES:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 171
    :cond_6
    new-instance v12, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;)V

    invoke-static {v9, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method private subTransformByDate(Ljava/util/List;I)Ljava/util/List;
    .locals 8
    .param p2, "numColumns"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v7, 0x0

    .line 187
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 188
    .local v2, "outList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v3, v2

    .line 203
    .end local v2    # "outList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local v3, "outList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :goto_0
    return-object v3

    .line 191
    .end local v3    # "outList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .restart local v2    # "outList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_0
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/data/Data;

    sget v6, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    .line 192
    invoke-virtual {v5, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v0, "_archived"

    .line 194
    .local v0, "archiveStateAppend":Ljava/lang/String;
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 198
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-direct {p0, v1, p1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->bucketMagazineDates(Ljava/util/Calendar;Ljava/util/List;)Ljava/util/Map;

    move-result-object v4

    .line 199
    .local v4, "yearMonths":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    invoke-direct {p0, v4, p2, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->generateDateRowData(Ljava/util/Map;ILjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 202
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    move-object v3, v2

    .line 203
    .restart local v3    # "outList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    goto :goto_0

    .line 192
    .end local v0    # "archiveStateAppend":Ljava/lang/String;
    .end local v1    # "calendar":Ljava/util/Calendar;
    .end local v3    # "outList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v4    # "yearMonths":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;>;>;"
    :cond_1
    const-string v0, "_not_archived"

    goto :goto_1
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 11
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 69
    invoke-direct {p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->getArchiveSublist(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v7

    .line 70
    .local v7, "unarchivedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-direct {p0, p1, v8}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->getArchiveSublist(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    .line 73
    .local v0, "archivedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->fragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    if-nez v9, :cond_2

    move v3, v8

    .line 74
    .local v3, "isArchiveShowing":Z
    :goto_0
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->resources:Landroid/content/res/Resources;

    sget v10, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 76
    .local v4, "numColumns":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getMyMagazinesSortByField()I

    move-result v9

    if-ne v9, v8, :cond_0

    move v2, v8

    .line 79
    .local v2, "groupByTitle":Z
    :cond_0
    if-eqz v2, :cond_3

    .line 80
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->subTransformByAppFamily(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 86
    .local v6, "totalRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 87
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->createToggleButton(Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    .line 88
    .local v5, "toggleButton":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    if-eqz v3, :cond_1

    .line 91
    if-eqz v2, :cond_4

    .line 92
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->subTransformByAppFamily(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 96
    .local v1, "archivedRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :goto_2
    invoke-interface {v6, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 99
    .end local v1    # "archivedRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v5    # "toggleButton":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return-object v6

    .line 73
    .end local v2    # "groupByTitle":Z
    .end local v3    # "isArchiveShowing":Z
    .end local v4    # "numColumns":I
    .end local v6    # "totalRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_2
    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->fragment:Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragment;->isArchiveShowing()Z

    move-result v3

    goto :goto_0

    .line 82
    .restart local v2    # "groupByTitle":Z
    .restart local v3    # "isArchiveShowing":Z
    .restart local v4    # "numColumns":I
    :cond_3
    invoke-direct {p0, v7, v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->subTransformByDate(Ljava/util/List;I)Ljava/util/List;

    move-result-object v6

    .restart local v6    # "totalRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    goto :goto_1

    .line 94
    .restart local v5    # "toggleButton":Lcom/google/android/libraries/bind/data/Data;
    :cond_4
    invoke-direct {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesGroupingFilter;->subTransformByDate(Ljava/util/List;I)Ljava/util/List;

    move-result-object v1

    .restart local v1    # "archivedRowList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    goto :goto_2
.end method
