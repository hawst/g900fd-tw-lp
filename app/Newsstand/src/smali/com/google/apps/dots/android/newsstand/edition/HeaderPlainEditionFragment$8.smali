.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;
.super Ljava/lang/Object;
.source "HeaderPlainEditionFragment.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->onPulledToRefresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 396
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 391
    return-void
.end method
