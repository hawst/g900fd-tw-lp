.class public abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;
.source "ExpanderView.java"


# static fields
.field private static final CHILD_LAYOUT_PARAMS:Landroid/view/ViewGroup$LayoutParams;

.field private static final FAR_SWIPE_DISTANCE:F = 1.5f

.field private static final NEAR_SWIPE_DISTANCE:F = 1.0f

.field private static final UPDATE_EXISTENCE_DELAY:I = 0xfa


# instance fields
.field private checkedExistsInitially:Z

.field private exists:Z

.field private isLoadComplete:Z

.field private final updateExistenceRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 17
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->CHILD_LAYOUT_PARAMS:Landroid/view/ViewGroup$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 24
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->updateExistenceRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->exists:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->exists:Z

    return p1
.end method

.method static synthetic access$100()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->CHILD_LAYOUT_PARAMS:Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->checkedExistsInitially:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->checkedExistsInitially:Z

    return p1
.end method


# virtual methods
.method public bridge synthetic applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->applyLayoutToViewCoordsTransform(Landroid/graphics/Matrix;)V

    return-void
.end method

.method protected abstract createChildView()Landroid/view/View;
.end method

.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->isLoadComplete:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->checkedExistsInitially:Z

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->isLoadComplete:Z

    .line 86
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->isLoadComplete:Z

    return v0

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic notify(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->notify(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onDetachedFromWindow()V

    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->updateExistenceRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 93
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 62
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->exists:Z

    if-eqz v2, :cond_0

    .line 64
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 65
    .local v0, "child":Landroid/view/View;
    sub-int v2, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 66
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->getContentArea()Landroid/graphics/RectF;

    move-result-object v1

    .line 68
    .local v1, "contentArea":Landroid/graphics/RectF;
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;

    .end local v0    # "child":Landroid/view/View;
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;->setContentArea(FFFF)V

    .line 72
    .end local v1    # "contentArea":Landroid/graphics/RectF;
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->updateExistenceRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->checkedExistsInitially:Z

    if-eqz v2, :cond_1

    const-wide/16 v2, 0xfa

    :goto_0
    const/4 v5, 0x2

    invoke-virtual {v4, v2, v3, v5}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 74
    return-void

    .line 72
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onLoadComplete()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onLoadComplete()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->measureChildren(II)V

    .line 56
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 57
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 56
    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->setMeasuredDimension(II)V

    .line 58
    return-void
.end method

.method public bridge synthetic onPageChanged()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onPageChanged()V

    return-void
.end method

.method public bridge synthetic onScrolled()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onScrolled()V

    return-void
.end method

.method public onTransformChanged()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onTransformChanged()V

    .line 79
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->updateExistenceRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ExpanderView;->checkedExistsInitially:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0xfa

    :goto_0
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 81
    return-void

    .line 79
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onUnhandledClick()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onUnhandledClick()V

    return-void
.end method

.method public bridge synthetic onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onUnhandledFling(Lcom/google/apps/dots/android/newsstand/util/MotionHelper$FlingDirection;)V

    return-void
.end method

.method public bridge synthetic onZoomAttempt()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->onZoomAttempt()V

    return-void
.end method

.method public bridge synthetic setContentArea(FFFF)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->setContentArea(FFFF)V

    return-void
.end method

.method public bridge synthetic setNBContext(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetViewGroup;->setNBContext(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    return-void
.end method
