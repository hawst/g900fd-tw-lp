.class Lcom/google/apps/dots/android/newsstand/store/NSStore$7;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "NSStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

.field final synthetic val$downloadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Ljava/util/concurrent/Executor;JLcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "priority"    # J

    .prologue
    .line 504
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->val$downloadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;J)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1300(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/http/NSClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->val$downloadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
