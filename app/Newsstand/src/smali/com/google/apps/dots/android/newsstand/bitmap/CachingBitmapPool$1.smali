.class Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;
.super Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
.source "CachingBitmapPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;-><init>(Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache",
        "<",
        "Ljava/lang/Object;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .param p2, "maxSize"    # I

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected entryRemoved(ZLjava/lang/Object;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "oldValue"    # Landroid/graphics/Bitmap;
    .param p4, "newValue"    # Landroid/graphics/Bitmap;

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v0, p3}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p3, Landroid/graphics/Bitmap;

    check-cast p4, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;->entryRemoved(ZLjava/lang/Object;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected sizeOf(Ljava/lang/Object;Landroid/graphics/Bitmap;)I
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;->this$0:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    # invokes: Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getBitmapSizeKb(Landroid/graphics/Bitmap;)I
    invoke-static {v1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->access$000(Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;Landroid/graphics/Bitmap;)I

    move-result v0

    .line 67
    .local v0, "size":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-gt v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    div-int/lit8 v0, v0, 0x5

    .line 70
    :cond_0
    return v0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 61
    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool$1;->sizeOf(Ljava/lang/Object;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method
