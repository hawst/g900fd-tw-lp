.class public Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
.super Ljava/lang/Object;
.source "AudioStatusHelper.java"


# instance fields
.field private final audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

.field private final post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field private final webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 1
    .param p1, "webView"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 33
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->onAudioStateChanged(Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V

    return-void
.end method

.method private onAudioStateChanged(Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V
    .locals 9
    .param p1, "currentItem"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "currentStatus"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 59
    const/4 v1, 0x0

    .line 61
    .local v1, "jsonAudioItem":Lorg/codehaus/jackson/node/ObjectNode;
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-static {v3, p1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findValueFromMediaItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 63
    .local v0, "audioValue":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->toJson(Ljava/lang/String;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    .line 65
    .end local v0    # "audioValue":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_0
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "dots.smart.updateCurrentAudio(%s, %d);"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    if-nez v1, :cond_1

    const-string v1, "undefined"

    .end local v1    # "jsonAudioItem":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_1
    aput-object v1, v5, v7

    .line 67
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    .line 65
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "updateAudioScript":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-array v4, v8, [Ljava/lang/String;

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 69
    return-void
.end method


# virtual methods
.method public register(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->register(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->unregister(Landroid/content/Context;)V

    .line 77
    return-void
.end method
