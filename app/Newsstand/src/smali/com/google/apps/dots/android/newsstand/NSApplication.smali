.class public Lcom/google/apps/dots/android/newsstand/NSApplication;
.super Landroid/app/Application;
.source "NSApplication.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static mainThreadHandler:Landroid/os/Handler;

.field private static volatile strictModeEnabled:Z

.field private static visibilityRunnable:Ljava/lang/Runnable;

.field private static visibleActivitiesCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/NSApplication;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibleActivitiesCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 44
    new-instance v0, Lcom/google/apps/dots/android/newsstand/NSApplication$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibilityRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibleActivitiesCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method public static crash(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 216
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/NSApplication$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/NSApplication$4;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    return-void
.end method

.method public static enableStrictMode()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->strictModeEnabled:Z

    .line 134
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    .line 135
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 134
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 140
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    .line 141
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    .line 140
    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 144
    return-void
.end method

.method private enableStrictModeIfNeeded()V
    .locals 7

    .prologue
    .line 148
    :try_start_0
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v0, ""

    .line 150
    .local v0, "manufacter":Ljava/lang/String;
    :goto_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isRunningInFeedbackProcess(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$bool;->enable_strict_mode_and_checker:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "samsung"

    .line 154
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "motorola"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v3, "4.4.3"

    .line 156
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v3, "4.4.4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->enableStrictMode()V

    .line 158
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 163
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/NSApplication$2;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/NSApplication$2;-><init>(Lcom/google/apps/dots/android/newsstand/NSApplication;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 176
    .end local v0    # "manufacter":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 148
    :cond_1
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 149
    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v1

    .line 174
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/NSApplication;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Exception while trying to enable strictmode: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 89
    .local v0, "mgr":Landroid/app/ActivityManager;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 90
    .local v1, "myPid":I
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 91
    .local v2, "p":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v4, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, v1, :cond_0

    .line 92
    iget-object v3, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 95
    .end local v2    # "p":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_0
    return-object v3

    :cond_1
    const-string v3, ""

    goto :goto_0
.end method

.method public static isCrashReporterRunningInForeground(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const-string v4, "activity"

    .line 100
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 101
    .local v0, "actvityManager":Landroid/app/ActivityManager;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "currentProcessName":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ":feedback"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "feedbackProcessName":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 105
    .local v3, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget v5, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_0

    .line 107
    const/4 v4, 0x1

    .line 110
    .end local v3    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isRunningInFeedbackProcess(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":feedback"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isStrictModeEnabled()Z
    .locals 1

    .prologue
    .line 128
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->strictModeEnabled:Z

    return v0
.end method

.method public static isVisible()Z
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibleActivitiesCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static killAndRestartIfInForeground()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 230
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 231
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 232
    .local v1, "context":Landroid/content/Context;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;

    invoke-direct {v4, v1}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/StartIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    .line 233
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    .line 234
    invoke-static {v1, v5, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 235
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 236
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x64

    add-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 238
    :cond_0
    invoke-static {v5}, Ljava/lang/System;->exit(I)V

    .line 239
    return-void
.end method

.method public static postOnMainThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 183
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 186
    :cond_0
    return-void
.end method

.method public static setVisible(Z)V
    .locals 4
    .param p0, "visible"    # Z

    .prologue
    const/4 v2, 0x1

    .line 114
    sget-object v3, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibleActivitiesCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz p0, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v0

    .line 115
    .local v0, "counter":I
    if-ltz v0, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 116
    if-nez v0, :cond_3

    .line 117
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibilityRunnable:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    .line 121
    :cond_0
    :goto_2
    return-void

    .line 114
    .end local v0    # "counter":I
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 115
    .restart local v0    # "counter":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 118
    :cond_3
    if-ne v0, v2, :cond_0

    if-eqz p0, :cond_0

    .line 119
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSApplication;->visibilityRunnable:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method private setupBind()V
    .locals 3

    .prologue
    .line 260
    invoke-static {p0}, Lcom/google/android/libraries/bind/util/Util;->init(Landroid/content/Context;)V

    .line 263
    const/16 v0, 0x2f

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->action_message:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->archive_toggle:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->article_tail_entity_item:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->bind__card_empty:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->bind__grid_group_row:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_continue_reading:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_icon:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_header_spacer:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_article_item:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_item:I

    aput v2, v0, v1

    const/16 v1, 0xa

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_pages_item:I

    aput v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_purchaseable_item:I

    aput v2, v0, v1

    const/16 v1, 0xc

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item:I

    aput v2, v0, v1

    const/16 v1, 0xd

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_compact:I

    aput v2, v0, v1

    const/16 v1, 0xe

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image:I

    aput v2, v0, v1

    const/16 v1, 0xf

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image_compact:I

    aput v2, v0, v1

    const/16 v1, 0x10

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image:I

    aput v2, v0, v1

    const/16 v1, 0x11

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image_compact:I

    aput v2, v0, v1

    const/16 v1, 0x12

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    aput v2, v0, v1

    const/16 v1, 0x13

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_horizontal:I

    aput v2, v0, v1

    const/16 v1, 0x14

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    aput v2, v0, v1

    const/16 v1, 0x15

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_horizontal:I

    aput v2, v0, v1

    const/16 v1, 0x16

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_refresh_button:I

    aput v2, v0, v1

    const/16 v1, 0x17

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_source_list_item:I

    aput v2, v0, v1

    const/16 v1, 0x18

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine:I

    aput v2, v0, v1

    const/16 v1, 0x19

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine_compact:I

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item:I

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_compact:I

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_horizontal:I

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item:I

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_list_item_compact:I

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    aput v2, v0, v1

    const/16 v1, 0x20

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    aput v2, v0, v1

    const/16 v1, 0x21

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_one_col:I

    aput v2, v0, v1

    const/16 v1, 0x22

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->empty_bg_and_card_warm_welcome_two_col:I

    aput v2, v0, v1

    const/16 v1, 0x23

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_topics_list_item:I

    aput v2, v0, v1

    const/16 v1, 0x24

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_topics_special_item:I

    aput v2, v0, v1

    const/16 v1, 0x25

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_big_card_layout:I

    aput v2, v0, v1

    const/16 v1, 0x26

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_one_two_layout:I

    aput v2, v0, v1

    const/16 v1, 0x27

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_two_one_layout:I

    aput v2, v0, v1

    const/16 v1, 0x28

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_normal_wide_layout:I

    aput v2, v0, v1

    const/16 v1, 0x29

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_single_card_row_layout:I

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_three_cards_row_layout:I

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_cards_row_layout:I

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_column_single_card_layout:I

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_wide_normal_layout:I

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_detailed_header:I

    aput v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->init([I)V

    .line 312
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSApplication;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Starting up..."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/NSApplication;->mainThreadHandler:Landroid/os/Handler;

    .line 61
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->enableStrictModeIfNeeded()V

    .line 64
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setup(Landroid/content/Context;)V

    .line 66
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->setupBind()V

    .line 68
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isRunningInFeedbackProcess(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    sget-object v1, Lcom/google/apps/dots/android/newsstand/NSApplication;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Running in feedback process, skip initialization"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 76
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_1

    .line 77
    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->initMyMagazinesObserver(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 80
    :cond_1
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    .line 244
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimCaches(F)V

    .line 245
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 250
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 251
    const/16 v0, 0x14

    if-lt p1, v0, :cond_1

    .line 252
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimCaches(F)V

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    .line 254
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->trimCaches(F)V

    goto :goto_0
.end method
