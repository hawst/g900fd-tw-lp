.class Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;
.super Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.source "RecentlyReadList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->recentlyReadHelper:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->access$100(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;)Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList$2;->this$0:Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->account:Landroid/accounts/Account;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->access$000(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;)Landroid/accounts/Account;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->loadRecentlyRead(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;

    move-result-object v4

    .line 69
    .local v4, "recentlyRead":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
    iget-object v5, v4, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    array-length v5, v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 70
    .local v3, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v6, v4, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v2, v6, v5

    .line 71
    .local v2, "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 72
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v8

    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 73
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->DK_EDITION:I

    invoke-virtual {v0, v8, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 74
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->hasLastRead()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 75
    sget v8, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;->DK_LAST_READ:I

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getLastRead()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 77
    :cond_0
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .end local v2    # "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    :cond_1
    return-object v3
.end method
