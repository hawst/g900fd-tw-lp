.class public final enum Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
.super Ljava/lang/Enum;
.source "DelayedContentWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoadState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field public static final enum LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field public static final enum LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field public static final enum LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field public static final enum NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    const-string v1, "NOT_LOADED"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    const-string v1, "LOAD_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 12
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->$VALUES:[Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    return-object v0
.end method


# virtual methods
.method public canFail()Z
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADING:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadedOrFailed()Z
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotLoadedOrFailed()Z
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
