.class public Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;
.super Ljava/lang/Object;
.source "NSViewStates.java"


# static fields
.field public static final STATE_READ:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$attr;->state_read:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dispatchSetIsRead(Landroid/view/ViewGroup;Z)V
    .locals 4
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p1, "isRead"    # Z

    .prologue
    .line 15
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 16
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 17
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 18
    .local v0, "child":Landroid/view/View;
    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;

    if-eqz v3, :cond_1

    .line 19
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;->setIsRead(Z)V

    .line 16
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 20
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 21
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    goto :goto_1

    .line 24
    :cond_2
    return-void
.end method
