.class public final Lcom/google/apps/dots/proto/client/DotsShared$Post;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Post"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Post;


# instance fields
.field public appId:Ljava/lang/String;

.field private articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

.field private bitField0_:I

.field public clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

.field private created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

.field private externalId_:Ljava/lang/String;

.field public item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

.field private languageCode_:Ljava/lang/String;

.field public postId:Ljava/lang/String;

.field private postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

.field public primaryFeatureIds:[I

.field private sectionId_:Ljava/lang/String;

.field private summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field private textDirection_:I

.field private translationCode_:Ljava/lang/String;

.field private untranslatedAppId_:Ljava/lang/String;

.field private untranslatedPostId_:Ljava/lang/String;

.field private untranslatedSectionId_:Ljava/lang/String;

.field private updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16382
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Post;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Post;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16383
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 16388
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    .line 16391
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    .line 16413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    .line 16416
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    .line 16438
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    .line 16498
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    .line 16520
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    .line 16542
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 16583
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    .line 16586
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    .line 16608
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    .line 16630
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    .line 16668
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 16383
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .locals 5

    .prologue
    .line 16698
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16702
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_0

    .line 16703
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16705
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v3, :cond_1

    .line 16706
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16708
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 16709
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Item;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 16710
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 16711
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v3, v3, v2

    if-eqz v3, :cond_2

    .line 16712
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v4

    aput-object v4, v3, v2

    .line 16710
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 16699
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 16700
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 16716
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v3, :cond_4

    .line 16717
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 16719
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v3, :cond_5

    .line 16720
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 16722
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v3, v3

    if-lez v3, :cond_6

    .line 16723
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-virtual {v3}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    .line 16725
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-eqz v3, :cond_7

    .line 16726
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    .line 16728
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 16729
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 16730
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-ge v2, v3, :cond_9

    .line 16731
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v3, v3, v2

    if-eqz v3, :cond_8

    .line 16732
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    aput-object v4, v3, v2

    .line 16730
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 16736
    .end local v2    # "i":I
    :cond_9
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16379
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16741
    if-ne p1, p0, :cond_1

    .line 16761
    :cond_0
    :goto_0
    return v1

    .line 16742
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 16743
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 16744
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16745
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16746
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16747
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16748
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 16749
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v3, :cond_3

    .line 16750
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16751
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16752
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 16753
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_3

    .line 16754
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v3, :cond_3

    .line 16755
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    .line 16756
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16757
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 16758
    :goto_d
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-nez v3, :cond_3

    .line 16760
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 16761
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    .line 16744
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    .line 16745
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    .line 16746
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    .line 16747
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    .line 16748
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16749
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16750
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    .line 16751
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    .line 16752
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    .line 16753
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 16754
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 16755
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    .line 16756
    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    .line 16757
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    .line 16758
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    .line 16760
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e
.end method

.method public getExternalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16440
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPostTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 1

    .prologue
    .line 16547
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-object v0
.end method

.method public getSectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16500
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 16869
    const/4 v2, 0x0

    .line 16870
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    .line 16871
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16872
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    .line 16873
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16874
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_0

    .line 16875
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    .line 16876
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16878
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_1

    .line 16879
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16880
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16882
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v4, :cond_2

    .line 16883
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16884
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16886
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_3

    .line 16887
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    .line 16888
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16890
    :cond_3
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-eqz v4, :cond_5

    .line 16891
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v1, v5, v4

    .line 16892
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-eqz v1, :cond_4

    .line 16893
    const/16 v7, 0x8

    .line 16894
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 16891
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 16898
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v4, :cond_6

    .line 16899
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 16900
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16902
    :cond_6
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v4, :cond_7

    .line 16903
    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 16904
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16906
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_8

    .line 16907
    const/16 v4, 0x16

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    .line 16908
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16910
    :cond_8
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_9

    .line 16911
    const/16 v4, 0x17

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    .line 16912
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16914
    :cond_9
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-eqz v4, :cond_a

    .line 16915
    const/16 v4, 0x1b

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    .line 16916
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 16918
    :cond_a
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v4, :cond_c

    .line 16919
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_c

    aget-object v1, v5, v4

    .line 16920
    .local v1, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v1, :cond_b

    .line 16921
    const/16 v7, 0x1c

    .line 16922
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 16919
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 16926
    .end local v1    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_c
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v4, v4

    if-lez v4, :cond_e

    .line 16927
    const/4 v0, 0x0

    .line 16928
    .local v0, "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_d

    aget v1, v4, v3

    .line 16930
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 16928
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 16932
    .end local v1    # "element":I
    :cond_d
    add-int/2addr v2, v0

    .line 16933
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 16935
    .end local v0    # "dataSize":I
    :cond_e
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_f

    .line 16936
    const/16 v3, 0x29

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    .line 16937
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 16939
    :cond_f
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_10

    .line 16940
    const/16 v3, 0x2a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    .line 16941
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 16943
    :cond_10
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_11

    .line 16944
    const/16 v3, 0x2b

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    .line 16945
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 16947
    :cond_11
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_12

    .line 16948
    const/16 v3, 0x2c

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    .line 16949
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 16951
    :cond_12
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->cachedSize:I

    .line 16952
    return v2
.end method

.method public getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 1

    .prologue
    .line 16566
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object v0
.end method

.method public getTextDirection()I
    .locals 1

    .prologue
    .line 16632
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    return v0
.end method

.method public getTranslationCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16610
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdated()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;
    .locals 1

    .prologue
    .line 16481
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    return-object v0
.end method

.method public hasPostTemplate()Z
    .locals 1

    .prologue
    .line 16557
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSummary()Z
    .locals 1

    .prologue
    .line 16576
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTextDirection()Z
    .locals 1

    .prologue
    .line 16640
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 16765
    const/16 v1, 0x11

    .line 16766
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 16767
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 16768
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 16769
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 16770
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 16771
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 16772
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_8

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 16773
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v2, :cond_9

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 16774
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 16775
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 16776
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-nez v2, :cond_c

    mul-int/lit8 v1, v1, 0x1f

    .line 16782
    :cond_0
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v2, :cond_e

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 16783
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v2, :cond_f

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 16784
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    if-nez v2, :cond_10

    mul-int/lit8 v1, v1, 0x1f

    .line 16790
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_11

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    .line 16791
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_c
    add-int v1, v4, v2

    .line 16792
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    add-int v1, v2, v4

    .line 16793
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-nez v2, :cond_13

    move v2, v3

    :goto_d
    add-int v1, v4, v2

    .line 16794
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v2, :cond_14

    mul-int/lit8 v1, v1, 0x1f

    .line 16800
    :cond_2
    return v1

    .line 16767
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 16768
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 16769
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 16770
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 16771
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 16772
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 16773
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 16774
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 16775
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 16778
    :cond_c
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 16779
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v2, v2, v0

    if-nez v2, :cond_d

    move v2, v3

    :goto_f
    add-int v1, v4, v2

    .line 16778
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 16779
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->hashCode()I

    move-result v2

    goto :goto_f

    .line 16782
    .end local v0    # "i":I
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 16783
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 16786
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 16787
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    aget v4, v4, v0

    add-int v1, v2, v4

    .line 16786
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 16790
    .end local v0    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 16791
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 16793
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 16796
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 16797
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    if-nez v2, :cond_15

    move v2, v3

    :goto_12
    add-int v1, v4, v2

    .line 16796
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 16797
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v2

    goto :goto_12
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 16960
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 16961
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 16965
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 16966
    :sswitch_0
    return-object p0

    .line 16971
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    goto :goto_0

    .line 16975
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    goto :goto_0

    .line 16979
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    .line 16980
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto :goto_0

    .line 16984
    :sswitch_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v6, :cond_1

    .line 16985
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16987
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 16991
    :sswitch_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-nez v6, :cond_2

    .line 16992
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    .line 16994
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 16998
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    .line 16999
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto :goto_0

    .line 17003
    :sswitch_7
    const/16 v6, 0x42

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 17004
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-nez v6, :cond_4

    move v1, v5

    .line 17005
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 17006
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-eqz v6, :cond_3

    .line 17007
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17009
    :cond_3
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 17010
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 17011
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item;-><init>()V

    aput-object v7, v6, v1

    .line 17012
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 17013
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 17010
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 17004
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v1, v6

    goto :goto_1

    .line 17016
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item;-><init>()V

    aput-object v7, v6, v1

    .line 17017
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 17021
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :sswitch_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v6, :cond_6

    .line 17022
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 17024
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 17028
    :sswitch_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v6, :cond_7

    .line 17029
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 17031
    :cond_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 17035
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    .line 17036
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 17040
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    .line 17041
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 17045
    :sswitch_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-nez v6, :cond_8

    .line 17046
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    .line 17048
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 17052
    :sswitch_d
    const/16 v6, 0xe2

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 17053
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v6, :cond_a

    move v1, v5

    .line 17054
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 17055
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v6, :cond_9

    .line 17056
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17058
    :cond_9
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 17059
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_b

    .line 17060
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v7, v6, v1

    .line 17061
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 17062
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 17059
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 17053
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v1, v6

    goto :goto_3

    .line 17065
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v7, v6, v1

    .line 17066
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 17070
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :sswitch_e
    const/16 v6, 0x120

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 17071
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v1, v6

    .line 17072
    .restart local v1    # "i":I
    add-int v6, v1, v0

    new-array v2, v6, [I

    .line 17073
    .local v2, "newArray":[I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17074
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    .line 17075
    :goto_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 17076
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    .line 17077
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 17075
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 17080
    :cond_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v7

    aput v7, v6, v1

    goto/16 :goto_0

    .line 17084
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 17085
    .local v4, "temp":I
    if-eqz v4, :cond_d

    const/4 v6, 0x1

    if-eq v4, v6, :cond_d

    const/4 v6, 0x2

    if-ne v4, v6, :cond_e

    .line 17088
    :cond_d
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    .line 17089
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 17091
    :cond_e
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    goto/16 :goto_0

    .line 17096
    .end local v4    # "temp":I
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    .line 17097
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 17101
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    .line 17102
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 17106
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    .line 17107
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    goto/16 :goto_0

    .line 16961
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x62 -> :sswitch_9
        0xb2 -> :sswitch_a
        0xba -> :sswitch_b
        0xda -> :sswitch_c
        0xe2 -> :sswitch_d
        0x120 -> :sswitch_e
        0x148 -> :sswitch_f
        0x152 -> :sswitch_10
        0x15a -> :sswitch_11
        0x162 -> :sswitch_12
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16379
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 16805
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16806
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16807
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_0

    .line 16808
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->externalId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16810
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_1

    .line 16811
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->created_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16813
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    if-eqz v2, :cond_2

    .line 16814
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->updated_:Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16816
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 16817
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->sectionId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16819
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    if-eqz v2, :cond_5

    .line 16820
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_5

    aget-object v0, v3, v2

    .line 16821
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    if-eqz v0, :cond_4

    .line 16822
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16820
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 16826
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_6

    .line 16827
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postTemplate_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16829
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v2, :cond_7

    .line 16830
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->summary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16832
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_8

    .line 16833
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16835
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_9

    .line 16836
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16838
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    if-eqz v2, :cond_a

    .line 16839
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->articleFeatures_:Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16841
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_c

    .line 16842
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_c

    aget-object v0, v3, v2

    .line 16843
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_b

    .line 16844
    const/16 v5, 0x1c

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 16842
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 16848
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    if-eqz v2, :cond_d

    .line 16849
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->primaryFeatureIds:[I

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_d

    aget v0, v2, v1

    .line 16850
    .local v0, "element":I
    const/16 v4, 0x24

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 16849
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 16853
    .end local v0    # "element":I
    :cond_d
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_e

    .line 16854
    const/16 v1, 0x29

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->textDirection_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 16856
    :cond_e
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_f

    .line 16857
    const/16 v1, 0x2a

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedPostId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16859
    :cond_f
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_10

    .line 16860
    const/16 v1, 0x2b

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16862
    :cond_10
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_11

    .line 16863
    const/16 v1, 0x2c

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->untranslatedSectionId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 16865
    :cond_11
    return-void
.end method
