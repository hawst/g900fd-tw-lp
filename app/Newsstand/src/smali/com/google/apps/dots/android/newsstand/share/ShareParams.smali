.class public abstract Lcom/google/apps/dots/android/newsstand/share/ShareParams;
.super Ljava/lang/Object;
.source "ShareParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;,
        Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;,
        Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;,
        Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;
    }
.end annotation


# instance fields
.field public dialogTitle:Ljava/lang/String;

.field public longUrl:Ljava/lang/String;

.field public shortUrl:Ljava/lang/String;

.field public type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;
    .param p2, "dialogTitle"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    .line 30
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->dialogTitle:Ljava/lang/String;

    .line 31
    return-void
.end method

.method private equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Z
    .locals 2
    .param p1, "other"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .prologue
    .line 39
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->dialogTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->dialogTitle:Ljava/lang/String;

    .line 40
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->shortUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->shortUrl:Ljava/lang/String;

    .line 41
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->longUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->longUrl:Ljava/lang/String;

    .line 42
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static forEdition(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->share_edition_dialog_title:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V

    return-object v0
.end method

.method public static forLink(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->share_article_dialog_title:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V

    return-object v0
.end method

.method public static forPost(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->share_article_dialog_title:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 35
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
