.class public Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;
.super Ljava/lang/Object;
.source "Upgrade.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final context:Landroid/content/Context;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->create(Z)Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    .line 46
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCaches()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Ljava/io/File;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->moveDiskCacheDir(Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldMagazinesBlobStores()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldMagazinesDatabases()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteSyncedAssets()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    return-object v0
.end method

.method private clearMagazinesData()V
    .locals 2

    .prologue
    .line 189
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$5;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 198
    return-void
.end method

.method private deleteOldMagazinesBlobStores()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 201
    sget-object v2, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Deleting old blob stores"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/io/File;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    const/4 v4, 0x0

    .line 203
    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    .line 204
    invoke-virtual {v4}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    .line 205
    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    .line 206
    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    aput-object v4, v2, v3

    .line 202
    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 207
    .local v1, "potentialBlobStoreLocations":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 208
    .local v0, "dir":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 209
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->deleteOldMagazinesBlobStoresFrom(Ljava/io/File;)V

    goto :goto_0

    .line 212
    .end local v0    # "dir":Ljava/io/File;
    :cond_1
    return-void
.end method

.method private deleteOldMagazinesBlobStoresFrom(Ljava/io/File;)V
    .locals 8
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 215
    if-eqz p1, :cond_0

    .line 216
    new-instance v2, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$6;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    invoke-virtual {p1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 222
    .local v1, "databaseFiles":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 223
    array-length v4, v1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v1, v2

    .line 224
    .local v0, "blobDir":Ljava/io/File;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Deleting old blob dir: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 225
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    .line 223
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 229
    .end local v0    # "blobDir":Ljava/io/File;
    .end local v1    # "databaseFiles":[Ljava/io/File;
    :cond_0
    return-void
.end method

.method private deleteOldMagazinesDatabases()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 232
    sget-object v3, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Deleting old databases"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    const-string v5, "blah"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 235
    .local v0, "databaseDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 236
    new-instance v3, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$7;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$7;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    invoke-virtual {v0, v3}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 242
    .local v1, "databaseFiles":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 243
    array-length v5, v1

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v2, v1, v3

    .line 244
    .local v2, "file":Ljava/io/File;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Deleting db file: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v2, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ii(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 243
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 249
    .end local v1    # "databaseFiles":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private deleteOldNewsstandDiskCacheDirs(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .param p1, "outerDir"    # Ljava/io/File;
    .param p2, "currentStoreRootName"    # Ljava/lang/String;

    .prologue
    .line 111
    if-eqz p1, :cond_0

    .line 112
    new-instance v0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;

    invoke-direct {v0, p0, p2}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$3;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    .line 125
    :cond_0
    return-void
.end method

.method private deleteOldNewsstandDiskCaches()V
    .locals 3

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v2, "externalStorageDir"

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 132
    .local v0, "external":Z
    :goto_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$4;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;Z)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 146
    return-void

    .line 131
    .end local v0    # "external":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deleteSyncedAssets()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    const-string v2, "synced"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 253
    .local v0, "syncedAssetsDir":Ljava/io/File;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Deleting synced assets dir: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    .line 255
    return-void
.end method

.method private handleMagazinesPreferences()V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 149
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->context:Landroid/content/Context;

    const-string v9, "CurrentsPreferences"

    invoke-virtual {v8, v9, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 151
    .local v4, "magazinesPrefs":Landroid/content/SharedPreferences;
    sget-object v8, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "Copying Magazines preferences"

    new-array v10, v6, [Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    const-string v8, "account"

    invoke-interface {v4, v8, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "accountName":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 155
    .local v2, "magazinesAccount":Landroid/accounts/Account;
    :goto_0
    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 156
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v8, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAccount(Landroid/accounts/Account;)V

    .line 158
    const-string v8, "true"

    const-string v9, "hasAppLaunched"

    .line 159
    invoke-static {v2, v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getUserKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 158
    invoke-interface {v4, v9, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 160
    .local v3, "magazinesLaunched":Z
    if-eqz v3, :cond_0

    .line 161
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v9, "wasMagazinesUser"

    invoke-virtual {v8, v9, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 167
    :cond_0
    const-string v8, "DOWNLOAD_CONTENT_ALWAYS"

    const-string v9, "contentDownloadMode"

    .line 168
    invoke-static {v2, v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getUserKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 167
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 170
    .local v1, "downloadContentAlways":Z
    if-eqz v1, :cond_2

    .line 171
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    if-nez v1, :cond_1

    const/4 v6, 0x1

    :cond_1
    invoke-virtual {v8, v6}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setDownloadViaWifiOnlyPreference(Z)V

    .line 177
    :cond_2
    const-string v6, "NOTIFICATIONS_ENABLED"

    const-string v8, "notificationMode"

    .line 178
    invoke-static {v2, v8}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getUserKey(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 177
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 180
    .local v5, "notificationsEnabled":Z
    if-eqz v5, :cond_3

    .line 181
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_ENABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setNotificationMode(Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;)V

    .line 185
    .end local v1    # "downloadContentAlways":Z
    .end local v3    # "magazinesLaunched":Z
    .end local v5    # "notificationsEnabled":Z
    :cond_3
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->deleteSharedPrefs(Landroid/content/SharedPreferences;)V

    .line 186
    return-void

    .end local v2    # "magazinesAccount":Landroid/accounts/Account;
    :cond_4
    move-object v2, v7

    .line 154
    goto :goto_0
.end method

.method private moveDiskCacheDir(Ljava/io/File;Ljava/io/File;)V
    .locals 12
    .param p1, "cacheDir"    # Ljava/io/File;
    .param p2, "permDir"    # Ljava/io/File;

    .prologue
    const/4 v9, 0x2

    const/4 v11, 0x0

    .line 88
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    move-object v4, p1

    .line 92
    .local v4, "oldDir":Ljava/io/File;
    move-object v1, p2

    .line 94
    .local v1, "newDir":Ljava/io/File;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->storeDirName(I)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "oldStoreDirName":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheManager;->storeDirName()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "newStoreDirName":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 98
    .local v6, "oldStoreRoot":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 99
    .local v3, "newStoreRoot":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 100
    sget-object v7, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "Moving DiskCache from %s to %s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v11

    const/4 v10, 0x1

    aput-object v3, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :try_start_0
    invoke-static {v6, v3}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->moveDir(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "ioe":Ljava/io/IOException;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "    trouble upgrading DiskCache. Failing."

    new-array v9, v11, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Couldn\'t upgrade DiskCache"

    invoke-direct {v7, v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
.end method

.method private upgradeDiskCacheIfNeeded()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$2;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 85
    return-void
.end method


# virtual methods
.method public runUpgradeFlow()V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->upgradeDiskCacheIfNeeded()V

    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "hasClearedMagazinesData"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->handleMagazinesPreferences()V

    .line 55
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->clearMagazinesData()V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade$1;-><init>(Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 65
    return-void
.end method

.method public whenUpgradeFinished()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/upgrade/Upgrade;->whenUpgradeFinished:Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    return-object v0
.end method
