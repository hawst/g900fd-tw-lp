.class public Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
.super Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
.source "ShareParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/share/ShareParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForPost"
.end annotation


# instance fields
.field public postTitle:Ljava/lang/String;

.field public snippet:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "dialogTitle"    # Ljava/lang/String;

    .prologue
    .line 120
    sget-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;->POST:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;-><init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V

    .line 121
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;)Z
    .locals 2
    .param p1, "other"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    .prologue
    .line 163
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->snippet:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->snippet:Ljava/lang/String;

    .line 164
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    return-object v0
.end method

.method public setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 0
    .param p1, "editionDescription"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .line 144
    return-object p0
.end method

.method public bridge synthetic setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    return-object v0
.end method

.method public setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 0
    .param p1, "editionName"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .line 138
    return-object p0
.end method

.method public bridge synthetic setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    return-object v0
.end method

.method public setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .line 132
    return-object p0
.end method

.method public setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 0
    .param p1, "postTitle"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    .line 149
    return-object p0
.end method

.method public bridge synthetic setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    move-result-object v0

    return-object v0
.end method

.method public setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .line 126
    return-object p0
.end method

.method public setSnippet(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .locals 1
    .param p1, "snippet"    # Ljava/lang/String;

    .prologue
    .line 153
    const/16 v0, 0x96

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->ellipsis(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->snippet:Ljava/lang/String;

    .line 154
    return-object p0
.end method
