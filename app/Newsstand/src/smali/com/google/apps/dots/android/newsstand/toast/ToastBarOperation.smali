.class public abstract Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
.super Ljava/lang/Object;
.source "ToastBarOperation.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$ActionClickedListener;


# instance fields
.field private account:Landroid/accounts/Account;

.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private operationLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "operationLabel"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 28
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->operationLabel:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->account:Landroid/accounts/Account;

    .line 30
    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->account:Landroid/accounts/Account;

    return-object v0
.end method

.method public getActionIconResourceId()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    return-object v0
.end method

.method public getOperationLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->operationLabel:Ljava/lang/String;

    return-object v0
.end method

.method public onActionClicked(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    return-void
.end method
