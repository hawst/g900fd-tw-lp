.class Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;
.super Ljava/lang/Object;
.source "CardSourceItemClickableMenuView.java"

# interfaces
.implements Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->onPreparePopupMenu(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 254
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Archive menu item selected"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .line 256
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 257
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    if-eqz v0, :cond_0

    .line 258
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "ArchiveUtil about to be called"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;

    .line 260
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/CardSourceItemClickableMenuView;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v2

    .line 259
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ArchiveUtil;->addEditionToArchive(Landroid/app/Activity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 262
    :cond_0
    return-void
.end method
