.class final Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardNewsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$postId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 349
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$postId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 348
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->playFirstAudioItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/content/Context;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 350
    return-void
.end method
