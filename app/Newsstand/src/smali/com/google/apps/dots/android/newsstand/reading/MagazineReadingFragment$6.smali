.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "MagazineReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 5

    .prologue
    .line 356
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .line 357
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v4

    .line 356
    invoke-static {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeSpecificErrorCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 360
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 366
    return-object v0
.end method
