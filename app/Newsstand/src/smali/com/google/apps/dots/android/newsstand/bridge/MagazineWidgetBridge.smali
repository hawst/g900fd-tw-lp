.class public Lcom/google/apps/dots/android/newsstand/bridge/MagazineWidgetBridge;
.super Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;
.source "MagazineWidgetBridge.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "context"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "articleWidget"    # Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p5, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 21
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 22
    return-void
.end method


# virtual methods
.method public switchToApp(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 28
    return-void
.end method
