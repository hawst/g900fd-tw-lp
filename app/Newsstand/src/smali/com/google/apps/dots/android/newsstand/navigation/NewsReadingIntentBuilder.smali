.class public Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "NewsReadingIntentBuilder.java"


# instance fields
.field private appId:Ljava/lang/String;

.field private originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private postId:Ljava/lang/String;

.field private postIndex:Ljava/lang/Integer;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private sectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 32
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 75
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-class v2, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 79
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postId:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    .line 82
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->appId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 83
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->appId:Ljava/lang/String;

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v2, :cond_2

    .line 86
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->appId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 88
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v2, :cond_3

    .line 89
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 92
    :cond_3
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postIndex:Ljava/lang/Integer;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Integer;)V

    .line 95
    .local v1, "state":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    const-string v2, "NewsArticlePagerFragment_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 96
    const-string v2, "addToBackStack"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    return-object v0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 103
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->appId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->prefetchNewsResources(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 105
    return-void
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->appId:Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public setOriginalEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 55
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postId:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setPostIndex(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "postIndex"    # Ljava/lang/Integer;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->postIndex:Ljava/lang/Integer;

    .line 65
    return-object p0
.end method

.method public setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 60
    return-object p0
.end method

.method public setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->sectionId:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;
    .locals 2
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->activity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->reading_activity_hero:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 70
    return-object p0
.end method
