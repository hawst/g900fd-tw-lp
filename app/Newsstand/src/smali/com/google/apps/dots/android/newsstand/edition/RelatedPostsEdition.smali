.class public Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "RelatedPostsEdition.java"


# direct methods
.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionMessage"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 24
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 27
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setType(I)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;-><init>()V

    .line 28
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->setRelatedPosts(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    .line 27
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 29
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 73
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->relatedPostsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 34
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    .line 36
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 38
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition$1;

    invoke-direct {v0, p0, p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    return-object v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getRelatedPosts()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$RelatedPostsEditionInfo;->getPostId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->getPostId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getRelatedArticles(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
