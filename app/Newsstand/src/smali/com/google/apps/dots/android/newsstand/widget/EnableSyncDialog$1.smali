.class final Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "EnableSyncDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->enableSyncInBackground(Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$userAccount:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 73
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;->val$userAccount:Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->enableGlobalSync()V

    .line 78
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;->val$userAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->enablePeriodicSync(Landroid/accounts/Account;)V

    .line 79
    return-void
.end method
