.class public abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "AnalyticsEditionBase.java"


# instance fields
.field private final librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

.field protected final originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private originalEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field protected postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 1
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 33
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 35
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 36
    return-void
.end method


# virtual methods
.method protected getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    if-nez v0, :cond_1

    .line 76
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not find edition summary for edition: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->originalEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v0
.end method

.method protected getPostSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 5
    .param p1, "postId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v1, :cond_2

    .line 57
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 56
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 59
    .local v0, "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    if-nez v0, :cond_1

    .line 60
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v3, "Could not find post for postId = "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 66
    .end local v0    # "post":Lcom/google/apps/dots/proto/client/DotsShared$Post;
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object v1
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getAppAnalyticsId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSection(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .locals 5
    .param p1, "sectionId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->BACKGROUND:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .line 86
    .local v0, "section":Lcom/google/apps/dots/proto/client/DotsShared$Section;
    if-nez v0, :cond_1

    .line 87
    new-instance v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;

    const-string v3, "Could not find section for sectionId = "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_1
    return-object v0
.end method

.method protected getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x4

    .line 44
    :goto_0
    return v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x3

    goto :goto_0

    .line 44
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected getTranslatedLanguage()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v1

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 95
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasTranslationCode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTranslationCode()Ljava/lang/String;

    move-result-object v1

    .line 98
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "None"

    goto :goto_0
.end method
