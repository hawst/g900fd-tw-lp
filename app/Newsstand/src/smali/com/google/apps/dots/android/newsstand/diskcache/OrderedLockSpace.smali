.class public Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;
.super Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;
.source "OrderedLockSpace.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace",
        "<",
        "Lcom/google/apps/dots/android/newsstand/diskcache/Interval",
        "<TS;>;>;"
    }
.end annotation


# instance fields
.field private final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace<TS;>;"
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TS;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->comparator:Ljava/util/Comparator;

    .line 15
    return-void
.end method


# virtual methods
.method protected intersects(Lcom/google/apps/dots/android/newsstand/diskcache/Interval;Lcom/google/apps/dots/android/newsstand/diskcache/Interval;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/diskcache/Interval",
            "<TS;>;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/Interval",
            "<TS;>;)Z"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace<TS;>;"
    .local p1, "a":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    .local p2, "b":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->comparator:Ljava/util/Comparator;

    invoke-virtual {p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->intersects(Lcom/google/apps/dots/android/newsstand/diskcache/Interval;Ljava/util/Comparator;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic intersects(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace<TS;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->intersects(Lcom/google/apps/dots/android/newsstand/diskcache/Interval;Lcom/google/apps/dots/android/newsstand/diskcache/Interval;)Z

    move-result v0

    return v0
.end method

.method public lockAll(Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    .locals 2
    .param p1, "exclusive"    # Z

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace<TS;>;"
    const/4 v1, 0x0

    .line 28
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;

    invoke-direct {v0, v1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->lock(Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    move-result-object v0

    return-object v0
.end method

.method public lockHalfInterval(Ljava/lang/Object;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    .locals 1
    .param p3, "exclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TS;Z)",
            "Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;, "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace<TS;>;"
    .local p1, "lb":Ljava/lang/Object;, "TS;"
    .local p2, "ub":Ljava/lang/Object;, "TS;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;

    invoke-direct {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0, p3}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->lock(Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    move-result-object v0

    return-object v0
.end method
