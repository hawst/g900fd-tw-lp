.class final Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "NewsSubscriptionsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->makeCardFilter(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Lcom/google/android/libraries/bind/data/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$editionType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 136
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;->val$editionType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 139
    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 140
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;->val$editionType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v1, v2, :cond_0

    .line 141
    const/4 v1, 0x0

    .line 145
    :goto_0
    return v1

    .line 144
    :cond_0
    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->fillInData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->access$000(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 145
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 3
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;->transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;

    move-result-object v0

    .line 151
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$2;->val$editionType:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v1, v2, :cond_0

    .line 152
    const/4 v1, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->makeRecommendedCardData()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->access$100()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 154
    :cond_0
    return-object v0
.end method
