.class public Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
.super Ljava/lang/Object;
.source "MagazineReadingState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final inLiteMode:Z

.field public final readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .param p4, "inLiteMode"    # Z

    .prologue
    .line 29
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    if-nez p3, :cond_0

    new-instance p3, Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .end local p3    # "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    invoke-direct {p3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;-><init>()V

    :cond_0
    invoke-direct {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    invoke-direct {p0, v0, p4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/reading/ReadingState;Z)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "inLiteMode"    # Z

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/ReadingState;Z)V
    .locals 0
    .param p1, "readingState"    # Lcom/google/apps/dots/android/newsstand/reading/ReadingState;
    .param p2, "inLiteMode"    # Z

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    .line 36
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    .line 37
    return-void
.end method

.method public static switchLiteMode(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .locals 3
    .param p0, "state"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .prologue
    .line 54
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/reading/ReadingState;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 59
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 61
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 64
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->pageLocation:Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    return-object v0
.end method

.method public postId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingState;->postId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 74
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "{%s%s}"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    if-eqz v0, :cond_0

    const-string v0, " Lite mode"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->readingState:Lcom/google/apps/dots/android/newsstand/reading/ReadingState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 88
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
