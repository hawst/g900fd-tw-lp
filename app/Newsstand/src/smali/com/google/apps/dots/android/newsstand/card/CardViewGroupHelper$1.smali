.class Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;
.super Ljava/lang/Object;
.source "CardViewGroupHelper.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged()V
    .locals 10

    .prologue
    .line 76
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$000(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 79
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$000(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    move-result-object v8

    instance-of v8, v8, Landroid/view/View;

    if-eqz v8, :cond_0

    .line 80
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$000(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 86
    .local v1, "cardView":Landroid/view/View;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->cardViewGroup:Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$000(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v8

    sget v9, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    invoke-virtual {v8, v9}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;

    .line 88
    .local v0, "analyticsProvider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
    if-eqz v0, :cond_1

    .line 89
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 90
    .local v3, "r":Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 93
    .local v2, "p":Landroid/graphics/Point;
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 96
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v9

    mul-int/2addr v8, v9

    int-to-double v4, v8

    .line 97
    .local v4, "rectArea":D
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v9

    mul-int/2addr v8, v9

    int-to-double v6, v8

    .line 98
    .local v6, "viewArea":D
    const-wide/high16 v8, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v8, v6

    cmpl-double v8, v4, v8

    if-ltz v8, :cond_0

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->beenOnScreenForTimeThreshold()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 99
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->track(Z)V

    .line 103
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    const/4 v9, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->suppressAnalytics:Z
    invoke-static {v8, v9}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$102(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;Z)Z

    .line 104
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->updateOnScrollChangedListener()V
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;->access$200(Lcom/google/apps/dots/android/newsstand/card/CardViewGroupHelper;)V

    .line 112
    .end local v0    # "analyticsProvider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
    .end local v1    # "cardView":Landroid/view/View;
    .end local v2    # "p":Landroid/graphics/Point;
    .end local v3    # "r":Landroid/graphics/Rect;
    .end local v4    # "rectArea":D
    .end local v6    # "viewArea":D
    :cond_0
    :goto_0
    return-void

    .line 109
    .restart local v0    # "analyticsProvider":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
    .restart local v1    # "cardView":Landroid/view/View;
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    goto :goto_0
.end method
