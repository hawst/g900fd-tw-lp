.class public Lcom/google/apps/dots/android/newsstand/icon/IconId;
.super Ljava/lang/Object;
.source "IconId.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/icon/IconSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/icon/IconSource",
        "<",
        "Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

.field public static final SEARCH_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;


# instance fields
.field public final attachmentId:Ljava/lang/String;

.field private final backgroundColorResId:I

.field public final resourceId:I

.field public final shouldUseRoundedMask:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_search_wht_24dp:I

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->SEARCH_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, v0, v0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(IZI)V

    .line 34
    return-void
.end method

.method public constructor <init>(IZI)V
    .locals 1
    .param p1, "resourceId"    # I
    .param p2, "shouldUseRoundedMask"    # Z
    .param p3, "backgroundColorResId"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    .line 46
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->shouldUseRoundedMask:Z

    .line 47
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->backgroundColorResId:I

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, v0, v0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;ZI)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;
    .param p2, "shouldUseRoundedMask"    # Z
    .param p3, "backgroundColorResId"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    .line 39
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->shouldUseRoundedMask:Z

    .line 40
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->backgroundColorResId:I

    .line 41
    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V
    .locals 2
    .param p1, "cacheableAttachmentView"    # Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getWidth()I

    move-result v0

    .line 81
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getHeight()I

    move-result v1

    .line 80
    invoke-virtual {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;II)V

    .line 82
    return-void
.end method

.method public apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;I)V
    .locals 0
    .param p1, "cacheableAttachmentView"    # Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .param p2, "iconSize"    # I

    .prologue
    .line 86
    invoke-virtual {p0, p1, p2, p2}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;II)V

    .line 87
    return-void
.end method

.method public apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;II)V
    .locals 4
    .param p1, "cacheableAttachmentView"    # Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .param p2, "iconWidth"    # I
    .param p3, "iconHeight"    # I

    .prologue
    const/4 v3, 0x0

    .line 92
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 93
    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;

    .line 95
    .local v1, "roundView":Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->shouldUseRoundedMask:Z

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setUseRoundedMask(Z)V

    .line 96
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->IF_SLOW_LOAD:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setFadeIn(Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;)V

    .line 97
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->backgroundColorResId:I

    if-eqz v2, :cond_0

    .line 98
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->backgroundColorResId:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setBackgroundPaintColor(I)V

    .line 101
    .end local v1    # "roundView":Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;
    :cond_0
    invoke-virtual {p1, v3, v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 102
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setBackgroundResource(I)V

    .line 103
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->isResourceId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    invoke-virtual {p1, v2, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 57
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .line 59
    .local v0, "otherId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    .line 60
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 62
    .end local v0    # "otherId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public isAttachmentId()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isResourceId()Z
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    goto :goto_0
.end method
