.class public Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;
.super Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;
.source "NSPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NSPopupListAdapter"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "popupActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 47
    return-void
.end method

.method private getPopupItem(I)Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;->getPopupItem(I)Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    move-result-object v0

    .line 61
    .local v0, "popupAction":Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;

    .end local v0    # "popupAction":Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
    # getter for: Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->access$000(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, -0x2

    .line 68
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;->getItemViewType(I)I

    move-result v3

    if-nez v3, :cond_0

    .line 69
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 71
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;->getPopupItem(I)Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;

    .line 72
    .local v2, "popupAction":Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;
    const/4 v3, 0x0

    invoke-super {p0, p1, v3, p3}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ListMenuItemView;

    .line 77
    .local v0, "listMenuItemView":Landroid/support/v7/internal/view/menu/ListMenuItemView;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ge v3, v4, :cond_1

    .line 78
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/ListMenuItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 80
    :cond_1
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 82
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const v3, 0x800015

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 84
    # getter for: Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->access$000(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 85
    # getter for: Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;->access$000(Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupAction;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/internal/view/menu/ListMenuItemView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x2

    return v0
.end method

.method public onSelect(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/nspopupmenu/NSPopupMenu$NSPopupListAdapter;->getPopupItem(I)Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    invoke-interface {v0}, Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    .line 93
    return-void
.end method
