.class public abstract Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "NormalEdition.java"


# instance fields
.field private editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method protected constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 25
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 60
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    if-nez v2, :cond_0

    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 63
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 62
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 64
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-eqz v1, :cond_0

    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v2

    iget-object v3, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 65
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 67
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    if-eqz v0, :cond_0

    .line 68
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 72
    .end local v0    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 29
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 30
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    .line 31
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 33
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-static {p1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->normalEditionList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;)Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public sectionCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppSectionsCollection(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    const-string v0, "%s - appId: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;->getAppId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
