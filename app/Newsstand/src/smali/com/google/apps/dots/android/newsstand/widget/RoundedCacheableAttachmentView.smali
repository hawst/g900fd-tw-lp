.class public Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;
.super Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
.source "RoundedCacheableAttachmentView.java"


# static fields
.field private static final BACKGROUND_PADDING:I = 0x1

.field public static final DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

.field public static final DK_ROUNDED_SOURCE_ICON_INSET:I

.field public static final DK_USE_ROUNDED_SOURCE_ICON:I


# instance fields
.field private backgroundPaint:Landroid/graphics/Paint;

.field private final backgroundRect:Landroid/graphics/RectF;

.field private final backgroundRectWithInset:Landroid/graphics/RectF;

.field private bindRoundIconBackgroundKey:Ljava/lang/Integer;

.field private bindRoundIconInsetKey:Ljava/lang/Integer;

.field private bindUseRoundIconKey:Ljava/lang/Integer;

.field private bitmapPaint:Landroid/graphics/Paint;

.field private final bitmapRect:Landroid/graphics/RectF;

.field private inset:I

.field private final matrix:Landroid/graphics/Matrix;

.field private shader:Landroid/graphics/BitmapShader;

.field private shouldUseRoundedMask:Z

.field private useBackgroundPaint:Z

.field private final viewRect:Landroid/graphics/RectF;

.field private final viewRectWithInset:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RoundedCacheableAttachmentView_useRoundedSourceIcon:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_USE_ROUNDED_SOURCE_ICON:I

    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RoundedCacheableAttachmentView_roundedSourceIconBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RoundedCacheableAttachmentView_roundedSourceIconInset:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_INSET:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shouldUseRoundedMask:Z

    .line 48
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    .line 49
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    .line 50
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    .line 51
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRectWithInset:Landroid/graphics/RectF;

    .line 52
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapRect:Landroid/graphics/RectF;

    .line 54
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapPaint:Landroid/graphics/Paint;

    .line 56
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundPaint:Landroid/graphics/Paint;

    .line 57
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->matrix:Landroid/graphics/Matrix;

    .line 63
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundedCacheableAttachmentView:[I

    .line 64
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 65
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundedCacheableAttachmentView_bindUseRoundIconKey:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindUseRoundIconKey:Ljava/lang/Integer;

    .line 67
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundedCacheableAttachmentView_bindRoundIconBackgroundKey:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconBackgroundKey:Ljava/lang/Integer;

    .line 69
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundedCacheableAttachmentView_bindRoundIconInsetKey:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconInsetKey:Ljava/lang/Integer;

    .line 71
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    return-void
.end method

.method private updateStateForBitmap(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 123
    if-eqz p1, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 125
    new-instance v0, Landroid/graphics/BitmapShader;

    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, p1, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    goto :goto_0
.end method


# virtual methods
.method public clearBackgroundColor()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->useBackgroundPaint:Z

    .line 94
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shouldUseRoundedMask:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    if-nez v0, :cond_1

    .line 140
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->onDraw(Landroid/graphics/Canvas;)V

    .line 166
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRectWithInset:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 147
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 151
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->useBackgroundPaint:Z

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRectWithInset:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRectWithInset:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRectWithInset:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->matrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 162
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 163
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRectWithInset:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 98
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->onSizeChanged(IIII)V

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->getPaddingLeft()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->getPaddingTop()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->getPaddingRight()I

    move-result v3

    sub-int v3, p1, v3

    int-to-float v3, v3

    .line 100
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->getPaddingBottom()I

    move-result v4

    sub-int v4, p2, v4

    int-to-float v4, v4

    .line 99
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 101
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v5

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v5

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v5

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->viewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 103
    return-void
.end method

.method protected releaseBitmap()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shader:Landroid/graphics/BitmapShader;

    .line 134
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->releaseBitmap()V

    .line 135
    return-void
.end method

.method public setBackgroundPaintColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    const/4 v1, 0x1

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 89
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->useBackgroundPaint:Z

    .line 90
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->updateStateForBitmap(Landroid/graphics/Bitmap;)V

    .line 108
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 113
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 114
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->updateStateForBitmap(Landroid/graphics/Bitmap;)V

    .line 119
    :goto_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->updateStateForBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setUseRoundedMask(Z)V
    .locals 0
    .param p1, "shouldUseRoundedMask"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->shouldUseRoundedMask:Z

    .line 84
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v3, 0x0

    .line 170
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 171
    if-eqz p1, :cond_2

    .line 173
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindUseRoundIconKey:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindUseRoundIconKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    .line 175
    .local v1, "useRoundIcon":Z
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setUseRoundedMask(Z)V

    .line 178
    .end local v1    # "useRoundIcon":Z
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconBackgroundKey:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconBackgroundKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconBackgroundKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 180
    .local v0, "colorResId":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setBackgroundPaintColor(I)V

    .line 183
    .end local v0    # "colorResId":I
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconInsetKey:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 184
    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconInsetKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 185
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->bindRoundIconInsetKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    .line 191
    :cond_2
    :goto_0
    return-void

    .line 187
    :cond_3
    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->inset:I

    goto :goto_0
.end method
