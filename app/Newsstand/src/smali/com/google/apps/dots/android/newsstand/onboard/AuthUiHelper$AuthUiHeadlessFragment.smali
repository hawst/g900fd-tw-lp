.class public Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;
.super Landroid/support/v4/app/Fragment;
.source "AuthUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthUiHeadlessFragment"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 505
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 503
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 507
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 508
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;Lcom/google/apps/dots/android/newsstand/async/AsyncScope;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    return-void
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 512
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 513
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " - onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 514
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->setRetainInstance(Z)V

    .line 515
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 516
    if-eqz p1, :cond_0

    .line 517
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->restoreInstanceState(Landroid/os/Bundle;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$500(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Landroid/os/Bundle;)V

    .line 519
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 533
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " - onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 535
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 536
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 523
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 524
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$AuthUiHeadlessFragment;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->saveInstanceState(Landroid/os/Bundle;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->access$600(Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;Landroid/os/Bundle;)V

    .line 525
    return-void
.end method
