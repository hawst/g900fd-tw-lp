.class public Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;
.super Ljava/lang/Object;
.source "ManifestBlobResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResourceBundle"
.end annotation


# instance fields
.field objectMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

.field uriMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->this$0:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->uriMap:Ljava/util/Map;

    .line 31
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->objectMap:Ljava/util/Map;

    .line 33
    return-void
.end method

.method private registerObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 71
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getObjectMapKey(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->access$300(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->objectMap:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void
.end method


# virtual methods
.method public registerAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 1
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 63
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    return-void
.end method

.method public registerAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 1
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 67
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public registerMapping(Lcom/google/apps/dots/android/newsstand/store/ResourceLink;)V
    .locals 6
    .param p1, "link"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    .prologue
    .line 44
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->id:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->getUriMapKey(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->access$000(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->this$0:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->lock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->access$100(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 46
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->this$0:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->findRef(Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    invoke-static {v3, v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->access$200(Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;Ljava/lang/Object;)[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v1

    .line 47
    .local v1, "prevLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    if-eqz v1, :cond_1

    .line 49
    const/4 v3, 0x0

    aget-object v2, v1, v3

    .line 52
    .local v2, "previousVersion":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-object v5, v2, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/store/Version;->newerThan(Lcom/google/apps/dots/android/newsstand/store/Version;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 58
    .end local v2    # "previousVersion":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->uriMap:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    monitor-exit v4

    .line 60
    return-void

    .line 56
    :cond_1
    const/4 v3, 0x1

    new-array v1, v3, [Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    .end local v1    # "prevLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    const/4 v3, 0x0

    aput-object p1, v1, v3

    .restart local v1    # "prevLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    goto :goto_0

    .line 59
    .end local v1    # "prevLinkRef":[Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public registerMapping(Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;)V
    .locals 1
    .param p1, "link"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .prologue
    .line 37
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->fromLink(Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    .line 38
    .local v0, "resourceLink":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver$ResourceBundle;->registerMapping(Lcom/google/apps/dots/android/newsstand/store/ResourceLink;)V

    .line 41
    :cond_0
    return-void
.end method
