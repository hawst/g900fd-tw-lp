.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addSynchronousCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
        "<TV;TV;>;"
    }
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/google/common/util/concurrent/FutureCallback;

.field final synthetic val$future:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method constructor <init>(Lcom/google/common/util/concurrent/FutureCallback;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;->val$callback:Lcom/google/common/util/concurrent/FutureCallback;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 273
    .local p1, "input":Ljava/lang/Object;, "TV;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;->val$callback:Lcom/google/common/util/concurrent/FutureCallback;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/FutureCallback;->onSuccess(Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$8;->val$callback:Lcom/google/common/util/concurrent/FutureCallback;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/FutureCallback;->onFailure(Ljava/lang/Throwable;)V

    .line 280
    throw p1
.end method
