.class Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;
.super Ljava/lang/Object;
.source "InAppPurchaseIntentBuilder.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->start(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

.field final synthetic val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 323
    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "onActivityResult"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "intent: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 325
    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "extras: %s"

    new-array v4, v7, [Ljava/lang/Object;

    if-nez p3, :cond_1

    const-string v1, "null"

    :goto_0
    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    # invokes: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->tryLoggingConversionEvent()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$100(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)V

    .line 328
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->showPurchaseToast:Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$200(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->activity:Landroid/app/Activity;

    .line 330
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    # invokes: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->getPurchaseSuccessMessageId()I
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$300(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 331
    .local v6, "message":Ljava/lang/String;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->val$nsActivity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 333
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$400(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Lcom/google/apps/dots/android/newsstand/util/DocType;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->backendDocId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$500(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    .line 334
    # getter for: Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->parentBackendDocId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->access$600(Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/DocType;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    .local v0, "goToPurchaseOperation":Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/navigation/InAppPurchaseIntentBuilder;->activity:Landroid/app/Activity;

    .line 336
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_display_iap_long_ms:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 335
    invoke-static {v1, v6, v0, v7, v2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V

    .line 340
    .end local v0    # "goToPurchaseOperation":Lcom/google/apps/dots/android/newsstand/toast/GoToPurchaseToastOperation;
    .end local v6    # "message":Ljava/lang/String;
    :cond_0
    return-void

    .line 325
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method
