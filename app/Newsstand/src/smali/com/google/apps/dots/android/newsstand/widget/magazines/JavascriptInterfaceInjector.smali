.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
.super Ljava/lang/Object;
.source "JavascriptInterfaceInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;
    }
.end annotation


# static fields
.field private static final COMMA_SPLITTER:Lcom/google/common/base/Splitter;

.field private static final DOTS_NATIVEBODY_ONAPPEAR:Ljava/lang/String; = "javascript:dots.nativeBody.onAppear()"

.field private static final DOTS_NATIVEBODY_ONDISAPPEAR:Ljava/lang/String; = "javascript:dots.nativeBody.onDisappear()"

.field public static final DOTS_NBBRIDGE:Ljava/lang/String; = "dots_nbbridge"

.field private static final DOTS_NBBRIDGE_JS_LOADER:Ljava/lang/String; = "javascript:(function(s,t){if(!document.querySelector(\'script[src=\"\'+s+\'\"]\')){var e=document.createElement(\'script\');e.src=s;document.lastChild.appendChild(e);if(t){e=document.createElement(\'script\');e.src=t;document.lastChild.appendChild(e)}}})(\'%s\',\'%s\')"

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final bridge:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

.field private final context:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private documentLoaded:Z

.field private final handler:Landroid/os/Handler;

.field private isOnScreen:Z

.field private nbInterfaceUri:Ljava/lang/String;

.field private final scriptInjectedRunnable:Ljava/lang/Runnable;

.field private scriptReady:Z

.field private final touchEventDefaultPreventedRunnable:Ljava/lang/Runnable;

.field private final unhandledClickRunnable:Ljava/lang/Runnable;

.field private final webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 30
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "webView"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .param p3, "contentBaseUri"    # Landroid/net/Uri;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;

    .line 41
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->unhandledClickRunnable:Ljava/lang/Runnable;

    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptInjectedRunnable:Ljava/lang/Runnable;

    .line 66
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->touchEventDefaultPreventedRunnable:Ljava/lang/Runnable;

    .line 119
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$1;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->bridge:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->context:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 123
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    .line 125
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "synced/nbInterface.js"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->nbInterfaceUri:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->bridge:Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector$Bridge;

    const-string v1, "dots_nbbridge"

    invoke-virtual {p2, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z

    return v0
.end method

.method static synthetic access$1000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->isOnScreen:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onAppear()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->unhandledClickRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptInjectedRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->touchEventDefaultPreventedRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900()Lcom/google/common/base/Splitter;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    return-object v0
.end method

.method private onAppear()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const-string v1, "javascript:dots.nativeBody.onAppear()"

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->executeJavascript(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method private onDisappear()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const-string v1, "javascript:dots.nativeBody.onDisappear()"

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->executeJavascript(Ljava/lang/String;)V

    .line 167
    return-void
.end method


# virtual methods
.method public onEnterScreen()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->isOnScreen:Z

    .line 149
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onAppear()V

    .line 152
    :cond_0
    return-void
.end method

.method public onExitScreen()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->isOnScreen:Z

    .line 156
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onDisappear()V

    .line 159
    :cond_0
    return-void
.end method

.method public onLoadComplete()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z

    .line 142
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->isOnScreen:Z

    if-eqz v0, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->onAppear()V

    .line 145
    :cond_0
    return-void
.end method

.method public requestDetachment()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const-string v1, "dots_nbbridge"

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public requestInjection()V
    .locals 6

    .prologue
    .line 135
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLoadExtraJs()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "loadExtraJs":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->webView:Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;

    const-string v2, "javascript:(function(s,t){if(!document.querySelector(\'script[src=\"\'+s+\'\"]\')){var e=document.createElement(\'script\');e.src=s;document.lastChild.appendChild(e);if(t){e=document.createElement(\'script\');e.src=t;document.lastChild.appendChild(e)}}})(\'%s\',\'%s\')"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->nbInterfaceUri:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    .line 137
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/WebPartView;->executeJavascript(Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public resetState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 130
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->documentLoaded:Z

    .line 131
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/JavascriptInterfaceInjector;->scriptReady:Z

    .line 132
    return-void
.end method
