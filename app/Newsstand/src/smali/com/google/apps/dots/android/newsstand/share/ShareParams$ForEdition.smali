.class public Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
.super Lcom/google/apps/dots/android/newsstand/share/ShareParams;
.source "ShareParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/share/ShareParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForEdition"
.end annotation


# instance fields
.field public editionDescription:Ljava/lang/String;

.field public editionName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;
    .param p2, "dialogTitle"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;-><init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;-><init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "dialogTitle"    # Ljava/lang/String;

    .prologue
    .line 74
    sget-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;->EDITION:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;-><init>(Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$1;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;)Z
    .locals 2
    .param p1, "other"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .prologue
    .line 107
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionDescription:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionDescription:Ljava/lang/String;

    .line 108
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->equalTo(Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEditionDescription(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 0
    .param p1, "editionDescription"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionDescription:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public setEditionName(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 0
    .param p1, "editionName"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionName:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method public setLongUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->longUrl:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public setShortUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->shortUrl:Ljava/lang/String;

    .line 83
    return-object p0
.end method
