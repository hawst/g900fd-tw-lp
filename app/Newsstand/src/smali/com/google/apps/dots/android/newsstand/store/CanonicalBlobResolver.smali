.class public Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;
.super Ljava/lang/Object;
.source "CanonicalBlobResolver.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;


# static fields
.field private static final PERSONALIZED_RESOURCE_TYPES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 21
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_FAMILY_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->POST:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .line 22
    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->PERSONALIZED_RESOURCE_TYPES:Ljava/util/EnumSet;

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 0
    .param p1, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p2, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 30
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 31
    return-void
.end method

.method public static isFifeUrl(Ljava/lang/String;)Z
    .locals 1
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 106
    if-eqz p0, :cond_1

    const-string v0, "fife://"

    .line 107
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fifes://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUrl(Ljava/lang/String;)Z
    .locals 1
    .param p0, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 111
    if-eqz p0, :cond_1

    const-string v0, "http://"

    .line 112
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    .prologue
    .line 35
    const/4 v3, 0x0

    .line 36
    .local v3, "uri":Landroid/net/Uri;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->LOCAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    .line 37
    .local v5, "uriType":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->PERSONALIZED_RESOURCE_TYPES:Ljava/util/EnumSet;

    invoke-virtual {v0, p3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 38
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getPersonalResourceUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v6

    .line 41
    .local v6, "resourceUri":Landroid/net/Uri;
    :goto_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$LinkType:[I

    invoke-virtual {p3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    const/4 v0, 0x0

    .line 102
    :goto_1
    return-object v0

    .line 38
    .end local v6    # "resourceUri":Landroid/net/Uri;
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 39
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getResourceUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_0

    .line 43
    .restart local v6    # "resourceUri":Landroid/net/Uri;
    :pswitch_0
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "apps"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 102
    :goto_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/Version;->ANY:Lcom/google/apps/dots/android/newsstand/store/Version;

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Landroid/net/Uri;Lcom/google/apps/dots/android/newsstand/store/Version;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;)V

    goto :goto_1

    .line 47
    :pswitch_1
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sections"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 48
    goto :goto_2

    .line 51
    :pswitch_2
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "posts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 52
    goto :goto_2

    .line 55
    :pswitch_3
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "forms"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 56
    goto :goto_2

    .line 59
    :pswitch_4
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "formtemplates"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 61
    goto :goto_2

    .line 64
    :pswitch_5
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "family-summary"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 66
    goto :goto_2

    .line 69
    :pswitch_6
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "application-summary"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 71
    goto :goto_2

    .line 74
    :pswitch_7
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->isUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 76
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->EXTERNAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    goto/16 :goto_2

    .line 77
    :cond_1
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->isFifeUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    const-string v1, "http"

    const-string v0, "fife"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 81
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;->FIFE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$UriType;

    goto/16 :goto_2

    .line 80
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 83
    :cond_3
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "attachments"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 86
    goto/16 :goto_2

    .line 90
    :pswitch_8
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 91
    goto/16 :goto_2

    .line 95
    :pswitch_9
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getLayoutResourceUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 96
    goto/16 :goto_2

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
