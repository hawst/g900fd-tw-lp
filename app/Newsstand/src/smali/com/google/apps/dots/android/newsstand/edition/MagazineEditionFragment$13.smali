.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .param p2, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 690
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 9
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 694
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 696
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/bind/data/Data;

    sget v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 697
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/bind/data/Data;

    sget v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 699
    :cond_0
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/bind/data/Data;

    sget v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 700
    :cond_1
    invoke-interface {p1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 703
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    move v1, v5

    .line 704
    .local v1, "hasInitialPostId":Z
    :goto_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v4

    sget-object v7, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v4, v7, :cond_a

    move v3, v5

    .line 705
    .local v3, "isLandscape":Z
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 706
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v1, :cond_7

    .line 713
    const/4 v2, 0x0

    .line 717
    .local v2, "isInitialCard":Z
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-nez v6, :cond_4

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 718
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 719
    :cond_4
    sget v6, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 731
    :cond_5
    :goto_3
    if-eqz v2, :cond_7

    .line 735
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-nez v6, :cond_e

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 736
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 737
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IS_INITIAL_POST:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 753
    :cond_6
    :goto_4
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 754
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IS_INITIAL_POST:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 762
    .end local v2    # "isInitialCard":Z
    :cond_7
    if-eqz v3, :cond_3

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    .line 766
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-nez v6, :cond_8

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 767
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 768
    :cond_8
    sget v6, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v7, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_spread_item:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_2

    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "hasInitialPostId":Z
    .end local v3    # "isLandscape":Z
    :cond_9
    move v1, v6

    .line 703
    goto/16 :goto_0

    .restart local v1    # "hasInitialPostId":Z
    :cond_a
    move v3, v6

    .line 704
    goto/16 :goto_1

    .line 720
    .restart local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "isInitialCard":Z
    .restart local v3    # "isLandscape":Z
    :cond_b
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->DK_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 721
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_d

    .line 722
    :cond_c
    sget v6, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    .line 723
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_3

    .line 725
    :cond_d
    sget v6, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->DK_PRIMARY_KEY:I

    .line 726
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 728
    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPostId()Ljava/lang/String;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 727
    invoke-static {v7, v8}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionCardListFilter;->getCombinedPostIdAndPageNumber(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    .line 726
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_3

    .line 741
    :cond_e
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 742
    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_10

    .line 743
    :cond_f
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IS_INITIAL_POST:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 745
    :cond_10
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 746
    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_11

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$13;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->initialPageNumber()Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$2400(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, v5, :cond_12

    .line 747
    :cond_11
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IS_INITIAL_POST:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 749
    :cond_12
    if-eqz v3, :cond_6

    .line 750
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_EITHER_IS_INITIAL_POST:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto/16 :goto_4

    .line 772
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "isInitialCard":Z
    :cond_13
    return-object p1
.end method
