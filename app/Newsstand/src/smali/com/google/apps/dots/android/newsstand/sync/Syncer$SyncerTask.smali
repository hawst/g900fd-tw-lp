.class abstract Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "SyncerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final debugId:Ljava/lang/String;

.field final debugTitle:Ljava/lang/String;

.field final maxTries:I

.field final taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const-wide/16 v4, 0x3e8

    .line 240
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 241
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->SYNC:Lcom/google/apps/dots/android/newsstand/async/Queue;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->priorityBoostMs:J
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->priority()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;J)V

    .line 236
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->maxTries:I

    .line 242
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 243
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->debugId:Ljava/lang/String;

    .line 244
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->debugTitle(Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {p1, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->debugTitle:Ljava/lang/String;

    .line 245
    iget v0, p2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->requiredProgress:F

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->increaseProgressRequired(F)V
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;F)V

    .line 246
    return-void
.end method

.method private extractStoreResponse(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;"
        }
    .end annotation

    .prologue
    .line 327
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    .local p1, "result":Ljava/lang/Object;, "TV;"
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    if-eqz v0, :cond_0

    .line 328
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .end local p1    # "result":Ljava/lang/Object;, "TV;"
    iget-object p1, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .line 334
    :goto_0
    return-object p1

    .line 329
    .restart local p1    # "result":Ljava/lang/Object;, "TV;"
    :cond_0
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    if-eqz v0, :cond_1

    .line 330
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    goto :goto_0

    .line 331
    :cond_1
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    if-eqz v0, :cond_2

    .line 332
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    .end local p1    # "result":Ljava/lang/Object;, "TV;"
    iget-object p1, p1, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    goto :goto_0

    .line 334
    .restart local p1    # "result":Ljava/lang/Object;, "TV;"
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->callInternal()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->SYNC:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 234
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public abstract callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method isFatal(Ljava/lang/Throwable;)Z
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 348
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$FatalSyncerException;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isIgnorable(Ljava/lang/Throwable;)Z
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 355
    instance-of v2, p1, Ljava/util/concurrent/ExecutionException;

    if-eqz v2, :cond_1

    .line 356
    check-cast p1, Ljava/util/concurrent/ExecutionException;

    .end local p1    # "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->isIgnorable(Ljava/lang/Throwable;)Z

    move-result v0

    .line 365
    :cond_0
    :goto_0
    return v0

    .line 359
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_1
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    if-eqz v2, :cond_3

    .line 361
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 362
    :cond_3
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;

    if-nez v2, :cond_0

    move v0, v1

    .line 365
    goto :goto_0
.end method

.method maybeSetPin(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    .local p1, "result":Ljava/lang/Object;, "TV;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 314
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->extractStoreResponse(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    move-result-object v0

    .line 315
    .local v0, "storeResponse":Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->pinId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 316
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->snapshotId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 318
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "%s: Setting pin %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v5

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v4

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->snapshotId:Ljava/lang/Integer;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->pinId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v3

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->snapshotId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->pin(II)V

    .line 321
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->exists()Z

    move-result v1

    const-string v2, "Failed to write blob file for %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p0, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 324
    :cond_0
    return-void
.end method

.method public onFailure(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 291
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->isFatal(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 308
    :goto_0
    return-object v0

    .line 295
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->isIgnorable(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: Ignoring %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v4

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->ignoreCounts:Lcom/google/common/collect/Multiset;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/common/collect/Multiset;

    move-result-object v0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/google/common/collect/Multiset;->add(Ljava/lang/Object;I)I

    .line 299
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 299
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 303
    :try_start_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->errorCounts:Lcom/google/common/collect/Multiset;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/common/collect/Multiset;

    move-result-object v0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/google/common/collect/Multiset;->add(Ljava/lang/Object;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->maxFailures:I

    if-lt v0, v2, :cond_2

    .line 304
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v2, "%s: Too many errors for type, failing."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$FatalSyncerException;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$FatalSyncerException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 307
    :cond_2
    :try_start_3
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v2, "%s: Recorded error. Proceeding."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 286
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    .local p1, "result":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->maybeSetPin(Ljava/lang/Object;)V

    .line 287
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    iget v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->requiredProgress:F

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->reportProgressCompleted(F)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;F)V

    .line 288
    return-void
.end method

.method shouldNotBeRetried(Ljava/lang/Throwable;)Z
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    instance-of v3, p1, Ljava/util/concurrent/ExecutionException;

    if-eqz v3, :cond_1

    .line 373
    check-cast p1, Ljava/util/concurrent/ExecutionException;

    .end local p1    # "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->shouldNotBeRetried(Ljava/lang/Throwable;)Z

    move-result v1

    .line 387
    :cond_0
    :goto_0
    return v1

    .line 376
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;

    if-nez v3, :cond_0

    .line 380
    instance-of v3, p1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    if-eqz v3, :cond_2

    .line 381
    check-cast p1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    .end local p1    # "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->getResponseStatus()Ljava/lang/Integer;

    move-result-object v0

    .line 382
    .local v0, "responseStatus":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 383
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0x64

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "responseStatus":Ljava/lang/Integer;
    :cond_2
    move v1, v2

    .line 387
    goto :goto_0
.end method

.method public start()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->startWithRetry(II)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method startWithRetry(II)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "currentTry"    # I
    .param p2, "numTries"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 266
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    if-ne v1, v2, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 268
    .local v0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    :goto_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask$2;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;II)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1

    .line 267
    .end local v0    # "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sharedTaskQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$300()Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 341
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<TV;>;"
    const-string v0, "Task[syncing %s \'%s\' (for %s)]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->taskType:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->debugId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;->debugTitle:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
