.class Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;
.super Ljava/lang/Object;
.source "ViewPartModel.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->onConfigureEvents(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/net/Uri;)V
    .locals 2
    .param p1, "event"    # Landroid/net/Uri;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->visible:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->visible:Z
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;Z)Z

    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->getFadeMillis(Landroid/net/Uri;)I
    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;Landroid/net/Uri;)I

    move-result v1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->fade(I)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/ViewPartModel;I)V

    .line 77
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
