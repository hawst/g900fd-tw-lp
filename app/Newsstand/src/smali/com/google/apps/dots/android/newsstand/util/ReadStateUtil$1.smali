.class final Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "ReadStateUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 5
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 79
    if-nez p1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$account:Landroid/accounts/Account;

    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->val$postId:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->access$000(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
