.class public Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "ViewActionIntentBuilder.java"


# instance fields
.field protected final intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 17
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->intent:Landroid/content/Intent;

    .line 18
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public setUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 22
    return-object p0
.end method

.method public setUri(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 27
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 28
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->setUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    .line 30
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p0
.end method
