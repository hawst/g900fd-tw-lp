.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 640
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 643
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v6

    const-string v7, "%s: syncing deps"

    new-array v8, v2, [Ljava/lang/Object;

    aput-object p0, v8, v5

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 644
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 645
    .local v4, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v6, v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2000(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsShared$Post;->appId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAppSummaryBlob(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v6, v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 647
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSectionId()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncSectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v6, v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 649
    const/4 v3, 0x0

    .line 651
    .local v3, "primaryAttachmentId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v6

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v6, v7, :cond_2

    .line 654
    .local v2, "isMagazinePost":Z
    :goto_0
    if-nez v2, :cond_0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasSummary()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 655
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    .line 659
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getPostAttachmentIds(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 664
    if-eqz v2, :cond_3

    .line 665
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 672
    .local v1, "attachmentTaskType":Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    :goto_2
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAttachmentBlob(Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v7, v0, v8, v5, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2300(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "attachmentTaskType":Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .end local v2    # "isMagazinePost":Z
    :cond_2
    move v2, v5

    .line 651
    goto :goto_0

    .line 667
    .restart local v0    # "attachmentId":Ljava/lang/String;
    .restart local v2    # "isMagazinePost":Z
    :cond_3
    invoke-static {v3, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_PRIMARY_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .restart local v1    # "attachmentTaskType":Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    :goto_3
    goto :goto_2

    .end local v1    # "attachmentTaskType":Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    :cond_4
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_OTHER_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    goto :goto_3

    .line 675
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$6;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v5, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    return-object v5
.end method
