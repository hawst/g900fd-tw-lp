.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 128
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$100(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->hide(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->purchasedEditionList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$100(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 137
    :cond_0
    return-void
.end method
