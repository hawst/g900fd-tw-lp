.class public Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "PlayStoreIntentBuilder.java"


# instance fields
.field private path:Ljava/lang/String;

.field private useDirectPurchase:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->path:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->useDirectPurchase:Z

    .line 24
    return-void
.end method

.method public static isPlayStoreUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 27
    const-string v0, "play.google.com"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 70
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->path:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 71
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 72
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->magazines_market_uri:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->path:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 74
    :cond_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 76
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_2

    .line 81
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    :cond_2
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->useDirectPurchase:Z

    if-eqz v2, :cond_3

    .line 85
    const-string v2, "use_direct_purchase"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 86
    const/high16 v2, 0x10000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 89
    :cond_3
    const-string v2, "authAccount"

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    return-object v0

    .line 72
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setMagazineCategory(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    .locals 5
    .param p1, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->activity:Landroid/app/Activity;

    .line 45
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->magazines_market_category_uri:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v1

    return-object v1
.end method

.method public setMagazineDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    .locals 5
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->magazines_market_title_uri:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v1

    return-object v1
.end method

.method public setNewsDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    .locals 5
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->news_market_title_uri:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    move-result-object v1

    return-object v1
.end method

.method public setPath(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->path:Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public setUseDirectPurchase(Z)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    .locals 0
    .param p1, "useDirectPurchase"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->useDirectPurchase:Z

    .line 64
    return-object p0
.end method

.method public start(Z)V
    .locals 1
    .param p1, "addToBackstack"    # Z

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->useDirectPurchase:Z

    if-eqz v0, :cond_0

    .line 96
    const/16 v0, 0x66

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->startForResult(IZ)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start(Z)V

    goto :goto_0
.end method
