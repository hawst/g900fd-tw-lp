.class public Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;
.super Landroid/app/Activity;
.source "CrashReportActivity.java"


# instance fields
.field private exitOnDismiss:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->exitOnDismiss:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->exitOnDismiss:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->exitOnDismiss:Z

    return p1
.end method

.method private static schedule(Landroid/content/Context;Ljava/lang/Class;JLjava/util/Map;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "delay"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "activity":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Activity;>;"
    .local p4, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 66
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    .local v1, "intent":Landroid/content/Intent;
    const v4, 0x10008000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 72
    if-eqz p4, :cond_0

    .line 73
    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 77
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-static {p0, v7, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 78
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 79
    .local v2, "mgr":Landroid/app/AlarmManager;
    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, p2

    invoke-virtual {v2, v4, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 80
    return-void
.end method

.method public static schedule(Landroid/content/Context;Ljava/lang/Throwable;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ex"    # Ljava/lang/Throwable;
    .param p2, "dumpHprof"    # Z

    .prologue
    .line 27
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->capture(Landroid/content/Context;Ljava/lang/Throwable;Z)Ljava/util/Map;

    move-result-object v0

    .line 28
    .local v0, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-class v1, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    const-wide/16 v2, 0x0

    invoke-static {p0, v1, v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->schedule(Landroid/content/Context;Ljava/lang/Class;JLjava/util/Map;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->app_name:I

    .line 35
    .local v0, "appName":I
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->crash_report_message:I

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 36
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    const/4 v4, 0x0

    .line 37
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->report:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$1;-><init>(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)V

    .line 38
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 49
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 50
    new-instance v2, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;-><init>(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 60
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 61
    return-void
.end method
