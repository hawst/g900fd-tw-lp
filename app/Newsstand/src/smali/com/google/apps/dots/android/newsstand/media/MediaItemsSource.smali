.class public Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;
.super Ljava/lang/Object;
.source "MediaItemsSource.java"


# static fields
.field private static final AUDIO_EQUALITY_FIELDS:[I

.field private static final AUDIO_FILTER:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

.field public static final DK_AUDIO_ID:I

.field public static final DK_AUDIO_INDEX:I

.field public static final DK_MEDIA_ITEM:I

.field public static final DK_POST_ID:I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SUPPORTED_MEDIA_DRAWER_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-class v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MediaItemsSource_mediaItem:I

    sput v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MediaItemsSource_audioId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_ID:I

    .line 41
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MediaItemsSource_audioIndex:I

    sput v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_INDEX:I

    .line 42
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MediaItemsSource_postId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    .line 44
    const/4 v0, 0x4

    .line 45
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->SUPPORTED_MEDIA_DRAWER_TYPES:Ljava/util/List;

    .line 47
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->AUDIO_EQUALITY_FIELDS:[I

    .line 51
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$1;-><init>(Ljava/util/concurrent/Executor;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->AUDIO_FILTER:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/List;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->SUPPORTED_MEDIA_DRAWER_TYPES:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->makeMediaItemData(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public static audioId(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "postId"    # Ljava/lang/String;
    .param p1, "index"    # I

    .prologue
    .line 148
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAudioPlaylistForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 143
    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 144
    .local v0, "dataList":Lcom/google/android/libraries/bind/data/DataList;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->AUDIO_EQUALITY_FIELDS:[I

    sget v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_ID:I

    sget-object v3, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->AUDIO_FILTER:Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    return-object v1
.end method

.method private static getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 4
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/libraries/bind/data/DataList;

    sget v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    invoke-direct {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 88
    .local v0, "dataList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 89
    .local v1, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;

    invoke-direct {v3, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$2;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 110
    return-object v0
.end method

.method public static getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/DataList;
    .locals 2
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "fieldId"    # Ljava/lang/String;
    .param p3, "restrictToSingleField"    # Z

    .prologue
    .line 115
    if-eqz p3, :cond_0

    invoke-static {p2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 118
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->getMediaDrawerList(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->restrictToSingleFieldFilter(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    goto :goto_0
.end method

.method private static makeMediaItemData(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 6
    .param p0, "postId"    # Ljava/lang/String;
    .param p1, "item"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .param p2, "offset"    # I

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 124
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    :try_start_0
    sget v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v5, v5, p2

    invoke-direct {v3, p0, p2, v4, v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    return-object v0

    .line 126
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Received MediaItem of unsupported type. Shouldn\'t have happened."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static restrictToSingleFieldFilter(Ljava/lang/String;)Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
    .locals 2
    .param p0, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$3;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource$3;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    return-object v0
.end method
