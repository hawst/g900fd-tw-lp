.class public abstract Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.super Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;
.source "EditionCardList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;
    }
.end annotation


# instance fields
.field private final account:Landroid/accounts/Account;

.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field private final observedPreferenceKeys:[Ljava/lang/String;

.field private prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "downloadedOnly"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "pinnedAccounts"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->observedPreferenceKeys:[Ljava/lang/String;

    .line 41
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->account:Landroid/accounts/Account;

    .line 44
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->account:Landroid/accounts/Account;

    invoke-virtual {p2, v1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStatesUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "readStatesUri":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 46
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->addEventUriToWatch(Landroid/net/Uri;)V

    .line 48
    :cond_0
    return-void
.end method

.method protected static preferenceAffectsApiUri(Ljava/lang/String;)Z
    .locals 1
    .param p0, "optPreferenceKey"    # Ljava/lang/String;

    .prologue
    .line 81
    if-eqz p0, :cond_0

    const-string v0, "pinnedAccounts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 3

    .prologue
    .line 140
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->appContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 141
    .local v1, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->rawCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 142
    .local v0, "cardGroup":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 143
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 144
    return-object v1
.end method

.method protected getObservedPreferenceKeys()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->observedPreferenceKeys:[Ljava/lang/String;

    return-object v0
.end method

.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onPreferencesChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "optPreferenceKey"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->preferenceAffectsApiUri(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->invalidateApiUri(Landroid/accounts/Account;)V

    .line 78
    :cond_0
    return-void
.end method

.method protected onRegisterForInvalidation()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->onRegisterForInvalidation()V

    .line 62
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->getObservedPreferenceKeys()[Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->onPreferencesChanged(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->onUnregisterForInvalidation()V

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->prefListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 89
    return-void
.end method

.method protected readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v0

    return-object v0
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 99
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList$EditionCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
