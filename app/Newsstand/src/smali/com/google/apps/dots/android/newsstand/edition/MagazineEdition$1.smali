.class final Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;
.super Ljava/lang/Object;
.source "MagazineEdition.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getMostRecentPostIdFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$appId:Ljava/lang/String;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$postIdPredicate:Lcom/google/common/base/Predicate;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$appId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$postIdPredicate:Lcom/google/common/base/Predicate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$appId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$1;->val$postIdPredicate:Lcom/google/common/base/Predicate;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->getMostRecentPostId(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/common/base/Predicate;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
