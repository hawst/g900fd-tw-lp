.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;
.super Ljava/lang/Object;
.source "EditionPagerFragment.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 204
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlayHeaderListLayoutChanged()V
    .locals 1

    .prologue
    .line 207
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isHeaderFloating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    goto :goto_0
.end method
