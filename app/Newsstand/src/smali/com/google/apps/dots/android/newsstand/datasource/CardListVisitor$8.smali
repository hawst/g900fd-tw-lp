.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeTopicWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 691
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 694
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 695
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleClickEvent;->track(Z)V

    .line 697
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .line 698
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 699
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 700
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 701
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->setPublisher(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;

    move-result-object v0

    .line 702
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->start()V

    .line 703
    return-void
.end method
