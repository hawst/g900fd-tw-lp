.class public Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;
.super Ljava/lang/Object;
.source "MutationRetryPolicy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isReadyToRetry(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z
    .locals 12
    .param p1, "mutationLog"    # Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .prologue
    const/4 v4, 0x1

    .line 21
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v5

    if-nez v5, :cond_1

    .line 31
    :cond_0
    :goto_0
    return v4

    .line 24
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 25
    .local v2, "now":J
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getLastHttpFailureTime()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gtz v5, :cond_0

    .line 29
    const-wide v6, 0x40c3880000000000L    # 10000.0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 30
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v5

    int-to-double v10, v5

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-long v0, v6

    .line 31
    .local v0, "backoffIntervalMs":J
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getLastHttpFailureTime()J

    move-result-wide v6

    sub-long v6, v2, v6

    cmp-long v5, v6, v0

    if-gtz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_0
.end method

.method public shouldRetryOnFailureResponse(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;I)Z
    .locals 4
    .param p1, "log"    # Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .param p2, "httpStatusCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    div-int/lit8 v2, p2, 0x64

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getNumTries()I

    move-result v2

    add-int/lit8 v0, v2, 0x1

    .line 43
    .local v0, "numTriesSoFar":I
    const/16 v2, 0xc

    if-ge v0, v2, :cond_0

    .line 46
    const/4 v1, 0x1

    goto :goto_0
.end method
