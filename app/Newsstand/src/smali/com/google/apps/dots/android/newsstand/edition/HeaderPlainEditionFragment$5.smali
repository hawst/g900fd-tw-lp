.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    invoke-static {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$900(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 270
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 264
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->header_plain_edition_content:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 265
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    invoke-static {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$800(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 260
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 279
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method
