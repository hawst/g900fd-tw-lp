.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "EditableLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->refreshSubscriptionCollectionAndStartSync(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

.field final synthetic val$onRefreshCompleted:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    .prologue
    .line 422
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;->val$onRefreshCompleted:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 1
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 425
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/SyncUtil;->startFullSyncIfConnected()V

    .line 426
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;->val$onRefreshCompleted:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;->val$onRefreshCompleted:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 429
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 422
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$11;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
