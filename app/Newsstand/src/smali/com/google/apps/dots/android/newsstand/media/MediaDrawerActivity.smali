.class public Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "MediaDrawerActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>(Z)V

    .line 24
    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x4

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->media_drawer_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->setContentView(I)V

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->media_drawer_pager_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .line 34
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 37
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 41
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 52
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 53
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 64
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->finish()V

    .line 66
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public toggleLightsOnMode()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->mediaDrawerPagerFragment:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->toggleLightsOnMode()V

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    goto :goto_0
.end method
