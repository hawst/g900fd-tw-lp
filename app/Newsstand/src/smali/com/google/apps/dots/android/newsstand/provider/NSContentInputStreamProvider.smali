.class public Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;
.super Ljava/lang/Object;
.source "NSContentInputStreamProvider.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method private getMatch(Landroid/net/Uri;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 61
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/Contract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 62
    .local v0, "result":I
    if-gez v0, :cond_0

    .line 63
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :cond_0
    return v0
.end method

.method private getStoreResponse(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->getMatch(Landroid/net/Uri;)I

    move-result v3

    .line 78
    .local v3, "match":I
    packed-switch v3, :pswitch_data_0

    .line 115
    :pswitch_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Unsupported uri: "

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 80
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v0, v10}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 109
    .end local v0    # "attachmentId":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 83
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 84
    .local v4, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    .restart local v0    # "attachmentId":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 86
    .local v7, "transformString":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/server/Transform;->parse(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v6

    .line 87
    .local v6, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    if-nez v6, :cond_0

    .line 88
    sget-object v8, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "Failed to parse transform: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :cond_0
    new-instance v8, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v9, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v8, v0, v9}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 91
    invoke-virtual {v8, v6}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 93
    .local v1, "givenTransformRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v8, v9, v1}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 94
    .local v2, "givenTransformResponse":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    new-instance v8, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;

    invoke-direct {v8, p0, v0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;-><init>(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;Ljava/lang/String;)V

    invoke-static {v2, v8}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 104
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "givenTransformRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .end local v2    # "givenTransformResponse":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    .end local v4    # "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    .end local v7    # "transformString":Ljava/lang/String;
    :pswitch_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v10, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .line 105
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v10, v11, v12}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 104
    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 109
    :pswitch_4
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->parse(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 110
    :catch_0
    move-exception v5

    .line 111
    .local v5, "tr":Ljava/lang/Throwable;
    sget-object v8, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v8, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 112
    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-direct {v8}, Ljava/io/FileNotFoundException;-><init>()V

    throw v8

    .line 78
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->getMatch(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 136
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :pswitch_1
    const-string v0, "application/octet-stream"

    .line 134
    :goto_0
    return-object v0

    .line 131
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 134
    :pswitch_3
    const-string v0, "application/octet-stream"

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 46
    .line 47
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->getStoreResponse(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$1;-><init>(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;)V

    .line 46
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 57
    .local v0, "storeResponseInputStreamFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/io/InputStream;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/io/AsyncInputStream;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    return-object v1
.end method
