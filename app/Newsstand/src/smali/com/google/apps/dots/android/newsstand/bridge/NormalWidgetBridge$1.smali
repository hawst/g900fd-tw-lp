.class Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;
.super Ljava/lang/Object;
.source "NormalWidgetBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->openAudio(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

.field final synthetic val$audioUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->val$audioUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->val$audioUri:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findAudioItemFromUri(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    move-result-object v0

    .line 44
    .local v0, "audio":Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    if-eqz v0, :cond_0

    .line 45
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;->this$0:Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-static {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->startAudio(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 47
    :cond_0
    return-void
.end method
