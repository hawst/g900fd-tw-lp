.class public Lcom/google/apps/dots/android/newsstand/server/Transform;
.super Ljava/lang/Object;
.source "Transform.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT:Ljava/lang/String;

.field public static final ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

.field private static final SPLITTER:Lcom/google/common/base/Splitter;


# instance fields
.field public final crop:Z

.field public final fcrop64Bottom:I

.field public final fcrop64Left:I

.field public final fcrop64Right:I

.field public final fcrop64Top:I

.field public final height:I

.field public final original:Z

.field public final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->original(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 32
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const-string v0, "nu-pa-rwu-l50"

    :goto_0
    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->DEFAULT:Ljava/lang/String;

    .line 38
    const-string v0, "-"

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(Ljava/lang/String;)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->SPLITTER:Lcom/google/common/base/Splitter;

    return-void

    .line 32
    :cond_0
    const-string v0, "nu-pa-l50"

    goto :goto_0
.end method

.method private constructor <init>(IIZIIIIZ)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "crop"    # Z
    .param p4, "fcrop64Left"    # I
    .param p5, "fcrop64Top"    # I
    .param p6, "fcrop64Right"    # I
    .param p7, "fcrop64Bottom"    # I
    .param p8, "original"    # Z

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    .line 174
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    .line 175
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    .line 176
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    .line 177
    iput p5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    .line 178
    iput p6, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    .line 179
    iput p7, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    .line 180
    iput-boolean p8, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    .line 181
    return-void
.end method

.method synthetic constructor <init>(IIZIIIIZLcom/google/apps/dots/android/newsstand/server/Transform$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # I
    .param p5, "x4"    # I
    .param p6, "x5"    # I
    .param p7, "x6"    # I
    .param p8, "x7"    # Z
    .param p9, "x8"    # Lcom/google/apps/dots/android/newsstand/server/Transform$1;

    .prologue
    .line 13
    invoke-direct/range {p0 .. p8}, Lcom/google/apps/dots/android/newsstand/server/Transform;-><init>(IIZIIIIZ)V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 7
    .param p0, "transform"    # Ljava/lang/String;

    .prologue
    .line 245
    const-string v4, "d"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 246
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 274
    :goto_0
    return-object v4

    .line 249
    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    .line 250
    .local v1, "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/Transform;->SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v4, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 251
    .local v2, "elem":Ljava/lang/String;
    const-string v5, "p"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 252
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->crop(Z)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    goto :goto_1

    .line 273
    .end local v1    # "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .end local v2    # "elem":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 274
    .local v3, "nfe":Ljava/lang/NumberFormatException;
    const/4 v4, 0x0

    goto :goto_0

    .line 253
    .end local v3    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v1    # "builder":Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .restart local v2    # "elem":Ljava/lang/String;
    :cond_2
    const-string v5, "w"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 254
    const-string v5, "w"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->width(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    goto :goto_1

    .line 255
    :cond_3
    const-string v5, "h"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 256
    const-string v5, "h"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->height(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    goto :goto_1

    .line 257
    :cond_4
    const-string v5, "fcrop64=1,"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 258
    const-string v5, "fcrop64=1,"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "arguments":Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v6, 0x4

    .line 260
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    .line 259
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Left(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 261
    const/4 v5, 0x4

    const/16 v6, 0x8

    .line 262
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    .line 261
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Top(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 264
    const/16 v5, 0x8

    const/16 v6, 0xc

    .line 265
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    .line 264
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Right(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    .line 267
    const/16 v5, 0xc

    const/16 v6, 0x10

    .line 268
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    .line 267
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    goto/16 :goto_1

    .line 272
    .end local v0    # "arguments":Ljava/lang/String;
    .end local v2    # "elem":Ljava/lang/String;
    :cond_5
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto/16 :goto_0
.end method


# virtual methods
.method public buildUpon()Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>(Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 198
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/server/Transform;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 199
    check-cast v0, Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 200
    .local v0, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 209
    .end local v0    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 214
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    .line 215
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 214
    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isCroppingTransform()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isFcrop64Transform()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFcrop64Transform()Z
    .locals 2

    .prologue
    const v1, 0xffff

    .line 192
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isResizeTransform()Z
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 220
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->original:Z

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "d"

    .line 237
    :goto_0
    return-object v1

    .line 223
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/Transform;->DEFAULT:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->crop:Z

    if-eqz v1, :cond_1

    .line 225
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "p"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    if-lez v1, :cond_2

    .line 228
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "w"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 230
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    if-lez v1, :cond_3

    .line 231
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "h"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 233
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/server/Transform;->isFcrop64Transform()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 234
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "fcrop64=1,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%04X%04X%04X%04X"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Left:I

    .line 235
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Top:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Right:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/server/Transform;->fcrop64Bottom:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 234
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method
