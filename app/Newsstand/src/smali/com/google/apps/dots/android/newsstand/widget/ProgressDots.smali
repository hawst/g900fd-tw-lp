.class public Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;
.super Landroid/widget/RelativeLayout;
.source "ProgressDots.java"


# static fields
.field private static final LOADING_DOTS_COUNT:I = 0x3


# instance fields
.field private final activePaint:Landroid/graphics/Paint;

.field private currentPageNum:I

.field private final dotContainer:Landroid/widget/LinearLayout;

.field private fillProgress:Z

.field private final inactivePaint:Landroid/graphics/Paint;

.field private final loadingDotsContainer:Landroid/widget/LinearLayout;

.field private pageCount:I

.field private pageCountIsFinal:Z

.field private segmentWidth:F

.field private smallTextSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v6, 0x11

    const/4 v1, -0x1

    const/high16 v5, 0x40800000    # 4.0f

    const/4 v4, 0x0

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    .line 30
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    .line 31
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->currentPageNum:I

    .line 44
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->inactivePaint:Landroid/graphics/Paint;

    .line 45
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->inactivePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->progress_dots_inactive:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->inactivePaint:Landroid/graphics/Paint;

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 47
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->inactivePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 48
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->activePaint:Landroid/graphics/Paint;

    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->activePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->progress_dots_active:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->activePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 52
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 54
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 55
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->progressDotContainer:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 57
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    .line 58
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 59
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 60
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->makeDot()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setBackgroundColor(I)V

    .line 65
    return-void
.end method

.method private makeDot()Landroid/widget/Button;
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x6

    const/4 v6, 0x0

    .line 117
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v3}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 118
    .local v1, "inactiveCircle":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    .line 119
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$color;->progress_dots_inactive:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 118
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 121
    .local v0, "dot":Landroid/widget/Button;
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 123
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 124
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->smallTextSize:F

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextSize(F)V

    .line 125
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 127
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 128
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v7, v6, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 129
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    return-object v0
.end method

.method private makeDotPulse(Landroid/view/View;)V
    .locals 3
    .param p1, "dot"    # Landroid/view/View;

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$anim;->pulse:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 135
    .local v0, "pulse":Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 136
    return-void
.end method

.method private resetView(I)V
    .locals 1
    .param p1, "pageCount"    # I

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->removeAllViews()V

    .line 77
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v0, 0x1

    .line 157
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 158
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_1

    move v7, v0

    .line 159
    .local v7, "rtl":Z
    :goto_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    if-le v1, v0, :cond_5

    .line 160
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    if-ge v6, v0, :cond_5

    .line 162
    if-eqz v7, :cond_2

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    sub-int/2addr v0, v6

    add-int/lit8 v8, v0, -0x1

    .line 163
    .local v8, "v":I
    :goto_2
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->fillProgress:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->currentPageNum:I

    if-gt v6, v0, :cond_4

    :cond_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->activePaint:Landroid/graphics/Paint;

    .line 165
    .local v5, "paint":Landroid/graphics/Paint;
    :goto_3
    int-to-float v0, v8

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    mul-float/2addr v1, v0

    add-int/lit8 v0, v8, 0x1

    int-to-float v0, v0

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    mul-float/2addr v3, v0

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 160
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 158
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "i":I
    .end local v7    # "rtl":Z
    .end local v8    # "v":I
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .restart local v6    # "i":I
    .restart local v7    # "rtl":Z
    :cond_2
    move v8, v6

    .line 162
    goto :goto_2

    .line 163
    .restart local v8    # "v":I
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->currentPageNum:I

    if-eq v6, v0, :cond_0

    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->inactivePaint:Landroid/graphics/Paint;

    goto :goto_3

    .line 168
    .end local v6    # "i":I
    .end local v8    # "v":I
    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 140
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 141
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    if-lez v1, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getMeasuredWidth()I

    move-result v0

    .line 143
    .local v0, "width":I
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->fillProgress:Z

    if-eqz v1, :cond_2

    .line 144
    int-to-float v1, v0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    .line 150
    .end local v0    # "width":I
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-eqz v1, :cond_1

    .line 151
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    float-to-int v1, v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    mul-int/2addr v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->setMeasuredDimension(II)V

    .line 153
    :cond_1
    return-void

    .line 146
    .restart local v0    # "width":I
    :cond_2
    int-to-float v1, v0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 147
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->progress_dots_max_segment_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 146
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    goto :goto_0
.end method

.method public setFillProgress(Z)V
    .locals 0
    .param p1, "fillProgress"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->fillProgress:Z

    .line 69
    return-void
.end method

.method public setPageCount(I)V
    .locals 2
    .param p1, "pageCount"    # I

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    rem-int/lit8 v1, p1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->makeDotPulse(Landroid/view/View;)V

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->resetView(I)V

    .line 103
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCount:I

    .line 105
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->requestLayout()V

    goto :goto_0
.end method

.method public setPageCountIsFinal(Z)V
    .locals 4
    .param p1, "isPageCountFinal"    # Z

    .prologue
    const/4 v3, -0x2

    .line 81
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-ne v1, p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 84
    :cond_0
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    .line 85
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-nez v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 88
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 89
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 90
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 92
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->loadingDotsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->removeView(Landroid/view/View;)V

    .line 93
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->dotContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPageNumber(I)V
    .locals 2
    .param p1, "pageNum"    # I

    .prologue
    .line 109
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->segmentWidth:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    if-ltz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->pageCountIsFinal:Z

    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->currentPageNum:I

    .line 113
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ProgressDots;->requestLayout()V

    goto :goto_0
.end method
