.class Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "SectionList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/SectionList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/SectionList;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/SectionList;
    .param p2, "primaryKey"    # I

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/SectionList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    return-void
.end method


# virtual methods
.method makeSectionCard(Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "sectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 86
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_ID:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getSectionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 87
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "sectionName":Ljava/lang/String;
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_NAME:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 89
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/SectionList;->DK_SECTION_SUMMARY:I

    invoke-virtual {v0, v2, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 90
    return-object v0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "sectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .prologue
    .line 81
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;->makeSectionCard(Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 82
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/SectionList$2;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V

    return-void
.end method
