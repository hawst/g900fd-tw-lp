.class Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioControlBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updateInfo(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 230
    if-nez p1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$mipmap;->ic_launcher_play_newsstand:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageResource(I)V

    .line 240
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->iconSource()Lcom/google/apps/dots/android/newsstand/icon/IconSource;

    move-result-object v0

    .line 234
    .local v0, "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    if-nez v1, :cond_1

    .line 235
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$mipmap;->ic_launcher_play_newsstand:I

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setImageResource(I)V

    goto :goto_0

    .line 237
    :cond_1
    check-cast v0, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .end local v0    # "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->apply(Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 227
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
