.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$400(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;->UP:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    iput-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapPoint:Lcom/google/apps/dots/android/newsstand/widget/BasePullDialog$SnapPoint;

    .line 191
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$4;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->snapToPoint(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method
