.class public Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsFragment;
.super Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;
.source "MyTopicsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_explore"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-object v0
.end method

.method protected getLibraryTag()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    return-object v0
.end method

.method protected getOfflineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleMyTopicsOfflineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getOnlineWarmWelcomeCard()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/topics/MyTopicsFragment;->updateHeaderCardsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->makeDismissibleMyTopicsOnlineCard(Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected getSubscriptionType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    return-object v0
.end method

.method protected getSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->curationSubscriptionsCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected isOfflineCardVisible()Z
    .locals 2

    .prologue
    .line 36
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_OFFLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z

    move-result v0

    return v0
.end method

.method protected isOnlineCardVisible()Z
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;->MY_TOPICS_ONLINE:Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowWarmWelcome(Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper$DismissibleCardType;)Z

    move-result v0

    return v0
.end method
