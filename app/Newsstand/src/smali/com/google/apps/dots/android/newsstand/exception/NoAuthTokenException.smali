.class public Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
.super Ljava/io/IOException;
.source "NoAuthTokenException.java"


# instance fields
.field private final promptIntent:Landroid/content/Intent;

.field private final requestingAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Landroid/content/Intent;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "optPromptIntent"    # Landroid/content/Intent;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->requestingAccount:Landroid/accounts/Account;

    .line 29
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->promptIntent:Landroid/content/Intent;

    .line 30
    return-void
.end method


# virtual methods
.method public getPromptIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->promptIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getRequestingAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->requestingAccount:Landroid/accounts/Account;

    return-object v0
.end method
