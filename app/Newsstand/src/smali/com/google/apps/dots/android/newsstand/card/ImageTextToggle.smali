.class public abstract Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ImageTextToggle.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected showingDefault:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->showingDefault:Z

    .line 28
    return-void
.end method


# virtual methods
.method public getAlternateAccessibleMessageId()I
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public getAlternateImageResourceId()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public getAlternateTextColor()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public getAlternateTextMessageId()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultAccessibleMessageId()I
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultImageResourceId()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultTextColor()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultTextMessageId()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getImageViewId()I
.end method

.method public abstract getNSTextViewId()I
.end method

.method public abstract getShowDefaultDataKey()I
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 39
    const-class v0, Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 41
    :cond_0
    return-void
.end method

.method public setShowDefault(Z)V
    .locals 14
    .param p1, "showingDefault"    # Z

    .prologue
    .line 61
    sget-object v9, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "setShowDefault as %b"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getImageViewId()I

    move-result v3

    .line 63
    .local v3, "imageViewId":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getNSTextViewId()I

    move-result v7

    .line 65
    .local v7, "textViewId":I
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->showingDefault:Z

    .line 67
    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getDefaultAccessibleMessageId()I

    move-result v1

    .line 69
    .local v1, "accessibleMessageId":I
    :goto_0
    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getDefaultImageResourceId()I

    move-result v8

    .line 70
    .local v8, "toggleIcon":I
    :goto_1
    if-eqz p1, :cond_6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getDefaultTextMessageId()I

    move-result v4

    .line 71
    .local v4, "messageId":I
    :goto_2
    if-eqz p1, :cond_7

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getDefaultTextColor()I

    move-result v6

    .line 72
    .local v6, "textColor":I
    :goto_3
    if-lez v3, :cond_0

    if-lez v8, :cond_0

    .line 73
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 74
    .local v2, "image":Landroid/widget/ImageView;
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    .end local v2    # "image":Landroid/widget/ImageView;
    :cond_0
    if-lez v7, :cond_2

    if-lez v4, :cond_2

    .line 77
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 78
    .local v5, "text":Lcom/google/apps/dots/android/newsstand/widget/NSTextView;
    if-eqz v6, :cond_1

    .line 79
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setTextColor(I)V

    .line 81
    :cond_1
    invoke-virtual {v5, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(I)V

    .line 83
    .end local v5    # "text":Lcom/google/apps/dots/android/newsstand/widget/NSTextView;
    :cond_2
    if-lez v1, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "accessibleMessage":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 87
    .end local v0    # "accessibleMessage":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->invalidate()V

    .line 88
    return-void

    .line 68
    .end local v1    # "accessibleMessageId":I
    .end local v4    # "messageId":I
    .end local v6    # "textColor":I
    .end local v8    # "toggleIcon":I
    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getAlternateAccessibleMessageId()I

    move-result v1

    goto :goto_0

    .line 69
    .restart local v1    # "accessibleMessageId":I
    :cond_5
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getAlternateImageResourceId()I

    move-result v8

    goto :goto_1

    .line 70
    .restart local v8    # "toggleIcon":I
    :cond_6
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getAlternateTextMessageId()I

    move-result v4

    goto :goto_2

    .line 71
    .restart local v4    # "messageId":I
    :cond_7
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getAlternateTextColor()I

    move-result v6

    goto :goto_3
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 46
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->getShowDefaultDataKey()I

    move-result v0

    .line 48
    .local v0, "showDefaultId":I
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/ImageTextToggle;->setShowDefault(Z)V

    .line 50
    .end local v0    # "showDefaultId":I
    :cond_0
    return-void
.end method
