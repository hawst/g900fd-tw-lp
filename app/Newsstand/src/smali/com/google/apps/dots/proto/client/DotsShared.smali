.class public interface abstract Lcom/google/apps/dots/proto/client/DotsShared;
.super Ljava/lang/Object;
.source "DotsShared.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Ad;,
        Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;,
        Lcom/google/apps/dots/proto/client/DotsShared$ScoredCountry;,
        Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;,
        Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;,
        Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$Action;,
        Lcom/google/apps/dots/proto/client/DotsShared$Lab;,
        Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;,
        Lcom/google/apps/dots/proto/client/DotsShared$Experiments;,
        Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;,
        Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;,
        Lcom/google/apps/dots/proto/client/DotsShared$Period;,
        Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;,
        Lcom/google/apps/dots/proto/client/DotsShared$RoleList;,
        Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;,
        Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;,
        Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;,
        Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;,
        Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;,
        Lcom/google/apps/dots/proto/client/DotsShared$Section;,
        Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$ArticleFeatures;,
        Lcom/google/apps/dots/proto/client/DotsShared$Post;,
        Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item;,
        Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;,
        Lcom/google/apps/dots/proto/client/DotsShared$Form;,
        Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;,
        Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;,
        Lcom/google/apps/dots/proto/client/DotsShared$DataSource;,
        Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;,
        Lcom/google/apps/dots/proto/client/DotsShared$Application;,
        Lcom/google/apps/dots/proto/client/DotsShared$GoogleSoldAdSettings;,
        Lcom/google/apps/dots/proto/client/DotsShared$AdContent;,
        Lcom/google/apps/dots/proto/client/DotsShared$AdFrequencySettings;,
        Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;,
        Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;,
        Lcom/google/apps/dots/proto/client/DotsShared$AdUnit;,
        Lcom/google/apps/dots/proto/client/DotsShared$Size;
    }
.end annotation
