.class Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "InEditionTranslateDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->translateEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$languageCode:Ljava/lang/String;

.field final synthetic val$translatedReadingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$languageCode:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$translatedReadingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$languageCode:Ljava/lang/String;

    invoke-static {v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->translateNewsSubscriptionIfSubscribed(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->optPostId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->access$000(Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 72
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$translatedReadingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 73
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;

    .line 77
    # getter for: Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->optPostId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;->access$000(Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$languageCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "translatedPostId":Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->val$translatedReadingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 80
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/translation/InEditionTranslateDialog$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
