.class public Lcom/google/apps/dots/android/newsstand/model/ObjectId;
.super Ljava/lang/Object;
.source "ObjectId.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final id:Ljava/lang/String;

.field private proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V
    .locals 1
    .param p1, "proto"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "Cannot create an ObjectId from a null proto."

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 38
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->id:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Cannot create an ObjectId from null or empty String."

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->id:Ljava/lang/String;

    .line 29
    :try_start_0
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;-><init>()V

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    return-void

    .line 25
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' is not a valid object ID"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "targetTranslationLanguage"    # Ljava/lang/String;

    .prologue
    .line 284
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 285
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->decodeTargetTranslationLanguage(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 286
    .local v0, "idAndLanguage":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    iget-object p0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local p0    # "id":Ljava/lang/String;
    check-cast p0, Ljava/lang/String;

    .line 292
    .end local v0    # "idAndLanguage":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local p0    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 289
    .restart local v0    # "idAndLanguage":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static addTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "targetTranslationLanguageCode"    # Ljava/lang/String;

    .prologue
    .line 260
    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 261
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Ljava/lang/String;)V

    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 262
    .local v1, "top":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    move-object v0, v1

    .line 267
    .local v0, "builder":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :goto_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v2

    if-nez v2, :cond_1

    .line 268
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->setTargetTranslationLanguage(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 269
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->id:Ljava/lang/String;

    return-object v2

    .line 260
    .end local v0    # "builder":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .end local v1    # "top":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 271
    .restart local v0    # "builder":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .restart local v1    # "top":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 272
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    goto :goto_1
.end method

.method static decodeTargetTranslationLanguage(Ljava/lang/String;)Landroid/util/Pair;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 235
    .local v2, "top":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    move-object v0, v2

    .line 240
    .local v0, "builder":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :goto_0
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasTargetTranslationLanguage()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 241
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getTargetTranslationLanguage()Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "targetTranslationLanguage":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->clearTargetTranslationLanguage()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .line 244
    new-instance v3, Landroid/util/Pair;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v4, v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->id:Ljava/lang/String;

    invoke-direct {v3, v4, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 252
    .end local v1    # "targetTranslationLanguage":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 247
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 248
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    goto :goto_0

    .line 252
    :cond_1
    new-instance v3, Landroid/util/Pair;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static find(ILcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 2
    .param p0, "type"    # I
    .param p1, "id"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .prologue
    const/4 v0, 0x0

    .line 101
    if-nez p1, :cond_1

    move-object p1, v0

    .line 110
    .end local p1    # "id":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_0
    :goto_0
    return-object p1

    .line 104
    .restart local p1    # "id":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_1
    iget v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    if-eq v1, p0, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->find(ILcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v0

    .line 110
    goto :goto_0
.end method

.method public static findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;I)Ljava/lang/String;
    .locals 1
    .param p0, "objectId"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .param p1, "type"    # I

    .prologue
    .line 154
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static findIdOfType(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;IZ)Ljava/lang/String;
    .locals 11
    .param p0, "objectId"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .param p1, "objectIdProtoType"    # I
    .param p2, "logErrors"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 190
    :try_start_0
    invoke-static {p1, p0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->find(ILcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v1

    .line 191
    .local v1, "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-nez v1, :cond_1

    .line 192
    if-eqz p2, :cond_0

    .line 193
    sget-object v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to find type %s from id %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_0
    :goto_0
    return-object v2

    .line 197
    .restart local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_1
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v3, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->getId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 198
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    if-eqz p2, :cond_0

    .line 200
    sget-object v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Error finding type %s from id %s, error: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object p0, v5, v9

    .line 201
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 200
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static findIdOfType(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "type"    # I

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static findIdOfType(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 11
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "type"    # I
    .param p2, "logErrors"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 171
    :try_start_0
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;-><init>()V

    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->find(ILcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v1

    .line 172
    .local v1, "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-nez v1, :cond_1

    .line 173
    if-eqz p2, :cond_0

    .line 174
    sget-object v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to find type %s from id %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p0, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v2, v3

    .line 183
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :goto_0
    return-object v2

    .line 178
    .restart local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_1
    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v2, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->getId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 179
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    if-eqz p2, :cond_2

    .line 181
    sget-object v2, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Error finding type %s from id %s, error: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object p0, v5, v9

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    move-object v2, v3

    .line 183
    goto :goto_0
.end method

.method private getObjectIdProtoTypeName(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;
    .locals 1
    .param p1, "proto"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .prologue
    .line 61
    iget v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->type:I

    packed-switch v0, :pswitch_data_0

    .line 93
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 63
    :pswitch_1
    const-string v0, "APP_FAMILY"

    goto :goto_0

    .line 65
    :pswitch_2
    const-string v0, "APPLICATION"

    goto :goto_0

    .line 67
    :pswitch_3
    const-string v0, "POST"

    goto :goto_0

    .line 69
    :pswitch_4
    const-string v0, "FORM"

    goto :goto_0

    .line 71
    :pswitch_5
    const-string v0, "SECTION"

    goto :goto_0

    .line 73
    :pswitch_6
    const-string v0, "ATTACHMENT"

    goto :goto_0

    .line 75
    :pswitch_7
    const-string v0, "ROLE"

    goto :goto_0

    .line 77
    :pswitch_8
    const-string v0, "SUBSCRIPTION"

    goto :goto_0

    .line 79
    :pswitch_9
    const-string v0, "PUBLISHER"

    goto :goto_0

    .line 81
    :pswitch_a
    const-string v0, "PRODUCT_PURCHASE"

    goto :goto_0

    .line 83
    :pswitch_b
    const-string v0, "PRODUCT"

    goto :goto_0

    .line 85
    :pswitch_c
    const-string v0, "TAX_TABLE"

    goto :goto_0

    .line 87
    :pswitch_d
    const-string v0, "DESIGNED_TEMPLATE"

    goto :goto_0

    .line 89
    :pswitch_e
    const-string v0, "CHECKOUT_ACCOUNT"

    goto :goto_0

    .line 91
    :pswitch_f
    const-string v0, "PRODUCT_PRICE_CHANGE"

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private toString(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;
    .locals 4
    .param p1, "proto"    # Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    .prologue
    .line 55
    const-string v1, "{type: %s, value: \'%s\', parent: %s}"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->getObjectIdProtoTypeName(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    .line 56
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x2

    .line 57
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->hasParentId()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;->getParentId()Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->toString(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    .line 55
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 57
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public static tryFindIdOfType(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "type"    # I

    .prologue
    .line 166
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static tryParseObjectId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 212
    :try_start_0
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;-><init>()V

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 216
    :goto_0
    return-object v1

    .line 213
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    move-object v1, v2

    .line 214
    goto :goto_0

    .line 215
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move-object v1, v2

    .line 216
    goto :goto_0
.end method


# virtual methods
.method public findAncestorOfType(I)Lcom/google/apps/dots/android/newsstand/model/ObjectId;
    .locals 11
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 121
    :try_start_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->find(ILcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    move-result-object v1

    .line 122
    .local v1, "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    if-nez v1, :cond_0

    .line 123
    sget-object v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to find ancestor of type %s from proto %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->toString(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :goto_0
    return-object v2

    .line 126
    .restart local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;

    invoke-direct {v3, v1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;-><init>(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_0

    .line 127
    .end local v1    # "foundProto":Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Error finding ancestor of type %s from proto %s, error: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->toString(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    .line 129
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 128
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->proto:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->toString(Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
