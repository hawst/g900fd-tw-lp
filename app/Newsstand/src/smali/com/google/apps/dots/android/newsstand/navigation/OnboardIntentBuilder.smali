.class public Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "OnboardIntentBuilder.java"


# instance fields
.field private accountNameOverride:Ljava/lang/String;

.field private sequenceType:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->sequenceType:I

    .line 29
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->accountNameOverride:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 50
    const-class v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 51
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "OnboardIntentBuilder_sequenceType"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->sequenceType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->accountNameOverride:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->accountNameOverride:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    :cond_0
    return-object v0
.end method

.method public fullSequence()Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->sequenceType:I

    .line 37
    return-object p0
.end method

.method public quizSequenceOnly()Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->sequenceType:I

    .line 45
    return-object p0
.end method
