.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "MagazineReadingFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReadingTransitionDelegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1276
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;

    .prologue
    .line 1276
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;
    .locals 3
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 1314
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MagazineReadingFragment_transitionImageAttachment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1315
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    new-instance v1, Landroid/transition/ChangeBounds;

    invoke-direct {v1}, Landroid/transition/ChangeBounds;-><init>()V

    .line 1316
    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    new-instance v1, Landroid/transition/ChangeImageTransform;

    invoke-direct {v1}, Landroid/transition/ChangeImageTransform;-><init>()V

    .line 1317
    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 1320
    :goto_0
    return-object v0

    .line 1319
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Shared element transition was not created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1320
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 1275
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;->createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 1346
    const/4 v0, 0x2

    return v0
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1327
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setVisibility(I)V

    .line 1328
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAlpha(F)V

    .line 1329
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1275
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 1336
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate$2;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1342
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 1275
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;->onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 6
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 1281
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 1282
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1284
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "MagazineReadingFragment_transitionImageAttachment"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1285
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->magazine_reading_activity_hero:I

    .line 1286
    invoke-virtual {p1, v5}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setTransitionName(Ljava/lang/String;)V

    .line 1288
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1290
    const-string v4, "MagazineReadingFragment_transitionImageAttachment"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1291
    .local v1, "attachmentId":Ljava/lang/String;
    const-string v4, "MagazineReadingFragment_transitionImageTransform"

    .line 1292
    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/server/Transform;->parse(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v3

    .line 1296
    .local v3, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setVisibility(I)V

    .line 1297
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAlpha(F)V

    .line 1298
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 1299
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate$1;

    invoke-direct {v5, p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenImageSet(Ljava/lang/Runnable;)V

    .line 1307
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->postponeEnterTransition()V

    .line 1309
    .end local v1    # "attachmentId":Ljava/lang/String;
    .end local v3    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_0
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1275
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    return-void
.end method
