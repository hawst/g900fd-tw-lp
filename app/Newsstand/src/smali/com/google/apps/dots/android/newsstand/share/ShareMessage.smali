.class public Lcom/google/apps/dots/android/newsstand/share/ShareMessage;
.super Ljava/lang/Object;
.source "ShareMessage.java"


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final htmlMessage:Ljava/lang/String;

.field private final longMessage:Ljava/lang/String;

.field private final shortMessage:Ljava/lang/String;

.field private final subject:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->activity:Landroid/app/Activity;

    .line 33
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->subject:Ljava/lang/String;

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_edition_format_html:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->shortUrl:Ljava/lang/String;

    .line 35
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildHtmlFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->htmlMessage:Ljava/lang/String;

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_edition_long_format_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->shortUrl:Ljava/lang/String;

    .line 37
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->longMessage:Ljava/lang/String;

    .line 38
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_edition_short_format_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->shortMessage:Ljava/lang/String;

    .line 39
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->longUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->url:Ljava/lang/String;

    .line 40
    return-void

    .line 35
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->activity:Landroid/app/Activity;

    .line 75
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->subject:Ljava/lang/String;

    .line 76
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_link_title_html:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->url:Ljava/lang/String;

    .line 77
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildHtmlFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->htmlMessage:Ljava/lang/String;

    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_link_title_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->longMessage:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->longMessage:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->shortMessage:Ljava/lang/String;

    .line 80
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->url:Ljava/lang/String;

    .line 81
    return-void

    .line 77
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->activity:Landroid/app/Activity;

    .line 47
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->editionName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->subject:Ljava/lang/String;

    .line 49
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_format_html_no_edition_name:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->shortUrl:Ljava/lang/String;

    .line 51
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildHtmlFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->htmlMessage:Ljava/lang/String;

    .line 52
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_long_format_text_no_edition_name:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->shortUrl:Ljava/lang/String;

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->longMessage:Ljava/lang/String;

    .line 55
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_short_format_text_no_edition_name:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->shortMessage:Ljava/lang/String;

    .line 67
    :goto_2
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->longUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->url:Ljava/lang/String;

    .line 68
    return-void

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 58
    :cond_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_subject_format:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->editionName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->subject:Ljava/lang/String;

    .line 60
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_format_html:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->shortUrl:Ljava/lang/String;

    .line 61
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildHtmlFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->htmlMessage:Ljava/lang/String;

    .line 62
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_long_format_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->shortUrl:Ljava/lang/String;

    .line 63
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->longMessage:Ljava/lang/String;

    .line 64
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_article_short_format_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->shortMessage:Ljava/lang/String;

    goto :goto_2

    .line 61
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 63
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method private buildFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shareUrl"    # Ljava/lang/String;

    .prologue
    .line 168
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_source_text:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildHtmlFooter(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shareUrl"    # Ljava/lang/String;

    .prologue
    .line 164
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->share_source_html:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;I)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;
    .param p3, "resourceId"    # I

    .prologue
    .line 135
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionDescription:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/google/common/base/CharMatcher;->JAVA_LETTER_OR_DIGIT:Lcom/google/common/base/CharMatcher;

    .line 137
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/common/base/CharMatcher;->matches(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->shortUrl:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;->editionName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;I)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;
    .param p3, "resourceId"    # I

    .prologue
    .line 155
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/google/common/base/CharMatcher;->JAVA_LETTER_OR_DIGIT:Lcom/google/common/base/CharMatcher;

    .line 157
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/common/base/CharMatcher;->matches(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->url:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private buildMessage(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;I)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;
    .param p3, "resourceId"    # I

    .prologue
    .line 145
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->postTitle:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/google/common/base/CharMatcher;->JAVA_LETTER_OR_DIGIT:Lcom/google/common/base/CharMatcher;

    .line 147
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/common/base/CharMatcher;->matches(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    :cond_0
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->shortUrl:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const/4 v2, 0x2

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->snippet:Ljava/lang/String;

    .line 151
    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;->editionName:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 150
    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static forParams(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Lcom/google/apps/dots/android/newsstand/share/ShareMessage;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "params"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .prologue
    .line 175
    sget-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage$1;->$SwitchMap$com$google$apps$dots$android$newsstand$share$ShareParams$Type:[I

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->type:Lcom/google/apps/dots/android/newsstand/share/ShareParams$Type;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized ShareDialogParams type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :pswitch_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;

    .end local p1    # "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForEdition;)V

    .line 181
    :goto_0
    return-object v0

    .line 179
    .restart local p1    # "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    :pswitch_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;

    .end local p1    # "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForPost;)V

    goto :goto_0

    .line 181
    .restart local p1    # "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    :pswitch_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;

    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    .end local p1    # "params":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;)V

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->htmlMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getShort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->shortMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public toIntentBuilder()Landroid/support/v4/app/ShareCompat$IntentBuilder;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->activity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->from(Landroid/app/Activity;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    const-string v1, "text/plain"

    .line 128
    invoke-virtual {v0, v1}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setType(Ljava/lang/String;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    .line 129
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->getShort()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setText(Ljava/lang/CharSequence;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    .line 130
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->getHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setHtmlText(Ljava/lang/String;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMessage;->getSubject()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->setSubject(Ljava/lang/String;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v0

    return-object v0
.end method
