.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;
.super Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;
.source "SettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupShowNotificationsPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;
    .param p2, "checkBoxPreference"    # Landroid/preference/CheckBoxPreference;
    .param p3, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;-><init>(Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getCurrentValue()Z
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getNotificationMode()Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_ENABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setNewValue(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_ENABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setNotificationMode(Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;)V

    .line 155
    return-void

    .line 153
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_DISABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    goto :goto_0
.end method
