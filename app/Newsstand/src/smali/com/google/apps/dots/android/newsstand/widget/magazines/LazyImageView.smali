.class public abstract Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
.super Landroid/view/View;
.source "LazyImageView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;
    }
.end annotation


# static fields
.field public static final USE_16_BIT_IMAGES_MEMORY_CLASS_THRESHOLD:I = 0x40

.field static final drawnSizePx:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private alpha:I

.field protected final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field protected final bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field private imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

.field private isDestroyed:Z

.field private isLaidOut:Z

.field private isLoaded:Z

.field protected final nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

.field protected final nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

.field private final paint:Landroid/graphics/Paint;

.field private final tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

.field private final tiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->drawnSizePx:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    .line 82
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    .line 87
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .line 88
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->alpha:I

    .line 92
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .line 93
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 94
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->paint:Landroid/graphics/Paint;

    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 98
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->tagHandlesAlpha(Landroid/view/View;)V

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isDestroyed:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    return-object v0
.end method

.method private postOnLoadComplete()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 132
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    .prologue
    .line 147
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->alpha:I

    div-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    return v0
.end method

.method protected abstract getBitmap(IZ)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;
        }
    .end annotation
.end method

.method getBitmapInfo()Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    return-object v0
.end method

.method public getContentArea()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getContentArea()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method getTiles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    return-object v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isDestroyed:Z

    return v0
.end method

.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isLoaded:Z

    return v0
.end method

.method isReadyForTileUpdate()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isLaidOut:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isDestroyed:Z

    .line 215
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->requestUpdate()V

    .line 216
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->onDestroyed(Landroid/view/View;)V

    .line 217
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->alpha:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 234
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isLaidOut:Z

    .line 222
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->requestUpdate()V

    .line 223
    return-void
.end method

.method protected onLoadComplete()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->onLoadComplete()V

    .line 187
    return-void
.end method

.method protected onSetAlpha(I)Z
    .locals 2
    .param p1, "alpha"    # I

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 137
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 140
    :cond_0
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->alpha:I

    .line 141
    const/4 v1, 0x1

    return v1
.end method

.method public onTransformChanged()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->requestUpdate()V

    .line 183
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 227
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tileUpdater:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->requestUpdate()V

    .line 229
    return-void
.end method

.method protected abstract releaseBitmap(Landroid/graphics/Bitmap;I)V
.end method

.method public setContentArea(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->setContentArea(FFFF)V

    .line 192
    return-void
.end method

.method protected setImageInfo(Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V
    .locals 2
    .param p1, "imageInfo"    # Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method setTiles(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "newTiles":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 166
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 168
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isDestroyed:Z

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->tiles:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 171
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->invalidate()V

    .line 173
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isLoaded:Z

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isLoaded:Z

    .line 175
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->postOnLoadComplete()V

    .line 178
    :cond_0
    return-void
.end method
