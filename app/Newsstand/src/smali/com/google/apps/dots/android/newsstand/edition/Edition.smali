.class public abstract Lcom/google/apps/dots/android/newsstand/edition/Edition;
.super Ljava/lang/Object;
.source "Edition.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

.field public static final SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;


# instance fields
.field protected editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

.field private titleHint:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 48
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 49
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    .line 333
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/Edition$3;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition$3;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 111
    return-void
.end method

.method public static createEditionByAppFamily(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p0, "appFamilyId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 313
    .local v0, "userToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/Edition$2;

    invoke-direct {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition$2;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public static curatedTopicEdition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;
    .locals 1
    .param p0, "appFamilyId"    # Ljava/lang/String;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "leadCurationClientEntityId"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static fromClientEntity(Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 4
    .param p0, "clientEntity"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hasCurationAppFamilyId()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hasCurationAppId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getCurationAppFamilyId()Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getCurationAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getId()Ljava/lang/String;

    move-result-object v3

    .line 145
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->curatedTopicEdition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "editionProto"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 139
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 120
    :pswitch_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    goto :goto_0

    .line 122
    :pswitch_2
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    goto :goto_0

    .line 124
    :pswitch_3
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 126
    :pswitch_4
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 128
    :pswitch_5
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 130
    :pswitch_6
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 132
    :pswitch_7
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 134
    :pswitch_8
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 136
    :pswitch_9
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method

.method public static fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 6
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v0

    .line 88
    .local v0, "appType":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 102
    :pswitch_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Received appType UNKNOWN for appId: %s, name: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 103
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 102
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 91
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v1

    goto :goto_0

    .line 93
    :pswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getLeadCurationClientEntityId()Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-static {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->curatedTopicEdition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    move-result-object v1

    goto :goto_0

    .line 96
    :pswitch_3
    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    goto :goto_0

    .line 98
    :pswitch_4
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;

    .prologue
    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static relatedPostsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;
    .locals 1
    .param p0, "postId"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static searchPostsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    .locals 1
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    .locals 1
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 61
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public actionBarColor(Landroid/content/Context;Z)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subscribed"    # Z

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public abstract cardList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/DataList;
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method public editionSummary(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 209
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 210
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method

.method public editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public abstract editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 285
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract getAppId()Ljava/lang/String;
.end method

.method public getTitleHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->titleHint:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 183
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    return-object v0
.end method

.method public goUpHierarchy(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition$4;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 264
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :pswitch_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 249
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 250
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 261
    :pswitch_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 291
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;
.end method

.method public abstract readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
.end method

.method public abstract readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;
.end method

.method public abstract refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end method

.method public setTitleHint(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleHint"    # Ljava/lang/String;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->titleHint:Ljava/lang/String;

    .line 305
    return-void
.end method

.method public abstract showKeepOnDeviceUi()Z
.end method

.method public abstract showOnDeviceOnlyUi()Z
.end method

.method public supportsBookmarking()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method public abstract supportsReadStates()Z
.end method

.method public abstract supportsRefresh()Z
.end method

.method public abstract supportsSubscription()Z
.end method

.method public toProto()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 275
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 0
    .param p1, "page"    # I

    .prologue
    .line 301
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 355
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->titleHint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 356
    return-void
.end method
