.class public Lcom/google/apps/dots/android/newsstand/sync/Notifications;
.super Ljava/lang/Object;
.source "Notifications.java"


# static fields
.field private static final appIdsNotifying:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static requestCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->requestCode:I

    .line 38
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    return-void
.end method

.method private static baseEditionNotificationBuilder(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentIntent"    # Landroid/app/PendingIntent;
    .param p2, "contentTitle"    # Ljava/lang/String;
    .param p3, "contentText"    # Ljava/lang/String;

    .prologue
    .line 131
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v1, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_notification:I

    .line 132
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 133
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const-string v2, "promo"

    .line 134
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 135
    invoke-virtual {v1, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 136
    invoke-virtual {v1, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 137
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 138
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {p3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 139
    invoke-virtual {v0, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 141
    :cond_0
    return-object v0
.end method

.method public static clearAllNotifications(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 146
    const-string v1, "notification"

    .line 147
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 148
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 149
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 150
    return-void
.end method

.method public static clearAppFamilyFromNotifications(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appFamilyId"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "anyAppIdsRemoved":Z
    sget-object v4, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 158
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 159
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 160
    .local v1, "appIdAndEditionSummary":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v4, v4, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    const/4 v0, 0x1

    .line 162
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 165
    .end local v1    # "appIdAndEditionSummary":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;>;"
    :cond_1
    if-eqz v0, :cond_2

    .line 166
    const-string v4, "notification"

    .line 167
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 168
    .local v3, "notificationManager":Landroid/app/NotificationManager;
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    .line 169
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->displayNewAppsNotification(Landroid/content/Context;)V

    .line 171
    .end local v3    # "notificationManager":Landroid/app/NotificationManager;
    :cond_2
    return-void
.end method

.method public static clearAppFromNotifications(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 177
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const-string v1, "notification"

    .line 179
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 180
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 182
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->displayNewAppsNotification(Landroid/content/Context;)V

    .line 184
    .end local v0    # "notificationManager":Landroid/app/NotificationManager;
    :cond_0
    return-void
.end method

.method private static displayNewAppsNotification(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    :goto_0
    return-void

    .line 64
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 66
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 67
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-static {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->notify(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    goto :goto_0

    .line 71
    .end local v1    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    :cond_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    .line 72
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v0, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 73
    .local v0, "consensusAppFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 74
    .restart local v1    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v4, v4, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 78
    const/4 v0, 0x0

    .line 82
    .end local v1    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    :cond_3
    const/4 v2, 0x0

    invoke-static {p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->notify(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    goto :goto_0
.end method

.method public static notificationsEnabled()Z
    .locals 2

    .prologue
    .line 41
    sget-object v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;->NOTIFICATIONS_ENABLED:Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    .line 42
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getNotificationMode()Lcom/google/apps/dots/android/newsstand/preference/Preferences$NotificationMode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static notify(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 94
    sget v6, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->requestCode:I

    add-int/lit8 v7, v6, 0x1

    sput v7, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->requestCode:I

    if-eqz p2, :cond_0

    .line 97
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->nonActivityMake(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v6

    iget-object v8, p2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 98
    invoke-virtual {v6, v8}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v6

    .line 99
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v6

    .line 103
    :goto_0
    const/high16 v8, 0x48000000    # 131072.0f

    .line 94
    invoke-static {p0, v7, v6, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 106
    .local v5, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->new_multi_magazine_notification_content:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "defaultContentTitle":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 108
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "contentTitle":Ljava/lang/String;
    :goto_1
    if-eqz p1, :cond_2

    .line 110
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->new_magazine_downloadable_notification:I

    new-array v8, v11, [Ljava/lang/Object;

    .line 111
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    .line 110
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "contentText":Ljava/lang/String;
    :goto_2
    invoke-static {p0, v5, v1, v0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->baseEditionNotificationBuilder(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 117
    invoke-virtual {v6, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    const/4 v7, 0x0

    .line 119
    invoke-static {p0, v5, v2, v7}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->baseEditionNotificationBuilder(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    .line 120
    invoke-virtual {v7, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    .line 121
    invoke-virtual {v7}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    .line 118
    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 122
    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    .line 124
    .local v3, "notification":Landroid/app/Notification;
    const-string v6, "notification"

    .line 125
    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 126
    .local v4, "notificationManager":Landroid/app/NotificationManager;
    const/4 v6, 0x2

    invoke-virtual {v4, v6, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 127
    return-void

    .line 100
    .end local v0    # "contentText":Ljava/lang/String;
    .end local v1    # "contentTitle":Ljava/lang/String;
    .end local v2    # "defaultContentTitle":Ljava/lang/String;
    .end local v3    # "notification":Landroid/app/Notification;
    .end local v4    # "notificationManager":Landroid/app/NotificationManager;
    .end local v5    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->nonActivityMake(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v6

    sget-object v8, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 101
    invoke-virtual {v6, v8}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v6

    sget-object v8, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 102
    invoke-virtual {v6, v8}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v6

    .line 103
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v6

    goto :goto_0

    .restart local v2    # "defaultContentTitle":Ljava/lang/String;
    .restart local v5    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_1
    move-object v1, v2

    .line 108
    goto :goto_1

    .line 112
    .restart local v1    # "contentTitle":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->new_multi_magazine_downloadable_notification_title:I

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    .line 113
    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    .line 112
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static notifyForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->notificationsEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v0, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 51
    .local v0, "appId":Ljava/lang/String;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->appIdsNotifying:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->displayNewAppsNotification(Landroid/content/Context;)V

    goto :goto_0
.end method
