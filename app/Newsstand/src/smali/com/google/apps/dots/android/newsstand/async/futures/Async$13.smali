.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeTimerFuture(JLjava/util/concurrent/Executor;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$durationMs:J

.field final synthetic val$executor:Ljava/util/concurrent/Executor;

.field final synthetic val$failAtEnd:Z

.field final synthetic val$result:Lcom/google/common/util/concurrent/SettableFuture;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;ZLcom/google/common/util/concurrent/SettableFuture;J)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$executor:Ljava/util/concurrent/Executor;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$failAtEnd:Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$result:Lcom/google/common/util/concurrent/SettableFuture;

    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$durationMs:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 432
    return-void
.end method
