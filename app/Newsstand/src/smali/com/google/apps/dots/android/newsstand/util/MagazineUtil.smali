.class public Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;
.super Ljava/lang/Object;
.source "MagazineUtil.java"


# direct methods
.method public static getDefaultToLiteMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-eq v0, v1, :cond_0

    .line 36
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDefaultToLiteModeForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .prologue
    const/4 v1, 0x1

    .line 21
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 23
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedVersion(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v0

    .line 24
    .local v0, "pinnedVersion":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 28
    .end local v0    # "pinnedVersion":Ljava/lang/Integer;
    :goto_0
    return v1

    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/MagazineUtil;->getDefaultToLiteMode(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0
.end method
