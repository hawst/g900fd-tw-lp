.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;
.super Ljava/lang/Object;
.source "MediaView.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayerDataSource()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 255
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 259
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;Landroid/content/res/AssetFileDescriptor;)V

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListeningExecutorService;->submit(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 273
    const/4 v1, 0x0

    return-object v1
.end method
