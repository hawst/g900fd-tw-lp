.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;
.super Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
.source "MagazineReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/android/libraries/bind/view/ViewHeap;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p3, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    return-void
.end method


# virtual methods
.method public getView(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "logicalPosition"    # I
    .param p3, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->createArticleWidget(ILcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    invoke-static {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1400(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;ILcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyedView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onPagerDestroyedView(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1500(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/view/View;)V

    .line 269
    return-void
.end method
