.class public Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "SavedFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x0

    const-string v1, "SavedFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->saved_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->onPulledToRefresh()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private onPulledToRefresh()V
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->getFragment()Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->onPulledToRefresh(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 68
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 67
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 70
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V

    .line 81
    return-void
.end method


# virtual methods
.method protected getFragment()Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 109
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 91
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_bookmarks"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 44
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->setupHeaderListLayout()V

    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->setupActionBar(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 47
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;)V
    .locals 0
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/saved/SavedFragmentState;

    .prologue
    .line 96
    return-void
.end method
