.class public Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;
.super Ljava/lang/Object;
.source "ReadStateUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/accounts/Account;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Float;
    .param p4, "x4"    # Z

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    return-void
.end method

.method public static markPostAsMagazineRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;)V
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "pageFraction"    # Ljava/lang/Float;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    .line 57
    return-void
.end method

.method public static markPostAsMeteredRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3, v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    .line 107
    if-eq p1, p2, :cond_0

    .line 108
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3, v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    .line 111
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredReadEvent;

    invoke-direct {v0, p2, p3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredReadEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MeteredReadEvent;->track(Z)V

    .line 112
    return-void
.end method

.method public static markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "optOwningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "postId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3, v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    .line 74
    if-nez p2, :cond_1

    .line 76
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;

    invoke-direct {v1, p1, p0, p3}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 75
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-static {p1, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3, v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V

    goto :goto_0
.end method

.method private static markPostAsRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Z)V
    .locals 9
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "pageFraction"    # Ljava/lang/Float;
    .param p4, "isMetered"    # Z

    .prologue
    const/4 v8, 0x0

    .line 28
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "markPostAsRead: %s %s"

    const/4 v4, 0x2

    new-array v7, v4, [Ljava/lang/Object;

    if-eqz p3, :cond_0

    move-object v4, p3

    :goto_0
    aput-object v4, v7, v8

    const/4 v4, 0x1

    aput-object p2, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    if-nez p1, :cond_1

    .line 52
    :goto_1
    return-void

    .line 28
    :cond_0
    const-string v4, ""

    goto :goto_0

    .line 35
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getAppReadStatesUrl(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "appReadStatesUri":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getPostReadStateUrl(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "url":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 39
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "pageFraction"

    .line 40
    invoke-virtual {p3}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    .line 41
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 43
    :cond_2
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    .line 44
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 45
    invoke-virtual {v4, v8}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 48
    invoke-static {p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makePostReadStateHint(Ljava/lang/String;Ljava/lang/Float;Z)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v5

    .line 47
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 49
    .local v0, "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v5, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    if-eqz p4, :cond_3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 50
    :goto_2
    invoke-virtual {v5, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 51
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v4

    invoke-virtual {v4, p0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 49
    .end local v2    # "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    :cond_3
    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->BATCH:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    goto :goto_2
.end method
