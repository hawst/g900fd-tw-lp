.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->updateActionBarTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 608
    if-eqz p1, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 610
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v1

    .line 609
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 612
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 605
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$10;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
