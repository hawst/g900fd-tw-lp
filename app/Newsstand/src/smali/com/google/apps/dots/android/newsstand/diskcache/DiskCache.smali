.class public final Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
.super Ljava/lang/Object;
.source "DiskCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;,
        Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;,
        Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;,
        Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    }
.end annotation


# static fields
.field private static final EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;",
            ">;"
        }
    .end annotation
.end field

.field private isOpen:Z

.field private final janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

.field private final lockSpace:Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private final regionMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;",
            ">;"
        }
    .end annotation
.end field

.field private rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

.field private final stateLock:Ljava/lang/Object;

.field private final upgradeFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 41
    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2
    .param p1, "janitorOptions"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p2, "upgradeFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 98
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->LEX:Ljava/util/Comparator;

    .line 101
    invoke-static {v0}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcom/google/common/collect/Maps;->newTreeMap(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    .line 123
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->LEX:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->lockSpace:Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;

    .line 165
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    .line 175
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .line 176
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->upgradeFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 177
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .locals 9
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "exclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 408
    const/4 v5, 0x0

    .line 409
    .local v5, "regionLock":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    const/4 v3, 0x0

    .line 411
    .local v3, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v8

    .line 412
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->open(Z)V

    .line 413
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLower(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    .line 414
    .local v0, "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v6

    .line 415
    .local v6, "ub":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 422
    :goto_0
    :try_start_1
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->lockSpace:Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;

    invoke-virtual {v7, v0, v6, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->lockHalfInterval(Ljava/lang/Object;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    move-result-object v5

    .line 424
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 427
    :try_start_2
    iget-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    if-nez v7, :cond_1

    .line 428
    new-instance v7, Ljava/io/IOException;

    invoke-direct {v7}, Ljava/io/IOException;-><init>()V

    throw v7

    .line 442
    :catchall_0
    move-exception v7

    :goto_1
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 446
    :catchall_1
    move-exception v7

    if-nez v3, :cond_0

    if-eqz v5, :cond_0

    .line 447
    invoke-interface {v5}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    :cond_0
    throw v7

    .line 415
    .end local v0    # "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v6    # "ub":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :catchall_2
    move-exception v7

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v7

    .line 430
    .restart local v0    # "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .restart local v6    # "ub":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :cond_1
    :try_start_5
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLower(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v1

    .line 431
    .local v1, "newLb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v2

    .line 432
    .local v2, "newUb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {v6, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 434
    new-instance v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v7, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-direct {v4, v7, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 435
    .end local v3    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .local v4, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_6
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 446
    if-nez v4, :cond_2

    if-eqz v5, :cond_2

    .line 447
    invoke-interface {v5}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 450
    :cond_2
    return-object v4

    .line 438
    .end local v4    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .restart local v3    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :cond_3
    :try_start_7
    invoke-interface {v5}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 439
    move-object v0, v1

    .line 440
    move-object v6, v2

    .line 442
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .end local v3    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .restart local v4    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :catchall_3
    move-exception v7

    move-object v3, v4

    .end local v4    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .restart local v3    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    goto :goto_1
.end method

.method private getLower(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 2
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 383
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    monitor-exit v1

    return-object v0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 2
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 392
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 393
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    monitor-exit v1

    return-object v0

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private makeStoreFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    .locals 6
    .param p1, "lb"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 283
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "filenameEncodedKey":Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .line 285
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileManifestFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 286
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileBlobsFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 287
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFilePermBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 288
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileCacheBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    return-object v1
.end method

.method private mapStoreFiles()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v12

    .line 310
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v11}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v11

    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 312
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 313
    .local v6, "files":[Ljava/io/File;
    if-nez v6, :cond_0

    .line 314
    new-instance v11, Ljava/io/IOException;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v13, v13, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, 0xc

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v15, "Cannot list "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 344
    .end local v6    # "files":[Ljava/io/File;
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 317
    .restart local v6    # "files":[Ljava/io/File;
    :cond_0
    :try_start_1
    array-length v11, v6

    invoke-static {v11}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v1

    .line 318
    .local v1, "activeFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/io/File;>;"
    array-length v13, v6

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v13, :cond_2

    aget-object v4, v6, v11

    .line 319
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 320
    .local v7, "fullName":Ljava/lang/String;
    const-string v14, "s_"

    invoke-virtual {v7, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    const-string v14, ".m"

    invoke-virtual {v7, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 321
    const-string v14, "s_"

    .line 322
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v15

    const-string v16, ".m"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    sub-int v15, v15, v16

    .line 321
    invoke-virtual {v7, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 323
    .local v5, "filenameEncodedKey":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameDecode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v8

    .line 324
    .local v8, "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    if-eqz v8, :cond_1

    .line 325
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileBlobsFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 326
    .local v2, "blobsFile":Ljava/io/File;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFilePermBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    .line 327
    .local v9, "permBlobsDir":Ljava/io/File;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileCacheBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 328
    .local v3, "cacheBlobsDir":Ljava/io/File;
    sget-object v14, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v15, "Mapping manifest file: %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v4, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    new-instance v10, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-direct {v10, v4, v2, v9, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    .line 331
    .local v10, "storeFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    new-instance v15, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v8, v10}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V

    invoke-virtual {v14, v8, v15}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 333
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 318
    .end local v2    # "blobsFile":Ljava/io/File;
    .end local v3    # "cacheBlobsDir":Ljava/io/File;
    .end local v5    # "filenameEncodedKey":Ljava/lang/String;
    .end local v8    # "lb":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .end local v9    # "permBlobsDir":Ljava/io/File;
    .end local v10    # "storeFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 339
    .end local v4    # "file":Ljava/io/File;
    .end local v7    # "fullName":Ljava/lang/String;
    :cond_2
    array-length v13, v6

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v13, :cond_4

    aget-object v4, v6, v11

    .line 340
    .restart local v4    # "file":Ljava/io/File;
    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 341
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 339
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 344
    .end local v4    # "file":Ljava/io/File;
    :cond_4
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    return-void
.end method

.method private storeFileBlobsFile(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p1, "filenameEncodedKey"    # Ljava/lang/String;

    .prologue
    .line 296
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    const-string v2, "s_"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".b"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private storeFileCacheBlobsDir(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p1, "filenameEncodedKey"    # Ljava/lang/String;

    .prologue
    .line 304
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    const-string v2, "s_"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".d"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private storeFileManifestFile(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p1, "filenameEncodedKey"    # Ljava/lang/String;

    .prologue
    .line 292
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    const-string v2, "s_"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".m"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private storeFilePermBlobsDir(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p1, "filenameEncodedKey"    # Ljava/lang/String;

    .prologue
    .line 300
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    const-string v2, "s_"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".d"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method addAvailabilityListener(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;)V
    .locals 2
    .param p1, "availabilityListener"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .prologue
    .line 195
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 196
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    monitor-exit v1

    .line 198
    return-void

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close(Z)V
    .locals 11
    .param p1, "writeback"    # Z

    .prologue
    const/4 v5, 0x1

    .line 349
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->lockSpace:Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/OrderedLockSpace;->lockAll(Z)Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    move-result-object v2

    .line 350
    .local v2, "regionLock":Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v5

    .line 352
    :try_start_0
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 370
    :try_start_1
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 377
    :goto_0
    return-void

    .line 355
    :cond_0
    :try_start_2
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Closing %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p0, v7, v8

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 358
    .local v3, "storeEntry":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;
    :try_start_3
    iget-object v6, v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-virtual {v6, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->close(Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 359
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    sget-object v6, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Trouble closing %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    aput-object v10, v8, v9

    invoke-virtual {v6, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    iget-object v6, v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 370
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "storeEntry":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;
    :catchall_0
    move-exception v4

    :try_start_5
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    throw v4

    .line 376
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    .line 365
    :cond_1
    :try_start_6
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->clear()V

    .line 366
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 370
    :try_start_7
    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 373
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .line 374
    .local v0, "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;->onClosed()V

    goto :goto_2

    .line 376
    .end local v0    # "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    :cond_2
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0
.end method

.method public contains(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 454
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 456
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 457
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 458
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->contains(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 460
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    return v1

    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    throw v1
.end method

.method public delete(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 624
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 626
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 627
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 628
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 630
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 631
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    return v1

    .line 630
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 631
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    throw v1
.end method

.method public deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 639
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 641
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 645
    return-void

    .line 643
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    throw v1
.end method

.method public getAssetFileDescriptor(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Landroid/content/res/AssetFileDescriptor;
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 543
    invoke-direct {p0, p1, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v2

    .line 544
    .local v2, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    const/4 v1, 0x0

    .line 546
    .local v1, "deleteRegion":Z
    :try_start_0
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 547
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 548
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getAssetFileDescriptor(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 554
    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 557
    if-eqz v1, :cond_0

    .line 558
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "File not found for key %s; likely corruption. Deleting region."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    :cond_0
    return-object v3

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    :try_start_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 551
    const/4 v1, 0x1

    .line 552
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    .end local v0    # "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    :catchall_0
    move-exception v3

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 557
    if-eqz v1, :cond_1

    .line 558
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "File not found for key %s; likely corruption. Deleting region."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    :cond_1
    throw v3
.end method

.method public getDiskBlob(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .locals 8
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 515
    invoke-direct {p0, p1, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v2

    .line 516
    .local v2, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    const/4 v1, 0x0

    .line 518
    .local v1, "deleteRegion":Z
    :try_start_0
    iget-object v3, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 519
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 520
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getDiskBlob(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 526
    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 529
    if-eqz v1, :cond_0

    .line 530
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "File not found for key %s; likely corruption. Deleting region."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    :cond_0
    return-object v3

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    :try_start_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 523
    const/4 v1, 0x1

    .line 524
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 526
    .end local v0    # "cbfe":Lcom/google/apps/dots/android/newsstand/diskcache/CorruptBlobsFileException;
    :catchall_0
    move-exception v3

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 529
    if-eqz v1, :cond_1

    .line 530
    sget-object v4, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "File not found for key %s; likely corruption. Deleting region."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    :cond_1
    throw v3
.end method

.method public getFsFreeBytes()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->getFsFreeBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 576
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 578
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 579
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 580
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 582
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    return-object v1

    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    throw v1
.end method

.method public getRootDirs()Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;
    .locals 2

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    monitor-exit v1

    return-object v0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public open(Z)V
    .locals 9
    .param p1, "pessimistic"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->upgradeFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->await(Ljava/util/concurrent/Future;)V

    .line 227
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v4

    .line 229
    :try_start_0
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_4

    .line 230
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    if-nez v3, :cond_2

    .line 231
    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;

    const-string v5, "Unknown root dir"

    invoke-direct {v3, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :catchall_0
    move-exception v3

    :try_start_1
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    if-nez v5, :cond_1

    .line 276
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->close(Z)V

    :cond_1
    throw v3

    .line 279
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v3

    .line 235
    :cond_2
    :try_start_2
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    .line 234
    invoke-static {v3, v5}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->isAncestor(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    .line 236
    .local v2, "rootDirIsExternal":Z
    if-eqz v2, :cond_4

    .line 237
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "externalStorageState":Ljava/lang/String;
    const-string v3, "mounted"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 240
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->close(Z)V

    .line 241
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .line 242
    .local v0, "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;->onUnmounted()V

    goto :goto_0

    .line 244
    .end local v0    # "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    :cond_3
    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xb

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Unmounted: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 249
    .end local v1    # "externalStorageState":Ljava/lang/String;
    .end local v2    # "rootDirIsExternal":Z
    :cond_4
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_6

    .line 274
    :try_start_3
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    if-nez v3, :cond_5

    .line 276
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->close(Z)V

    :cond_5
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 280
    :goto_1
    return-void

    .line 253
    :cond_6
    :try_start_4
    sget-object v3, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Opening %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    invoke-virtual {v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_7

    .line 256
    new-instance v3, Ljava/io/IOException;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Expected a directory at path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 257
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_8

    .line 258
    new-instance v3, Ljava/io/IOException;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x23

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Couldn\'t create directory at path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 261
    :cond_8
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->mapStoreFiles()V

    .line 264
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v3, v5}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 265
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    new-instance v6, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    sget-object v8, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-direct {p0, v8}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->makeStoreFile(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v8

    invoke-direct {v6, p0, v7, v8}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V

    invoke-virtual {v3, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_9
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    .line 270
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .line 271
    .restart local v0    # "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;->onAvailable()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 274
    .end local v0    # "availabilityListener":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
    :cond_a
    :try_start_5
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->isOpen:Z

    if-nez v3, :cond_b

    .line 276
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->close(Z)V

    .line 279
    :cond_b
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1
.end method

.method public pin(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;II)V
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "pinId"    # I
    .param p3, "snapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 480
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 481
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 482
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->pin(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 485
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    .line 487
    return-void

    .line 484
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 485
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    throw v1
.end method

.method removeAvailabilityListener(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;)V
    .locals 2
    .param p1, "availabilityListener"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;

    .prologue
    .line 201
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->availabilityListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 203
    monitor-exit v1

    .line 204
    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "metadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 588
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 590
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 591
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 592
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 595
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    .line 597
    return-void

    .line 594
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 595
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    throw v1
.end method

.method public setRootDirs(Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;)V
    .locals 6
    .param p1, "rootDirs"    # Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 185
    :try_start_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Old rootDirs: %s, new rootDirs: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Setting perm root dir %s, cache root dir %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->close(Z)V

    .line 189
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    .line 191
    :cond_0
    monitor-exit v1

    .line 192
    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method split(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V
    .locals 9
    .param p1, "originalRegion"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .param p2, "splitPoint"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 696
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;

    move-result-object v7

    .line 698
    .local v7, "filenameEncodedSplitPoint":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v0

    .line 700
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileManifestFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 701
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileBlobsFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 702
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFilePermBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 703
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->storeFileCacheBlobsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    move-object v1, p2

    .line 698
    invoke-virtual/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->splitFrom(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v8

    .line 705
    .local v8, "splitStoreFile":Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 706
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->regionMap:Ljava/util/TreeMap;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-direct {v2, p0, p2, v8}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V

    invoke-virtual {v0, p2, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 707
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 709
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->lb()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;

    move-result-object v6

    .line 710
    .local v6, "fileEncodedOriginal":Ljava/lang/String;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Split %s --> %s, %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    const/4 v3, 0x1

    aput-object v6, v2, v3

    const/4 v3, 0x2

    aput-object v7, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 712
    return-void

    .line 707
    .end local v6    # "fileEncodedOriginal":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 724
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "perm"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    .line 725
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "cache"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->rootDirs:Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->cache:Ljava/io/File;

    .line 726
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 727
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public touch(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 465
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 467
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 468
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 469
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->touch(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 471
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 472
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    return v1

    .line 471
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 472
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    throw v1
.end method

.method public unpinAll(Landroid/util/SparseIntArray;I)V
    .locals 3
    .param p1, "pinSnapshotIds"    # Landroid/util/SparseIntArray;
    .param p2, "defaultSnapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 495
    const/4 v1, 0x1

    new-array v0, v1, [Z

    aput-boolean v2, v0, v2

    .line 496
    .local v0, "changed":[Z
    new-instance v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;[ZLandroid/util/SparseIntArray;I)V

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V

    .line 504
    aget-boolean v1, v0, v2

    if-eqz v1, :cond_0

    .line 505
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    .line 507
    :cond_0
    return-void
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
    .locals 2
    .param p1, "blobVisitor"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 648
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V

    .line 674
    return-void
.end method

.method visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V
    .locals 4
    .param p1, "regionVisitor"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;
    .param p2, "exclusive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 682
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->EMPTY_KEY:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 683
    .local v0, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :goto_0
    if-eqz v0, :cond_0

    .line 684
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v1

    .line 686
    .local v1, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    invoke-interface {p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V

    .line 687
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 689
    iget-object v2, v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    throw v2

    .line 692
    .end local v1    # "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :cond_0
    return-void
.end method

.method public writeStream(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/InputStream;)V
    .locals 3
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 612
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getLockedRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Z)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    move-result-object v0

    .line 614
    .local v0, "region":Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    :try_start_0
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeEntry:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->touch()V

    .line 615
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->open()V

    .line 616
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->writeStream(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 619
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    .line 621
    return-void

    .line 618
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->regionLock:Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/LockSpace$Lock;->unlock()V

    .line 619
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->janitor:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->request()V

    throw v1
.end method
