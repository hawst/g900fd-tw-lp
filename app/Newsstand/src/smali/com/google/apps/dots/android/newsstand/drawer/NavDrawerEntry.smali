.class public abstract Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
.super Ljava/lang/Object;
.source "NavDrawerEntry.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXPLORE_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

.field public static final HELP_FEEDBACK_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

.field private static final HOME_PAGE_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

.field private static final MISC_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

.field public static final MY_LIBRARY_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

.field public static final READ_NOW_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

.field public static final SAVED_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

.field public static final SETTINGS_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;


# instance fields
.field public final entryType:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_readnow:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_readnow_selected:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;IIZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->READ_NOW_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 28
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_mylibrary:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_mylibrary_selected:I

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;IIZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->MY_LIBRARY_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_bookmarks:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_bookmarks_selected:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;IIZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SAVED_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_explore:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_drawer_explore_selected:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage;IIZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->EXPLORE_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->READ_NOW_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->MY_LIBRARY_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SAVED_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->EXPLORE_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->HOME_PAGE_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 45
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/SettingsEntry;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/drawer/SettingsEntry;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SETTINGS_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 46
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackEntry;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackEntry;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->HELP_FEEDBACK_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 47
    new-array v0, v6, [Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SETTINGS_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->HELP_FEEDBACK_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->MISC_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 135
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$2;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$2;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;)V
    .locals 0
    .param p1, "type"    # Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->entryType:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    .line 62
    return-void
.end method

.method public static getNavDrawerEntryFromHomePageType(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .locals 2
    .param p0, "type"    # Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .prologue
    .line 162
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$3;->$SwitchMap$com$google$apps$dots$android$newsstand$home$HomePage$Type:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 164
    :pswitch_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->READ_NOW_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .line 170
    :goto_0
    return-object v0

    .line 166
    :pswitch_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->MY_LIBRARY_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    goto :goto_0

    .line 168
    :pswitch_2
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SAVED_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    goto :goto_0

    .line 170
    :pswitch_3
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->EXPLORE_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    goto :goto_0

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getNavDrawerEntryFromMiscType(Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;)Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .locals 2
    .param p0, "type"    # Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    .prologue
    .line 176
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$3;->$SwitchMap$com$google$apps$dots$android$newsstand$drawer$MiscEntry$MiscType:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 178
    :pswitch_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->SETTINGS_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    .line 180
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->HELP_FEEDBACK_ENTRY:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getPrimaryActions(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Ljava/util/List;
    .locals 8
    .param p0, "activeNavDrawerEntry"    # Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;",
            "Lcom/google/apps/dots/android/newsstand/activity/NSActivity;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v2, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    sget-object v5, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->HOME_PAGE_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v1, v5, v4

    .line 68
    .local v1, "entry":Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;
    if-eqz p0, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v0, 0x1

    .line 69
    .local v0, "active":Z
    :goto_1
    invoke-virtual {v1, v0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->getPrimaryAction(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .end local v0    # "active":Z
    :cond_0
    move v0, v3

    .line 68
    goto :goto_1

    .line 71
    .end local v1    # "entry":Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;
    :cond_1
    return-object v2
.end method

.method public static getSecondaryActions(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Ljava/util/List;
    .locals 6
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/activity/NSActivity;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v1, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    sget-object v3, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->MISC_ENTRIES:[Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 77
    .local v0, "entry":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getSecondaryAction(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "entry":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public getIconResId()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public getSecondaryAction(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    .locals 3
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$1;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$1;-><init>(Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public abstract getTitle(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract onClick(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
.end method

.method public showDownloadedOnly()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;->entryType:Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry$EntryType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    if-eqz v0, :cond_1

    .line 129
    check-cast p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;

    .end local p0    # "this":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/HomePageEntry;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    .restart local p0    # "this":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    :cond_1
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;

    if-eqz v0, :cond_0

    .line 131
    check-cast p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;

    .end local p0    # "this":Lcom/google/apps/dots/android/newsstand/drawer/NavDrawerEntry;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;->miscType:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
