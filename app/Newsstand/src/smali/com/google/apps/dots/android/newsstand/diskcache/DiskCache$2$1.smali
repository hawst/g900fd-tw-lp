.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;
.super Ljava/lang/Object;
.source "DiskCache.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;

.field final synthetic val$lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

.field final synthetic val$region:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

.field final synthetic val$ub:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;

    .prologue
    .line 656
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$ub:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$region:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;JJJZ)V
    .locals 10
    .param p1, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p2, "readTime"    # J
    .param p4, "writeTime"    # J
    .param p6, "size"    # J
    .param p8, "pinned"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 662
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$ub:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$ub:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v9, 0x1

    .line 663
    .local v9, "keyIsInRegion":Z
    :goto_0
    if-eqz v9, :cond_2

    .line 664
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2;->val$blobVisitor:Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;JJJZ)V

    .line 670
    :goto_1
    return-void

    .line 662
    .end local v9    # "keyIsInRegion":Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 667
    .restart local v9    # "keyIsInRegion":Z
    :cond_2
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Deleting spurious entry: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 668
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$2$1;->val$region:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->delete(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z

    goto :goto_1
.end method
