.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;
.super Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

.field final synthetic val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;Lcom/google/apps/dots/proto/client/DotsShared$Section;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .param p2, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p3, "debugId"    # Ljava/lang/String;
    .param p4, "debugOwner"    # Ljava/lang/Object;

    .prologue
    .line 689
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 692
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v3, "%s: syncing deps"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v4

    invoke-virtual {v0, v3, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 693
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 694
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hasFormId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getFormId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncFormAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v0, v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$2400(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    iget-object v7, v0, Lcom/google/apps/dots/proto/client/DotsShared$Section;->sectionLevelAttachmentIds:[Ljava/lang/String;

    array-length v8, v7

    move v6, v4

    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v2, v7, v6

    .line 698
    .local v2, "sectionLevelAttachmentId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->val$section:Lcom/google/apps/dots/proto/client/DotsShared$Section;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 697
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 701
    .end local v2    # "sectionLevelAttachmentId":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$7;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->startTasks(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
