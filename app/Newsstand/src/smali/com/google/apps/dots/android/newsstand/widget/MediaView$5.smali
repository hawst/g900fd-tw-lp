.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;
.super Ljava/lang/Object;
.source "MediaView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initSurfaceView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$602(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Z)Z

    .line 540
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->bindSurface()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$700(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    .line 541
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$800(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$900()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "onSurfaceChanged(): auto-starting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 543
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    .line 546
    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 528
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$602(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Z)Z

    .line 533
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->unbindSurface()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    .line 534
    return-void
.end method
