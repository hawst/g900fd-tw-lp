.class public Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;
.super Ljava/lang/Object;
.source "ShareUrisUtil.java"


# static fields
.field private static final FINSKY_MAGAZINES_READER:Ljava/lang/String;

.field private static final FINSKY_NEWS_READER:Ljava/lang/String;

.field private static final HOME_ISSUES:Ljava/lang/String;

.field private static final ID_IN_PATH_REGEX:Ljava/lang/String;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MAGAZINES_READER:Ljava/lang/String;

.field private static final NEWSSTAND_EDITIONS:Ljava/lang/String;

.field private static final NEWSSTAND_OFFERS:Ljava/lang/String;

.field private static final NEWSSTAND_POSTS:Ljava/lang/String;

.field private static final NEWSSTAND_SHARE:Ljava/lang/String;

.field private static final NEWS_READER:Ljava/lang/String;

.field private static final OPT_ID_IN_PATH_REGEX:Ljava/lang/String;

.field private static final PROTOCOL_PATTERN:Ljava/util/regex/Pattern;

.field private static final READER_BASE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "([a-zA-Z0-9\\-\\_]+)"

    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(?:$|\\/|\\?)"

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?:/"

    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")?"

    .line 74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->OPT_ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 77
    const-string v0, "^https?|currents|magazines|newsstand"

    .line 78
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->PROTOCOL_PATTERN:Ljava/util/regex/Pattern;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(.*google\\.com)/newsstand/s/"

    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "([a-zA-Z0-9\\-\\_]+)"

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(?:$|\\?.*)"

    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_SHARE:Ljava/lang/String;

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".*google\\.com/newsstand/s/editions/"

    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_EDITIONS:Ljava/lang/String;

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".*google\\.com/newsstand/s/posts/"

    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_POSTS:Ljava/lang/String;

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^newsstand://play\\.google\\.com/magazines/reader/issue/"

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->MAGAZINES_READER:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^newsstand://play\\.google\\.com/offers"

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->OPT_ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_OFFERS:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^newsstand://play\\.google\\.com/news/reader/"

    .line 112
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWS_READER:Ljava/lang/String;

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https?://play\\.google\\.com/magazines/reader/issue/"

    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->FINSKY_MAGAZINES_READER:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https?://play\\.google\\.com/magazines/reader/news/"

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 126
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->FINSKY_NEWS_READER:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".*play\\.google\\.com/magazines/reader/home/issues/"

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->ID_IN_PATH_REGEX:Ljava/lang/String;

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->HOME_ISSUES:Ljava/lang/String;

    .line 137
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "newsstand"

    .line 138
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "play.google.com"

    .line 139
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "news"

    .line 140
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "reader"

    .line 141
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->READER_BASE_URI:Landroid/net/Uri;

    .line 137
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applyTranslationIfNecessary(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 158
    sget-object v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 159
    .local v0, "targetTranslationLanguage":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v1

    const-string v2, "translate"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    const-string v1, "translate"

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getStringQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    :cond_0
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static findRedirectPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 306
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 309
    invoke-static {}, Ljava/net/HttpURLConnection;->getFollowRedirects()Z

    move-result v1

    .line 310
    .local v1, "currentFollowRedirectSetting":Z
    const/4 v3, 0x0

    .line 311
    .local v3, "httpConnection":Ljava/net/HttpURLConnection;
    const/4 v4, 0x0

    .line 313
    .local v4, "newLocation":Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 314
    .local v6, "url":Ljava/net/URL;
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 315
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v3, v0

    .line 316
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    .line 318
    .local v5, "responseCode":I
    const/16 v7, 0x12d

    if-eq v5, v7, :cond_0

    const/16 v7, 0x12e

    if-ne v5, v7, :cond_1

    .line 321
    :cond_0
    const-string v7, "location"

    invoke-virtual {v3, v7}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 330
    :cond_1
    if-eqz v3, :cond_2

    .line 331
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 333
    :cond_2
    invoke-static {v1}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 335
    return-object v4

    .line 323
    .end local v5    # "responseCode":I
    .end local v6    # "url":Ljava/net/URL;
    :catch_0
    move-exception v2

    .line 325
    .local v2, "e":Ljava/net/MalformedURLException;
    :try_start_1
    new-instance v7, Ljava/lang/IllegalArgumentException;

    invoke-direct {v7, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330
    .end local v2    # "e":Ljava/net/MalformedURLException;
    :catchall_0
    move-exception v7

    if-eqz v3, :cond_3

    .line 331
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 333
    :cond_3
    invoke-static {v1}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    throw v7

    .line 326
    :catch_1
    move-exception v2

    .line 328
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public static findTargetFromIntent(Landroid/content/Intent;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "resolveRedirects"    # Z

    .prologue
    .line 166
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findTargetFromUri(Landroid/net/Uri;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static findTargetFromUri(Landroid/net/Uri;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "resolveRedirects"    # Z

    .prologue
    .line 174
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->PROTOCOL_PATTERN:Ljava/util/regex/Pattern;

    .line 175
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;-><init>()V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->build()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v1

    .line 202
    :goto_0
    return-object v1

    .line 179
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findTargetInformationFromUrl(Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    move-result-object v0

    .line 182
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getPostId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 183
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getPostId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->applyTranslationIfNecessary(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 185
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 186
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->applyTranslationIfNecessary(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 188
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppFamilyId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 189
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppFamilyId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->applyTranslationIfNecessary(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 193
    :cond_4
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getPostId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 194
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getPostId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 195
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->getAppId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 202
    :cond_5
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->build()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v1

    goto :goto_0
.end method

.method public static findTargetInformationFromUrl(Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "resolveRedirects"    # Z

    .prologue
    const/4 v5, 0x1

    .line 225
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;-><init>()V

    .line 226
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 227
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 230
    .local v1, "matchGroups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_SHARE:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    if-eqz p1, :cond_1

    .line 233
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 235
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findRedirectPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 236
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 237
    .local v2, "newPath":Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findTargetInformationFromUrl(Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    move-result-object v0

    .line 276
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 239
    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNRESOLVED_REDIRECT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto :goto_0

    .line 241
    :cond_2
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_EDITIONS:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 243
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 244
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto :goto_0

    .line 245
    :cond_3
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_POSTS:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 247
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 248
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto :goto_0

    .line 249
    :cond_4
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->MAGAZINES_READER:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 250
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 251
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto :goto_0

    .line 252
    :cond_5
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWSSTAND_OFFERS:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 253
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_OFFERS:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 254
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 255
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setOfferId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto :goto_0

    .line 257
    :cond_6
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->NEWS_READER:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 258
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 259
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 260
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto/16 :goto_0

    .line 261
    :cond_7
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->FINSKY_MAGAZINES_READER:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 262
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 263
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto/16 :goto_0

    .line 264
    :cond_8
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->FINSKY_NEWS_READER:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 265
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 266
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto/16 :goto_0

    .line 270
    :cond_9
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->HOME_ISSUES:Ljava/lang/String;

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 271
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_ISSUES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    .line 272
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto/16 :goto_0

    .line 273
    :cond_a
    const-string v3, ".*play\\.google\\.com/magazines/reader/home/titles.*"

    invoke-static {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_TITLES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    goto/16 :goto_0
.end method

.method public static isShareTarget(Landroid/content/Intent;)Z
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 211
    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->findTargetFromIntent(Landroid/content/Intent;Z)Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v0

    .line 212
    .local v0, "target":Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-eq v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static makeExternalReadingIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 151
    sget-object v1, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->READER_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 152
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 153
    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 154
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private static matches(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 284
    .local p2, "optReturnMatchesGroup":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 285
    .local v3, "patternObject":Ljava/util/regex/Pattern;
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 286
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    .line 289
    .local v2, "matches":Z
    if-eqz p2, :cond_0

    .line 290
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 291
    if-eqz v2, :cond_0

    .line 297
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-gt v0, v4, :cond_0

    .line 298
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    .end local v0    # "i":I
    :cond_0
    return v2
.end method
