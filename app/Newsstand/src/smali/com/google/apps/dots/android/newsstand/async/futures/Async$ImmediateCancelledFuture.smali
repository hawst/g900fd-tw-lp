.class Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImmediateCancelledFuture"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 68
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/async/futures/Async$1;

    .prologue
    .line 68
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;-><init>()V

    return-void
.end method


# virtual methods
.method public addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 97
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 98
    return-void
.end method

.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 87
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->getGenericCancellation()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->getGenericCancellation()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$ImmediateCancelledFuture<TV;>;"
    const/4 v0, 0x1

    return v0
.end method
