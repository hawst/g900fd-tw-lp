.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
.super Ljava/lang/Object;
.source "SyncerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingOp"
.end annotation


# instance fields
.field anyFreshness:Z

.field final edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final key:Ljava/lang/Object;

.field pinnedVersion:I

.field preemptivelyCancelled:Z

.field progress:Ljava/lang/Float;

.field sync:Z

.field final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final userRequested:Z

.field wifiOnlyDownloadOverride:Z


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;ZZ)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p3, "sync"    # Z
    .param p4, "userRequested"    # Z

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 116
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 117
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->key:Ljava/lang/Object;

    .line 118
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->sync:Z

    .line 119
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->userRequested:Z

    .line 120
    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setWifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .param p1, "x1"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    move-result-object v0

    return-object v0
.end method

.method private setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 0
    .param p1, "anyFreshness"    # Z

    .prologue
    .line 234
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->anyFreshness:Z

    .line 235
    return-object p0
.end method

.method private setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 0
    .param p1, "pinnedVersion"    # I

    .prologue
    .line 239
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->pinnedVersion:I

    .line 240
    return-object p0
.end method

.method private setWifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;
    .locals 0
    .param p1, "override"    # Z

    .prologue
    .line 229
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->wifiOnlyDownloadOverride:Z

    .line 230
    return-object p0
.end method


# virtual methods
.method remove()V
    .locals 2

    .prologue
    .line 215
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->pendingOps:Ljava/util/Map;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$200()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    # invokes: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->notifyObservers()V
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$300()V

    .line 217
    return-void
.end method

.method public start()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->preemptivelyCancelled:Z

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->stop()V

    .line 130
    :cond_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->sync:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->startSync()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->startUnsync()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method startSync()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 134
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Syncing %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p0, v7, v0

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v1

    .line 137
    .local v1, "pinId":Ljava/lang/Integer;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getSnapshotId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v2

    .line 138
    .local v2, "previousSnapshotId":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 140
    .local v0, "nextSnapshotId":I
    :goto_0
    new-instance v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-direct {v5, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    .line 141
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->snapshotId(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->userRequested:Z

    .line 142
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->wifiOnlyDownloadOverride:Z

    .line 143
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->wifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->anyFreshness:Z

    .line 144
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->anyFreshness(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v4

    .line 147
    .local v4, "syncerRequest":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->pinnedVersion:I

    if-ne v5, v8, :cond_0

    .line 148
    new-instance v5, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;-><init>()V

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate(Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    .line 151
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v3, v5, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;)V

    .line 152
    .local v3, "syncer":Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)V

    invoke-virtual {v3, v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->setProgressListener(Lcom/google/apps/dots/android/newsstand/sync/Syncer$ProgressListener;)V

    .line 161
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->progress:Ljava/lang/Float;

    .line 163
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 164
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->thenUnpin(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;

    invoke-direct {v7, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$2;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;Ljava/lang/Integer;I)V

    .line 163
    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    return-object v5

    .line 138
    .end local v0    # "nextSnapshotId":I
    .end local v3    # "syncer":Lcom/google/apps/dots/android/newsstand/sync/Syncer;
    .end local v4    # "syncerRequest":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v0, v5, 0x1

    goto :goto_0
.end method

.method startUnsync()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 184
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Unpinning %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    const/4 v1, 0x0

    .line 186
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->thenUnpin(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)V

    .line 185
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroy()V

    .line 124
    return-void
.end method

.method thenUnpin(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 203
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$4;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->SYNC:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 221
    const-class v0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "edition"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 222
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "progress"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->progress:Ljava/lang/Float;

    .line 223
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "sync"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->sync:Z

    .line 224
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
