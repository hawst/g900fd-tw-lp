.class final Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardSourceListItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZLandroid/accounts/Account;ZZLcom/google/android/libraries/bind/data/Data;Landroid/view/View$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field final synthetic val$isPurchased:Z

.field final synthetic val$isSubscribed:Z


# direct methods
.method constructor <init>(ZLandroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Z)V
    .locals 0

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$isSubscribed:Z

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$isPurchased:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    .line 198
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$isSubscribed:Z

    if-nez v0, :cond_0

    .line 199
    check-cast p2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$account:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-static {p2, v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->addSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    .line 205
    :goto_0
    return-void

    .line 202
    .restart local p2    # "activity":Landroid/app/Activity;
    :cond_0
    check-cast p2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$account:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;->val$isPurchased:Z

    const/4 v3, 0x0

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V

    goto :goto_0
.end method
