.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->callInternal()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

.field final synthetic val$tasks:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    .prologue
    .line 570
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncAll(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v1, p2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1400(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 598
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 7
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    .line 603
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    .line 604
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    sget-object v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v4, p2

    .line 602
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageOrAttachmentTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1500(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 608
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 6
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v4, 0x0

    .line 612
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 613
    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncerRequest:Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$900(Lcom/google/apps/dots/android/newsstand/sync/Syncer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623
    :goto_0
    return-void

    .line 616
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasAuthor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v3, p2

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncImageBlobTaskIfPresent(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1600(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSourceIconId()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    move-object v3, p2

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->addSyncAttachmentBlobTaskIfPresent(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1700(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;ZLcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;)Z

    .line 622
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v2, p2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncPostAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v1, v2, p2, v4}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1800(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 10
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    const/4 v3, 0x0

    .line 573
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 579
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getIncomplete()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 580
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasSelf()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 581
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 583
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, "collectionId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncCollectionAll(Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v5, v0, v6, v3}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1100(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Ljava/lang/Object;Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    .end local v0    # "collectionId":Ljava/lang/String;
    :cond_0
    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v5, v4

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v1, v4, v3

    .line 589
    .local v1, "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v7, v7, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Syncer;

    .line 590
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->getLinkType()I

    move-result v9

    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    move-result-object v9

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->storeRequest(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1200(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;

    iget-object v8, v8, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5;->val$collection:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 589
    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Syncer;->syncBlob(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;
    invoke-static {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->access$1300(Lcom/google/apps/dots/android/newsstand/sync/Syncer;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;

    move-result-object v2

    .line 591
    .local v2, "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$5$1;->val$tasks:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 593
    .end local v1    # "link":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .end local v2    # "task":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask;, "Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerTask<*>;"
    :cond_1
    return-void
.end method
