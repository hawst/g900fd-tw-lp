.class Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;
.super Ljava/lang/Object;
.source "NewsArticleParentLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->setupArticleTail()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

.field final synthetic val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;->val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$000(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;->val$articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setup(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 192
    return-void
.end method
