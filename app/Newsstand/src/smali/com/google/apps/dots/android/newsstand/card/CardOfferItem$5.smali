.class final Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardOfferItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getSplashClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$analyticsReadingScreen:Ljava/lang/String;

.field final synthetic val$offerEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field final synthetic val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$analyticsReadingScreen:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$offerEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 335
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$analyticsReadingScreen:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 336
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->EDITION_CLICKED:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->track(Z)V

    .line 337
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$5;->val$offerEditionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 338
    return-void
.end method
