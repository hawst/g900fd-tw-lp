.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserReview"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;


# instance fields
.field private bitField0_:I

.field private comment_:Ljava/lang/String;

.field private publisher_:Ljava/lang/String;

.field private rating_:I

.field private title_:Ljava/lang/String;

.field private url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13119
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13120
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 13125
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    .line 13147
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    .line 13169
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    .line 13188
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    .line 13120
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    .locals 3

    .prologue
    .line 13243
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13247
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v2, :cond_0

    .line 13248
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13250
    :cond_0
    return-object v0

    .line 13244
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    :catch_0
    move-exception v1

    .line 13245
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13116
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13255
    if-ne p1, p0, :cond_1

    .line 13262
    :cond_0
    :goto_0
    return v1

    .line 13256
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 13257
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    .line 13258
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13259
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 13261
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 13262
    goto :goto_0

    .line 13258
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    .line 13259
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    .line 13261
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13262
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 13297
    const/4 v0, 0x0

    .line 13298
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13299
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    .line 13300
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13302
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13303
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    .line 13304
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13306
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13307
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    .line 13308
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13310
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13311
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    .line 13312
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13314
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v1, :cond_4

    .line 13315
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13316
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13318
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->cachedSize:I

    .line 13319
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 13266
    const/16 v0, 0x11

    .line 13267
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 13268
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 13269
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 13270
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    add-int v0, v1, v3

    .line 13271
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 13272
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 13273
    return v0

    .line 13268
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 13269
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 13271
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 13272
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 13328
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 13332
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 13333
    :sswitch_0
    return-object p0

    .line 13338
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    .line 13339
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    goto :goto_0

    .line 13343
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    .line 13344
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    goto :goto_0

    .line 13348
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    .line 13349
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    goto :goto_0

    .line 13353
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    .line 13354
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    goto :goto_0

    .line 13358
    :sswitch_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v1, :cond_1

    .line 13359
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 13361
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 13328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13116
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13278
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13279
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->publisher_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13281
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13282
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->title_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13284
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13285
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->rating_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 13287
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13288
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->comment_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13290
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v0, :cond_4

    .line 13291
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product$UserReview;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 13293
    :cond_4
    return-void
.end method
