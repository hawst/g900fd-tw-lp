.class public final enum Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
.super Ljava/lang/Enum;
.source "SearchResultsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchResultsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum ENTITIES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum FEEDS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum NEWS_EDITIONS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

.field public static final enum SAVED_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "NEWS_EDITIONS"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->NEWS_EDITIONS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "MAGAZINES"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "SAVED_POSTS"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->SAVED_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "LIVE_POSTS"

    invoke-direct {v0, v1, v6}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "FEEDS"

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->FEEDS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    const-string v1, "ENTITIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ENTITIES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 65
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->NEWS_EDITIONS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->MAGAZINES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->SAVED_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->FEEDS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ENTITIES:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->$VALUES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->$VALUES:[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    return-object v0
.end method


# virtual methods
.method public shelfTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    sget-object v0, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$1;->$SwitchMap$com$google$apps$dots$android$newsstand$search$SearchResultsList$Type:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 84
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 71
    :pswitch_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->feeds:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :pswitch_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->articles:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :pswitch_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->magazines:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_3
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->news:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :pswitch_4
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->saved_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    :pswitch_5
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->topics:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
