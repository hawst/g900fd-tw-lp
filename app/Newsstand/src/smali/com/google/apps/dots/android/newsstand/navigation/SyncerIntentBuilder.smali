.class public Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;
.source "SyncerIntentBuilder.java"


# instance fields
.field private account:Landroid/accounts/Account;

.field private action:Ljava/lang/String;

.field private anyFreshness:Z

.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private gcmMessage:[B

.field private pinnedVersion:I

.field private resultReceiver:Landroid/os/ResultReceiver;

.field private userRequested:Z

.field private wifiOnlyDownloadOverride:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ServiceIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 33
    const-class v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 34
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v1, :cond_0

    .line 36
    const-string v1, "edition"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->resultReceiver:Landroid/os/ResultReceiver;

    if-eqz v1, :cond_1

    .line 39
    const-string v1, "resultReceiver"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->resultReceiver:Landroid/os/ResultReceiver;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->account:Landroid/accounts/Account;

    if-eqz v1, :cond_2

    .line 42
    const-string v1, "extraAccount"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->account:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 44
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->gcmMessage:[B

    if-eqz v1, :cond_3

    .line 45
    const-string v1, "gcmMessage"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->gcmMessage:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 47
    :cond_3
    const-string v1, "wifiOnlyDownloadOverride"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->wifiOnlyDownloadOverride:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    const-string v1, "userRequested"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->userRequested:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    const-string v1, "anyFreshness"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->anyFreshness:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 50
    const-string v1, "pinnedVersion"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinnedVersion:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    return-object v0
.end method

.method public fullSync()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1

    .prologue
    .line 73
    const-string v0, "fullSync"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 75
    return-object p0
.end method

.method public pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 55
    const-string v0, "pinEdition"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 56
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 57
    return-object p0
.end method

.method public setAccount(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->account:Landroid/accounts/Account;

    .line 106
    return-object p0
.end method

.method public setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "anyFreshness"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->anyFreshness:Z

    .line 125
    return-object p0
.end method

.method public setPinnedVersion(I)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "pinnedVersion"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinnedVersion:I

    .line 133
    return-object p0
.end method

.method public setResultReceiver(Landroid/os/ResultReceiver;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "resultReceiver"    # Landroid/os/ResultReceiver;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->resultReceiver:Landroid/os/ResultReceiver;

    .line 101
    return-object p0
.end method

.method public setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "userRequested"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->userRequested:Z

    .line 111
    return-object p0
.end method

.method public setWifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 0
    .param p1, "override"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->wifiOnlyDownloadOverride:Z

    .line 116
    return-object p0
.end method

.method public syncConfig()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1

    .prologue
    .line 95
    const-string v0, "syncConfig"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public syncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 67
    const-string v0, "syncEdition"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 69
    return-object p0
.end method

.method public syncMyCurations()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1

    .prologue
    .line 84
    const-string v0, "syncMyCurations"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public syncMyMagazines([B)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1
    .param p1, "optGcmMessage"    # [B

    .prologue
    .line 89
    const-string v0, "syncMyMagazines"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 90
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->gcmMessage:[B

    .line 91
    return-object p0
.end method

.method public syncMyNews()Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1

    .prologue
    .line 79
    const-string v0, "syncMyNews"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public unpinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 61
    const-string v0, "unpinEdition"

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->action:Ljava/lang/String;

    .line 62
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 63
    return-object p0
.end method
