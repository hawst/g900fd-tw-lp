.class public final Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PinnedAccounts"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;


# instance fields
.field private bitField0_:I

.field private highestPinId_:I

.field public pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1299
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1304
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 1307
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    .line 1299
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .locals 5

    .prologue
    .line 1337
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1341
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1342
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 1343
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 1344
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 1345
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->clone()Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1343
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1338
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 1339
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 1349
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->clone()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1354
    if-ne p1, p0, :cond_1

    .line 1357
    :cond_0
    :goto_0
    return v1

    .line 1355
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1356
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    .line 1357
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getHighestPinId()I
    .locals 1

    .prologue
    .line 1309
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1390
    const/4 v1, 0x0

    .line 1391
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-eqz v2, :cond_1

    .line 1392
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 1393
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    if-eqz v0, :cond_0

    .line 1394
    const/4 v5, 0x1

    .line 1395
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1392
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1399
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 1400
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    .line 1401
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 1403
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->cachedSize:I

    .line 1404
    return v1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 1362
    const/16 v1, 0x11

    .line 1363
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 1364
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 1370
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    add-int v1, v2, v3

    .line 1371
    return v1

    .line 1366
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1367
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 1366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1367
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1412
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1413
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1417
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1418
    :sswitch_0
    return-object p0

    .line 1423
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1424
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-nez v5, :cond_2

    move v1, v4

    .line 1425
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 1426
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-eqz v5, :cond_1

    .line 1427
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1429
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    .line 1430
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1431
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;-><init>()V

    aput-object v6, v5, v1

    .line 1432
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1433
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1430
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1424
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v1, v5

    goto :goto_1

    .line 1436
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;-><init>()V

    aput-object v6, v5, v1

    .line 1437
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1441
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    .line 1442
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    goto :goto_0

    .line 1413
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1295
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v0

    return-object v0
.end method

.method public setHighestPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1312
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    .line 1313
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    .line 1314
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1376
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    if-eqz v1, :cond_1

    .line 1377
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->pinned:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1378
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    if-eqz v0, :cond_0

    .line 1379
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1377
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1383
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 1384
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;->highestPinId_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1386
    :cond_2
    return-void
.end method
