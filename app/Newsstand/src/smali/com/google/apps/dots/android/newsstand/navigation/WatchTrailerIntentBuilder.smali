.class public Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.source "WatchTrailerIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<",
        "Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final TRAILER_URI_AUTHORITY_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 34
    const-string v0, "(.+\\.)?youtube.com$"

    .line 35
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->TRAILER_URI_AUTHORITY_PATTERN:Ljava/util/regex/Pattern;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 46
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static isMovieTrailerUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 38
    if-eqz p0, :cond_0

    .line 39
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->TRAILER_URI_AUTHORITY_PATTERN:Ljava/util/regex/Pattern;

    .line 40
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "v"

    .line 41
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isVideosAppTrailerPlaybackSupported(Landroid/content/pm/PackageManager;)Z
    .locals 10
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 116
    const-string v5, "com.google.android.videos"

    invoke-static {p0, v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 117
    sget-object v5, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Videos App installed"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    :try_start_0
    const-string v5, "com.google.android.videos"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget v2, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 121
    .local v2, "version":I
    sget-object v5, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Videos app version is %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finsky.video_app_trailer_playback_min_version"

    const/16 v7, 0x6996

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 129
    .local v1, "minVersion":I
    if-lt v2, v1, :cond_1

    :goto_0
    move v4, v3

    .line 131
    .end local v1    # "minVersion":I
    .end local v2    # "version":I
    :cond_0
    :goto_1
    return v4

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error retrieving videos app version: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v5, v6, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "minVersion":I
    .restart local v2    # "version":I
    :cond_1
    move v3, v4

    .line 129
    goto :goto_0
.end method


# virtual methods
.method protected buildViewCollectionIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WatchTrailerIntentBuilder doesn\'t support collections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected buildViewItemIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 98
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "buildViewItemIntent [%s]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->webUri:Landroid/net/Uri;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 101
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->isVideosAppTrailerPlaybackSupported(Landroid/content/pm/PackageManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Playing back in videos app"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.videos.intent.action.trailers.VIEW"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->webUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 104
    .local v1, "trailerIntent":Landroid/content/Intent;
    const-string v2, "com.google.android.videos"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    :goto_0
    const-string v2, "authAccount"

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 112
    return-object v1

    .line 106
    .end local v1    # "trailerIntent":Landroid/content/Intent;
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Attempting to play in YouTube"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->webUri:Landroid/net/Uri;

    .line 108
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->setUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;

    move-result-object v2

    .line 109
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/YouTubeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .restart local v1    # "trailerIntent":Landroid/content/Intent;
    goto :goto_0
.end method

.method protected startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method protected startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "options"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x256

    .line 55
    sget-object v3, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "starting activity"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    const-string v3, "com.google.android.videos"

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->activity:Landroid/app/Activity;

    .line 65
    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v8, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->activity:Landroid/app/Activity;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 68
    .local v0, "nsActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getRequestedOrientation()I

    move-result v1

    .line 69
    .local v1, "prevOrientation":I
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getCurrentDisplayOrientationDegrees()I

    move-result v2

    .line 70
    .local v2, "prevOrientationDegrees":I
    invoke-virtual {v0, v8}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setRequestedOrientation(I)V

    .line 71
    sget-object v3, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Setting result handler"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder$1;-><init>(Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;II)V

    invoke-virtual {v0, v6, v3}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setResultHandlerForActivityCode(ILcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;)V

    .line 83
    .end local v0    # "nsActivity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .end local v1    # "prevOrientation":I
    .end local v2    # "prevOrientationDegrees":I
    :cond_0
    if-nez p2, :cond_1

    .line 84
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v3, p1, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WatchTrailerIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v3, p1, v6, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0
.end method
