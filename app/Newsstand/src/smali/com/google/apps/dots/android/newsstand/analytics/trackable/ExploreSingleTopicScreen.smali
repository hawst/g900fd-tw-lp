.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/ExploreSingleTopicScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;
.source "ExploreSingleTopicScreen.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V
    .locals 3
    .param p1, "topic"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .prologue
    .line 12
    const-string v0, "Explore - "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
