.class Lcom/google/apps/dots/android/newsstand/NSDepend$3;
.super Lcom/google/android/play/utils/PlayCommonNetworkStack;
.source "NSDepend.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/NSDepend;->getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/NSDepend;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/NSDepend;Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/NSDepend;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;

    .prologue
    .line 756
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/NSDepend$3;->this$0:Lcom/google/apps/dots/android/newsstand/NSDepend;

    invoke-direct {p0, p2, p3}, Lcom/google/android/play/utils/PlayCommonNetworkStack;-><init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized getPlayDfeApi(Ljava/lang/String;)Lcom/google/android/play/dfe/api/PlayDfeApi;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 761
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 762
    .local v0, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-super {p0, p1}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getPlayDfeApi(Ljava/lang/String;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v1

    .line 763
    .local v1, "playDfeApi":Lcom/google/android/play/dfe/api/PlayDfeApi;
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 764
    monitor-exit p0

    return-object v1

    .line 761
    .end local v0    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    .end local v1    # "playDfeApi":Lcom/google/android/play/dfe/api/PlayDfeApi;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
