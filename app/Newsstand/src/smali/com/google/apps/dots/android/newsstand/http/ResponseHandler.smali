.class public abstract Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;
.super Ljava/lang/Object;
.source "ResponseHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DOTS_PROTO_LENGTH_HEADER:Ljava/lang/String; = "X-Dots-Proto-Length"

.field private static final EXECUTION_LATENCY_WARN_METERED_MS:J = 0xbb8L

.field private static final EXECUTION_LATENCY_WARN_UNMETERED_MS:J = 0x7d0L

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MAX_AUTH_RETRY:I = 0x1


# instance fields
.field private final account:Landroid/accounts/Account;

.field private authRetry:I

.field protected autoCloseEntityStream:Z

.field private final client:Lorg/apache/http/client/HttpClient;

.field private managedEntity:Lorg/apache/http/conn/BasicManagedEntity;

.field protected request:Lorg/apache/http/client/methods/HttpRequestBase;

.field protected requestContext:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lorg/apache/http/client/HttpClient;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "client"    # Lorg/apache/http/client/HttpClient;

    .prologue
    .line 68
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->authRetry:I

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->autoCloseEntityStream:Z

    .line 69
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->account:Landroid/accounts/Account;

    .line 70
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->client:Lorg/apache/http/client/HttpClient;

    .line 71
    return-void
.end method

.method private getWarnExecutionLatency()J
    .locals 2

    .prologue
    .line 209
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isMetered()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0xbb8

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x7d0

    goto :goto_0
.end method


# virtual methods
.method protected abortConnection()V
    .locals 4

    .prologue
    .line 216
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->managedEntity:Lorg/apache/http/conn/BasicManagedEntity;

    if-eqz v1, :cond_0

    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->managedEntity:Lorg/apache/http/conn/BasicManagedEntity;

    invoke-virtual {v1}, Lorg/apache/http/conn/BasicManagedEntity;->abortConnection()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->managedEntity:Lorg/apache/http/conn/BasicManagedEntity;

    .line 224
    :cond_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Failed to abort connection"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public execute()Ljava/lang/Object;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->installIfNeeded()V

    .line 86
    const/16 v19, 0x0

    .line 87
    .local v19, "response":Lorg/apache/http/HttpResponse;
    const/4 v15, 0x0

    .line 88
    .local v15, "offline":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 90
    .local v6, "before":J
    new-instance v13, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v13}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 92
    .local v13, "httpContext":Lorg/apache/http/protocol/HttpContext;
    const-string v23, "account"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->account:Landroid/accounts/Account;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v13, v0, v1}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    const-string v23, "request"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->requestContext:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v13, v0, v1}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    const-string v23, "HTTP"

    const-string v24, "%s:%s"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getMethod()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    :try_start_0
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual/range {v23 .. v23}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v23

    if-eqz v23, :cond_0

    .line 99
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "Request: %s"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/http/client/methods/HttpRequestBase;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v24

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v25, v0

    const/16 v23, 0x0

    :goto_0
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_0

    aget-object v12, v24, v23

    .line 101
    .local v12, "header":Lorg/apache/http/Header;
    sget-object v26, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v27, "Header: %s"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aput-object v12, v28, v29

    invoke-virtual/range {v26 .. v28}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .line 104
    .end local v12    # "header":Lorg/apache/http/Header;
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->getHttpResponse(Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/NoRouteToHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v19

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 128
    :goto_1
    if-eqz v15, :cond_2

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleOffline()Ljava/lang/Object;

    move-result-object v20

    .line 197
    :cond_1
    :goto_2
    return-object v20

    .line 105
    :catch_0
    move-exception v9

    .line 106
    .local v9, "e":Ljava/net/URISyntaxException;
    :try_start_1
    new-instance v23, Lcom/google/apps/dots/android/newsstand/sync/SyncException;

    const-string v24, "Failed to construct HTTP client"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v9}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    .end local v9    # "e":Ljava/net/URISyntaxException;
    :catchall_0
    move-exception v23

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v23

    .line 107
    :catch_1
    move-exception v9

    .line 108
    .local v9, "e":Ljava/net/UnknownHostException;
    const/4 v15, 0x1

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_1

    .line 109
    .end local v9    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v9

    .line 110
    .local v9, "e":Ljava/net/NoRouteToHostException;
    const/4 v15, 0x1

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_1

    .line 111
    .end local v9    # "e":Ljava/net/NoRouteToHostException;
    :catch_3
    move-exception v9

    .line 112
    .local v9, "e":Ljava/net/ConnectException;
    const/4 v15, 0x1

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_1

    .line 113
    .end local v9    # "e":Ljava/net/ConnectException;
    :catch_4
    move-exception v9

    .line 114
    .local v9, "e":Ljava/net/SocketException;
    const/4 v15, 0x1

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_1

    .line 115
    .end local v9    # "e":Ljava/net/SocketException;
    :catch_5
    move-exception v9

    .line 116
    .local v9, "e":Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleUpgradeRequired()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 117
    const/4 v15, 0x1

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_1

    .line 118
    .end local v9    # "e":Lcom/google/apps/dots/android/newsstand/exception/UpgradeRequiredException;
    :catch_6
    move-exception v9

    .line 119
    .local v9, "e":Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleNoAuthToken(Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v20

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_2

    .line 120
    .end local v9    # "e":Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
    :catch_7
    move-exception v9

    .line 121
    .local v9, "e":Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleHttpAuthException(Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v20

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_2

    .line 122
    .end local v9    # "e":Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;
    :catch_8
    move-exception v14

    .line 123
    .local v14, "ioe":Ljava/io/IOException;
    :try_start_5
    new-instance v23, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v14, v1}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;-><init>(Ljava/lang/Throwable;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleHttpException(Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v20

    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_2

    .line 133
    .end local v14    # "ioe":Ljava/io/IOException;
    :cond_2
    if-nez v19, :cond_3

    .line 134
    new-instance v23, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    const-string v24, "Response was null"

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleHttpException(Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;)Ljava/lang/Object;

    move-result-object v20

    goto :goto_2

    .line 136
    :cond_3
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "Response: %s"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v21

    .line 139
    .local v21, "statusCode":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 140
    .local v4, "after":J
    sub-long v16, v4, v6

    .line 141
    .local v16, "latencyMs":J
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->getWarnExecutionLatency()J

    move-result-wide v24

    cmp-long v23, v16, v24

    if-lez v23, :cond_5

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isStrictModeEnabled()Z

    move-result v23

    if-eqz v23, :cond_5

    .line 142
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "%s: %,d msecs (ServerPerf)"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :goto_3
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v10

    .line 148
    .local v10, "entity":Lorg/apache/http/HttpEntity;
    instance-of v0, v10, Lorg/apache/http/conn/BasicManagedEntity;

    move/from16 v23, v0

    if-eqz v23, :cond_4

    .line 149
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v23

    check-cast v23, Lorg/apache/http/conn/BasicManagedEntity;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->managedEntity:Lorg/apache/http/conn/BasicManagedEntity;

    .line 151
    :cond_4
    const/16 v23, 0x130

    move/from16 v0, v23

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    .line 152
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleNotModified(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v20

    goto/16 :goto_2

    .line 144
    .end local v10    # "entity":Lorg/apache/http/HttpEntity;
    :cond_5
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "%s: %,d msecs (ServerPerf)"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 154
    .restart local v10    # "entity":Lorg/apache/http/HttpEntity;
    :cond_6
    if-nez v10, :cond_7

    const/16 v23, 0xcc

    move/from16 v0, v23

    move/from16 v1, v21

    if-eq v0, v1, :cond_7

    .line 155
    new-instance v23, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    const-string v24, "Entity was null"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleHttpException(Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;)Ljava/lang/Object;

    move-result-object v20

    goto/16 :goto_2

    .line 157
    :cond_7
    const/4 v8, 0x1

    .line 159
    .local v8, "closeEntityStream":Z
    const/16 v20, 0x0

    .line 161
    .local v20, "result":Ljava/lang/Object;, "TT;"
    const/16 v23, 0xc8

    move/from16 v0, v23

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 162
    :try_start_6
    const-string v23, "X-Dots-Proto-Length"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v12

    .line 163
    .restart local v12    # "header":Lorg/apache/http/Header;
    if-eqz v12, :cond_8

    .line 164
    invoke-interface {v12}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 165
    .local v18, "protoLength":I
    new-instance v11, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler$1;

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v11, v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler$1;-><init>(Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;Lorg/apache/http/HttpEntity;I)V

    .end local v10    # "entity":Lorg/apache/http/HttpEntity;
    .local v11, "entity":Lorg/apache/http/HttpEntity;
    move-object v10, v11

    .line 172
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v18    # "protoLength":I
    .restart local v10    # "entity":Lorg/apache/http/HttpEntity;
    :cond_8
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "%s: content length: %d"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-interface {v10}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v20

    .line 181
    .end local v12    # "header":Lorg/apache/http/Header;
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->autoCloseEntityStream:Z
    :try_end_6
    .catch Lcom/google/apps/dots/android/newsstand/sync/SyncException; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 186
    if-eqz v8, :cond_1

    if-eqz v10, :cond_1

    .line 190
    :try_start_7
    invoke-interface {v10}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9

    goto/16 :goto_2

    .line 191
    :catch_9
    move-exception v9

    .line 192
    .local v9, "e":Ljava/io/IOException;
    sget-object v23, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v24, "Error executing consumeContent()"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v9, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 174
    .end local v9    # "e":Ljava/io/IOException;
    :cond_9
    const/16 v23, 0xcc

    move/from16 v0, v23

    move/from16 v1, v21

    if-ne v0, v1, :cond_a

    .line 175
    :try_start_8
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleNoContent(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v20

    goto :goto_4

    .line 176
    :cond_a
    const/16 v23, 0x193

    move/from16 v0, v23

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleForbidden()Ljava/lang/Object;

    move-result-object v20

    goto :goto_4

    .line 179
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleBadStatusCode(ILorg/apache/http/HttpResponse;)Ljava/lang/Object;
    :try_end_8
    .catch Lcom/google/apps/dots/android/newsstand/sync/SyncException; {:try_start_8 .. :try_end_8} :catch_a
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v20

    goto :goto_4

    .line 182
    .end local v20    # "result":Ljava/lang/Object;, "TT;"
    :catch_a
    move-exception v22

    .line 183
    .local v22, "syncException":Lcom/google/apps/dots/android/newsstand/sync/SyncException;
    const/4 v8, 0x1

    .line 184
    :try_start_9
    throw v22
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 186
    .end local v22    # "syncException":Lcom/google/apps/dots/android/newsstand/sync/SyncException;
    :catchall_1
    move-exception v23

    if-eqz v8, :cond_c

    if-eqz v10, :cond_c

    .line 190
    :try_start_a
    invoke-interface {v10}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_b

    .line 193
    :cond_c
    :goto_5
    throw v23

    .line 191
    :catch_b
    move-exception v9

    .line 192
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v24, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v25, "Error executing consumeContent()"

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v9, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5
.end method

.method protected getHttpResponse(Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 2
    .param p1, "httpContext"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 205
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->client:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v0, v1, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleBadStatusCode(ILorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 7
    .param p1, "statusCode"    # I
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    const/4 v6, 0x0

    .line 241
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unexpected status code [%,d] for uri %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 242
    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 241
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isStrictModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->handleHttpException(Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected handleForbidden()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/ForbiddenSyncException;
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "uriString":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isStrictModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "status code 403 for uri %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/ForbiddenSyncException;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/sync/ForbiddenSyncException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected handleHttpAuthException(Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;)Ljava/lang/Object;
    .locals 2
    .param p1, "e"    # Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/exception/HttpAuthException;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 250
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->authRetry:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->authRetry:I

    .line 251
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->authRetry:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->execute()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 254
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/InvalidAuthSyncException;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/InvalidAuthSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected handleHttpException(Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;)Ljava/lang/Object;
    .locals 0
    .param p1, "e"    # Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;
        }
    .end annotation

    .prologue
    .line 263
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    throw p1
.end method

.method protected handleNoAuthToken(Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;)Ljava/lang/Object;
    .locals 2
    .param p1, "e"    # Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/FatalSyncException;
        }
    .end annotation

    .prologue
    .line 284
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;->getRequestingAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfAccountProblem()V

    .line 287
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/InvalidAuthSyncException;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/InvalidAuthSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected abstract handleNoContent(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation
.end method

.method protected handleNotModified(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 232
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/SyncException;

    const-string v1, "unsupported operation"

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected handleOffline()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;
        }
    .end annotation

    .prologue
    .line 259
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation
.end method

.method protected handleUpgradeRequired()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/FatalSyncException;
        }
    .end annotation

    .prologue
    .line 275
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfRequiredUpgrade()V

    .line 276
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/UpgradeRequiredSyncException;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/sync/UpgradeRequiredSyncException;-><init>()V

    throw v0
.end method

.method protected setRequest(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;)V
    .locals 0
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "optRequestContext"    # Ljava/lang/String;

    .prologue
    .line 74
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;, "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler<TT;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 75
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;->requestContext:Ljava/lang/String;

    .line 76
    return-void
.end method
