.class final Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "MagazineEdition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;->setPageFractionForPost(Landroid/accounts/Account;Ljava/lang/String;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$appId:Ljava/lang/String;

.field final synthetic val$pageFraction:F

.field final synthetic val$postId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 0
    .param p1, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 151
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$appId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$postId:Ljava/lang/String;

    iput p5, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$pageFraction:F

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$account:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$appId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$postId:Ljava/lang/String;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition$3;->val$pageFraction:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsMagazineRead(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;)V

    .line 155
    return-void
.end method
