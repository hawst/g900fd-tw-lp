.class public final Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DataSource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;,
        Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource;


# instance fields
.field private bitField0_:I

.field private sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

.field private socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7985
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7986
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 8518
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    .line 7986
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    .locals 3

    .prologue
    .line 8587
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8591
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-eqz v2, :cond_0

    .line 8592
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    .line 8594
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-eqz v2, :cond_1

    .line 8595
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    .line 8597
    :cond_1
    return-object v0

    .line 8588
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    :catch_0
    move-exception v1

    .line 8589
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7982
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8602
    if-ne p1, p0, :cond_1

    .line 8607
    :cond_0
    :goto_0
    return v1

    .line 8603
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 8604
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    .line 8605
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-nez v3, :cond_3

    .line 8606
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 8607
    goto :goto_0

    .line 8605
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    .line 8606
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    .line 8607
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 8634
    const/4 v0, 0x0

    .line 8635
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8636
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    .line 8637
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8639
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-eqz v1, :cond_1

    .line 8640
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    .line 8641
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8643
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-eqz v1, :cond_2

    .line 8644
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    .line 8645
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8647
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->cachedSize:I

    .line 8648
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 8611
    const/16 v0, 0x11

    .line 8612
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 8613
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    add-int v0, v1, v3

    .line 8614
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 8615
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 8616
    return v0

    .line 8614
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->hashCode()I

    move-result v1

    goto :goto_0

    .line 8615
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8656
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 8657
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 8661
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8662
    :sswitch_0
    return-object p0

    .line 8667
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 8668
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 8675
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    .line 8676
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->bitField0_:I

    goto :goto_0

    .line 8678
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    goto :goto_0

    .line 8683
    .end local v1    # "temp":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-nez v2, :cond_3

    .line 8684
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    .line 8686
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 8690
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-nez v2, :cond_4

    .line 8691
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    .line 8693
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 8657
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x5a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7982
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8621
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8622
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 8624
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-eqz v0, :cond_1

    .line 8625
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->socialData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 8627
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    if-eqz v0, :cond_2

    .line 8628
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource;->sectionData_:Lcom/google/apps/dots/proto/client/DotsShared$DataSource$SectionData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 8630
    :cond_2
    return-void
.end method
