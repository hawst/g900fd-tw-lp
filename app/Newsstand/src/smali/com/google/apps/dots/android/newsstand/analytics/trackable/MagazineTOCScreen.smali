.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;
.source "MagazineTOCScreen.java"


# instance fields
.field private final inLiteMode:Z

.field private final page:I


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;IZ)V
    .locals 0
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .param p2, "page"    # I
    .param p3, "inLiteMode"    # Z

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionScreenBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 20
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->page:I

    .line 21
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->inLiteMode:Z

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 64
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;

    .line 66
    .local v0, "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->page:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 67
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->inLiteMode:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->inLiteMode:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 70
    .end local v0    # "screen":Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 3
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 28
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v1, v1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 29
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 30
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 31
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->page:I

    .line 32
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 34
    const-string v1, "LiteMode"

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->inLiteMode:Z

    .line 35
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 54
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 43
    .local v0, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const-string v1, "%s %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[Magazine - TOC]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 44
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 43
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineTOCScreen;->inLiteMode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method
