.class Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "NSImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setZoomable(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

.field translation:[F


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 204
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->translation:[F

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 226
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;Z)Z

    .line 227
    return v1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v8, 0x0

    .line 209
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v2

    if-nez v2, :cond_2

    .line 210
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z
    invoke-static {v2, v8}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;Z)Z

    .line 221
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Z

    move-result v2

    return v2

    .line 212
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    neg-float v3, p3

    neg-float v4, p4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->translation:[F

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V
    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V

    .line 213
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->invalidate()V

    .line 214
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .local v0, "absDX":F
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 217
    .local v1, "absDY":F
    float-to-double v2, v1

    const-wide v4, 0x3fc999999999999aL    # 0.2

    float-to-double v6, v0

    mul-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    float-to-double v2, v0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->translation:[F

    aget v2, v2, v8

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 218
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->isScrolling:Z
    invoke-static {v2, v8}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;Z)Z

    goto :goto_0
.end method
