.class public interface abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
.super Ljava/lang/Object;
.source "InAppPurchaseConversionEventProvider.java"


# virtual methods
.method public abstract get(Ljava/lang/String;ZLjava/util/Map;Ljava/util/Map;)Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEvent;"
        }
    .end annotation
.end method
