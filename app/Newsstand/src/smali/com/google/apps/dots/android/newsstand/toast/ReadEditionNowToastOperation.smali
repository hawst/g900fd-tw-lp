.class public Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;
.super Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
.source "ReadEditionNowToastOperation.java"


# instance fields
.field private newEdition:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "newEdition"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->read_new_subscription_now_prompt:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 27
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;->newEdition:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 28
    return-void
.end method


# virtual methods
.method public onActionClicked(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;->getActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;->newEdition:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 33
    return-void
.end method
