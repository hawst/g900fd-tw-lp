.class public Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;
.source "CardSourceListItem.java"


# static fields
.field public static final DK_CARD_BACKGROUND:I

.field public static final DK_CARD_TRANSITION_NAME:I

.field public static final DK_SOURCE_BACKGROUND:I

.field public static final DK_SOURCE_CLICKHANDLER:I

.field public static final DK_SOURCE_DESCRIPTION:I

.field public static final DK_SOURCE_DESCRIPTION_TEXT_COLOR:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_SOURCE_ICON_TRANSITION_NAME:I

.field public static final DK_SOURCE_INITIALS:I

.field public static final DK_SOURCE_INITIALS_BACKGROUND:I

.field public static final DK_SOURCE_INITIALS_TEXT_COLOR:I

.field public static final DK_SOURCE_NAME:I

.field public static final DK_SOURCE_NAME_TEXT_COLOR:I

.field public static final DK_SUBSCRIBE_CLICKHANDLER:I

.field public static final DK_SUBSCRIBE_DESCRIPTION:I

.field public static final DK_SUBSCRIBE_ICON:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_cardBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_CARD_BACKGROUND:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_NAME:I

    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceNameTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_NAME_TEXT_COLOR:I

    .line 39
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    .line 41
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_BACKGROUND:I

    .line 43
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceInitials:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS:I

    .line 45
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceInitialsBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_BACKGROUND:I

    .line 48
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceInitialsTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_TEXT_COLOR:I

    .line 51
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_DESCRIPTION:I

    .line 53
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceDescriptionTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_DESCRIPTION_TEXT_COLOR:I

    .line 56
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_subscribeClickHandler:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_CLICKHANDLER:I

    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_CLICKHANDLER:I

    .line 60
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_subscribeIcon:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_ICON:I

    .line 62
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_subscribeDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_DESCRIPTION:I

    .line 64
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_cardTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_CARD_TRANSITION_NAME:I

    .line 66
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceListItem_sourceIconTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_TRANSITION_NAME:I

    .line 69
    new-array v0, v2, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->EQUALITY_FIELDS:[I

    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_source_list_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public static fillInData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZLandroid/accounts/Account;ZZLcom/google/android/libraries/bind/data/Data;Landroid/view/View$OnClickListener;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p2, "isSubscribed"    # Z
    .param p3, "isPurchased"    # Z
    .param p4, "account"    # Landroid/accounts/Account;
    .param p5, "fillInDescription"    # Z
    .param p6, "useDarkTheme"    # Z
    .param p7, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p8, "sourceOnClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const v4, 0x106000b

    const/4 v6, 0x7

    .line 126
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_NAME:I

    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 127
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconImage()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 128
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    .line 127
    :goto_0
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 131
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_TRANSITION_NAME:I

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 136
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->image_loading:I

    .line 137
    .local v2, "sourceBackgroundId":I
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v1

    .line 138
    .local v1, "editionType":I
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconImage()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 139
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 161
    :cond_0
    :goto_1
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_BACKGROUND:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 163
    if-ne v1, v6, :cond_1

    .line 164
    sget v3, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_USE_ROUNDED_SOURCE_ICON:I

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 165
    sget v3, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 167
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;

    move-result-object v5

    .line 166
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 165
    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 168
    sget v3, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_INSET:I

    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$dimen;->card_source_list_item_topic_icon_inset:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 168
    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 173
    :cond_1
    if-eqz p5, :cond_2

    .line 174
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "description":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 176
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_DESCRIPTION:I

    invoke-virtual {p7, v3, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 180
    .end local v0    # "description":Ljava/lang/String;
    :cond_2
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_CLICKHANDLER:I

    invoke-virtual {p7, v3, p8}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 182
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_ICON:I

    if-eqz p2, :cond_a

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_check_circle_card_24dp:I

    .line 183
    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 182
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 184
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_DESCRIPTION:I

    if-eqz p2, :cond_b

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->unsubscribe:I

    .line 185
    :goto_3
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 184
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 188
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_CARD_BACKGROUND:I

    if-eqz p6, :cond_c

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_dark:I

    .line 189
    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 188
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 190
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_NAME_TEXT_COLOR:I

    if-eqz p6, :cond_d

    move v3, v4

    .line 191
    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 190
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 192
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_DESCRIPTION_TEXT_COLOR:I

    if-eqz p6, :cond_e

    .line 193
    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 192
    invoke-virtual {p7, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 195
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SUBSCRIBE_CLICKHANDLER:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;

    invoke-direct {v4, p2, p4, p1, p3}, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem$1;-><init>(ZLandroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Z)V

    invoke-virtual {p7, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 208
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_CARD_TRANSITION_NAME:I

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v4, v4, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {p7, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 209
    return-void

    .line 128
    .end local v1    # "editionType":I
    .end local v2    # "sourceBackgroundId":I
    :cond_3
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 129
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 140
    .restart local v1    # "editionType":I
    .restart local v2    # "sourceBackgroundId":I
    :cond_4
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconAttachmentId()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 141
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 142
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 143
    :cond_5
    if-ne v1, v6, :cond_8

    .line 144
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS:I

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 145
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_TEXT_COLOR:I

    if-eqz p2, :cond_6

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_label_text_white:I

    .line 146
    :goto_7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 145
    invoke-virtual {p7, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 147
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    invoke-virtual {p7, v3}, Lcom/google/android/libraries/bind/data/Data;->remove(I)V

    .line 148
    if-eqz p2, :cond_7

    .line 149
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_BACKGROUND:I

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 150
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;

    move-result-object v5

    .line 149
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 145
    :cond_6
    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background_unsubscribed_text:I

    goto :goto_7

    .line 152
    :cond_7
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->default_gray_with_gray_highlight:I

    goto/16 :goto_1

    .line 154
    :cond_8
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 156
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    const-string v5, ""

    invoke-virtual {p7, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 157
    if-eqz p2, :cond_9

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v3, v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 158
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getRssDrawableId(Ljava/lang/String;)I

    move-result v2

    :goto_8
    goto/16 :goto_1

    :cond_9
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_rss_default:I

    goto :goto_8

    .line 182
    :cond_a
    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_add_circle_card_24dp:I

    goto/16 :goto_2

    .line 184
    :cond_b
    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->subscribe:I

    goto/16 :goto_3

    .line 188
    :cond_c
    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background:I

    goto/16 :goto_4

    .line 190
    :cond_d
    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_source_list_item_title_text:I

    goto/16 :goto_5

    .line 192
    :cond_e
    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->play_secondary_text:I

    goto/16 :goto_6
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 95
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->source_icon_no_image:I

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 99
    .local v3, "initialsView":Lcom/google/apps/dots/android/newsstand/widget/NSTextView;
    if-eqz v3, :cond_0

    .line 101
    if-eqz p1, :cond_1

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_BACKGROUND:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 102
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_INITIALS_BACKGROUND:I

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 103
    .local v1, "backgroundColorId":I
    sget v4, Lcom/google/android/apps/newsstanddev/R$drawable;->solid_color_circle_bg:I

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setBackgroundResource(I)V

    .line 104
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 105
    .local v0, "background":Landroid/graphics/drawable/LayerDrawable;
    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->solid_circle:I

    .line 106
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/GradientDrawable;

    .line 107
    .local v2, "circle":Landroid/graphics/drawable/GradientDrawable;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 112
    .end local v0    # "background":Landroid/graphics/drawable/LayerDrawable;
    .end local v1    # "backgroundColorId":I
    .end local v2    # "circle":Landroid/graphics/drawable/GradientDrawable;
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setBackgroundResource(I)V

    goto :goto_0
.end method
