.class public abstract Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;
.super Landroid/widget/LinearLayout;
.source "GroupRowSingleRowLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;


# instance fields
.field protected shouldEnlarge:Z

.field protected shouldShrink:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method private updateShouldShrink()V
    .locals 4

    .prologue
    .line 92
    const/4 v1, 0x1

    .line 93
    .local v1, "childrenCanShrink":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 94
    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getContainedCardAt(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 95
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_1

    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    if-eqz v3, :cond_1

    .line 96
    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;

    .end local v0    # "childView":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/card/ResizingCard;->canShrink()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 93
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 96
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 98
    .restart local v0    # "childView":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    .line 102
    .end local v0    # "childView":Landroid/view/View;
    :cond_2
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->shouldShrink:Z

    .line 103
    return-void
.end method


# virtual methods
.method public arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I
    .locals 5
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->getNumCards()I

    move-result v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, p3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 83
    .local v1, "numCardsToFill":I
    new-array v2, v1, [I

    .line 84
    .local v2, "returnArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 85
    add-int v3, p3, v0

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    return-object v2
.end method

.method protected getDesiredHeightMeasureSpec(I)I
    .locals 2
    .param p1, "desiredRowHeight"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->updateShouldShrink()V

    .line 58
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->shouldShrink()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, -0x80000000

    .line 59
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 61
    .local v0, "returnSpec":I
    :goto_0
    return v0

    .line 59
    .end local v0    # "returnSpec":I
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    .line 60
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method protected abstract getNumCards()I
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 39
    const-string v1, "GroupRowSingleRowLayout.onMeasure"

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 41
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getIdealRowHeight()I

    move-result v0

    .line 42
    .local v0, "desiredRowHeight":I
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->getDesiredHeightMeasureSpec(I)I

    move-result p2

    .line 43
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 44
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 45
    return-void
.end method

.method public prepareGroupRowForRecycling()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 117
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->shouldEnlarge:Z

    .line 118
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->shouldShrink:Z

    .line 119
    return-void
.end method

.method public shouldEnlarge()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public shouldShrink()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleRowLayout;->shouldShrink:Z

    return v0
.end method
