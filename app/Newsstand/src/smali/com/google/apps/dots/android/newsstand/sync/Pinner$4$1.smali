.class Lcom/google/apps/dots/android/newsstand/sync/Pinner$4$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;
.source "Pinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->getEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 7
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 485
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    iget-object v2, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 487
    iget-object v3, p2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 488
    .local v0, "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 489
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v5

    .line 490
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v6

    .line 489
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 491
    .local v1, "editionToAdd":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v1, :cond_0

    .line 492
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4$1;->this$1:Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;

    iget-object v5, v5, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->editions:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 487
    .end local v1    # "editionToAdd":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 497
    .end local v0    # "appNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_1
    return-void
.end method
