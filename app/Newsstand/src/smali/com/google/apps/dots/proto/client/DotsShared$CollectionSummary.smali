.class public final Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CollectionSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;


# instance fields
.field private bitField0_:I

.field private collectionId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30044
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30045
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30050
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    .line 30045
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    .locals 3

    .prologue
    .line 30082
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30086
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    return-object v0

    .line 30083
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    :catch_0
    move-exception v1

    .line 30084
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30041
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30091
    if-ne p1, p0, :cond_1

    .line 30094
    :cond_0
    :goto_0
    return v1

    .line 30092
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 30093
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    .line 30094
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCollectionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30052
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 30113
    const/4 v0, 0x0

    .line 30114
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30115
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    .line 30116
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30118
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->cachedSize:I

    .line 30119
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 30098
    const/16 v0, 0x11

    .line 30099
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 30100
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 30101
    return v0

    .line 30100
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 30128
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 30132
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30133
    :sswitch_0
    return-object p0

    .line 30138
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    .line 30139
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->bitField0_:I

    goto :goto_0

    .line 30128
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30041
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30106
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30107
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->collectionId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30109
    :cond_0
    return-void
.end method
