.class Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "OffersList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;->processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;
    .param p2, "primaryKey"    # I

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    return-void
.end method

.method private makeOfferData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 43
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "Offers"

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->fillOffersCardData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;ZLjava/lang/String;)V

    .line 48
    return-object v0
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 53
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v1

    .line 54
    .local v1, "storeType":I
    const/4 v0, 0x0

    .line 55
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 57
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;->makeOfferData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 60
    :cond_1
    if-eqz v0, :cond_2

    .line 61
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 63
    :cond_2
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
