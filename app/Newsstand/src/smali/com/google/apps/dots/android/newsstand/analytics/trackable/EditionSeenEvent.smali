.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.source "EditionSeenEvent.java"


# instance fields
.field private final readingScreen:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "readingScreen"    # Ljava/lang/String;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 20
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 21
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->readingScreen:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 65
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;

    .line 67
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->readingScreen:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->readingScreen:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 68
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 70
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 3
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->getOriginalEditionSummary()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 35
    .local v0, "originalEditionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const-string v1, "Edition Seen"

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 36
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 37
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 38
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 39
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 40
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 41
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setUserSubscriptionType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 43
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 45
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 55
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->readingScreen:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->readingScreen:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionSeenEvent;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method
