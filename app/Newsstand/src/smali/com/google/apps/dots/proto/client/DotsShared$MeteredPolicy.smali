.class public final Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MeteredPolicy"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;


# instance fields
.field private bitField0_:I

.field public maxArticleCount:I

.field public period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

.field private startTime_:J

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27587
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27588
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .line 27601
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    .line 27604
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    .line 27623
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    .line 27588
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    .locals 3

    .prologue
    .line 27655
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27659
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-eqz v2, :cond_0

    .line 27660
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Period;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .line 27662
    :cond_0
    return-object v0

    .line 27656
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    :catch_0
    move-exception v1

    .line 27657
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27584
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27667
    if-ne p1, p0, :cond_1

    .line 27670
    :cond_0
    :goto_0
    return v1

    .line 27668
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 27669
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 27670
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 27702
    const/4 v0, 0x0

    .line 27703
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-eqz v1, :cond_0

    .line 27704
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .line 27705
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27707
    :cond_0
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    .line 27708
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27709
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 27710
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    .line 27711
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27713
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 27714
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    .line 27715
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27717
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->cachedSize:I

    .line 27718
    return v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 27625
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 27677
    const/16 v0, 0x11

    .line 27678
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 27679
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 27680
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    add-int v0, v1, v2

    .line 27681
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    add-int v0, v1, v2

    .line 27682
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 27683
    return v0

    .line 27679
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27726
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 27727
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 27731
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 27732
    :sswitch_0
    return-object p0

    .line 27737
    :sswitch_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-nez v2, :cond_1

    .line 27738
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Period;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .line 27740
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 27744
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    goto :goto_0

    .line 27748
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 27749
    .local v1, "temp":I
    if-eqz v1, :cond_2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 27751
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    .line 27752
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    goto :goto_0

    .line 27754
    :cond_3
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    goto :goto_0

    .line 27759
    .end local v1    # "temp":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    .line 27760
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    goto :goto_0

    .line 27727
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27584
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27688
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    if-eqz v0, :cond_0

    .line 27689
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 27691
    :cond_0
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27692
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 27693
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 27695
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 27696
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->startTime_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 27698
    :cond_2
    return-void
.end method
