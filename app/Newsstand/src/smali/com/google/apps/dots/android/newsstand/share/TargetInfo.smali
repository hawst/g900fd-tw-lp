.class public Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
.super Ljava/lang/Object;
.source "TargetInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;,
        Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;
    }
.end annotation


# instance fields
.field public final appFamilyId:Ljava/lang/String;

.field public final appId:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

.field public final offerId:Ljava/lang/String;

.field public final postId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;Ljava/lang/String;)V
    .locals 0
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "offerId"    # Ljava/lang/String;
    .param p5, "location"    # Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;
    .param p6, "description"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 47
    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->description:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 64
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    .line 66
    .local v0, "target":Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    .line 67
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    .line 68
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    .line 69
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->description:Ljava/lang/String;

    .line 71
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 73
    .end local v0    # "target":Lcom/google/apps/dots/android/newsstand/share/TargetInfo;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    .line 53
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "appFamilyId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "appId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "postId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "offerId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "location"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 58
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
