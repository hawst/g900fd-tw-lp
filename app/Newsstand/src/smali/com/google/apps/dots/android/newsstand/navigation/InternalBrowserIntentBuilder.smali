.class public Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "InternalBrowserIntentBuilder.java"


# instance fields
.field private final intent:Landroid/content/Intent;

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 19
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/BrowserActivity;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->intent:Landroid/content/Intent;

    .line 20
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public setUri(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/InternalBrowserIntentBuilder;->uri:Landroid/net/Uri;

    .line 26
    :cond_0
    return-object p0
.end method
