.class public Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;
.super Ljava/lang/Object;
.source "ViewPropertyTransitions.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;,
        Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;,
        Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;
    }
.end annotation


# direct methods
.method public static colorFilter(IIZ)Landroid/transition/Transition;
    .locals 3
    .param p0, "startColor"    # I
    .param p1, "endColor"    # I
    .param p2, "isEntering"    # Z

    .prologue
    .line 84
    new-instance v0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    return-object v0
.end method

.method public static elevation(FZ)Landroid/transition/Transition;
    .locals 3
    .param p0, "startElevation"    # F
    .param p1, "isEntering"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$2;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$2;-><init>()V

    .line 75
    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public static fadeAlpha(ZZ)Landroid/transition/Transition;
    .locals 3
    .param p0, "fadeIn"    # Z
    .param p1, "isEntering"    # Z

    .prologue
    .line 35
    new-instance v1, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;-><init>()V

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    .line 51
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v1, v2, v0, p1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;Ljava/lang/Object;Z)V

    return-object v1

    .line 35
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method
