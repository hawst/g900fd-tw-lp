.class public final Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;
.super Ljava/lang/Object;
.source "ContinuationTraverser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;,
        Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field private final root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

.field private final token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 1
    .param p1, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    .line 60
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    .line 61
    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
    .locals 1
    .param p1, "mutationStore"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p2, "token"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "root"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, "Mutation store cannot be null"

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 66
    const-string v0, "Async token cannot be null"

    invoke-static {p2, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 67
    const-string v0, "Collection root cannot be null"

    invoke-static {p3, v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 68
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method


# virtual methods
.method public traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
            "<-",
            "Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "visitor":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor<-Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->root:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->mutationStore:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 82
    return-void
.end method
