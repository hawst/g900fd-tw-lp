.class final Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "SubscriptionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->addSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field final synthetic val$provideReadNow:Z

.field final synthetic val$showToast:Z


# direct methods
.method constructor <init>(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZLandroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$showToast:Z

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$provideReadNow:Z

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x1

    .line 80
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$showToast:Z

    if-eqz v2, :cond_1

    .line 81
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->edition_added:I

    new-array v4, v8, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 82
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 81
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    .line 84
    .local v1, "readNowOperation":Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$provideReadNow:Z

    if-eqz v2, :cond_0

    .line 85
    new-instance v1, Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;

    .end local v1    # "readNowOperation":Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$account:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 87
    .restart local v1    # "readNowOperation":Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;->val$activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-static {v2, v0, v1, v8}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V

    .line 89
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "readNowOperation":Lcom/google/apps/dots/android/newsstand/toast/ReadEditionNowToastOperation;
    :cond_1
    return-void
.end method
