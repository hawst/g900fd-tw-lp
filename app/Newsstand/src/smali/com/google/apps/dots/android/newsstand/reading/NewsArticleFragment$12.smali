.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 3
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 527
    if-eqz p1, :cond_0

    .line 528
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    .line 529
    .local v0, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->account()Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$900(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->unsavePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 531
    .end local v0    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 524
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$12;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
