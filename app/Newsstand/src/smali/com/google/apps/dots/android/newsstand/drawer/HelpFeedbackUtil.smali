.class public Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;
.super Ljava/lang/Object;
.source "HelpFeedbackUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addGlobalInfoToBundle(Landroid/app/Activity;Landroid/accounts/Account;Landroid/os/Bundle;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "outBundle"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    .line 97
    .local v0, "serverUris":Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    const-string v1, "appName"

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->app_name:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v2, "apiUrl"

    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {p2, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->addLocalLogsIfAllowed(Landroid/app/Activity;Landroid/os/Bundle;Landroid/accounts/Account;)V

    .line 101
    return-void

    .line 98
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getBaseUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static addLocalLogs(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 13
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "outBundle"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/high16 v12, 0x40000

    .line 121
    new-instance v3, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;-><init>(Landroid/app/Activity;)V

    .line 122
    .local v3, "feedbackInfo":Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;
    const/4 v6, 0x0

    .line 124
    .local v6, "localLogByteCount":I
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/feedback/FeedbackInformation;->getLocalLogs()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 125
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    if-lt v6, v12, :cond_1

    .line 147
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    :cond_0
    :goto_1
    return-void

    .line 128
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 130
    .local v4, "fileBytes":[B
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x13

    if-lt v9, v11, :cond_2

    .line 131
    new-instance v5, Ljava/lang/String;

    sget-object v9, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v5, v4, v9}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 140
    .local v5, "fileText":Ljava/lang/String;
    :goto_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    sub-int v11, v12, v6

    div-int/lit8 v11, v11, 0x2

    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 142
    .local v0, "charsOut":I
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    sub-int v8, v9, v0

    .line 143
    .local v8, "startIndex":I
    add-int v9, v0, v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 144
    .local v7, "outText":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p1, v9, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    mul-int/lit8 v9, v0, 0x2

    add-int/2addr v6, v9

    .line 146
    goto :goto_0

    .line 134
    .end local v0    # "charsOut":I
    .end local v5    # "fileText":Ljava/lang/String;
    .end local v7    # "outText":Ljava/lang/String;
    .end local v8    # "startIndex":I
    :cond_2
    :try_start_0
    new-instance v5, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-direct {v5, v4, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v5    # "fileText":Ljava/lang/String;
    goto :goto_2

    .line 135
    .end local v5    # "fileText":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 136
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_1
.end method

.method private static addLocalLogsIfAllowed(Landroid/app/Activity;Landroid/os/Bundle;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "outBundle"    # Landroid/os/Bundle;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 107
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->submit_local_logs:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 108
    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->addLocalLogs(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 111
    :cond_1
    return-void
.end method

.method private static getBitmap(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 79
    instance-of v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    const-string v3, "Help should only be called from an instance of NavigationDrawerActivity"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 83
    :try_start_0
    check-cast p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->getPrimaryFragmentView(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)Landroid/view/View;

    move-result-object v0

    .line 84
    .local v0, "currentView":Landroid/view/View;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 87
    :goto_0
    return-object v2

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getPrimaryFragmentView(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)Landroid/view/View;
    .locals 1
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static launchHelpFeedback(Landroid/app/Activity;)V
    .locals 14
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v13, 0x0

    .line 150
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/newsstanddev/R$string;->help_root_topic_uri:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "defaultUri":Ljava/lang/String;
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v7

    .line 152
    .local v7, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-object v9, p0

    .line 155
    check-cast v9, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v4

    .line 156
    .local v4, "feedbackBundle":Landroid/os/Bundle;
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string v9, "help_context_key"

    invoke-virtual {v4, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "contextId":Ljava/lang/String;
    sget-object v9, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v10, "Context set to %s with bundle %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v1, v11, v13

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 160
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->addGlobalInfoToBundle(Landroid/app/Activity;Landroid/accounts/Account;Landroid/os/Bundle;)V

    .line 161
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 162
    .local v3, "fallbackSupportUri":Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->newInstance(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v9

    .line 163
    invoke-virtual {v9, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v9

    .line 164
    invoke-virtual {v9, v4}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setFeedbackProductSpecificData(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v9

    .line 165
    invoke-virtual {v9, v3}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v5

    .line 166
    .local v5, "googleHelp":Lcom/google/android/gms/googlehelp/GoogleHelp;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->getBitmap(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 167
    .local v8, "screenShot":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_0

    .line 168
    invoke-virtual {v5, v8}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 170
    :cond_0
    invoke-virtual {v5, p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    .line 171
    .local v6, "helpIntent":Landroid/content/Intent;
    new-instance v9, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;

    invoke-direct {v9, p0}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v9, v6}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;->launch(Landroid/content/Intent;)V

    .line 172
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/StrictModeCompat;->setThreadPolicyDelayed(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 174
    check-cast p0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/drawer/HelpFeedbackUtil;->getPrimaryFragmentView(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 175
    return-void
.end method
