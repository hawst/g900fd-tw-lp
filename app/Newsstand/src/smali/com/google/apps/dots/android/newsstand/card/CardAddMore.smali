.class public Lcom/google/apps/dots/android/newsstand/card/CardAddMore;
.super Lcom/google/android/libraries/bind/widget/BindingLinearLayout;
.source "CardAddMore.java"


# static fields
.field public static final DK_ON_CLICK_LISTENER:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_add_more:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->LAYOUT:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardAddMore_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->DK_ON_CLICK_LISTENER:I

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method
