.class public Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;
.super Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;
.source "LayoutStore.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;)V
    .locals 1
    .param p1, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->LAYOUT_LINK:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCacheBase;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected parse(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;
    .locals 4
    .param p1, "storeResponse"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v2}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 25
    .local v0, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2}, Lcom/google/common/io/CharStreams;->toString(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "response":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit16 v3, v3, 0x400

    invoke-direct {v2, p1, v1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/CacheItem;-><init>(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v2

    .end local v1    # "response":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v2
.end method
