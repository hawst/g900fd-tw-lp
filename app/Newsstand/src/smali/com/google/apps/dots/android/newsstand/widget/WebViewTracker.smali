.class public Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;
.super Ljava/lang/Object;
.source "WebViewTracker.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private activityCount:I

.field private final appContext:Landroid/content/Context;

.field private hadWebViews:Z

.field private webViewCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->appContext:Landroid/content/Context;

    .line 36
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    .line 37
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    .line 38
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->hadWebViews:Z

    .line 39
    return-void
.end method

.method private clearWebViewData()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method


# virtual methods
.method public enterActivity()V
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    .line 61
    return-void
.end method

.method public enterWebView()V
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    .line 91
    return-void
.end method

.method public exitActivity()V
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->hadWebViews:Z

    if-eqz v0, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->clearWebViewData()V

    .line 71
    :cond_0
    return-void
.end method

.method public exitWebView()V
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->activityCount:I

    if-nez v0, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->clearWebViewData()V

    .line 101
    :cond_0
    return-void
.end method

.method public startWebView()V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->hadWebViews:Z

    if-nez v0, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->clearWebViewData()V

    .line 81
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->webViewCount:I

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/WebViewTracker;->hadWebViews:Z

    .line 83
    return-void
.end method
