.class public Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;
.super Landroid/app/Service;
.source "SyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private syncAdapter:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 89
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static enableGlobalSync()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    .line 55
    return-void
.end method

.method public static enablePeriodicSync(Landroid/accounts/Account;)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 49
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "authority":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 51
    return-void
.end method

.method private getSyncAdapter()Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->syncAdapter:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    invoke-direct {v0, p0, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->syncAdapter:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->syncAdapter:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    return-object v0
.end method

.method public static isGlobalSyncEnabled()Z
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    return v0
.end method

.method public static isPeriodicSyncEnabled(Landroid/accounts/Account;)Z
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "authority":Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static requestOneTimeSync(Landroid/accounts/Account;)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 58
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "authority":Ljava/lang/String;
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public static updatePeriodicSync(Landroid/accounts/Account;J)V
    .locals 13
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "syncPeriodMs"    # J

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->contentAuthority()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "authority":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 68
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Updating periodic sync interval to %.2f hours for %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    long-to-double v4, p1

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    div-double/2addr v4, v8

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v10

    aput-object p0, v3, v11

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-static {p0, v0, v1, v2, v3}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 75
    :goto_0
    return-void

    .line 72
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Disabling periodic sync for %s"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p0, v3, v10

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->getSyncAdapter()Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
