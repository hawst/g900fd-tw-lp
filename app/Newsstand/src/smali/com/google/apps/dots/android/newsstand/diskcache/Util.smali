.class public Lcom/google/apps/dots/android/newsstand/diskcache/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static final filenameEncoder:Lcom/google/common/io/BaseEncoding;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/common/io/BaseEncoding;->base32()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/io/BaseEncoding;->omitPadding()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/io/BaseEncoding;->lowerCase()Lcom/google/common/io/BaseEncoding;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncoder:Lcom/google/common/io/BaseEncoding;

    .line 33
    return-void
.end method

.method public static decodeUtf8([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    const/4 v3, 0x0

    .line 23
    if-eqz p0, :cond_0

    :try_start_0
    new-instance v2, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v2, p0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    move-object v3, v2

    .line 28
    :goto_1
    return-object v3

    :cond_0
    move-object v2, v3

    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-exception v1

    .line 25
    .local v1, "uee":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/Error;

    invoke-direct {v2, v1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 26
    .end local v1    # "uee":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 28
    .local v0, "tr":Ljava/lang/Throwable;
    goto :goto_1
.end method

.method public static encodeUtf8(Ljava/lang/String;)[B
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 14
    if-eqz p0, :cond_0

    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 15
    :catch_0
    move-exception v0

    .line 17
    .local v0, "uee":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static filenameDecode(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 3
    .param p0, "encodedFilename"    # Ljava/lang/String;

    .prologue
    .line 42
    :try_start_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "lowerCased":Ljava/lang/String;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncoder:Lcom/google/common/io/BaseEncoding;

    invoke-virtual {v2, v0}, Lcom/google/common/io/BaseEncoding;->decode(Ljava/lang/CharSequence;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->of([B)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 46
    .end local v0    # "lowerCased":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 44
    :catch_0
    move-exception v1

    .line 46
    .local v1, "tr":Ljava/lang/Throwable;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static filenameEncode(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Ljava/lang/String;
    .locals 2
    .param p0, "bytes"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 37
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/Util;->filenameEncoder:Lcom/google/common/io/BaseEncoding;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->bytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/io/BaseEncoding;->encode([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
