.class public Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
.super Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;
.source "NSSettableFuture.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private cancellable:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 0
    .param p1, "cancellable"    # Z

    .prologue
    .line 17
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture<TV;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;-><init>()V

    .line 18
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->cancellable:Z

    .line 19
    return-void
.end method

.method public static create(Z)Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;
    .locals 1
    .param p0, "cancellable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 33
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture<TV;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;->cancellable:Z

    if-nez v0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->cancel(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public set(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture<TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->set(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setException(Ljava/lang/Throwable;)Z
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 28
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSSettableFuture<TV;>;"
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->setException(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method
