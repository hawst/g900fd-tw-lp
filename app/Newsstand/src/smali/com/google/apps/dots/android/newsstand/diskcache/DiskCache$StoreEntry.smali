.class Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;
.super Ljava/lang/Object;
.source "DiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StoreEntry"
.end annotation


# instance fields
.field volatile lastAccessedTime:J

.field final lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

.field final storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .param p2, "lb"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .param p3, "storeFile"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 76
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .line 77
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 81
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "range"

    const-string v2, "StoreEntry[%s, %s): "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lb:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 82
    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getUpper(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->access$000(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "storeFile"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->storeFile:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastAccessedTime"

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lastAccessedTime:J

    .line 84
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method touch()V
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$StoreEntry;->lastAccessedTime:J

    .line 72
    return-void
.end method
