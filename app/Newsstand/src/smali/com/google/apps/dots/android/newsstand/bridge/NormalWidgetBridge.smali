.class public Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;
.super Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;
.source "NormalWidgetBridge.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 7
    .param p1, "context"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "articleWidget"    # Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p5, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 30
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/bridge/BaseArticleWidgetBridge;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 31
    return-void
.end method


# virtual methods
.method public openAudio(Ljava/lang/String;)V
    .locals 1
    .param p1, "audioUri"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 36
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 39
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$1;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public switchToApp(Ljava/lang/String;)V
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 65
    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "undefined"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "switchToApp() with bad appId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :goto_0
    return-void

    .line 70
    :cond_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$3;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->safePostIfAllowed(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public toggleActionBar()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge$2;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/bridge/NormalWidgetBridge;->safePost(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method
