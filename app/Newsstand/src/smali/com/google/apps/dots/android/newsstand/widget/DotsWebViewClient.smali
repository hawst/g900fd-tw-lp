.class public Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "DotsWebViewClient.java"


# static fields
.field private static final CORS_RESPONSE_HEADER_MAP:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final NS_CONTENT_URI_SCHEME:Ljava/lang/String; = "newsstand-content"


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private webviewUserAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 45
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const-string v1, "Access-Control-Allow-Origin"

    const-string v2, "*"

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->CORS_RESPONSE_HEADER_MAP:Lcom/google/common/collect/ImmutableMap;

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 53
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "webviewUserAgent"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 58
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->webviewUserAgent:Ljava/lang/String;

    .line 59
    return-void
.end method

.method private getCORSModifiedResponseForUri(Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 205
    new-instance v9, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {v9, v0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 207
    .local v9, "provider":Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "content"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 210
    :try_start_0
    invoke-virtual {v9, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v7

    .line 211
    .local v7, "contentInputStream":Ljava/io/InputStream;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;-><init>(Ljava/io/InputStream;)V

    .line 212
    .local v6, "inputStream":Ljava/io/InputStream;
    invoke-virtual {v9, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "contentType":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 214
    const-string v1, "application/octet-stream"

    .line 219
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v0, v2, :cond_1

    .line 220
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const/4 v2, 0x0

    const/16 v3, 0xc8

    const-string v4, "OK"

    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->CORS_RESPONSE_HEADER_MAP:Lcom/google/common/collect/ImmutableMap;

    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Ljava/io/InputStream;)V

    .line 234
    .end local v1    # "contentType":Ljava/lang/String;
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "contentInputStream":Ljava/io/InputStream;
    :goto_0
    return-object v0

    .line 223
    .restart local v1    # "contentType":Ljava/lang/String;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "contentInputStream":Ljava/io/InputStream;
    :cond_1
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 225
    .end local v1    # "contentType":Ljava/lang/String;
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v7    # "contentInputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v8

    .line 226
    .local v8, "e":Ljava/io/FileNotFoundException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Exception when opening File at Uri: %s"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v11

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    new-instance v0, Landroid/webkit/WebResourceResponse;

    invoke-direct {v0, v10, v10, v10}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0

    .line 228
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v8

    .line 233
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Exception when parsing Uri: %s"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v11

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    new-instance v0, Landroid/webkit/WebResourceResponse;

    invoke-direct {v0, v10, v10, v10}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method private getResponseForUrl(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 10
    .param p1, "url"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 172
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 174
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;->get(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 177
    .local v2, "clientFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;>;"
    :try_start_0
    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    .line 178
    .local v1, "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->webviewUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->getResource(Ljava/net/URL;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v4

    .line 190
    .end local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    :goto_0
    return-object v4

    .line 179
    :catch_0
    move-exception v3

    .line 180
    .local v3, "e":Ljava/net/MalformedURLException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to fetch URL %s: %s"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    aput-object v3, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 182
    .end local v3    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 183
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to fetch URL %s: %s"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    aput-object v3, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 185
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 186
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to create http client for webview: %s"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v3, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v3

    .line 189
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to create http client for webview: %s"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v3, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private loadRedirectUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/util/Set;)V
    .locals 12
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/activity/NSActivity;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/net/URL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "optPrevRedirectUrls":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/URL;>;"
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 141
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "loadRedirectUrl(%s)"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object p2, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 144
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webviewHttpClientPool()Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;->get(Landroid/accounts/Account;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 145
    .local v2, "clientFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;>;"
    if-nez p3, :cond_0

    .line 146
    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object p3

    .line 149
    :cond_0
    :try_start_0
    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    .line 150
    .local v1, "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->webviewUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->getRedirectLocation(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v4

    .line 151
    .local v4, "redirect":Ljava/net/URL;
    if-nez v4, :cond_1

    .line 152
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error - %s redirects to nowhere"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    .end local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .end local v4    # "redirect":Ljava/net/URL;
    :goto_0
    return-void

    .line 153
    .restart local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .restart local v4    # "redirect":Ljava/net/URL;
    :cond_1
    invoke-interface {p3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 154
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error - detected redirect loop %s -> %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 159
    .end local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .end local v4    # "redirect":Ljava/net/URL;
    :catch_0
    move-exception v3

    .line 160
    .local v3, "e":Ljava/net/MalformedURLException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to fetch redirect URL %s: %s"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object p2, v7, v9

    aput-object v3, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 156
    .end local v3    # "e":Ljava/net/MalformedURLException;
    .restart local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .restart local v4    # "redirect":Ljava/net/URL;
    :cond_2
    :try_start_1
    invoke-interface {p3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p1, v5, p3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->loadUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/util/Set;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 161
    .end local v1    # "client":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
    .end local v4    # "redirect":Ljava/net/URL;
    :catch_1
    move-exception v3

    .line 162
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to fetch redirect URL %s: %s"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object p2, v7, v9

    aput-object v3, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 163
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 164
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to create http client for webview: %s"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v3, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v3

    .line 166
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Error while trying to create http client for webview: %s"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v3, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected loadUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/util/Set;)V
    .locals 4
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/activity/NSActivity;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/net/URL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p3, "optPrevRedirectUrls":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/URL;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "loadUrl(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->loadRedirectUrl(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;Ljava/util/Set;)V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->show(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onLayoutChange(IZII)V
    .locals 0
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    .line 75
    return-void
.end method

.method public onReady()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "oldScale"    # F
    .param p3, "newScale"    # F

    .prologue
    .line 125
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    if-eqz v0, :cond_0

    .line 126
    check-cast p1, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;

    .end local p1    # "view":Landroid/webkit/WebView;
    invoke-virtual {p1, p3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebView;->updateScale(F)V

    .line 128
    :cond_0
    return-void
.end method

.method public setWebviewUserAgent(Ljava/lang/String;)V
    .locals 4
    .param p1, "webviewUserAgent"    # Ljava/lang/String;

    .prologue
    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "setWebviewUserAgent(\"%s\")"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->webviewUserAgent:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 9
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 80
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "shouldInterceptRequest? %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p2, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    if-eqz p2, :cond_2

    .line 82
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 83
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/google/common/collect/Iterables;->getFirst(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 89
    .local v0, "firstSegment":Ljava/lang/String;
    const-string v3, "android_asset"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android_res"

    .line 90
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 91
    new-instance v3, Landroid/webkit/WebResourceResponse;

    invoke-direct {v3, v6, v6, v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 108
    .end local v0    # "firstSegment":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v3

    .line 94
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->maybeRewriteWebviewRequestUri(Landroid/webkit/WebView;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 95
    .local v1, "newUri":Landroid/net/Uri;
    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 97
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "rewrote uri to %s and fetching manually"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->getResponseForUrl(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v3

    goto :goto_0

    .line 99
    :cond_1
    const-string v3, "newsstand-content"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->getCORSModifiedResponseForUri(Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;

    move-result-object v3

    goto :goto_0

    .line 107
    .end local v1    # "newUri":Landroid/net/Uri;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_2
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "returning super.shouldInterceptRequest(%s)"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p2, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v3

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 113
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "shouldOverrideUrlLoading %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->NETWORK_API:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;Landroid/webkit/WebView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/Queue;->execute(Ljava/lang/Runnable;)V

    .line 120
    return v4
.end method
