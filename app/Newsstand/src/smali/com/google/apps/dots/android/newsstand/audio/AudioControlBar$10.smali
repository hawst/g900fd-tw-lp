.class Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;
.super Ljava/lang/Object;
.source "AudioControlBar.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setupSeekBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->progressText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$1100(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/TextView;

    move-result-object v0

    div-int/lit16 v1, p2, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isTrackingTouch:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$902(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;Z)Z

    .line 286
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 288
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 290
    :cond_0
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isTrackingTouch:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$902(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;Z)Z

    .line 276
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->seekAudio(Landroid/content/Context;I)V

    .line 277
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideThumbRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 280
    :cond_0
    return-void
.end method
