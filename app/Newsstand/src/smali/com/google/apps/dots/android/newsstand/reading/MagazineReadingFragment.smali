.class public Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "MagazineReadingFragment.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;
.implements Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;,
        Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;",
        ">;",
        "Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;",
        "Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;"
    }
.end annotation


# static fields
.field public static final DK_BACKGROUND_COLOR:I

.field public static final DK_POST_ID:I

.field public static final DK_POST_ORIGINAL_EDITION:I

.field public static final DK_POST_SUMMARY:I

.field public static final DK_SECTION_SUMMARY:I

.field public static final EQUALITY_FIELDS:[I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private allPostsList:Lcom/google/android/libraries/bind/data/DataList;

.field private altFormats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;",
            ">;"
        }
    .end annotation
.end field

.field private connectivityListener:Ljava/lang/Runnable;

.field private final editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

.field private ignoreGotoArticlePage:Z

.field private lastVisualPageSelected:I

.field private magazinePostReadingFilter:Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

.field private pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

.field private pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

.field private pager:Landroid/support/v4/view/NSViewPager;

.field private pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

.field private final pagerObserver:Landroid/database/DataSetObserver;

.field private postReadingList:Lcom/google/android/libraries/bind/data/DataList;

.field private postReadingListRefreshed:Z

.field private final postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private preloadOrder:[I

.field private restrictToLiteMode:Z

.field private sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private toolbar:Landroid/support/v7/widget/Toolbar;

.field private transitionPlaceholderView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 115
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 123
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    .line 124
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    .line 126
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->MagazineReadingFragment_sectionSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    .line 128
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_postOriginalEdition:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    .line 129
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ReadingFragment_backgroundColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_BACKGROUND_COLOR:I

    .line 131
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_BACKGROUND_COLOR:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 207
    const/4 v0, 0x0

    const-string v1, "MagazineReadingFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->magazine_reading_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 142
    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    .line 156
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 158
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 159
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    .line 166
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I

    .line 169
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerObserver:Landroid/database/DataSetObserver;

    .line 208
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->resolvePostIdIfNeeded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateErrorView()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateRestrictToLiteMode()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onBackToEdition()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;ILcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->createArticleWidget(ILcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onPagerDestroyedView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->gotoRightArticle()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getMagazineImageAttachmentId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->loadArticlesIfNeeded()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->markPostAsReadIfNeeded()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateAltFormats()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeConfigureOrientationPreference()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateMenu()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    return-object v0
.end method

.method private articleLoader()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .locals 2

    .prologue
    .line 427
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 428
    .local v0, "postData":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 430
    :goto_0
    return-object v1

    .line 428
    :cond_0
    sget v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_LOADER:I

    .line 429
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;

    .line 430
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v1

    goto :goto_0
.end method

.method private changePageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V
    .locals 5
    .param p1, "pageLocation"    # Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    .prologue
    const/4 v4, 0x1

    .line 1036
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->ignoreGotoArticlePage:Z

    .line 1037
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 1038
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v3

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 1039
    .local v0, "newState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    invoke-virtual {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 1040
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->ignoreGotoArticlePage:Z

    .line 1041
    return-void
.end method

.method private createArticleWidget(ILcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    .locals 15
    .param p1, "logicalPosition"    # I
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 918
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    move/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v14

    .line 919
    .local v14, "visualPosition":I
    const/4 v9, 0x0

    .line 920
    .local v9, "loadingArticleWidget":Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 921
    .local v11, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 922
    .local v12, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasNativeBodySummary()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 924
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNativeBodySummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->getNativeBodyVersion()I

    move-result v2

    .line 923
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyUtil;->isSupportedVersion(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 925
    new-instance v10, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .line 926
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v3

    invoke-direct {v10, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V

    .line 932
    .local v10, "nativeBodyWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    iget-object v2, v11, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 933
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageFraction()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageFraction()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 934
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageNumber()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageNumber()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_2

    .line 935
    :cond_1
    const/4 v2, 0x4

    invoke-virtual {v10, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->setVisibility(I)V

    .line 939
    :cond_2
    new-instance v9, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .line 940
    .end local v9    # "loadingArticleWidget":Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-direct {v9, v2, v10, v3}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 946
    .restart local v9    # "loadingArticleWidget":Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_5

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    .line 947
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 946
    :goto_0
    invoke-virtual {v9, v2}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setBackgroundColor(I)V

    .line 952
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I

    add-int/lit8 v3, v14, -0x1

    if-ne v2, v3, :cond_6

    .line 953
    const/4 v2, -0x1

    invoke-virtual {v10, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToEdge(I)V

    .line 994
    .end local v10    # "nativeBodyWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 995
    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->EQUALITY_FIELDS:[I

    .line 994
    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setDataRow(Lcom/google/android/libraries/bind/data/FilteredDataRow;)V

    .line 999
    invoke-virtual {v9, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 1000
    invoke-virtual {v9, p0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 1001
    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_BACKGROUND_COLOR:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1002
    .local v8, "backgroundColor":Ljava/lang/Integer;
    if-eqz v8, :cond_4

    .line 1003
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v9, v2}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setBackgroundColor(I)V

    .line 1008
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$10;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 1014
    return-object v9

    .line 947
    .end local v8    # "backgroundColor":Ljava/lang/Integer;
    .restart local v10    # "nativeBodyWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    :cond_5
    const/high16 v2, -0x1000000

    goto :goto_0

    .line 954
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I

    add-int/lit8 v3, v14, 0x1

    if-ne v2, v3, :cond_3

    .line 955
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToEdge(I)V

    goto :goto_1

    .line 959
    .end local v10    # "nativeBodyWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    :cond_7
    if-eqz v12, :cond_9

    .line 960
    invoke-virtual {v12}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasLayoutEngineVersionOverride()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 961
    invoke-virtual {v12}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getLayoutEngineVersionOverride()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    const/4 v6, 0x1

    .line 964
    .local v6, "usesLegacyLayoutEngine":Z
    :goto_2
    if-eqz v11, :cond_a

    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 967
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f19999a    # 0.6f

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    .line 970
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    .line 971
    invoke-virtual {v11}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v3

    div-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/NSViewPager;->getHeight()I

    move-result v3

    if-lt v2, v3, :cond_a

    const/4 v13, 0x1

    .line 972
    .local v13, "usesTopSplashImage":Z
    :goto_3
    if-nez v6, :cond_b

    if-eqz v13, :cond_b

    const/4 v7, 0x1

    .line 974
    .local v7, "enableDownArrow":Z
    :goto_4
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    .line 975
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    .line 976
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    move/from16 v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;IZZ)V

    .line 978
    .local v1, "articleWidget":Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
    new-instance v9, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .line 979
    .end local v9    # "loadingArticleWidget":Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-direct {v9, v2, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 981
    .restart local v9    # "loadingArticleWidget":Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;
    if-eqz v6, :cond_8

    .line 984
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_c

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    .line 985
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x0

    .line 984
    :goto_5
    invoke-virtual {v9, v2}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setBackgroundColor(I)V

    .line 990
    :cond_8
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_3

    .line 991
    const-string v2, " "

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->setTransitionName(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 961
    .end local v1    # "articleWidget":Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
    .end local v6    # "usesLegacyLayoutEngine":Z
    .end local v7    # "enableDownArrow":Z
    .end local v13    # "usesTopSplashImage":Z
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 971
    .restart local v6    # "usesLegacyLayoutEngine":Z
    :cond_a
    const/4 v13, 0x0

    goto :goto_3

    .line 972
    .restart local v13    # "usesTopSplashImage":Z
    :cond_b
    const/4 v7, 0x0

    goto :goto_4

    .line 985
    .restart local v1    # "articleWidget":Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
    .restart local v7    # "enableDownArrow":Z
    :cond_c
    const/high16 v2, -0x1000000

    goto :goto_5
.end method

.method private getMagazineImageAttachmentId(I)Ljava/lang/String;
    .locals 4
    .param p1, "page"    # I

    .prologue
    const/4 v1, 0x0

    .line 1018
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1032
    :cond_0
    :goto_0
    return-object v1

    .line 1021
    :cond_1
    const/4 v0, 0x0

    .line 1022
    .local v0, "scrubberImages":[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    if-ne v2, v3, :cond_3

    .line 1023
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v2, v2

    if-lez v2, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v0, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 1029
    :goto_1
    if-eqz v0, :cond_0

    array-length v2, v0

    if-le v2, p1, :cond_0

    .line 1030
    aget-object v1, v0, p1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1024
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v0, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_1

    .line 1026
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 1027
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v0, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->landscapeScrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    :goto_2
    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v0, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    goto :goto_2
.end method

.method private getRelativePageView(I)Landroid/view/View;
    .locals 2
    .param p1, "relativePosition"    # I

    .prologue
    .line 1189
    if-ltz p1, :cond_1

    const/4 v1, 0x2

    if-gt p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 1190
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/NSViewPager;->getPageViews()[Landroid/view/View;

    move-result-object v1

    aget-object v0, v1, p1

    .line 1191
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    if-eqz v1, :cond_0

    .line 1192
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .end local v0    # "view":Landroid/view/View;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    .line 1194
    :cond_0
    return-object v0

    .line 1189
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private gotoRightArticle()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 603
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "gotoRightArticle()"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 604
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v1

    .line 605
    .local v1, "postId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 608
    :cond_0
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "hasn\'t refreshed yet"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623
    :goto_0
    return-void

    .line 611
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 612
    .local v0, "page":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 615
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unable to find post with postId: %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 618
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v2

    if-eq v2, v0, :cond_3

    .line 619
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Going to right page, was: %d, going to: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2, v0, v6}, Landroid/support/v4/view/NSViewPager;->setCurrentLogicalItem(IZ)V

    .line 622
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->loadArticlesIfNeeded()V

    goto :goto_0
.end method

.method private gotoRightArticlePage()V
    .locals 5

    .prologue
    .line 626
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->ignoreGotoArticlePage:Z

    if-eqz v4, :cond_1

    .line 641
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v3

    .line 630
    .local v3, "widget":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    .line 631
    .local v2, "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    instance-of v4, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v4, :cond_3

    move-object v1, v3

    .line 633
    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .line 634
    .local v1, "nbWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageFraction()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageNumber()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 635
    :cond_2
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    goto :goto_0

    .line 637
    .end local v1    # "nbWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    :cond_3
    instance-of v4, v3, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    if-eqz v4, :cond_0

    move-object v0, v3

    .line 638
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    .line 639
    .local v0, "articleWidget":Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    goto :goto_0
.end method

.method private hasPostId()Z
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inLiteMode()Z
    .locals 1

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    goto :goto_0
.end method

.method private isCurrentView(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadArticlesIfNeeded()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1198
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1231
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1203
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v0

    .line 1204
    .local v0, "currentLogicalPosition":I
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    aput v0, v6, v10

    .line 1205
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    add-int/lit8 v7, v0, 0x1

    aput v7, v6, v9

    .line 1206
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    const/4 v7, 0x2

    add-int/lit8 v8, v0, -0x1

    aput v8, v6, v7

    .line 1207
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    array-length v6, v6

    if-ge v1, v6, :cond_0

    .line 1208
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->preloadOrder:[I

    aget v3, v6, v1

    .line 1209
    .local v3, "logicalPosition":I
    if-ltz v3, :cond_2

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 1210
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v6, v3}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v4

    .line 1211
    .local v4, "view":Landroid/view/View;
    instance-of v6, v4, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    if-eqz v6, :cond_2

    move-object v5, v4

    .line 1212
    check-cast v5, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    .line 1213
    .local v5, "widget":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
    invoke-interface {v5}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v2

    .line 1214
    .local v2, "loadState":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    sget-object v6, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$12;->$SwitchMap$com$google$apps$dots$android$newsstand$widget$DelayedContentWidget$LoadState:[I

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1207
    .end local v2    # "loadState":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "widget":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1217
    .restart local v2    # "loadState":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .restart local v4    # "view":Landroid/view/View;
    .restart local v5    # "widget":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;
    :pswitch_1
    sget-object v6, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "loading content at page: %d"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1218
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->loadDelayedContents(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private markEditionAsRecentlyReadIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 690
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->recentlyReadHelper()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->account()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->markAsRecentlyRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 691
    return-void
.end method

.method private markPostAsReadIfNeeded()V
    .locals 8

    .prologue
    .line 694
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hasPostId()Z

    move-result v4

    if-nez v4, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 697
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 698
    .local v1, "loadState":Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v0

    .line 699
    .local v0, "currentWidget":Landroid/view/View;
    instance-of v4, v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    if-eqz v4, :cond_2

    move-object v4, v0

    .line 700
    check-cast v4, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->getLoadState()Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    move-result-object v1

    .line 702
    :cond_2
    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v1, v4, :cond_0

    .line 705
    instance-of v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v4, :cond_3

    move-object v2, v0

    .line 706
    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .line 707
    .local v2, "nbWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getPageFraction()F

    move-result v3

    .line 716
    .local v3, "pageFraction":F
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    if-eqz v4, :cond_0

    .line 717
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->queueStorePageFraction(F)V

    goto :goto_0

    .line 721
    .end local v2    # "nbWidget":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;
    .end local v3    # "pageFraction":F
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->account()Landroid/accounts/Account;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private maybeConfigureOrientationPreference()V
    .locals 7

    .prologue
    .line 727
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    .line 728
    .local v3, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasNativeBodySummary()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 729
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getNativeBodySummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;

    move-result-object v2

    .line 730
    .local v2, "nbSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v4

    .line 731
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->getHasLandscapeNativeBody()Z

    move-result v5

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;->getHasPortraitNativeBody()Z

    move-result v6

    .line 730
    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->configureOrientationPreference(ZZ)V

    .line 764
    .end local v2    # "nbSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$NativeBodySummary;
    :cond_0
    :goto_0
    return-void

    .line 738
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->articleLoader()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v1

    .line 739
    .local v1, "loader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    if-eqz v1, :cond_0

    .line 740
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 742
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 741
    invoke-virtual {v0, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private maybeTrackAnalyticsReadingEvent(I)V
    .locals 3
    .param p1, "pageNumber"    # I

    .prologue
    .line 1234
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 1235
    .local v1, "postOriginalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1239
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    iget-boolean v2, v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    if-eqz v2, :cond_2

    .line 1240
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineLiteReadingScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;I)V

    .line 1244
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    :goto_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->track(Z)V

    goto :goto_0

    .line 1242
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    :cond_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineReadingScreen;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MagazineReadingScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;I)V

    .restart local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    goto :goto_1
.end method

.method private maybeTriggerMagazinesSync()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 568
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 569
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isLowMemoryDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 570
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->getCurrentForegroundSyncPolicy()Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/sync/ImageSyncType$SyncPolicy;->allowsMagazines()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 571
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Syncing %s"

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 572
    new-instance v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-direct {v2, v0}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;-><init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    invoke-virtual {v2, v6}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    move-result-object v1

    .line 573
    .local v1, "request":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 575
    new-instance v2, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/sync/MagazineLiteOnlySkipNodePredicate;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate(Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;

    .line 577
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer;->sync(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 579
    .end local v1    # "request":Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    :cond_1
    return-void
.end method

.method private onBackToEdition()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 804
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v4

    .line 809
    .local v4, "postId":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    sget v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-object v7, v1

    .line 810
    .local v7, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v7, :cond_0

    .line 811
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v1

    if-eq v1, v9, :cond_0

    .line 812
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    .line 813
    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$9;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 812
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/DataListUtil;->findClosestData(Lcom/google/android/libraries/bind/data/DataList;ILcom/google/common/base/Predicate;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    .line 821
    .local v6, "closestData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v6, :cond_0

    .line 822
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v4

    .line 829
    .end local v6    # "closestData":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    .line 830
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v3

    .line 834
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getPageCount()I

    move-result v8

    if-gt v8, v9, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V

    .line 835
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 836
    return-void

    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
    .end local v7    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_1
    move-object v7, v5

    .line 809
    goto :goto_0

    .line 834
    .restart local v7    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getPageNumber()Ljava/lang/Integer;

    move-result-object v5

    goto :goto_1
.end method

.method private onPagerDestroyedView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 1118
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1119
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget;->setDelayedLoadEventHandler(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$EventHandler;)V

    .line 1121
    :cond_0
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    if-eqz v0, :cond_1

    .line 1122
    check-cast p1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->setArticleEventHandler(Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;)V

    .line 1124
    :cond_1
    return-void
.end method

.method private pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .locals 1

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    goto :goto_0
.end method

.method private postData()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "postId":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v2, :cond_0

    .line 413
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 414
    .local v1, "postIndex":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 415
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 418
    .end local v1    # "postIndex":I
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private postOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 2

    .prologue
    .line 434
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 435
    .local v0, "postData":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ORIGINAL_EDITION:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method

.method private postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 2

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 423
    .local v0, "postData":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_SUMMARY:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    goto :goto_0
.end method

.method private prepareAltFormatsMenuItem(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 877
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_magazine_alt_formats:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 878
    .local v1, "menuItem":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->altFormats:Ljava/util/List;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->hasValidAltFormats(Ljava/util/List;Lcom/google/android/libraries/bind/data/DataList;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 880
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v0

    .line 881
    .local v0, "isInLiteMode":Z
    if-eqz v0, :cond_0

    .line 882
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_lite:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 886
    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->getAltFormatToggleA11yString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 887
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 888
    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/support/v4/view/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 892
    .end local v0    # "isInLiteMode":Z
    :goto_1
    return-void

    .line 884
    .restart local v0    # "isInLiteMode":Z
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_print:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 890
    .end local v0    # "isInLiteMode":Z
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method private readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    goto :goto_0
.end method

.method private resolvePostIdIfNeeded()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 393
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hasPostId()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 407
    :goto_0
    return v1

    .line 399
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 400
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    sget v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_POST_ID:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "postId":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 402
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v5

    invoke-direct {v3, v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Z)V

    .line 401
    invoke-virtual {p0, v3, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changeState(Landroid/os/Parcelable;Z)V

    goto :goto_0

    .end local v0    # "postId":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 407
    goto :goto_0
.end method

.method private setupActionBar(Landroid/view/View;)V
    .locals 2
    .param p1, "toolbarContainer"    # Landroid/view/View;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 244
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNavigationDrawerActivity()Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->attach(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)V

    .line 252
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->setToolbarContainer(Landroid/view/View;)V

    .line 253
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hide(Z)V

    .line 257
    :cond_0
    return-void
.end method

.method private setupPager()V
    .locals 3

    .prologue
    .line 260
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->viewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/android/libraries/bind/view/ViewHeap;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    .line 271
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Landroid/support/v4/view/NSViewPager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    .line 350
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 351
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 353
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)V

    .line 369
    return-void
.end method

.method private updateActionBar()V
    .locals 3

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$7;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 686
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_AUTO_HIDE:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->setMode(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;)V

    .line 687
    return-void
.end method

.method private updateAllPostsList()V
    .locals 2

    .prologue
    .line 899
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->readingList(Landroid/content/Context;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    .line 900
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updatePostReadingList()V

    .line 901
    return-void
.end method

.method private updateAltFormats()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 644
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hasPostId()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    if-nez v2, :cond_1

    .line 673
    :cond_0
    return-void

    .line 651
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getPageNumber()Ljava/lang/Integer;

    move-result-object v1

    .line 655
    .local v1, "currentPage":Ljava/lang/Integer;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->altFormats:Ljava/util/List;

    .line 656
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v3, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->altFormat:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 657
    .local v0, "altFormat":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->hasType()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->hasFormat()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 658
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getType()I

    move-result v5

    if-eq v5, v7, :cond_3

    .line 656
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 667
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getFormat()I

    move-result v5

    if-eq v5, v7, :cond_4

    .line 668
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v5

    sget-object v6, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-eq v5, v6, :cond_4

    if-eqz v1, :cond_4

    .line 669
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->getIndex()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 670
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->altFormats:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private updateEditionSummary()V
    .locals 2

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 599
    return-void
.end method

.method private updateErrorView()V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->refreshErrorView()V

    .line 564
    return-void
.end method

.method private updateMenu()V
    .locals 0

    .prologue
    .line 895
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->invalidateOptionsMenu()V

    .line 896
    return-void
.end method

.method private updatePageChangeListener()V
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 560
    return-void
.end method

.method private updatePostReadingList()V
    .locals 3

    .prologue
    .line 904
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;-><init>(Lcom/google/android/libraries/bind/data/DataList;Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->magazinePostReadingFilter:Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

    .line 905
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->EQUALITY_FIELDS:[I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->magazinePostReadingFilter:Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 906
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->autoRefreshOnce()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 908
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingListRefreshed:Z

    .line 911
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 913
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 914
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 915
    return-void
.end method

.method private updateRestrictToLiteMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 587
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->restrictToLiteMode:Z

    .line 588
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->canForegroundSyncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 590
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedVersion(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v0

    .line 591
    .local v0, "pinnedVersion":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 592
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->restrictToLiteMode:Z

    .line 595
    .end local v0    # "pinnedVersion":Ljava/lang/Integer;
    :cond_0
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1268
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$LiteModeTransitionDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$ReadingTransitionDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;)V

    goto :goto_0
.end method

.method protected getDisableA11yMode()I
    .locals 1

    .prologue
    .line 1171
    const/4 v0, 0x2

    return v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 768
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 769
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 770
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "help_context_key"

    const-string v3, "mobile_magazine_object"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const-string v2, "editionInfo"

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    return-object v0
.end method

.method public getPageCount()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 469
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v1, :cond_0

    .line 470
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getPageCount()I

    move-result v0

    .line 472
    :cond_0
    return v0
.end method

.method public getPageNumber()Ljava/lang/Integer;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 454
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->hasValidPageNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageNumber()Ljava/lang/Integer;

    move-result-object v0

    .line 462
    :goto_0
    return-object v0

    .line 459
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v0, :cond_1

    .line 460
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->getCurrentPage()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 462
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleOnBackPressed()Z
    .locals 1

    .prologue
    .line 799
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onBackToEdition()V

    .line 800
    const/4 v0, 0x0

    return v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onArticleOverscrolled(Landroid/view/View;Z)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "top"    # Z

    .prologue
    .line 1070
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->isCurrentView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1071
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onArticleOverscrolled - top: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1072
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0, p2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->onOverscroll(Z)V

    .line 1074
    :cond_0
    return-void
.end method

.method public onArticlePageChanged(Landroid/view/View;IIZ)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "page"    # I
    .param p3, "pageCount"    # I
    .param p4, "userDriven"    # Z

    .prologue
    const/4 v8, 0x1

    .line 1078
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->isCurrentView(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1079
    sget-object v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "onArticlePageChanged - page: %d, pageCount: %d, userDriven: %b"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    .line 1080
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 1079
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1082
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1083
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getMagazineImageAttachmentId(I)Ljava/lang/String;

    move-result-object v0

    .line 1084
    .local v0, "attachmentId":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1085
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 1086
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v4

    .line 1085
    invoke-virtual {v3, v0, v4}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 1089
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, v8}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v2

    .line 1091
    .local v2, "widget":Landroid/view/View;
    if-eqz p4, :cond_2

    instance-of v3, v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v3, :cond_2

    .line 1092
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    if-eqz v3, :cond_1

    .line 1093
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    if-le p3, v8, :cond_4

    int-to-float v3, p2

    add-int/lit8 v5, p3, -0x1

    int-to-float v5, v5

    div-float/2addr v3, v5

    :goto_0
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->queueStorePageFraction(F)V

    .line 1100
    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromNumber(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v1

    .line 1101
    .local v1, "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changePageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    .line 1104
    .end local v1    # "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    :cond_2
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeTrackAnalyticsReadingEvent(I)V

    .line 1106
    .end local v2    # "widget":Landroid/view/View;
    :cond_3
    return-void

    .line 1093
    .restart local v2    # "widget":Landroid/view/View;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onArticleScrolled(Landroid/view/View;IIIZ)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "scrollOffset"    # I
    .param p3, "scrollRange"    # I
    .param p4, "scrollDelta"    # I
    .param p5, "userDriven"    # Z

    .prologue
    .line 1046
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->isCurrentView(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1047
    sget-object v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "onArticleScrolled - scrollOffset: %d, scrollRange: %d, scrollDelta: %d, userDriven: %b"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 1049
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1047
    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1050
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v4, p4, p2}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->onScroll(II)Z

    .line 1051
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;

    move-result-object v3

    .line 1054
    .local v3, "widget":Landroid/view/View;
    if-eqz p5, :cond_1

    instance-of v4, v3, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    if-eqz v4, :cond_1

    .line 1055
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1056
    .local v2, "scrollPageSize":I
    if-eqz p2, :cond_0

    if-gtz p3, :cond_3

    :cond_0
    const/4 v0, 0x0

    .line 1058
    .local v0, "pageFraction":F
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromFraction(Ljava/lang/Float;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v1

    .line 1059
    .local v1, "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changePageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)V

    .line 1061
    .end local v0    # "pageFraction":F
    .end local v1    # "pageLocation":Lcom/google/apps/dots/android/newsstand/article/PageLocation;
    .end local v2    # "scrollPageSize":I
    :cond_1
    if-eqz p5, :cond_2

    instance-of v4, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    if-eqz v4, :cond_2

    .line 1062
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    if-lez p3, :cond_4

    int-to-float v4, p2

    int-to-float v6, p3

    div-float/2addr v4, v6

    :goto_1
    invoke-virtual {v5, v4}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;->queueStorePageFraction(F)V

    .line 1066
    .end local v3    # "widget":Landroid/view/View;
    :cond_2
    return-void

    .line 1056
    .restart local v2    # "scrollPageSize":I
    .restart local v3    # "widget":Landroid/view/View;
    :cond_3
    int-to-float v4, p2

    int-to-float v5, v2

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    int-to-float v5, p3

    div-float v0, v4, v5

    goto :goto_0

    .line 1062
    .end local v2    # "scrollPageSize":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onArticleUnhandledClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1110
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->isCurrentView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111
    sget-object v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onArticleUnhandledClick"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->onClick()V

    .line 1114
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 860
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 861
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->magazine_reading_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 862
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 373
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 376
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 377
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 378
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 379
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hidingActionBar:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->detach()V

    .line 380
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 381
    return-void
.end method

.method public onLoadStateChanged(Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;Ljava/lang/Throwable;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "loadState"    # Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 1128
    sget-object v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "onLoadStateChanged - tag: %s, loadState: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1129
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->isLoadedOrFailed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1130
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->loadArticlesIfNeeded()V

    .line 1132
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne p2, v1, :cond_4

    .line 1137
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    if-eqz v1, :cond_5

    move-object v1, p1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;

    .line 1138
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/LoadingArticleWidget;->getWidget()Landroid/view/View;

    move-result-object v0

    .line 1139
    .local v0, "widget":Landroid/view/View;
    :goto_0
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;

    .line 1141
    .end local v0    # "widget":Landroid/view/View;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->usesLegacyLayoutEngine()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1142
    :cond_1
    const/high16 v1, -0x1000000

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1144
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->isCurrentView(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1145
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->markPostAsReadIfNeeded()V

    .line 1148
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 1149
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->getAlpha()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1152
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$11;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1161
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->gotoRightArticlePage()V

    .line 1164
    :cond_4
    return-void

    .line 1138
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 840
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 841
    .local v0, "itemId":I
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_magazine_alt_formats:I

    if-ne v0, v1, :cond_2

    .line 842
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->restrictToLiteMode:Z

    if-eqz v1, :cond_0

    .line 843
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MagazineLiteOnlyDialog;->show(Landroid/support/v4/app/FragmentActivity;)V

    :goto_0
    move v1, v2

    .line 855
    :goto_1
    return v1

    .line 844
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->altFormats:Ljava/util/List;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->hasValidAltFormats(Ljava/util/List;Lcom/google/android/libraries/bind/data/DataList;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 845
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->allPostsList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->altFormats:Ljava/util/List;

    invoke-static {v1, p0, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormats(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/List;)V

    goto :goto_0

    .line 848
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->switchLiteMode(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->changeState(Landroid/os/Parcelable;Z)V

    goto :goto_0

    .line 851
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_3

    .line 852
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->onBackToEdition()V

    .line 853
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    .line 855
    :cond_3
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 866
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 871
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 872
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->prepareAltFormatsMenuItem(Landroid/view/Menu;)V

    .line 874
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 782
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateMenu()V

    .line 783
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 784
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->magazinePostReadingFilter:Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

    .line 785
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->inLiteMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 790
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->magazinePostReadingFilter:Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;

    .line 791
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/reading/MagazinePostReadingFilter;->isRepub()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)V

    .line 792
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->start()V

    .line 794
    :cond_1
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onResume()V

    .line 795
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 222
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toolbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 223
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->pager:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/NSViewPager;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;

    .line 224
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->bg_preview_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->sharedAttachmentView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 225
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->magazine_expando_hero:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->transitionPlaceholderView:Landroid/view/View;

    .line 227
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 235
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toolbar_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->setupActionBar(Landroid/view/View;)V

    .line 236
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->setupPager()V

    .line 237
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateLayoutDirection()V

    .line 238
    return-void
.end method

.method public postId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->postId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1249
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    if-eqz v0, :cond_0

    move-object v8, p2

    .line 1250
    check-cast v8, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 1251
    .local v8, "readingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 1252
    .local v6, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 1253
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 1254
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 1252
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v7, p2

    .line 1255
    check-cast v7, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .line 1256
    .local v7, "magazineReadingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    .line 1257
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    iget-boolean v3, v7, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    .line 1258
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->postId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->getPageNumber()Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;ZLjava/lang/String;Ljava/lang/Integer;)V

    .line 1259
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 1256
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1260
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    invoke-direct {v0, p1, v8}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1263
    .end local v6    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v7    # "magazineReadingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .end local v8    # "readingState":Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method protected updateLayoutDirection()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 549
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    .line 550
    const/4 v0, 0x0

    .line 551
    .local v0, "layoutDirection":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->rootView()Landroid/view/View;

    move-result-object v1

    .line 552
    .local v1, "rootView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutDirection(I)V

    .line 556
    .end local v0    # "layoutDirection":I
    .end local v1    # "rootView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 112
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;)V
    .locals 9
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 478
    if-nez p1, :cond_1

    .line 542
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    :cond_2
    move v0, v6

    .line 484
    .local v0, "editionChanged":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 485
    sget-object v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Edition changed"

    new-array v8, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 487
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->clearAppFromNotifications(Landroid/content/Context;Ljava/lang/String;)V

    .line 488
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateLayoutDirection()V

    .line 489
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateEditionSummary()V

    .line 490
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updatePageChangeListener()V

    .line 491
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateActionBar()V

    .line 492
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateRestrictToLiteMode()V

    .line 493
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->markEditionAsRecentlyReadIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 495
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->resolvePostIdIfNeeded()Z

    .line 496
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateErrorView()V

    .line 499
    :cond_3
    if-nez v0, :cond_4

    iget-boolean v4, p2, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    .line 500
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;

    iget-boolean v4, v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->inLiteMode:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    :cond_4
    move v1, v6

    .line 502
    .local v1, "inLiteModeChanged":Z
    :goto_2
    if-nez v0, :cond_5

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/data/NSDataPagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    if-nez v4, :cond_e

    .line 503
    :cond_5
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateAllPostsList()V

    .line 509
    :cond_6
    :goto_3
    if-eqz v1, :cond_7

    .line 512
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeTriggerMagazinesSync()V

    .line 516
    :cond_7
    if-nez v0, :cond_8

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->postId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->postId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingListRefreshed:Z

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    .line 517
    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v7

    if-eq v4, v7, :cond_f

    :cond_8
    move v3, v6

    .line 518
    .local v3, "postChanged":Z
    :goto_4
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postReadingListRefreshed:Z

    .line 519
    if-eqz v3, :cond_a

    .line 520
    sget-object v4, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Post changed"

    new-array v8, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 521
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 524
    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeTrackAnalyticsReadingEvent(I)V

    .line 526
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->hasPostId()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 527
    new-instance v4, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->account()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postId()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pageFractionHelper:Lcom/google/apps/dots/android/newsstand/reading/PageFractionHelper;

    .line 530
    :cond_9
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->markPostAsReadIfNeeded()V

    .line 531
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->gotoRightArticle()V

    .line 532
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeConfigureOrientationPreference()V

    .line 535
    :cond_a
    if-nez v3, :cond_b

    .line 536
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingState;->pageLocation()Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    :cond_b
    move v2, v6

    .line 537
    .local v2, "pageChanged":Z
    :goto_5
    if-eqz v2, :cond_0

    .line 538
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateAltFormats()V

    .line 539
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateMenu()V

    .line 540
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->gotoRightArticlePage()V

    goto/16 :goto_0

    .end local v0    # "editionChanged":Z
    .end local v1    # "inLiteModeChanged":Z
    .end local v2    # "pageChanged":Z
    .end local v3    # "postChanged":Z
    :cond_c
    move v0, v5

    .line 483
    goto/16 :goto_1

    .restart local v0    # "editionChanged":Z
    :cond_d
    move v1, v5

    .line 500
    goto/16 :goto_2

    .line 504
    .restart local v1    # "inLiteModeChanged":Z
    :cond_e
    if-eqz v1, :cond_6

    .line 506
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updatePostReadingList()V

    goto/16 :goto_3

    :cond_f
    move v3, v5

    .line 517
    goto :goto_4

    .restart local v3    # "postChanged":Z
    :cond_10
    move v2, v5

    .line 536
    goto :goto_5
.end method
