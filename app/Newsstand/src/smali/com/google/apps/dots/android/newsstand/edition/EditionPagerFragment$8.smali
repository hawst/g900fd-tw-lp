.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;
.super Ljava/lang/Object;
.source "EditionPagerFragment.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 308
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBeforeTabSelected(I)V
    .locals 0
    .param p1, "pageIndex"    # I

    .prologue
    .line 311
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;"
    return-void
.end method

.method public onTabSelected(I)V
    .locals 4
    .param p1, "pageIndex"    # I

    .prologue
    .line 315
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->isChangingState()Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$300(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v1

    if-nez v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 317
    .local v0, "logicalPosition":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 319
    .end local v0    # "logicalPosition":I
    :cond_0
    return-void
.end method
