.class final Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;
.super Ljava/lang/Object;
.source "AssetFileDescriptorHelper.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Landroid/content/res/AssetFileDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$fds:[Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>([Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    .line 85
    return-void
.end method

.method public onSuccess(Landroid/content/res/AssetFileDescriptor;)V
    .locals 8
    .param p1, "afd"    # Landroid/content/res/AssetFileDescriptor;

    .prologue
    const/4 v7, 0x1

    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 69
    .local v2, "os":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 70
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    .end local v2    # "os":Ljava/io/FileOutputStream;
    .local v3, "os":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-static {v1, v3}, Lcom/google/common/io/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 75
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 76
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 77
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    .line 78
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    aget-object v4, v4, v7

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    move-object v2, v3

    .line 80
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v2    # "os":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    :try_start_2
    # getter for: Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Trouble pumping AFD"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 76
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 77
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    .line 78
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    aget-object v4, v4, v7

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 75
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 76
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 77
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    .line 78
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->val$fds:[Landroid/os/ParcelFileDescriptor;

    aget-object v5, v5, v7

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/os/ParcelFileDescriptor;)V

    throw v4

    .line 75
    .end local v2    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v2    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 72
    .end local v2    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v2    # "os":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;->onSuccess(Landroid/content/res/AssetFileDescriptor;)V

    return-void
.end method
