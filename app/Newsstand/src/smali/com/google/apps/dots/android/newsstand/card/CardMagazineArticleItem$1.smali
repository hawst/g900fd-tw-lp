.class final Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardMagazineArticleItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$postId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x1

    .line 118
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->track(Z)V

    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;->val$postId:Ljava/lang/String;

    .line 121
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v4}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setInLiteMode(Z)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    .line 123
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    const/16 v1, 0xc8

    .line 124
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->startForResult(I)V

    .line 125
    return-void
.end method
