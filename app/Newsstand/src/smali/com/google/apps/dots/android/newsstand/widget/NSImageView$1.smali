.class Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;
.super Ljava/lang/Object;
.source "NSImageView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setZoomable(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private oldFocusX:F

.field private oldFocusY:F

.field private final oldValues:[F

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldValues:[F

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 8
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v5, 0x0

    .line 165
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v6

    if-nez v6, :cond_0

    .line 187
    :goto_0
    return v5

    .line 169
    :cond_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 170
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldValues:[F

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->getValues([F)V

    .line 171
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldValues:[F

    aget v4, v6, v5

    .line 172
    .local v4, "oldScale":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v5

    mul-float v2, v4, v5

    .line 173
    .local v2, "newScale":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 174
    .local v0, "newFocusX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    .line 177
    .local v1, "newFocusY":F
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->minScale:F
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)F

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    const/high16 v6, 0x40c00000    # 6.0f

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 178
    div-float v3, v2, v4

    .line 180
    .local v3, "newScaleFactor":F
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;

    move-result-object v5

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusX:F

    neg-float v6, v6

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusY:F

    neg-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 181
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v5, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 182
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->transform:Landroid/graphics/Matrix;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$100(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 183
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->dimensions:Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$000(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v6

    const/4 v7, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->translateImage(FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V
    invoke-static {v5, v0, v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->access$300(Lcom/google/apps/dots/android/newsstand/widget/NSImageView;FFLcom/google/apps/dots/android/newsstand/util/Dimensions;[F)V

    .line 184
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->invalidate()V

    .line 185
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusX:F

    .line 186
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusY:F

    .line 187
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->onScaleBegin()V

    .line 193
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusX:F

    .line 194
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$1;->oldFocusY:F

    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 199
    return-void
.end method
