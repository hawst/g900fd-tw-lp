.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScrollDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;


# instance fields
.field private bitField0_:I

.field private initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

.field private scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

.field public snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

.field private zoomInitial_:F

.field private zoomMax_:F

.field private zoomMin_:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1592
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1593
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1617
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1620
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    .line 1639
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    .line 1658
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    .line 1593
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    .locals 5

    .prologue
    .line 1711
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1715
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v3, :cond_0

    .line 1716
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1718
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 1719
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1720
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 1721
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 1722
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1720
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1712
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 1713
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 1726
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v3, :cond_3

    .line 1727
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1729
    :cond_3
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1589
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1734
    if-ne p1, p0, :cond_1

    .line 1742
    :cond_0
    :goto_0
    return v1

    .line 1735
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1736
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    .line 1737
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1738
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 1742
    goto :goto_0

    .line 1737
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 1738
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1742
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getInitialOffset()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    .locals 1

    .prologue
    .line 1600
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    return-object v0
.end method

.method public getScrollExtent()Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;
    .locals 1

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1790
    const/4 v1, 0x0

    .line 1791
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v2, :cond_0

    .line 1792
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1793
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1795
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-eqz v2, :cond_2

    .line 1796
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 1797
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    if-eqz v0, :cond_1

    .line 1798
    const/4 v5, 0x2

    .line 1799
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1796
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1803
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 1804
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    .line 1805
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    .line 1807
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v2, :cond_4

    .line 1808
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1809
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1811
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    .line 1812
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    .line 1813
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    .line 1815
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_6

    .line 1816
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    .line 1817
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    .line 1819
    :cond_6
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->cachedSize:I

    .line 1820
    return v1
.end method

.method public getZoomMax()F
    .locals 1

    .prologue
    .line 1622
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    return v0
.end method

.method public getZoomMin()F
    .locals 1

    .prologue
    .line 1641
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    return v0
.end method

.method public hasInitialOffset()Z
    .locals 1

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasScrollExtent()Z
    .locals 1

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasZoomMax()Z
    .locals 1

    .prologue
    .line 1630
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasZoomMin()Z
    .locals 1

    .prologue
    .line 1649
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1746
    const/16 v1, 0x11

    .line 1747
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 1748
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 1749
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 1755
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 1756
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 1757
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int v1, v2, v4

    .line 1758
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v4, :cond_4

    :goto_1
    add-int v1, v2, v3

    .line 1759
    return v1

    .line 1748
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->hashCode()I

    move-result v2

    goto :goto_0

    .line 1751
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1752
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 1751
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1752
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->hashCode()I

    move-result v2

    goto :goto_3

    .line 1758
    .end local v0    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1828
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1829
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1833
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1834
    :sswitch_0
    return-object p0

    .line 1839
    :sswitch_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-nez v5, :cond_1

    .line 1840
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    .line 1842
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1846
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1847
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-nez v5, :cond_3

    move v1, v4

    .line 1848
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1849
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-eqz v5, :cond_2

    .line 1850
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1852
    :cond_2
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 1853
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 1854
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;-><init>()V

    aput-object v6, v5, v1

    .line 1855
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1856
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1853
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1847
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v1, v5

    goto :goto_1

    .line 1859
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;-><init>()V

    aput-object v6, v5, v1

    .line 1860
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1864
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    .line 1865
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    goto :goto_0

    .line 1869
    :sswitch_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-nez v5, :cond_5

    .line 1870
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    .line 1872
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1876
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    .line 1877
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    goto/16 :goto_0

    .line 1881
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    .line 1882
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    goto/16 :goto_0

    .line 1829
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1589
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1764
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    if-eqz v1, :cond_0

    .line 1765
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->initialOffset_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1767
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    if-eqz v1, :cond_2

    .line 1768
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->snap:[Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1769
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    if-eqz v0, :cond_1

    .line 1770
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1768
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1774
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 1775
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMax_:F

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 1777
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    if-eqz v1, :cond_4

    .line 1778
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->scrollExtent_:Lcom/google/apps/dots/proto/client/DotsNativeBody$Rectangle;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1780
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 1781
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomMin_:F

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 1783
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 1784
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$ScrollDetails;->zoomInitial_:F

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 1786
    :cond_6
    return-void
.end method
