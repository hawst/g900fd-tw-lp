.class public abstract Lcom/google/apps/dots/android/newsstand/activity/StartActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "StartActivity.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;
.implements Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog$ResultHandler;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private allowOnboardingFlow:Ljava/lang/Boolean;

.field private final destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private enableDebuggerAttachPause:Z

.field private enableDogfoodWarningNotice:Z

.field private isDestroyed:Z

.field private isStarted:Z

.field private loadingView:Landroid/view/View;

.field private oarData:Landroid/content/Intent;

.field private oarRequestCode:Ljava/lang/Integer;

.field private oarResultCode:Ljava/lang/Integer;

.field private prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

.field private processingActivityResult:Z

.field private retryButton:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 66
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->checkOnboardFlagAndStartOrResumeFlow()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->loadingView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->retryButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private checkOnboardFlagAndStartOrResumeFlow()V
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startOrResumeFlow()V

    .line 199
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->checkAllowOnboardingFlow()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 190
    .local v0, "onboardFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Boolean;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$2;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private clearProcessingActivityResult()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->processingActivityResult:Z

    .line 496
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarRequestCode:Ljava/lang/Integer;

    .line 497
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    .line 498
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarData:Landroid/content/Intent;

    .line 499
    return-void
.end method

.method private getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .locals 1

    .prologue
    .line 177
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->instanceForActivity(Landroid/support/v4/app/FragmentActivity;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    return-object v0
.end method

.method private maybeSendAnalyticsSettingsDataEvent()V
    .locals 4

    .prologue
    .line 288
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->shouldSendEvent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinnedEditionList()Ljava/util/List;

    move-result-object v0

    .line 290
    .local v0, "pinnedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;-><init>(Ljava/util/List;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsSettingsDataEvent;->track(Z)V

    .line 291
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLastAnalyticsSettingsEventSentTime(J)V

    .line 293
    .end local v0    # "pinnedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/edition/Edition;>;"
    :cond_0
    return-void
.end method

.method private onActivityResultInternal()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 463
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->processingActivityResult:Z

    .line 464
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarRequestCode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarData:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarRequestCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 491
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->clearProcessingActivityResult()V

    .line 492
    return-void

    .line 467
    :sswitch_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 468
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->checkOnboardFlagAndStartOrResumeFlow()V

    goto :goto_0

    .line 470
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto :goto_0

    .line 474
    :sswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 477
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$6;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$6;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 484
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->completeFlow(Z)V

    goto :goto_0

    .line 465
    :sswitch_data_0
    .sparse-switch
        0x2bd -> :sswitch_0
        0x2d0 -> :sswitch_1
    .end sparse-switch
.end method

.method private pauseToAttachDebugger()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 502
    const/4 v1, 0x0

    const-string v2, "Attach yer debugger!"

    new-instance v5, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$7;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$7;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    .line 514
    return-void
.end method

.method private startEditionHelper(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "appFamilyId"    # Ljava/lang/String;
    .param p2, "optPostId"    # Ljava/lang/String;

    .prologue
    .line 523
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->createEditionByAppFamily(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;

    invoke-direct {v1, p0, p2}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$8;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;Ljava/lang/String;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 554
    return-void
.end method


# virtual methods
.method protected completeFlow(Z)V
    .locals 3
    .param p1, "showedOnboarding"    # Z

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->isShareTarget(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->showOnboardQuizSeparatelyIfNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 261
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->maybeSendAnalyticsSettingsDataEvent()V

    .line 262
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "firstLaunch"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    const-string v0, "StartActivity"

    const-string v1, "FIRST_LAUNCH = false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setFirstLaunch(Z)V

    .line 266
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->webViewUriWhitelist()Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/uri/WebViewUriWhitelist;->load()V

    .line 267
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->navigateToTarget()V

    goto :goto_0
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 172
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Finishing StartActivity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 174
    return-void
.end method

.method protected navigateToTarget()V
    .locals 3

    .prologue
    .line 300
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$4;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    .line 301
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 308
    .local v0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/share/TargetInfo;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 327
    return-void
.end method

.method protected navigateToTarget(Lcom/google/apps/dots/android/newsstand/share/TargetInfo;)V
    .locals 8
    .param p1, "targetInfo"    # Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    .line 331
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 332
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_DEFAULT:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_2

    .line 333
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 335
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 336
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "android.intent.action.MAIN"

    .line 337
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 340
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    .line 439
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 344
    :cond_1
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->restoreStateIfPossible(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 346
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto :goto_0

    .line 353
    :cond_2
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_POST:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_4

    .line 354
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    if-eqz v4, :cond_3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 355
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;->getAppId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 356
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "sectionId":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 358
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v0

    .line 426
    .end local v3    # "sectionId":Ljava/lang/String;
    :cond_3
    :goto_1
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    if-eqz v4, :cond_e

    .line 428
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    .line 429
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;

    move-result-object v4

    .line 430
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NewsReadingIntentBuilder;->start()V

    .line 438
    :goto_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto :goto_0

    .line 362
    :cond_4
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWSSTAND_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_5

    .line 363
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 364
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    invoke-direct {p0, v4, v7}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startEditionHelper(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 369
    :cond_5
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_TITLES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-eq v4, v5, :cond_3

    .line 371
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_HOME_ISSUES:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_6

    .line 372
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 373
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    .line 374
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v4

    .line 375
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 376
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 377
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 378
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto/16 :goto_0

    .line 381
    :cond_6
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_MAGAZINES_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_8

    .line 382
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 383
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v0

    goto :goto_1

    .line 385
    :cond_7
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 386
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    .line 387
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;

    move-result-object v4

    .line 388
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/TitleIssuesIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 389
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 390
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 391
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto/16 :goto_0

    .line 394
    :cond_8
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_NEWS_EDITION:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_b

    .line 395
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 396
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->findIdOfType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 397
    .restart local v3    # "sectionId":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 398
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->sectionEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    move-result-object v0

    goto/16 :goto_1

    .line 401
    .end local v3    # "sectionId":Ljava/lang/String;
    :cond_9
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    if-eqz v4, :cond_a

    .line 402
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v0

    goto/16 :goto_1

    .line 404
    :cond_a
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 405
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    invoke-direct {p0, v4, v7}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startEditionHelper(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 408
    :cond_b
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_OFFERS:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_d

    .line 409
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 410
    .local v2, "offersIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    if-eqz v4, :cond_c

    .line 411
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->offerId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->setOfferId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;

    .line 413
    :cond_c
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 414
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 415
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    .line 416
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto/16 :goto_0

    .line 418
    .end local v2    # "offersIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;
    :cond_d
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->location:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    if-ne v4, v5, :cond_3

    .line 419
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 421
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->appFamilyId:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;->postId:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startEditionHelper(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 432
    :cond_e
    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 433
    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v4

    .line 434
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 435
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v4

    .line 436
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start()V

    goto/16 :goto_2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 443
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 444
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onActivityResult - requestCode: %d, resultCode: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->processingActivityResult:Z

    .line 447
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarRequestCode:Ljava/lang/Integer;

    .line 448
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarResultCode:Ljava/lang/Integer;

    .line 449
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->oarData:Landroid/content/Intent;

    .line 451
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isStarted:Z

    if-eqz v0, :cond_0

    .line 453
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->onActivityResultInternal()V

    .line 456
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->start_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->setContentView(I)V

    .line 86
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "hasAppLaunched"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 88
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_debugger_attach_pause:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->enableDebuggerAttachPause:Z

    .line 89
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_dogfood_warnings:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->enableDogfoodWarningNotice:Z

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isDestroyed:Z

    .line 92
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->loading_view:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->loadingView:Landroid/view/View;

    .line 93
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->retry_button:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->retryButton:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->retryButton:Landroid/view/View;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$1;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isDestroyed:Z

    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->destroy()V

    .line 161
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 162
    return-void
.end method

.method public onExternalStorageResolution(Z)V
    .locals 1
    .param p1, "diskCacheAvailable"    # Z

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isDestroyed:Z

    if-nez v0, :cond_0

    .line 149
    if-eqz p1, :cond_1

    .line 150
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->checkOnboardFlagAndStartOrResumeFlow()V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->finish()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 166
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 167
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->setIntent(Landroid/content/Intent;)V

    .line 168
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 519
    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 104
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 105
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isStarted:Z

    .line 106
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStart - processingActivityResult: %b"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->processingActivityResult:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->processingActivityResult:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->onActivityResultInternal()V

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->updateVersionPreferencesIfNeeded(Landroid/content/Context;)V

    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/diskcache/ExternalStorageDialog;->resolveDiskCacheUnavailability(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->isStarted:Z

    .line 123
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 124
    return-void
.end method

.method protected showOnboardQuizSeparatelyIfNeeded()Z
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->shouldShowOnboardingQuiz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 280
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->quizSequenceOnly()Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;

    move-result-object v0

    const/16 v1, 0x2d0

    .line 281
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->startForResult(I)V

    .line 282
    const/4 v0, 0x1

    .line 284
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method startOrResumeFlow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 202
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->loadingView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 205
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->retryButton:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->enableDebuggerAttachPause:Z

    if-eqz v1, :cond_0

    .line 209
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->enableDebuggerAttachPause:Z

    .line 210
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->pauseToAttachDebugger()V

    .line 246
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->enableDogfoodWarningNotice:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;->shouldShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/apps/dots/android/newsstand/activity/ConfidentialityNoticeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0x2bd

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 222
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->isShareTarget(Landroid/content/Intent;)Z

    move-result v0

    .line 226
    .local v0, "hasShareTarget":Z
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->allowOnboardingFlow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getShowedTutorial()Z

    move-result v1

    if-nez v1, :cond_2

    .line 227
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 228
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->fullSequence()Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;

    move-result-object v1

    const/16 v2, 0x2d0

    .line 229
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/OnboardIntentBuilder;->startForResult(I)V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->destroyAsyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->getAuthUiHelper()Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->newAuthFlowFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$3;-><init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
