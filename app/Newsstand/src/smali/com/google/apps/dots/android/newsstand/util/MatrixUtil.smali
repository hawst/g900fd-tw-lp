.class public Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;
.super Ljava/lang/Object;
.source "MatrixUtil.java"


# static fields
.field static glScratchValues:[F

.field static scratchValues:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [F

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    .line 18
    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->glScratchValues:[F

    return-void
.end method

.method public static getMatrixScale(Landroid/graphics/Matrix;)F
    .locals 4
    .param p0, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    mul-float/2addr v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->scratchValues:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static getMatrixTranslation(Landroid/graphics/Matrix;[F)V
    .locals 2
    .param p0, "matrix"    # Landroid/graphics/Matrix;
    .param p1, "translation"    # [F

    .prologue
    const/4 v1, 0x0

    .line 52
    const/4 v0, 0x0

    aput v1, p1, v0

    .line 53
    const/4 v0, 0x1

    aput v1, p1, v0

    .line 54
    invoke-virtual {p0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 55
    return-void
.end method
