.class public final Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;
.super Ljava/lang/Object;
.source "PreferenceKeys.java"


# static fields
.field public static keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;


# instance fields
.field public final ABOUT:Ljava/lang/String;

.field public final ALWAYS_SHOW_GOOGLE_SOLD_ADS:Ljava/lang/String;

.field public final ARTICLE_TEXT_SIZE:Ljava/lang/String;

.field public final COUNTRY_OVERRIDE:Ljava/lang/String;

.field public final CUSTOM_BASE_URL:Ljava/lang/String;

.field public final CUSTOM_GUC_URL:Ljava/lang/String;

.field public final DESIGNER_MODE:Ljava/lang/String;

.field public final DEVELOPER:Ljava/lang/String;

.field public final DOWNLOADING_CATEGORY:Ljava/lang/String;

.field public final DOWNLOAD_VIA_CHARGING_ONLY:Ljava/lang/String;

.field public final DOWNLOAD_VIA_WIFI_ONLY:Ljava/lang/String;

.field public final ENABLE_ALL_DEBUG_LOGGERS:Ljava/lang/String;

.field public final IMAGE_SYNC_TYPE:Ljava/lang/String;

.field public final LOAD_EXTRA_JS:Ljava/lang/String;

.field public final OPEN_SOURCE_LICENSES:Ljava/lang/String;

.field public final PRIVACY_POLICY:Ljava/lang/String;

.field public final READING_POSITION_SYNC:Ljava/lang/String;

.field public final RESET_WARM_WELCOME_CARDS:Ljava/lang/String;

.field public final SERVER_TYPE:Ljava/lang/String;

.field public final SHOW_NOTIFICATIONS:Ljava/lang/String;

.field public final SHOW_ONBOARD_QUIZ:Ljava/lang/String;

.field public final SHOW_ONBOARD_TUTORIAL:Ljava/lang/String;

.field public final SIMULATE_CRASH_EVENT:Ljava/lang/String;

.field public final START_BACKGROUND_SYNC:Ljava/lang/String;

.field public final SYNC_INFO:Ljava/lang/String;

.field public final SYNC_MINIMUM_STORAGE_MAGS:Ljava/lang/String;

.field public final SYNC_MINIMUM_STORAGE_NEWS:Ljava/lang/String;

.field public final TERMS_OF_SERVICE:Ljava/lang/String;

.field public final USAGE_POLICY:Ljava/lang/String;

.field public final USE_EXTERNAL_STORAGE:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->article_text_size_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ARTICLE_TEXT_SIZE:Ljava/lang/String;

    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->show_notifications_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_NOTIFICATIONS:Ljava/lang/String;

    .line 61
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->about_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ABOUT:Ljava/lang/String;

    .line 62
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->open_source_licenses_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->OPEN_SOURCE_LICENSES:Ljava/lang/String;

    .line 63
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->terms_of_service_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->TERMS_OF_SERVICE:Ljava/lang/String;

    .line 64
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->privacy_policy_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->PRIVACY_POLICY:Ljava/lang/String;

    .line 65
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->usage_policy_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->USAGE_POLICY:Ljava/lang/String;

    .line 68
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->downloading_category_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOADING_CATEGORY:Ljava/lang/String;

    .line 69
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_via_wifi_only_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOAD_VIA_WIFI_ONLY:Ljava/lang/String;

    .line 70
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->download_while_charging_only_preference_key:I

    .line 71
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DOWNLOAD_VIA_CHARGING_ONLY:Ljava/lang/String;

    .line 72
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->use_external_storage_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->USE_EXTERNAL_STORAGE:Ljava/lang/String;

    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->developer_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DEVELOPER:Ljava/lang/String;

    .line 76
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->custom_base_url_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->CUSTOM_BASE_URL:Ljava/lang/String;

    .line 77
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->custom_guc_url_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->CUSTOM_GUC_URL:Ljava/lang/String;

    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->server_type_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SERVER_TYPE:Ljava/lang/String;

    .line 79
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->reading_position_sync_preference_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->READING_POSITION_SYNC:Ljava/lang/String;

    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->country_override_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->COUNTRY_OVERRIDE:Ljava/lang/String;

    .line 81
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->load_extra_js_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->LOAD_EXTRA_JS:Ljava/lang/String;

    .line 82
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->designer_mode_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->DESIGNER_MODE:Ljava/lang/String;

    .line 83
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->show_onboard_tutorial_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_ONBOARD_TUTORIAL:Ljava/lang/String;

    .line 84
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->show_onboard_quiz_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SHOW_ONBOARD_QUIZ:Ljava/lang/String;

    .line 85
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->reset_warm_welcome_cards_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->RESET_WARM_WELCOME_CARDS:Ljava/lang/String;

    .line 86
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->simulate_crash_event_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SIMULATE_CRASH_EVENT:Ljava/lang/String;

    .line 87
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->enable_all_debug_loggers_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ENABLE_ALL_DEBUG_LOGGERS:Ljava/lang/String;

    .line 89
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->image_sync_type_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->IMAGE_SYNC_TYPE:Ljava/lang/String;

    .line 90
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->sync_minimum_storage_mags_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_MINIMUM_STORAGE_MAGS:Ljava/lang/String;

    .line 91
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->sync_minimum_storage_news_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_MINIMUM_STORAGE_NEWS:Ljava/lang/String;

    .line 92
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->start_background_sync_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->START_BACKGROUND_SYNC:Ljava/lang/String;

    .line 93
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->sync_info_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->SYNC_INFO:Ljava/lang/String;

    .line 95
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->always_show_google_sold_ads_key:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->ALWAYS_SHOW_GOOGLE_SOLD_ADS:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;->keys:Lcom/google/apps/dots/android/newsstand/preference/PreferenceKeys;

    .line 100
    return-void
.end method
