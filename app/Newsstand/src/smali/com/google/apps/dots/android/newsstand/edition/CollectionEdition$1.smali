.class Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$1;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "CollectionEdition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->headerImageList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/android/libraries/bind/data/DataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 7
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 94
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "imageId":Ljava/lang/String;
    sget v5, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_IMAGE_ID:I

    invoke-virtual {p1, v5, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 96
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_WIDTH:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    .line 97
    .local v2, "width":Ljava/lang/Integer;
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ELIGIBLE_FOR_HEADER:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v5

    if-eqz v5, :cond_0

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ELIGIBLE_FOR_HEADER:I

    .line 98
    invoke-virtual {p1, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    .line 100
    .local v0, "eligible":Z
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 101
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isImageTooLowResForHeader(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v0, :cond_1

    :goto_1
    return v3

    .end local v0    # "eligible":Z
    :cond_0
    move v0, v3

    .line 98
    goto :goto_0

    .restart local v0    # "eligible":Z
    :cond_1
    move v3, v4

    .line 101
    goto :goto_1
.end method
