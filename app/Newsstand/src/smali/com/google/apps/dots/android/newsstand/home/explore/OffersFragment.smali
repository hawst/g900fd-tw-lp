.class public Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "OffersFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

.field private offersObserver:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    const-string v1, "OffersFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->offers_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->updateErrorView()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->updateTopOffer()V

    return-void
.end method

.method private refreshIfNeeded()V
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 90
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method private sendAnalyticsEvent()V
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OffersScreen;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OffersScreen;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OffersScreen;->track(Z)V

    .line 192
    return-void
.end method

.method private setUpAdapter()V
    .locals 8

    .prologue
    .line 119
    new-instance v5, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v5, v6}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 122
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$2;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 128
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$3;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v6, p0, v7}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 142
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_news:I

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_offers:I

    invoke-static {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 144
    .local v3, "emptyViewData":Lcom/google/android/libraries/bind/data/Data;
    sget v5, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_TEXT:I

    .line 145
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->visit_my_library:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 144
    invoke-virtual {v3, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 146
    sget v5, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_ON_CLICK_LISTENER:I

    new-instance v6, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$4;

    invoke-direct {v6, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)V

    invoke-virtual {v3, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 152
    new-instance v5, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v5, v6}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 153
    invoke-virtual {v5, v3}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;->setEmptyMessageData(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    move-result-object v4

    .line 154
    .local v4, "emptyViewProvider":Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v5, v4}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setEmptyViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 157
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->offersList()Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    .line 158
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;->freshen()V

    .line 159
    new-instance v5, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    .line 160
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->useSpacerTopPadding(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v2

    .line 161
    .local v2, "cardListBuilder":Lcom/google/android/libraries/bind/card/CardListBuilder;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersList:Lcom/google/apps/dots/android/newsstand/home/explore/OffersList;

    invoke-direct {v0, v5}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 162
    .local v0, "cardGroup":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/bind/card/CardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 163
    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 164
    .local v1, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v5, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 165
    new-instance v5, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$5;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;)V

    iput-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersObserver:Landroid/database/DataSetObserver;

    .line 171
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 172
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 173
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v6, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v6}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 174
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 84
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 85
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    return-void
.end method

.method private updateErrorView()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 181
    return-void
.end method

.method private updateTopOffer()V
    .locals 3

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    .line 215
    .local v1, "startingOfferId":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->findRowWithCardId(Ljava/lang/Object;)I

    move-result v0

    .line 217
    .local v0, "offerPosition":I
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setSelection(I)V

    .line 221
    .end local v0    # "offerPosition":I
    :cond_0
    return-void

    .line 214
    .end local v1    # "startingOfferId":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    iget-object v1, v2, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 96
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_offers"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 185
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 186
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 187
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->offersObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 188
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onResume()V

    .line 103
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->refreshIfNeeded()V

    .line 106
    :cond_0
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 68
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 69
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->setupHeaderListLayout()V

    .line 70
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 71
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->setUpAdapter()V

    .line 72
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->updateErrorView()V

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->sendAnalyticsEvent()V

    .line 74
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->refreshIfNeeded()V

    .line 75
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    instance-of v2, p2, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    if-eqz v2, :cond_0

    move-object v1, p2

    .line 197
    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .line 198
    .local v1, "offersFragmentState":Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 199
    .local v0, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->EXPLORE_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;

    invoke-direct {v2, p1, v1}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;)V

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    .end local v0    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v1    # "offersFragmentState":Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;)V
    .locals 2
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .prologue
    .line 208
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;->startingOfferId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragment;->updateTopOffer()V

    .line 211
    :cond_1
    return-void
.end method
