.class Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;
.super Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;
.source "AudioStatusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

.field private currentStatus:I

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentStatus:I

    return-void
.end method


# virtual methods
.method protected onReceiveUpdate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 43
    const-string v2, "audio_item"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .line 44
    .local v0, "newItem":Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    const-string v2, "status"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 46
    .local v1, "newStatus":I
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentStatus:I

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    invoke-static {v2, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 47
    :cond_0
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .line 48
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentStatus:I

    .line 49
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentStatus:I

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->onAudioStateChanged(Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->access$000(Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;Lcom/google/apps/dots/android/newsstand/media/AudioItem;I)V

    .line 50
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper$1;->currentStatus:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 51
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->audio_error:I

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 54
    :cond_1
    return-void
.end method
