.class final Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$2;
.super Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;
.source "ViewPropertyTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;->elevation(FZ)Landroid/transition/Transition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;)V

    return-void
.end method


# virtual methods
.method public getPropertyValue(Landroid/view/View;)Ljava/lang/Float;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPropertyValue(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$2;->getPropertyValue(Landroid/view/View;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTypeEvaluator()Landroid/animation/TypeEvaluator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/animation/TypeEvaluator",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Landroid/animation/FloatEvaluator;

    invoke-direct {v0}, Landroid/animation/FloatEvaluator;-><init>()V

    return-object v0
.end method

.method public getViewPropertyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, "translationZ"

    return-object v0
.end method
