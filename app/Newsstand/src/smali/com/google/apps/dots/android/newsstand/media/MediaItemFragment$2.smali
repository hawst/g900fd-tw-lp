.class Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;
.super Ljava/lang/Object;
.source "MediaItemFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getImageView(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

.field final synthetic val$errorView:Landroid/widget/TextView;

.field final synthetic val$image:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field final synthetic val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field final synthetic val$spinner:Landroid/widget/ProgressBar;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;Landroid/widget/ProgressBar;Landroid/widget/TextView;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$spinner:Landroid/widget/ProgressBar;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$errorView:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$image:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    # invokes: Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getScaledDimensions(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->access$200(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    .line 104
    .local v0, "scaledDimensions":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    if-nez v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$spinner:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$errorView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$image:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->onImageNotLoaded(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->access$300(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$spinner:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 109
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->applyGravity()V

    .line 110
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;->val$imageView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setVisibility(I)V

    goto :goto_0
.end method
