.class public final Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsSyncV3.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsSyncV3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientAction"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;


# instance fields
.field private actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

.field private actionTimestamp_:J

.field private bitField0_:I

.field private body_:[B

.field private method_:I

.field private simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

.field private uri_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1870
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1871
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1881
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    .line 1903
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    .line 1941
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    .line 1960
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    .line 1871
    return-void
.end method


# virtual methods
.method public clearSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1

    .prologue
    .line 1997
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 1998
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 3

    .prologue
    .line 2016
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2020
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v2, :cond_0

    .line 2021
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 2023
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v2, :cond_1

    .line 2024
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 2026
    :cond_1
    return-object v0

    .line 2017
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :catch_0
    move-exception v1

    .line 2018
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1867
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2031
    if-ne p1, p0, :cond_1

    .line 2039
    :cond_0
    :goto_0
    return v1

    .line 2032
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2033
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 2034
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v3, :cond_3

    .line 2036
    :goto_2
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    .line 2038
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 2039
    goto :goto_0

    .line 2034
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 2036
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    .line 2038
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 2039
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getMethod()I
    .locals 1

    .prologue
    .line 1905
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 2083
    const/4 v0, 0x0

    .line 2084
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2085
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    .line 2086
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2088
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2089
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    .line 2090
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2092
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v1, :cond_2

    .line 2093
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 2094
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2096
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 2097
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    .line 2098
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2100
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 2101
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    .line 2102
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2104
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v1, :cond_5

    .line 2105
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 2106
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2108
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->cachedSize:I

    .line 2109
    return v0
.end method

.method public getSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1

    .prologue
    .line 1984
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public hasSimulationHint()Z
    .locals 1

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2043
    const/16 v1, 0x11

    .line 2044
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 2045
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 2046
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    add-int v1, v2, v4

    .line 2047
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 2048
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 2049
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    if-nez v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    .line 2055
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v4, :cond_4

    :goto_2
    add-int v1, v2, v3

    .line 2056
    return v1

    .line 2045
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 2047
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->hashCode()I

    move-result v2

    goto :goto_1

    .line 2051
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2052
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    aget-byte v4, v4, v0

    add-int v1, v2, v4

    .line 2051
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2055
    .end local v0    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2118
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2122
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2123
    :sswitch_0
    return-object p0

    .line 2128
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    .line 2129
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    goto :goto_0

    .line 2133
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2134
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 2136
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    .line 2137
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    goto :goto_0

    .line 2139
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    goto :goto_0

    .line 2144
    .end local v1    # "temp":I
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v2, :cond_3

    .line 2145
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 2147
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2151
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    .line 2152
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    goto :goto_0

    .line 2156
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    .line 2157
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    goto :goto_0

    .line 2161
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v2, :cond_4

    .line 2162
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 2164
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2118
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1867
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    return-object v0
.end method

.method public setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1946
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    .line 1947
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    .line 1948
    return-object p0
.end method

.method public setBody([B)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1
    .param p1, "value"    # [B

    .prologue
    .line 1965
    if-nez p1, :cond_0

    .line 1966
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1968
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    .line 1969
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    .line 1970
    return-object p0
.end method

.method public setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1908
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    .line 1909
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    .line 1910
    return-object p0
.end method

.method public setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 1987
    if-nez p1, :cond_0

    .line 1988
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1990
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 1991
    return-object p0
.end method

.method public setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1886
    if-nez p1, :cond_0

    .line 1887
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1889
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    .line 1890
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    .line 1891
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2061
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2062
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->uri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2064
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2065
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->method_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2067
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v0, :cond_2

    .line 2068
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2070
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 2071
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->actionTimestamp_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 2073
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 2074
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->body_:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 2076
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v0, :cond_5

    .line 2077
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->simulationHint_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2079
    :cond_5
    return-void
.end method
