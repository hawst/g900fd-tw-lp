.class public final Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PsvStart"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;


# instance fields
.field private bitField0_:I

.field private cancelUrlRegexp_:Ljava/lang/String;

.field private startUrl_:Ljava/lang/String;

.field private targetUrlRegexp_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30589
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30590
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30595
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    .line 30617
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    .line 30639
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    .line 30590
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    .locals 3

    .prologue
    .line 30673
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30677
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    return-object v0

    .line 30674
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    :catch_0
    move-exception v1

    .line 30675
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30586
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30682
    if-ne p1, p0, :cond_1

    .line 30687
    :cond_0
    :goto_0
    return v1

    .line 30683
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 30684
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    .line 30685
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 30686
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 30687
    goto :goto_0

    .line 30685
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    .line 30686
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    .line 30687
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 30714
    const/4 v0, 0x0

    .line 30715
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30716
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    .line 30717
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30719
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 30720
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    .line 30721
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30723
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 30724
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    .line 30725
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30727
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cachedSize:I

    .line 30728
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 30691
    const/16 v0, 0x11

    .line 30692
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 30693
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 30694
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 30695
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 30696
    return v0

    .line 30693
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 30694
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 30695
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30736
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 30737
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 30741
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30742
    :sswitch_0
    return-object p0

    .line 30747
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    .line 30748
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    goto :goto_0

    .line 30752
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    .line 30753
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    goto :goto_0

    .line 30757
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    .line 30758
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    goto :goto_0

    .line 30737
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30586
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30701
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30702
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->startUrl_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30704
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 30705
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->targetUrlRegexp_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30707
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 30708
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->cancelUrlRegexp_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30710
    :cond_2
    return-void
.end method
