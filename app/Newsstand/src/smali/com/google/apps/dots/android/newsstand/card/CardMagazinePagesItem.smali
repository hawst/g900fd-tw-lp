.class public Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.source "CardMagazinePagesItem.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# static fields
.field public static final DK_EITHER_IS_INITIAL_POST:I

.field public static final DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

.field public static final DK_LEFT_IMAGE_ID:I

.field public static final DK_LEFT_IS_INITIAL_POST:I

.field public static final DK_LEFT_ON_CLICK_LISTENER:I

.field public static final DK_RIGHT_IMAGE_ID:I

.field public static final DK_RIGHT_IS_INITIAL_POST:I

.field public static final DK_RIGHT_ON_CLICK_LISTENER:I

.field public static final DK_VERSION:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_version:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_VERSION:I

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_leftImageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_rightImageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_imageAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_leftOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_ON_CLICK_LISTENER:I

    .line 43
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_rightOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_ON_CLICK_LISTENER:I

    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_leftIsInitialPost:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IS_INITIAL_POST:I

    .line 48
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_rightIsInitialPost:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IS_INITIAL_POST:I

    .line 49
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazinePagesItem_eitherIsInitialPost:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_EITHER_IS_INITIAL_POST:I

    .line 52
    new-array v0, v3, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_VERSION:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->EQUALITY_FIELDS:[I

    .line 56
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_pages_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public static fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 76
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_VERSION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 78
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-lez v3, :cond_0

    .line 79
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v1

    .line 80
    .local v1, "imageHeight":I
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v2

    .line 81
    .local v2, "imageWidth":I
    int-to-float v3, v1

    int-to-float v4, v2

    div-float v0, v3, v4

    .line 82
    .local v0, "aspectRatio":F
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_IMAGE_ID:I

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v4, v4, v6

    .line 83
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 82
    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 84
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 85
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v3, v3

    if-le v3, v7, :cond_0

    .line 86
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_IMAGE_ID:I

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    aget-object v4, v4, v7

    .line 87
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 86
    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 91
    .end local v0    # "aspectRatio":F
    .end local v1    # "imageHeight":I
    .end local v2    # "imageWidth":I
    :cond_0
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_LEFT_ON_CLICK_LISTENER:I

    .line 92
    invoke-static {p2, p1, v6}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v4

    .line 91
    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->DK_RIGHT_ON_CLICK_LISTENER:I

    .line 94
    invoke-static {p2, p1, v7}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;

    move-result-object v4

    .line 93
    invoke-virtual {p3, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 95
    return-void
.end method

.method private static makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
    .locals 1
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "pageIndex"    # I

    .prologue
    .line 99
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)V

    return-object v0
.end method
