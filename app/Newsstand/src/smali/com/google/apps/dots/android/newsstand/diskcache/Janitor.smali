.class public Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
.super Ljava/lang/Object;
.source "Janitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field private fsDiskCacheBytes:J

.field private fsFreeBytes:J

.field private final janitorLock:Ljava/lang/Object;

.field private final janitorTask:Ljava/lang/Runnable;

.field private final mainThreadHandler:Landroid/os/Handler;

.field private final options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

.field private requested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;)V
    .locals 2
    .param p1, "diskCache"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .param p2, "options"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->mainThreadHandler:Landroid/os/Handler;

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorLock:Ljava/lang/Object;

    .line 104
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$2;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorTask:Ljava/lang/Runnable;

    .line 86
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 87
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->shouldSplit(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->shouldCompact(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->statFs()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->evictOldBlobs()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->splitStoreFiles()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->compactStoreFiles()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->closeStoreFiles()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->requested:Z

    return p1
.end method

.method private closeStoreFiles()V
    .locals 4

    .prologue
    .line 350
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$8;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$8;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cancelling close store files"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private compactStoreFiles()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 329
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$7;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$7;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :goto_0
    return-void

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cancelling compact store files"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private evictOldBlobs()V
    .locals 32

    .prologue
    .line 192
    invoke-direct/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->freeCacheSpace()J

    move-result-wide v2

    neg-long v0, v2

    move-wide/from16 v22, v0

    .line 193
    .local v22, "targetDelete":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-boolean v2, v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruEnabled:Z

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v22, v2

    if-gtz v2, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "LRU stat: space %d KB, total %d KB, target %d KB"

    const/16 v25, 0x3

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsFreeBytes:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    .line 199
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x2

    const-wide/16 v28, 0x400

    div-long v28, v22, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    .line 198
    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    new-instance v4, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;

    const/16 v2, 0xc8

    invoke-direct {v4, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;-><init>(I)V

    .line 204
    .local v4, "sampler":Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v20

    .line 205
    .local v20, "startSample":J
    const/4 v2, 0x1

    new-array v7, v2, [J

    .line 206
    .local v7, "totalSize":[J
    const/4 v2, 0x1

    new-array v8, v2, [I

    .line 207
    .local v8, "totalCount":[I
    const/4 v2, 0x1

    new-array v5, v2, [J

    .line 208
    .local v5, "unpinnedSize":[J
    const/4 v2, 0x1

    new-array v6, v2, [I

    .line 210
    .local v6, "unpinnedCount":[I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-object/from16 v25, v0

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$4;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;[J[I[J[I)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "LRU sampled: %d ms. %s total blobs in %d KB, %s unpinned blobs in %d KB"

    const/16 v25, 0x5

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    .line 230
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    sub-long v28, v28, v20

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    const/16 v27, 0x0

    aget v27, v8, v27

    .line 231
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x2

    const/16 v27, 0x0

    aget-wide v28, v7, v27

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x3

    const/16 v27, 0x0

    aget v27, v6, v27

    .line 232
    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x4

    const/16 v27, 0x0

    aget-wide v28, v5, v27

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    .line 229
    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    const/4 v2, 0x0

    aget-wide v2, v7, v2

    const/16 v25, 0x0

    aget-wide v26, v5, v25

    sub-long v14, v2, v26

    .line 236
    .local v14, "pinnedSize":J
    sub-long v22, v22, v14

    .line 237
    const-wide/16 v2, 0x0

    cmp-long v2, v22, v2

    if-ltz v2, :cond_0

    .line 243
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/diskcache/WeightedReservoirSampler;->getSamples()[I

    move-result-object v13

    .line 244
    .local v13, "samples":[I
    if-eqz v13, :cond_0

    .line 249
    const/4 v2, 0x0

    array-length v3, v13

    invoke-static {v13, v2, v3}, Ljava/util/Arrays;->sort([III)V

    .line 250
    move-wide/from16 v0, v22

    long-to-double v2, v0

    const/16 v25, 0x0

    aget-wide v26, v5, v25

    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v2, v2, v26

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v26

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v16

    .line 252
    .local v16, "quantile":D
    array-length v2, v13

    int-to-double v2, v2

    mul-double v2, v2, v16

    double-to-int v11, v2

    .line 253
    .local v11, "index":I
    array-length v2, v13

    if-ge v11, v2, :cond_2

    aget v24, v13, v11

    .line 254
    .local v24, "threshold":I
    :goto_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "LRU thresholded: quantile at %.2f, %d seconds ago"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    .line 255
    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    const-wide/16 v30, 0x3e8

    div-long v28, v28, v30

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v30, v0

    sub-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    .line 254
    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 259
    .local v18, "startDelete":J
    const/4 v2, 0x1

    new-array v10, v2, [J

    .line 260
    .local v10, "deletedSize":[J
    const/4 v2, 0x1

    new-array v9, v2, [I

    .line 262
    .local v9, "deletedCount":[I
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v3, v0, v1, v10, v9}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$5;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;I[J[I)V

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visit(Lcom/google/apps/dots/android/newsstand/diskcache/BlobVisitor;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 283
    :goto_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "LRU eviction in %d ms. Deleted %d blobs, %d KB"

    const/16 v25, 0x3

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    .line 284
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v28

    sub-long v28, v28, v18

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    const/16 v27, 0x0

    aget v27, v9, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x2

    const/16 v27, 0x0

    aget-wide v28, v10, v27

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    aput-object v27, v25, v26

    .line 283
    move-object/from16 v0, v25

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 224
    .end local v9    # "deletedCount":[I
    .end local v10    # "deletedSize":[J
    .end local v11    # "index":I
    .end local v13    # "samples":[I
    .end local v14    # "pinnedSize":J
    .end local v16    # "quantile":D
    .end local v18    # "startDelete":J
    .end local v24    # "threshold":I
    :catch_0
    move-exception v12

    .line 225
    .local v12, "ioe":Ljava/io/IOException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Cancelling cache sampling."

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v2, v12, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 253
    .end local v12    # "ioe":Ljava/io/IOException;
    .restart local v11    # "index":I
    .restart local v13    # "samples":[I
    .restart local v14    # "pinnedSize":J
    .restart local v16    # "quantile":D
    :cond_2
    const v24, 0x7fffffff

    goto/16 :goto_1

    .line 279
    .restart local v9    # "deletedCount":[I
    .restart local v10    # "deletedSize":[J
    .restart local v18    # "startDelete":J
    .restart local v24    # "threshold":I
    :catch_1
    move-exception v12

    .line 280
    .restart local v12    # "ioe":Ljava/io/IOException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Cancelling cache eviction."

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v2, v12, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private freeCacheSpace()J
    .locals 6

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v0, v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruMaxSize:J

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsFreeBytes:J

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v4, v4, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruTargetFsHeadroom:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private shouldCompact(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    .locals 12
    .param p1, "storeFileStats"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 288
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->freeCacheSpace()J

    move-result-wide v8

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v10, v10, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactFreeSpaceAggressiveThreshold:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_0

    move v0, v6

    .line 289
    .local v0, "aggressive":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget v1, v8, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactMinUtilizationAggressive:F

    .line 291
    .local v1, "minUtilization":F
    :goto_1
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileBlobsSize()J

    move-result-wide v2

    .line 292
    .local v2, "blobsFileUsedBytes":J
    iget-wide v8, p1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileSize:J

    sub-long v4, v8, v2

    .line 293
    .local v4, "reclaimableBlobsFileBytes":J
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v8, v8, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactWastedBytes:J

    cmp-long v8, v4, v8

    if-ltz v8, :cond_2

    long-to-float v8, v2

    iget-wide v10, p1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->blobsFileSize:J

    long-to-float v9, v10

    mul-float/2addr v9, v1

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    :goto_2
    return v6

    .end local v0    # "aggressive":Z
    .end local v1    # "minUtilization":F
    .end local v2    # "blobsFileUsedBytes":J
    .end local v4    # "reclaimableBlobsFileBytes":J
    :cond_0
    move v0, v7

    .line 288
    goto :goto_0

    .line 289
    .restart local v0    # "aggressive":Z
    :cond_1
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget v1, v8, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactMinUtilization:F

    goto :goto_1

    .restart local v1    # "minUtilization":F
    .restart local v2    # "blobsFileUsedBytes":J
    .restart local v4    # "reclaimableBlobsFileBytes":J
    :cond_2
    move v6, v7

    .line 293
    goto :goto_2
.end method

.method private shouldSplit(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    .locals 6
    .param p1, "storeFileStats"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    .prologue
    const/4 v0, 0x1

    .line 298
    iget v1, p1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    if-le v1, v0, :cond_1

    .line 299
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->totalBlobsSize()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v4, v1, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->splitMaxSize:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    iget v1, p1, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;->numBlobs:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget v2, v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->splitMaxBlobs:I

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private splitStoreFiles()V
    .locals 4

    .prologue
    .line 305
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$6;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :goto_0
    return-void

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cancelling split store files"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private statFs()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getRootDirs()Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;

    move-result-object v3

    .line 153
    .local v3, "rootDirs":Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;
    if-nez v3, :cond_0

    .line 154
    new-instance v6, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;

    const-string v7, "DiskCache unmounted"

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/diskcache/UnmountedException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 156
    :cond_0
    iget-object v4, v3, Lcom/google/apps/dots/android/newsstand/diskcache/RootDirs;->perm:Ljava/io/File;

    .line 158
    .local v4, "statDir":Ljava/io/File;
    :try_start_0
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 159
    .local v5, "statFs":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 160
    .local v0, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v6, v0

    iput-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsFreeBytes:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsDiskCacheBytes:J

    .line 167
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    new-instance v7, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;

    invoke-direct {v7, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$3;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->visitRegions(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;Z)V

    .line 175
    return-void

    .line 161
    .end local v0    # "blockSize":J
    .end local v5    # "statFs":Landroid/os/StatFs;
    :catch_0
    move-exception v2

    .line 162
    .local v2, "iae":Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/io/IOException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0xc

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Cannot stat "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method


# virtual methods
.method public getFsFreeBytes()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->statFs()V

    .line 179
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->fsFreeBytes:J

    return-wide v0
.end method

.method request()V
    .locals 6

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->janitorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->requested:Z

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->requested:Z

    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->mainThreadHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$1;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->options:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    iget-wide v4, v3, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->janitorDelayMs:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 101
    :cond_0
    monitor-exit v1

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
