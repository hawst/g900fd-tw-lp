.class final Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardOfferItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->getPositiveClickListener(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Ljava/lang/String;Lcom/google/android/libraries/bind/data/DataList;Z)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$analyticsReadingScreen:Ljava/lang/String;

.field final synthetic val$listToLocallyModify:Lcom/google/android/libraries/bind/data/DataList;

.field final synthetic val$longToastDisplayTime:Z

.field final synthetic val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$longToastDisplayTime:Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$analyticsReadingScreen:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$listToLocallyModify:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 269
    check-cast p2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .end local p2    # "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$longToastDisplayTime:Z

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$analyticsReadingScreen:Ljava/lang/String;

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->acceptOffer(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;ZLjava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$2;->val$listToLocallyModify:Lcom/google/android/libraries/bind/data/DataList;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->invalidateListIfNecessary(Lcom/google/android/libraries/bind/data/DataList;)V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->access$000(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 272
    return-void
.end method
