.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;
.super Ljava/lang/Object;
.source "SyncerService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->startUnsync()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 196
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Unpin failed %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->remove()V

    .line 198
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 190
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncerService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Unpinned %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp$3;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->remove()V

    .line 192
    return-void
.end method
