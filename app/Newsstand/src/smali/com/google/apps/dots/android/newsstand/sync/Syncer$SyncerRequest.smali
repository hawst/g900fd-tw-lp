.class public Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
.super Ljava/lang/Object;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncerRequest"
.end annotation


# instance fields
.field public anyFreshness:Z

.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field public pinId:Ljava/lang/Integer;

.field public skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

.field public snapshotId:Ljava/lang/Integer;

.field public userRequested:Z

.field public wifiOnlyDownloadOverride:Z


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .line 102
    return-void
.end method


# virtual methods
.method public anyFreshness(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 0
    .param p1, "anyFreshness"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->anyFreshness:Z

    .line 125
    return-object p0
.end method

.method public skipNodePredicate(Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 0
    .param p1, "skipNodePredicate"    # Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->skipNodePredicate:Lcom/google/apps/dots/android/newsstand/sync/SkipNodePredicate;

    .line 136
    return-object p0
.end method

.method public snapshotId(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 0
    .param p1, "pinId"    # Ljava/lang/Integer;
    .param p2, "snapshotId"    # Ljava/lang/Integer;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->pinId:Ljava/lang/Integer;

    .line 106
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->snapshotId:Ljava/lang/Integer;

    .line 107
    return-object p0
.end method

.method public userRequested(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 0
    .param p1, "userRequested"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->userRequested:Z

    .line 112
    return-object p0
.end method

.method public wifiOnlyDownloadOverride(Z)Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;
    .locals 0
    .param p1, "override"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$SyncerRequest;->wifiOnlyDownloadOverride:Z

    .line 117
    return-object p0
.end method
