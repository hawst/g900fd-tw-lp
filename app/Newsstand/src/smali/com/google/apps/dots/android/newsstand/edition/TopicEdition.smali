.class public Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;
.super Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.source "TopicEdition.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V
    .locals 0
    .param p1, "editionMessage"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;-><init>(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)V

    .line 32
    return-void
.end method


# virtual methods
.method public editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 87
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 88
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-eqz v1, :cond_0

    .line 90
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appFamilySummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;

    move-result-object v2

    iget-object v3, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/AppFamilySummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 89
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 91
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    if-eqz v0, :cond_0

    .line 92
    invoke-static {p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->topicEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v2

    .line 95
    .end local v0    # "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 55
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .line 56
    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getEditionCardList(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x33

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Attempted to get card list for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", should not happen."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->editionProto:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->getTopic()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto$TopicEditionInfo;->getClientEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readingCollectionUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public showKeepOnDeviceUi()Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public showOnDeviceOnlyUi()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public supportsReadStates()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public supportsSubscription()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public syncCollectionUris(Landroid/accounts/Account;)Ljava/util/List;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "This should not happen. Requesting the collection Uris for %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 66
    const-string v0, "%s - entity: %s (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 66
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackAnalytics(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;-><init>(Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/TopicEditionScreen;->track(Z)V

    .line 134
    return-void
.end method
