.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazinePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 782
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .locals 4

    .prologue
    .line 785
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleSeenEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 782
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;->get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    move-result-object v0

    return-object v0
.end method
