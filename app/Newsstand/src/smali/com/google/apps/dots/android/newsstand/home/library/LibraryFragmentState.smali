.class public Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
.super Ljava/lang/Object;
.source "LibraryFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->DEFAULT_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V
    .locals 0
    .param p1, "libraryPage"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 34
    instance-of v1, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    .line 36
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 38
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 29
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{library_page: %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 54
    return-void
.end method
