.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "ExploreSingleTopicActivity.java"


# instance fields
.field private mainFragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    .line 24
    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->mainFragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->mainFragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->mainFragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->explore_single_topic_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->setContentView(I)V

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->main_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->mainFragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    .line 32
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 57
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$menu;->explore_single_topic_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 58
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 39
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 63
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->onOptionsItemSelectedCloseDrawerIfNeeded(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    :goto_0
    return v0

    .line 65
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicActivity;->supportFinishAfterTransition()V

    goto :goto_0

    .line 69
    :cond_1
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
