.class public abstract Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.super Ljava/lang/Object;
.source "UncheckedCallback.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;, "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;, "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback<TT;>;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Exception."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    return-void
.end method
