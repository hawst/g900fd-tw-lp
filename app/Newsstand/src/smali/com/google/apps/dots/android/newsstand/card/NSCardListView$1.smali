.class Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;
.super Lcom/google/android/libraries/bind/data/BaseDataSetObserver;
.source "NSCardListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/BaseDataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 40
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 41
    .local v1, "listAdapter":Landroid/widget/ListAdapter;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getDataAdapter()Lcom/google/android/libraries/bind/data/DataAdapter;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->access$000(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Lcom/google/android/libraries/bind/data/DataAdapter;

    move-result-object v0

    .line 42
    .local v0, "dataAdapter":Lcom/google/android/libraries/bind/data/DataAdapter;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->hasRefreshedOnce()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->shuffleAnimationOnNextInvalidation()V

    .line 45
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->firstShuffleObserver:Landroid/database/DataSetObserver;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->access$100(Lcom/google/apps/dots/android/newsstand/card/NSCardListView;)Landroid/database/DataSetObserver;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 47
    :cond_0
    return-void
.end method
