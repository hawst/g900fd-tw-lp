.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setupOffersIfSupported(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 555
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 13
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 558
    if-nez p1, :cond_0

    .line 559
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v8, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z
    invoke-static {v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1102(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z

    .line 590
    :goto_0
    return-void

    .line 562
    :cond_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->offers_progress:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 564
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->offers:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 565
    .local v6, "view":Landroid/widget/LinearLayout;
    iget-object v7, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    iget-object v8, v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v9, v8

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v9, :cond_3

    aget-object v2, v8, v7

    .line 566
    .local v2, "offerNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v1

    .line 568
    .local v1, "offer":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferType()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_1

    .line 565
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 571
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getType()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 572
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Landroid/view/View;

    move-result-object v10

    sget v11, Lcom/google/android/apps/newsstanddev/R$id;->psv:I

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 573
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meterBody:Landroid/view/View;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Landroid/view/View;

    move-result-object v10

    sget v11, Lcom/google/android/apps/newsstanddev/R$id;->psv:I

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 574
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferType()I

    move-result v12

    invoke-virtual {v11, v1, v12}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getInAppClickHandler(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 577
    :cond_2
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v10}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    sget v11, Lcom/google/android/apps/newsstanddev/R$layout;->offer_line:I

    const/4 v12, 0x0

    .line 578
    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 579
    .local v3, "offerView":Landroid/widget/FrameLayout;
    sget v10, Lcom/google/android/apps/newsstanddev/R$id;->price:I

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 580
    .local v4, "price":Landroid/widget/TextView;
    sget v10, Lcom/google/android/apps/newsstanddev/R$id;->title:I

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 581
    .local v5, "title":Landroid/widget/TextView;
    sget v10, Lcom/google/android/apps/newsstanddev/R$id;->byline:I

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 582
    .local v0, "byline":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAmount()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getDescription()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v11, 0x1

    invoke-virtual {v10, v1, v11}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->getInAppClickHandler(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;I)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586
    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->initialized:Z

    .line 587
    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 589
    .end local v0    # "byline":Landroid/widget/TextView;
    .end local v1    # "offer":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .end local v2    # "offerNode":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v3    # "offerView":Landroid/widget/FrameLayout;
    .end local v4    # "price":Landroid/widget/TextView;
    .end local v5    # "title":Landroid/widget/TextView;
    :cond_3
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v8, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->doneLayout:Z
    invoke-static {v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1102(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 555
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$9;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
