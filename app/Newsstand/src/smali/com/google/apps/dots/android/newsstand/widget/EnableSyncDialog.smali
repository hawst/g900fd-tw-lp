.class public Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "EnableSyncDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final ACCOUNT:Ljava/lang/String; = "account"

.field private static final TAG:Ljava/lang/String; = "EnableSyncDialog"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    return-void
.end method

.method protected static enableSyncInBackground(Landroid/accounts/Account;)V
    .locals 2
    .param p0, "userAccount"    # Landroid/accounts/Account;

    .prologue
    .line 73
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;Landroid/accounts/Account;)V

    .line 80
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog$1;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 81
    return-void
.end method

.method private getAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method public static showIfSyncNotEnabled(Landroid/accounts/Account;Landroid/support/v4/app/FragmentActivity;)V
    .locals 5
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->isPeriodicSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->isGlobalSyncEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 36
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    const-string v3, "autoEnabledSync"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 38
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->enableSyncInBackground(Landroid/accounts/Account;)V

    .line 39
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    const-string v3, "autoEnabledSync"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setBoolean(Ljava/lang/String;Z)V

    .line 48
    :cond_1
    :goto_0
    return-void

    .line 41
    :cond_2
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;-><init>()V

    .line 42
    .local v1, "dialog":Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->setArguments(Landroid/os/Bundle;)V

    .line 45
    const-string v2, "EnableSyncDialog"

    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 66
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->enableSyncInBackground(Landroid/accounts/Account;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/EnableSyncDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->enable_sync_dialog_title:I

    .line 57
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->enable_sync_dialog_body:I

    .line 58
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    .line 59
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->cancel:I

    .line 60
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
