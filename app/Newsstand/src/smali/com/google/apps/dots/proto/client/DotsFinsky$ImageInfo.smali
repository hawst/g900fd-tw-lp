.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;


# instance fields
.field private bitField0_:I

.field private dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

.field private imageType_:I

.field private imageUrl_:Ljava/lang/String;

.field private positionInSequence_:I

.field private secureUrl_:Ljava/lang/String;

.field private supportsFifeUrlOptions_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 726
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 727
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 896
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    .line 915
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    .line 953
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    .line 975
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    .line 997
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    .line 727
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    .locals 3

    .prologue
    .line 1031
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1035
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-eqz v2, :cond_0

    .line 1036
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    .line 1038
    :cond_0
    return-object v0

    .line 1032
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    :catch_0
    move-exception v1

    .line 1033
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1043
    if-ne p1, p0, :cond_1

    .line 1050
    :cond_0
    :goto_0
    return v1

    .line 1044
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1045
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1046
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-nez v3, :cond_3

    .line 1048
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1049
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1050
    :goto_3
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 1046
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    .line 1048
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    .line 1049
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    .line 1050
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1090
    const/4 v0, 0x0

    .line 1091
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1092
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    .line 1093
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1095
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1096
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    .line 1097
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1099
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-eqz v1, :cond_2

    .line 1100
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    .line 1101
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1103
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 1104
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    .line 1105
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1107
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 1108
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    .line 1109
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1111
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 1112
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    .line 1113
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1115
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->cachedSize:I

    .line 1116
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1055
    const/16 v0, 0x11

    .line 1056
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1057
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    add-int v0, v1, v3

    .line 1058
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    add-int v0, v1, v3

    .line 1059
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 1060
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1061
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 1062
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    add-int v0, v2, v1

    .line 1063
    return v0

    .line 1059
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1060
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1061
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 1062
    :cond_3
    const/4 v1, 0x2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1124
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1125
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1129
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1130
    :sswitch_0
    return-object p0

    .line 1135
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1136
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/16 v2, 0xe

    if-eq v1, v2, :cond_1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0xf

    if-ne v1, v2, :cond_2

    .line 1148
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    .line 1149
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    goto :goto_0

    .line 1151
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    goto :goto_0

    .line 1156
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    .line 1157
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    goto :goto_0

    .line 1161
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-nez v2, :cond_3

    .line 1162
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    .line 1164
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1168
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    .line 1169
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    goto :goto_0

    .line 1173
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    .line 1174
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    goto :goto_0

    .line 1178
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    .line 1179
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    goto/16 :goto_0

    .line 1125
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1068
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1069
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1071
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1072
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->positionInSequence_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    if-eqz v0, :cond_2

    .line 1075
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->dimensions_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo$Dimensions;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1077
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 1078
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->imageUrl_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1080
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 1081
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->secureUrl_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1083
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 1084
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->supportsFifeUrlOptions_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1086
    :cond_5
    return-void
.end method
