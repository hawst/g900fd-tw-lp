.class public Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
.super Landroid/support/v4/app/Fragment;
.source "NSFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final instanceStatsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
            ">;",
            "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private final pauseAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private rootView:Landroid/view/View;

.field private transitionDelegate:Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/transition/delegate/TransitionDelegate",
            "<-",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 37
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->instanceStatsMap:Ljava/util/Map;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->pauseAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 267
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private getInstanceStats()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    .locals 3

    .prologue
    .line 258
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 259
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;>;"
    sget-object v2, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->instanceStatsMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    .line 260
    .local v1, "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    if-nez v1, :cond_0

    .line 261
    new-instance v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    .end local v1    # "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;-><init>(Ljava/lang/Class;I)V

    .line 262
    .restart local v1    # "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->instanceStatsMap:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    :cond_0
    return-object v1
.end method

.method private getTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->transitionDelegate:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->transitionDelegate:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->transitionDelegate:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    return-object v0
.end method

.method public static setStrictModeLimits(Ljava/lang/Class;I)V
    .locals 2
    .param p1, "maxAllowedResumed"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;>;"
    sget-object v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->instanceStatsMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    .line 243
    .local v0, "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    if-nez v0, :cond_0

    .line 244
    new-instance v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    .end local v0    # "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;-><init>(Ljava/lang/Class;I)V

    .line 245
    .restart local v0    # "instanceStats":Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->instanceStatsMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method protected account()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->account()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method protected doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getDisableA11yMode()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x4

    return v0
.end method

.method protected getEnableA11yMode()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 187
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 188
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_newsstand"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-object v0
.end method

.method public getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    return-object v0
.end method

.method public getNavigationDrawerActivity()Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    return-object v0
.end method

.method public getSupportActionBar()Landroid/support/v7/app/ActionBar;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->supportInvalidateOptionsMenu()V

    .line 212
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onActivityCreated(Ljava/lang/Object;)V

    .line 77
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onCreate: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 53
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->doOnCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    .line 56
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->updateAccessibility(Landroid/view/View;Z)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 162
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onDestroy()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 164
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 157
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 158
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 139
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPause: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 141
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->pauseAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 144
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->updateOnPauseTime()V

    .line 145
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getInstanceStats()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->onPause()V

    .line 146
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 131
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onResume: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->pauseAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 134
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getInstanceStats()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment$InstanceStats;->onResume()V

    .line 135
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 125
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStart: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 127
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    .line 150
    sget-object v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onStop: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 152
    return-void
.end method

.method protected rootView()Landroid/view/View;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public setActionBarBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setActionBarBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 221
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/app/ActionBar;->getDisplayOptions()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-lez v3, :cond_1

    move v0, v1

    .line 224
    .local v0, "showTitle":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 227
    .end local v0    # "showTitle":Z
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 223
    goto :goto_0

    .restart local v0    # "showTitle":Z
    :cond_2
    move v1, v2

    .line 224
    goto :goto_1
.end method

.method public setActionBarTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method protected setRootView(Landroid/view/View;)V
    .locals 0
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    .line 255
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 6
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getUserVisibleHint()Z

    move-result v0

    .line 195
    .local v0, "wasVisible":Z
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 196
    if-eqz p1, :cond_0

    .line 197
    sget-object v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Set Visible %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    :cond_0
    if-eq v0, p1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 202
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    invoke-virtual {p0, v1, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->updateAccessibility(Landroid/view/View;Z)V

    .line 203
    if-eqz p1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->rootView:Landroid/view/View;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->announce(Landroid/view/View;)V

    .line 208
    :cond_1
    return-void
.end method

.method protected updateAccessibility(Landroid/view/View;Z)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "userVisible"    # Z

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getImportantForAccessibility(Landroid/view/View;)I

    move-result v0

    .line 82
    .local v0, "wasImportant":I
    if-eqz p2, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getEnableA11yMode()I

    move-result v1

    .line 82
    :goto_0
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 85
    sget-object v1, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "View %s accessibility updated to %d was %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 86
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getImportantForAccessibility(Landroid/view/View;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 85
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    .end local v0    # "wasImportant":I
    :cond_0
    return-void

    .line 84
    .restart local v0    # "wasImportant":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getDisableA11yMode()I

    move-result v1

    goto :goto_0
.end method
