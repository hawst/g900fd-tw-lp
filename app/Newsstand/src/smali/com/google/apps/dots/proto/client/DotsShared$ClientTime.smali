.class public final Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientTime"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;


# instance fields
.field private bitField0_:I

.field private elapsedTime_:J

.field private localTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25630
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 25631
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25636
    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    .line 25655
    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    .line 25631
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    .locals 3

    .prologue
    .line 25685
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25689
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    return-object v0

    .line 25686
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    :catch_0
    move-exception v1

    .line 25687
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25627
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25694
    if-ne p1, p0, :cond_1

    .line 25697
    :cond_0
    :goto_0
    return v1

    .line 25695
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 25696
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 25697
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 25721
    const/4 v0, 0x0

    .line 25722
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 25723
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    .line 25724
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25726
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 25727
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    .line 25728
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25730
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->cachedSize:I

    .line 25731
    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 25702
    const/16 v0, 0x11

    .line 25703
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 25704
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 25705
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 25706
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25739
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 25740
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 25744
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 25745
    :sswitch_0
    return-object p0

    .line 25750
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    .line 25751
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    goto :goto_0

    .line 25755
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    .line 25756
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    goto :goto_0

    .line 25740
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25627
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25711
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 25712
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->elapsedTime_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 25714
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 25715
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->localTime_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 25717
    :cond_1
    return-void
.end method
