.class public Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;
.super Lcom/google/android/libraries/bind/widget/BoundFrameLayout;
.source "NSFrameLayout.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/SupportsReadState;


# instance fields
.field private final accessibilityClassOverride:I

.field private final cardSize:I

.field private isRead:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->NSFrameLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 35
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->NSFrameLayout_cardSize:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    .line 36
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/AccessibilityHelper;->getClassOverride(Landroid/content/res/TypedArray;)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->accessibilityClassOverride:I

    .line 37
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    return-void
.end method


# virtual methods
.method public getCardSize()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public isLargeCard()Z
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNormalCard()Z
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->isRead:Z

    return v0
.end method

.method public isTallCard()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 85
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWideCard()Z
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->cardSize:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/data/NSBoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->isRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 69
    .local v0, "drawableState":[I
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->STATE_READ:[I

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->mergeDrawableStates([I[I)[I

    .line 72
    .end local v0    # "drawableState":[I
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 43
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->accessibilityClassOverride:I

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/AccessibilityHelper;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;I)V

    .line 44
    return-void
.end method

.method public setIsRead(Z)V
    .locals 1
    .param p1, "isRead"    # Z

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->isRead:Z

    if-eq v0, p1, :cond_0

    .line 59
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->isRead:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->refreshDrawableState()V

    .line 61
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSViewStates;->dispatchSetIsRead(Landroid/view/ViewGroup;Z)V

    .line 63
    :cond_0
    return-void
.end method
