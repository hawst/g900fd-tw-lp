.class public Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "NSOnboardHostActivity.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/activity/ActivityResultHandler;


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleIntent()V
    .locals 4

    .prologue
    .line 61
    const/4 v1, 0x0

    .line 62
    .local v1, "sequenceType":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 63
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 64
    const-string v2, "OnboardIntentBuilder_sequenceType"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 67
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->initializeFragment(I)V

    .line 68
    return-void
.end method

.method protected hostFragment()Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    return-object v0
.end method

.method protected initializeFragment(I)V
    .locals 6
    .param p1, "sequenceType"    # I

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->hostFragment()Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    move-result-object v0

    .line 41
    .local v0, "existingFragment":Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getSequenceType()I

    move-result v4

    if-eq v4, p1, :cond_1

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 43
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 44
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;-><init>()V

    .line 45
    .local v2, "newFragment":Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;
    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->setSequenceType(I)V

    .line 47
    const v4, 0x1020002

    sget-object v5, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v2, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 48
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 49
    move-object v0, v2

    .line 51
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    .end local v2    # "newFragment":Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->instanceForActivity(Landroid/support/v4/app/FragmentActivity;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 86
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->hostFragment()Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 79
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 28
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 30
    .local v0, "decorView":Landroid/view/View;
    const/4 v1, 0x4

    .line 31
    .local v1, "uiOptions":I
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 33
    .end local v0    # "decorView":Landroid/view/View;
    .end local v1    # "uiOptions":I
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->handleIntent()V

    .line 35
    return-void
.end method

.method public onFinishedOnboardFlow()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->setResult(I)V

    .line 93
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->finish()V

    .line 94
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 56
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->setIntent(Landroid/content/Intent;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostActivity;->handleIntent()V

    .line 58
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method
