.class final Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState$1;
.super Ljava/lang/Object;
.source "SearchFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "query":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 68
    .local v2, "searchTypeInt":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const/4 v1, 0x0

    .line 70
    .local v1, "searchType":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :goto_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    invoke-direct {v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)V

    return-object v3

    .line 69
    .end local v1    # "searchType":Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->values()[Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    move-result-object v3

    aget-object v1, v3, v2

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 75
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState$1;->newArray(I)[Lcom/google/apps/dots/android/newsstand/search/SearchFragmentState;

    move-result-object v0

    return-object v0
.end method
