.class Lcom/google/apps/dots/android/newsstand/diskcache/Interval;
.super Ljava/lang/Object;
.source "OrderedLockSpace.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final lb:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field final ub:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TS;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    .local p1, "lb":Ljava/lang/Object;, "TS;"
    .local p2, "ub":Ljava/lang/Object;, "TS;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->lb:Ljava/lang/Object;

    .line 46
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->ub:Ljava/lang/Object;

    .line 47
    return-void
.end method


# virtual methods
.method containsPoint(Ljava/lang/Object;Ljava/util/Comparator;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/util/Comparator",
            "<TS;>;)Z"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    .local p1, "s":Ljava/lang/Object;, "TS;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TS;>;"
    const/4 v0, -0x1

    .line 51
    .local v0, "leftCompare":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->lb:Ljava/lang/Object;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->lb:Ljava/lang/Object;

    .line 53
    invoke-interface {p2, v1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->ub:Ljava/lang/Object;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->ub:Ljava/lang/Object;

    .line 56
    invoke-interface {p2, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public intersects(Lcom/google/apps/dots/android/newsstand/diskcache/Interval;Ljava/util/Comparator;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/diskcache/Interval",
            "<TS;>;",
            "Ljava/util/Comparator",
            "<TS;>;)Z"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    .local p1, "b":Lcom/google/apps/dots/android/newsstand/diskcache/Interval;, "Lcom/google/apps/dots/android/newsstand/diskcache/Interval<TS;>;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TS;>;"
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->lb:Ljava/lang/Object;

    invoke-virtual {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->containsPoint(Ljava/lang/Object;Ljava/util/Comparator;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->lb:Ljava/lang/Object;

    invoke-virtual {p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/Interval;->containsPoint(Ljava/lang/Object;Ljava/util/Comparator;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
