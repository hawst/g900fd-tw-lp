.class final Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->playFirstAudioItem(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/content/Context;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 5
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 92
    if-nez p1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->getAudioItemsList(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;

    move-result-object v0

    .line 97
    .local v0, "audioItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/media/AudioItem;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 98
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$context:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->val$owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->startAudio(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil$1;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
