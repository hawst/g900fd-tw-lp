.class public final enum Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;
.super Ljava/lang/Enum;
.source "MiscEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MiscType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

.field public static final enum HELP_FEEDBACK:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

.field public static final enum SETTINGS:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->SETTINGS:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    const-string v1, "HELP_FEEDBACK"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->HELP_FEEDBACK:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->SETTINGS:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->HELP_FEEDBACK:Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/drawer/MiscEntry$MiscType;

    return-object v0
.end method
