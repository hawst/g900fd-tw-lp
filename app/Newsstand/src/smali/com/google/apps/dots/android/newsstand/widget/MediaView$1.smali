.class Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MediaView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setAttachmentId(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

.field final synthetic val$attachmentUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Landroid/net/Uri;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->val$attachmentUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$002(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .line 154
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayerDataSource()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    .line 155
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->val$attachmentUri:Landroid/net/Uri;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->access$102(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Landroid/net/Uri;)Landroid/net/Uri;

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 145
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V

    return-void
.end method
