.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

.field public static final enum DETECT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

.field public static final enum LTR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

.field public static final enum RTL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;


# instance fields
.field public final textDirection:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    const-string v1, "DETECT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->DETECT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    .line 313
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    const-string v1, "LTR"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->LTR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    .line 314
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    const-string v1, "RTL"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->RTL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    .line 311
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->DETECT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->LTR:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->RTL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "textDirection"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 328
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->textDirection:I

    .line 329
    return-void
.end method

.method public static fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    .locals 5
    .param p0, "textDirection"    # I

    .prologue
    .line 319
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 320
    .local v0, "direction":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    iget v4, v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->textDirection:I

    if-ne v4, p0, :cond_0

    .line 324
    .end local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    :goto_1
    return-object v0

    .line 319
    .restart local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 324
    .end local v0    # "direction":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 311
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    return-object v0
.end method
