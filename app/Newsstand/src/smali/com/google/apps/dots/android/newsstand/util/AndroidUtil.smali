.class public Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
.super Ljava/lang/Object;
.source "AndroidUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static largerDisplayDimension:I

.field private static smallerDisplayDimension:I


# instance fields
.field private final appContext:Landroid/content/Context;

.field private deviceCategory:Lcom/google/apps/dots/shared/DeviceCategory;

.field private final isTouchExplorationEnabled:Z

.field private memoryClass:I

.field private minimumFeaturedImageSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 66
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 72
    sput v1, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->largerDisplayDimension:I

    .line 73
    sput v1, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->smallerDisplayDimension:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->memoryClass:I

    .line 79
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->minimumFeaturedImageSize:I

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    .line 84
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    const-string v2, "activity"

    .line 85
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 86
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->memoryClass:I

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isTouchExplorationEnabledFromContext(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isTouchExplorationEnabled:Z

    .line 90
    return-void
.end method

.method public static canCallGooglePlayServicesApi(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 599
    .line 600
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 599
    invoke-static {v0, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isPackageGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static computeLargerDisplayDimension()I
    .locals 4

    .prologue
    .line 250
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 251
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 252
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    return v2
.end method

.method private static computeSmallerDisplayDimension()I
    .locals 4

    .prologue
    .line 258
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 259
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 260
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    return v2
.end method

.method public static filterNonNull(Ljava/lang/Class;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;[TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    .local p1, "src":[Ljava/lang/Object;, "[TT;"
    const/4 v6, 0x0

    .line 521
    if-nez p1, :cond_0

    .line 522
    invoke-static {p0, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    check-cast v6, [Ljava/lang/Object;

    .line 539
    :goto_0
    return-object v6

    .line 524
    :cond_0
    const/4 v1, 0x0

    .line 525
    .local v1, "dstSize":I
    array-length v7, p1

    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v5, p1, v6

    .line 526
    .local v5, "t":Ljava/lang/Object;, "TT;"
    if-eqz v5, :cond_1

    .line 527
    add-int/lit8 v1, v1, 0x1

    .line 525
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 530
    .end local v5    # "t":Ljava/lang/Object;, "TT;"
    :cond_2
    array-length v6, p1

    if-eq v1, v6, :cond_5

    .line 531
    invoke-static {p0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    .line 532
    .local v0, "dst":[Ljava/lang/Object;, "[TT;"
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    array-length v6, p1

    if-ge v2, v6, :cond_4

    .line 533
    aget-object v6, p1, v2

    if-eqz v6, :cond_3

    .line 534
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aget-object v6, p1, v2

    aput-object v6, v0, v3

    move v3, v4

    .line 532
    .end local v4    # "j":I
    .restart local v3    # "j":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move-object v6, v0

    .line 537
    goto :goto_0

    .end local v0    # "dst":[Ljava/lang/Object;, "[TT;"
    .end local v2    # "i":I
    .end local v3    # "j":I
    :cond_5
    move-object v6, p1

    .line 539
    goto :goto_0
.end method

.method public static getActivityFromView(Landroid/view/View;)Landroid/app/Activity;
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 508
    move-object v0, p0

    .line 509
    .local v0, "current":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    .line 510
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Activity;

    if-eqz v3, :cond_1

    .line 511
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 516
    :cond_0
    return-object v2

    .line 513
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 514
    .local v1, "parent":Landroid/view/ViewParent;
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_2

    check-cast v1, Landroid/view/View;

    .end local v1    # "parent":Landroid/view/ViewParent;
    move-object v0, v1

    .line 515
    :goto_1
    goto :goto_0

    .restart local v1    # "parent":Landroid/view/ViewParent;
    :cond_2
    move-object v0, v2

    .line 514
    goto :goto_1
.end method

.method public static getAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 427
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->app_name:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAppSubtitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 431
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 432
    .local v1, "versionName":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    .line 433
    .local v0, "versionCode":I
    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->release:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getDefaultViewportDpi()F
    .locals 5

    .prologue
    .line 605
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    if-ne v3, v4, :cond_0

    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_PHONE:Ljava/util/Map;

    .line 607
    .local v0, "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getArticleFontSize()Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;

    move-result-object v2

    .line 608
    .local v2, "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    move-object v1, v2

    .line 609
    .local v1, "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    return v3

    .line 605
    .end local v0    # "dpiMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;Ljava/lang/Integer;>;"
    .end local v1    # "fontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    .end local v2    # "prefsFontSize":Lcom/google/apps/dots/shared/ArticleRenderSettings$FontSize;
    :cond_0
    sget-object v0, Lcom/google/apps/dots/shared/ArticleRenderSettings;->FONT_DPI_MAP_TABLET:Ljava/util/Map;

    goto :goto_0
.end method

.method public static getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 102
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v3, 0xf

    .line 103
    .local v2, "screenLayoutSize":I
    packed-switch v2, :pswitch_data_0

    .line 113
    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->NORMAL_TABLET:Lcom/google/apps/dots/shared/DeviceCategory;

    .line 116
    .local v1, "result":Lcom/google/apps/dots/shared/DeviceCategory;
    :goto_0
    return-object v1

    .line 106
    .end local v1    # "result":Lcom/google/apps/dots/shared/DeviceCategory;
    :pswitch_0
    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->PHONE:Lcom/google/apps/dots/shared/DeviceCategory;

    .line 107
    .restart local v1    # "result":Lcom/google/apps/dots/shared/DeviceCategory;
    goto :goto_0

    .line 109
    .end local v1    # "result":Lcom/google/apps/dots/shared/DeviceCategory;
    :pswitch_1
    sget-object v1, Lcom/google/apps/dots/shared/DeviceCategory;->SMALL_TABLET:Lcom/google/apps/dots/shared/DeviceCategory;

    .line 110
    .restart local v1    # "result":Lcom/google/apps/dots/shared/DeviceCategory;
    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getLargerDisplayDimension()I
    .locals 1

    .prologue
    .line 264
    sget v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->largerDisplayDimension:I

    if-gtz v0, :cond_0

    .line 265
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->computeLargerDisplayDimension()I

    move-result v0

    sput v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->largerDisplayDimension:I

    .line 267
    :cond_0
    sget v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->largerDisplayDimension:I

    return v0
.end method

.method public static getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 496
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 497
    .local v0, "activity":Landroid/app/Activity;
    instance-of v1, v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v1, :cond_0

    .line 498
    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 500
    .end local v0    # "activity":Landroid/app/Activity;
    :goto_0
    return-object v0

    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 322
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/apps/dots/shared/Orientation;->PORTRAIT:Lcom/google/apps/dots/shared/Orientation;

    goto :goto_0
.end method

.method public static getResourceName(I)Ljava/lang/String;
    .locals 2
    .param p0, "resId"    # I

    .prologue
    .line 579
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    .line 580
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 582
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 587
    :goto_0
    return-object v1

    .line 583
    :catch_0
    move-exception v1

    .line 587
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getSmallerDisplayDimension()I
    .locals 1

    .prologue
    .line 271
    sget v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->smallerDisplayDimension:I

    if-gtz v0, :cond_0

    .line 272
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->computeSmallerDisplayDimension()I

    move-result v0

    sput v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->smallerDisplayDimension:I

    .line 274
    :cond_0
    sget v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->smallerDisplayDimension:I

    return v0
.end method

.method public static getStackTrace(Ljava/lang/Thread;)Ljava/lang/String;
    .locals 6
    .param p0, "optThread"    # Ljava/lang/Thread;

    .prologue
    .line 448
    if-nez p0, :cond_0

    .line 449
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p0

    .line 451
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 452
    .local v2, "stackTraceElements":[Ljava/lang/StackTraceElement;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 453
    .local v1, "sb":Ljava/lang/StringBuilder;
    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 454
    .local v0, "element":Ljava/lang/StackTraceElement;
    const/16 v5, 0x9

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 455
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 458
    .end local v0    # "element":Ljava/lang/StackTraceElement;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 7
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 415
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 416
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 417
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Couldn\'t find launch intent for app %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v4, v5, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 423
    :goto_0
    return v3

    .line 420
    :cond_0
    const/high16 v4, 0x10000

    invoke-virtual {p0, v0, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 422
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "ResolveInfo: %s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 423
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public static isBlindAccessibilityEnabledFromContext(Landroid/content/Context;)Z
    .locals 20
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 343
    invoke-static/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->isTouchExplorationEnabledFromContext(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 344
    const/4 v2, 0x1

    .line 398
    :goto_0
    return v2

    .line 347
    :cond_0
    const-string v2, "accessibility"

    .line 348
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/accessibility/AccessibilityManager;

    .line 352
    .local v7, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    const/16 v17, 0x1

    .line 354
    .local v17, "spoken":I
    move/from16 v0, v17

    invoke-static {v7, v0}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat;->getEnabledAccessibilityServiceList(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;

    move-result-object v16

    .line 355
    .local v16, "services":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v13, 0x1

    .line 356
    .local v13, "screenReader":Z
    :goto_1
    if-eqz v13, :cond_2

    .line 357
    const/4 v2, 0x1

    goto :goto_0

    .line 355
    .end local v13    # "screenReader":Z
    :cond_1
    const/4 v13, 0x0

    goto :goto_1

    .line 362
    .restart local v13    # "screenReader":Z
    :cond_2
    const-string v10, "android.accessibilityservice.AccessibilityService"

    .line 363
    .local v10, "intentAction":Ljava/lang/String;
    const-string v11, "android.accessibilityservice.category.FEEDBACK_SPOKEN"

    .line 367
    .local v11, "intentCategory":Ljava/lang/String;
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 368
    .local v14, "screenReaderIntent":Landroid/content/Intent;
    invoke-virtual {v14, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v14, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v15

    .line 371
    .local v15, "screenReaders":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 373
    .local v1, "cr":Landroid/content/ContentResolver;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/pm/ResolveInfo;

    .line 376
    .local v12, "reader":Landroid/content/pm/ResolveInfo;
    iget-object v2, v12, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "content://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".providers.StatusProvider"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 379
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 381
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 384
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v18

    .line 385
    .local v18, "status":I
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_4

    .line 386
    const/4 v2, 0x1

    .line 392
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v18    # "status":I
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 389
    :catch_0
    move-exception v9

    .line 390
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Could not detect screen reader due to broken cursor."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v9, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 398
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v12    # "reader":Landroid/content/pm/ResolveInfo;
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public static isTouchExplorationEnabledFromContext(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 328
    const-string v1, "accessibility"

    .line 329
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 330
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    return v1
.end method

.method public static showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V
    .locals 8
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "dialogFragment"    # Landroid/support/v4/app/DialogFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 544
    if-eqz p0, :cond_1

    .line 547
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 549
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 550
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 551
    .local v2, "prev":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_0

    .line 552
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 554
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 555
    invoke-virtual {p1, v1, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 560
    .end local v0    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v2    # "prev":Landroid/support/v4/app/Fragment;
    :cond_1
    :goto_0
    return-void

    .line 556
    :catch_0
    move-exception v3

    .line 557
    .local v3, "tr":Ljava/lang/Throwable;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Error showing dialog %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private validateDpi(F)F
    .locals 7
    .param p1, "dpi"    # F

    .prologue
    const/high16 v4, 0x43a00000    # 320.0f

    const/high16 v3, 0x43700000    # 240.0f

    const/high16 v2, 0x43550000    # 213.0f

    const/high16 v6, 0x43200000    # 160.0f

    .line 195
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v1

    .line 196
    .local v0, "densityDpi":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v1, :sswitch_data_0

    move p1, v0

    .line 212
    .end local p1    # "dpi":F
    :cond_0
    :goto_0
    return p1

    .line 198
    .restart local p1    # "dpi":F
    :sswitch_0
    cmpl-float v1, p1, v4

    if-gtz v1, :cond_0

    move p1, v0

    goto :goto_0

    .line 200
    :sswitch_1
    cmpl-float v1, p1, v3

    if-lez v1, :cond_1

    const/high16 v1, 0x43f00000    # 480.0f

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    :cond_1
    move p1, v0

    goto :goto_0

    .line 202
    :sswitch_2
    cmpl-float v1, p1, v2

    if-lez v1, :cond_2

    cmpg-float v1, p1, v4

    if-ltz v1, :cond_0

    :cond_2
    move p1, v0

    goto :goto_0

    .line 204
    :sswitch_3
    cmpl-float v1, p1, v6

    if-lez v1, :cond_3

    cmpg-float v1, p1, v3

    if-ltz v1, :cond_0

    :cond_3
    move p1, v0

    goto :goto_0

    .line 207
    :sswitch_4
    const/high16 v1, 0x42f00000    # 120.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_4

    cmpg-float v1, p1, v2

    if-ltz v1, :cond_0

    :cond_4
    move p1, v0

    goto :goto_0

    .line 209
    :sswitch_5
    float-to-double v2, p1

    const-wide/high16 v4, 0x4058000000000000L    # 96.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_5

    cmpg-float v1, p1, v6

    if-ltz v1, :cond_0

    :cond_5
    move p1, v0

    goto :goto_0

    .line 196
    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_5
        0xa0 -> :sswitch_4
        0xd5 -> :sswitch_3
        0xf0 -> :sswitch_2
        0x140 -> :sswitch_1
        0x1e0 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getDefaultTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 1

    .prologue
    .line 283
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getScaledDefaultTransform(F)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    return-object v0
.end method

.method public getDensityDpi()I
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method public getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->deviceCategory:Lcom/google/apps/dots/shared/DeviceCategory;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->deviceCategory:Lcom/google/apps/dots/shared/DeviceCategory;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->deviceCategory:Lcom/google/apps/dots/shared/DeviceCategory;

    return-object v0
.end method

.method public getDeviceIdString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLegacyTemplate(Lcom/google/apps/dots/proto/client/DotsShared$Form;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 3
    .param p1, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 134
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 135
    :cond_0
    const/4 v1, 0x0

    .line 150
    :cond_1
    :goto_0
    return-object v1

    .line 137
    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v1

    .line 138
    .local v1, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasPostTemplate()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getPostTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    .line 143
    .local v0, "postTemplate":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :try_start_0
    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v2

    .line 142
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/CodedInputByteBufferNano;

    move-result-object v2

    .line 141
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 146
    :catch_0
    move-exception v2

    goto :goto_0

    .line 144
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public getLocaleLayoutDirection()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 241
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    .line 244
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMetrics()Landroid/util/DisplayMetrics;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    return-object v0
.end method

.method public getPerApplicationMemoryClass()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->memoryClass:I

    return v0
.end method

.method public getPixelSize(ILandroid/content/Context;)I
    .locals 3
    .param p1, "id"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 225
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 226
    .local v0, "pixelSize":Landroid/util/TypedValue;
    invoke-virtual {p2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 227
    iget v1, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v1

    return v1
.end method

.method public getPixelsFromDips(I)F
    .locals 2
    .param p1, "dip"    # I

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    return v0
.end method

.method public getScaledDefaultTransform(F)Lcom/google/apps/dots/android/newsstand/server/Transform;
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 278
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getLargerDisplayDimension()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 279
    .local v0, "pixels":I
    new-instance v1, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->square(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    return-object v1
.end method

.method public getSystemAnalyticsId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->analytics_application_id:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSystemDebugAnalyticsId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->appContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->debug_analytics_id:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 2
    .param p1, "displayTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .prologue
    .line 120
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil$1;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory()Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 129
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->getMainTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hasPhoneTemplate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->getPhoneTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->getMainTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getXDpi()F
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->validateDpi(F)F

    move-result v0

    return v0
.end method

.method public getYDpi()F
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->validateDpi(F)F

    move-result v0

    return v0
.end method

.method public isBlindAccessibilityEnabled()Z
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x0

    return v0
.end method

.method public isLocaleRtl()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 236
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getLocaleLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLowMemoryDevice()Z
    .locals 2

    .prologue
    .line 318
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->memoryClass:I

    const/16 v1, 0x60

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scaleForMemoryClass(II)I
    .locals 3
    .param p1, "base"    # I
    .param p2, "maxScale"    # I

    .prologue
    .line 307
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->memoryClass:I

    int-to-float v1, v1

    const/high16 v2, 0x42400000    # 48.0f

    div-float/2addr v1, v2

    int-to-float v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 308
    .local v0, "scale":F
    const/4 v1, 0x1

    int-to-float v2, p1

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method
