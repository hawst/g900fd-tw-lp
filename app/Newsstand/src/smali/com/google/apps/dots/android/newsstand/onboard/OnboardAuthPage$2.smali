.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "OnboardAuthPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->onAuthSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$300(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->shouldShowOnboardingQuiz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->getPageList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardQuizPage;->addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->goToNextPage()V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->finishOnboardFlow()V

    goto :goto_0
.end method
