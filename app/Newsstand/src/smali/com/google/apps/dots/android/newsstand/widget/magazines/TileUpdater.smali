.class Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
.super Ljava/lang/Object;
.source "LazyImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;,
        Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;
    }
.end annotation


# static fields
.field private static final ALLOCATION_LIMIT:I = 0x8

.field private static final DRAW_TILE_BOUNDARIES:Z = false

.field public static final INVISIBLE_THRESHOLDS:[F

.field public static final LARGE_BITMAP_THRESHOLD:F = 1.2f

.field static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MIN_SIZE_FOR_HIGH_RES:I = 0x5

.field public static final SMALL_BITMAP_THRESHOLD:F = 0.25f

.field private static final SWIPE_DISTANCE_RADIUS:F = 1.1f

.field private static final UPDATE_RETRY_LIMIT:I = 0x14

.field private static final UPDATE_TILES_DELAY:I = 0x14

.field private static final UPDATE_TILES_UI_DELAY:I = 0x32

.field public static final VISIBLE_THRESHOLDS:[F

.field private static final interruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

.field private static final uninterruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;


# instance fields
.field private final bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

.field private final coveredRegion:Landroid/graphics/Region;

.field private final currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

.field private final invisibleThresholdsPx:[I

.field private isUpdatingTiles:Z

.field private final lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

.field private final memClass:I

.field private final paint:Landroid/graphics/Paint;

.field private scale:F

.field private final screenful:I

.field private final sourceBitmapsInTiles:[Landroid/graphics/Bitmap;

.field private final tempRegion:Landroid/graphics/Region;

.field private final tileScreenResThresholdPx:I

.field private tileSize:I

.field private final tileStates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;",
            ">;"
        }
    .end annotation
.end field

.field private updateFailuresSinceLastRequest:I

.field private final updateTilesRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private final visibleThresholdsPx:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 313
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 315
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->uninterruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    .line 316
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;-><init>(I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->interruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    .line 358
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->VISIBLE_THRESHOLDS:[F

    .line 359
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->INVISIBLE_THRESHOLDS:[F

    return-void

    .line 358
    nop

    :array_0
    .array-data 4
        0x41000000    # 8.0f
        0x41400000    # 12.0f
        0x41a00000    # 20.0f
    .end array-data

    .line 359
    :array_1
    .array-data 4
        0x40400000    # 3.0f
        0x40a00000    # 5.0f
        0x40e00000    # 7.0f
    .end array-data
.end method

.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)V
    .locals 6
    .param p1, "lazyImageView"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .prologue
    const/4 v4, 0x0

    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z

    .line 387
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I

    .line 410
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->VISIBLE_THRESHOLDS:[F

    array-length v3, v3

    new-array v3, v3, [Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;

    .line 412
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;-><init>()V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    .line 421
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;

    .line 426
    new-instance v3, Landroid/graphics/Region;

    invoke-direct {v3}, Landroid/graphics/Region;-><init>()V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;

    .line 427
    new-instance v3, Landroid/graphics/Region;

    invoke-direct {v3}, Landroid/graphics/Region;-><init>()V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tempRegion:Landroid/graphics/Region;

    .line 430
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .line 432
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->paint:Landroid/graphics/Paint;

    .line 433
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->paint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 434
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->paint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 436
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bitmapPool()Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    .line 438
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    .line 439
    .local v2, "util":Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;
    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 442
    .local v1, "screenRect":Landroid/graphics/RectF;
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40200000    # 2.5f

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    .line 443
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    const/16 v4, 0x100

    const/16 v5, 0x200

    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(III)I

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    .line 445
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    and-int/lit8 v3, v3, -0x10

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    .line 447
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->screenful:I

    .line 449
    const v3, 0x3f99999a    # 1.2f

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->screenful:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileScreenResThresholdPx:I

    .line 451
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->VISIBLE_THRESHOLDS:[F

    array-length v3, v3

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I

    .line 452
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->INVISIBLE_THRESHOLDS:[F

    array-length v3, v3

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->invisibleThresholdsPx:[I

    .line 453
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 454
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->VISIBLE_THRESHOLDS:[F

    aget v4, v4, v0

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->screenful:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v3, v0

    .line 455
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->invisibleThresholdsPx:[I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->INVISIBLE_THRESHOLDS:[F

    aget v4, v4, v0

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->screenful:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v3, v0

    .line 453
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 458
    :cond_0
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPerApplicationMemoryClass()I

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->memClass:I

    .line 460
    new-instance v3, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)V

    invoke-direct {v3, v4, v5}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateTilesRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 466
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$100(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 312
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sampleSizeForIndex(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/List;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 312
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->computeTileSizes(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->memClass:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->invisibleThresholdsPx:[I

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileScreenResThresholdPx:I

    return v0
.end method

.method static synthetic access$1702(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .param p1, "x1"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I

    return v0
.end method

.method static synthetic access$1808(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I

    return v0
.end method

.method static synthetic access$1900(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/Map;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;
    .param p2, "x2"    # Ljava/util/Collection;

    .prologue
    .line 312
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->extractTilesWithState(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/android/libraries/bind/async/DelayedRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateTilesRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Ljava/util/Map;Landroid/graphics/RectF;)Ljava/util/Map$Entry;
    .locals 1
    .param p0, "x0"    # Ljava/util/Map;
    .param p1, "x1"    # Landroid/graphics/RectF;

    .prologue
    .line 312
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->findTile(Ljava/util/Map;Landroid/graphics/RectF;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->paint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .prologue
    .line 312
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F

    return v0
.end method

.method static synthetic access$802(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .param p1, "x1"    # F

    .prologue
    .line 312
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F

    return p1
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Landroid/graphics/Region;Landroid/graphics/RectF;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .param p1, "x1"    # Landroid/graphics/Region;
    .param p2, "x2"    # Landroid/graphics/RectF;

    .prologue
    .line 312
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->regionContainsRect(Landroid/graphics/Region;Landroid/graphics/RectF;)Z

    move-result v0

    return v0
.end method

.method private static closeEnough(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 485
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static closeEnough(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2
    .param p0, "a"    # Landroid/graphics/RectF;
    .param p1, "b"    # Landroid/graphics/RectF;

    .prologue
    .line 489
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->closeEnough(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->closeEnough(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    .line 490
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->closeEnough(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->closeEnough(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private computeTileSizes(Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 533
    .local p1, "tiles":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;>;"
    const/4 v1, 0x0

    .line 534
    .local v1, "result":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    .line 535
    .local v2, "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 536
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 537
    goto :goto_0

    .line 538
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    :cond_0
    return v1
.end method

.method private static extractTilesWithState(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V
    .locals 3
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;",
            ">;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p0, "tileStates":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    .local p2, "output":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;>;"
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 523
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 524
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 525
    .local v1, "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 526
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 527
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 530
    .end local v1    # "next":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    :cond_1
    return-void
.end method

.method private static findTile(Ljava/util/Map;Landroid/graphics/RectF;)Ljava/util/Map$Entry;
    .locals 3
    .param p1, "region"    # Landroid/graphics/RectF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;",
            ">;",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    .local p0, "tiles":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 496
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getRegion()Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->closeEnough(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private regionContainsRect(Landroid/graphics/Region;Landroid/graphics/RectF;)Z
    .locals 4
    .param p1, "region"    # Landroid/graphics/Region;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x2

    .line 508
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 509
    .local v1, "tempRect":Landroid/graphics/Rect;
    invoke-virtual {p2, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 510
    invoke-virtual {v1, v2, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 511
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tempRegion:Landroid/graphics/Region;

    invoke-virtual {v2, p1}, Landroid/graphics/Region;->set(Landroid/graphics/Region;)Z

    .line 512
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tempRegion:Landroid/graphics/Region;

    sget-object v3, Landroid/graphics/Region$Op;->REVERSE_DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Region;->op(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 513
    .local v0, "result":Z
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Rect;)V

    .line 514
    return v0

    .line 512
    .end local v0    # "result":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static sampleSizeForIndex(I)I
    .locals 1
    .param p0, "sampleSizeIndex"    # I

    .prologue
    .line 481
    const/4 v0, 0x1

    shl-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public isUpdatingTiles()Z
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z

    return v0
.end method

.method public requestUpdate()V
    .locals 4

    .prologue
    .line 472
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I

    .line 473
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateTilesRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v2, 0x32

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 474
    return-void
.end method

.method public update()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 548
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isReadyForTileUpdate()Z

    move-result v3

    if-nez v3, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->update(Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;)V

    .line 556
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z

    if-nez v3, :cond_0

    .line 559
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z

    .line 561
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .line 563
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getTiles()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    .line 565
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getBitmapInfo()Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->getNumPixels()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3e800000    # 0.25f

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->screenful:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 567
    .local v2, "uninterrupted":Z
    :cond_3
    :goto_1
    const/high16 v3, 0x447a0000    # 1000.0f

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget v4, v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->swipeDistance:F

    mul-float/2addr v3, v4

    float-to-long v0, v3

    .line 568
    .local v0, "priority":J
    if-eqz v2, :cond_5

    .line 569
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->uninterruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->PRIORITY_UNINTERRUPTED:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v4, p0, v5, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/concurrent/Executor;J)V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 565
    .end local v0    # "priority":J
    .end local v2    # "uninterrupted":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 571
    .restart local v0    # "priority":J
    .restart local v2    # "uninterrupted":Z
    :cond_5
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->interruptedQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v4, p0, v5, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/concurrent/Executor;J)V

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
