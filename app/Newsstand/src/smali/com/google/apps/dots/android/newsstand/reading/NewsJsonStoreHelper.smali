.class public final Lcom/google/apps/dots/android/newsstand/reading/NewsJsonStoreHelper;
.super Ljava/lang/Object;
.source "NewsJsonStoreHelper.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataAttachmentBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$Attachments;->exportedContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getJsonDataContent(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Lcom/google/apps/dots/proto/client/DotsShared$Form;Z)Ljava/lang/String;
    .locals 5
    .param p1, "articleTemplate"    # Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .param p2, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .param p3, "useLegacyLayout"    # Z

    .prologue
    .line 25
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s<style>%s</style>"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->getTemplate()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-nez p2, :cond_0

    const-string v0, ""

    .line 27
    :goto_0
    aput-object v0, v3, v4

    .line 26
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplateCss()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
