.class public Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;,
        Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;,
        Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static newsBaseHtmlFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private appFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            ">;"
        }
    .end annotation
.end field

.field private final appId:Ljava/lang/String;

.field private articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation
.end field

.field private formFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            ">;"
        }
    .end annotation
.end field

.field private idToAdTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation
.end field

.field private final isMagazine:Z

.field private loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private postFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ">;"
        }
    .end annotation
.end field

.field private final postId:Ljava/lang/String;

.field protected resourceLoadListener:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;

.field private sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "sectionId"    # Ljava/lang/String;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "isMagazine"    # Z

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appId:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionId:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postId:Ljava/lang/String;

    .line 75
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->isMagazine:Z

    .line 76
    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->useLegacyLayout(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createIdToAdTemplateMapFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;[Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # [Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->collectAdTemplateIds([Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Ljava/util/Set;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadIdToAdTemplateMap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private varargs collectAdTemplateIds([Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/util/Set;
    .locals 5
    .param p1, "allSettings"    # [Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 293
    .local v1, "templateIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v0, p1, v2

    .line 294
    .local v0, "settings":Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    if-nez v0, :cond_1

    .line 293
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hasPubSold()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 298
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 299
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getPubSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 301
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->hasGoogleSold()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 302
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;->getGoogleSold()Lcom/google/apps/dots/proto/client/DotsShared$AdContent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$AdContent;->getAdTemplateId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 306
    .end local v0    # "settings":Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;
    :cond_3
    return-object v1
.end method

.method private createArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    .line 181
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 180
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private createArticleTemplateFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 203
    .local v0, "formFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Form;>;"
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 204
    .local v1, "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    .line 205
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->whenAllDone([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$5;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 204
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    return-object v2
.end method

.method private createIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 245
    .line 246
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 245
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private createIdToAdTemplateMapFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 261
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 262
    .local v0, "empty":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method private loadIdToAdTemplateMap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 267
    .local p2, "adTemplateIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 269
    .local v2, "empty":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 275
    .end local v2    # "empty":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    :goto_0
    return-object v3

    .line 271
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 272
    .local v1, "allFutures":Ljava/util/List;, "Ljava/util/List<Lcom/google/common/util/concurrent/ListenableFuture<+Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;>;>;"
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    .local v0, "adTemplateId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    move-result-object v4

    invoke-virtual {v4, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 276
    .end local v0    # "adTemplateId":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList(Ljava/util/Collection;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$8;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$8;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 275
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0
.end method

.method private static shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->wasFailure(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private useLegacyLayout(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z
    .locals 2
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;

    .prologue
    .line 318
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->hasLayoutEngineVersionOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getLayoutEngineVersionOverride()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/AppStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 101
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->appFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->isMagazine:Z

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 173
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 170
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->articleTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "widget"    # Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->isMagazine:Z

    if-eqz v0, :cond_0

    .line 342
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    .line 344
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 345
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getHtmlFromFileFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    aput-object v2, v0, v1

    .line 343
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$10;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;)V

    .line 342
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 368
    :goto_0
    return-object v0

    .line 369
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getHtmlFromFileFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;)V

    .line 368
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    monitor-enter p0

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 136
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 145
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 147
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->formFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 147
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getHtmlFromFileFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 380
    .line 381
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getUseLegacyLayoutFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$12;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 380
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public getIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 218
    monitor-enter p0

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->idToAdTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->isMagazine:Z

    if-eqz v0, :cond_1

    .line 223
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$6;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$6;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->idToAdTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 238
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->idToAdTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 235
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->idToAdTemplateFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getNewsBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->newsBaseHtmlFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    const-string v0, "webview.html"

    .line 402
    .local v0, "id":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->layoutStore()Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/LayoutStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    sput-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->newsBaseHtmlFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 404
    .end local v0    # "id":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->newsBaseHtmlFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 405
    sget-object v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->newsBaseHtmlFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$13;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$13;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    monitor-enter p0

    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 118
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 129
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->postId:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->shouldReinitialize(Lcom/google/common/util/concurrent/ListenableFuture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->sectionStore()Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/SectionStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedNotifyingCallback;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$1;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 111
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->sectionFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 111
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getUseLegacyLayoutFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->isMagazine:Z

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$9;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    .line 325
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 334
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public getUserRolesFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$RoleList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;-><init>()V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public prefetchMagazineResources(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 436
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 437
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 438
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 439
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 440
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 441
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 442
    return-void
.end method

.method public prefetchNewsResources(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 421
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getApplicationFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 422
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getSectionFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 423
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 424
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 425
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 426
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 427
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getNewsBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 428
    return-void
.end method

.method public setResourceLoadFailedListener(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->resourceLoadListener:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ResourceLoadFailedListener;

    .line 80
    return-void
.end method
