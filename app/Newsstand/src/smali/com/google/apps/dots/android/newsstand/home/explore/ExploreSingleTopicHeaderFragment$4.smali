.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;
.super Lcom/google/android/libraries/bind/data/BaseDataSetObserver;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->setupAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/BaseDataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->hasRefreshedOnce()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragmentState;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    .line 188
    .local v0, "topic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$4;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setSingleTabTitle(Ljava/lang/CharSequence;)V

    .line 190
    .end local v0    # "topic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    :cond_0
    return-void
.end method
