.class public Lcom/google/apps/dots/android/newsstand/file/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/file/FileUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cleanup(Ljava/io/OutputStream;ZLjava/io/File;)V
    .locals 2
    .param p0, "os"    # Ljava/io/OutputStream;
    .param p1, "deleteFile"    # Z
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 188
    if-eqz p0, :cond_0

    .line 190
    :try_start_0
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 196
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->removeFile(Ljava/io/File;)Z

    .line 198
    :cond_1
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "Error closing output stream"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static closeQuietly(Ljava/io/Closeable;)V
    .locals 4
    .param p0, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 315
    if-eqz p0, :cond_0

    .line 316
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 318
    :catch_0
    move-exception v0

    .line 319
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Failure closing Closeable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static copyDir(Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "srcDir"    # Ljava/io/File;
    .param p1, "destDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->copyDirInternal(Ljava/io/File;Ljava/io/File;)V

    .line 293
    return-void
.end method

.method private static copyDirInternal(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .param p0, "srcDir"    # Ljava/io/File;
    .param p1, "destDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 296
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_0

    .line 297
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Source %s is not a directory."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 299
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_1

    .line 300
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Couldn\'t create destination %s."

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 302
    :cond_1
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->listFilesNonNull(Ljava/io/File;)[Ljava/io/File;

    move-result-object v2

    .line 303
    .local v2, "srcFiles":[Ljava/io/File;
    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v2, v3

    .line 304
    .local v1, "srcFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 305
    .local v0, "destFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 306
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->copyDirInternal(Ljava/io/File;Ljava/io/File;)V

    .line 303
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 308
    :cond_2
    invoke-static {v1, v0}, Lcom/google/common/io/Files;->copy(Ljava/io/File;Ljava/io/File;)V

    goto :goto_1

    .line 311
    .end local v0    # "destFile":Ljava/io/File;
    .end local v1    # "srcFile":Ljava/io/File;
    :cond_3
    return-void
.end method

.method private static createTempFileFor(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .param p0, "base"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static deleteDir(Ljava/io/File;)Z
    .locals 1
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    .line 240
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDirInternal(Ljava/io/File;)V

    .line 242
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static deleteDirInternal(Ljava/io/File;)V
    .locals 5
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    .line 225
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 226
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 227
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 228
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 229
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDirInternal(Ljava/io/File;)V

    .line 227
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 231
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 235
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 236
    return-void
.end method

.method private static handle(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/IOException;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "e"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    const-string v1, "Error %s writing %s to file %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    .line 178
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "message":Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;->detect(Ljava/io/IOException;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;->create(Ljava/lang/Throwable;)Lcom/google/apps/dots/android/newsstand/exception/NoSpaceLeftException;

    move-result-object v1

    throw v1

    .line 183
    :cond_0
    throw p3
.end method

.method public static isAncestor(Ljava/io/File;Ljava/io/File;)Z
    .locals 1
    .param p0, "ancestor"    # Ljava/io/File;
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 77
    :cond_0
    invoke-virtual {p1, p0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    .line 80
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    if-nez p1, :cond_0

    .line 81
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static listFilenamesNonNull(Ljava/io/File;)[Ljava/lang/String;
    .locals 2
    .param p0, "directory"    # Ljava/io/File;

    .prologue
    .line 60
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "ls":[Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    .end local v0    # "ls":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static listFilesNonNull(Ljava/io/File;)[Ljava/io/File;
    .locals 2
    .param p0, "directory"    # Ljava/io/File;

    .prologue
    .line 34
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 35
    .local v0, "ls":[Ljava/io/File;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/io/File;

    .end local v0    # "ls":[Ljava/io/File;
    :cond_0
    return-object v0
.end method

.method public static move(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p0, "oldFile"    # Ljava/io/File;
    .param p1, "newFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/common/io/Files;->move(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "ioe":Ljava/io/IOException;
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 267
    invoke-static {p0, p1}, Lcom/google/common/io/Files;->move(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0
.end method

.method public static moveDir(Ljava/io/File;Ljava/io/File;)V
    .locals 4
    .param p0, "srcDir"    # Ljava/io/File;
    .param p1, "destDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->copyDir(Ljava/io/File;Ljava/io/File;)V

    .line 278
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->deleteDir(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to delete source directory %s after copying to %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    return-void
.end method

.method public static removeFile(Ljava/io/File;)Z
    .locals 5
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 215
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 221
    :cond_0
    :goto_0
    return v1

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unable to remove file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v1

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static writeBitmap(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)V
    .locals 8
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "compressFormat"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p5, "quality"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 145
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->createTempFileFor(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 146
    .local v4, "tmpPath":Ljava/io/File;
    const/4 v1, 0x0

    .line 147
    .local v1, "os":Ljava/io/OutputStream;
    const/4 v3, 0x0

    .line 149
    .local v3, "success":Z
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v1    # "os":Ljava/io/OutputStream;
    .local v2, "os":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {p3, p4, p5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 151
    invoke-virtual {v4, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 155
    if-nez v3, :cond_0

    :goto_0
    invoke-static {v2, v5, v4}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->cleanup(Ljava/io/OutputStream;ZLjava/io/File;)V

    move-object v1, v2

    .line 157
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v1    # "os":Ljava/io/OutputStream;
    :goto_1
    return-void

    .end local v1    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :cond_0
    move v5, v6

    .line 155
    goto :goto_0

    .line 152
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v1    # "os":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    invoke-static {p0, p1, v4, v0}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->handle(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/IOException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    if-nez v3, :cond_1

    :goto_3
    invoke-static {v1, v5, v4}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->cleanup(Ljava/io/OutputStream;ZLjava/io/File;)V

    goto :goto_1

    :cond_1
    move v5, v6

    goto :goto_3

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_4
    if-nez v3, :cond_2

    :goto_5
    invoke-static {v1, v5, v4}, Lcom/google/apps/dots/android/newsstand/file/FileUtil;->cleanup(Ljava/io/OutputStream;ZLjava/io/File;)V

    throw v7

    :cond_2
    move v5, v6

    goto :goto_5

    .end local v1    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v1    # "os":Ljava/io/OutputStream;
    goto :goto_4

    .line 152
    .end local v1    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v1    # "os":Ljava/io/OutputStream;
    goto :goto_2
.end method
