.class public Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;
.super Landroid/support/v7/widget/Toolbar;
.source "BugFixinToolbar.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private disableChildrenA11y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->init()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->init()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->init()V

    .line 39
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->disableChildrenA11y:Z

    return p1
.end method

.method private init()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;)V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 56
    return-void
.end method


# virtual methods
.method public addChildrenForAccessibility(Ljava/util/ArrayList;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "childrenForAccessibility":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->disableChildrenA11y:Z

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/BugFixinToolbar;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Snuffing addChildrenForAccessibility"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/Toolbar;->addChildrenForAccessibility(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
