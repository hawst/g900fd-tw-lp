.class Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "KeepEditionMenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;->this$0:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 1
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 114
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasMeteredPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;->this$0:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;->showPinningMeteredContentDialog(Landroid/app/Activity;)V

    .line 117
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
