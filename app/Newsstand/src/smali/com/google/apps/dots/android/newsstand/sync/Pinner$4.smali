.class Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "Pinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpinUnsubscribedHelper(Landroid/accounts/Account;Ljava/lang/String;Ljava/util/EnumSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field editions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$filterTypes:Ljava/util/EnumSet;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Landroid/accounts/Account;Ljava/util/EnumSet;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .prologue
    .line 476
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->val$filterTypes:Ljava/util/EnumSet;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method getEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Set;
    .locals 2
    .param p1, "libraryRoot"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->editions:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 481
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->editions:Ljava/util/Set;

    .line 482
    new-instance v0, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->editions:Ljava/util/Set;

    return-object v0
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 9
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 505
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    # getter for: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->lock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$300(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 506
    :try_start_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->loadPinnedAccounts()Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$400(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;

    move-result-object v3

    .line 507
    .local v3, "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->val$account:Landroid/accounts/Account;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getPinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    invoke-static {v6, v3, v8}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$500(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$Pinned;

    move-result-object v2

    .line 508
    .local v2, "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    iget-object v6, v2, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 509
    .local v4, "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 510
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 511
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v6

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 512
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    .line 513
    .local v5, "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->val$filterTypes:Ljava/util/EnumSet;

    invoke-virtual {v6, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 515
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->getEditions(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 516
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 521
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    .end local v2    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .end local v3    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .end local v4    # "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    .end local v5    # "type":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 519
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    .restart local v2    # "pinned":Lcom/google/apps/dots/proto/client/DotsClient$Pinned;
    .restart local v3    # "pinnedAccounts":Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;
    .restart local v4    # "pinnedItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;>;"
    :cond_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    iput-object v6, v2, Lcom/google/apps/dots/proto/client/DotsClient$Pinned;->item:[Lcom/google/apps/dots/proto/client/DotsClient$Pinned$PinnedItem;

    .line 520
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->this$0:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    # invokes: Lcom/google/apps/dots/android/newsstand/sync/Pinner;->storePinned(Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V
    invoke-static {v6, v3, v2}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->access$600(Lcom/google/apps/dots/android/newsstand/sync/Pinner;Lcom/google/apps/dots/proto/client/DotsClient$PinnedAccounts;Lcom/google/apps/dots/proto/client/DotsClient$Pinned;)V

    .line 521
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 476
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner$4;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
