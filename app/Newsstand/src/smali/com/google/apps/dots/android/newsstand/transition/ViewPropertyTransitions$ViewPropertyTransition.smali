.class Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;
.super Landroid/transition/Transition;
.source "ViewPropertyTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewPropertyTransition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/transition/Transition;"
    }
.end annotation


# instance fields
.field private final isEntering:Z

.field private final originValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final transitionProperties:[Ljava/lang/String;

.field private final viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;Ljava/lang/Object;Z)V
    .locals 3
    .param p3, "isEntering"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy",
            "<TT;>;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    .local p1, "viewPropertyProxy":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy<TT;>;"
    .local p2, "originValue":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;

    .line 167
    iput-boolean p3, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->isEntering:Z

    .line 168
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->originValue:Ljava/lang/Object;

    .line 169
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->getPropertyName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->transitionProperties:[Ljava/lang/String;

    .line 170
    return-void
.end method

.method private captureValues(Landroid/transition/TransitionValues;Z)V
    .locals 3
    .param p1, "values"    # Landroid/transition/TransitionValues;
    .param p2, "useOriginValue"    # Z

    .prologue
    .line 183
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->originValue:Ljava/lang/Object;

    .line 184
    .local v0, "value":Ljava/lang/Object;, "TT;"
    :goto_0
    if-eqz v0, :cond_0

    .line 185
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->getPropertyName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_0
    return-void

    .line 183
    .end local v0    # "value":Ljava/lang/Object;, "TT;"
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;

    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;->getPropertyValue(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private getPropertyName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 212
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    const-string v1, "android:viewPropertyTransition:"

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;->getViewPropertyName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 1
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 179
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->isEntering:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->captureValues(Landroid/transition/TransitionValues;Z)V

    .line 180
    return-void

    .line 179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 1
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 174
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->isEntering:Z

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->captureValues(Landroid/transition/TransitionValues;Z)V

    .line 175
    return-void
.end method

.method protected createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;)Landroid/animation/Animator;
    .locals 4
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "TT;TT;)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    .local p3, "startValue":Ljava/lang/Object;, "TT;"
    .local p4, "endValue":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;

    .line 205
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;->getViewPropertyName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->viewPropertyProxy:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;

    .line 206
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;->getTypeEvaluator()Landroid/animation/TypeEvaluator;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    .line 203
    invoke-static {p2, v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 5
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 193
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 194
    iget-object v2, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 195
    .local v2, "view":Landroid/view/View;
    iget-object v3, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 196
    .local v1, "startValue":Ljava/lang/Object;, "TT;"
    iget-object v3, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 197
    .local v0, "endValue":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1, v2, v1, v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;)Landroid/animation/Animator;

    move-result-object v3

    .line 199
    .end local v0    # "endValue":Ljava/lang/Object;, "TT;"
    .end local v1    # "startValue":Ljava/lang/Object;, "TT;"
    .end local v2    # "view":Landroid/view/View;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;, "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition<TT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyTransition;->transitionProperties:[Ljava/lang/String;

    return-object v0
.end method
