.class public Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;
.source "CardNewsItem.java"


# static fields
.field public static final DK_ABSTRACT:I

.field public static final DK_AUDIO_ON_CLICK_LISTENER:I

.field public static final DK_AUDIO_POST_ID:I

.field public static final DK_CARD_BACKGROUND:I

.field public static final DK_ENTITY:I

.field public static final DK_ENTITY_ID:I

.field public static final DK_ENTITY_IS_SUBSCRIBED:I

.field public static final DK_ENTITY_ON_CLICK_LISTENER:I

.field public static final DK_GALLERY_ON_CLICK_LISTENER:I

.field public static final DK_HAS_GALLERY:I

.field public static final DK_HEADLINE_TEXT_COLOR:I

.field public static final DK_IMAGE_ATTRIBUTION:I

.field public static final DK_IMAGE_ELIGIBLE_FOR_HEADER:I

.field public static final DK_IMAGE_HEIGHT:I

.field public static final DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

.field public static final DK_IMAGE_ID:I

.field public static final DK_IMAGE_WIDTH:I

.field public static final DK_IS_FULL_IMAGE:I

.field public static final DK_IS_READ:I

.field public static final DK_IS_VIDEO:I

.field public static final DK_NUM_AUDIO:I

.field public static final DK_SHOW_LOCK:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_SOURCE_IS_SUBSCRIBED:I

.field public static final DK_SOURCE_NAME:I

.field public static final DK_SOURCE_ON_CLICK_LISTENER:I

.field public static final DK_SOURCE_TEXT_COLOR:I

.field public static final DK_SOURCE_TRANSITION_NAME:I

.field public static final DK_STORY_ON_CLICK_LISTENER:I

.field public static final DK_TIME:I

.field public static final DK_TIME_TEXT_COLOR:I

.field public static final DK_TITLE:I

.field public static final DK_VERSION:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I

.field public static final LAYOUTS_COMPACT:[I

.field public static final LAYOUTS_FULL_IMAGE:[I

.field public static final LAYOUTS_FULL_IMAGE_COMPACT:[I

.field public static final LAYOUTS_NO_IMAGE:[I

.field public static final LAYOUTS_NO_IMAGE_COMPACT:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_version:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_VERSION:I

    .line 49
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TITLE:I

    .line 51
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    .line 53
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageHeight:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_HEIGHT:I

    .line 55
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageWidth:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_WIDTH:I

    .line 57
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    .line 59
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageAttribution:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ATTRIBUTION:I

    .line 61
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_imageEligibleForHeader:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ELIGIBLE_FOR_HEADER:I

    .line 64
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ICON_ID:I

    .line 66
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    .line 68
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceIsSubscribed:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_IS_SUBSCRIBED:I

    .line 70
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_time:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME:I

    .line 72
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ON_CLICK_LISTENER:I

    .line 74
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_abstract:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ABSTRACT:I

    .line 76
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_storyOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_STORY_ON_CLICK_LISTENER:I

    .line 78
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_genomeEntity:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY:I

    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_entityOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_ON_CLICK_LISTENER:I

    .line 82
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_entityIsSubscribed:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_IS_SUBSCRIBED:I

    .line 84
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_entityId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_ID:I

    .line 86
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_isVideo:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_VIDEO:I

    .line 88
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_numAudio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    .line 90
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_audioPostId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_AUDIO_POST_ID:I

    .line 92
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_audioOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_AUDIO_ON_CLICK_LISTENER:I

    .line 94
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_isRead:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_READ:I

    .line 96
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_hasGallery:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_HAS_GALLERY:I

    .line 98
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_galleryOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_GALLERY_ON_CLICK_LISTENER:I

    .line 100
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_isFullImage:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_FULL_IMAGE:I

    .line 102
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_showLock:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SHOW_LOCK:I

    .line 104
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_TRANSITION_NAME:I

    .line 106
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_cardBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_CARD_BACKGROUND:I

    .line 108
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_headlineTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_HEADLINE_TEXT_COLOR:I

    .line 110
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_sourceTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_TEXT_COLOR:I

    .line 112
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardNewsItem_timeTextColor:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME_TEXT_COLOR:I

    .line 114
    const/4 v0, 0x4

    new-array v0, v0, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_READ:I

    aput v1, v0, v3

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_VERSION:I

    aput v1, v0, v4

    const/4 v1, 0x2

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_IS_SUBSCRIBED:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_IS_SUBSCRIBED:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->EQUALITY_FIELDS:[I

    .line 121
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS:[I

    .line 122
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_compact:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_COMPACT:[I

    .line 123
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_NO_IMAGE:[I

    .line 124
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_no_image_compact:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_NO_IMAGE_COMPACT:[I

    .line 125
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE:[I

    .line 126
    new-array v0, v4, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image_compact:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE_COMPACT:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 138
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 139
    return-void
.end method

.method public static addTheming(Lcom/google/android/libraries/bind/data/Data;Z)V
    .locals 4
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "useDarkTheme"    # Z

    .prologue
    const v3, 0x106000b

    .line 378
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 379
    .local v0, "res":Landroid/content/res/Resources;
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_CARD_BACKGROUND:I

    if-eqz p1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_background_dark:I

    .line 380
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 379
    invoke-virtual {p0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 381
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_HEADLINE_TEXT_COLOR:I

    if-eqz p1, :cond_1

    .line 382
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 381
    :goto_1
    invoke-virtual {p0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 383
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_TEXT_COLOR:I

    if-eqz p1, :cond_2

    .line 384
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 383
    :goto_2
    invoke-virtual {p0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 385
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME_TEXT_COLOR:I

    if-eqz p1, :cond_3

    .line 386
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 385
    :goto_3
    invoke-virtual {p0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 387
    return-void

    .line 379
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_background:I

    goto :goto_0

    .line 382
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    .line 384
    :cond_2
    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_source_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_2

    .line 386
    :cond_3
    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->card_time_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_3
.end method

.method public static fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p4, "isPurchased"    # Z
    .param p5, "includeSourceData"    # Z
    .param p6, "isFullLayout"    # Z
    .param p7, "useDarkTheme"    # Z
    .param p8, "analyticsScreenName"    # Ljava/lang/String;
    .param p9, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 233
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_VERSION:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 234
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_TRANSITION_NAME:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 236
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 237
    .local v7, "postId":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v4

    .line 238
    .local v4, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->isRead(Ljava/lang/String;)Z

    move-result v12

    .line 239
    .local v12, "isRead":Z
    if-nez v12, :cond_0

    if-nez p4, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getIsMetered()Z

    move-result v3

    if-nez v3, :cond_7

    :cond_0
    const/4 v8, 0x1

    .line 240
    .local v8, "allowShortcuts":Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSectionType()I

    move-result v3

    const/4 v5, 0x6

    if-eq v3, v5, :cond_1

    .line 241
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFormTemplateId()Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "video_default"

    .line 242
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFormTemplateId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_1
    const/4 v13, 0x1

    .line 243
    .local v13, "isVideo":Z
    :goto_1
    if-nez p4, :cond_9

    .line 244
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getIsMetered()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 245
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasMeteredPolicyType()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 246
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getMeteredPolicyType()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_9

    const/4 v14, 0x1

    .line 247
    .local v14, "showLock":Z
    :goto_2
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;

    move-object/from16 v3, p2

    move-object/from16 v5, p1

    move-object/from16 v6, p9

    invoke-direct/range {v2 .. v7}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    .line 270
    .local v2, "readArticleClickListener":Landroid/view/View$OnClickListener;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TITLE:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 271
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_READ:I

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 272
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SHOW_LOCK:I

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 273
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 274
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v10

    .line 275
    .local v10, "imageHeight":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v11

    .line 276
    .local v11, "imageWidth":I
    int-to-float v3, v10

    int-to-float v5, v11

    div-float v9, v3, v5

    .line 277
    .local v9, "aspectRatio":F
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 278
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_HEIGHT:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 279
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_WIDTH:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 280
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_HEIGHT_TO_WIDTH_RATIO:I

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 281
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hasAttribution()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 282
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ATTRIBUTION:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttribution()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 284
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasCanUseImagesAsHeaderBackground()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 285
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ELIGIBLE_FOR_HEADER:I

    .line 286
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getCanUseImagesAsHeaderBackground()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 285
    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 290
    .end local v9    # "aspectRatio":F
    .end local v10    # "imageHeight":I
    .end local v11    # "imageWidth":I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getExternalCreated()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->relativePastTimeString(J)Ljava/lang/String;

    move-result-object v16

    .line 291
    .local v16, "timeText":Ljava/lang/String;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME:I

    move-object/from16 v0, p9

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 292
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ABSTRACT:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAbstract()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 293
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_STORY_ON_CLICK_LISTENER:I

    if-eqz v13, :cond_a

    if-eqz v8, :cond_a

    new-instance v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v4, v1, v7}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Ljava/lang/String;)V

    :goto_3
    move-object/from16 v0, p9

    invoke-virtual {v0, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 308
    if-eqz p5, :cond_b

    .line 309
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSourceIconId()Ljava/lang/String;

    move-result-object v15

    .line 310
    .local v15, "sourceIconId":Ljava/lang/String;
    invoke-static {v15}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFavicon()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 313
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFavicon()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v15

    .line 316
    :cond_4
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ICON_ID:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v15}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 317
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 318
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;

    move-object/from16 v0, p8

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v4, v1}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$5;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 341
    .end local v15    # "sourceIconId":Ljava/lang/String;
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAudioItemCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 342
    sget v3, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 343
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_NUM_AUDIO:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAudioItemCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 344
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_AUDIO_ON_CLICK_LISTENER:I

    if-eqz v8, :cond_c

    new-instance v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v7, v1, v4}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$6;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    :goto_5
    move-object/from16 v0, p9

    invoke-virtual {v0, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 353
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_AUDIO_POST_ID:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 355
    :cond_5
    if-eqz v13, :cond_d

    .line 356
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_VIDEO:I

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 373
    .end local v2    # "readArticleClickListener":Landroid/view/View$OnClickListener;
    :cond_6
    :goto_6
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_FULL_IMAGE:I

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 374
    move-object/from16 v0, p9

    move/from16 v1, p7

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->addTheming(Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 375
    return-void

    .line 239
    .end local v8    # "allowShortcuts":Z
    .end local v13    # "isVideo":Z
    .end local v14    # "showLock":Z
    .end local v16    # "timeText":Ljava/lang/String;
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 242
    .restart local v8    # "allowShortcuts":Z
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 246
    .restart local v13    # "isVideo":Z
    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_2

    .restart local v2    # "readArticleClickListener":Landroid/view/View$OnClickListener;
    .restart local v14    # "showLock":Z
    .restart local v16    # "timeText":Ljava/lang/String;
    :cond_a
    move-object v3, v2

    .line 293
    goto/16 :goto_3

    .line 338
    :cond_b
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_STORY_ON_CLICK_LISTENER:I

    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ON_CLICK_LISTENER:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    goto :goto_4

    :cond_c
    move-object v3, v2

    .line 344
    goto :goto_5

    .line 357
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFormTemplateId()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "photo_gallery"

    .line 358
    invoke-virtual/range {p1 .. p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFormTemplateId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 359
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_HAS_GALLERY:I

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 360
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_GALLERY_ON_CLICK_LISTENER:I

    if-eqz v8, :cond_e

    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;

    .end local v2    # "readArticleClickListener":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p2

    invoke-direct {v2, v7, v0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    :cond_e
    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_6
.end method

.method public static getAppropriateCardLayout(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)I
    .locals 4
    .param p0, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v3, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isImageLowResForColumn(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_NO_IMAGE:[I

    aget v1, v1, v3

    .line 220
    :goto_0
    return v1

    .line 210
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFormTemplateId()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 211
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFormTemplateId()Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "templateId":Ljava/lang/String;
    const-string v1, "photo_default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "photo_gallery"

    .line 213
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    :cond_2
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE:[I

    aget v1, v1, v3

    goto :goto_0

    .line 217
    .end local v0    # "templateId":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSectionType()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 218
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE:[I

    aget v1, v1, v3

    goto :goto_0

    .line 220
    :cond_4
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS:[I

    aget v1, v1, v3

    goto :goto_0
.end method

.method public static isFullImageLayout(I)Z
    .locals 4
    .param p0, "layout"    # I

    .prologue
    const/4 v3, 0x0

    .line 201
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE:[I

    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_FULL_IMAGE_COMPACT:[I

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->onFinishInflate()V

    .line 144
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->gradient_text_container:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 145
    .local v0, "textContainer":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    :cond_0
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 10
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v9, 0x0

    .line 153
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 155
    if-nez p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    sget v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IS_FULL_IMAGE:I

    invoke-virtual {p1, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 160
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->gradient_text_container:I

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 161
    .local v6, "textContainer":Landroid/view/View;
    if-eqz v6, :cond_2

    .line 162
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 163
    .local v3, "imageView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    new-instance v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$1;

    invoke-direct {v8, p0, v6}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$1;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;Landroid/view/View;)V

    invoke-virtual {v3, v8}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenImageSet(Ljava/lang/Runnable;)V

    .line 169
    new-instance v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$2;

    invoke-direct {v8, p0, v6}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$2;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;Landroid/view/View;)V

    invoke-virtual {v3, v8}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenBitmapReleased(Ljava/lang/Runnable;)V

    .line 178
    .end local v3    # "imageView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .end local v6    # "textContainer":Landroid/view/View;
    :cond_2
    sget v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_ID:I

    invoke-virtual {p1, v8}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 179
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->genome_text:I

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 180
    .local v2, "genomeTag":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 181
    invoke-virtual {v2}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/LayerDrawable;

    .line 182
    .local v4, "tagBackground":Landroid/graphics/drawable/LayerDrawable;
    if-eqz v4, :cond_0

    .line 183
    sget v8, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_IS_SUBSCRIBED:I

    invoke-virtual {p1, v8, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v7

    .line 184
    .local v7, "useColoredBackground":Z
    if-eqz v7, :cond_3

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_SUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    .line 187
    .local v5, "tagColor":Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    :goto_1
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->color_layer:I

    .line 188
    invoke-virtual {v4, v8}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 189
    .local v1, "baseLayer":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget v9, v5, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->colorResId:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 190
    sget v8, Lcom/google/android/apps/newsstanddev/R$id;->accent_layer:I

    .line 191
    invoke-virtual {v4, v8}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 192
    .local v0, "accentLayer":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget v9, v5, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->accentColorResId:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 194
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget v9, v5, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->textColorResId:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 193
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_0

    .line 184
    .end local v0    # "accentLayer":Landroid/graphics/drawable/GradientDrawable;
    .end local v1    # "baseLayer":Landroid/graphics/drawable/GradientDrawable;
    .end local v5    # "tagColor":Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;
    :cond_3
    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;->DEFAULT_UNSUBSCRIBED:Lcom/google/apps/dots/android/newsstand/util/ColorHelper$TopicColor;

    goto :goto_1
.end method
