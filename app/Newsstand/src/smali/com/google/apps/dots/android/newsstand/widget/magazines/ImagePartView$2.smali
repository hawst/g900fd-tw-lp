.class final Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;
.super Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;
.source "ImagePartView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->loadBitmap(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/util/RetryWithGC",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field final synthetic val$options:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->val$options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/util/RetryWithGC;-><init>()V

    return-void
.end method


# virtual methods
.method protected work()Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 170
    const/4 v5, 0x0

    .line 171
    .local v5, "result":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 173
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->val$blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v6}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v6

    long-to-int v3, v6

    .line 177
    .local v3, "length":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->bytePool()Lcom/google/apps/dots/android/newsstand/util/BytePool;

    move-result-object v4

    .line 178
    .local v4, "pool":Lcom/google/apps/dots/android/newsstand/util/BytePool;
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->acquire(I)[B

    move-result-object v1

    .line 179
    .local v1, "buffer":[B
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v1, v7, v3}, Lcom/google/common/io/ByteStreams;->readFully(Ljava/io/InputStream;[BII)V

    .line 180
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->val$options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v6, v3, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 181
    invoke-virtual {v4, v1}, Lcom/google/apps/dots/android/newsstand/util/BytePool;->release([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    .line 187
    .end local v1    # "buffer":[B
    .end local v3    # "length":I
    .end local v4    # "pool":Lcom/google/apps/dots/android/newsstand/util/BytePool;
    :goto_0
    return-object v5

    .line 182
    :catch_0
    move-exception v2

    .line 183
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v6

    const-string v7, "Exception thrown decoding bitmap."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v2, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    throw v6
.end method

.method protected bridge synthetic work()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->work()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
