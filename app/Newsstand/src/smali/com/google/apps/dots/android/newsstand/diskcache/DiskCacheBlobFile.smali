.class public Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;
.super Ljava/lang/Object;
.source "DiskCacheBlobFile.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;


# instance fields
.field private final diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field private final key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V
    .locals 0
    .param p1, "diskCache"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .param p2, "key"    # Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 22
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    .line 23
    return-void
.end method


# virtual methods
.method public exists()Z
    .locals 3

    .prologue
    .line 28
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->contains(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 30
    :goto_0
    return v1

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "ioe":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMetadata()Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    move-result-object v0

    return-object v0
.end method

.method public key()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    return-object v0
.end method

.method public makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getAssetFileDescriptor(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getDiskBlob(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;

    move-result-object v0

    return-object v0
.end method

.method public makeInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->getAssetFileDescriptor(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    return-object v0
.end method

.method public pin(II)V
    .locals 2
    .param p1, "pinId"    # I
    .param p2, "snapshotId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->pin(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;II)V

    .line 51
    return-void
.end method

.method public setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V

    .line 86
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DiskCacheBlobFile["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public touch()Z
    .locals 3

    .prologue
    .line 37
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->touch(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 39
    :goto_0
    return v1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "ioe":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeStream(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;->key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->writeStream(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Ljava/io/InputStream;)V

    .line 61
    return-void
.end method
