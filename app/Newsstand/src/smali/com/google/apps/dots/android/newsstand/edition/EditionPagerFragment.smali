.class public abstract Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<TS;>;"
    }
.end annotation


# static fields
.field protected static final DK_PAGER_TITLE:I


# instance fields
.field protected backgroundView:Landroid/view/View;

.field protected headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

.field protected headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected heroView:Landroid/view/View;

.field private pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

.field protected pageList:Lcom/google/android/libraries/bind/data/DataList;

.field private pageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field protected pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field protected pager:Landroid/support/v4/view/NSViewPager;

.field protected pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

.field protected final pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->EditionPagerFragment_pagerTitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->DK_PAGER_TITLE:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Ljava/lang/String;)V
    .locals 1
    .param p2, "stateExtraKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    .local p1, "defaultState":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_fragment:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "stateExtraKey"    # Ljava/lang/String;

    .prologue
    .line 82
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupPager()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->onPulledToRefresh()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->isChangingState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->isChangingState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateImageRotator()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
    .param p1, "x1"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method

.method private currentCardListView()Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    .locals 1

    .prologue
    .line 420
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getCurrentListView()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    return-object v0
.end method

.method private onPulledToRefresh()V
    .locals 5

    .prologue
    .line 217
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 129
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 175
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 176
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 175
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V

    .line 204
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnLayoutChangedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;)V

    .line 214
    return-void
.end method

.method private setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 2
    .param p1, "imageRotatorList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 482
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setImageIdList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 483
    if-eqz p1, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    .line 485
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setVisibility(I)V

    .line 490
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 488
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupPager()V
    .locals 3

    .prologue
    .line 256
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;

    .line 257
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 277
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    sget v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->DK_PAGER_TITLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setTitleKey(Ljava/lang/Integer;)V

    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$6;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setErrorMessageDataProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 287
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/support/v4/view/NSViewPager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    .line 306
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 308
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    .line 321
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V

    .line 324
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 348
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 349
    return-void
.end method

.method private updateImageRotator()V
    .locals 4

    .prologue
    .line 466
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 467
    .local v0, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-nez v0, :cond_0

    .line 479
    :goto_0
    return-void

    .line 470
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$11;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private updatePullToRefresh()V
    .locals 0

    .prologue
    .line 236
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 120
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_background:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->backgroundView:Landroid/view/View;

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->backgroundView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->header_image_rotator:I

    .line 122
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    .line 124
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->image_rotator_overlay_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 123
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->backgroundView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 126
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 115
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_hero:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->heroView:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->heroView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 117
    return-void
.end method

.method protected alwaysShowActionBarText()Z
    .locals 1

    .prologue
    .line 243
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 541
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$12;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    return-object v0
.end method

.method protected abstract currentPageIndex()I
.end method

.method protected abstract dataListId(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Object;
.end method

.method protected getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    .line 356
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 357
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getRetryRunnable()Ljava/lang/Runnable;

    move-result-object v3

    .line 356
    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 87
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 88
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 89
    .local v1, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-string v2, "help_context_key"

    const-string v3, "mobile_news_topic"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    if-eqz v1, :cond_0

    .line 91
    const-string v2, "editionInfo"

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    return-object v0
.end method

.method protected getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 365
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$10;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$10;-><init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    return-object v0
.end method

.method public handleRemapSharedElements(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/Map;)V
    .locals 8
    .param p1, "listView"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 517
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getUpcomingState()Landroid/os/Bundle;

    move-result-object v1

    .line 518
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 519
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 520
    const-string v4, "EditionPagerFragment_upcoming_post"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 521
    .local v3, "postId":Ljava/lang/String;
    const-class v4, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    sget v7, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_TRANSITION_NAME:I

    aput v7, v5, v6

    invoke-static {p1, v4, v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getFirstViewWithProperty(Landroid/view/ViewGroup;Ljava/lang/Class;Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/BindingViewGroup;

    move-result-object v2

    .line 526
    .local v2, "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    if-eqz v2, :cond_2

    .line 527
    instance-of v4, v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    if-eqz v4, :cond_1

    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    move-object v0, v2

    .line 530
    .local v0, "card":Landroid/view/View;
    :goto_0
    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->reading_activity_hero:I

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    .end local v0    # "card":Landroid/view/View;
    .end local v3    # "postId":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 527
    .restart local v2    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .restart local v3    # "postId":Ljava/lang/String;
    :cond_1
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    const-class v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->card:I

    .line 529
    invoke-static {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 533
    .restart local v2    # "match":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 534
    invoke-interface {p3}, Ljava/util/Map;->clear()V

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 375
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 376
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 377
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->destroy()V

    .line 378
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 379
    return-void
.end method

.method protected onPageListChanged()V
    .locals 0

    .prologue
    .line 353
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onPause()V

    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 106
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 110
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onResume()V

    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    .line 112
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 98
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 99
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupHeaderListLayout()V

    .line 100
    return-void
.end method

.method protected pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 382
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method protected pageEdition(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 3
    .param p1, "position"    # Ljava/lang/Integer;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v2, 0x0

    .line 386
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_4

    .line 387
    :cond_0
    if-nez p1, :cond_1

    .line 388
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v1

    if-nez v1, :cond_3

    .line 391
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    move-result-object v0

    .line 392
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    if-nez v0, :cond_2

    move-object v1, v2

    .line 396
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    :goto_0
    return-object v1

    .line 392
    .restart local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    :cond_2
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0

    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    :cond_3
    move-object v1, v2

    .line 394
    goto :goto_0

    .line 396
    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method

.method protected abstract pageFragment(I)Landroid/support/v4/app/Fragment;
.end method

.method protected abstract pageList()Lcom/google/android/libraries/bind/data/DataList;
.end method

.method protected refreshCurrentEditionWithSpinner()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v2, 0x1

    .line 501
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 502
    .local v0, "pageEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsRefresh()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 505
    :cond_0
    return-void
.end method

.method protected showTabs()Z
    .locals 1

    .prologue
    .line 251
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract stateFromPosition(I)Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TS;"
        }
    .end annotation
.end method

.method protected toggleCompactMode()Z
    .locals 2

    .prologue
    .line 493
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->currentCardListView()Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v0

    .line 494
    .local v0, "listView":Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->blendAnimationOnNextInvalidation()V

    .line 497
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->toggleCompactMode()Z

    move-result v1

    return v1
.end method

.method protected updateErrorView()V
    .locals 1

    .prologue
    .line 361
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->refreshErrorView()Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 362
    return-void
.end method

.method protected updatePage()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    const/4 v1, 0x1

    .line 453
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    :goto_0
    return-void

    .line 457
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->currentPageIndex()I

    move-result v0

    .line 458
    .local v0, "currentPageIndex":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 459
    const/4 v0, 0x0

    .line 461
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v2

    sub-int v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 462
    .local v1, "smoothScroll":Z
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/view/NSViewPager;->setCurrentLogicalItem(IZ)V

    goto :goto_0

    .line 461
    .end local v1    # "smoothScroll":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected updatePageList()V
    .locals 2

    .prologue
    .line 443
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 446
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 447
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 448
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 449
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateErrorView()V

    .line 450
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 54
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TS;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment<TS;>;"
    .local p1, "newState":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    .local p2, "oldState":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;, "TS;"
    const/4 v1, 0x0

    .line 426
    if-nez p2, :cond_0

    .line 427
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updatePageList()V

    .line 429
    :cond_0
    if-eqz p2, :cond_1

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragmentState;->pageEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 430
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 431
    .local v0, "pageEditionChanged":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 432
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "updateViews: pageEditionChanged"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 433
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pageScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 434
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updatePage()V

    .line 435
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updateImageRotator()V

    .line 436
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->updatePullToRefresh()V

    .line 438
    :cond_2
    return-void

    .end local v0    # "pageEditionChanged":Z
    :cond_3
    move v0, v1

    .line 430
    goto :goto_0
.end method
