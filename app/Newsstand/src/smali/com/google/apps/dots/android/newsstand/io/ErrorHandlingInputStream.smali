.class public Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;
.super Ljava/io/InputStream;
.source "ErrorHandlingInputStream.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final delegate:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "delegate"    # Ljava/io/InputStream;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    .line 25
    return-void
.end method

.method private closeQuietly()V
    .locals 1

    .prologue
    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private rethrowIfDisabled(Ljava/io/IOException;)V
    .locals 0
    .param p1, "e"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    return-void
.end method


# virtual methods
.method public available()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 61
    :goto_0
    return v1

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 59
    sget-object v2, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Error checking data length: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    goto :goto_0
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 71
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error closing input stream: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public mark(I)V
    .locals 1
    .param p1, "readlimit"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 79
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 49
    :goto_0
    return v1

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 47
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error reading data: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    .line 49
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public read([B)I
    .locals 5
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 106
    :goto_0
    return v1

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 104
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error reading data: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    .line 106
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "byteOffset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 94
    :goto_0
    return v1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 92
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error reading data: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    .line 94
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 116
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error resetting input stream: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    goto :goto_0
.end method

.method public skip(J)J
    .locals 5
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 130
    :goto_0
    return-wide v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->rethrowIfDisabled(Ljava/io/IOException;)V

    .line 128
    sget-object v1, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Error skipping data: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/io/ErrorHandlingInputStream;->closeQuietly()V

    .line 130
    const-wide/16 v2, 0x0

    goto :goto_0
.end method
