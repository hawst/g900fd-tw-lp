.class Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;
.super Lcom/google/android/play/IUserContentService$Stub;
.source "MagazinesUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MagazinesUserContentBinder"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/google/android/play/IUserContentService$Stub;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->mContext:Landroid/content/Context;

    .line 163
    return-void
.end method

.method private getFinskyDocId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 184
    const-string v0, "magazine-issue-"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getOfferIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 193
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "newsstand://play.google.com/offers/"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1

    .line 193
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getViewIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 188
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "newsstand://play.google.com/magazines/reader/issue/"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1

    .line 188
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getWhatsNext(I)Ljava/util/List;
    .locals 13
    .param p1, "numItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->lock:Ljava/lang/Object;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$000()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 203
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v7, "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesClearer:Lcom/google/android/libraries/bind/async/DelayedRunnable;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$300()Lcom/google/android/libraries/bind/async/DelayedRunnable;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 206
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$400()Lcom/google/common/collect/TreeMultimap;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 207
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->finalMagazinesData:Lcom/google/common/collect/TreeMultimap;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$400()Lcom/google/common/collect/TreeMultimap;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/common/collect/TreeMultimap;->entries()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 208
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 210
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;

    iget-object v0, v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->appId:Ljava/lang/String;

    .line 211
    .local v0, "appId":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;

    iget-object v2, v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->coverAttachmentId:Ljava/lang/String;

    .line 212
    .local v2, "coverAttachmentId":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;

    iget-object v6, v8, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;->offerId:Ljava/lang/String;

    .line 213
    .local v6, "offerId":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 215
    .local v4, "lastUpdate":J
    const-string v8, "Play.FinskyDocId"

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->getFinskyDocId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v8, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 220
    const-string v8, "Play.ViewIntent"

    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->getOfferIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v1, v8, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 221
    const-string v8, "Play.Reason"

    .line 222
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v11

    sget v12, Lcom/google/android/apps/newsstanddev/R$string;->offer_widget_reason:I

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 221
    invoke-virtual {v1, v8, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :goto_0
    const-string v8, "Play.ImageUri"

    .line 231
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/provider/NSContentUris;->getAttachmentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 230
    invoke-virtual {v1, v8, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 233
    const-string v8, "Play.LastUpdateTimeMillis"

    invoke-virtual {v1, v8, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 235
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-ge v8, p1, :cond_1

    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 244
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "coverAttachmentId":Ljava/lang/String;
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    .end local v4    # "lastUpdate":J
    .end local v6    # "offerId":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->clearFinalMagazinesData(Z)V
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$100(Z)V

    .line 263
    monitor-exit v9

    .end local v7    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :goto_1
    return-object v7

    .line 224
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "coverAttachmentId":Ljava/lang/String;
    .restart local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    .restart local v4    # "lastUpdate":J
    .restart local v6    # "offerId":Ljava/lang/String;
    .restart local v7    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_2
    const-string v8, "Play.ViewIntent"

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->getViewIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v1, v8, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 264
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "coverAttachmentId":Ljava/lang/String;
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazineWidgetInfo;>;"
    .end local v4    # "lastUpdate":J
    .end local v6    # "offerId":Ljava/lang/String;
    .end local v7    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 246
    .restart local v7    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_3
    :try_start_1
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v8

    const-string v10, "No magazines data available yet; we\'ll fetch it, and tell you when it\'s ready"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v8, v10, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    const/4 v8, 0x1

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->clearFinalMagazinesData(Z)V
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$100(Z)V

    .line 256
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->mContext:Landroid/content/Context;

    # invokes: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->fetchMagazinesForWidget(Landroid/content/Context;)V
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$500(Landroid/content/Context;)V

    .line 260
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .end local v7    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public getDocuments(II)Ljava/util/List;
    .locals 6
    .param p1, "dataTypeToFetch"    # I
    .param p2, "numItemsToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 167
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/SecurityUtil;->checkCallerSignature(Landroid/content/pm/PackageManager;I)V

    .line 169
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    .line 170
    .local v0, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    .line 171
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "No account selected for Newsstand, therefore no magazines to return"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 179
    :goto_0
    return-object v1

    .line 174
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 178
    # getter for: Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "Unknown dataTypeToFetch: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    .line 176
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$MagazinesUserContentBinder;->getWhatsNext(I)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
