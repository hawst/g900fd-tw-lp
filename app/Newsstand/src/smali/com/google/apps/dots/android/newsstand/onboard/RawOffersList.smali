.class public Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "RawOffersList.java"


# static fields
.field public static final DK_OFFER_ID:I

.field public static final DK_OFFER_SUMMARY:I

.field public static final DK_STORE_TYPE:I

.field public static final EQUALITY_FIELDS:[I


# instance fields
.field private freeMagazineOffersList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RawOffersList_offerId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RawOffersList_offerSummary:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_SUMMARY:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->RawOffersList_storeType:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_STORE_TYPE:I

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 43
    return-void
.end method

.method public static isFreeMagazineOffer(Lcom/google/android/libraries/bind/data/Data;)Z
    .locals 6
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 88
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_STORE_TYPE:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 89
    .local v2, "storeType":I
    if-eq v2, v3, :cond_0

    .line 94
    :goto_0
    return v4

    .line 92
    :cond_0
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_SUMMARY:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 93
    .local v0, "offerSummary":Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getType()I

    move-result v1

    .line 94
    .local v1, "offerType":I
    const/4 v5, 0x3

    if-ne v1, v5, :cond_1

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method


# virtual methods
.method public freeMagazineOffersList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 72
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->EQUALITY_FIELDS:[I

    sget v1, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->DK_OFFER_ID:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$2;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v2, p0, v3}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$2;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;Ljava/util/concurrent/Executor;)V

    .line 73
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList:Lcom/google/android/libraries/bind/data/DataList;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->freeMagazineOffersList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method protected onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 47
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMyOffers(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 2
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;->primaryKey()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/RawOffersList;I)V

    .line 63
    .local v0, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 64
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->getResults()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
