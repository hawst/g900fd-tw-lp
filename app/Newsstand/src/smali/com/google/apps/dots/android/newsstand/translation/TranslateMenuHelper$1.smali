.class Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;
.super Ljava/lang/Object;
.source "TranslateMenuHelper.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field final synthetic val$optPostId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$optPostId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->this$0:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper$1;->val$optPostId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->untranslate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x1

    return v0
.end method
