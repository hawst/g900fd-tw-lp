.class public Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;
.super Ljava/lang/Object;
.source "DecodeOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private bitmapConfig:Landroid/graphics/Bitmap$Config;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 49
    return-void
.end method


# virtual methods
.method public bitmapConfig(Landroid/graphics/Bitmap$Config;)Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;
    .locals 0
    .param p1, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 58
    return-object p0
.end method

.method public build()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;-><init>(Landroid/graphics/Bitmap$Config;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->build()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
