.class public Lcom/google/apps/dots/android/newsstand/card/ActionMessage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ActionMessage.java"


# static fields
.field public static final DK_ACTION_ON_CLICK_LISTENER:I

.field public static final DK_ACTION_TEXT:I

.field public static final DK_ICON_DRAWABLE:I

.field public static final DK_MESSAGE_TEXT:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TOP_PADDING:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_topPadding:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_TOP_PADDING:I

    .line 42
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ON_CLICK_LISTENER:I

    .line 45
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_iconDrawable:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    .line 48
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_messageText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    .line 51
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_actionText:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_TEXT:I

    .line 54
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ActionMessage_actionOnClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_ON_CLICK_LISTENER:I

    .line 56
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->EQUALITY_FIELDS:[I

    .line 58
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->action_message:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method protected static addBaseCardData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 272
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 273
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->EQUALITY_FIELDS:[I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 274
    return-void
.end method

.method public static addHeaderPadding(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 280
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_TOP_PADDING:I

    invoke-virtual {p2, p1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 281
    return-object p0
.end method

.method protected static addRetryIfPresent(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "optRetryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 170
    if-eqz p2, :cond_0

    .line 171
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_TEXT:I

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->retry:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 172
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_ON_CLICK_LISTENER:I

    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage$1;

    invoke-direct {v1, p2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage$1;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 179
    :cond_0
    return-void
.end method

.method public static addStandardEmptyCardData(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p0, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iconResId"    # I
    .param p3, "messageTextId"    # I

    .prologue
    .line 290
    if-eqz p2, :cond_0

    .line 291
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 293
    :cond_0
    if-eqz p3, :cond_1

    .line 294
    sget v0, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 296
    :cond_1
    return-object p0
.end method

.method protected static extractCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 310
    instance-of v0, p0, Lcom/google/android/libraries/bind/data/DataException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/util/concurrent/ExecutionException;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    .line 311
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->getResponseStatus()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 314
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->extractCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    .line 317
    .end local p0    # "t":Ljava/lang/Throwable;
    :cond_1
    return-object p0
.end method

.method protected static getGenericErrorConfiguration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "errorMsg"    # Ljava/lang/String;
    .param p2, "retryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 135
    .local v0, "messageData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_error:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 136
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 137
    invoke-static {v0, p0, p2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addRetryIfPresent(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 138
    return-object v0
.end method

.method protected static getNotPinnedErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "retryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 161
    .local v0, "messageData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 162
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->content_error_offline_downloadable_not_downloaded:I

    .line 163
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 164
    invoke-static {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addRetryIfPresent(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 165
    return-object v0
.end method

.method protected static getNotSyncedErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "retryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 147
    .local v0, "messageData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_download:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 148
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->content_error_domode_downloaded_not_synced:I

    .line 149
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 150
    invoke-static {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addRetryIfPresent(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 151
    return-object v0
.end method

.method protected static getOfflineErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "retryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 116
    .local v0, "messageData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_error:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 117
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->content_error_offline:I

    .line 118
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 119
    invoke-static {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addRetryIfPresent(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 120
    return-object v0
.end method

.method public static getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "optEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "t"    # Ljava/lang/Throwable;
    .param p3, "optRetryRunnable"    # Ljava/lang/Runnable;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 188
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 190
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->extractCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    .line 192
    .local v1, "cause":Ljava/lang/Throwable;
    instance-of v11, p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v11, :cond_0

    .line 193
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p1    # "optEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object p1

    .line 196
    .restart local p1    # "optEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v11

    if-nez v11, :cond_5

    move v4, v10

    .line 197
    .local v4, "isDeviceOffline":Z
    :goto_0
    if-nez v4, :cond_1

    instance-of v11, v1, Lcom/google/apps/dots/android/newsstand/sync/OfflineSyncException;

    if-eqz v11, :cond_2

    :cond_1
    move v5, v10

    .line 198
    .local v5, "isOfflineError":Z
    :cond_2
    const/4 v6, 0x0

    .line 199
    .local v6, "isPinnable":Z
    const/4 v7, 0x0

    .line 204
    .local v7, "isPinned":Z
    if-nez v5, :cond_9

    .line 205
    const/4 v3, 0x0

    .line 207
    .local v3, "errorMessage":Ljava/lang/String;
    instance-of v9, v1, Lcom/google/apps/dots/android/newsstand/exception/LayoutEngineException;

    if-eqz v9, :cond_6

    .line 208
    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->content_error_layout_engine:I

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 219
    .end local v1    # "cause":Ljava/lang/Throwable;
    :cond_3
    :goto_1
    if-nez v3, :cond_4

    .line 220
    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->content_error_generic:I

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 222
    :cond_4
    invoke-static {p0, v3, p3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getGenericErrorConfiguration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    .line 252
    .end local v3    # "errorMessage":Ljava/lang/String;
    .end local p3    # "optRetryRunnable":Ljava/lang/Runnable;
    :goto_2
    return-object v9

    .end local v4    # "isDeviceOffline":Z
    .end local v5    # "isOfflineError":Z
    .end local v6    # "isPinnable":Z
    .end local v7    # "isPinned":Z
    .restart local v1    # "cause":Ljava/lang/Throwable;
    .restart local p3    # "optRetryRunnable":Ljava/lang/Runnable;
    :cond_5
    move v4, v5

    .line 196
    goto :goto_0

    .line 209
    .restart local v3    # "errorMessage":Ljava/lang/String;
    .restart local v4    # "isDeviceOffline":Z
    .restart local v5    # "isOfflineError":Z
    .restart local v6    # "isPinnable":Z
    .restart local v7    # "isPinned":Z
    :cond_6
    instance-of v9, v1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    if-eqz v9, :cond_7

    .line 210
    check-cast v1, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;

    .end local v1    # "cause":Ljava/lang/Throwable;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/sync/HttpSyncException;->getResponseStatusOrZero()I

    move-result v2

    .line 211
    .local v2, "errorCode":I
    div-int/lit8 v9, v2, 0x64

    const/4 v10, 0x5

    if-ne v9, v10, :cond_3

    .line 212
    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->content_error_server:I

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 214
    .end local v2    # "errorCode":I
    .restart local v1    # "cause":Ljava/lang/Throwable;
    :cond_7
    instance-of v9, v1, Ljava/net/SocketException;

    if-nez v9, :cond_8

    instance-of v9, v1, Ljava/io/InterruptedIOException;

    if-eqz v9, :cond_3

    .line 216
    :cond_8
    sget v9, Lcom/google/android/apps/newsstanddev/R$string;->content_error_network:I

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 225
    .end local v3    # "errorMessage":Ljava/lang/String;
    :cond_9
    if-eqz p1, :cond_c

    .line 226
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .line 227
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinnable(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v6

    .line 230
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v8

    .line 231
    .local v8, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v10

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v10, v11, :cond_a

    .line 232
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v10

    sget-object v11, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v10, v11, :cond_b

    .line 233
    :cond_a
    invoke-virtual {v8, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v10

    and-int/2addr v6, v10

    .line 235
    :cond_b
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v10

    invoke-virtual {v10, v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->isPinned(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v7

    .line 240
    .end local v8    # "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    :cond_c
    if-nez v7, :cond_d

    instance-of v10, p1, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    if-eqz v10, :cond_f

    .line 241
    :cond_d
    if-eqz v4, :cond_e

    move-object p3, v9

    .end local p3    # "optRetryRunnable":Ljava/lang/Runnable;
    :cond_e
    invoke-static {p0, p3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getNotSyncedErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    goto :goto_2

    .line 246
    .restart local p3    # "optRetryRunnable":Ljava/lang/Runnable;
    :cond_f
    if-eqz v6, :cond_11

    if-nez v7, :cond_11

    .line 247
    if-eqz v4, :cond_10

    :goto_3
    invoke-static {p0, v9}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getNotPinnedErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    goto :goto_2

    :cond_10
    move-object v9, p3

    goto :goto_3

    .line 252
    :cond_11
    if-eqz v4, :cond_12

    :goto_4
    invoke-static {p0, v9}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getOfflineErrorConfiguration(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    goto :goto_2

    :cond_12
    move-object v9, p3

    goto :goto_4
.end method

.method public static makeSpecificErrorCardData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "optEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "t"    # Ljava/lang/Throwable;
    .param p3, "optRetryRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 261
    invoke-static {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 262
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    sget v2, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->LAYOUT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addBaseCardData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 265
    :cond_0
    return-object v0
.end method

.method public static makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "iconResId"    # I
    .param p2, "messageTextId"    # I

    .prologue
    .line 304
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 305
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addBaseCardData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 306
    invoke-static {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->addStandardEmptyCardData(Lcom/google/android/libraries/bind/data/Data;Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public configure(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "messageData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 107
    return-void
.end method

.method public configure(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;ILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    .param p2, "iconResId"    # I
    .param p3, "messageText"    # Ljava/lang/String;
    .param p4, "actionText"    # Ljava/lang/String;
    .param p5, "actionOnClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 88
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ICON_DRAWABLE:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 89
    if-eqz p3, :cond_0

    .line 90
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_MESSAGE_TEXT:I

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    :cond_0
    if-eqz p4, :cond_1

    .line 93
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_TEXT:I

    invoke-virtual {v0, v1, p4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 95
    :cond_1
    if-eqz p5, :cond_2

    .line 96
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_ACTION_ON_CLICK_LISTENER:I

    invoke-virtual {v0, v1, p5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 98
    :cond_2
    if-eqz p1, :cond_3

    .line 99
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->DK_TOP_PADDING:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 101
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 102
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getPaddingTop()I

    move-result v1

    if-lez v1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 79
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 80
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void
.end method
