.class public Lcom/google/apps/dots/android/newsstand/home/HomeActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;",
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private delayHandlingIntentExtras:Z

.field private homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>(Z)V

    .line 31
    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->handleExtras(Landroid/os/Bundle;)Z

    .line 116
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->setActiveHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)V

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 73
    const/16 v0, 0xc

    return v0
.end method

.method protected getDrawerIndicatorEnabled()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    return-object v0
.end method

.method public onActivityReenter(ILandroid/content/Intent;)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 122
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onActivityReenter(ILandroid/content/Intent;)V

    .line 123
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->handleUpcomingResult(Landroid/os/Bundle;)Z

    .line 126
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->handleOnBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$color;->home_background:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 37
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->home_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->setContentView(I)V

    .line 42
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->home_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->setEventHandler(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment$EventHandler;)V

    .line 46
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->homeFragment:Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->setActiveHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)V

    .line 48
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 58
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    sget v1, Lcom/google/android/apps/newsstanddev/R$menu;->home_activity_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 79
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    const-string v0, "finish_immediately"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->finish()V

    .line 96
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->isActivityStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->delayHandlingIntentExtras:Z

    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 95
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 64
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->menu_demo:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 65
    .local v0, "demo":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_demo_menu_item:I

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 68
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onStart()V

    .line 103
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->delayHandlingIntentExtras:Z

    if-eqz v1, :cond_0

    .line 104
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->delayHandlingIntentExtras:Z

    .line 105
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 106
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->removeBackstackExtra(Landroid/content/Intent;)V

    .line 109
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public bridge synthetic onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    check-cast p3, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V

    return-void
.end method

.method public onStateChanged(Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V
    .locals 3
    .param p2, "newState"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    .param p3, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
            ">;",
            "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
            "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "fragment":Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;, "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment<Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;>;"
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 132
    .local v0, "pageChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 133
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->setActiveHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeActivity;->closeDrawer()V

    .line 136
    :cond_1
    return-void

    .line 131
    .end local v0    # "pageChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
