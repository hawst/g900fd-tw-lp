.class Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;
.super Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 414
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;->headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method protected maintainsHeroAspectRatio()Z
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x1

    return v0
.end method

.method public sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic sharedElementView(Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 414
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;->sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
