.class public Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;
.super Lcom/google/android/libraries/bind/widget/BindingLinearLayout;
.source "OnboardQuizItem.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final CLICK_FORWARDER:Landroid/view/View$OnClickListener;

.field public static final DK_CLICK_LISTENER:I

.field public static final DK_CONTENT_DESCRIPTION:I

.field public static final DK_DEFAULT_ON:I

.field public static final DK_INITIALS:I

.field public static final DK_ITEM_ID:I

.field public static final DK_SELECTED:I

.field public static final DK_SELECTION_COLOR_RES_ID:I

.field public static final DK_SOURCE_ASPECT_RATIO:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_TITLE:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUT_CURATION:I

.field public static final LAYOUT_MAGAZINE:I


# instance fields
.field private sourceIconView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_itemId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ICON_ID:I

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_sourceAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ASPECT_RATIO:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_TITLE:I

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_initials:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_INITIALS:I

    .line 38
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_selected:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_clickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CLICK_LISTENER:I

    .line 45
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_selectionColorResId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTION_COLOR_RES_ID:I

    .line 47
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_defaultOn:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_DEFAULT_ON:I

    .line 49
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->OnboardQuizItem_contentDescription:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_CONTENT_DESCRIPTION:I

    .line 51
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_quiz_magazine_item:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->LAYOUT_MAGAZINE:I

    .line 52
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_quiz_curation_item:I

    sput v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->LAYOUT_CURATION:I

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->EQUALITY_FIELDS:[I

    .line 141
    new-instance v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->CLICK_FORWARDER:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method


# virtual methods
.method protected getSourceIconView()Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->sourceIconView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    if-nez v0, :cond_0

    .line 80
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->sourceIconView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->sourceIconView:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method protected isCurationItem()Z
    .locals 2

    .prologue
    .line 87
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->onboard_quiz_curation_item:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 6
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 94
    if-eqz p1, :cond_0

    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTED:I

    invoke-virtual {p1, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    .line 97
    .local v2, "selected":Z
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->setSelected(Z)V

    .line 99
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 100
    .local v0, "colorResId":Ljava/lang/Integer;
    :goto_1
    if-nez v0, :cond_2

    move v1, v3

    .line 102
    .local v1, "opaqueColor":I
    :goto_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->isCurationItem()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->updateCurationDrawableColors(I)V

    .line 105
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->getSourceIconView()Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->setUseRoundedMask(Z)V

    .line 109
    :goto_3
    return-void

    .end local v0    # "colorResId":Ljava/lang/Integer;
    .end local v1    # "opaqueColor":I
    .end local v2    # "selected":Z
    :cond_0
    move v2, v3

    .line 94
    goto :goto_0

    .line 99
    .restart local v2    # "selected":Z
    :cond_1
    sget v5, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTION_COLOR_RES_ID:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 100
    .restart local v0    # "colorResId":Ljava/lang/Integer;
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_2

    .line 107
    .restart local v1    # "opaqueColor":I
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->updateMagazineOverlayColor(I)V

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onFinishInflate()V

    .line 75
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->background:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->CLICK_FORWARDER:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method protected setDiskColor(II)V
    .locals 4
    .param p1, "viewId"    # I
    .param p2, "argb"    # I

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 122
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 123
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 124
    .local v1, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->disk:I

    .line 125
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 126
    .local v0, "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 128
    .end local v0    # "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    .end local v1    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_0
    return-void
.end method

.method protected updateCurationDrawableColors(I)V
    .locals 4
    .param p1, "opaqueColor"    # I

    .prologue
    .line 112
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->background:I

    invoke-virtual {p0, v2, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->setDiskColor(II)V

    .line 115
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->onboard_quiz_curation_overlay_opacity_percent:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x40233333    # 2.55f

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 116
    .local v0, "overlayAlpha":I
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->applyAlpha(II)I

    move-result v1

    .line 117
    .local v1, "overlayColor":I
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->overlay:I

    invoke-virtual {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->setDiskColor(II)V

    .line 118
    return-void
.end method

.method protected updateMagazineOverlayColor(I)V
    .locals 5
    .param p1, "opaqueColor"    # I

    .prologue
    .line 131
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->overlay:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 132
    .local v2, "overlayView":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$integer;->onboard_quiz_magazine_overlay_opacity_percent:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x40233333    # 2.55f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    .line 136
    .local v0, "overlayAlpha":I
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->applyAlpha(II)I

    move-result v1

    .line 137
    .local v1, "overlayColor":I
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 139
    .end local v0    # "overlayAlpha":I
    .end local v1    # "overlayColor":I
    :cond_0
    return-void
.end method
