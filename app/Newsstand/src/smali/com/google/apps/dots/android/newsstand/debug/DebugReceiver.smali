.class public Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DebugReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private handleLogd(Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "enable"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "className":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 51
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 52
    const-string v3, "class"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    :cond_0
    if-nez v1, :cond_2

    .line 55
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->enableAll(Z)V

    .line 56
    const-string v4, "Logd globally %s"

    new-array v5, v5, [Ljava/lang/Object;

    if-eqz p3, :cond_1

    const-string v3, "enabled"

    :goto_0
    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->notifyUser(Landroid/content/Context;Ljava/lang/String;)V

    .line 67
    :goto_1
    return-void

    .line 56
    :cond_1
    const-string v3, "disabled"

    goto :goto_0

    .line 58
    :cond_2
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    .line 59
    .local v0, "classLogger":Lcom/google/apps/dots/android/newsstand/logging/Logd;
    if-eqz p3, :cond_3

    .line 60
    const-string v3, "Enabled Logd for class %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->notifyUser(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->enable()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    goto :goto_1

    .line 63
    :cond_3
    const-string v3, "Disabled Logd for class %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->notifyUser(Landroid/content/Context;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->disable()Lcom/google/android/libraries/bind/logging/Logd;

    goto :goto_1
.end method

.method private handleStrictModeEnable(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->enableStrictMode()V

    .line 71
    const-string v0, "Strict mode enabled"

    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->notifyUser(Landroid/content/Context;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method private static notifyUser(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 76
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "actionName":Ljava/lang/String;
    const-string v1, "com.google.apps.dots.android.newsstand.debug.LOGD_ENABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->handleLogd(Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const-string v1, "com.google.apps.dots.android.newsstand.debug.LOGD_DISABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->handleLogd(Landroid/content/Context;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 43
    :cond_2
    const-string v1, "com.google.apps.dots.android.newsstand.debug.STRICT_MODE_ENABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/debug/DebugReceiver;->handleStrictModeEnable(Landroid/content/Context;)V

    goto :goto_0
.end method
