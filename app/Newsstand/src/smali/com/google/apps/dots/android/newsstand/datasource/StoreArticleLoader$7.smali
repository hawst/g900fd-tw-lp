.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createIdToAdTemplateMapFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Application;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1, "app"    # Lcom/google/apps/dots/proto/client/DotsShared$Application;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Application;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    const/4 v3, 0x0

    .line 251
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getInterstitialAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 252
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getLeaderboardAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 253
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Application;->getMrectAdSettings()Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;

    move-result-object v4

    aput-object v4, v2, v3

    .line 250
    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->collectAdTemplateIds([Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/util/Set;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$700(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;[Lcom/google/apps/dots/proto/client/DotsShared$AdFormatSettings;)Ljava/util/Set;

    move-result-object v0

    .line 254
    .local v0, "adTemplateIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadIdToAdTemplateMap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$800(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 247
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$7;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Application;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
