.class Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "NewsArticleParentLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->setupScrollDelegation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private lastScrollDirection:Ljava/lang/Boolean;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 297
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->lastScrollDirection:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->lastScrollDirection:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    cmpg-float v0, p4, v4

    if-gez v0, :cond_1

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    if-eqz v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    mul-float/2addr p4, v0

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    move-result-object v0

    float-to-int v3, p4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->flingScroll(II)V

    .line 318
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F
    invoke-static {v0, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$502(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;F)F

    .line 319
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z
    invoke-static {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$602(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Z)Z

    .line 320
    return v1

    :cond_1
    move v0, v2

    .line 313
    goto :goto_0

    :cond_2
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 303
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    float-to-int v3, p4

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->tryScrollBy(II)Z
    invoke-static {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->access$300(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;II)Z

    .line 304
    const/4 v2, 0x0

    cmpl-float v2, p4, v2

    if-lez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;->lastScrollDirection:Ljava/lang/Boolean;

    .line 305
    return v1
.end method
