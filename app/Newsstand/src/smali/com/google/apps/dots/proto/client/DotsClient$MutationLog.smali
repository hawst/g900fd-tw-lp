.class public final Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MutationLog"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;


# instance fields
.field private account_:Ljava/lang/String;

.field public action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

.field private batchEndpointUri_:Ljava/lang/String;

.field private bitField0_:I

.field private lastHttpFailureTime_:J

.field private numTries_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1749
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1750
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1755
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    .line 1777
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    .line 1799
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 1802
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    .line 1821
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    .line 1750
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 5

    .prologue
    .line 1854
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1858
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1859
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 1860
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 1861
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 1862
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1860
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1855
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 1856
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 1866
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1746
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->clone()Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1871
    if-ne p1, p0, :cond_1

    .line 1876
    :cond_0
    :goto_0
    return v1

    .line 1872
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1873
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    .line 1874
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1875
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 1876
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 1874
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    .line 1875
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1757
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    return-object v0
.end method

.method public getBatchEndpointUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1779
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastHttpFailureTime()J
    .locals 2

    .prologue
    .line 1804
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    return-wide v0
.end method

.method public getNumTries()I
    .locals 1

    .prologue
    .line 1823
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1922
    const/4 v1, 0x0

    .line 1923
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1924
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    .line 1925
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1927
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 1928
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    .line 1929
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1931
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-eqz v2, :cond_3

    .line 1932
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 1933
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    if-eqz v0, :cond_2

    .line 1934
    const/4 v5, 0x3

    .line 1935
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1932
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1939
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    .line 1940
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    .line 1941
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 1943
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_5

    .line 1944
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    .line 1945
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 1947
    :cond_5
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->cachedSize:I

    .line 1948
    return v1
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1882
    const/16 v1, 0x11

    .line 1883
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 1884
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 1885
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 1886
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-nez v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    .line 1892
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 1893
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    add-int v1, v2, v3

    .line 1894
    return v1

    .line 1884
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 1885
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 1888
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1889
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 1888
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1889
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1956
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1957
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1961
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1962
    :sswitch_0
    return-object p0

    .line 1967
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    .line 1968
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    goto :goto_0

    .line 1972
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    .line 1973
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    goto :goto_0

    .line 1977
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1978
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-nez v5, :cond_2

    move v1, v4

    .line 1979
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 1980
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-eqz v5, :cond_1

    .line 1981
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1983
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 1984
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1985
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    aput-object v6, v5, v1

    .line 1986
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1987
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1984
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1978
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v1, v5

    goto :goto_1

    .line 1990
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    aput-object v6, v5, v1

    .line 1991
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1995
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    .line 1996
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    goto :goto_0

    .line 2000
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    .line 2001
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    goto/16 :goto_0

    .line 1957
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1746
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-result-object v0

    return-object v0
.end method

.method public setAccount(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1760
    if-nez p1, :cond_0

    .line 1761
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1763
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    .line 1764
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    .line 1765
    return-object p0
.end method

.method public setBatchEndpointUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1782
    if-nez p1, :cond_0

    .line 1783
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1785
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    .line 1786
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    .line 1787
    return-object p0
.end method

.method public setLastHttpFailureTime(J)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1807
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    .line 1808
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    .line 1809
    return-object p0
.end method

.method public setNumTries(I)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1826
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    .line 1827
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    .line 1828
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1899
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1900
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->account_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1902
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1903
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->batchEndpointUri_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1905
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    if-eqz v1, :cond_3

    .line 1906
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 1907
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    if-eqz v0, :cond_2

    .line 1908
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1906
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1912
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 1913
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->lastHttpFailureTime_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1915
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 1916
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->numTries_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1918
    :cond_5
    return-void
.end method
