.class public final Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NewItem"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;


# instance fields
.field private appFamilyId_:Ljava/lang/String;

.field private appId_:Ljava/lang/String;

.field private bitField0_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23154
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23155
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 23160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    .line 23182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    .line 23155
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    .locals 3

    .prologue
    .line 23215
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23219
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    return-object v0

    .line 23216
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    :catch_0
    move-exception v1

    .line 23217
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23151
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->clone()Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23224
    if-ne p1, p0, :cond_1

    .line 23228
    :cond_0
    :goto_0
    return v1

    .line 23225
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 23226
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    .line 23227
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 23228
    goto :goto_0

    .line 23227
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    .line 23228
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23184
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 23251
    const/4 v0, 0x0

    .line 23252
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 23253
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    .line 23254
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23256
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 23257
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    .line 23258
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23260
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->cachedSize:I

    .line 23261
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 23232
    const/16 v0, 0x11

    .line 23233
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 23234
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 23235
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 23236
    return v0

    .line 23234
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 23235
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 23270
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 23274
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 23275
    :sswitch_0
    return-object p0

    .line 23280
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    .line 23281
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    goto :goto_0

    .line 23285
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    .line 23286
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    goto :goto_0

    .line 23270
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23151
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23241
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 23242
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 23244
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 23245
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$NewsstandSyncMessage$NewItem;->appId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 23247
    :cond_1
    return-void
.end method
