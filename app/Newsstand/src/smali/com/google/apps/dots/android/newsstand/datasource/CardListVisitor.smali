.class public abstract Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_PRIMARY_KEY:I

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SUPPORT_TOPIC_CARDS:Z


# instance fields
.field private final appContext:Landroid/content/Context;

.field protected final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private currentReadNowShelf:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

.field currentReadNowShelfPrimaryKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private currentReadNowShelfType:I

.field private currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

.field private currentTopicData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field protected final librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

.field private postCounter:I

.field private final readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

.field private rowCounter:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 100
    const-class v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 102
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardListVisitor_defaultPrimaryKey:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    .line 108
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->SUPPORT_TOPIC_CARDS:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "primaryKey"    # I
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;-><init>(I)V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    .line 116
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    .line 118
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    .line 124
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfPrimaryKeys:Ljava/util/Set;

    .line 126
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->rowCounter:I

    .line 127
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->postCounter:I

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    .line 145
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 146
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    .line 147
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p4, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 131
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;-><init>(Landroid/content/Context;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 136
    return-void
.end method

.method private addReadNowShelfIfNeeded()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 402
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    if-eq v4, v6, :cond_2

    .line 403
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    const/4 v0, 0x1

    .line 406
    .local v0, "isMagazineShelf":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    .line 436
    .end local v0    # "isMagazineShelf":Z
    :goto_1
    return-void

    .line 403
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 413
    .restart local v0    # "isMagazineShelf":Z
    :cond_1
    new-instance v2, Lcom/google/android/libraries/bind/data/DataList;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 414
    .local v2, "shelfList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 415
    .local v3, "shelfRow":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP_PROVIDER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;

    invoke-direct {v5, p0, v2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/android/libraries/bind/data/DataList;Z)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 427
    if-eqz v0, :cond_3

    const-string v1, "magazineShelf"

    .line 428
    .local v1, "rowId":Ljava/lang/String;
    :goto_2
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v3, v4, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 429
    sget v4, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {v3, v4, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 430
    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 432
    .end local v0    # "isMagazineShelf":Z
    .end local v1    # "rowId":Ljava/lang/String;
    .end local v2    # "shelfList":Lcom/google/android/libraries/bind/data/DataList;
    .end local v3    # "shelfRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    iput v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    .line 433
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 434
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfPrimaryKeys:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 435
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    goto :goto_1

    .line 427
    .restart local v0    # "isMagazineShelf":Z
    .restart local v2    # "shelfList":Lcom/google/android/libraries/bind/data/DataList;
    .restart local v3    # "shelfRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    const-string v1, "newsShelf"

    goto :goto_2
.end method

.method private makeMagazineSplashCardData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    const/4 v6, 0x0

    .line 443
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 445
    .local v2, "cardData":Lcom/google/android/libraries/bind/data/Data;
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 446
    .local v0, "appId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v3

    .line 447
    .local v3, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazineSplashKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 449
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->LAYOUTS:[I

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 450
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 452
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SPLASH_ATTACHMENT_ID:I

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_0

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    aget-object v4, v4, v6

    :goto_0
    invoke-virtual {v2, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 455
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-static {p1, v5}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v5

    div-float v1, v4, v5

    .line 456
    .local v1, "aspectRatioInverse":F
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_ICON_ID:I

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 457
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_HINT_TEXT:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->continue_reading:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 458
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_NAME:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 459
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SOURCE_ASPECT_RATIO:I

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 460
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_TIME:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 461
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardSplashItem;->DK_SPLASH_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;

    invoke-direct {v5, p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 474
    sget v4, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$3;

    invoke-direct {v5, p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$3;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 482
    return-object v2

    .line 454
    .end local v1    # "aspectRatioInverse":F
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private static makeMagazineSplashKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "appId"    # Ljava/lang/String;

    .prologue
    .line 439
    const-string v0, "magazine_splash"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private makeShelfData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;I)V
    .locals 9
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "shelfType"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 301
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    if-ne p2, v7, :cond_4

    move v2, v4

    .line 307
    .local v2, "isMagazineShelf":Z
    :goto_1
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 312
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 314
    .local v1, "cardData":Lcom/google/android/libraries/bind/data/Data;
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 315
    .local v0, "appId":Ljava/lang/String;
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v1, v3, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 316
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v6, Lcom/google/android/apps/newsstanddev/R$layout;->card_icon:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 317
    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v6, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->EQUALITY_FIELDS:[I

    invoke-virtual {v1, v3, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 320
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v3

    const/4 v6, 0x3

    if-ne v3, v6, :cond_5

    .line 321
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_BACKGROUND:I

    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 322
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getRssDrawableId(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 321
    invoke-virtual {v1, v3, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 327
    :goto_2
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_ASPECT_RATIO:I

    if-eqz v2, :cond_6

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 328
    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v3

    :goto_3
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 327
    invoke-virtual {v1, v6, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 329
    if-eqz v2, :cond_7

    .line 332
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_CONTENT_DESCRIPTION:I

    const-string v6, "%s %s"

    new-array v7, v7, [Ljava/lang/Object;

    .line 333
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 332
    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 337
    :goto_4
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_TRANSITION_NAME:I

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 338
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_ON_CLICK_LISTENER:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;

    .line 339
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v2, v0, v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 340
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    if-eq v3, p2, :cond_3

    .line 341
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addReadNowShelfIfNeeded()V

    .line 342
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfType:I

    .line 344
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfPrimaryKeys:Ljava/util/Set;

    invoke-virtual {p0, v1, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->checkAndAddPrimaryKey(Lcom/google/android/libraries/bind/data/Data;Ljava/util/Set;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 345
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 346
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "isMagazineShelf":Z
    :cond_4
    move v2, v5

    .line 304
    goto/16 :goto_1

    .line 324
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v1    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "isMagazineShelf":Z
    :cond_5
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_IMAGE_ID:I

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 328
    :cond_6
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_3

    .line 335
    :cond_7
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_CONTENT_DESCRIPTION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_4
.end method

.method private static shouldIgnoreEntity(Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;)Z
    .locals 3
    .param p0, "ignoreEntity"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .param p1, "entity"    # Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .prologue
    const/4 v0, 0x0

    .line 542
    if-nez p0, :cond_1

    .line 550
    :cond_0
    :goto_0
    return v0

    .line 546
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getCurationAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getCurationAppId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 550
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 8
    .param p1, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 848
    .line 851
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v2

    .line 852
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 855
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->nextPostIndex()I

    move-result v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-object v0, p1

    move-object v1, p2

    .line 848
    invoke-static/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/reading/PostReadingHelper;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 857
    return-void
.end method

.method addTopicCardData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3
    .param p1, "postData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 603
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->cardIds:Ljava/util/Set;

    invoke-virtual {p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->checkAndAddPrimaryKey(Lcom/google/android/libraries/bind/data/Data;Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ENTITY_IMAGE_ID:I

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 606
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_PRIMARY_IMAGE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    .line 607
    .local v0, "imageId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 608
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ENTITY_IMAGE_ID:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 611
    .end local v0    # "imageId":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 613
    :cond_1
    return-void
.end method

.method addTopicData(Lcom/google/android/libraries/bind/data/Data;Ljava/util/List;)V
    .locals 9
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/Data;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "clientEntityList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 499
    .line 500
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getShowTopicTags()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getShowTopicTags()Z

    move-result v8

    if-nez v8, :cond_5

    :cond_1
    move v2, v7

    .line 503
    .local v2, "hideTopicTags":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasUntranslatedSectionId()Z

    move-result v8

    if-nez v8, :cond_3

    .line 504
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasTranslationCode()Z

    move-result v8

    if-eqz v8, :cond_6

    :cond_3
    move v3, v7

    .line 505
    .local v3, "hideTranslatedTopicTags":Z
    :goto_1
    if-nez v2, :cond_4

    if-eqz v3, :cond_7

    .line 539
    :cond_4
    :goto_2
    return-void

    .end local v2    # "hideTopicTags":Z
    .end local v3    # "hideTranslatedTopicTags":Z
    :cond_5
    move v2, v6

    .line 501
    goto :goto_0

    .restart local v2    # "hideTopicTags":Z
    :cond_6
    move v3, v6

    .line 504
    goto :goto_1

    .line 510
    .restart local v3    # "hideTranslatedTopicTags":Z
    :cond_7
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 512
    .local v0, "entity":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->ignoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    .line 513
    .local v4, "ignoreEntity":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    invoke-static {v4, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->shouldIgnoreEntity(Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 514
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromClientEntity(Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 515
    .local v1, "entityEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v1, :cond_8

    .line 516
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY:I

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 517
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->isSubscribedToEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v5

    .line 518
    .local v5, "subscribed":Z
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_IS_SUBSCRIBED:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 519
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_ID:I

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 520
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ENTITY_ON_CLICK_LISTENER:I

    new-instance v7, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$4;

    invoke-direct {v7, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$4;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;)V

    invoke-virtual {p1, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method addWebPageReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;Ljava/lang/String;)V
    .locals 3
    .param p1, "cardData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "webPageSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .param p3, "fakePostId"    # Ljava/lang/String;

    .prologue
    .line 930
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_POST_ID:I

    invoke-virtual {p1, v1, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 931
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->LINK_VIEW:Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 932
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_TITLE:I

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 933
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_URL:I

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 934
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_PUBLISHER:I

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 935
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/share/ShareParams;->forLink(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    move-result-object v1

    .line 936
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->setUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    move-result-object v1

    .line 937
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;->setTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/share/ShareParams$ForLink;

    move-result-object v0

    .line 938
    .local v0, "shareParams":Lcom/google/apps/dots/android/newsstand/share/ShareParams;
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_SHARE_PARAMS:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/ImmediateTask;->create(Ljava/lang/Object;)Lcom/google/apps/dots/android/newsstand/async/Task;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 939
    return-void
.end method

.method public exit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 8
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    const/4 v7, 0x0

    .line 195
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasClusterSummary()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 196
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v3, :cond_3

    .line 197
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ARTICLE_ARRAY:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    .line 198
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    .line 197
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 199
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/Boolean;

    .line 200
    .local v2, "readStateArray":[Ljava/lang/Boolean;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 201
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 202
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_IS_READ:I

    invoke-virtual {v0, v3, v7}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v1

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 204
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_READ_STATE_ARRAY:I

    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 206
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 207
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 209
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    .line 210
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 218
    .end local v1    # "i":I
    .end local v2    # "readStateArray":[Ljava/lang/Boolean;
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/BaseCardListVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 219
    return-void

    .line 212
    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v3

    const-string v4, "Expected APPLICATION_NODE as child of CLUSTER_NODE, but APPLICATION_NODE not found."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 215
    :cond_4
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasReadNowEditionShelfSummary()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 216
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addReadNowShelfIfNeeded()V

    goto :goto_1
.end method

.method public bridge synthetic exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    return-void
.end method

.method protected getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getAnalyticsScreenName()Ljava/lang/String;
.end method

.method getTrendingTimeText(J)Ljava/lang/String;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->trending_time:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->relativePastTimeString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected ignoreEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isPurchasedEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    return v0
.end method

.method protected isSubscribedToEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    return v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected makeCardSourceListItem(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 10
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    const/4 v6, 0x0

    .line 1045
    invoke-static {p2, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v9

    .line 1046
    .local v9, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-nez v9, :cond_0

    .line 1047
    const/4 v7, 0x0

    .line 1093
    :goto_0
    return-object v7

    .line 1050
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    .line 1051
    .local v7, "card":Lcom/google/android/libraries/bind/data/Data;
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->LAYOUTS:[I

    aget v4, v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1052
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v7, v0, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1054
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->isSubscribedToEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    .line 1055
    .local v2, "subscribed":Z
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->isPurchasedEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v3

    .line 1056
    .local v3, "purchased":Z
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v1, v9, p2, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    .line 1057
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    .line 1061
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    const/4 v5, 0x1

    new-instance v8, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;

    invoke-direct {v8, p0, v9}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$16;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 1057
    invoke-static/range {v0 .. v8}, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZLandroid/accounts/Account;ZZLcom/google/android/libraries/bind/data/Data;Landroid/view/View$OnClickListener;)V

    .line 1085
    sget v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v4, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$17;

    invoke-direct {v4, p0, v9}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$17;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v7, v0, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected makeCommonShelfCard(Ljava/lang/Object;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "primaryValue"    # Ljava/lang/Object;

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 1001
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1004
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->LAYOUT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1006
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1007
    return-object v0
.end method

.method protected makeMagazineOfferCard(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 9
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 942
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 944
    .local v2, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    .line 945
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    .line 947
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iget-object v6, v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v4

    .line 950
    .local v4, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    iget-object v7, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 953
    sget v6, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->LAYOUTS:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 954
    sget v6, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 957
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_ISSUE_NAME:I

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 958
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_MAGAZINE_NAME:I

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 960
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_COVER_ATTACHMENT_ID:I

    .line 961
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v7

    .line 960
    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 962
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_COVER_HEIGHT_TO_WIDTH_RATIO:I

    const/high16 v7, 0x3fc00000    # 1.5f

    .line 963
    invoke-static {v1, v7}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    .line 962
    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 965
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_RATING:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getFiveStarRating()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 967
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hasAmount()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 968
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_PRICE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getAmount()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 971
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hasBackendDocId()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 972
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getBackendDocId()Ljava/lang/String;

    move-result-object v5

    .line 973
    .local v5, "titleDocId":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$14;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 981
    .local v3, "onClickListener":Landroid/view/View$OnClickListener;
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePurchaseableItem;->DK_CLICK_LISTENER:I

    invoke-virtual {v2, v6, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 986
    .end local v3    # "onClickListener":Landroid/view/View$OnClickListener;
    .end local v5    # "titleDocId":Ljava/lang/String;
    :cond_1
    sget v6, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v7, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$15;

    invoke-direct {v7, p0, v4}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$15;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v2, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 994
    return-object v2
.end method

.method protected makeMagazinePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 10
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    const/4 v9, 0x0

    .line 741
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 743
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 744
    .local v3, "postId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v4

    .line 745
    .local v4, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    move-object v2, v4

    .line 748
    .local v2, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 749
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v0, v5, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 752
    const/4 v1, 0x0

    .line 753
    .local v1, "layout":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 771
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v5

    const-string v6, "Unrecognized ToC type %d, falling back on TEXT_METADATA."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    .line 772
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getTocType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    .line 771
    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 773
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->LAYOUTS:[I

    aget v1, v5, v9

    .line 774
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    invoke-static {v5, p1, v4, v6, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/android/libraries/bind/data/Data;)V

    .line 777
    :goto_0
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 781
    sget v5, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v6, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;

    invoke-direct {v6, p0, v4, v2, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$10;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    invoke-virtual {v0, v5, v6}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 789
    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 791
    return-object v0

    .line 758
    :pswitch_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->LAYOUTS:[I

    aget v1, v5, v9

    .line 759
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    invoke-static {v5, p1, v4, v6, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0

    .line 763
    :pswitch_1
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->LAYOUTS:[I

    aget v1, v5, v9

    .line 764
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    invoke-static {v5, p1, v4, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePagesItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0

    .line 767
    :pswitch_2
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->LAYOUTS:[I

    aget v1, v5, v9

    .line 768
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    invoke-static {v5, p1, v4, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0

    .line 753
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected makeMerchandisingShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 5
    .param p1, "shelfSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .prologue
    .line 1013
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonShelfCard(Ljava/lang/Object;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 1016
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 1019
    .local v2, "title":Ljava/lang/String;
    :goto_0
    if-nez v2, :cond_0

    .line 1020
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getType()I

    move-result v1

    .line 1021
    .local v1, "shelfType":I
    packed-switch v1, :pswitch_data_0

    .line 1030
    .end local v1    # "shelfType":I
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 1031
    sget v3, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE:I

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1032
    sget v3, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TEXT_COLOR:I

    sget v4, Lcom/google/android/apps/newsstanddev/R$color;->play_primary_text:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1035
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->hasSubtitle()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1036
    sget v3, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_SUBTITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 1039
    :cond_2
    return-object v0

    .line 1016
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1023
    .restart local v1    # "shelfType":I
    .restart local v2    # "title":Ljava/lang/String;
    :pswitch_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->recommended_magazines:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1024
    goto :goto_1

    .line 1021
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected makeNewsPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;ZZ)Lcom/google/android/libraries/bind/data/Data;
    .locals 14
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "includeSourceData"    # Z
    .param p3, "useDarkTheme"    # Z

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    .line 798
    .local v9, "cardData":Lcom/google/android/libraries/bind/data/Data;
    iget-object v10, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    .line 799
    .local v10, "appId":Ljava/lang/String;
    iget-object v13, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 800
    .local v13, "postId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v2

    .line 801
    .local v2, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v10}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v12

    .line 804
    .local v12, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v9, v0, v13}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 811
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->getAppropriateCardLayout(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)I

    move-result v11

    .line 812
    .local v11, "layout":I
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 815
    if-eqz p2, :cond_0

    .line 816
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 817
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->RELATED_POSTS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v0, v1, :cond_1

    const/16 p2, 0x1

    .line 820
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    .line 824
    invoke-virtual {p0, v12}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->isPurchasedEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v4

    .line 826
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->isFullImageLayout(I)Z

    move-result v6

    .line 828
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    move/from16 v5, p2

    move/from16 v7, p3

    .line 820
    invoke-static/range {v0 .. v9}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V

    .line 833
    sget v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$11;

    invoke-direct {v1, p0, v2, v12, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$11;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    invoke-virtual {v9, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 841
    iget-object v0, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addTopicData(Lcom/google/android/libraries/bind/data/Data;Ljava/util/List;)V

    .line 842
    invoke-virtual {p0, v9, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 844
    return-object v9

    .line 817
    :cond_1
    const/16 p2, 0x0

    goto :goto_0
.end method

.method public makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 3
    .param p1, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .param p2, "listToLocallyModify"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 487
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    const/4 v1, 0x0

    .line 491
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAnalyticsScreenName()Ljava/lang/String;

    move-result-object v2

    .line 487
    invoke-static {v0, p1, p2, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->fillOffersCardData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;ZLjava/lang/String;)V

    .line 492
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->getOfferId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 493
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 494
    return-void
.end method

.method protected makePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 725
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method protected makePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "includeSourceData"    # Z

    .prologue
    .line 733
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_0

    .line 734
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazinePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 736
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeNewsPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;ZZ)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method makeTopicPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 9
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 618
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v2

    .line 619
    .local v2, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->newsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    move-result-object v1

    .line 622
    .local v1, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 627
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->LAYOUT_SELECTOR:Lcom/google/android/libraries/bind/data/DataProperty;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 628
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 631
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_TITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 632
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSourceIconId()Ljava/lang/String;

    move-result-object v3

    .line 633
    .local v3, "sourceIconId":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 634
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Missing sourceIconId for postId %s, falling back on favicon."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 636
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFavicon()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 637
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFavicon()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v3

    .line 640
    :cond_0
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_SOURCE_NAME:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 641
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_IS_READ:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readStateCollection:Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->isRead(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 642
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_PRIMARY_IMAGE:I

    .line 643
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 642
    :goto_0
    invoke-virtual {v0, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 645
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_STORY_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;

    invoke-direct {v5, p0, v1, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$6;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 660
    sget v4, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->DK_ARTICLE_VERSION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 664
    sget v4, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$7;

    invoke-direct {v5, p0, v2, v1, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$7;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 672
    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addPostReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 673
    return-object v0

    .line 643
    :cond_1
    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    goto :goto_0
.end method

.method makeTopicWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "webPageSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 679
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v1

    .line 682
    .local v1, "fakePostId":Ljava/lang/String;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 685
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->LAYOUT_SELECTOR:Lcom/google/android/libraries/bind/data/DataProperty;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 686
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 689
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_TITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 690
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_SOURCE_NAME:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 691
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardTopicListItem;->DK_STORY_ON_CLICK_LISTENER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$8;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 705
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_STORY_ON_CLICK_LISTENER:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ON_CLICK_LISTENER:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->copy(II)V

    .line 710
    sget v2, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$9;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$9;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 718
    invoke-virtual {p0, v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addWebPageReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;Ljava/lang/String;)V

    .line 719
    return-object v0
.end method

.method makeWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p1, "webPageSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .prologue
    const/4 v6, 0x0

    .line 862
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 864
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v1

    .line 865
    .local v1, "fakePostId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 868
    .local v2, "postTitle":Ljava/lang/String;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v0, v4, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 871
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->LAYOUTS_NO_IMAGE:[I

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 872
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 876
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_VERSION:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 877
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TITLE:I

    invoke-virtual {v0, v4, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 880
    sget v5, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_IMAGE_ID:I

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 881
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->hasThumbnail()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 882
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_ICON_ID:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 884
    :cond_0
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 885
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->hasExternalCreatedMs()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 886
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getExternalCreatedMs()J

    move-result-wide v4

    .line 885
    invoke-virtual {p0, v4, v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getTrendingTimeText(J)Ljava/lang/String;

    move-result-object v3

    .line 888
    .local v3, "timeText":Ljava/lang/String;
    :goto_0
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_TIME:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 889
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_ABSTRACT:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getAbstract()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 890
    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_STORY_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;

    invoke-direct {v5, p0, p1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$12;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 913
    sget v4, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;

    invoke-direct {v5, p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 922
    invoke-virtual {p0, v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addWebPageReadingData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;Ljava/lang/String;)V

    .line 923
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addTopicData(Lcom/google/android/libraries/bind/data/Data;Ljava/util/List;)V

    .line 924
    invoke-static {v0, v6}, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->addTheming(Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 926
    return-object v0

    .line 885
    .end local v3    # "timeText":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->appContext:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->web_content:I

    .line 887
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected nextPostIndex()I
    .locals 2

    .prologue
    .line 177
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->postCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->postCounter:I

    return v0
.end method

.method protected nextRowIndex()I
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->rowCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->rowCounter:I

    return v0
.end method

.method populateTopicCardData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 7
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 569
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 572
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->LAYOUTS:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 573
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->EQUALITY_FIELDS:[I

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 575
    iget-object v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 576
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 577
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getLeadCurationClientEntityId()Ljava/lang/String;

    move-result-object v6

    .line 575
    invoke-static {v3, v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->curatedTopicEdition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    move-result-object v0

    .line 579
    .local v0, "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_TITLE:I

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 581
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getColorResourceId(Z)I

    move-result v1

    .line 582
    .local v1, "topicColorResId":I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v2

    .line 583
    .local v2, "topicIconAttachmentId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 584
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_TOPIC_ICON_ID:I

    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 585
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

    .line 586
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 585
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 592
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_ENTITY_ON_CLICK_LISTENER:I

    new-instance v5, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$5;

    invoke-direct {v5, p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$5;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 599
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_BACKGROUND_COLOR:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 600
    return-void

    .line 588
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    sget v4, Lcom/google/apps/dots/android/newsstand/card/CardTopicItem;->DK_INITIALS:I

    .line 589
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 588
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected abstract readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 5
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v1

    .line 247
    .local v1, "shelfSummary":Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v2, :cond_1

    .line 248
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->populateTopicCardData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    const/4 v0, 0x1

    .line 255
    .local v0, "isSplash":Z
    :goto_1
    if-eqz v0, :cond_4

    .line 256
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 261
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    .line 262
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->magazineEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v4

    .line 261
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->getSnapshotId(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 263
    :cond_2
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeMagazineSplashCardData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0

    .line 254
    .end local v0    # "isSplash":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 267
    .restart local v0    # "isSplash":Z
    :cond_4
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->getType()I

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeShelfData(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;I)V

    goto :goto_0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "clusterSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    .line 565
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeOfferCardData(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;)V

    .line 242
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 223
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->SUPPORT_TOPIC_CARDS:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    if-nez v0, :cond_1

    .line 224
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makePostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeTopicPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addTopicCardData(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V
    .locals 5
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "sectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .prologue
    .line 273
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v2, :cond_0

    .line 274
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardIcon;->DK_ON_CLICK_LISTENER:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;

    .line 277
    .local v0, "listener":Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->getSectionId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfPrimaryKeys:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 282
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->getSectionId()Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "sectionId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->primaryKey:I

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 284
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfPrimaryKeys:Ljava/util/Set;

    invoke-virtual {p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->checkAndAddPrimaryKey(Lcom/google/android/libraries/bind/data/Data;Ljava/util/Set;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;->setSectionId(Ljava/lang/String;)V

    .line 292
    .end local v0    # "listener":Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;
    .end local v1    # "sectionId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 287
    .restart local v0    # "listener":Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$ShelfIconClickListener;
    .restart local v1    # "sectionId":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelf:Ljava/util/List;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 288
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentReadNowShelfCard:Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "webPageSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .prologue
    .line 232
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->SUPPORT_TOPIC_CARDS:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->currentTopicCard:Lcom/google/android/libraries/bind/data/Data;

    if-nez v0, :cond_1

    .line 233
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeTopicWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->addTopicCardData(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V

    return-void
.end method
