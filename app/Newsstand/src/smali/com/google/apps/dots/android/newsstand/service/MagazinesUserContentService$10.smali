.class final Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.source "MagazinesUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->processMyMagazinesResponse(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor",
        "<",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$archivedAppIds:Ljava/util/Set;

.field final synthetic val$myMagazinesAppSummaries:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;->val$myMagazinesAppSummaries:Ljava/util/List;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;->val$archivedAppIds:Ljava/util/Set;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;->val$myMagazinesAppSummaries:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "purchaseSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .prologue
    .line 462
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->getIsArchived()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService$10;->val$archivedAppIds:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 465
    :cond_0
    return-void
.end method
