.class Lcom/google/apps/dots/android/newsstand/store/NSStore$9;
.super Ljava/lang/Object;
.source "NSStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

.field final synthetic val$key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

.field final synthetic val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

.field final synthetic val$resourceLink:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 548
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$resourceLink:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "%s: removing pending download %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$request:Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$resourceLink:Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 552
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/NSStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/NSStore;->pendingDownloads:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->access$1500(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;->val$key:Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    return-void
.end method
