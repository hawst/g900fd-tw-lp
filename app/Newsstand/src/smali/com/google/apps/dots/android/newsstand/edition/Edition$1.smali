.class Lcom/google/apps/dots/android/newsstand/edition/Edition$1;
.super Ljava/lang/Object;
.source "Edition.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 217
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryImp(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 218
    .local v0, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    if-nez v0, :cond_0

    .line 219
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to retrieve edition summary"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 221
    :cond_0
    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/Edition$1;->call()Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    return-object v0
.end method
