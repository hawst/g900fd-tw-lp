.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->simulateBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$attachmentId:Ljava/lang/String;

.field final synthetic val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

.field final synthetic val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 372
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$attachmentId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "defaultBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 375
    const-string v0, "AttStore-sim"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$attachmentId:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->val$decodeOptions:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->transformDefaultBitmap(Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Bitmap;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;
    invoke-static {v0, v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$700(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;Lcom/google/apps/dots/android/newsstand/server/Transform;Landroid/graphics/Bitmap;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 380
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 372
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$8;->apply(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
