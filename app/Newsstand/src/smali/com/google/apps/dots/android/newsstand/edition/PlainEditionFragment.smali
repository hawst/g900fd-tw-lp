.class public Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "PlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private connectivityListener:Ljava/lang/Runnable;

.field private final editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private emptyViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

.field private errorViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;

.field private final keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

.field private loadingViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

.field private pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

.field private pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

.field private final pinnedStateObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

.field private subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

.field private final translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 94
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->setStrictModeLimits(Ljava/lang/Class;I)V

    .line 95
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x0

    const-string v1, "PlainEditionFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->plain_edition_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    .line 80
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    .line 86
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedStateObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateActionBar()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    return-object v0
.end method

.method private edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method

.method private editionSummaryFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_0

    .line 176
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 177
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-eqz v0, :cond_0

    .line 178
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 181
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v1
.end method

.method private isSubscribed()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshIfNeeded()V
    .locals 3

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 425
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 427
    :cond_0
    return-void
.end method

.method private setupAdapter()V
    .locals 4

    .prologue
    .line 243
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    .line 244
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->emptyViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    .line 245
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->emptyViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    .line 246
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_news:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_edition:I

    .line 245
    invoke-static {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;->setEmptyMessageData(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    .line 248
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->emptyViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setEmptyViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 249
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->loadingViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    .line 250
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->loadingViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setLoadingViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 252
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->errorViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;

    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->errorViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setErrorViewProvider(Lcom/google/android/libraries/bind/data/ViewProvider;)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 282
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 283
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 284
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 285
    return-void
.end method

.method private setupPageViewAnalytics()V
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    .line 168
    return-void
.end method

.method private setupSubscribeMenuHelper()V
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getHasOptionsMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$3;

    invoke-direct {v0, p0, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .line 157
    :cond_0
    return-void
.end method

.method private updateActionBar()V
    .locals 5

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->ownsActionBar()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    .line 224
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V

    .line 223
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 235
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 236
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v1

    .line 238
    .local v1, "subscribed":Z
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/ActionBarUtil;->getActionBarDrawableForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 237
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setActionBarBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 240
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "subscribed":Z
    :cond_0
    return-void
.end method

.method private updateAdapter()V
    .locals 4

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 289
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v2, v3, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 290
    .local v2, "headerType":Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 291
    .local v0, "cardList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pageViewOnScrollListener:Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/PageViewOnScrollListener;->reset()V

    .line 292
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v3, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 293
    return-void

    .line 290
    .end local v0    # "cardList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->cardList(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    goto :goto_0
.end method

.method private updateShareParams()V
    .locals 3

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getHasOptionsMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V

    .line 434
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 435
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V

    .line 434
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 444
    :cond_0
    return-void
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 459
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 460
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 461
    const-string v2, "editionInfo"

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :cond_0
    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 343
    sget v0, Lcom/google/android/apps/newsstanddev/R$menu;->plain_edition_fragment_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 344
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 345
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 346
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onDestroyView()V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onDestroyView()V

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 137
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedStateObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 140
    :cond_1
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 141
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 377
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_add_edition:I

    if-ne v3, v4, :cond_1

    .line 378
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->account()Landroid/accounts/Account;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 413
    :cond_0
    :goto_0
    return v2

    .line 380
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_remove_edition:I

    if-ne v3, v4, :cond_2

    .line 381
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->account()Landroid/accounts/Account;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->unsubscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 383
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v3, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 385
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_refresh:I

    if-ne v3, v4, :cond_3

    .line 386
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->refreshEditionWithSpinner()V

    goto :goto_0

    .line 388
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$id;->menu_mini_cards:I

    if-ne v3, v4, :cond_4

    .line 389
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->blendAnimationOnNextInvalidation()V

    .line 390
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->toggleCompactMode()Z

    move-result v1

    .line 391
    .local v1, "isEnabled":Z
    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 393
    .end local v1    # "isEnabled":Z
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x102002c

    if-ne v3, v4, :cond_8

    .line 394
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->isTaskRoot()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 396
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->isSubscribed()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->NEWS:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_6

    .line 397
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 398
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 399
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v3

    .line 400
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    .line 410
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->supportFinishAfterTransition()V

    goto/16 :goto_0

    .line 402
    :cond_6
    new-instance v3, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    .line 403
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v0

    .line 404
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->isSubscribed()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_7

    .line 405
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setReadNowEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    .line 407
    :cond_7
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->start()V

    goto :goto_1

    .line 413
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    :cond_8
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto/16 :goto_0
.end method

.method onPageView(I)V
    .locals 6
    .param p1, "page"    # I

    .prologue
    .line 530
    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "onPageView: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 532
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    if-nez v0, :cond_0

    .line 538
    :goto_0
    return-void

    .line 537
    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->sendAnalyticsEditionEventIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 356
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->shareHelper:Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 357
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 358
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->menu_refresh:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsRefresh()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 359
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->menu_mini_cards:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 360
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->useCompactMode()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    .line 361
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->isCompactModeAvailable()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 363
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 365
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Landroid/view/Menu;)V

    .line 364
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 358
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onPulledToRefresh(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "onRefreshCompleted"    # Ljava/lang/Runnable;

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 329
    :goto_0
    return-void

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 313
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$7;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Ljava/lang/Runnable;)V

    .line 312
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 468
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onResume()V

    .line 472
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->getMsSincePause()J

    move-result-wide v0

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 473
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->refreshIfNeeded()V

    .line 475
    :cond_0
    return-void
.end method

.method protected bridge synthetic onStateSet(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 67
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->onStateSet(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V

    return-void
.end method

.method protected onStateSet(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setInvisibleHeight(I)V

    .line 527
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 115
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 116
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setupSubscribeMenuHelper()V

    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->onViewCreated()V

    .line 119
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->connectivityListener:Ljava/lang/Runnable;

    .line 126
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setupPageViewAnalytics()V

    .line 127
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setupAdapter()V

    .line 128
    return-void
.end method

.method public refreshEditionWithSpinner()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 418
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->supportsRefresh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 421
    :cond_0
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 486
    instance-of v3, p2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    if-eqz v3, :cond_4

    move-object v2, p2

    .line 487
    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .line 488
    .local v2, "plainEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    iget-object v0, v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 490
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v3, v4, :cond_0

    .line 491
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_1

    .line 492
    :cond_0
    new-array v3, v7, [Landroid/content/Intent;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 493
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    .line 494
    invoke-virtual {v4, v0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setReadNowEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    .line 495
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v6

    .line 492
    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 521
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "plainEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    :goto_0
    return-object v3

    .line 497
    .restart local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .restart local v2    # "plainEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    :cond_1
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SAVED:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_2

    .line 498
    new-array v3, v7, [Landroid/content/Intent;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 499
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v6

    .line 498
    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0

    .line 501
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->TOPIC:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_3

    .line 502
    new-array v3, v5, [Landroid/content/Intent;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 504
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    .line 505
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setReadNowEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v6

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;

    invoke-direct {v4, p1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V

    .line 506
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/PlainEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v7

    .line 502
    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0

    .line 508
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->SECTION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v3, v4, :cond_4

    move-object v3, v0

    .line 509
    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 510
    .local v1, "parentEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-array v3, v5, [Landroid/content/Intent;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v4, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 512
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 513
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryPage(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v4

    .line 514
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v6

    new-instance v4, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    invoke-direct {v5, v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-direct {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V

    .line 517
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v3, v7

    .line 510
    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    goto/16 :goto_0

    .line 521
    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v1    # "parentEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v2    # "plainEditionFragmentState":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method protected saveRestorableStateIfAppropriate()V
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->ownsActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->saveRestorableStateIfAppropriate()V

    .line 482
    :cond_0
    return-void
.end method

.method public sendAnalyticsEditionEventIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V
    .locals 1
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "page"    # I

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->trackAnalytics(I)V

    goto :goto_0
.end method

.method public setUserVisibleHint(Z)V
    .locals 2
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 333
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->setUserVisibleHint(Z)V

    .line 334
    if-eqz p1, :cond_0

    .line 335
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->refreshIfNeeded()V

    .line 338
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->sendAnalyticsEditionEventIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 339
    return-void
.end method

.method protected updateEmptyView()V
    .locals 2

    .prologue
    .line 301
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->emptyViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;->setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;

    .line 302
    return-void
.end method

.method protected updateErrorView()V
    .locals 2

    .prologue
    .line 296
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->errorViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;->setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;

    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->refreshErrorView()Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 298
    return-void
.end method

.method protected updateLoadingView()V
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->loadingViewProvider:Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->headerType:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;->setHeaderType(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)Lcom/google/apps/dots/android/newsstand/data/NSLoadingViewProvider;

    .line 306
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 67
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V
    .locals 4
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .prologue
    const/4 v1, 0x0

    .line 186
    if-eqz p2, :cond_0

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 187
    .local v0, "editionChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 188
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 189
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 190
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateAdapter()V

    .line 191
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->refreshIfNeeded()V

    .line 193
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setTag(Ljava/lang/Object;)V

    .line 194
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateActionBar()V

    .line 195
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateShareParams()V

    .line 196
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->keepEditionMenuHelper:Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 197
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updatedPinnedListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 198
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateErrorView()V

    .line 199
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateEmptyView()V

    .line 200
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateLoadingView()V

    .line 208
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->sendAnalyticsEditionEventIfNeeded(Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 210
    :cond_1
    return-void

    .end local v0    # "editionChanged":Z
    :cond_2
    move v0, v1

    .line 186
    goto :goto_0
.end method

.method protected updatedPinnedListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 3
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 213
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedStateObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 216
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    aput v2, v0, v1

    .line 217
    .local v0, "fields":[I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->filterRow(Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    .line 218
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedEditionRow:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->pinnedStateObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 219
    return-void
.end method
