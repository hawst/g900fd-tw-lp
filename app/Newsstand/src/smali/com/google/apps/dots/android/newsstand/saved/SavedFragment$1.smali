.class Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/PlainConfigurator;
.source "SavedFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/PlainConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 53
    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 55
    .local v2, "state":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/saved/SavedFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 56
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-class v3, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 57
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 58
    .local v0, "fragment":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .end local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;-><init>()V

    .line 60
    .restart local v0    # "fragment":Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;
    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setInitialState(Landroid/os/Parcelable;)V

    .line 61
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 62
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getId()I

    move-result v4

    const-class v5, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v0, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 63
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 65
    :cond_0
    return-void
.end method
