.class public Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "MediaItemFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/media/MediaItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private mediaItemContainer:Landroid/widget/FrameLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x0

    const-string v1, "MediaItemFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->media_item_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 42
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->mediaItemContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 1
    .param p0, "x0"    # Landroid/widget/ImageView;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getScaledDimensions(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/widget/TextView;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->onImageNotLoaded(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method private static getScaledDimensions(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    .locals 2
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "dimens":Lcom/google/apps/dots/android/newsstand/util/Dimensions;
    instance-of v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    if-eqz v1, :cond_0

    move-object v1, p0

    .line 159
    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->getScaledDimensions()Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    .line 161
    :cond_0
    if-nez v0, :cond_1

    .line 162
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Dimensions;->fromBitmapDrawable(Landroid/widget/ImageView;)Lcom/google/apps/dots/android/newsstand/util/Dimensions;

    move-result-object v0

    .line 164
    :cond_1
    return-object v0
.end method

.method private onImageNotLoaded(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 4
    .param p1, "spinner"    # Landroid/view/View;
    .param p2, "errorView"    # Landroid/widget/TextView;
    .param p3, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 149
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 150
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->image_not_available_offline:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Couldn\'t load the image %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    return-void
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 170
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "help_context_key"

    const-string v2, "mobile_news_object"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v2, "mediaDescription"

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-object v0
.end method

.method public getImageView(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Landroid/view/View;
    .locals 8
    .param p1, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->media_item_image:I

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 93
    .local v7, "view":Landroid/view/View;
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->spinner:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    .line 94
    .local v3, "spinner":Landroid/widget/ProgressBar;
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    .line 95
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 96
    .local v2, "imageView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->error:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 99
    .local v4, "errorView":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDefaultTransform()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAttachmentIdPx(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)V

    .line 100
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;Landroid/widget/ProgressBar;Landroid/widget/TextView;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setRunWhenImageSet(Ljava/lang/Runnable;)V

    .line 114
    new-instance v6, Landroid/support/v4/view/GestureDetectorCompat;

    .line 115
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;)V

    invoke-direct {v6, v0, v1}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 138
    .local v6, "gestureDetector":Landroid/support/v4/view/GestureDetectorCompat;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$4;

    invoke-direct {v0, p0, v6}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Landroid/support/v4/view/GestureDetectorCompat;)V

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 145
    return-object v7
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 56
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->mediaItemContainer:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->mediaItemContainer:Landroid/widget/FrameLayout;

    .line 57
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)V
    .locals 4
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->mediaItemContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 64
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItemFragment;Lcom/google/apps/dots/android/newsstand/media/MediaItem;)V

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 88
    return-void
.end method
