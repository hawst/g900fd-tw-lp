.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;
.super Ljava/lang/Object;
.source "NewsWebView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/util/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IsTouchNavigationAllowedProvider"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/bind/util/Provider",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;

    .prologue
    .line 713
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->access$600(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 719
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 713
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
