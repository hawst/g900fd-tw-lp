.class public Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
.super Landroid/widget/FrameLayout;
.source "ImageRotatorView.java"


# static fields
.field private static final DECODE_OPTIONS:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

.field public static final DK_ANIMATE_IMAGE:I

.field public static final DK_IMAGE_ID:I

.field private static final FADE_INTERVAL_MS:I = 0x3e8

.field private static final IMAGE_TRANSFORM:Lcom/google/apps/dots/android/newsstand/server/Transform;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final REFRESH_INTERVAL_MS:I = 0x2710


# instance fields
.field private activeIndex:I

.field private afterRefreshRunnable:Ljava/lang/Runnable;

.field private attached:Z

.field private colorFilter:Landroid/graphics/ColorFilter;

.field private final hideInactivePageRunnable:Ljava/lang/Runnable;

.field private imageIdList:Lcom/google/android/libraries/bind/data/DataList;

.field private final imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final imageViews:[Landroid/widget/ImageView;

.field private inactiveIndex:I

.field private final kenBurns:Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;

.field private final random:Ljava/util/Random;

.field private final refreshRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private final refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private refreshWhenPossible:Z

.field private refreshing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 38
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ImageRotatorView_imageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_IMAGE_ID:I

    .line 41
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ImageRotatorView_animateImage:I

    sput v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_ANIMATE_IMAGE:I

    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;->newBuilder()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->bitmapConfig(Landroid/graphics/Bitmap$Config;)Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/DecodeOptions$Builder;->build()Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DECODE_OPTIONS:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    .line 48
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getSmallerDisplayDimension()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->square(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->IMAGE_TRANSFORM:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 55
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->random:Ljava/util/Random;

    .line 65
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->activeIndex:I

    .line 66
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->inactiveIndex:I

    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;-><init>()V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->setDuration(J)Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->kenBurns:Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;

    .line 80
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->hideInactivePageRunnable:Ljava/lang/Runnable;

    .line 86
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 95
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->inactiveIndex:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->scheduleImageRefresh()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshWhenPossible:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshWhenPossible:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->swapImage(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method private scheduleImageRefresh()V
    .locals 4

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v2, 0x2710

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 191
    return-void
.end method

.method private swapImage(Landroid/graphics/Bitmap;Z)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "animateImage"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 195
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "swapImage"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->activeIndex:I

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->inactiveIndex:I

    .line 197
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->activeIndex:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->activeIndex:I

    .line 198
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->activeIndex:I

    aget-object v0, v2, v3

    .line 199
    .local v0, "activeImage":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->inactiveIndex:I

    aget-object v1, v2, v3

    .line 202
    .local v1, "inactiveImage":Landroid/widget/ImageView;
    if-nez p1, :cond_0

    .line 203
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "swapImage - no bitmap"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 209
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->cancelFade(Landroid/view/View;)V

    .line 210
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 211
    if-eqz p2, :cond_1

    .line 212
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 213
    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    .line 214
    const/16 v2, 0x3e8

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->hideInactivePageRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 215
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->kenBurns:Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/animation/KenBurnsAnimation;->animate(Landroid/view/View;)V

    .line 221
    :goto_1
    return-void

    .line 206
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 217
    :cond_1
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 219
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->clearImageViewPropertyAnimation(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method public getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->colorFilter:Landroid/graphics/ColorFilter;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->attached:Z

    .line 227
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 230
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 235
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->attached:Z

    .line 237
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 240
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    const/4 v2, 0x0

    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->image_0:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 186
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    const/4 v2, 0x1

    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->image_1:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 187
    return-void
.end method

.method public refreshNow()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 142
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 143
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 144
    :cond_0
    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "refreshNow() - no data yet"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iput-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshWhenPossible:Z

    .line 171
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->random:Ljava/util/Random;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 148
    .local v2, "row":I
    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "refreshNow() - Picked row %d of %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 150
    .local v3, "rowData":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_IMAGE_ID:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 151
    .local v1, "imageId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget v4, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DK_ANIMATE_IMAGE:I

    invoke-virtual {v3, v4, v8}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    .line 153
    .local v0, "animateImage":Z
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 154
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->IMAGE_TRANSFORM:Lcom/google/apps/dots/android/newsstand/server/Transform;

    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->DECODE_OPTIONS:Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;

    .line 153
    invoke-virtual {v5, v6, v1, v7, v8}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;Lcom/google/apps/dots/android/newsstand/store/DecodeOptions;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;

    invoke-direct {v6, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Z)V

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public runAfterNextRefresh(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->afterRefreshRunnable:Ljava/lang/Runnable;

    .line 139
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->colorFilter:Landroid/graphics/ColorFilter;

    .line 179
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageViews:[Landroid/widget/ImageView;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 181
    return-void
.end method

.method public setImageIdList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 2
    .param p1, "imageIdList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->attached:Z

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdList:Lcom/google/android/libraries/bind/data/DataList;

    .line 111
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->attached:Z

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->imageIdObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 113
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    if-eqz v0, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshNow()V

    .line 117
    :cond_1
    return-void
.end method

.method public startRefresh()V
    .locals 3

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 132
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshNow()V

    .line 133
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Starting refresh"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    :cond_0
    return-void
.end method

.method public stopRefresh()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    if-eqz v0, :cond_0

    .line 121
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshing:Z

    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 124
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Stopping refresh"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    :cond_0
    return-void
.end method
