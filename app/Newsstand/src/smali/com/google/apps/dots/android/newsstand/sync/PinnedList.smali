.class public Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
.super Lcom/google/apps/dots/android/newsstand/data/EventDataList;
.source "PinnedList.java"


# static fields
.field public static final DK_EDITION:I

.field public static final DK_LAST_SYNCED:I

.field public static final DK_LAST_SYNC_STARTED:I

.field public static final DK_SYNC_FAILED:I

.field public static final DK_SYNC_PROGRESS:I


# instance fields
.field private final account:Landroid/accounts/Account;

.field private final pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

.field private prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->PinnedList_edition:I

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    .line 32
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->PinnedList_lastSynced:I

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNCED:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->PinnedList_lastSyncStarted:I

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNC_STARTED:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->PinnedList_syncProgress:I

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_PROGRESS:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->PinnedList_syncFailed:I

    sput v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_FAILED:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Pinner;)V
    .locals 1
    .param p1, "pinner"    # Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .prologue
    .line 41
    sget v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_EDITION:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;-><init>(I)V

    .line 42
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->account:Landroid/accounts/Account;

    .line 44
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$SyncerServiceUris;->contentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->addEventUriToWatch(Landroid/net/Uri;)V

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->setDirty(Z)V

    .line 46
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->account:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)Lcom/google/apps/dots/android/newsstand/sync/Pinner;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->pinner:Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    return-object v0
.end method


# virtual methods
.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$2;-><init>(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 5

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onRegisterForInvalidation()V

    .line 54
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList$1;-><init>(Lcom/google/apps/dots/android/newsstand/sync/PinnedList;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pinnedAccounts"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 60
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onUnregisterForInvalidation()V

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->setDirty(Z)V

    .line 67
    return-void
.end method
