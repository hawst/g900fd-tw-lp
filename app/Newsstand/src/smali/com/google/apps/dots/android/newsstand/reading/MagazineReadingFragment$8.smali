.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "MagazineReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeConfigureOrientationPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 743
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 5
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 746
    const-string v4, "landscapeBody"

    .line 747
    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findItemFromFieldId(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 748
    .local v0, "hasLandscapeBody":Z
    :goto_0
    const-string v4, "portraitBody"

    .line 749
    invoke-static {p1, v4}, Lcom/google/apps/dots/android/newsstand/model/PostUtil;->findItemFromFieldId(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;

    move-result-object v4

    if-eqz v4, :cond_2

    move v1, v2

    .line 750
    .local v1, "hasPortraitBody":Z
    :goto_1
    if-eq v0, v1, :cond_0

    .line 751
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->configureOrientationPreference(ZZ)V

    .line 754
    :cond_0
    return-void

    .end local v0    # "hasLandscapeBody":Z
    .end local v1    # "hasPortraitBody":Z
    :cond_1
    move v0, v3

    .line 747
    goto :goto_0

    .restart local v0    # "hasLandscapeBody":Z
    :cond_2
    move v1, v3

    .line 749
    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 743
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$8;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
