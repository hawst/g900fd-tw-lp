.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "ArchiveFilter.java"


# instance fields
.field private final keepingArchivedIssues:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "keepingArchivedIssues"    # Z

    .prologue
    .line 18
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 19
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;->keepingArchivedIssues:Z

    .line 20
    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v0, 0x0

    .line 24
    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/ArchiveFilter;->keepingArchivedIssues:Z

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
