.class Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "PlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->updateActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 2
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 228
    if-nez p1, :cond_0

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->setActionBarTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 225
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
