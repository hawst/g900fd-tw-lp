.class final Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "NewsSubscriptionsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->fillInData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 262
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;

    const-string v2, "MyNews"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;->track(Z)V

    .line 265
    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_icon:I

    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    .line 266
    .local v0, "icon":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    :cond_0
    const-class v1, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->source_icon_no_image:I

    invoke-static {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;

    move-result-object v0

    .line 269
    :cond_1
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v1, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$4;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v1

    .line 270
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v1

    .line 271
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 272
    return-void
.end method
