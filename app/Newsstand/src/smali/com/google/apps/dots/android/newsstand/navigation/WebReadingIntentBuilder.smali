.class public Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "WebReadingIntentBuilder.java"


# instance fields
.field private postTitle:Ljava/lang/String;

.field private postUrl:Ljava/lang/String;

.field private publisher:Ljava/lang/String;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 26
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 59
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postTitle:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->publisher:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-class v2, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->publisher:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .local v1, "state":Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;
    const-string v2, "NewsArticlePagerFragment_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 68
    return-object v0
.end method

.method public setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
    .locals 0
    .param p1, "postTitle"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postTitle:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method public setPostUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
    .locals 0
    .param p1, "postUrl"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->postUrl:Ljava/lang/String;

    .line 34
    return-object p0
.end method

.method public setPublisher(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
    .locals 0
    .param p1, "publisher"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->publisher:Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public setReadingEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 44
    return-object p0
.end method

.method public setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;
    .locals 2
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->activity:Landroid/app/Activity;

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->reading_activity_hero:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/WebReadingIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 54
    return-object p0
.end method
