.class public interface abstract Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
.super Ljava/lang/Object;
.source "HttpContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Handle"
.end annotation


# virtual methods
.method public abstract getContentBaseUri()Landroid/net/Uri;
.end method

.method public abstract getExportedContentBaseUri()Landroid/net/Uri;
.end method

.method public abstract getWebDataBaseUri()Landroid/net/Uri;
.end method

.method public abstract unbind()V
.end method
