.class final Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "RefreshUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->refreshIfNeeded(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$collectionUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$collectionUri:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V
    .locals 9
    .param p1, "response"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 79
    if-nez p1, :cond_0

    .line 80
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$collectionUri:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 93
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->storeResponse:Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobMetadata:Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    iget-wide v4, v4, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->writeTime:J

    sub-long v0, v2, v4

    .line 85
    .local v0, "writeAge":J
    const-wide/32 v2, 0x1b7740

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 86
    # getter for: Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Autorefreshing collection: %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$collectionUri:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$collectionUri:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getFresh(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 89
    :cond_1
    # getter for: Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v2

    const-string v3, "Skipped autorefresh of collection: %s, writeAge: %d seconds"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->val$collectionUri:Ljava/lang/String;

    aput-object v5, v4, v6

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    .line 90
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    .line 89
    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/util/RefreshUtil$2;->onSuccess(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)V

    return-void
.end method
