.class public final Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CacheVersion"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;


# instance fields
.field private attachmentVersion_:I

.field private bitField0_:I

.field private blobVersion_:I

.field private collectionVersion_:I

.field private uriVersion_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28678
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28679
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 28684
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    .line 28703
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    .line 28722
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    .line 28741
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    .line 28679
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    .locals 3

    .prologue
    .line 28773
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28777
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    return-object v0

    .line 28774
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    :catch_0
    move-exception v1

    .line 28775
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28675
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28782
    if-ne p1, p0, :cond_1

    .line 28785
    :cond_0
    :goto_0
    return v1

    .line 28783
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 28784
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    .line 28785
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getAttachmentVersion()I
    .locals 1

    .prologue
    .line 28724
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    return v0
.end method

.method public getBlobVersion()I
    .locals 1

    .prologue
    .line 28705
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    return v0
.end method

.method public getCollectionVersion()I
    .locals 1

    .prologue
    .line 28686
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 28819
    const/4 v0, 0x0

    .line 28820
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 28821
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    .line 28822
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28824
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 28825
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    .line 28826
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28828
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 28829
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    .line 28830
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28832
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 28833
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    .line 28834
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28836
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->cachedSize:I

    .line 28837
    return v0
.end method

.method public getUriVersion()I
    .locals 1

    .prologue
    .line 28743
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 28792
    const/16 v0, 0x11

    .line 28793
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 28794
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    add-int v0, v1, v2

    .line 28795
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    add-int v0, v1, v2

    .line 28796
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    add-int v0, v1, v2

    .line 28797
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    add-int v0, v1, v2

    .line 28798
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28845
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 28846
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 28850
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 28851
    :sswitch_0
    return-object p0

    .line 28856
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    .line 28857
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    goto :goto_0

    .line 28861
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    .line 28862
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    goto :goto_0

    .line 28866
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    .line 28867
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    goto :goto_0

    .line 28871
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    .line 28872
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    goto :goto_0

    .line 28846
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28675
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28803
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 28804
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->collectionVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28806
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 28807
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->blobVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28809
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 28810
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->attachmentVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28812
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 28813
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->uriVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 28815
    :cond_3
    return-void
.end method
