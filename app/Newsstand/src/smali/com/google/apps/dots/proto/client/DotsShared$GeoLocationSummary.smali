.class public final Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GeoLocationSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;


# instance fields
.field private bitField0_:I

.field private center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

.field public clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

.field private storiesCount_:I

.field private zoom_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24559
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24560
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 24584
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24587
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    .line 24606
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    .line 24560
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    .locals 5

    .prologue
    .line 24638
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24642
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v3, :cond_0

    .line 24643
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 24645
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 24646
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24647
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 24648
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 24649
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    aput-object v4, v3, v2

    .line 24647
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 24639
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 24640
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 24653
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    :cond_2
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24556
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24658
    if-ne p1, p0, :cond_1

    .line 24662
    :cond_0
    :goto_0
    return v1

    .line 24659
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 24660
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    .line 24661
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24662
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 24661
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 24704
    const/4 v1, 0x0

    .line 24705
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v2, :cond_0

    .line 24706
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 24707
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24709
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_2

    .line 24710
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 24711
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_1

    .line 24712
    const/4 v5, 0x2

    .line 24713
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 24710
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 24717
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 24718
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    .line 24719
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 24721
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 24722
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    .line 24723
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 24725
    :cond_4
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->cachedSize:I

    .line 24726
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 24668
    const/16 v1, 0x11

    .line 24669
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 24670
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 24671
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 24677
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    add-int v1, v2, v3

    .line 24678
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    add-int v1, v2, v3

    .line 24679
    return v1

    .line 24670
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->hashCode()I

    move-result v2

    goto :goto_0

    .line 24673
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 24674
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 24673
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 24674
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 24734
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 24735
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 24739
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 24740
    :sswitch_0
    return-object p0

    .line 24745
    :sswitch_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v5, :cond_1

    .line 24746
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 24748
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 24752
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 24753
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v5, :cond_3

    move v1, v4

    .line 24754
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24755
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v5, :cond_2

    .line 24756
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24758
    :cond_2
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 24759
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 24760
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 24761
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 24762
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 24759
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 24753
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v1, v5

    goto :goto_1

    .line 24765
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 24766
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 24770
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    .line 24771
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    goto :goto_0

    .line 24775
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    .line 24776
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 24735
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24556
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24684
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v1, :cond_0

    .line 24685
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->center_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24687
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v1, :cond_2

    .line 24688
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clientEntity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 24689
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_1

    .line 24690
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 24688
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 24694
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 24695
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->zoom_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 24697
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 24698
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->storiesCount_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 24700
    :cond_4
    return-void
.end method
