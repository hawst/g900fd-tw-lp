.class public Lcom/google/apps/dots/android/newsstand/model/ItemUtil;
.super Ljava/lang/Object;
.source "ItemUtil.java"


# direct methods
.method private static addAudioIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;)V
    .locals 2
    .param p1, "audio"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 85
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static addImageIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V
    .locals 4
    .param p1, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 63
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v1

    .line 65
    .local v1, "height":I
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v2

    .line 66
    .local v2, "width":I
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    if-lez v1, :cond_0

    if-lez v2, :cond_0

    .line 67
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "height":I
    .end local v2    # "width":I
    :cond_0
    return-void
.end method

.method private static addInlineFrameIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;)V
    .locals 5
    .param p1, "inlineFrame"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 103
    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->resource:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 104
    .local v0, "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;->getAttachmentId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "resource":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame$Resource;
    :cond_0
    return-void
.end method

.method private static addPdfIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;)V
    .locals 4
    .param p1, "pdf"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->getHeight()I

    move-result v1

    .line 76
    .local v1, "height":I
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->getWidth()I

    move-result v2

    .line 77
    .local v2, "width":I
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    if-lez v1, :cond_0

    if-lez v2, :cond_0

    .line 78
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "height":I
    .end local v2    # "width":I
    :cond_0
    return-void
.end method

.method private static addStreamingVideoIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;)V
    .locals 2
    .param p1, "video"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 93
    .local p0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 94
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getAttachmentId()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "attachmentId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static getAudio(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 200
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 201
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getImage(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .param p1, "valueIndex"    # I

    .prologue
    .line 180
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 181
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInlineFrame(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 196
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasInlineFrame()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getInlineFrame()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getItem(Lcom/google/apps/dots/proto/client/DotsShared$Post;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .locals 5
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 37
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 41
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :goto_1
    return-object v0

    .line 36
    .restart local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getItemType(Lcom/google/apps/dots/proto/client/DotsShared$Item;)I
    .locals 1
    .param p0, "item"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->hasSafeType()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item;->getSafeType()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->type:I

    goto :goto_0
.end method

.method public static getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 1
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    return-object v0
.end method

.method public static getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;I)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 1
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .param p1, "valueIndex"    # I

    .prologue
    .line 48
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    aget-object v0, v0, p1

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getNativeBodies(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 175
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 176
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasNativeBodies()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getNativeBodies()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getPdf(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 185
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 186
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasPdf()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getPdf()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getPostAttachmentIds(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/List;
    .locals 17
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 115
    .local v2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasAuthor()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 116
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->hasThumbnail()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addImageIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    .line 120
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasFavicon()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFavicon()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addImageIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    .line 124
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v13, v12

    const/4 v10, 0x0

    move v11, v10

    :goto_0
    if-ge v11, v13, :cond_b

    aget-object v5, v12, v11

    .line 125
    .local v5, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemType(Lcom/google/apps/dots/proto/client/DotsShared$Item;)I

    move-result v6

    .line 126
    .local v6, "itemType":I
    const/4 v10, 0x4

    if-eq v6, v10, :cond_2

    const/16 v10, 0x9

    if-eq v6, v10, :cond_2

    const/4 v10, 0x1

    if-eq v6, v10, :cond_2

    const/4 v10, 0x5

    if-eq v6, v10, :cond_2

    const/16 v10, 0xf

    if-eq v6, v10, :cond_2

    const/16 v10, 0xc

    if-eq v6, v10, :cond_2

    const/16 v10, 0xd

    if-ne v6, v10, :cond_a

    .line 133
    :cond_2
    iget-object v14, v5, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v15, v14

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v15, :cond_a

    aget-object v9, v14, v10

    .line 134
    .local v9, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    const/4 v3, 0x0

    .line 135
    .local v3, "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    const/4 v7, 0x0

    .line 136
    .local v7, "pdf":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    const/4 v1, 0x0

    .line 137
    .local v1, "audio":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    const/4 v8, 0x0

    .line 138
    .local v8, "streamingVideo":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    const/4 v4, 0x0

    .line 139
    .local v4, "inlineFrame":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v16

    if-eqz v16, :cond_4

    .line 140
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    .line 155
    :cond_3
    :goto_2
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addImageIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)V

    .line 156
    invoke-static {v2, v7}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addPdfIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;)V

    .line 157
    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addAudioIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;)V

    .line 158
    invoke-static {v2, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addStreamingVideoIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;)V

    .line 159
    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->addInlineFrameIfUsableHelper(Ljava/util/List;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;)V

    .line 133
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 141
    :cond_4
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v16

    if-eqz v16, :cond_5

    .line 142
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    goto :goto_2

    .line 143
    :cond_5
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v16

    if-eqz v16, :cond_6

    .line 144
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    goto :goto_2

    .line 146
    :cond_6
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasLocation()Z

    move-result v16

    if-eqz v16, :cond_7

    .line 147
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getLocation()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v3

    goto :goto_2

    .line 148
    :cond_7
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasPdf()Z

    move-result v16

    if-eqz v16, :cond_8

    .line 149
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getPdf()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    move-result-object v7

    goto :goto_2

    .line 150
    :cond_8
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v16

    if-eqz v16, :cond_9

    .line 151
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v8

    goto :goto_2

    .line 152
    :cond_9
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasInlineFrame()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 153
    invoke-virtual {v9}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getInlineFrame()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v4

    goto :goto_2

    .line 124
    .end local v1    # "audio":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .end local v3    # "image":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .end local v4    # "inlineFrame":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .end local v7    # "pdf":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    .end local v8    # "streamingVideo":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .end local v9    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :cond_a
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto/16 :goto_0

    .line 163
    .end local v5    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v6    # "itemType":I
    :cond_b
    return-object v2
.end method

.method public static getStreamingVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 190
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 191
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getUrlHref(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Ljava/lang/String;
    .locals 2
    .param p0, "optItem"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 228
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemValue(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    .line 229
    .local v0, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getUrl()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->getHref()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static indexPostByField(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/util/Map;
    .locals 6
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v2, v2

    invoke-static {v2}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v1

    .line 168
    .local v1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 169
    .local v0, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    iget-object v5, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    :cond_0
    return-object v1
.end method
