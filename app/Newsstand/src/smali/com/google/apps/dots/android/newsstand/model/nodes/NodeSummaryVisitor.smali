.class public abstract Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;
.super Ljava/lang/Object;
.source "NodeSummaryVisitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeTraversalT::",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor",
        "<TNodeTraversalT;>;"
    }
.end annotation


# instance fields
.field private final ancestors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    return-void
.end method

.method private currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v1, 0x0

    .line 42
    .local v1, "summary":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-nez v1, :cond_0

    if-ltz v0, :cond_0

    .line 43
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/Nodes;->getSummary(Ljava/lang/Class;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    .line 42
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 45
    :cond_0
    return-object v1
.end method


# virtual methods
.method protected final currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1

    .prologue
    .line 62
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    return-object v0
.end method

.method protected final currentAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    return-object v0
.end method

.method protected final currentCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    .locals 1

    .prologue
    .line 57
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    return-object v0
.end method

.method protected final currentEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    .locals 1

    .prologue
    .line 117
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    return-object v0
.end method

.method protected final currentExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    .locals 1

    .prologue
    .line 92
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    return-object v0
.end method

.method protected final currentNode()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 2

    .prologue
    .line 52
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    return-object v0
.end method

.method protected final currentPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object v0
.end method

.method protected final currentSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    return-object v0
.end method

.method protected final currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    .locals 1

    .prologue
    .line 102
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    const-class v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->currentSummary(Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    return-object v0
.end method

.method public exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 2
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")V"
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 182
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 0
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V
    .locals 0
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;)V
    .locals 0
    .param p2, "clusterSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 238
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;)V
    .locals 0
    .param p2, "collectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 190
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;)V
    .locals 0
    .param p2, "exploreGroupSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 254
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V
    .locals 0
    .param p2, "exploreLinkSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 262
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;)V
    .locals 0
    .param p2, "geoLocationSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 246
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)V
    .locals 0
    .param p2, "shelfSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 270
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 278
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
    .locals 0
    .param p2, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;)V
    .locals 0
    .param p2, "purchaseSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 294
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;)V
    .locals 0
    .param p2, "editionShelfSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V
    .locals 0
    .param p2, "sectionSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V
    .locals 0
    .param p2, "webPageSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;",
            ")V"
        }
    .end annotation

    .prologue
    .line 230
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    return-void
.end method

.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 1
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;, "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor<TNodeTraversalT;>;"
    .local p1, "traversal":Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;, "TNodeTraversalT;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->ancestors:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasCollectionSummary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;)V

    .line 132
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppFamilySummary()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    .line 135
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)V

    .line 138
    :cond_2
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasSectionSummary()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 139
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;)V

    .line 141
    :cond_3
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasPostSummary()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 142
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    .line 144
    :cond_4
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasWebPageSummary()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 145
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getWebPageSummary()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V

    .line 147
    :cond_5
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasClusterSummary()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 148
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getClusterSummary()Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;)V

    .line 150
    :cond_6
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasGeoLocationSummary()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 151
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getGeoLocationSummary()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;)V

    .line 153
    :cond_7
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasExploreGroupSummary()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 154
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;)V

    .line 156
    :cond_8
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasExploreLinkSummary()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 157
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getExploreLinkSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V

    .line 159
    :cond_9
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasMerchandisingShelfSummary()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 160
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getMerchandisingShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)V

    .line 162
    :cond_a
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasOfferSummary()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 163
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    .line 165
    :cond_b
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasReadNowEditionShelfSummary()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 166
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getReadNowEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;)V

    .line 168
    :cond_c
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasPurchaseSummary()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 169
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPurchaseSummary()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeSummaryVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;)V

    .line 171
    :cond_d
    return-void
.end method
