.class public abstract Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomAwareOnDoubleTapListener.java"


# instance fields
.field private downX:F

.field private downY:F

.field private realDoubleTap:Z

.field final touchSlopSquared:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 20
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->touchSlopSquared:F

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract handleOnDoubleTapUp(Landroid/view/MotionEvent;)Z
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public final onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 35
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 36
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 54
    :cond_0
    :goto_0
    return v2

    .line 38
    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->realDoubleTap:Z

    .line 39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->downX:F

    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->downY:F

    goto :goto_0

    .line 44
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->downX:F

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->downY:F

    sub-float/2addr v3, v6

    float-to-double v6, v3

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    double-to-float v1, v4

    .line 45
    .local v1, "distanceSquared":F
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->touchSlopSquared:F

    cmpl-float v3, v1, v3

    if-lez v3, :cond_0

    .line 46
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->realDoubleTap:Z

    goto :goto_0

    .line 50
    .end local v1    # "distanceSquared":F
    :pswitch_2
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->realDoubleTap:Z

    if-eqz v3, :cond_0

    .line 51
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ZoomAwareOnDoubleTapListener;->handleOnDoubleTapUp(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
