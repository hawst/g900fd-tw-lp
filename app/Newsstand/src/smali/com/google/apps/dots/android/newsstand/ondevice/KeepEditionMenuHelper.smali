.class public Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;
.super Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;
.source "KeepEditionMenuHelper.java"


# static fields
.field private static final DK_EDITION_PINNED:I


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->KeepEditionMenuHelper_editionPinned:I

    sput v0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->DK_EDITION_PINNED:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/DataMenuHelper;-><init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 42
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->DK_EDITION_PINNED:I

    return v0
.end method


# virtual methods
.method public filterRow(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 9
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 45
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    if-eqz v0, :cond_0

    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->magazineSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/magazines/MagazinesSubscriptionsList;

    move-result-object v6

    .line 48
    .local v6, "subscriptionList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 62
    .local v3, "filter":Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/MergeList;

    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    const/4 v2, 0x0

    check-cast v2, [I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/libraries/bind/data/DataList;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const/4 v7, 0x1

    .line 67
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/data/MergeList;-><init>(I[ILcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;Lcom/google/android/libraries/bind/async/Queue;[Lcom/google/android/libraries/bind/data/DataList;)V

    return-object v0

    .line 47
    .end local v3    # "filter":Lcom/google/apps/dots/android/newsstand/data/MergeList$MergeFilter;
    .end local v6    # "subscriptionList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v6

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 85
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 101
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v4, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->menu_keep_on_device:I

    if-eq v4, v5, :cond_2

    :cond_0
    move v3, v2

    .line 127
    :cond_1
    :goto_0
    return v3

    .line 104
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 105
    .local v0, "editionSummaryToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v4

    if-nez v4, :cond_3

    move v2, v3

    :cond_3
    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 106
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v2, :cond_1

    .line 107
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 108
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v1

    .line 109
    .local v1, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 110
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v4, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;

    invoke-direct {v4, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper$2;-><init>(Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;)V

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 120
    :cond_4
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->pinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 121
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_0

    .line 123
    .end local v1    # "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    :cond_5
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v2, v4}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->unpinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 89
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 90
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-nez v2, :cond_0

    .line 91
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_keep_on_device:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 97
    :goto_0
    return-void

    .line 94
    :cond_0
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_keep_on_device:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 95
    .local v1, "menuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    move v2, v3

    :goto_1
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 96
    if-eqz v0, :cond_2

    sget v2, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->DK_EDITION_PINNED:I

    invoke-virtual {v0, v2, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    move v2, v4

    .line 95
    goto :goto_1

    :cond_2
    move v3, v4

    .line 96
    goto :goto_2
.end method

.method public setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v1, 0x0

    .line 71
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v0, :cond_0

    .line 73
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local p1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 81
    :goto_0
    return-void

    .line 74
    .restart local p1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->showKeepOnDeviceUi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 76
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->filterRow(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_0

    .line 78
    :cond_1
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 79
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/ondevice/KeepEditionMenuHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_0
.end method
