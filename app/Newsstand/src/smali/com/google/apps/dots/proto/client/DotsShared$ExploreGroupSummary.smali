.class public final Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExploreGroupSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;


# instance fields
.field private bitField0_:I

.field private iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private id_:Ljava/lang/String;

.field private title_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25046
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25047
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25052
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    .line 25074
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    .line 25047
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    .locals 3

    .prologue
    .line 25127
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25131
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 25132
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 25134
    :cond_0
    return-object v0

    .line 25128
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    :catch_0
    move-exception v1

    .line 25129
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25043
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25139
    if-ne p1, p0, :cond_1

    .line 25144
    :cond_0
    :goto_0
    return v1

    .line 25140
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 25141
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .line 25142
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 25143
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 25144
    goto :goto_0

    .line 25142
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    .line 25143
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 25144
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 25098
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25054
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 25171
    const/4 v0, 0x0

    .line 25172
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 25173
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    .line 25174
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25176
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 25177
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    .line 25178
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25180
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_2

    .line 25181
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 25182
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25184
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->cachedSize:I

    .line 25185
    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25076
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasIconImage()Z
    .locals 1

    .prologue
    .line 25108
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 25065
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 25148
    const/16 v0, 0x11

    .line 25149
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 25150
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 25151
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 25152
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 25153
    return v0

    .line 25150
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 25151
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 25152
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 25194
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 25198
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 25199
    :sswitch_0
    return-object p0

    .line 25204
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    .line 25205
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    goto :goto_0

    .line 25209
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    .line 25210
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    goto :goto_0

    .line 25214
    :sswitch_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_1

    .line 25215
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 25217
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 25194
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25043
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25158
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 25159
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->id_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 25161
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 25162
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 25164
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_2

    .line 25165
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 25167
    :cond_2
    return-void
.end method
