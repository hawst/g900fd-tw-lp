.class public Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;
.super Ljava/lang/Object;
.source "ShareMenuHelper.java"


# instance fields
.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

.field private fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

.field private shareParams:Lcom/google/apps/dots/android/newsstand/share/ShareParams;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 22
    return-void
.end method


# virtual methods
.method public onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 41
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->menu_share:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 43
    .local v0, "shareMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 44
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->shareParams:Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 45
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->MAGAZINE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-eq v2, v3, :cond_1

    const/4 v1, 0x1

    .line 46
    .local v1, "showShareMenu":Z
    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 48
    if-eqz v1, :cond_0

    .line 49
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .line 50
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->shareParams:Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .line 49
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/share/ShareUtil;->getShareIntentBuilder(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/share/ShareParams;)Landroid/support/v4/app/ShareCompat$IntentBuilder;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Landroid/support/v4/app/ShareCompat$IntentBuilder;->createChooserIntent()Landroid/content/Intent;

    move-result-object v2

    .line 49
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 54
    .end local v1    # "showShareMenu":Z
    :cond_0
    return-void

    .line 45
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 49
    .restart local v1    # "showShareMenu":Z
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 50
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    goto :goto_1
.end method

.method public setShareParams(Lcom/google/apps/dots/android/newsstand/share/ShareParams;)V
    .locals 1
    .param p1, "shareParams"    # Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->shareParams:Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    if-eq v0, p1, :cond_1

    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->shareParams:Lcom/google/apps/dots/android/newsstand/share/ShareParams;

    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->fragment:Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->invalidateOptionsMenu()V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/share/ShareMenuHelper;->activity:Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->supportInvalidateOptionsMenu()V

    .line 38
    :cond_1
    return-void
.end method
