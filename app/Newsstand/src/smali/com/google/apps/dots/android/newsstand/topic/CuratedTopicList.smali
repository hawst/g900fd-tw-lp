.class public Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.source "CuratedTopicList.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList$1;-><init>(Lcom/google/apps/dots/android/newsstand/topic/CuratedTopicList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
