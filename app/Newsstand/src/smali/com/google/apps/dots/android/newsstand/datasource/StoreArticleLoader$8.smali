.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$8;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadIdToAdTemplateMap(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$8;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 277
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$8;->apply(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/util/List;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "formTemplates":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 281
    .local v1, "idToTemplate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    .line 283
    .local v0, "formTemplate":Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->getTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getTemplateForDevice(Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v2

    .line 284
    .local v2, "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->getFormTemplateId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 286
    .end local v0    # "formTemplate":Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    .end local v2    # "template":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :cond_0
    return-object v1
.end method
