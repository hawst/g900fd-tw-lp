.class public Lcom/google/apps/dots/android/newsstand/media/AudioItem;
.super Lcom/google/apps/dots/android/newsstand/media/MediaItem;
.source "AudioItem.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V
    .locals 1
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "fieldId"    # Ljava/lang/String;
    .param p4, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/media/MediaItem;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V

    .line 15
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 16
    return-void
.end method


# virtual methods
.method public toJson(Ljava/lang/String;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "audioUri"    # Ljava/lang/String;

    .prologue
    .line 19
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 20
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "postId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    const-string v1, "fieldId"

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v1, "offset"

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->offset:I

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 23
    const-string v1, "audioUri"

    invoke-virtual {v0, v1, p1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-object v0
.end method
