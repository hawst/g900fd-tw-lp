.class public Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;
.super Ljava/lang/Object;
.source "AuthHelper.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final accountManager:Landroid/accounts/AccountManager;

.field private final appContext:Landroid/content/Context;

.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountManager"    # Landroid/accounts/AccountManager;
    .param p3, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountManager:Landroid/accounts/AccountManager;

    .line 49
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->appContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public static accountFromName(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static accountHash(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 58
    if-nez p0, :cond_0

    .line 59
    const-string v2, "shared"

    .line 63
    :goto_0
    return-object v2

    .line 61
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getOriginalName(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "originalName":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/hash/Hashing;->md5()Lcom/google/common/hash/HashFunction;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/common/hash/HashFunction;->newHasher()Lcom/google/common/hash/Hasher;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/common/hash/Hasher;->putString(Ljava/lang/CharSequence;)Lcom/google/common/hash/Hasher;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/common/hash/Hasher;->hash()Lcom/google/common/hash/HashCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/hash/HashCode;->asBytes()[B

    move-result-object v0

    .line 63
    .local v0, "hashBytes":[B
    const/16 v2, 0xb

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getAuthTokenFromManager(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "getPromptIntent"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;,
            Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->installIfNeeded()V

    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountManager:Landroid/accounts/AccountManager;

    const/4 v3, 0x0

    if-nez p3, :cond_0

    const/4 v4, 0x1

    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    .line 213
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v9

    .line 218
    .local v9, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const-wide/16 v0, 0xa

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v9, v0, v1, v2}, Landroid/accounts/AccountManagerFuture;->getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Bundle;

    .line 219
    .local v11, "result":Landroid/os/Bundle;
    const-string v0, "authtoken"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 220
    .local v7, "authToken":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 221
    return-object v7

    .line 212
    .end local v7    # "authToken":Ljava/lang/String;
    .end local v9    # "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v11    # "result":Landroid/os/Bundle;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 223
    .restart local v7    # "authToken":Ljava/lang/String;
    .restart local v9    # "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .restart local v11    # "result":Landroid/os/Bundle;
    :cond_1
    const/4 v10, 0x0

    .line 224
    .local v10, "promptIntent":Landroid/content/Intent;
    if-eqz p3, :cond_2

    .line 225
    const-string v0, "intent"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "promptIntent":Landroid/content/Intent;
    check-cast v10, Landroid/content/Intent;

    .line 226
    .restart local v10    # "promptIntent":Landroid/content/Intent;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Account manager returned an intent to correct auth error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    :cond_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;

    invoke-direct {v0, p1, v10}, Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;-><init>(Landroid/accounts/Account;Landroid/content/Intent;)V

    throw v0
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 230
    .end local v7    # "authToken":Ljava/lang/String;
    .end local v10    # "promptIntent":Landroid/content/Intent;
    .end local v11    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v8

    .line 232
    .local v8, "e":Landroid/accounts/OperationCanceledException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "OperationCanceledException while retrieving auth token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v8}, Landroid/accounts/OperationCanceledException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;

    invoke-direct {v0, v8}, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 234
    .end local v8    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v8

    .line 236
    .local v8, "e":Landroid/accounts/AuthenticatorException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "AuthenticatorException while retrieving auth token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v8}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;

    invoke-direct {v0, v8}, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 238
    .end local v8    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v8

    .line 240
    .local v8, "e":Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
    throw v8

    .line 241
    .end local v8    # "e":Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
    :catch_3
    move-exception v8

    .line 243
    .local v8, "e":Ljava/io/IOException;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "IOException while retrieving auth token: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    new-instance v0, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;

    invoke-direct {v0, v8}, Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private getAuthTokenFromManager(Landroid/accounts/Account;Z)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "getPromptIntent"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;,
            Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
        }
    .end annotation

    .prologue
    .line 203
    const-string v0, "print"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthTokenFromManager(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuthToken(Landroid/accounts/Account;Z)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "getPromptIntent"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/auth/AuthTokenRetrievalException;,
            Lcom/google/apps/dots/android/newsstand/auth/InvalidAccountException;,
            Lcom/google/apps/dots/android/newsstand/exception/NoAuthTokenException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 99
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 101
    new-instance v1, Lcom/google/apps/dots/android/newsstand/auth/InvalidAccountException;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/auth/InvalidAccountException;-><init>()V

    throw v1

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->getAuthTokenFromManager(Landroid/accounts/Account;Z)Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 108
    :cond_1
    return-object v0
.end method

.method public getAuthTokenFuture(Landroid/accounts/Account;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "getPromptIntent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;Landroid/accounts/Account;Z)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/Queue;->submit(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 82
    .local v0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/String;>;"
    return-object v0
.end method

.method public hasCachedAuthToken(Landroid/accounts/Account;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initAccountIfNeeded(Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 4
    .param p1, "optAccount"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException;
        }
    .end annotation

    .prologue
    .line 147
    move-object v0, p1

    .line 148
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->authHelper()Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;

    move-result-object v1

    .line 149
    .local v1, "authHelper":Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 153
    :cond_0
    if-nez v0, :cond_1

    .line 154
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 155
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 156
    const/4 v0, 0x0

    .line 160
    :cond_1
    if-nez v0, :cond_3

    .line 161
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getAllGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 162
    .local v2, "googleAccounts":[Landroid/accounts/Account;
    array-length v3, v2

    if-nez v3, :cond_2

    .line 163
    new-instance v3, Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException;

    invoke-direct {v3}, Lcom/google/apps/dots/android/newsstand/auth/NoGoogleAccountException;-><init>()V

    throw v3

    .line 165
    :cond_2
    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 168
    .end local v2    # "googleAccounts":[Landroid/accounts/Account;
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 169
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->setAccount(Landroid/accounts/Account;)V

    .line 171
    :cond_4
    return-object v0
.end method

.method public invalidateToken(Landroid/accounts/Account;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 130
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v2, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "authToken":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 132
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 136
    .local v1, "policy":Landroid/os/StrictMode$ThreadPolicy;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountManager:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    invoke-virtual {v2, v3, v0}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 139
    .end local v1    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    :cond_0
    return-void
.end method

.method public isValidAccount(Landroid/accounts/Account;)Z
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 118
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 119
    .local v0, "policy":Landroid/os/StrictMode$ThreadPolicy;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountManager:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    .line 120
    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 121
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 122
    .local v1, "returnValue":Z
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 123
    return v1
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 178
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 181
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioUtil;->cancelAudio(Landroid/content/Context;)V

    .line 182
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 183
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setAccount(Landroid/accounts/Account;)V

    .line 184
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->invalidateToken(Landroid/accounts/Account;)V

    .line 185
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->resetAccountScope()V

    .line 186
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->reset()V

    .line 187
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getFreshConfig(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 188
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->setAccount(Landroid/accounts/Account;)V

    .line 189
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetService;->setAccount(Landroid/accounts/Account;)V

    .line 194
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->appContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/service/MagazinesUserContentService;->initMyMagazinesObserver(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/appwidget/NewsWidgetProvider;->restartAllWidgets(Landroid/content/Context;)V

    .line 198
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->appContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/Notifications;->clearAllNotifications(Landroid/content/Context;)V

    .line 199
    return-void
.end method
