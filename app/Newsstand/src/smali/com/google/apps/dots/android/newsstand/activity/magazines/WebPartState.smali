.class public Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;
.super Ljava/lang/Object;
.source "WebPartState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final appId:Ljava/lang/String;

.field public final fieldId:Ljava/lang/String;

.field public final localUrl:Ljava/lang/String;

.field public final postId:Ljava/lang/String;

.field public final sectionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "sectionId"    # Ljava/lang/String;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "fieldId"    # Ljava/lang/String;
    .param p5, "localUrl"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 31
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 32
    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    .line 33
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    .line 34
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    .line 35
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    .line 36
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    .line 37
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 39
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 49
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{%s - %s - %s --- %s - %s}"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->appId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->sectionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->postId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->fieldId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;->localUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    return-void
.end method
