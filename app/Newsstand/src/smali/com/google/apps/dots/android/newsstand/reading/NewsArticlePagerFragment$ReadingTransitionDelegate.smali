.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "NewsArticlePagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReadingTransitionDelegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$1;

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;-><init>()V

    return-void
.end method

.method private onSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 622
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->background:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 623
    return-void
.end method

.method private sharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Z)Landroid/transition/Transition;
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .param p2, "isEntering"    # Z

    .prologue
    .line 593
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->intrinsic_card_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 594
    .local v0, "inset":I
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->transitionPlaceholderView:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$600(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;

    move-result-object v1

    .line 595
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v3

    .line 594
    invoke-static {v1, v0, v2, v3, p2}, Lcom/google/apps/dots/android/newsstand/transition/CrossFadeExpandoHelper;->sharedElementEnterTransition(Landroid/view/View;IILandroid/view/View;Z)Landroid/transition/Transition;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected createEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;
    .locals 3
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 604
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->home_background_dark:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 605
    .local v0, "fadeToColor":I
    new-instance v1, Lcom/google/android/play/transition/BackgroundFade;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/google/android/play/transition/BackgroundFade;-><init>(II)V

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->background:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/transition/BackgroundFade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic createEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 578
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->createEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 583
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->sharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Z)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 578
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 588
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->sharedElementTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Z)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 578
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->createSharedElementReturnTransition(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 641
    const/4 v0, 0x2

    return v0
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 636
    const-wide/16 v0, 0x190

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 612
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->setupTransitionForSlowLoad(Landroid/view/View;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 614
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->onSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V

    .line 615
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 578
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/transition/Transition;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 628
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 630
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->background:Landroid/view/View;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Landroid/view/View;

    move-result-object v0

    .line 631
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->home_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 630
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 632
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 578
    check-cast p1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$ReadingTransitionDelegate;->onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;Landroid/transition/Transition;)V

    return-void
.end method
