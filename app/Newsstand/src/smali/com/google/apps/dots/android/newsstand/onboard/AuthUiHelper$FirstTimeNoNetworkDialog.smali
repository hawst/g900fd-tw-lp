.class public Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;
.source "AuthUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FirstTimeNoNetworkDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 542
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/ResultingDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 546
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper$FirstTimeNoNetworkDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 547
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 548
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->no_network_connection:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 549
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/VersionUtil;->isUpdatedVersion()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->start_offline_updated_version:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 552
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1

    .line 549
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->start_offline:I

    goto :goto_0
.end method
