.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "ExploreSingleTopicHeaderFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Delegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private headerParentLayoutBounds:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$1;

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;-><init>()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;J)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "x2"    # J

    .prologue
    .line 332
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->showList(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;J)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Ljava/lang/Object;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private showList(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;J)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "delay"    # J

    .prologue
    .line 478
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 495
    :cond_0
    return-void
.end method


# virtual methods
.method protected createReturnTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/transition/Transition;
    .locals 6
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 349
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->listViewBackground(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v1

    .line 350
    .local v1, "listViewBackground":Landroid/view/View;
    new-instance v3, Landroid/transition/Slide;

    invoke-direct {v3}, Landroid/transition/Slide;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->listViewBackground(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    .line 351
    .local v2, "slide":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/Explode;

    invoke-direct {v3}, Landroid/transition/Explode;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Landroid/transition/Explode;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    move-result-object v0

    .line 352
    .local v0, "explode":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/TransitionSet;

    invoke-direct {v3}, Landroid/transition/TransitionSet;-><init>()V

    .line 353
    invoke-virtual {v3, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v3

    .line 354
    invoke-virtual {v3, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v3

    const-wide/16 v4, 0xfa

    .line 355
    invoke-virtual {v3, v4, v5}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v3

    return-object v3
.end method

.method protected bridge synthetic createReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->createReturnTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected createSharedElementEnterTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/transition/TransitionSet;
    .locals 12
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 361
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v5

    .line 362
    .local v5, "interpolator":Landroid/view/animation/Interpolator;
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->headerParent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v2

    .line 363
    .local v2, "headerParent":Landroid/view/View;
    new-instance v6, Landroid/transition/ChangeBounds;

    invoke-direct {v6}, Landroid/transition/ChangeBounds;-><init>()V

    .line 364
    invoke-virtual {v6, v2}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    .line 365
    .local v3, "headerParentChangeBounds":Landroid/transition/Transition;
    new-instance v6, Landroid/transition/ArcMotion;

    invoke-direct {v6}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v3, v6}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 368
    const/high16 v6, 0x41a00000    # 20.0f

    invoke-static {v6, v10}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;->elevation(FZ)Landroid/transition/Transition;

    move-result-object v6

    .line 369
    invoke-virtual {v6, v2}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 370
    .local v1, "elevation":Landroid/transition/Transition;
    sget-object v6, Landroid/view/ViewOutlineProvider;->BOUNDS:Landroid/view/ViewOutlineProvider;

    invoke-virtual {v2, v6}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 371
    invoke-virtual {v2, v9}, Landroid/view/View;->setClipToOutline(Z)V

    .line 375
    new-instance v6, Lcom/google/android/play/transition/CircularReveal;

    invoke-direct {v6, v9}, Lcom/google/android/play/transition/CircularReveal;-><init>(I)V

    const/4 v7, 0x0

    const v8, 0x3eb33333    # 0.35f

    .line 376
    invoke-virtual {v6, v7, v8}, Lcom/google/android/play/transition/CircularReveal;->addKeyFrame(FF)Lcom/google/android/play/transition/CircularReveal;

    move-result-object v6

    const/high16 v7, 0x3f000000    # 0.5f

    const v8, 0x3e4ccccd    # 0.2f

    .line 377
    invoke-virtual {v6, v7, v8, v5}, Lcom/google/android/play/transition/CircularReveal;->addKeyFrame(FFLandroid/animation/TimeInterpolator;)Lcom/google/android/play/transition/CircularReveal;

    move-result-object v6

    .line 378
    invoke-virtual {v6, v11, v11, v5}, Lcom/google/android/play/transition/CircularReveal;->addKeyFrame(FFLandroid/animation/TimeInterpolator;)Lcom/google/android/play/transition/CircularReveal;

    move-result-object v6

    .line 379
    invoke-virtual {v6, v2}, Lcom/google/android/play/transition/CircularReveal;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v4

    .line 384
    .local v4, "headerParentReveal":Landroid/transition/Transition;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 383
    invoke-static {v10, v6, v9}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions;->colorFilter(IIZ)Landroid/transition/Transition;

    move-result-object v6

    .line 385
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->dummyHeaderImage(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 387
    .local v0, "colorFilter":Landroid/transition/Transition;
    const-wide/16 v6, 0x1c2

    const/4 v8, 0x4

    new-array v8, v8, [Landroid/transition/Transition;

    aput-object v3, v8, v10

    aput-object v4, v8, v9

    const/4 v9, 0x2

    aput-object v1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->aggregate(Landroid/view/animation/Interpolator;J[Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v6

    return-object v6
.end method

.method protected dummyHeaderImage(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 505
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->transitionDummyImage:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method protected getControllerType()I
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x2

    return v0
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 517
    const-wide/16 v0, 0xfa

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;Ljava/util/Map;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    const/4 v3, 0x4

    .line 396
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->isTransitioningToExit()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 398
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 399
    invoke-interface {p3}, Ljava/util/Map;->clear()V

    .line 422
    :goto_0
    return-void

    .line 403
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->headerParent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->viewBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->headerParentLayoutBounds:Landroid/graphics/Rect;

    .line 406
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->listViewBackground(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v0

    .line 407
    .local v0, "listViewBackground":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 410
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;->NONE:Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setFadeIn(Lcom/google/apps/dots/android/newsstand/widget/NSImageView$FadeInType;)V

    .line 411
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerImage:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;->setAlpha(F)V

    .line 414
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setVisibility(I)V

    .line 415
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v1

    const/4 v2, 0x2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$1;

    invoke-direct {v3, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->addOnChildrenAddedListener(ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->handleEnterRemapSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method protected handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v3, 0x0

    .line 429
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->headerParent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->headerParentLayoutBounds:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/transition/TransitionUtil;->resizeToBounds(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 432
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->dummyHeaderImage(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;

    move-result-object v0

    .line 433
    .local v0, "dummyHeaderImage":Landroid/widget/ImageView;
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 434
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 435
    return-void
.end method

.method protected bridge synthetic handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->handleEnterSetSharedElementStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected handleExitRejectedSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;)V
    .locals 1
    .param p1, "controller"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 470
    .local p2, "rejectedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V

    .line 471
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 475
    :cond_0
    return-void
.end method

.method protected bridge synthetic handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->handleExitRejectedSharedElements(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/util/List;)V

    return-void
.end method

.method protected headerParent(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 512
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->transitionDummyImage:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method protected listViewBackground(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    .line 498
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->alt_play_background:I

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 447
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V

    .line 448
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$2;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    # invokes: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->updateHeader(Ljava/lang/Runnable;)V
    invoke-static {p1, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$800(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Ljava/lang/Runnable;)V

    .line 464
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setVisibility(I)V

    .line 465
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->onSharedElementEnterTransitionEnd(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/transition/Transition;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;
    .param p2, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 440
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/play/transition/PlayTransitionUtil;->setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V

    .line 441
    return-void
.end method

.method protected bridge synthetic onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->onSharedElementEnterTransitionStart(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;Landroid/transition/Transition;)V

    return-void
.end method

.method protected prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V
    .locals 2
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .prologue
    const/4 v1, 0x0

    .line 343
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementsUseOverlay(Z)V

    .line 344
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataAdapter;->setSupportsLoadingView(Z)Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 345
    return-void
.end method

.method protected bridge synthetic prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 331
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->prepareViewsForTransition(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V

    return-void
.end method
