.class public final Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormTemplate"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;


# instance fields
.field private bitField0_:I

.field private formTemplateId_:Ljava/lang/String;

.field private template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10792
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10793
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 10798
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    .line 10793
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    .locals 3

    .prologue
    .line 10850
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10854
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v2, :cond_0

    .line 10855
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10857
    :cond_0
    return-object v0

    .line 10851
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    :catch_0
    move-exception v1

    .line 10852
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 10789
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->clone()Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10862
    if-ne p1, p0, :cond_1

    .line 10866
    :cond_0
    :goto_0
    return v1

    .line 10863
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 10864
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    .line 10865
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 10866
    goto :goto_0

    .line 10865
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10866
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getFormTemplateId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10800
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 10889
    const/4 v0, 0x0

    .line 10890
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10891
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    .line 10892
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10894
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v1, :cond_1

    .line 10895
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10896
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10898
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->cachedSize:I

    .line 10899
    return v0
.end method

.method public getTemplate()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
    .locals 1

    .prologue
    .line 10822
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 10870
    const/16 v0, 0x11

    .line 10871
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 10872
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 10873
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 10874
    return v0

    .line 10872
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 10873
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10907
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 10908
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 10912
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 10913
    :sswitch_0
    return-object p0

    .line 10918
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    .line 10919
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->bitField0_:I

    goto :goto_0

    .line 10923
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-nez v1, :cond_1

    .line 10924
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    .line 10926
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 10908
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10789
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10879
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10880
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->formTemplateId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 10882
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    if-eqz v0, :cond_1

    .line 10883
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$FormTemplate;->template_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 10885
    :cond_1
    return-void
.end method
