.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;

.field final synthetic val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;->this$1:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 539
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$8$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 540
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getPricingLearnMoreUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    move-result-object v0

    .line 541
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->start()V

    .line 542
    return-void
.end method
