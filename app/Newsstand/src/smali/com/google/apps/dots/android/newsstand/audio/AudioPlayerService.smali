.class public Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
.super Landroid/app/Service;
.source "AudioPlayerService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private audioManager:Landroid/media/AudioManager;

.field private currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

.field private favicon:Landroid/graphics/Bitmap;

.field private faviconDrawable:I

.field private indexInPost:I

.field private mediaButtonReceiver:Landroid/content/ComponentName;

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private notification:Landroid/app/Notification;

.field private notificationIconSizePx:I

.field private notificationManager:Landroid/app/NotificationManager;

.field private owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private pausedByTransientAudioFocusLoss:Z

.field private playlist:Lcom/google/android/libraries/bind/data/DataList;

.field private playlistObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private publisher:Ljava/lang/String;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private status:I

.field private title:Ljava/lang/String;

.field private final updateNotificationRunnable:Ljava/lang/Runnable;

.field private updateProgressRunnable:Ljava/lang/Runnable;

.field private visibilityReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 160
    new-instance v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotificationRunnable:Ljava/lang/Runnable;

    .line 173
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 181
    sget v0, Lcom/google/android/apps/newsstanddev/R$mipmap;->ic_launcher_play_newsstand:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I

    .line 185
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotification()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendProgressUpdate()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V

    return-void
.end method

.method static synthetic access$1102(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->indexInPost:I

    return p1
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    return-object v0
.end method

.method static synthetic access$1300()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updatePublisherInfo()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->postUpdateNotification()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdate()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2002(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I

    return p1
.end method

.method static synthetic access$2102(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->publisher:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationIconSizePx:I

    return v0
.end method

.method static synthetic access$2302(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->favicon:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/content/ComponentName;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Landroid/os/ResultReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;
    .param p1, "x1"    # Landroid/os/ResultReceiver;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private generateNewNotification()V
    .locals 5

    .prologue
    .line 598
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Generating a new notification"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 599
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    .line 600
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 601
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->app_name:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;

    .line 602
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getReadingIntent()Landroid/app/PendingIntent;

    move-result-object v4

    .line 600
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 603
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    sget v1, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_notification:I

    iput v1, v0, Landroid/app/Notification;->icon:I

    .line 604
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    const/16 v1, 0x2a

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 606
    return-void
.end method

.method private getActionIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "isBigLayout"    # Z

    .prologue
    .line 764
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 765
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 766
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p2, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audioNotificationExpanded:I

    :goto_0
    const/high16 v3, 0x8000000

    invoke-static {v2, v1, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1

    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audioNotification:I

    goto :goto_0
.end method

.method private getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "isBigLayout"    # Z

    .prologue
    .line 757
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 758
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 760
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntent(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getPlaylistPosition()I
    .locals 3

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_1

    .line 582
    :cond_0
    const/4 v0, -0x1

    .line 584
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->indexInPost:I

    .line 585
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->audioId(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 584
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private getReadingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 609
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    .line 610
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/share/ShareUrisUtil;->makeExternalReadingIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    .line 609
    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private hasNext()Z
    .locals 3

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getPlaylistPosition()I

    move-result v0

    .line 496
    .local v0, "playlistPosition":I
    if-ltz v0, :cond_0

    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 497
    const/4 v1, 0x1

    .line 499
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private hasPrevious()Z
    .locals 2

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getPlaylistPosition()I

    move-result v0

    .line 504
    .local v0, "playlistPosition":I
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_0

    .line 505
    const/4 v1, 0x1

    .line 507
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isPrepared()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 590
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    if-ne v1, v0, :cond_1

    .line 591
    :cond_0
    const/4 v0, 0x0

    .line 593
    :cond_1
    return v0
.end method

.method private next()V
    .locals 4

    .prologue
    .line 511
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 512
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Playing next audio."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 513
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getPlaylistPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 514
    .local v0, "next":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_INDEX:I

    .line 515
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 514
    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Ljava/lang/String;I)V

    .line 517
    .end local v0    # "next":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method private play(Z)V
    .locals 6
    .param p1, "play"    # Z

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 460
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v1

    if-nez v1, :cond_0

    .line 485
    :goto_0
    return-void

    .line 464
    :cond_0
    if-eqz p1, :cond_2

    .line 465
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 466
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2, v5, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 468
    .local v0, "result":I
    if-ne v0, v4, :cond_1

    .line 469
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 470
    iput v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    .line 471
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendProgressUpdate()V

    .line 472
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 473
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Obtained audio focus, playing media."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 484
    .end local v0    # "result":I
    :goto_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotification()V

    goto :goto_0

    .line 475
    .restart local v0    # "result":I
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Audiofocus request failed"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V

    goto :goto_1

    .line 479
    .end local v0    # "result":I
    :cond_2
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    .line 480
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 481
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 482
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Abandoned audio focus, pausing media."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private postUpdateNotification()V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotificationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 616
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotificationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 617
    return-void
.end method

.method private previous()V
    .locals 4

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 521
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Playing previous audio."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 522
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getPlaylistPosition()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 523
    .local v0, "previous":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_POST_ID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_AUDIO_INDEX:I

    .line 524
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 523
    invoke-direct {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Ljava/lang/String;I)V

    .line 526
    .end local v0    # "previous":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method private resetAudio(I)V
    .locals 4
    .param p1, "nextState"    # I

    .prologue
    const/4 v3, 0x0

    .line 389
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Resetting audio state."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 395
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 397
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 398
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    .line 399
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;

    .line 400
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->publisher:Ljava/lang/String;

    .line 401
    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->favicon:Landroid/graphics/Bitmap;

    .line 402
    sget v0, Lcom/google/android/apps/newsstanddev/R$mipmap;->ic_launcher_play_newsstand:I

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I

    .line 404
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    .line 405
    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    .line 406
    return-void
.end method

.method private seek(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method private sendErrorUpdate()V
    .locals 1

    .prologue
    .line 780
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    .line 781
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    .line 782
    return-void
.end method

.method private sendErrorUpdateAndStopSelf()V
    .locals 0

    .prologue
    .line 785
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdate()V

    .line 786
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelf()V

    .line 787
    return-void
.end method

.method private sendProgressUpdate()V
    .locals 4

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateProgressRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 776
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    .line 777
    return-void
.end method

.method private sendStatusUpdate(Landroid/os/ResultReceiver;)V
    .locals 4
    .param p1, "optResultReceiver"    # Landroid/os/ResultReceiver;

    .prologue
    .line 790
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 791
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "status"

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 792
    const-string v2, "audio_item"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 793
    const-string v2, "reading_edition"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 794
    const-string v2, "owning_edition"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 795
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 796
    const-string v2, "progress"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 797
    const-string v2, "duration"

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 800
    :cond_0
    const-string v2, "has_previous"

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasPrevious()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 801
    const-string v2, "has_next"

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasNext()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 803
    if-nez p1, :cond_1

    .line 804
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.UPDATE_STATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 805
    .local v1, "statusIntent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 806
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 810
    .end local v1    # "statusIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 808
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method private sendViewAnalyticsEvent()V
    .locals 0

    .prologue
    .line 814
    return-void
.end method

.method private setCurrentAudioItem(Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 4
    .param p1, "audioItem"    # Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    .param p2, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 551
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    invoke-static {v2, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 552
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 554
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    .line 568
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 554
    goto :goto_0

    .line 559
    :cond_2
    sget-object v2, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Set current audio item to %s"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 560
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .line 561
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setupAudio()V

    .line 563
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 564
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 565
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setupPlayList()V

    .line 567
    :cond_3
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_1
.end method

.method private setCurrentAudioItem(Ljava/lang/String;I)V
    .locals 3
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 530
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;

    invoke-direct {v2, p0, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$7;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)V

    .line 529
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 547
    return-void
.end method

.method private setupAudio()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 409
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->resetAudio(I)V

    .line 411
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    if-nez v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelf()V

    .line 415
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Set up audio: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 416
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendViewAnalyticsEvent()V

    .line 419
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 420
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 421
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$6;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 420
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 457
    return-void
.end method

.method private setupNotification(Landroid/widget/RemoteViews;Z)V
    .locals 4
    .param p1, "notificationLayout"    # Landroid/widget/RemoteViews;
    .param p2, "isBigLayout"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 674
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->favicon:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_notification:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 676
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->play_button:I

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 677
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_pause_full_32dp:I

    .line 676
    :goto_0
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 680
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->play_button:I

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PAUSE"

    .line 681
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v0

    .line 680
    :goto_1
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 683
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v0, v3, :cond_0

    .line 684
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->play_button:I

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 685
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->media_pause:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 684
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 687
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->cancel_button:I

    const-string v3, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.HIDE_NOTIFICATION"

    .line 688
    invoke-direct {p0, v3, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v3

    .line 687
    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 689
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->previous_button:I

    const-string v3, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PREVIOUS"

    .line 690
    invoke-direct {p0, v3, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v3

    .line 689
    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 691
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->next_button:I

    const-string v3, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.NEXT"

    .line 692
    invoke-direct {p0, v3, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v3

    .line 691
    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 695
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->next_button:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 696
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->previous_button:I

    .line 697
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 696
    :goto_4
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 700
    if-eqz p2, :cond_1

    .line 701
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->previous_button_disabled:I

    .line 702
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    .line 701
    :goto_5
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 703
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->next_button_disabled:I

    .line 704
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 703
    :goto_6
    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 707
    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->article_title:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 708
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->publisher:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->publisher:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 709
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->favicon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    .line 710
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->favicon:I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->favicon:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 714
    :goto_7
    return-void

    .line 677
    :cond_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_play_arrow_full_32dp:I

    goto/16 :goto_0

    .line 681
    :cond_3
    const-string v0, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PLAY"

    .line 682
    invoke-direct {p0, v0, p2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getActionIntentCompat(Ljava/lang/String;Z)Landroid/app/PendingIntent;

    move-result-object v0

    goto/16 :goto_1

    .line 685
    :cond_4
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->media_play:I

    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 695
    goto :goto_3

    :cond_6
    move v0, v2

    .line 697
    goto :goto_4

    :cond_7
    move v0, v1

    .line 702
    goto :goto_5

    :cond_8
    move v2, v1

    .line 704
    goto :goto_6

    .line 712
    :cond_9
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->favicon:I

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_7
.end method

.method private setupPlayList()V
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlistObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 574
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->getAudioPlaylistForEdition(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    .line 575
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlistObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 576
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    .line 577
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotification()V

    .line 578
    return-void
.end method

.method private stopSelfIfNecessary()V
    .locals 2

    .prologue
    .line 350
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    if-nez v0, :cond_1

    .line 351
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelf()V

    .line 353
    :cond_1
    return-void
.end method

.method private updateNotification()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 621
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Updating notification"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 622
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSApplication;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 627
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->title:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 628
    sget-object v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Fetching title, publisher, favicon for audioItem: %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 629
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 630
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/media/AudioItem;->postId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$8;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    .line 629
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 650
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    if-nez v1, :cond_3

    .line 651
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->generateNewNotification()V

    .line 655
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_4

    .line 656
    new-instance v0, Landroid/widget/RemoteViews;

    .line 657
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->audio_notification_expanded:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 658
    .local v0, "notificationLayout":Landroid/widget/RemoteViews;
    invoke-direct {p0, v0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setupNotification(Landroid/widget/RemoteViews;Z)V

    .line 659
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    iput-object v0, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 663
    .end local v0    # "notificationLayout":Landroid/widget/RemoteViews;
    :cond_4
    new-instance v0, Landroid/widget/RemoteViews;

    .line 664
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->audio_notification:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 665
    .restart local v0    # "notificationLayout":Landroid/widget/RemoteViews;
    invoke-direct {p0, v0, v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setupNotification(Landroid/widget/RemoteViews;Z)V

    .line 666
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 669
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->audioNotification:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method private updatePublisherInfo()V
    .locals 3

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 753
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 346
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 366
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Completed audio: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->next()V

    .line 373
    :goto_0
    return-void

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelf()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 199
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 200
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 203
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->audio_player_favicon_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 204
    .local v0, "notificationSizeDips":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPixelsFromDips(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationIconSizePx:I

    .line 205
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 208
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 209
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 210
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 212
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    .line 213
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationManager:Landroid/app/NotificationManager;

    .line 215
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$2;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateProgressRunnable:Ljava/lang/Runnable;

    .line 222
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$3;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 248
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$4;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlistObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 256
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/apps/dots/android/newsstand/audio/MediaButtonIntentReceiver;

    .line 257
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;

    .line 259
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$5;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->visibilityReceiver:Landroid/content/BroadcastReceiver;

    .line 274
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->visibilityReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.apps.dots.android.newsstand.NSApplication.action.APPLICATION_VISIBLE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 276
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 280
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->resetAudio(I)V

    .line 281
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 282
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 283
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaButtonReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 284
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->visibilityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 285
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->audioNotification:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 286
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlist:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->playlistObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 289
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 290
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v5, 0x1

    .line 377
    sget-object v0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Error, audio: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->currentAudioItem:Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendErrorUpdateAndStopSelf()V

    .line 379
    return v5
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 357
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->status:I

    .line 358
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    .line 359
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotification()V

    .line 361
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    .line 362
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 294
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "action":Ljava/lang/String;
    const-string v8, "audio_item"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/AudioItem;

    .line 296
    .local v1, "audioItem":Lcom/google/apps/dots/android/newsstand/media/AudioItem;
    const-string v8, "reading_edition"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 297
    .local v4, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-string v8, "owning_edition"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 298
    .local v2, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-string v8, "postId"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 300
    .local v3, "postId":Ljava/lang/String;
    sget-object v8, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v9, "Received intent: %s"

    new-array v10, v6, [Ljava/lang/Object;

    aput-object v0, v10, v7

    invoke-virtual {v8, v9, v10}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    const-string v8, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PLAY"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 304
    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    .line 339
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelfIfNecessary()V

    .line 341
    const/4 v6, 0x2

    return v6

    .line 305
    :cond_1
    const-string v8, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PAUSE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 306
    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z

    .line 307
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    goto :goto_0

    .line 308
    :cond_2
    const-string v8, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.TOGGLE_PLAY_PAUSE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 309
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->isPrepared()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 310
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v8}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v8

    if-nez v8, :cond_3

    :goto_1
    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1

    .line 312
    :cond_4
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.HIDE_NOTIFICATION"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 313
    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->pausedByTransientAudioFocusLoss:Z

    .line 314
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->play(Z)V

    .line 315
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->audioItemScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updateNotificationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 316
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationManager:Landroid/app/NotificationManager;

    sget v7, Lcom/google/android/apps/newsstanddev/R$id;->audioNotification:I

    invoke-virtual {v6, v7}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 317
    :cond_5
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.CANCEL"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 318
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->stopSelf()V

    goto :goto_0

    .line 319
    :cond_6
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.SEEK"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 320
    const-string v6, "progress"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->seek(I)V

    goto :goto_0

    .line 321
    :cond_7
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.SET"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 322
    if-eqz v1, :cond_8

    .line 323
    invoke-direct {p0, v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Lcom/google/apps/dots/android/newsstand/media/AudioItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto :goto_0

    .line 325
    :cond_8
    invoke-direct {p0, v3, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->setCurrentAudioItem(Ljava/lang/String;I)V

    goto :goto_0

    .line 327
    :cond_9
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.NEXT"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 328
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->next()V

    goto/16 :goto_0

    .line 329
    :cond_a
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.PREVIOUS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 330
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->previous()V

    goto/16 :goto_0

    .line 333
    :cond_b
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.STATUS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 334
    const-string v6, "com.google.apps.dots.android.newsstand.audio.AudioPlayerService.RESULT_RECEIVER"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/os/ResultReceiver;

    .line 335
    .local v5, "receiver":Landroid/os/ResultReceiver;
    if-eqz v5, :cond_0

    .line 336
    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->sendStatusUpdate(Landroid/os/ResultReceiver;)V

    goto/16 :goto_0
.end method
