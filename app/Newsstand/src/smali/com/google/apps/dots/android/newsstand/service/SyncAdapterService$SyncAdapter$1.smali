.class Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;
.super Landroid/os/ResultReceiver;
.source "SyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;->onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;

.field final synthetic val$syncResult:Landroid/content/SyncResult;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;Landroid/os/Handler;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;
    .param p2, "arg0"    # Landroid/os/Handler;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;->this$1:Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;->val$syncResult:Landroid/content/SyncResult;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0, p2}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "resultCode"    # I
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 136
    packed-switch p1, :pswitch_data_0

    .line 151
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 152
    return-void

    .line 138
    :pswitch_0
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "background sync succeeded"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "result %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    if-eqz p2, :cond_0

    .line 141
    const-string v1, "numFailures"

    invoke-virtual {p2, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 142
    .local v0, "failures":I
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService$SyncAdapter$1;->val$syncResult:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0

    .line 146
    .end local v0    # "failures":I
    :pswitch_1
    # getter for: Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/service/SyncAdapterService;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "background sync failed with: %s"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "exceptionMessage"

    .line 147
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 146
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
