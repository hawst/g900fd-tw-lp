.class Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;
.super Ljava/lang/Object;
.source "AsyncService.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/AsyncService;->track(Landroid/content/Intent;Lcom/google/common/util/concurrent/ListenableFuture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

.field final synthetic val$future:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$intentStartTime:J

.field final synthetic val$receiver:Landroid/os/ResultReceiver;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/AsyncService;Landroid/os/ResultReceiver;Landroid/content/Intent;JLcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$receiver:Landroid/os/ResultReceiver;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intent:Landroid/content/Intent;

    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intentStartTime:J

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private finish()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->untrack()V

    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intent:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/WakefulIntents;->completeWakefulIntent(Landroid/content/Intent;)Z

    .line 91
    return-void
.end method

.method private untrack()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    # getter for: Lcom/google/apps/dots/android/newsstand/service/AsyncService;->pending:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->access$000(Lcom/google/apps/dots/android/newsstand/service/AsyncService;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$future:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    # getter for: Lcom/google/apps/dots/android/newsstand/service/AsyncService;->pending:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->access$000(Lcom/google/apps/dots/android/newsstand/service/AsyncService;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->stopSelf()V

    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "All done! Goodbye."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    :cond_0
    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 9
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v8, 0x1

    .line 78
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$receiver:Landroid/os/ResultReceiver;

    if-eqz v1, :cond_0

    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "exceptionMessage"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$receiver:Landroid/os/ResultReceiver;

    invoke-virtual {v1, v8, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 83
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Failure handling intent %s in %.2f seconds"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intent:Landroid/content/Intent;

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;->toString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intentStartTime:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    .line 83
    invoke-virtual {v1, p1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->finish()V

    .line 86
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 68
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$receiver:Landroid/os/ResultReceiver;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$receiver:Landroid/os/ResultReceiver;

    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_1

    check-cast p1, Landroid/os/Bundle;

    .end local p1    # "result":Ljava/lang/Object;
    :goto_0
    invoke-virtual {v0, v4, p1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/AsyncService;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/service/AsyncService;->logd:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Success handling intent %s in %.2f seconds"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intent:Landroid/content/Intent;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;->toString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    .line 72
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->val$intentStartTime:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/service/AsyncService$1;->finish()V

    .line 74
    return-void

    .line 69
    .restart local p1    # "result":Ljava/lang/Object;
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method
