.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;
.super Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;
.source "RemoveDownloadWarningDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final EXTRA_DONT_ASK_AGAIN_IS_CHECKED:Ljava/lang/String; = "RemoveDownloadWarningDialog_dontAskAgainIsChecked"

.field protected static final EXTRA_EDITION:Ljava/lang/String; = "RemoveDownloadWarningDialog_edition"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dontAskAgainIsChecked:Z

.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;-><init>()V

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->dontAskAgainIsChecked:Z

    return p1
.end method

.method public static maybeShow(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 3
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 64
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDontShowRemoveDownloadWarning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->startUnpinIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 78
    :goto_0
    return-void

    .line 68
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;-><init>()V

    .line 72
    .local v1, "dialog":Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 73
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "RemoveDownloadWarningDialog_edition"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 74
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->setArguments(Landroid/os/Bundle;)V

    .line 76
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->TAG:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static startUnpinIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 142
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    .line 143
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->unpinEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    const/4 v1, 0x1

    .line 144
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 146
    return-void
.end method


# virtual methods
.method protected handleExtras(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 54
    :goto_0
    return-void

    .line 49
    :cond_0
    const-string v0, "RemoveDownloadWarningDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const-string v0, "RemoveDownloadWarningDialog_edition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 52
    :cond_1
    const-string v0, "RemoveDownloadWarningDialog_dontAskAgainIsChecked"

    .line 53
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDontShowRemoveDownloadWarning()Z

    move-result v1

    .line 52
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->dontAskAgainIsChecked:Z

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 133
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->dontAskAgainIsChecked:Z

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setDontShowRemoveDownloadWarning(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->startUnpinIntent(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 139
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->handleExtras(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 85
    .local v3, "inflater":Landroid/view/LayoutInflater;
    sget v5, Lcom/google/android/apps/newsstanddev/R$layout;->remove_download_warning_body:I

    const/4 v6, 0x0

    .line 86
    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 88
    .local v4, "linear":Landroid/widget/LinearLayout;
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->edition_title:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 89
    .local v2, "editionTitleTextView":Landroid/widget/TextView;
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->edition_subtitle:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 90
    .local v1, "editionSubtitleTextView":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->destroyAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;

    invoke-direct {v7, p0, v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 114
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->checkbox:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 115
    .local v0, "checkbox":Landroid/widget/CheckBox;
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->dontAskAgainIsChecked:Z

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 116
    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$2;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;)V

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 123
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->remove_download_warning_title:I

    .line 124
    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 125
    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    .line 126
    invoke-virtual {v5, v6, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->cancel:I

    .line 127
    invoke-virtual {v5, v6, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 128
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const-string v0, "RemoveDownloadWarningDialog_edition"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    const-string v0, "RemoveDownloadWarningDialog_dontAskAgainIsChecked"

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/RemoveDownloadWarningDialog;->dontAskAgainIsChecked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 60
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/NSDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method
