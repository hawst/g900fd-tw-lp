.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "WebArticleReadingScreen.java"


# instance fields
.field private final page:I

.field private final postTitle:Ljava/lang/String;

.field private final publisher:Ljava/lang/String;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V
    .locals 0
    .param p1, "postTitle"    # Ljava/lang/String;
    .param p2, "publisher"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p5, "page"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    invoke-static {p4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->url:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 39
    iput p5, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->page:I

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 64
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;

    .line 66
    .local v0, "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    .line 67
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->url:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->url:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 69
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->page:I

    iget v3, v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->page:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 72
    .end local v0    # "event":Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;
    :cond_0
    return v1
.end method

.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 5
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    .line 85
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    .line 86
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPostTitle(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->page:I

    .line 87
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setPage(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 91
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v1, :cond_0

    .line 92
    const-string v0, "ReadNow"

    .line 106
    .local v0, "readFromString":Ljava/lang/String;
    :goto_0
    const-string v1, "ReadFrom"

    invoke-static {p1, v1, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-object p1

    .line 93
    .end local v0    # "readFromString":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v1, :cond_2

    .line 94
    const-string v1, "[Topic] - "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 95
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "readFromString":Ljava/lang/String;
    :goto_1
    goto :goto_0

    .end local v0    # "readFromString":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    if-eqz v1, :cond_3

    .line 97
    const-string v0, "Bookmarks"

    .restart local v0    # "readFromString":Ljava/lang/String;
    goto :goto_0

    .line 98
    .end local v0    # "readFromString":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    if-eqz v1, :cond_4

    .line 99
    const-string v0, "Search"

    .restart local v0    # "readFromString":Ljava/lang/String;
    goto :goto_0

    .line 100
    .end local v0    # "readFromString":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    if-eqz v1, :cond_5

    .line 101
    const-string v1, "%s %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[Related]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "readFromString":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v0    # "readFromString":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "readFromString":Ljava/lang/String;
    goto :goto_0
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 0
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 114
    return-object p1
.end method

.method protected getDedupeExpiryTime()J
    .locals 2

    .prologue
    .line 54
    const-wide/32 v0, 0xafc8

    return-wide v0
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 78
    const-string v0, "[WebLink] "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->postTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->publisher:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleReadingScreen;->page:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method
