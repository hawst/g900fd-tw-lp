.class public Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;
.super Ljava/lang/Object;
.source "OnDeviceUiHelper.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/util/Disposable;


# instance fields
.field private final additionalListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/apps/dots/android/newsstand/util/Disposable;",
            ">;"
        }
    .end annotation
.end field

.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method protected constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 5
    .param p1, "playHeaderListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->additionalListeners:Ljava/util/Set;

    .line 33
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 34
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper$1;-><init>(Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "downloadedOnly"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 41
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->updateBannerVisibility()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method public static create(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;
    .locals 1
    .param p0, "playHeaderListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 114
    new-instance v0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    return-object v0
.end method

.method private isBannerEnabled()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->showOnDeviceOnlyUi()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOnDeviceOnly()Z
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getOnDeviceOnly()Z

    move-result v0

    return v0
.end method

.method public static userSetOnDeviceOnly(Z)V
    .locals 1
    .param p0, "onDeviceOnly"    # Z

    .prologue
    .line 50
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    .line 51
    .local v0, "prefs":Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setOnDeviceOnly(Z)V

    .line 52
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->preferenceListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->additionalListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 108
    .local v0, "disposable":Lcom/google/apps/dots/android/newsstand/util/Disposable;
    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    goto :goto_0

    .line 110
    .end local v0    # "disposable":Lcom/google/apps/dots/android/newsstand/util/Disposable;
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->additionalListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 111
    return-void
.end method

.method protected updateBannerVisibility()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isBannerEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isOnDeviceOnly()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 85
    .local v0, "showBanner":Z
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->downloaded_only_upper:I

    :cond_0
    invoke-virtual {v2, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(I)V

    .line 86
    if-eqz v0, :cond_1

    .line 87
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper$3;-><init>(Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    :cond_1
    return-void

    .end local v0    # "showBanner":Z
    :cond_2
    move v0, v1

    .line 84
    goto :goto_0
.end method
