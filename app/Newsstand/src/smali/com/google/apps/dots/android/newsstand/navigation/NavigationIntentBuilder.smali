.class public abstract Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;
.source "NavigationIntentBuilder.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field protected final activity:Landroid/app/Activity;

.field private clearBackstack:Z

.field private enterAnim:I

.field private exitAnim:I

.field private overridePendingTransition:Z

.field private sharedElementPairs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    .line 42
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    .line 43
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransition:Z

    .line 44
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack:Z

    .line 45
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    .line 50
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransition:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack:Z

    .line 52
    return-void
.end method

.method private getActivityOptions()Landroid/app/ActivityOptions;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 206
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    .line 207
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isA11yEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    .line 209
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Landroid/util/Pair;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/util/Pair;

    .line 208
    invoke-static {v1, v0}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private overridePendingTransitionIfNeeded()V
    .locals 3

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransition:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->enterAnim:I

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->exitAnim:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 218
    :cond_0
    return-void
.end method

.method private startInternal(ZZI)V
    .locals 9
    .param p1, "addToBackstack"    # Z
    .param p2, "forResult"    # Z
    .param p3, "requestCode"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 159
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 160
    sget-object v3, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Attempted to call start() with a null intent"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->onStart()V

    .line 165
    if-eqz p1, :cond_1

    .line 166
    const-string v3, "addToBackStack"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "%s - starting intent: %s, extras: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v1, v5, v8

    const/4 v6, 0x2

    .line 169
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    aput-object v7, v5, v6

    .line 168
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    .line 172
    .local v2, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 175
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->getActivityOptions()Landroid/app/ActivityOptions;

    move-result-object v0

    .line 176
    .local v0, "activityOptions":Landroid/app/ActivityOptions;
    if-eqz p2, :cond_3

    .line 177
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    if-eqz v0, :cond_2

    .line 179
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v1, p3, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 191
    :goto_1
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 192
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransitionIfNeeded()V

    goto :goto_0

    .line 181
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v3, v1, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 184
    :cond_3
    if-eqz v0, :cond_4

    .line 185
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_1

    .line 187
    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method protected addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "sharedElementName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 92
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    return-object p0
.end method

.method protected addSystemUiAsSharedElements()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 111
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isA11yEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 112
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    const v3, 0x1020030

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "navBarBackground":Landroid/view/View;
    const-string v2, "android:navigation:background"

    invoke-virtual {p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 115
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    const v3, 0x102002f

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 116
    .local v1, "statusBarBg":Landroid/view/View;
    const-string v2, "android:status:background"

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 118
    .end local v0    # "navBarBackground":Landroid/view/View;
    .end local v1    # "statusBarBg":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public abstract build()Landroid/content/Intent;
.end method

.method public clearBackstack()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack:Z

    .line 87
    return-object p0
.end method

.method protected copySceneTransitionPairs(Ljava/util/List;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "pairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;>;"
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 101
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 102
    .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    goto :goto_0

    .line 105
    .end local v0    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    :cond_0
    return-object p0
.end method

.method protected makeIntent(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/high16 v2, 0x10000000

    .line 56
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/IntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 57
    .local v0, "intent":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->clearBackstack:Z

    if-eqz v1, :cond_1

    .line 58
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 59
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 60
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 66
    :cond_0
    :goto_0
    return-object v0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 64
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public noTransition()Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-virtual {p0, v0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransition(II)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public overridePendingTransition(II)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
    .locals 1
    .param p1, "enterAnim"    # I
    .param p2, "exitAnim"    # I

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->overridePendingTransition:Z

    .line 76
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->enterAnim:I

    .line 77
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->exitAnim:I

    .line 78
    return-object p0
.end method

.method sharedElementPairs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->sharedElementPairs:Ljava/util/List;

    return-object v0
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start(Z)V

    .line 139
    return-void
.end method

.method public start(Z)V
    .locals 1
    .param p1, "addToBackstack"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-direct {p0, p1, v0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startInternal(ZZI)V

    .line 144
    return-void
.end method

.method public final startForResult(I)V
    .locals 1
    .param p1, "requestCode"    # I

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startForResult(IZ)V

    .line 149
    return-void
.end method

.method public startForResult(IZ)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "addToBackstack"    # Z

    .prologue
    .line 152
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startInternal(ZZI)V

    .line 153
    return-void
.end method

.method public final startIfOnline()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->startIfOnline(Z)V

    .line 123
    return-void
.end method

.method public final startIfOnline(Z)V
    .locals 3
    .param p1, "addToBackstack"    # Z

    .prologue
    .line 127
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->start(Z)V

    .line 133
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;->activity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->content_error_offline:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
