.class public Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;
.super Ljava/lang/Object;
.source "SubscriptionUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field public static final UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 42
    sget-object v0, Lcom/google/apps/dots/android/newsstand/translation/Translation;->UNDEFINED:Lcom/google/apps/dots/android/newsstand/translation/Translation;

    .line 43
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/translation/Translation;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V
    .locals 8
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "showToast"    # Z
    .param p4, "provideReadNow"    # Z

    .prologue
    const/4 v2, 0x1

    .line 69
    if-nez p2, :cond_0

    .line 70
    invoke-static {p0, v2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->handleSubscriptionFailure(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V

    .line 91
    :goto_0
    return-void

    .line 74
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Adding subscription for edition: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->addSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 77
    .local v6, "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v7

    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;

    move v1, p3

    move-object v2, p0

    move-object v3, p2

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$1;-><init>(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZLandroid/accounts/Account;)V

    invoke-virtual {v7, v6, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static addSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    .line 101
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getIdForMutation(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;

    move-result-object v6

    .line 100
    invoke-virtual {v4, p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 102
    .local v3, "subscribeUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 103
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 104
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v4, v5}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeAddSubscriptionHint(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 107
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    invoke-virtual {v4, p0, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "mutationUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v4, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 109
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 111
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v4

    invoke-virtual {v4, p0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4
.end method

.method public static getIdForMutation(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;
    .locals 2
    .param p0, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->CURATION:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getAppFamilyId()Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getAppId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    .locals 4

    .prologue
    .line 372
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 373
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->subscribedEditionSet()Ljava/util/Set;

    move-result-object v1

    .line 374
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->purchasedEditionSet()Ljava/util/Set;

    move-result-object v2

    .line 375
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->getOffersStatusSnapshot(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;-><init>(Ljava/util/Set;Ljava/util/Set;Lcom/google/apps/dots/android/newsstand/util/OffersUtil$OffersStatusSnapshot;)V

    return-object v0
.end method

.method private static handleSubscriptionFailure(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V
    .locals 3
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "adding"    # Z

    .prologue
    .line 190
    if-eqz p1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->add_subscription_failed:I

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V

    .line 193
    return-void

    .line 190
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->remove_subscription_failed:I

    goto :goto_0
.end method

.method public static removeSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V
    .locals 8
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "isPurchased"    # Z
    .param p4, "provideUndo"    # Z

    .prologue
    const/4 v4, 0x0

    .line 125
    if-nez p2, :cond_0

    .line 126
    invoke-static {p0, v4}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->handleSubscriptionFailure(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Z)V

    .line 163
    :goto_0
    return-void

    .line 130
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Removing subscription for edition: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    if-eqz p3, :cond_1

    .line 133
    invoke-static {p0, p2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->showCancelPurchaseDialog(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    goto :goto_0

    .line 137
    :cond_1
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList;->getPrecedingAppFamilyIdOfSameType(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "undoAppFamilyPivotId":Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 141
    .local v6, "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v7

    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$2;

    move-object v1, p0

    move-object v2, p2

    move v3, p4

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$2;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZLandroid/accounts/Account;Ljava/lang/String;)V

    invoke-virtual {v7, v6, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 157
    new-instance v0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;

    invoke-direct {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;-><init>(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSApplication;->postOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static removeSubscriptionNoUi(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    .line 174
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getIdForMutation(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)Ljava/lang/String;

    move-result-object v6

    .line 173
    invoke-virtual {v4, p0, v5, v6}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->subscribe(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175
    .local v3, "subscribeUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 176
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 179
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    invoke-virtual {v4, p0, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, "mutationUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v4, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 181
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 183
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v4

    invoke-virtual {v4, p0, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    return-object v4
.end method

.method public static reorderSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionType"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;
    .param p3, "moveAppFamilyId"    # Ljava/lang/String;
    .param p4, "pivotAppFamilyId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 206
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Reordering subscription"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->subscriptionReorder(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 210
    .local v3, "subscribeReorderUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    .line 211
    invoke-virtual {v4, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 212
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 213
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 215
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "mutationUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v4, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 217
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 219
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 220
    return-void
.end method

.method public static resetNewsSubscriptionTranslation(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 9
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    const/4 v8, 0x0

    .line 306
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Undoing subscription translation"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasTranslationCode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 328
    :goto_0
    return-void

    .line 313
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 314
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    iget-object v6, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v6, v6, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 313
    invoke-virtual {v4, p1, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->subscriptionTranslate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 315
    .local v3, "subscribeTranslateUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    .line 316
    invoke-virtual {v4, v8}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 317
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    .line 318
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 319
    invoke-static {p2, v5}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeSubscriptionTranslationHint(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v0

    .line 323
    .local v0, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v4

    iget-object v5, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v1

    .line 324
    .local v1, "mutationUri":Ljava/lang/String;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v4, v1, v0}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v5, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 325
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v2

    .line 327
    .local v2, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static showCancelPurchaseDialog(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 5
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 333
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 334
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->cancel_metered_subscription_title:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 335
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->cancel_metered_subscription_warning:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 336
    invoke-virtual {p1, p0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 335
    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 339
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->go_to_play_store:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$5;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 349
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->cancel:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$6;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$6;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 356
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 357
    return-void
.end method

.method public static translateNewsSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;Z)V
    .locals 15
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "translationCode"    # Ljava/lang/String;
    .param p4, "provideToast"    # Z

    .prologue
    .line 234
    sget-object v2, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Translating subscription"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasTranslationCode()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 238
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTranslationCode()Ljava/lang/String;

    move-result-object v9

    .line 240
    .local v9, "currentLanguage":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 271
    :goto_1
    return-void

    .line 238
    .end local v9    # "currentLanguage":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 239
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getLanguageCode()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 244
    .restart local v9    # "currentLanguage":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 245
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v4, v4, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 244
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v2, v0, v3, v4, v1}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->subscriptionTranslate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 246
    .local v13, "subscribeTranslateUri":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;-><init>()V

    const/4 v3, 0x0

    .line 247
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setMethod(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 248
    invoke-virtual {v2, v13}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setUri(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 249
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setActionTimestamp(J)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v2

    .line 251
    invoke-static/range {p2 .. p3}, Lcom/google/apps/dots/android/newsstand/store/BackendSimulator;->makeSubscriptionTranslationHint(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v3

    .line 250
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->setSimulationHint(Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    move-result-object v8

    .line 254
    .local v8, "clientAction":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/server/ServerUris;->getMySubscriptions(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;)Ljava/lang/String;

    move-result-object v11

    .line 255
    .local v11, "mutationUri":Ljava/lang/String;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    invoke-direct {v2, v11, v8}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;-><init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;

    .line 256
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreMutation;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreMutation$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreMutation;

    move-result-object v12

    .line 258
    .local v12, "storeMutation":Lcom/google/apps/dots/android/newsstand/store/StoreMutation;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v12}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->mutate(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreMutation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 259
    .local v10, "mutationFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->stopAsyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v14

    new-instance v2, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;

    move/from16 v3, p4

    move-object v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$4;-><init>(ZLcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    invoke-virtual {v14, v10, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 270
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->pinner()Lcom/google/apps/dots/android/newsstand/sync/Pinner;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/google/apps/dots/android/newsstand/sync/Pinner;->unpin(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    goto/16 :goto_1
.end method

.method public static translateNewsSubscriptionIfSubscribed(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p3, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 283
    iget-object v0, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 284
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v2, :cond_0

    .line 287
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .end local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 288
    .restart local v0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    .end local p2    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    move-object p2, v1

    .line 294
    .end local v1    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .restart local p2    # "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 296
    sget-object v2, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 297
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->resetNewsSubscriptionTranslation(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    .line 302
    :cond_1
    :goto_0
    return-void

    .line 299
    :cond_2
    const/4 v2, 0x0

    invoke-static {p0, p1, p2, p3, v2}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->translateNewsSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;Z)V

    goto :goto_0
.end method
