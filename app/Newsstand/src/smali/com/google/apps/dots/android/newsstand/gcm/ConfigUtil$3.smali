.class Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;
.super Ljava/lang/Object;
.source "ConfigUtil.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->registerWithGcm()Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 307
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 308
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$500(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/gcm/GoogleCloudMessaging;

    move-result-object v1

    .line 309
    .local v1, "gcm":Lcom/google/android/gms/gcm/GoogleCloudMessaging;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil$3;->this$0:Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->appContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$500(Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;)Landroid/content/Context;

    move-result-object v4

    sget v7, Lcom/google/android/apps/newsstanddev/R$string;->gcm_sender_id:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 310
    .local v3, "gcmSenderId":Ljava/lang/String;
    const-string v4, "MUST_OVERRIDE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 313
    const/4 v4, 0x1

    :try_start_0
    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v4, v7

    invoke-virtual {v1, v4}, Lcom/google/android/gms/gcm/GoogleCloudMessaging;->register([Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 318
    .local v2, "gcmRegId":Ljava/lang/String;
    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v7, "Got gcm registration %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v4, v7, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    .end local v2    # "gcmRegId":Ljava/lang/String;
    :goto_2
    return-object v2

    .end local v1    # "gcm":Lcom/google/android/gms/gcm/GoogleCloudMessaging;
    .end local v3    # "gcmSenderId":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 307
    goto :goto_0

    .restart local v1    # "gcm":Lcom/google/android/gms/gcm/GoogleCloudMessaging;
    .restart local v3    # "gcmSenderId":Ljava/lang/String;
    :cond_1
    move v4, v6

    .line 310
    goto :goto_1

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->access$300()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v4

    const-string v5, "Failed to get GCM registration"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    const/4 v2, 0x0

    goto :goto_2
.end method
