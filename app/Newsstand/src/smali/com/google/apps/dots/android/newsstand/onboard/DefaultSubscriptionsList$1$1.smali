.class Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;
.super Ljava/lang/Object;
.source "DefaultSubscriptionsList.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

.field final synthetic val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

.field final synthetic val$defaultOn:Z

.field final synthetic val$itemId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Z)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$itemId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$defaultOn:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 88
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->this$1:Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;->access$100(Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 90
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->LAYOUT_CURATION:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 91
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_ITEM_ID:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$itemId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_TITLE:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_DEFAULT_ON:I

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$defaultOn:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 94
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ASPECT_RATIO:I

    const/high16 v3, 0x3f800000    # 1.0f

    .line 95
    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconHeightToWidthRatio(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;F)F

    move-result v3

    .line 94
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 96
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SELECTION_COLOR_RES_ID:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 99
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getIdToUseForColor(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Ljava/lang/String;

    move-result-object v3

    .line 98
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorResIdForEditionId(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 96
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 101
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AttachmentUtil;->getIconAttachmentId(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "iconAttachmentId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 103
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_SOURCE_ICON_ID:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 110
    :goto_0
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_SUMMARY:I

    invoke-virtual {v0, v2, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 111
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_APP_FAMILY_SUMMARY:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 112
    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->val$appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 113
    invoke-static {p1, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v3

    .line 112
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 115
    return-object v0

    .line 105
    :cond_0
    sget v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_INITIALS:I

    sget v3, Lcom/google/apps/dots/android/newsstand/onboard/OnboardQuizItem;->DK_TITLE:I

    .line 106
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/DefaultSubscriptionsList$1$1;->apply(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
