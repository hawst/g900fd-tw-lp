.class public Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;
.super Ljava/lang/Object;
.source "NSWebviewHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;,
        Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$Pool;
    }
.end annotation


# static fields
.field private static final CONNECT_TIMEOUT_MS:I = 0x1388

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final cookieManager:Ljava/net/CookieManager;

.field private final cookieStore:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    invoke-direct {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->cookieStore:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .line 60
    new-instance v0, Ljava/net/CookieManager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->cookieStore:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->cookiePolicy()Lcom/google/apps/dots/android/newsstand/http/NSCookiePolicy;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/net/CookieManager;-><init>(Ljava/net/CookieStore;Ljava/net/CookiePolicy;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->cookieManager:Ljava/net/CookieManager;

    .line 61
    return-void
.end method

.method private get(Ljava/net/URL;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
    .locals 5
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "followRedirects"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->defaultCookieManager()Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->cookieManager:Ljava/net/CookieManager;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;

    invoke-direct {v3, p0, p1, p3, p2}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$1;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;Ljava/net/URL;ZLjava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->callWithCookieHandler(Ljava/net/CookieHandler;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 149
    :goto_0
    return-object v1

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/io/IOException;
    throw v0

    .line 147
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Execution error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearCookies()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->cookieStore:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->removeAll()Z

    move-result v0

    return v0
.end method

.method public getRedirectLocation(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;
    .locals 3
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "userAgent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->get(Ljava/net/URL;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;

    move-result-object v1

    .line 94
    .local v1, "result":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
    const-string v2, "Location"

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "location":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 96
    const/4 v2, 0x0

    .line 98
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResource(Ljava/net/URL;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 10
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "userAgent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v5, 0x1

    invoke-direct {p0, p1, p2, v5}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->get(Ljava/net/URL;Ljava/lang/String;Z)Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;

    move-result-object v4

    .line 66
    .local v4, "result":Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "request properties: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v4, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;->headers:Ljava/util/Map;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    const-string v5, "Content-Type"

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "contentType":Ljava/lang/String;
    const-string v5, "Content-Encoding"

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "contentEncoding":Ljava/lang/String;
    const/4 v5, 0x0

    .line 73
    :try_start_0
    invoke-static {v1, v5}, Lorg/apache/http/message/BasicHeaderValueParser;->parseElements(Ljava/lang/String;Lorg/apache/http/message/HeaderValueParser;)[Lorg/apache/http/HeaderElement;

    move-result-object v3

    .line 77
    .local v3, "elements":[Lorg/apache/http/HeaderElement;
    array-length v5, v3

    if-lez v5, :cond_0

    const/4 v5, 0x0

    aget-object v5, v3, v5

    invoke-interface {v5}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 87
    .end local v3    # "elements":[Lorg/apache/http/HeaderElement;
    :goto_0
    sget-object v5, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "%s %s -> %s, %s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object v1, v7, v8

    const/4 v8, 0x2

    aput-object v1, v7, v8

    const/4 v8, 0x3

    aput-object v0, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    new-instance v5, Landroid/webkit/WebResourceResponse;

    iget-object v6, v4, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient$HttpResult;->inputStream:Ljava/io/InputStream;

    invoke-direct {v5, v1, v0, v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    return-object v5

    .line 77
    .restart local v3    # "elements":[Lorg/apache/http/HeaderElement;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 78
    .end local v3    # "elements":[Lorg/apache/http/HeaderElement;
    :catch_0
    move-exception v2

    .line 80
    .local v2, "e":Lorg/apache/http/ParseException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Exception while parsing content type %s from url %s: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    const/4 v8, 0x2

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    .end local v2    # "e":Lorg/apache/http/ParseException;
    :catch_1
    move-exception v2

    .line 84
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/http/NSWebviewHttpClient;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v6, "Exception while parsing content type %s from url %s: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    const/4 v8, 0x2

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
