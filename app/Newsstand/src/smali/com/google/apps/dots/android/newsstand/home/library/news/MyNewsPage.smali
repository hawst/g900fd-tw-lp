.class public Lcom/google/apps/dots/android/newsstand/home/library/news/MyNewsPage;
.super Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
.source "MyNewsPage.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->MY_NEWS:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;)V

    .line 16
    return-void
.end method


# virtual methods
.method public getFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/news/MyNewsFragment;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/news/MyNewsFragment;-><init>()V

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->news_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
