.class final Lcom/google/apps/dots/android/newsstand/util/OffersUtil$2;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "OffersUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->makeEnsureNoWarmWelcomeCardIfOfferCardFilter()Lcom/google/android/libraries/bind/data/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 6
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 236
    .local v4, "warmWelcomeCardIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 241
    .local v2, "offerCardFound":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 242
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 243
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 244
    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 245
    .local v3, "resId":I
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardWarmWelcome;->LAYOUTS:[I

    # invokes: Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->containsResId([II)Z
    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->access$000([II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 246
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    .end local v3    # "resId":I
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 247
    .restart local v3    # "resId":I
    :cond_1
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->LAYOUTS:[I

    # invokes: Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->containsResId([II)Z
    invoke-static {v5, v3}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->access$000([II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 248
    const/4 v2, 0x1

    goto :goto_1

    .line 255
    .end local v0    # "cardData":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "resId":I
    :cond_2
    if-eqz v2, :cond_3

    .line 256
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_2
    if-ltz v1, :cond_3

    .line 257
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {p1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 256
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 260
    :cond_3
    return-object p1
.end method
