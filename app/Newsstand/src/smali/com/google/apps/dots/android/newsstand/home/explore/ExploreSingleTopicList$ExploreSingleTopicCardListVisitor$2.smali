.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "ExploreSingleTopicList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMagazineShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

.field final synthetic val$categoryId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;->this$1:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;->val$categoryId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 330
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 332
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;->val$categoryId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;->val$categoryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->setMagazineCategory(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;

    .line 335
    :cond_0
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/PlayStoreIntentBuilder;->start()V

    .line 336
    return-void
.end method
