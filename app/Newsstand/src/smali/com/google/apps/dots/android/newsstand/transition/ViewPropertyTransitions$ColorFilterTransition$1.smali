.class Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;
.super Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;
.source "ViewPropertyTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$endValue:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;->val$endValue:Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ViewPropertyProxy;-><init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$1;)V

    return-void
.end method


# virtual methods
.method public getPropertyValue(Landroid/view/View;)Ljava/lang/Integer;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;->val$endValue:Ljava/lang/Integer;

    return-object v0
.end method

.method public bridge synthetic getPropertyValue(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$1;->getPropertyValue(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTypeEvaluator()Landroid/animation/TypeEvaluator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/animation/TypeEvaluator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewPropertyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string v0, "colorFilter"

    return-object v0
.end method
