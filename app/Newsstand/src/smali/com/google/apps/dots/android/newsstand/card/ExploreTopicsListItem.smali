.class public Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "ExploreTopicsListItem.java"


# static fields
.field public static final DK_ENTITY_IMAGE_ID:I

.field public static final DK_ON_CLICK_LISTENER:I

.field public static final DK_TOPIC_NAME:I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsListItem_topicName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_TOPIC_NAME:I

    .line 15
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsListItem_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_ON_CLICK_LISTENER:I

    .line 16
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ExploreTopicsListItem_entityImageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->DK_ENTITY_IMAGE_ID:I

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->explore_topics_list_item:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/ExploreTopicsListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method
