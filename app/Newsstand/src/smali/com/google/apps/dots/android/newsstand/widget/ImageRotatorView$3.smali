.class Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "ImageRotatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshWhenPossible:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$400(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x0

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshWhenPossible:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->access$402(Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;Z)Z

    .line 100
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->refreshNow()V

    .line 102
    :cond_0
    return-void
.end method
