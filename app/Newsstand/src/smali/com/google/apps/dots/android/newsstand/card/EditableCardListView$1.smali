.class Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;
.super Ljava/lang/Object;
.source "EditableCardListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 107
    # getter for: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v2, "setEditOperation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$000(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->editedGroup:Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$200(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # getter for: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->pendingOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$000(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/card/CardGroup;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 109
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->isOperationRemove()Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$300(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;)Z

    move-result v0

    .line 110
    .local v0, "isRemove":Z
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateRemoveView(Z)V
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$400(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Z)V

    .line 111
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView$1;->this$0:Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;

    # invokes: Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->updateDraggableView(Z)V
    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->access$500(Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;Z)V

    .line 112
    return-void
.end method
