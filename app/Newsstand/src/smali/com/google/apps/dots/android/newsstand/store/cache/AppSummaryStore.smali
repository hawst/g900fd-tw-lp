.class public Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;
.super Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;
.source "AppSummaryStore.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
        ">;"
    }
.end annotation


# instance fields
.field private final manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;)V
    .locals 2
    .param p1, "nsStore"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p2, "manifestBlobResolver"    # Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .prologue
    .line 19
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->APPLICATION_SUMMARY:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore$1;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore$1;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/common/base/Supplier;)V

    .line 25
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    .line 26
    return-void
.end method


# virtual methods
.method public get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;

    invoke-virtual {v1, p2}, Lcom/google/apps/dots/android/newsstand/store/ManifestBlobResolver;->resolveAppSummary(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    .line 31
    .local v0, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-eqz v0, :cond_0

    .line 32
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 34
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/ProtoStoreBase;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
