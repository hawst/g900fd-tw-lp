.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "ExploreSingleTopicList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeInternationalLinkNode(Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

.field final synthetic val$exploreLinkSummary:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;->this$1:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;->val$exploreLinkSummary:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;->val$title:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x1

    .line 236
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;->val$exploreLinkSummary:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 237
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->getEntityId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;->val$title:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 238
    .local v0, "exploreTopic":Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;

    invoke-direct {v1, p2, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;)V

    .line 239
    invoke-virtual {v1, v4}, Lcom/google/apps/dots/android/newsstand/navigation/ExploreSingleTopicIntentBuilder;->start(Z)V

    .line 240
    return-void
.end method
