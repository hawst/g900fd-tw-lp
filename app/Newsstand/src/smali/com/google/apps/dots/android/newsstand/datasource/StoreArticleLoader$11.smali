.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getBaseHtmlFuture(Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 370
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "htmlFromFile"    # Ljava/lang/String;

    .prologue
    .line 373
    new-instance v0, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$11;->val$widget:Lcom/google/apps/dots/android/newsstand/widget/BaseArticleWidget;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;-><init>(Lcom/google/apps/dots/android/newsstand/widget/HtmlWidget;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/article/WebViewHtmlProcessor;->processBaseHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
