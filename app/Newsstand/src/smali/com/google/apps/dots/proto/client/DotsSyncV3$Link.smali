.class public final Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsSyncV3.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsSyncV3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Link"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;


# instance fields
.field private bitField0_:I

.field private expiresMillis_:J

.field private id_:Ljava/lang/String;

.field private linkType_:I

.field private uriType_:I

.field private uri_:Ljava/lang/String;

.field private version_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1547
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1548
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1560
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    .line 1582
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    .line 1601
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    .line 1623
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    .line 1642
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    .line 1661
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    .line 1548
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .locals 3

    .prologue
    .line 1695
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1699
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    return-object v0

    .line 1696
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :catch_0
    move-exception v1

    .line 1697
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1544
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1704
    if-ne p1, p0, :cond_1

    .line 1709
    :cond_0
    :goto_0
    return v1

    .line 1705
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1706
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 1707
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1709
    :goto_2
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 1707
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    .line 1709
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkType()I
    .locals 1

    .prologue
    .line 1584
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1751
    const/4 v0, 0x0

    .line 1752
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1753
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    .line 1754
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1756
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1757
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    .line 1758
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1760
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1761
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    .line 1762
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1764
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1765
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    .line 1766
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1768
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1769
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    .line 1770
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1772
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 1773
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    .line 1774
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1776
    :cond_5
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->cachedSize:I

    .line 1777
    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1603
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public getUriType()I
    .locals 1

    .prologue
    .line 1644
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    return v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 1625
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    return-wide v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 1573
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUri()Z
    .locals 1

    .prologue
    .line 1614
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v2, 0x0

    .line 1716
    const/16 v0, 0x11

    .line 1717
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1718
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 1719
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    add-int v0, v1, v3

    .line 1720
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 1721
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1722
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    add-int v0, v1, v2

    .line 1723
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1724
    return v0

    .line 1718
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1720
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1786
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1790
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1791
    :sswitch_0
    return-object p0

    .line 1796
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    .line 1797
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto :goto_0

    .line 1801
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1802
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v5, :cond_1

    if-eq v1, v6, :cond_1

    if-eq v1, v7, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    const/16 v2, 0xb

    if-eq v1, v2, :cond_1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_2

    .line 1815
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    .line 1816
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto :goto_0

    .line 1818
    :cond_2
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    goto :goto_0

    .line 1823
    .end local v1    # "temp":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    .line 1824
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto :goto_0

    .line 1828
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    .line 1829
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto :goto_0

    .line 1833
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1834
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-eq v1, v5, :cond_3

    if-eq v1, v6, :cond_3

    if-ne v1, v7, :cond_4

    .line 1838
    :cond_3
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    .line 1839
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto/16 :goto_0

    .line 1841
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    goto/16 :goto_0

    .line 1846
    .end local v1    # "temp":I
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    .line 1847
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    goto/16 :goto_0

    .line 1786
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1544
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1729
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1730
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->id_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1732
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1733
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->linkType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1735
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1736
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1738
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1739
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->version_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1741
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1742
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->uriType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1744
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 1745
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->expiresMillis_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1747
    :cond_5
    return-void
.end method
