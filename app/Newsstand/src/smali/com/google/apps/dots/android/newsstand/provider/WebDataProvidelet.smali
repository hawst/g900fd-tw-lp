.class public Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;
.super Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;
.source "WebDataProvidelet.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SPLIT_APP_SECTION_IDS:Lcom/google/common/base/Splitter;

.field private static final SPLIT_APP_SECTION_POST_FIELD_IDS:Lcom/google/common/base/Splitter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x2f

    .line 32
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 34
    invoke-static {v2}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/common/base/Splitter;->limit(I)Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->SPLIT_APP_SECTION_POST_FIELD_IDS:Lcom/google/common/base/Splitter;

    .line 35
    invoke-static {v2}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/common/base/Splitter;->limit(I)Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->SPLIT_APP_SECTION_IDS:Lcom/google/common/base/Splitter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/AbstractProvidelet;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->getAttachmentFD(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static getAttachmentFD(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p0, "attachmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    .line 114
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v3

    .line 113
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 115
    .local v0, "storeFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->extractAFD(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public static hashString(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 107
    invoke-static {}, Lcom/google/common/hash/Hashing;->md5()Lcom/google/common/hash/HashFunction;

    move-result-object v0

    sget-object v1, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v0, p0, v1}, Lcom/google/common/hash/HashFunction;->hashString(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/google/common/hash/HashCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/hash/HashCode;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContentType(ILandroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 39
    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method public openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;
    .locals 13
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .param p4, "contentProvider"    # Landroid/content/ContentProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 46
    packed-switch p1, :pswitch_data_0

    .line 101
    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 48
    :pswitch_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->SPLIT_APP_SECTION_POST_FIELD_IDS:Lcom/google/common/base/Splitter;

    .line 49
    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    .line 50
    .local v8, "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 52
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 53
    .local v5, "sectionId":Ljava/lang/String;
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 54
    .local v10, "postId":Ljava/lang/String;
    const/4 v0, 0x6

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "fieldId":Ljava/lang/String;
    const/4 v0, 0x7

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 58
    .local v3, "path":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 59
    .local v9, "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet$1;-><init>(Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-static {v9, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 83
    .local v6, "afdFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/content/res/AssetFileDescriptor;>;"
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->isNoPumpFd(Landroid/net/Uri;)Z

    move-result v0

    .line 82
    invoke-static {v6, v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 95
    .end local v2    # "fieldId":Ljava/lang/String;
    .end local v6    # "afdFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/content/res/AssetFileDescriptor;>;"
    .end local v9    # "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    .end local v10    # "postId":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 88
    .end local v3    # "path":Ljava/lang/String;
    .end local v5    # "sectionId":Ljava/lang/String;
    .end local v8    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->SPLIT_APP_SECTION_IDS:Lcom/google/common/base/Splitter;

    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    .line 89
    .restart local v8    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 91
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 92
    .restart local v5    # "sectionId":Ljava/lang/String;
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 93
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->hashString(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v4, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 94
    .local v7, "attachmentId":Ljava/lang/String;
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Mapping URI %s to attachment %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p2, v4, v11

    const/4 v11, 0x1

    aput-object v7, v4, v11

    invoke-virtual {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/provider/WebDataProvidelet;->getAttachmentFD(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 96
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->isNoPumpFd(Landroid/net/Uri;)Z

    move-result v1

    .line 95
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
