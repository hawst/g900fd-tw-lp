.class public Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
.super Ljava/lang/Object;
.source "ReadStateCollection.java"


# instance fields
.field private final readStatesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "readStatesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->readStatesMap:Ljava/util/Map;

    .line 26
    return-void
.end method

.method public static emptyCollection()Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static getCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .locals 5
    .param p0, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .param p2, "readStatesUri"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 80
    if-nez p2, :cond_0

    .line 81
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->emptyCollection()Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v3

    .line 95
    :goto_0
    return-object v3

    .line 83
    :cond_0
    const-string v3, "ReadStates"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, p2, v4}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 86
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v3

    invoke-virtual {v3, p0, p2}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .line 87
    .local v2, "response":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    if-eqz v2, :cond_1

    .line 88
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->lookupMeteredDuration(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/Edition;)J

    move-result-wide v0

    .line 89
    .local v0, "meteredDuration":J
    new-instance v3, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-static {v4, v0, v1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->getReadStatesMap(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;J)Ljava/util/Map;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_0

    .line 92
    .end local v0    # "meteredDuration":J
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->emptyCollection()Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 95
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    goto :goto_0

    .end local v2    # "response":Lcom/google/apps/dots/android/newsstand/store/MutationResponse;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    throw v3
.end method


# virtual methods
.method public getMeteredReadCount()I
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "count":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->readStatesMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 66
    .local v1, "readState":Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->isMeteredRead(Lcom/google/android/libraries/bind/data/Data;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    .end local v1    # "readState":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return v0
.end method

.method public getMostRecentPostReadState(Lcom/google/common/base/Predicate;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Predicate",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "postIdPredicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 48
    .local v1, "mostRecent":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    const-wide/16 v4, 0x0

    .line 49
    .local v4, "updateTimestamp":J
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->readStatesMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 50
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/bind/data/Data;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->getReadState(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v2

    .line 51
    .local v2, "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getUpdateTimestamp()J

    move-result-wide v8

    cmp-long v3, v8, v4

    if-lez v3, :cond_0

    .line 52
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/google/common/base/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    move-object v1, v2

    .line 54
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->getUpdateTimestamp()J

    move-result-wide v4

    goto :goto_0

    .line 57
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v2    # "postReadState":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :cond_1
    return-object v1
.end method

.method public getReadState(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 2
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->readStatesMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 40
    .local v0, "readStateData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateList;->getReadState(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRead(Ljava/lang/String;)Z
    .locals 1
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->readStatesMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
