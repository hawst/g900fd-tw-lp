.class public Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
.super Ljava/lang/Object;
.source "WebArticleFragmentState.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/reading/ArticleFragmentState;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final postTitle:Ljava/lang/String;

.field final postUrl:Ljava/lang/String;

.field final publisher:Ljava/lang/String;

.field final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 0
    .param p1, "postUrl"    # Ljava/lang/String;
    .param p2, "postTitle"    # Ljava/lang/String;
    .param p3, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "publisher"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 28
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 33
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;

    .line 35
    .local v0, "otherState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 37
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    .line 38
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 40
    .end local v0    # "otherState":Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toDataObject(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "primaryKey"    # I

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 91
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 92
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_TITLE:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 93
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_LINK_VIEW_POST_URL:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 94
    sget v1, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment;->DK_VIEW_TYPE:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;->LINK_VIEW:Lcom/google/apps/dots/android/newsstand/reading/ReadingFragment$ViewType;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 95
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->DK_SOURCE_NAME:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 96
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "{%s - %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->postUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/WebArticleFragmentState;->publisher:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    return-void
.end method
