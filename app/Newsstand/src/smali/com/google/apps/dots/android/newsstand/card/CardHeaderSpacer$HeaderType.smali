.class public final enum Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
.super Ljava/lang/Enum;
.source "CardHeaderSpacer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HeaderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

.field public static final enum NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;


# instance fields
.field private final heightResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "DEFAULT"

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->card_list_view_padding:I

    invoke-direct {v0, v1, v3, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 34
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "NONE"

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->header_spacer_none:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 35
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "EDITION_HEADER"

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->header_spacer_edition:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 36
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "MAGAZINE_HEADER"

    sget v2, Lcom/google/android/apps/newsstanddev/R$dimen;->magazine_edition_header_spacer_height:I

    invoke-direct {v0, v1, v6, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 38
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "ACTION_BAR"

    invoke-direct {v0, v1, v7}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    const-string v1, "LIBRARY_HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .line 32
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->DEFAULT:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->NONE:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->MAGAZINE_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ACTION_BAR:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->heightResId:I

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "heightResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->heightResId:I

    .line 45
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    return-object v0
.end method


# virtual methods
.method public getHeightPx(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->heightResId:I

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->heightResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 71
    .local v0, "height":I
    :goto_0
    return v0

    .line 56
    .end local v0    # "height":I
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$1;->$SwitchMap$com$google$apps$dots$android$newsstand$card$CardHeaderSpacer$HeaderType:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 68
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 58
    :pswitch_0
    const/4 v1, 0x0

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->header_bottom_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 58
    invoke-static {p1, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    .line 61
    .restart local v0    # "height":I
    goto :goto_0

    .line 63
    .end local v0    # "height":I
    :pswitch_1
    const/4 v1, 0x2

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->header_bottom_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 63
    invoke-static {p1, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    .line 66
    .restart local v0    # "height":I
    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
