.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;
.super Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;
.source "SettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupDeveloperPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;
    .param p2, "checkBoxPreference"    # Landroid/preference/CheckBoxPreference;
    .param p3, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0, p2, p3}, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;-><init>(Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getCurrentValue()Z
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v0

    return v0
.end method

.method protected setNewValue(Z)V
    .locals 3
    .param p1, "designerModeEnabled"    # Z

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->access$100(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setDesignerMode(Z)V

    .line 376
    if-eqz p1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$10;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->designer_mode_toast:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 379
    :cond_0
    return-void
.end method
