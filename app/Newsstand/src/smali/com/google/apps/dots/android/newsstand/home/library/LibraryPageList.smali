.class public Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "LibraryPageList.java"


# static fields
.field private static final DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

.field public static final DK_LIBRARY_PAGE:I

.field public static final DK_LIBRARY_TITLE:I


# instance fields
.field private prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->LibraryPageList_libraryPage:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->LibraryPageList_libraryTitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_TITLE:I

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_NEWS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_TOPICS_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    sget v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 42
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->buildLibraryPageList()Ljava/util/List;

    move-result-object v0

    .line 43
    .local v0, "libraryPageListData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v1, Lcom/google/android/libraries/bind/data/Snapshot;

    sget v2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    sget-object v2, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 47
    return-void
.end method


# virtual methods
.method buildLibraryPageList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    array-length v3, v3

    .line 51
    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 52
    .local v2, "libraryPageListData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 54
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aget-object v3, v3, v1

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->MY_MAGAZINES_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 52
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_0
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 59
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_PAGE:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aget-object v4, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 60
    sget v3, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_TITLE:I

    sget-object v4, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DEFAULT_LIBRARY_PAGES:[Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aget-object v4, v4, v1

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 61
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 60
    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 62
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_1
    return-object v2
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$2;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 5

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onRegisterForInvalidation()V

    .line 70
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "areMagazinesAvailable"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 77
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->onUnregisterForInvalidation()V

    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->prefsListener:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/util/Disposable;->dispose()V

    .line 84
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->setDirty(Z)V

    .line 85
    return-void
.end method
