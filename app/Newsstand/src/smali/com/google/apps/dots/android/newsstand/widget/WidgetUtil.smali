.class public Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;
.super Lcom/google/android/libraries/bind/widget/WidgetUtil;
.source "WidgetUtil.java"


# static fields
.field private static tempLocation:[I

.field private static tempRect:Landroid/graphics/Rect;

.field private static tempRect2:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    .line 21
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/libraries/bind/widget/WidgetUtil;-><init>()V

    return-void
.end method

.method public static checkConvertViewTag(Landroid/view/View;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p0, "convertView"    # Landroid/view/View;
    .param p1, "expectedTag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getTag(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p0    # "convertView":Landroid/view/View;
    :goto_0
    return-object p0

    .restart local p0    # "convertView":Landroid/view/View;
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static checkConvertViewTagClass(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;
    .locals 1
    .param p0, "convertView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<*>;)TT;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "expectedTagClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getTagClass(Landroid/view/View;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p0    # "convertView":Landroid/view/View;
    :goto_0
    return-object p0

    .restart local p0    # "convertView":Landroid/view/View;
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static doViewsIntersect(Landroid/view/View;Landroid/view/View;)Z
    .locals 8
    .param p0, "view1"    # Landroid/view/View;
    .param p1, "view2"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 94
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 95
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v1, v1, v6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v2, v2, v7

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v3, v3, v6

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v4, v4, v7

    .line 96
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 95
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 97
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 98
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v1, v1, v6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v2, v2, v7

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v3, v3, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempLocation:[I

    aget v4, v4, v7

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 98
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 100
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public static findViewInParent(Landroid/view/View;Ljava/lang/Class;I)Landroid/view/View;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p2, "findViewId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<*>;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "parentClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 115
    :goto_0
    return-object v2

    .line 107
    :cond_0
    move-object v0, p0

    .line 108
    .local v0, "currentView":Landroid/view/View;
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 109
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 110
    .local v1, "parent":Landroid/view/View;
    move-object v0, v1

    .line 111
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    .end local v1    # "parent":Landroid/view/View;
    :cond_2
    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    goto :goto_0
.end method

.method public static getAllDescendants(Landroid/view/ViewGroup;Lcom/google/common/base/Predicate;)Ljava/util/Collection;
    .locals 1
    .param p0, "root"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/google/common/base/Predicate",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "optPredicate":Lcom/google/common/base/Predicate;, "Lcom/google/common/base/Predicate<Landroid/view/View;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/play/onboard/OnboardUtils;->getAllDescendants(Landroid/view/ViewGroup;Lcom/google/android/play/onboard/OnboardUtils$Predicate;)Ljava/util/Collection;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil$1;-><init>(Lcom/google/common/base/Predicate;)V

    goto :goto_0
.end method

.method public static varargs getFirstViewWithProperty(Landroid/view/ViewGroup;Ljava/lang/Class;Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .locals 8
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "value"    # Ljava/lang/Object;
    .param p3, "keys"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/bind/data/BindingViewGroup;",
            ">(",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            "[I)TT;"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 126
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 127
    .local v3, "outer":Landroid/view/View;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v5, v3

    .line 128
    check-cast v5, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v5}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 129
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    array-length v6, p3

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_1

    aget v2, p3, v5

    .line 130
    .local v2, "key":I
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 131
    check-cast v3, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .line 142
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "key":I
    .end local v3    # "outer":Landroid/view/View;
    :goto_2
    return-object v3

    .line 129
    .restart local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "key":I
    .restart local v3    # "outer":Landroid/view/View;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 135
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "key":I
    :cond_1
    if-eqz v3, :cond_2

    instance-of v5, v3, Landroid/view/ViewGroup;

    if-eqz v5, :cond_2

    .line 136
    check-cast v3, Landroid/view/ViewGroup;

    .end local v3    # "outer":Landroid/view/View;
    invoke-static {v3, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getFirstViewWithProperty(Landroid/view/ViewGroup;Ljava/lang/Class;Ljava/lang/Object;[I)Lcom/google/android/libraries/bind/data/BindingViewGroup;

    move-result-object v4

    .line 137
    .local v4, "result":Lcom/google/android/libraries/bind/data/BindingViewGroup;, "TT;"
    if-eqz v4, :cond_2

    move-object v3, v4

    .line 138
    goto :goto_2

    .line 125
    .end local v4    # "result":Lcom/google/android/libraries/bind/data/BindingViewGroup;, "TT;"
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static getTag(Landroid/view/View;)Ljava/lang/Object;
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 56
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTagClass(Landroid/view/View;)Ljava/lang/Class;
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->getTag(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    .local v0, "tag":Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0
.end method

.method public static getTypedAncestor(Ljava/lang/Class;Landroid/view/View;)Landroid/view/View;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/view/View;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, "klass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v2, 0x0

    .line 81
    move-object v0, p1

    .line 82
    .local v0, "current":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_2

    .line 83
    invoke-virtual {p0, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    .end local v0    # "current":Landroid/view/View;
    :goto_1
    return-object v0

    .line 86
    .restart local v0    # "current":Landroid/view/View;
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 87
    .local v1, "parent":Landroid/view/ViewParent;
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/view/View;

    .end local v1    # "parent":Landroid/view/ViewParent;
    move-object v0, v1

    .line 88
    :goto_2
    goto :goto_0

    .restart local v1    # "parent":Landroid/view/ViewParent;
    :cond_1
    move-object v0, v2

    .line 87
    goto :goto_2

    .end local v1    # "parent":Landroid/view/ViewParent;
    :cond_2
    move-object v0, v2

    .line 89
    goto :goto_1
.end method

.method public static isVisible(Landroid/view/View;)Z
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-virtual {p0}, Landroid/view/View;->getWindowVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    :goto_0
    return v1

    .line 45
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 46
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_2

    move-object p0, v0

    .line 47
    check-cast p0, Landroid/view/View;

    .line 41
    .end local v0    # "parent":Landroid/view/ViewParent;
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 52
    .restart local v0    # "parent":Landroid/view/ViewParent;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static removeViewFromParent(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 26
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 29
    :cond_0
    return-void
.end method

.method private static viewHierarchyToString(Landroid/view/View;Ljava/lang/String;I)Ljava/lang/String;
    .locals 11
    .param p0, "view"    # Landroid/view/View;
    .param p1, "hierarchyString"    # Ljava/lang/String;
    .param p2, "depth"    # I

    .prologue
    .line 174
    const-string v3, "\n"

    .line 175
    .local v3, "tabs":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, p2, :cond_0

    .line 176
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 178
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->viewToString(Landroid/view/View;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 179
    instance-of v5, p0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_1

    move-object v4, p0

    .line 180
    check-cast v4, Landroid/view/ViewGroup;

    .line 181
    .local v4, "viewGroup":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 182
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 183
    .local v0, "child":Landroid/view/View;
    add-int/lit8 v5, p2, 0x1

    invoke-static {v0, p1, v5}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->viewHierarchyToString(Landroid/view/View;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 186
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v4    # "viewGroup":Landroid/view/ViewGroup;
    :cond_1
    return-object p1
.end method

.method public static viewHierarchyToString(Landroid/view/ViewGroup;)Ljava/lang/String;
    .locals 2
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 170
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->viewHierarchyToString(Landroid/view/View;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static viewToString(Landroid/view/View;)Ljava/lang/String;
    .locals 4
    .param p0, "child"    # Landroid/view/View;

    .prologue
    .line 160
    if-nez p0, :cond_0

    .line 161
    const-string v0, "null"

    .line 163
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
