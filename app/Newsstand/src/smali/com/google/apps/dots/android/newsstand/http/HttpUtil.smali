.class public Lcom/google/apps/dots/android/newsstand/http/HttpUtil;
.super Ljava/lang/Object;
.source "HttpUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static date(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;
    .locals 7
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 24
    const-string v2, "Date"

    invoke-interface {p0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 25
    .local v0, "dateHeader":Lorg/apache/http/Header;
    if-eqz v0, :cond_0

    .line 27
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 32
    :goto_0
    return-object v2

    .line 28
    :catch_0
    move-exception v1

    .line 29
    .local v1, "e":Lorg/apache/http/impl/cookie/DateParseException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Can\'t parse date header: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    .end local v1    # "e":Lorg/apache/http/impl/cookie/DateParseException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static expiration(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;
    .locals 22
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 51
    const-string v12, "Cache-Control"

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 52
    .local v2, "cacheControlHeader":Lorg/apache/http/Header;
    if-eqz v2, :cond_1

    .line 53
    sget-object v12, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "Found cache-control header: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    invoke-interface {v2}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v13

    array-length v14, v13

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v14, :cond_1

    aget-object v4, v13, v12

    .line 55
    .local v4, "element":Lorg/apache/http/HeaderElement;
    const-string v15, "max-age"

    invoke-interface {v4}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 56
    invoke-static/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->date(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v11

    .line 57
    .local v11, "serverDate":Ljava/lang/Long;
    if-eqz v11, :cond_0

    .line 59
    :try_start_0
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    invoke-interface {v4}, Lorg/apache/http/HeaderElement;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    mul-long v18, v18, v20

    add-long v6, v16, v18

    .line 60
    .local v6, "expiration":J
    sget-object v15, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v16, "Computed expiration: %tF %tT%tz"

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 83
    .end local v4    # "element":Lorg/apache/http/HeaderElement;
    .end local v6    # "expiration":J
    .end local v11    # "serverDate":Ljava/lang/Long;
    :goto_1
    return-object v12

    .line 62
    .restart local v4    # "element":Lorg/apache/http/HeaderElement;
    .restart local v11    # "serverDate":Ljava/lang/Long;
    :catch_0
    move-exception v10

    .line 63
    .local v10, "nfe":Ljava/lang/NumberFormatException;
    sget-object v15, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v16, "Can\'t parse cache-control header: %s"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v10, v0, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    .end local v10    # "nfe":Ljava/lang/NumberFormatException;
    .end local v11    # "serverDate":Ljava/lang/Long;
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 71
    .end local v4    # "element":Lorg/apache/http/HeaderElement;
    :cond_1
    const-string v12, "Expires"

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    .line 72
    .local v5, "expiresHeader":Lorg/apache/http/Header;
    if-eqz v5, :cond_2

    .line 73
    sget-object v12, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "Found expires header: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    :try_start_1
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 76
    .local v8, "expires":J
    sget-object v12, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "Computed expiration: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    goto :goto_1

    .line 78
    .end local v8    # "expires":J
    :catch_1
    move-exception v3

    .line 79
    .local v3, "e":Lorg/apache/http/impl/cookie/DateParseException;
    sget-object v12, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v13, "Can\'t parse date expires: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v3, v13, v14}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    .end local v3    # "e":Lorg/apache/http/impl/cookie/DateParseException;
    :cond_2
    const/4 v12, 0x0

    goto :goto_1
.end method

.method public static getFirstHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "response"    # Lorg/apache/http/HttpResponse;
    .param p1, "headerName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-interface {p0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 20
    .local v0, "h":Lorg/apache/http/Header;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static lastModified(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;
    .locals 7
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 36
    const-string v2, "Last-Modified"

    invoke-interface {p0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 37
    .local v1, "modifiedHeader":Lorg/apache/http/Header;
    if-eqz v1, :cond_0

    .line 39
    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 44
    :goto_0
    return-object v2

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Lorg/apache/http/impl/cookie/DateParseException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/http/HttpUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Can\'t parse date header: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    .end local v0    # "e":Lorg/apache/http/impl/cookie/DateParseException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
