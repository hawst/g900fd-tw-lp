.class Lcom/google/apps/dots/android/newsstand/sync/Syncer$SkippedTaskException;
.super Lcom/google/apps/dots/android/newsstand/sync/SyncException;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SkippedTaskException"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "taskType"    # Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 159
    const-string v0, "Skipped %s \'%s\'. Reason: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/sync/SyncException;-><init>(Ljava/lang/String;)V

    .line 160
    return-void
.end method
