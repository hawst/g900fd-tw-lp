.class public Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "EditionIntentBuilder.java"


# instance fields
.field private edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private sharedElement:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 32
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 10

    .prologue
    .line 46
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v6, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 110
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v6}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v6

    .line 50
    :pswitch_0
    new-instance v6, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v7, Lcom/google/apps/dots/android/newsstand/home/HomePage;->READ_NOW_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 51
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v6

    .line 52
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    .line 107
    :cond_0
    :goto_0
    return-object v3

    .line 55
    :pswitch_1
    new-instance v6, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v7, Lcom/google/apps/dots/android/newsstand/home/HomePage;->SAVED_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 56
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v6

    .line 57
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    goto :goto_0

    .line 60
    :pswitch_2
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    new-instance v7, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v7, v8}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-direct {v1, v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;)V

    .line 62
    .local v1, "headerEditionIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    if-eqz v6, :cond_1

    .line 63
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    invoke-virtual {v1, v6}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;

    .line 64
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;->sharedElementPairs()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->copySceneTransitionPairs(Ljava/util/List;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 66
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    goto :goto_0

    .line 69
    .end local v1    # "headerEditionIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/HeaderEditionIntentBuilder;
    :pswitch_3
    const-class v6, Lcom/google/apps/dots/android/newsstand/activity/HeaderEditionActivity;

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    .line 70
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v5, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    .line 71
    .local v5, "sectionEdition":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    const-string v6, "HeaderEditionFragment_state"

    new-instance v7, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;

    .line 72
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v8

    invoke-direct {v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 71
    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 73
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    if-eqz v6, :cond_0

    .line 74
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    sget v8, Lcom/google/android/apps/newsstanddev/R$string;->expando_hero:I

    .line 75
    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 74
    invoke-virtual {p0, v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->addSceneTransitionPair(Landroid/view/View;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    goto :goto_0

    .line 80
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "sectionEdition":Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;
    :pswitch_4
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v0, v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 81
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    if-eqz v6, :cond_2

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_2

    .line 82
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    invoke-virtual {v0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->setTransitionHeroElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;

    .line 83
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->sharedElementPairs()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->copySceneTransitionPairs(Ljava/util/List;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 85
    :cond_2
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    goto/16 :goto_0

    .line 88
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/MagazineEditionIntentBuilder;
    :pswitch_5
    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    new-instance v7, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    sget-object v9, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    invoke-direct {v2, v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V

    .line 91
    .local v2, "headerPlainEditionIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    if-eqz v6, :cond_3

    .line 92
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    invoke-virtual {v2, v6}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;

    .line 93
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->sharedElementPairs()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->copySceneTransitionPairs(Ljava/util/List;)Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;

    .line 95
    :cond_3
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    goto/16 :goto_0

    .line 98
    .end local v2    # "headerPlainEditionIntentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/HeaderPlainEditionIntentBuilder;
    :pswitch_6
    const-class v6, Lcom/google/apps/dots/android/newsstand/activity/PlainEditionActivity;

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    .line 99
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v6, "PlainEditionFragment_state"

    new-instance v7, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v7, v8}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 103
    .end local v3    # "intent":Landroid/content/Intent;
    :pswitch_7
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v4, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    .line 104
    .local v4, "searchPostsEdition":Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 105
    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->setQuery(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    move-result-object v6

    sget-object v7, Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;->LIVE_POSTS:Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;

    .line 106
    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->setSearchType(Lcom/google/apps/dots/android/newsstand/search/SearchResultsList$Type;)Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;

    move-result-object v6

    .line 107
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/navigation/SearchIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    goto/16 :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 36
    return-object p0
.end method

.method public setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;
    .locals 0
    .param p1, "sharedElement"    # Landroid/view/View;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->sharedElement:Landroid/view/View;

    .line 41
    return-object p0
.end method
