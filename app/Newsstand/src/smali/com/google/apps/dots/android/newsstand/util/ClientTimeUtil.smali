.class public Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;
.super Ljava/lang/Object;
.source "ClientTimeUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static clockSkew()J
    .locals 4

    .prologue
    .line 41
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "clockSkew"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static serverNow()J
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->toServerTime(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static setClockSkew(J)V
    .locals 2
    .param p0, "clockSkew"    # J

    .prologue
    .line 45
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    const-string v1, "clockSkew"

    invoke-virtual {v0, v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 46
    return-void
.end method

.method public static toServerTime(J)J
    .locals 2
    .param p0, "clientTime"    # J

    .prologue
    .line 33
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->clockSkew()J

    move-result-wide v0

    add-long/2addr v0, p0

    return-wide v0
.end method

.method public static updateClockSkew(J)V
    .locals 6
    .param p0, "serverTimeNow"    # J

    .prologue
    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, p0, v2

    .line 23
    .local v0, "clockSkew":J
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->clockSkew()J

    move-result-wide v2

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 24
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->setClockSkew(J)V

    .line 26
    :cond_0
    return-void
.end method
