.class final Lcom/google/apps/dots/android/newsstand/edition/Edition$3;
.super Ljava/lang/Object;
.source "Edition.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/Edition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 338
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->parseFrom([B)Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 339
    .local v1, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->setTitleHint(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    return-object v1

    .line 341
    .end local v1    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition$3;->createFromParcel(Landroid/os/Parcel;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 348
    new-array v0, p1, [Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition$3;->newArray(I)[Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method
