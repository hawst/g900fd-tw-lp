.class public final Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;
.super Ljava/lang/Object;
.source "RenderSourceImpl.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/reading/RenderSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$RenderSourceFactory;
    }
.end annotation


# instance fields
.field private final articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private final baseHtmlLoader:Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;

.field private final itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

.field private final jsonStoreLoader:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

.field private final originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private serializedPostDataFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;)V
    .locals 11
    .param p1, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "articleLoader"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p4, "optPostIndex"    # Ljava/lang/Integer;
    .param p5, "baseHtmlLoader"    # Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;
    .param p6, "jsonStoreHelper"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;
    .param p7, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p8, "itemJsonSerializer"    # Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 125
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 126
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .line 127
    invoke-static/range {p5 .. p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->baseHtmlLoader:Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;

    .line 128
    invoke-static/range {p7 .. p7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 129
    invoke-static/range {p8 .. p8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    .line 130
    invoke-static/range {p6 .. p6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    .line 134
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 135
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    .line 136
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v3

    .line 137
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v4

    .line 138
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->serverUris()Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    move-result-object v5

    .line 140
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->getSubscriptionType(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;

    move-result-object v7

    .line 143
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v10

    move-object v6, p3

    move-object v8, p4

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/home/library/news/NewsSubscriptionsList$SubscriptionType;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->jsonStoreLoader:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    .line 146
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->loadSerializedPostData()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 147
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "x1"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "x2"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .param p4, "x3"    # Ljava/lang/Integer;
    .param p5, "x4"    # Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;
    .param p6, "x5"    # Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;
    .param p7, "x6"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p8, "x7"    # Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
    .param p9, "x8"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;

    .prologue
    .line 31
    invoke-direct/range {p0 .. p8}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Ljava/lang/Integer;Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;Lcom/google/apps/dots/android/newsstand/reading/JsonStoreHelper;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;)Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->itemJsonSerializer:Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    return-object v0
.end method


# virtual methods
.method public getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method public loadJsonStore()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lorg/codehaus/jackson/node/ObjectNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->jsonStoreLoader:Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/JsonStoreLoader;->load(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public loadPost()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized loadSerializedPostData()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->serializedPostDataFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->serializedPostDataFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getPostFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 214
    .local v1, "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->articleLoader:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getFormFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 216
    .local v0, "formFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Form;>;"
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 217
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->whenAllDone([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl$1;-><init>(Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    .line 216
    invoke-static {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->serializedPostDataFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 229
    .end local v0    # "formFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Form;>;"
    .end local v1    # "postFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/proto/client/DotsShared$Post;>;"
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->serializedPostDataFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 212
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public loadWebviewBaseHtml()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->baseHtmlLoader:Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/RenderSourceImpl;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/reading/ArticleBaseHtmlLoader;->load(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
