.class Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;
.super Ljava/lang/Object;
.source "NSContentInputStreamProvider.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureFallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->getStoreResponse(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureFallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

.field final synthetic val$attachmentId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;->this$0:Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;->val$attachmentId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;->this$0:Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;

    # getter for: Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;->access$000(Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentInputStreamProvider$2;->val$attachmentId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
