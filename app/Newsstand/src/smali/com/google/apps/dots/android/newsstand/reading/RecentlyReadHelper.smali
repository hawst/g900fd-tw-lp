.class public Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;
.super Ljava/lang/Object;
.source "RecentlyReadHelper.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 0
    .param p1, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 28
    return-void
.end method

.method private isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z
    .locals 1
    .param p1, "item"    # Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->hasEdition()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromProto(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private storeRecentlyRead(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "recentlyRead"    # Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v1, "recentlyReadEditions"

    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setString(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method


# virtual methods
.method public loadRecentlyRead(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 31
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;-><init>()V

    .line 33
    .local v1, "result":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    const-string v3, "recentlyReadEditions"

    invoke-virtual {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getString(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->decodeBase64(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-object v1

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public markAsRecentlyRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 56
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 57
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->loadRecentlyRead(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;

    move-result-object v3

    .line 61
    .local v3, "recentlyRead":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
    const/4 v2, -0x1

    .line 62
    .local v2, "leastRecentlyReadIndex":I
    const-wide v4, 0x7fffffffffffffffL

    .line 63
    .local v4, "leastRecentlyReadTime":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    array-length v6, v6

    if-ge v0, v6, :cond_2

    .line 64
    iget-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    aget-object v1, v6, v0

    .line 65
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getLastRead()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    .line 66
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->getLastRead()J

    move-result-wide v4

    .line 67
    move v2, v0

    .line 69
    :cond_0
    invoke-direct {p0, v1, p2}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->isForEdition(Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->setLastRead(J)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    .line 72
    invoke-direct {p0, p1, v3}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->storeRecentlyRead(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;)V

    .line 86
    :goto_1
    return-void

    .line 63
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    :cond_2
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;-><init>()V

    .line 78
    .restart local v1    # "item":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->toProto()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->setLastRead(J)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    .line 80
    iget-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    array-length v6, v6

    const/16 v7, 0x64

    if-lt v6, v7, :cond_3

    if-ltz v2, :cond_3

    .line 81
    iget-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    aput-object v1, v6, v2

    .line 85
    :goto_2
    invoke-direct {p0, p1, v3}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;->storeRecentlyRead(Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;)V

    goto :goto_1

    .line 83
    :cond_3
    iget-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    invoke-static {v6, v1}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    iput-object v6, v3, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;->item:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    goto :goto_2
.end method

.method public recentlyReadList()Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadList;-><init>(Lcom/google/apps/dots/android/newsstand/reading/RecentlyReadHelper;)V

    return-object v0
.end method
