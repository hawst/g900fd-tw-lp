.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;
.super Ljava/lang/Object;
.source "ExploreSingleTopicHeaderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->showList(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

.field final synthetic val$fragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->val$fragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 484
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->val$fragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;->setVisibility(I)V

    .line 485
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->val$fragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->listViewBackground(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;)Landroid/view/View;

    move-result-object v0

    .line 486
    .local v0, "listViewBackground":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 487
    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    .line 489
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v4

    const/4 v3, 0x1

    sget-object v4, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate$3;->val$fragment:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment;

    .line 490
    # invokes: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;->access$900(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicHeaderFragment$Delegate;Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    .line 487
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 491
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 492
    return-void
.end method
