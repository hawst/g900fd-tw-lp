.class Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "SyncerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/service/SyncerService;->handleIntent(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final synthetic val$failureCount:[I

.field final synthetic val$pending:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

.field final synthetic val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;[ILcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/service/SyncerService;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->this$0:Lcom/google/apps/dots/android/newsstand/service/SyncerService;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$pending:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$failureCount:[I

    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$userWriteToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 354
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->val$pending:Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$PendingOp;->start()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 355
    .local v0, "pendingResult":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    new-instance v1, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/service/SyncerService$1;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
