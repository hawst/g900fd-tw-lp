.class public abstract Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;
.super Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;
.source "NSClientCommand.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/http/ResponseHandler",
        "<TT;>;",
        "Ljava/util/concurrent/Callable",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lorg/apache/http/client/HttpClient;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "client"    # Lorg/apache/http/client/HttpClient;

    .prologue
    .line 21
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;, "Lcom/google/apps/dots/android/newsstand/http/NSClientCommand<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/http/ResponseHandler;-><init>(Landroid/accounts/Account;Lorg/apache/http/client/HttpClient;)V

    .line 22
    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;, "Lcom/google/apps/dots/android/newsstand/http/NSClientCommand<TT;>;"
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/http/NSClientCommand;->execute()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract handleNoContent(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation
.end method

.method protected abstract handleOk(Lorg/apache/http/HttpEntity;Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpEntity;",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/sync/SyncException;
        }
    .end annotation
.end method
