.class public Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
.super Ljava/lang/Object;
.source "DiskBlob.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final offset:J

.field public final raf:Ljava/io/RandomAccessFile;

.field public final size:J


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;JJ)V
    .locals 0
    .param p1, "raf"    # Ljava/io/RandomAccessFile;
    .param p2, "offset"    # J
    .param p4, "size"    # J

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->raf:Ljava/io/RandomAccessFile;

    .line 21
    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->offset:J

    .line 22
    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->size:J

    .line 23
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 36
    return-void
.end method

.method public createInputStream()Ljava/io/InputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->raf:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->offset:J

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;->size:J

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/io/RafInputStream;-><init>(Ljava/io/RandomAccessFile;JJ)V

    return-object v0
.end method
