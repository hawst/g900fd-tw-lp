.class public final Lcom/google/apps/dots/proto/client/DotsShared$Ad;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Ad"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Ad;


# instance fields
.field private bitField0_:I

.field private explanation_:Ljava/lang/String;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30781
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30782
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30791
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    .line 30810
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    .line 30782
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    .locals 3

    .prologue
    .line 30843
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30847
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    return-object v0

    .line 30844
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    :catch_0
    move-exception v1

    .line 30845
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30778
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30852
    if-ne p1, p0, :cond_1

    .line 30856
    :cond_0
    :goto_0
    return v1

    .line 30853
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 30854
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    .line 30855
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 30856
    goto :goto_0

    .line 30855
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    .line 30856
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 30879
    const/4 v0, 0x0

    .line 30880
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30881
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    .line 30882
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30884
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 30885
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    .line 30886
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30888
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->cachedSize:I

    .line 30889
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 30860
    const/16 v0, 0x11

    .line 30861
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 30862
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    add-int v0, v1, v2

    .line 30863
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 30864
    return v0

    .line 30863
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Ad;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 30897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 30898
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 30902
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 30903
    :sswitch_0
    return-object p0

    .line 30908
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 30909
    .local v1, "temp":I
    if-ne v1, v3, :cond_1

    .line 30910
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    .line 30911
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    goto :goto_0

    .line 30913
    :cond_1
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    goto :goto_0

    .line 30918
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    .line 30919
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    goto :goto_0

    .line 30898
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30778
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30869
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30870
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 30872
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 30873
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->explanation_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 30875
    :cond_1
    return-void
.end method
