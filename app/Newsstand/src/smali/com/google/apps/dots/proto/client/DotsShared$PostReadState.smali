.class public final Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostReadState"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;


# instance fields
.field private bitField0_:I

.field private isPostInMeteredSection_:Z

.field private pageFraction_:F

.field private postId_:Ljava/lang/String;

.field private state_:I

.field private updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

.field private updateTimestamp_:J

.field private wasEditionOwnedWhenRead_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25899
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25900
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25911
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    .line 25933
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    .line 25971
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    .line 25990
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    .line 26009
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    .line 26028
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    .line 25900
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 3

    .prologue
    .line 26063
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26067
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v2, :cond_0

    .line 26068
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 26070
    :cond_0
    return-object v0

    .line 26064
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    :catch_0
    move-exception v1

    .line 26065
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25896
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26075
    if-ne p1, p0, :cond_1

    .line 26080
    :cond_0
    :goto_0
    return v1

    .line 26076
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 26077
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 26078
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v3, :cond_3

    .line 26080
    :goto_2
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 26078
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 26080
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getIsPostInMeteredSection()Z
    .locals 1

    .prologue
    .line 26011
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    return v0
.end method

.method public getPageFraction()F
    .locals 1

    .prologue
    .line 25992
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    return v0
.end method

.method public getPostId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25913
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 26127
    const/4 v0, 0x0

    .line 26128
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 26129
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    .line 26130
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26132
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 26133
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    .line 26134
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 26136
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v1, :cond_2

    .line 26137
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 26138
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26140
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 26141
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    .line 26142
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 26144
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 26145
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    .line 26146
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 26148
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 26149
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    .line 26150
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 26152
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 26153
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    .line 26154
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 26156
    :cond_6
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->cachedSize:I

    .line 26157
    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 25935
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    return v0
.end method

.method public getUpdateTimestamp()J
    .locals 2

    .prologue
    .line 25973
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    return-wide v0
.end method

.method public getWasEditionOwnedWhenRead()Z
    .locals 1

    .prologue
    .line 26030
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    return v0
.end method

.method public hasPageFraction()Z
    .locals 1

    .prologue
    .line 26000
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26088
    const/16 v0, 0x11

    .line 26089
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 26090
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 26091
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    add-int v0, v1, v5

    .line 26092
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v5, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 26093
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    const/16 v2, 0x20

    ushr-long/2addr v8, v2

    xor-long/2addr v6, v8

    long-to-int v2, v6

    add-int v0, v1, v2

    .line 26094
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 26095
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    add-int v0, v2, v1

    .line 26096
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    if-eqz v2, :cond_3

    :goto_3
    add-int v0, v1, v3

    .line 26097
    return v0

    .line 26090
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 26092
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    move v1, v4

    .line 26095
    goto :goto_2

    :cond_3
    move v3, v4

    .line 26096
    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 26166
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 26170
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 26171
    :sswitch_0
    return-object p0

    .line 26176
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    .line 26177
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26181
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 26182
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 26185
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    .line 26186
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26188
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    goto :goto_0

    .line 26193
    .end local v1    # "temp":I
    :sswitch_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-nez v2, :cond_3

    .line 26194
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    .line 26196
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 26200
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    .line 26201
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26205
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    .line 26206
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26210
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    .line 26211
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26215
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    .line 26216
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    goto :goto_0

    .line 26166
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2d -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25896
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v0

    return-object v0
.end method

.method public setIsPostInMeteredSection(Z)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 26014
    iput-boolean p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    .line 26015
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 26016
    return-object p0
.end method

.method public setPageFraction(F)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 25995
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    .line 25996
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 25997
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 25916
    if-nez p1, :cond_0

    .line 25917
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25919
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    .line 25920
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 25921
    return-object p0
.end method

.method public setState(I)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 25938
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    .line 25939
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 25940
    return-object p0
.end method

.method public setUpdateTimestamp(J)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 25976
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    .line 25977
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 25978
    return-object p0
.end method

.method public setWasEditionOwnedWhenRead(Z)Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 26033
    iput-boolean p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    .line 26034
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    .line 26035
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26102
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 26103
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->postId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 26105
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 26106
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->state_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 26108
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    if-eqz v0, :cond_2

    .line 26109
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTime_:Lcom/google/apps/dots/proto/client/DotsShared$ClientTime;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 26111
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 26112
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->updateTimestamp_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 26114
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 26115
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->pageFraction_:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 26117
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 26118
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->isPostInMeteredSection_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 26120
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 26121
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->wasEditionOwnedWhenRead_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 26123
    :cond_6
    return-void
.end method
