.class public final enum Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;
.super Ljava/lang/Enum;
.source "ProtoEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/util/ProtoEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LabId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

.field public static final enum ARTICLE_AUTO_PULLQUOTES:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

.field public static final enum ENABLE_MINDREADER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

.field public static final enum NEW_USER_READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

.field public static final enum UNKNOWN_LAB:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;


# instance fields
.field public final labId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 334
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    const-string v1, "UNKNOWN_LAB"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->UNKNOWN_LAB:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    .line 335
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    const-string v1, "NEW_USER_READ_NOW"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->NEW_USER_READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    .line 336
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    const-string v1, "ENABLE_MINDREADER"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ENABLE_MINDREADER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    .line 337
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    const-string v1, "ARTICLE_AUTO_PULLQUOTES"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ARTICLE_AUTO_PULLQUOTES:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    .line 333
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->UNKNOWN_LAB:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->NEW_USER_READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ENABLE_MINDREADER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->ARTICLE_AUTO_PULLQUOTES:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "labId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 351
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->labId:I

    .line 352
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 333
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LabId;

    return-object v0
.end method
