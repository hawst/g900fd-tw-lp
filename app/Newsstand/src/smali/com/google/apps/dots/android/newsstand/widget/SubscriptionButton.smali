.class public Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;
.super Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;
.source "SubscriptionButton.java"


# static fields
.field public static final APPEARANCE_CARD:I = 0x0

.field public static final APPEARANCE_PHOTO:I = 0x1

.field private static final LAYOUT_PARAMS:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field private appearance:I

.field private bindIsSubscribedKey:Ljava/lang/Integer;

.field private currentIconResId:I

.field private iconView:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

.field private subscribed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 25
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->LAYOUT_PARAMS:Landroid/widget/FrameLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$layout;->subscription_button:I

    invoke-virtual {v3, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 50
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->LAYOUT_PARAMS:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->icon:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->iconView:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .line 53
    sget-object v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SubscriptionButton:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    .local v0, "a":Landroid/content/res/TypedArray;
    if-nez v0, :cond_2

    move v1, v2

    .line 59
    .local v1, "appearance":I
    :goto_0
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    :cond_0
    sget-object v3, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    if-nez v0, :cond_3

    .line 66
    .local v2, "subscribed":Z
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->setState(IZ)V

    .line 68
    if-eqz v0, :cond_1

    .line 70
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable_bindIsSubscribed:I

    .line 71
    invoke-static {v0, v3}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->bindIsSubscribedKey:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    :cond_1
    return-void

    .line 57
    .end local v1    # "appearance":I
    .end local v2    # "subscribed":Z
    :cond_2
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->SubscriptionButton_appearance:I

    .line 58
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    goto :goto_0

    .line 64
    .restart local v1    # "appearance":I
    :cond_3
    sget v3, Lcom/google/android/apps/newsstanddev/R$styleable;->Subscribable_isSubscribed:I

    .line 65
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    goto :goto_1
.end method

.method protected static pickIconResId(IZ)I
    .locals 1
    .param p0, "appearance"    # I
    .param p1, "subscribed"    # Z

    .prologue
    .line 120
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 121
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_check_circle_wht_24dp:I

    .line 125
    :goto_0
    return v0

    .line 121
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_add_circle_outline_wht_24dp:I

    goto :goto_0

    .line 125
    :cond_1
    if-eqz p1, :cond_2

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_check_circle_card_24dp:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_add_circle_card_24dp:I

    goto :goto_0
.end method


# virtual methods
.method public getAppearance()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->appearance:I

    return v0
.end method

.method public isSubscribed()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->subscribed:Z

    return v0
.end method

.method public setAppearance(I)V
    .locals 1
    .param p1, "appearance"    # I

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->isSubscribed()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->setState(IZ)V

    .line 83
    return-void
.end method

.method protected setState(IZ)V
    .locals 4
    .param p1, "appearance"    # I
    .param p2, "subscribed"    # Z

    .prologue
    .line 98
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->appearance:I

    .line 99
    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->subscribed:Z

    .line 101
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->pickIconResId(IZ)I

    move-result v0

    .line 102
    .local v0, "iconResId":I
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->currentIconResId:I

    if-eq v1, v0, :cond_0

    .line 103
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->iconView:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->iconView:Lcom/google/apps/dots/android/newsstand/widget/NSImageView;

    .line 107
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz p2, :cond_1

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->unsubscribe:I

    :goto_0
    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/NSImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    return-void

    .line 107
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->subscribe:I

    goto :goto_0
.end method

.method public setSubscribed(Z)V
    .locals 1
    .param p1, "subscribed"    # Z

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->getAppearance()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->setState(IZ)V

    .line 91
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 114
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->bindIsSubscribedKey:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 115
    if-nez p1, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->setSubscribed(Z)V

    .line 117
    :cond_0
    return-void

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/SubscriptionButton;->bindIsSubscribedKey:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v0

    goto :goto_0
.end method
