.class public abstract Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;
.super Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.source "CollectionCardList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$CollectionCardListRefreshTask;
    }
.end annotation


# instance fields
.field protected final appContext:Landroid/content/Context;

.field private final readingListFilter:Lcom/google/android/libraries/bind/data/Filter;

.field private final subscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

.field private final subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->DEFAULT_PRIMARY_KEY:I

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;-><init>(Landroid/content/Context;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "primaryKey"    # I

    .prologue
    .line 44
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;-><init>(I)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->appContext:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->CPU:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->readingListFilter:Lcom/google/android/libraries/bind/data/Filter;

    .line 70
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 71
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 77
    return-void
.end method


# virtual methods
.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$CollectionCardListRefreshTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$CollectionCardListRefreshTask;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$1;)V

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->onRegisterForInvalidation()V

    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 112
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->onUnregisterForInvalidation()V

    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 118
    return-void
.end method

.method protected processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .locals 4
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "rootNode"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v1

    .line 134
    .local v1, "readStateCollection":Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    check-cast p1, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$CollectionCardListRefreshTask;

    .end local p1    # "refreshTask":Lcom/google/android/libraries/bind/data/RefreshTask;
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList$CollectionCardListRefreshTask;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 136
    .local v0, "librarySnapshot":Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;
    invoke-virtual {p0, p2, v1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    move-result-object v2

    .line 137
    .local v2, "visitor":Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;

    invoke-direct {v3, p2, p3}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 138
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->getResults()Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method public rawCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/CompactCardFilter;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/card/CompactCardFilter;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .locals 1
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->emptyCollection()Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v0

    return-object v0
.end method

.method public readingList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->readingListFilter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/CollectionCardList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected abstract visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.end method
