.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;
.super Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 275
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/JankBustinOnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 276
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v1, p2

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->currentlyCenteredPosition:I
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$502(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;I)I

    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isHeaderFloating()Z

    move-result v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isHeaderFloating()Z

    move-result v1

    # setter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$702(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Z)Z

    .line 282
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->modeToggleMenuItem:Landroid/view/MenuItem;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$800(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 284
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_lite:I

    .line 283
    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 290
    :cond_0
    return-void

    .line 284
    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_image_lite:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$2;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 286
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->isHeaderFloating:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$700(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_print:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->magazine_mode_toggle_image_print:I

    goto :goto_0
.end method
