.class public Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;
.super Ljava/lang/Object;
.source "AsyncUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static mainThreadExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;

.field private static mainThreadHandler:Landroid/os/Handler;

.field private static pauseTimersWebView:Landroid/webkit/WebView;

.field private static resumeWebViews:Ljava/lang/Runnable;

.field private static webViewsArePaused:Z

.field private static webViewsResumeTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    .line 37
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->makeExecutor(Landroid/os/Handler;)Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;

    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->resumeWebViews:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static await(Ljava/util/concurrent/Future;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 174
    return-void
.end method

.method public static checkMainThread()V
    .locals 1

    .prologue
    .line 125
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->isMainThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 126
    return-void
.end method

.method public static checkNotMainThread()V
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 130
    return-void

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static doAfter(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<TV;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 279
    .local p0, "trigger":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    .local p1, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<TV;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$4;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$4;-><init>(Lcom/google/apps/dots/android/newsstand/async/Task;)V

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static doSequentially(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/android/newsstand/async/Task",
            "<+TV;>;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .prologue
    .line 290
    .local p0, "trigger":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;"
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/async/Task<+TV;>;>;"
    move-object v0, p0

    .line 291
    .local v0, "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/async/Task;

    .line 292
    .local v1, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<+TV;>;"
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->doAfter(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 293
    goto :goto_0

    .line 294
    .end local v1    # "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<+TV;>;"
    :cond_0
    return-object v0
.end method

.method private static getPauseTimersWebView()Landroid/webkit/WebView;
    .locals 3

    .prologue
    .line 55
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseTimersWebView:Landroid/webkit/WebView;

    if-nez v1, :cond_0

    .line 57
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 58
    .local v0, "policy":Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 59
    new-instance v1, Landroid/webkit/WebView;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseTimersWebView:Landroid/webkit/WebView;

    .line 60
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 62
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseTimersWebView:Landroid/webkit/WebView;

    return-object v1
.end method

.method public static init()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public static isMainThread()Z
    .locals 2

    .prologue
    .line 121
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mainThreadExecutor()Lcom/google/common/util/concurrent/ListeningExecutorService;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadExecutor:Lcom/google/common/util/concurrent/ListeningExecutorService;

    return-object v0
.end method

.method public static mainThreadHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static makeExecutor(Landroid/os/Handler;)Lcom/google/common/util/concurrent/ListeningExecutorService;
    .locals 1
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 137
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$2;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$2;-><init>(Landroid/os/Handler;)V

    invoke-static {v0}, Lcom/google/common/util/concurrent/MoreExecutors;->listeningDecorator(Ljava/util/concurrent/ExecutorService;)Lcom/google/common/util/concurrent/ListeningExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public static nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TV;>;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static nullingGet(Ljava/util/concurrent/Future;Z)Ljava/lang/Object;
    .locals 3
    .param p1, "warnOnException"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TV;>;Z)TV;"
        }
    .end annotation

    .prologue
    .local p0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TV;>;"
    const/4 v1, 0x0

    .line 189
    invoke-interface {p0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 190
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkNotMainThread()V

    .line 193
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 200
    :cond_1
    :goto_0
    return-object v1

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    if-eqz p1, :cond_1

    .line 196
    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v0

    .line 200
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    goto :goto_0
.end method

.method public static pauseBackgroundProcessingTemporarily()V
    .locals 2

    .prologue
    .line 79
    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseBackgroundProcessingTemporarily(J)V

    .line 80
    return-void
.end method

.method public static pauseBackgroundProcessingTemporarily(J)V
    .locals 2
    .param p0, "milliseconds"    # J

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseWebviewsTemporarily(J)V

    .line 84
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 85
    return-void
.end method

.method private static pauseWebViews()V
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->getPauseTimersWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->pauseTimers()V

    .line 100
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsArePaused:Z

    .line 101
    return-void
.end method

.method private static pauseWebviewsTemporarily(J)V
    .locals 4
    .param p0, "milliseconds"    # J

    .prologue
    .line 88
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->checkMainThread()V

    .line 89
    sget-boolean v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsArePaused:Z

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->resumeWebViews:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 94
    :goto_0
    sget-wide v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsResumeTime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsResumeTime:J

    .line 95
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->resumeWebViews:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsResumeTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 96
    return-void

    .line 92
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->pauseWebViews()V

    goto :goto_0
.end method

.method static resumeWebViews()V
    .locals 1

    .prologue
    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->getPauseTimersWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->resumeTimers()V

    .line 105
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->webViewsArePaused:Z

    .line 106
    return-void
.end method

.method public static tryRunElsePost(Landroid/os/Handler;Ljava/lang/Runnable;)Z
    .locals 2
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 73
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    goto :goto_0
.end method

.method public static varargs warnOnError(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/logging/Logd;[Ljava/lang/Class;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1, "logd"    # Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;",
            "Lcom/google/apps/dots/android/newsstand/logging/Logd;",
            "[",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<TV;>;"
    .local p2, "ignoreTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<+Ljava/lang/Throwable;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$3;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;[Ljava/lang/Class;Lcom/google/apps/dots/android/newsstand/logging/Logd;)V

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static wasFailure(Ljava/util/concurrent/Future;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 262
    invoke-interface {p0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v3

    if-nez v3, :cond_0

    .line 271
    :goto_0
    return v1

    .line 266
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    move v1, v2

    .line 269
    goto :goto_0

    .line 270
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/util/concurrent/CancellationException;
    move v1, v2

    .line 271
    goto :goto_0
.end method

.method public static withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;TV;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 299
    .local p0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TV;>;"
    .local p1, "fallback":Ljava/lang/Object;, "TV;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$5;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil$5;-><init>(Ljava/lang/Object;)V

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
