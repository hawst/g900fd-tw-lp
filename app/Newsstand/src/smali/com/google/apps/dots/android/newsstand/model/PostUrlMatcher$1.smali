.class final Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;
.super Ljava/lang/Object;
.source "PostUrlMatcher.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher;->findPostIdInSectionWithUrl(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/android/newsstand/store/MutationResponse;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;->apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/String;
    .locals 3
    .param p1, "mutationResponse"    # Lcom/google/apps/dots/android/newsstand/store/MutationResponse;

    .prologue
    .line 37
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    .line 38
    .local v0, "result":[Ljava/lang/String;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/store/MutationResponse;->simulatedRoot:Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;-><init>(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V

    new-instance v2, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;-><init>(Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/model/nodes/ProtoTraverser;->traverse(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;)V

    .line 49
    const/4 v1, 0x0

    aget-object v1, v0, v1

    return-object v1
.end method
