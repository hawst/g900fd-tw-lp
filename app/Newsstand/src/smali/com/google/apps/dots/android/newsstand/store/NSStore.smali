.class public Lcom/google/apps/dots/android/newsstand/store/NSStore;
.super Ljava/lang/Object;
.source "NSStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/store/NSStore$NotAvailableException;,
        Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;
    }
.end annotation


# instance fields
.field private final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private final cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

.field private final canonicalBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

.field private final diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

.field private final downloadQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

.field private final eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

.field private lastTimeoutChangedTimestamp:J

.field private lastTimeoutDurationMs:J

.field private final manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

.field private final nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

.field private pendingDownloads:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

.field private final storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

.field private final timeoutLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;Lcom/google/apps/dots/android/newsstand/events/EventNotifier;)V
    .locals 2
    .param p1, "diskCache"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
    .param p2, "storeCache"    # Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .param p3, "manifestBlobResolver"    # Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;
    .param p4, "nsClient"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;
    .param p5, "serverUris"    # Lcom/google/apps/dots/android/newsstand/server/ServerUris;
    .param p6, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;
    .param p7, "eventNotifier"    # Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 87
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->timeoutLock:Ljava/lang/Object;

    .line 89
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    .line 99
    new-instance v0, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->downloadQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    .line 105
    new-instance v0, Lcom/google/common/collect/MapMaker;

    invoke-direct {v0}, Lcom/google/common/collect/MapMaker;-><init>()V

    .line 106
    invoke-virtual {v0}, Lcom/google/common/collect/MapMaker;->makeMap()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->pendingDownloads:Ljava/util/concurrent/ConcurrentMap;

    .line 115
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    .line 116
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    .line 117
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

    .line 118
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .line 119
    iput-object p5, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    .line 120
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;

    invoke-direct {v0, p5, p6}, Lcom/google/apps/dots/android/newsstand/store/CanonicalBlobResolver;-><init>(Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->canonicalBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

    .line 121
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    invoke-direct {v0, p6}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;-><init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    .line 122
    iput-object p7, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getManifestUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getCanonicalUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "x6"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .prologue
    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->canFallBackToOriginalTransform(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/http/NSClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->nsClient:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->invalidateStoreCache(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->pendingDownloads:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/store/NSStore;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFile(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobMetadata(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/store/NSStore;)Lcom/google/apps/dots/android/newsstand/store/CachePolicy;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/store/NSStore;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->recoverCacheFallbackTimeoutMs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "x6"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .param p7, "x7"    # J

    .prologue
    .line 49
    invoke-direct/range {p0 .. p8}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownloadWithTimeout(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/store/NSStore;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->decayCacheFallbackTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/store/NSStore;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "x3"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "x4"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "x5"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "x6"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private canFallBackToOriginalTransform(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/server/Transform;)Z
    .locals 9
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "originalTransform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 405
    if-eqz p3, :cond_0

    iget-object v6, p1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-virtual {p3, v6}, Lcom/google/apps/dots/android/newsstand/server/Transform;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 426
    :cond_0
    :goto_0
    return v4

    .line 408
    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->clone()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v6

    invoke-virtual {v6, p3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->transform(Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v3

    .line 409
    .local v3, "untransformedRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    iget-object v6, p2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-direct {p0, v6, v3}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobFile(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    move-result-object v0

    .line 410
    .local v0, "untransformedBlobFile":Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    const/4 v1, 0x0

    .line 412
    .local v1, "untransformedBlobMetadata":Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :try_start_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getBlobMetadata(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 417
    :goto_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    .line 418
    invoke-virtual {v6, v1, v3}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->mayUseCachedVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)I

    move-result v2

    .line 419
    .local v2, "untransformedCacheRule":I
    if-eqz v2, :cond_2

    if-ne v2, v5, :cond_3

    .line 421
    :cond_2
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "Untransformed attachment available for %s. Untransformed version: %s."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v4

    aput-object v3, v8, v5

    invoke-virtual {v6, v7, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v5

    .line 423
    goto :goto_0

    .line 425
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "No untransformed attachment available for %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v4

    invoke-virtual {v6, v7, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 413
    .end local v2    # "untransformedCacheRule":I
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method private decayCacheFallbackTimeout()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x32

    .line 167
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->timeoutLock:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutChangedTimestamp:J

    .line 170
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    long-to-float v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    float-to-long v2, v0

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    .line 171
    const-wide/16 v2, 0x32

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cache fallback timeout decayed to %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    cmp-long v0, v2, v8

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Cache fallback timeout at min (%d ms)."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/16 v6, 0x32

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->di(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    :cond_0
    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    monitor-exit v1

    return-wide v2

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 18
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "resourceLink"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p5, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    if-nez p3, :cond_0

    .line 478
    new-instance v6, Ljava/lang/Exception;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0xb

    move/from16 v0, v16

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "No URI for "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFailedFuture(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 563
    :goto_0
    return-object v6

    .line 480
    :cond_0
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->getKey(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v12

    .line 481
    .local v12, "key":Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->pendingDownloads:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v12}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 482
    .local v13, "pendingDownload":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    if-eqz v13, :cond_1

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->postData:[B

    if-eqz v6, :cond_5

    .line 486
    :cond_1
    new-instance v4, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uri:Landroid/net/Uri;

    .line 487
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->postData:[B

    if-eqz p5, :cond_2

    move-object/from16 v0, p5

    iget-object v7, v0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->eTag:Ljava/lang/String;

    :goto_1
    if-eqz p5, :cond_3

    move-object/from16 v0, p5

    iget-object v8, v0, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    :goto_2
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->locale:Ljava/util/Locale;

    invoke-direct/range {v4 .. v10}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;-><init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;Ljava/util/Locale;)V

    .line 494
    .local v4, "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "%s: submitting download %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p2, v11, v16

    const/16 v16, 0x1

    aput-object v4, v11, v16

    invoke-virtual {v6, v7, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v10

    .line 498
    .local v10, "downloadToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    .line 499
    .local v14, "nanoTime":J
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    neg-long v8, v14

    .line 504
    .local v8, "priority":J
    :goto_3
    new-instance v5, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;

    sget-object v7, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    move-object/from16 v6, p0

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/apps/dots/android/newsstand/store/NSStore$7;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Ljava/util/concurrent/Executor;JLcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;)V

    .line 511
    .local v5, "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->downloadQueue:Lcom/google/apps/dots/android/newsstand/async/TaskQueue;

    .line 512
    invoke-virtual {v6, v5}, Lcom/google/apps/dots/android/newsstand/async/TaskQueue;->addTask(Lcom/google/apps/dots/android/newsstand/async/Task;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p1

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/NSStore$8;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 511
    invoke-static {v6, v7}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v13

    .line 548
    new-instance v6, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v6, v0, v1, v2, v12}, Lcom/google/apps/dots/android/newsstand/store/NSStore$9;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    invoke-static {v13, v6}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 556
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->pendingDownloads:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v12, v13}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    .end local v4    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .end local v5    # "task":Lcom/google/apps/dots/android/newsstand/async/Task;, "Lcom/google/apps/dots/android/newsstand/async/Task<Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;>;"
    .end local v8    # "priority":J
    .end local v10    # "downloadToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v14    # "nanoTime":J
    :goto_4
    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->nonCancellationPropagating(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    goto/16 :goto_0

    .line 487
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 499
    .restart local v4    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .restart local v10    # "downloadToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .restart local v14    # "nanoTime":J
    :cond_4
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    .line 503
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ordinal()I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v16, 0x5

    mul-long v6, v6, v16

    const-wide/16 v16, 0x3c

    mul-long v6, v6, v16

    const-wide/16 v16, 0x3e8

    mul-long v6, v6, v16

    const-wide/16 v16, 0x3e8

    mul-long v6, v6, v16

    const-wide/16 v16, 0x3e8

    mul-long v6, v6, v16

    add-long v8, v14, v6

    goto :goto_3

    .line 558
    .end local v4    # "clientRequest":Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;
    .end local v10    # "downloadToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v14    # "nanoTime":J
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v7, "%s: found pending download for %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p2, v11, v16

    const/16 v16, 0x1

    aput-object p3, v11, v16

    invoke-virtual {v6, v7, v11}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method private finishUri(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "baseLink"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p3, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 448
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->serverUris:Lcom/google/apps/dots/android/newsstand/server/ServerUris;

    invoke-virtual {p2, p1, v0, p3}, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->finishUri(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/server/ServerUris;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAccount(Ljava/util/Map;)Landroid/accounts/Account;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)",
            "Landroid/accounts/Account;"
        }
    .end annotation

    .prologue
    .line 592
    .local p0, "eventExtras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    const-string v0, "account"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method private getBlobFile(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 452
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->diskCache:Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    invoke-virtual {p2, p1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->getKey(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCacheBlobFile;-><init>(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    return-object v0
.end method

.method private static getBlobMetadata(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .locals 1
    .param p0, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    invoke-interface {p0}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->touch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->getMetadata()Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCanonicalUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p4, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->canonicalBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;->resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    invoke-direct {p0, p1, v0, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->finishUri(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    return-object v0
.end method

.method private getManifestUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p4, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->manifestBlobResolver:Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/store/NSStore$BlobResolver;->resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    invoke-direct {p0, p1, v0, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->finishUri(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    return-object v0
.end method

.method public static getVersion(Ljava/util/Map;)Lcom/google/apps/dots/android/newsstand/store/Version;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)",
            "Lcom/google/apps/dots/android/newsstand/store/Version;"
        }
    .end annotation

    .prologue
    .line 596
    .local p0, "eventExtras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    const-string v0, "version"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    return-object v0
.end method

.method private invalidateStoreCache(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;->clear(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 582
    return-void
.end method

.method public static makeNotificationExtras(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/Version;)Ljava/util/Map;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "version"    # Lcom/google/apps/dots/android/newsstand/store/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/apps/dots/android/newsstand/store/Version;",
            ")",
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 600
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 601
    .local v0, "builder":Lcom/google/common/collect/ImmutableMap$Builder;, "Lcom/google/common/collect/ImmutableMap$Builder<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    .line 602
    if-eqz p1, :cond_0

    .line 603
    const-string v1, "version"

    invoke-virtual {v0, v1, p1}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    .line 605
    :cond_0
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    return-object v1
.end method

.method private recoverCacheFallbackTimeoutMs()J
    .locals 12

    .prologue
    const-wide/16 v10, 0xbb8

    .line 140
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->timeoutLock:Ljava/lang/Object;

    monitor-enter v3

    .line 141
    :try_start_0
    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    cmp-long v2, v4, v10

    if-gez v2, :cond_0

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 143
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutChangedTimestamp:J

    sub-long v6, v0, v6

    long-to-float v2, v6

    const v6, 0x3c211bfd

    mul-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    .line 145
    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    const-wide/16 v6, 0xbb8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    .line 146
    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutChangedTimestamp:J

    .line 147
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Cache fallback timeout recovered to %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    cmp-long v2, v4, v10

    if-nez v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Cache fallback timeout at max (%d ms)."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-wide/16 v8, 0xbb8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->di(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    .end local v0    # "currentTime":J
    :cond_0
    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->lastTimeoutDurationMs:J

    monitor-exit v3

    return-wide v4

    .line 154
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private triggerAttachmentDownloadWithFallbackToClientTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "manifestUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "canonicalUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 328
    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v6, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    if-ne v4, v6, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 330
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 331
    .local v0, "combinedResponseFuture":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 335
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 339
    .local v2, "optimalResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->recoverCacheFallbackTimeoutMs()J

    move-result-wide v6

    sget-object v4, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {v6, v7, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeTimerFuture(JLjava/util/concurrent/Executor;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 340
    .local v3, "timerFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Object;>;"
    invoke-virtual {p1, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 342
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore$4;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 343
    invoke-static {v3, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 357
    .local v1, "delayedOriginalTransformFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/server/Transform;>;"
    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 361
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$5;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/common/util/concurrent/SettableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 379
    new-instance v4, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;

    invoke-direct {v4, p0, p2, p1, v0}, Lcom/google/apps/dots/android/newsstand/store/NSStore$6;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {v1, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 397
    return-object v0

    .end local v0    # "combinedResponseFuture":Lcom/google/common/util/concurrent/SettableFuture;, "Lcom/google/common/util/concurrent/SettableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    .end local v1    # "delayedOriginalTransformFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/server/Transform;>;"
    .end local v2    # "optimalResponseFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    .end local v3    # "timerFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Object;>;"
    :cond_0
    move v4, v5

    .line 328
    goto :goto_0
.end method

.method private triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "manifestUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "canonicalUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    if-eqz p3, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    .line 282
    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/store/NSStore$3;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V

    .line 280
    invoke-static {v8, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->withFallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureFallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 295
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->download(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private triggerDownloadWithTimeout(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "manifestUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p4, "canonicalUri"    # Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .param p5, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p6, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .param p7, "timeoutMs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/store/ResourceLink;",
            "Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;",
            "Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    .line 311
    invoke-direct/range {p0 .. p6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->triggerDownload(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/store/ResourceLink;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 312
    .local v0, "networkResponse":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    const/4 v1, 0x0

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {v0, v1, p7, p8, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeExpiringFuture(Lcom/google/common/util/concurrent/ListenableFuture;ZJLjava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public cachePolicy()Lcom/google/apps/dots/android/newsstand/store/CachePolicy;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->cachePolicy:Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    return-object v0
.end method

.method public deleteStoreFileForCorruptResponseIfNeeded(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "response"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;
    .param p3, "optException"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 572
    if-eqz p3, :cond_0

    .line 574
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Cached file for %s was corrupt. Deleting store region. Exception: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->diskCache()Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;

    move-result-object v0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->blobFile:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;->key()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;->deleteRegionForKey(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)V

    .line 578
    :cond_0
    return-void
.end method

.method public getBlobFileVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Lcom/google/apps/dots/android/newsstand/store/Version;
    .locals 4
    .param p1, "blobMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .prologue
    .line 462
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 463
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->lastModified:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(J)V

    .line 466
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/Version;

    iget-wide v2, p1, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->writeTime:J

    invoke-direct {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/Version;-><init>(J)V

    goto :goto_0
.end method

.method notifyContentProvider(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/store/StoreResponse;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p3, "response"    # Lcom/google/apps/dots/android/newsstand/store/StoreResponse;

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->eventNotifier:Lcom/google/apps/dots/android/newsstand/events/EventNotifier;

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    .line 587
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p3, Lcom/google/apps/dots/android/newsstand/store/StoreResponse;->version:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 588
    invoke-static {p1, v2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->makeNotificationExtras(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/Version;)Ljava/util/Map;

    move-result-object v2

    .line 586
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/events/EventNotifier;->notify(Landroid/net/Uri;Ljava/util/Map;)V

    .line 589
    return-void
.end method

.method public resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;
    .param p4, "transform"    # Lcom/google/apps/dots/android/newsstand/server/Transform;

    .prologue
    .line 432
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getManifestUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    .line 433
    .local v0, "manifestUri":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    if-eqz v0, :cond_0

    .end local v0    # "manifestUri":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    :goto_0
    return-object v0

    .restart local v0    # "manifestUri":Lcom/google/apps/dots/android/newsstand/store/ResourceLink;
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getCanonicalUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v0

    goto :goto_0
.end method

.method public storeCache()Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/NSStore;->storeCache:Lcom/google/apps/dots/android/newsstand/store/cache/StoreCache;

    return-object v0
.end method

.method public submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/android/newsstand/store/StoreRequest;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSSTORE_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 250
    invoke-virtual {v1, p1}, Lcom/google/apps/dots/android/newsstand/store/NSStore$1;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 253
    .local v0, "result":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    iget-boolean v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->permanent:Z

    if-eqz v1, :cond_0

    .line 254
    new-instance v1, Lcom/google/apps/dots/android/newsstand/store/NSStore$2;

    invoke-direct {v1, p0, p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore$2;-><init>(Lcom/google/apps/dots/android/newsstand/store/NSStore;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 266
    :cond_0
    return-object v0
.end method
