.class public Lcom/google/apps/dots/android/newsstand/play/MarketInfo;
.super Ljava/lang/Object;
.source "MarketInfo.java"


# direct methods
.method public static areMagazinesAvailable()Z
    .locals 4

    .prologue
    .line 19
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 20
    .local v0, "resources":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_magazines_market_override:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21
    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->magazines_market_override_magazines_available:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 23
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    const-string v2, "areMagazinesAvailable"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method
