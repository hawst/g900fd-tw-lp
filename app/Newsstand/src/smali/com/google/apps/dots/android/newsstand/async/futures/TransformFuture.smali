.class Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;
.super Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;
.source "TransformFuture.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture",
        "<TO;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<-TI;+TO;>;"
        }
    .end annotation
.end field

.field private inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;"
        }
    .end annotation
.end field

.field private volatile outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TO;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
            "<-TI;+TO;>;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    .local p1, "function":Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;, "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform<-TI;+TO;>;"
    .local p2, "inputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TI;>;"
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    .line 27
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 28
    return-void
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;
    .param p1, "x1"    # Lcom/google/common/util/concurrent/ListenableFuture;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object p1
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 32
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->propagateCancellation(Ljava/util/concurrent/Future;Z)V

    .line 35
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->propagateCancellation(Ljava/util/concurrent/Future;Z)V

    .line 36
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected propagateCancellation(Ljava/util/concurrent/Future;Z)V
    .locals 0
    .param p2, "mayInterruptIfRunning"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    .local p1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    if-eqz p1, :cond_0

    .line 46
    invoke-interface {p1, p2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 48
    :cond_0
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture<TI;TO;>;"
    const/4 v6, 0x0

    .line 52
    const/4 v2, 0x0

    .line 53
    .local v2, "sourceResult":Ljava/lang/Object;, "TI;"
    const/4 v3, 0x0

    .line 55
    .local v3, "sourceThrowable":Ljava/lang/Throwable;
    :try_start_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v5}, Lcom/google/common/util/concurrent/Uninterruptibles;->getUninterruptibly(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 61
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 64
    .end local v2    # "sourceResult":Ljava/lang/Object;, "TI;"
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 112
    :goto_1
    return-void

    .line 56
    .restart local v2    # "sourceResult":Ljava/lang/Object;, "TI;"
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_1
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 61
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 58
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_1
    move-exception v4

    .line 59
    .local v4, "t":Ljava/lang/Throwable;
    move-object v3, v4

    .line 61
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    throw v5

    .line 70
    .end local v2    # "sourceResult":Ljava/lang/Object;, "TI;"
    :cond_0
    if-nez v3, :cond_1

    .line 71
    :try_start_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    invoke-interface {v5, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;->apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 88
    .local v1, "outputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TO;>;"
    :goto_2
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    .line 91
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 92
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->wasInterrupted()Z

    move-result v5

    invoke-virtual {p0, v1, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->propagateCancellation(Ljava/util/concurrent/Future;Z)V

    .line 93
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 73
    .end local v1    # "outputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TO;>;"
    :cond_1
    :try_start_3
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    invoke-interface {v5, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;->fallback(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->outputFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .restart local v1    # "outputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TO;>;"
    goto :goto_2

    .line 75
    .end local v1    # "outputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TO;>;"
    :catch_2
    move-exception v0

    .line 78
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    const/4 v5, 0x0

    :try_start_4
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->cancel(Z)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 88
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    goto :goto_1

    .line 80
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    :catch_3
    move-exception v0

    .line 81
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_5
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 88
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    goto :goto_1

    .line 83
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_4
    move-exception v4

    .line 84
    .restart local v4    # "t":Ljava/lang/Throwable;
    :try_start_6
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 88
    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    goto :goto_1

    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v5

    iput-object v6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;->function:Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;

    throw v5

    .line 97
    .restart local v1    # "outputFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TO;>;"
    :cond_2
    new-instance v5, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;

    invoke-direct {v5, p0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-static {v1, v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addListener(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method
