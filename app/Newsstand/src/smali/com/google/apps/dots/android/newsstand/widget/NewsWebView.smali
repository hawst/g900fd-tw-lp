.class public Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
.super Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;
.source "NewsWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$JavascriptEvaluator;,
        Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;
    }
.end annotation


# static fields
.field private static final DOTS_BRIDGE:Ljava/lang/String; = "dots_bridge"

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SCRIPT_REFRESH_BG_IMAGES:Ljava/lang/String; = "(function() {  var i, l, els = document.querySelectorAll(\'.g-splash[style*=\"background-image\"]\');  for (i = 0, l = els.length; i < l; i++) {    els[i].style.backgroundImage =       els[i].style.backgroundImage.replace(/(\\?[\\d]+)?\\)$/, \'?\' + (+new Date()) + \')\');  }})();"

.field private static final SCRIPT_REFRESH_IMGS:Ljava/lang/String; = "(function() {  var i, l, els = document.querySelectorAll(\'img[src^=\"attachment/\"]\');  for (i = 0, l = els.length; i < l; i++) {    els[i].src = els[i].src.replace(/(\\?[\\d]+)?$/, \'?\' + (+new Date()));  }})();"


# instance fields
.field private articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

.field private audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

.field protected bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

.field private bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

.field private connectivityListener:Ljava/lang/Runnable;

.field protected destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private initialPageFraction:F

.field protected isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isVisibleToUser:Z

.field private loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

.field private meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field private notifyOfScrollEvents:Z

.field protected onLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

.field protected renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

.field private totalArticlePageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    .line 94
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->doConstructorTasks()V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    .line 99
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->doConstructorTasks()V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->NOT_LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    .line 104
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->doConstructorTasks()V

    .line 105
    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->sendJsonPostDataToJs(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->fixNavigatorOnlineInJs()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->startArticleLayout(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;)Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    return-object v0
.end method

.method private calculatePage(F)I
    .locals 2
    .param p1, "y"    # F

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 518
    float-to-int v0, p1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v1

    div-int/2addr v0, v1

    .line 520
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doConstructorTasks()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 110
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->init()V

    .line 111
    return-void
.end method

.method private ensureScriptReady()V
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, Lcom/google/common/util/concurrent/SettableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    .line 705
    invoke-virtual {v0}, Lcom/google/common/util/concurrent/SettableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 706
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Method depends on inline script being fully loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 708
    :cond_1
    return-void
.end method

.method private fixNavigatorOnlineInJs()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "dots.androidOffline = %s;"

    new-array v4, v0, [Ljava/lang/Object;

    .line 264
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 263
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 265
    return-void

    :cond_0
    move v0, v1

    .line 264
    goto :goto_0
.end method

.method private gotoRightArticlePageQuietly(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    .line 576
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPage()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v1

    mul-int/2addr v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->scrollTo(II)V

    .line 579
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    .line 580
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, Lcom/google/common/util/concurrent/SettableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    .line 146
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    .line 151
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setupBridge()V

    goto :goto_0
.end method

.method private onPageChanged(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isVisibleToUser:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->triggerPageViewInJSIfNeeded(I)V

    .line 588
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onPageChanged(I)V

    .line 592
    :cond_0
    return-void
.end method

.method private sendJsonPostDataToJs(Ljava/lang/String;)V
    .locals 5
    .param p1, "jsonPostData"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 256
    const-string v1, "dots.store.setupPost(%s);"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "setupPostClause":Ljava/lang/String;
    new-array v1, v4, [Ljava/lang/String;

    aput-object v0, v1, v3

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method private setupAudioStatusHelper()V
    .locals 3

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    if-nez v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadPost()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 378
    :cond_0
    return-void
.end method

.method private setupBridge()V
    .locals 5

    .prologue
    .line 155
    new-instance v1, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 156
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$IsTouchNavigationAllowedProvider;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;)V

    invoke-direct {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/android/libraries/bind/util/Provider;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    .line 157
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeEventHandler;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;)V

    .line 158
    .local v0, "bridgeEventHandler":Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;-><init>(Lcom/google/apps/dots/android/newsstand/bridge/BridgeEventHandler;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    .line 159
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    const-string v2, "dots_bridge"

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method private shouldNotifyDelegate()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startArticleLayout(Ljava/lang/String;)V
    .locals 6
    .param p1, "serializedJsonStore"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 268
    const-string v1, "dots.loadContent(%s, %s, %s);"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 269
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p1, v2, v3

    .line 268
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "loadContentClause":Ljava/lang/String;
    new-array v1, v5, [Ljava/lang/String;

    aput-object v0, v1, v4

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method private tearDownBridge()V
    .locals 1

    .prologue
    .line 163
    const-string v0, "dots_bridge"

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;->destroy()V

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    .line 166
    return-void
.end method


# virtual methods
.method public calculateCurrentPage()I
    .locals 1

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculatePage(F)I

    move-result v0

    return v0
.end method

.method public calculateCurrentPageFraction()F
    .locals 6

    .prologue
    .line 550
    const/4 v1, 0x0

    .line 551
    .local v1, "pageFraction":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPage()I

    move-result v3

    int-to-float v0, v3

    .line 552
    .local v0, "currentPage":F
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateVerticalScrollRange()I

    move-result v2

    .line 553
    .local v2, "scrollRange":I
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 554
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->totalArticlePageCount:I

    if-lez v3, :cond_1

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->totalArticlePageCount:I

    int-to-float v3, v3

    div-float v1, v0, v3

    .line 559
    :goto_0
    return v1

    .line 554
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 557
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    int-to-float v4, v2

    div-float v1, v3, v4

    goto :goto_0
.end method

.method protected calculatePageFromPageFraction(F)I
    .locals 4
    .param p1, "pageFraction"    # F

    .prologue
    .line 532
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->totalArticlePageCount:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 533
    .local v0, "pageIndex":I
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->totalArticlePageCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public calculateVerticalScrollRange()I
    .locals 1

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public evaluateStatement(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 0
    .param p1, "script"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 695
    .local p2, "receiver":Landroid/webkit/ValueCallback;, "Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 696
    # invokes: Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$JavascriptEvaluator;->evaluate(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$JavascriptEvaluator;->access$500(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 697
    return-void
.end method

.method public varargs executeStatements([Ljava/lang/String;)V
    .locals 6
    .param p1, "statements"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 656
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 685
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->ensureScriptReady()V

    .line 663
    array-length v2, p1

    if-eqz v2, :cond_0

    .line 664
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 665
    .local v0, "sb":Ljava/lang/StringBuilder;
    array-length v4, p1

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v1, p1, v2

    .line 666
    .local v1, "statement":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 667
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Statement is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 669
    :cond_2
    const-string v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 670
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Missing semicolon: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 672
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 674
    .end local v1    # "statement":Ljava/lang/String;
    :cond_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v2, v4, :cond_6

    .line 677
    const-string v2, "javascript"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 681
    :cond_6
    const-string v2, "javascript:"

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getOnLoadFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method public getOnScriptLoadFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method public isLoadComplete()Z
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadArticle()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 223
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridge:Lcom/google/apps/dots/android/newsstand/bridge/WebViewBridge;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 226
    .local v0, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    new-array v2, v6, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 229
    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadSerializedPostData()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    aput-object v3, v2, v5

    .line 227
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    .line 226
    invoke-static {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addSynchronousCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 240
    .local v1, "setupPostInJsFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    aput-object v3, v2, v4

    aput-object v1, v2, v5

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 244
    invoke-interface {v3}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadJsonStore()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    aput-object v3, v2, v6

    .line 241
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    .line 240
    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 253
    return-void
.end method

.method public loadBaseHtml(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 207
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->loadWebviewBaseHtml()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$2;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Landroid/accounts/Account;)V

    .line 206
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 215
    return-void
.end method

.method protected onArticleUnhandledTouchEvent()V
    .locals 1

    .prologue
    .line 603
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onArticleUnhandledTouchEvent()V

    .line 606
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 430
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onAttachedToWindow()V

    .line 433
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->connectivityListener:Ljava/lang/Runnable;

    .line 450
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 454
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onDetachedFromWindow()V

    .line 455
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 456
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 457
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->connectivityListener:Ljava/lang/Runnable;

    .line 459
    :cond_0
    return-void
.end method

.method public onLayoutChange(IZII)V
    .locals 6
    .param p1, "pageCount"    # I
    .param p2, "isDone"    # Z
    .param p3, "pageWidth"    # I
    .param p4, "pageHeight"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 289
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    if-gez p1, :cond_2

    .line 293
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "onLayoutChange - invalid pageNumber: %d"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 296
    :cond_2
    if-gez p3, :cond_3

    .line 297
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "onLayoutChange - invalid pageWidth: %d"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 300
    :cond_3
    if-gez p4, :cond_4

    .line 301
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "onLayoutChange - invalid pageHeight: %d"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 304
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 310
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v1, p1, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onLayoutChange(III)V

    .line 313
    :cond_5
    if-eqz p2, :cond_0

    .line 315
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->totalArticlePageCount:I

    .line 318
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->initialPageFraction:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_7

    .line 325
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-le v1, v2, :cond_6

    .line 326
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->initialPageFraction:F

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculatePageFromPageFraction(F)I

    move-result v0

    .line 327
    .local v0, "pageToGoTo":I
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->gotoRightArticlePageQuietly(I)V

    .line 330
    .end local v0    # "pageToGoTo":I
    :cond_6
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->initialPageFraction:F

    .line 333
    :cond_7
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLayoutComplete()V

    goto :goto_0
.end method

.method protected onLayoutComplete()V
    .locals 2

    .prologue
    .line 338
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOADED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 339
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 342
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPage()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onPageChanged(I)V

    .line 343
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setupAudioStatusHelper()V

    .line 344
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onLayoutComplete()V

    .line 347
    :cond_0
    return-void
.end method

.method protected onLayoutFailed(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 351
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    .line 352
    if-eqz p1, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 354
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Error when displaying post %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getPostId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onLayoutFailed(Ljava/lang/Throwable;)V

    .line 359
    :cond_0
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 1
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onArticleOverscrolled(IIZZ)V

    .line 510
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onOverScrolled(IIZZ)V

    .line 511
    return-void
.end method

.method public onReallyDestroy()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->tearDownBridge()V

    .line 134
    return-void
.end method

.method public onScriptLoad()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 280
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 7
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    const/4 v5, 0x0

    .line 475
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->notifyOfScrollEvents:Z

    if-eqz v4, :cond_3

    .line 476
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onScrollChanged(IIII)V

    .line 481
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

    if-eqz v4, :cond_0

    .line 482
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;->onScrollChanged()V

    .line 485
    :cond_0
    int-to-float v4, p4

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculatePage(F)I

    move-result v0

    .line 486
    .local v0, "lastPage":I
    int-to-float v4, p2

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculatePage(F)I

    move-result v1

    .line 487
    .local v1, "page":I
    if-eq v1, v0, :cond_1

    .line 488
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onPageChanged(I)V

    .line 491
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 493
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 494
    .local v3, "scrollOffset":I
    sub-int v2, p2, p4

    .line 495
    .local v2, "scrollDelta":I
    if-nez v3, :cond_2

    .line 496
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 498
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->computeVerticalScrollRange()I

    move-result v5

    const/4 v6, 0x1

    invoke-interface {v4, v3, v5, v2, v6}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onArticleScrolled(IIIZ)V

    .line 502
    .end local v0    # "lastPage":I
    .end local v1    # "page":I
    .end local v2    # "scrollDelta":I
    .end local v3    # "scrollOffset":I
    :cond_3
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "ow"    # I
    .param p4, "oh"    # I

    .prologue
    .line 467
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onSizeChanged(IIII)V

    .line 468
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;->onSizeChanged()V

    .line 471
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 388
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v3, :cond_1

    .line 389
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    .line 390
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->toggleActionBar()V

    .line 413
    :cond_0
    :goto_0
    return v3

    .line 395
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    if-eqz v5, :cond_3

    .line 396
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->handleArticleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 397
    .local v0, "allowWebViewToHandleTouchEvent":Z
    if-eqz v0, :cond_2

    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    move v3, v4

    goto :goto_0

    .line 402
    .end local v0    # "allowWebViewToHandleTouchEvent":Z
    :cond_3
    const/4 v2, 0x0

    .line 404
    .local v2, "returnVal":Z
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 410
    :goto_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 411
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v3, p1}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onTouchEvent(Landroid/view/MotionEvent;)V

    :cond_4
    move v3, v2

    .line 413
    goto :goto_0

    .line 405
    :catch_0
    move-exception v1

    .line 407
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Suppressing exception from WebView onTouchEvent"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v5, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 419
    const/4 v0, 0x0

    return v0
.end method

.method public scrollToTop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 423
    const-string v1, "scrollY"

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v3

    aput v3, v2, v4

    const/4 v3, 0x1

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 424
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 425
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    .line 426
    return-void
.end method

.method public setInitialPageFraction(F)V
    .locals 0
    .param p1, "fraction"    # F

    .prologue
    .line 568
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->initialPageFraction:F

    .line 569
    return-void
.end method

.method public setMeterDialog(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V
    .locals 0
    .param p1, "meterDialog"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 184
    return-void
.end method

.method public setRenderDelegate(Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;)V
    .locals 1
    .param p1, "renderDelegate"    # Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->setRenderDelegate(Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;)V

    .line 180
    :cond_0
    return-void
.end method

.method public setRenderSource(Lcom/google/apps/dots/android/newsstand/reading/RenderSource;)V
    .locals 1
    .param p1, "renderSource"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->bridgeResponder:Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bridge/NewsBridgeResponder;->setRenderSource(Lcom/google/apps/dots/android/newsstand/reading/RenderSource;)V

    .line 173
    :cond_0
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 617
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isVisibleToUser:Z

    .line 618
    if-eqz p1, :cond_0

    .line 619
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setFocusable(Z)V

    .line 620
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->requestFocus()Z

    .line 622
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateCurrentPage()I

    move-result v0

    .line 623
    .local v0, "currentPage":I
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onPageChanged(I)V

    .line 627
    .end local v0    # "currentPage":I
    :goto_0
    return-void

    .line 625
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setFocusable(Z)V

    goto :goto_0
.end method

.method public setupNewsArticleTail(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
    .locals 1
    .param p1, "parentLayout"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 191
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->articleTailParentListener:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;

    .line 202
    return-void
.end method

.method protected shouldFailOnJsError()Z
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->loadState:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->canFail()Z

    move-result v0

    return v0
.end method

.method public startDestroy()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;->unregister(Landroid/content/Context;)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->audioStatusHelper:Lcom/google/apps/dots/android/newsstand/widget/AudioStatusHelper;

    .line 124
    :cond_1
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/WebViewBase;->startDestroy()V

    goto :goto_0
.end method

.method public toggleActionBar()V
    .locals 1

    .prologue
    .line 611
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->shouldNotifyDelegate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->renderDelegate:Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/reading/RenderDelegate;->onToggleActionBar()V

    .line 614
    :cond_0
    return-void
.end method

.method public triggerPageViewInJSIfNeeded(I)V
    .locals 3
    .param p1, "pageNumber"    # I

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->isVisibleToUser:Z

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$7;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;I)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 640
    :cond_0
    return-void
.end method
