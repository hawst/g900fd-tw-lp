.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;
.super Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TransitionDelegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1118
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$1;

    .prologue
    .line 1118
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleExitRemapSharedElements(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/List;Ljava/util/Map;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1125
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/transition/NSHeroReloadoDelegate;->handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V

    .line 1126
    invoke-virtual {p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->handleRemapSharedElements(Ljava/util/List;Ljava/util/Map;)V

    .line 1127
    return-void
.end method

.method protected bridge synthetic handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1118
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;->handleExitRemapSharedElements(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method public headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 1131
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$600(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 1118
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;->headerListLayout(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    return-object v0
.end method

.method public sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/View;
    .locals 1
    .param p1, "fragment"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .prologue
    .line 1136
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic sharedElementView(Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1118
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$TransitionDelegate;->sharedElementView(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
