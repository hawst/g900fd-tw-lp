.class public final enum Lcom/google/apps/dots/android/newsstand/util/OfferType;
.super Ljava/lang/Enum;
.source "OfferType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/util/OfferType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field public static final enum PURCHASE:Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field public static final enum PURCHASE_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field public static final enum RENTAL:Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field public static final enum RENTAL_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field public static final enum UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/OfferType;

.field private static final protoEnumValueToFinskyOfferType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/apps/dots/android/newsstand/util/OfferType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final protoValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/dots/android/newsstand/util/OfferType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 13
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    const-string v1, "PURCHASE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/dots/android/newsstand/util/OfferType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->PURCHASE:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 14
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    const-string v1, "RENTAL"

    invoke-direct {v0, v1, v7, v5}, Lcom/google/apps/dots/android/newsstand/util/OfferType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->RENTAL:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    const-string v1, "RENTAL_HIGH_DEF"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/apps/dots/android/newsstand/util/OfferType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->RENTAL_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 16
    new-instance v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    const-string v1, "PURCHASE_HIGH_DEF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lcom/google/apps/dots/android/newsstand/util/OfferType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->PURCHASE_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 10
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/util/OfferType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->PURCHASE:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->RENTAL:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->RENTAL_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->PURCHASE_HIGH_DEF:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 18
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoEnumValueToFinskyOfferType:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "protoValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoValue:I

    .line 23
    return-void
.end method

.method public static forProtoValue(I)Lcom/google/apps/dots/android/newsstand/util/OfferType;
    .locals 6
    .param p0, "protoValue"    # I

    .prologue
    .line 27
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoEnumValueToFinskyOfferType:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/OfferType;->values()[Lcom/google/apps/dots/android/newsstand/util/OfferType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 29
    .local v0, "offerType":Lcom/google/apps/dots/android/newsstand/util/OfferType;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoEnumValueToFinskyOfferType:Ljava/util/Map;

    iget v5, v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    .end local v0    # "offerType":Lcom/google/apps/dots/android/newsstand/util/OfferType;
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/OfferType;->protoEnumValueToFinskyOfferType:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 33
    .restart local v0    # "offerType":Lcom/google/apps/dots/android/newsstand/util/OfferType;
    if-nez v0, :cond_1

    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->UNKNOWN:Lcom/google/apps/dots/android/newsstand/util/OfferType;

    .line 36
    .end local v0    # "offerType":Lcom/google/apps/dots/android/newsstand/util/OfferType;
    :cond_1
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/OfferType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/util/OfferType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/OfferType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/util/OfferType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/util/OfferType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/util/OfferType;

    return-object v0
.end method
