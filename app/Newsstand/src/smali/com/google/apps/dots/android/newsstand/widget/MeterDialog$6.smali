.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Landroid/view/View;Landroid/webkit/WebView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "result"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 252
    if-nez p1, :cond_0

    .line 265
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 256
    .local v0, "appFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasMeteredPolicy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v2, 0x1

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->setup:Z
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$402(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Z)Z

    .line 258
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->hide(Z)V

    goto :goto_0

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getMeteredPolicy()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v2

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$602(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 262
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->fillInDialog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$700(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->watchReadStatesUriIfNecessary(Landroid/accounts/Account;)V
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$800(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Landroid/accounts/Account;)V

    .line 264
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateMeteredCount()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$200(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 249
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$6;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
