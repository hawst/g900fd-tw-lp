.class public abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.super Ljava/lang/Object;
.source "AnalyticsBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static deduper:Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;


# instance fields
.field protected final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private final createdTimeInMillis:J

.field private hasBeenTracked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    .line 52
    return-void
.end method

.method protected constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 2
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->createdTimeInMillis:J

    .line 59
    return-void
.end method

.method protected static addCuratedDimensionIfNecessary(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 2
    .param p0, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 226
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    if-eqz v0, :cond_1

    .line 227
    const-string v0, "CuratedTopic"

    const-string v1, "false"

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    instance-of v0, p1, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "CuratedTopic"

    const-string v1, "true"

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static appendMetric(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/Float;)V
    .locals 2
    .param p0, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Float;

    .prologue
    .line 170
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->metric(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    move-result-object v0

    .line 172
    .local v0, "metric":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    invoke-static {v1, v0}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalMetrics:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    .line 173
    return-void
.end method

.method public static appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->pair(Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    move-result-object v0

    .line 163
    .local v0, "pair":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    invoke-static {v1, v0}, Lcom/google/common/collect/ObjectArrays;->concat([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->additionalData:[Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    .line 164
    return-void
.end method

.method private fillCommonAnalyticsEventData()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->getScreen()Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "screen":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->getExperiments()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "experiments":Ljava/lang/String;
    new-instance v3, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    invoke-direct {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;-><init>()V

    .line 129
    invoke-virtual {v3, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setScreen(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->createdTimeInMillis:J

    .line 130
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCreated(J)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v4

    .line 131
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v4, v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setIsOffline(Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    .line 132
    .local v0, "event":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->analyticsEventId:Ljava/lang/String;

    .line 135
    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setExperiments(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 139
    :cond_0
    return-object v0

    .line 131
    .end local v0    # "event":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getDeduper()Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->deduper:Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->deduper:Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;

    .line 179
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->deduper:Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;

    return-object v0
.end method

.method public static metric(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Float;

    .prologue
    .line 44
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;-><init>()V

    .line 45
    .local v0, "metric":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;
    iput-object p0, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->name:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$Metric;->value:F

    .line 47
    return-object v0
.end method

.method public static pair(Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;-><init>()V

    .line 38
    .local v0, "pair":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;
    iput-object p0, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->name:Ljava/lang/String;

    .line 39
    iput-object p1, v0, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent$NameValuePair;->value:Ljava/lang/String;

    .line 40
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->isDedupable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 207
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation
.end method

.method protected abstract fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
.end method

.method protected getDedupeExpiryTime()J
    .locals 1

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getExperiments()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 146
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    if-nez v2, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-object v1

    .line 150
    :cond_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->configUtil()Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/gcm/ConfigUtil;->getCachedConfig(Landroid/accounts/Account;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    .line 151
    .local v0, "config":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getExperiments()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 152
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->getExperiments()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->getExperimentIds()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected abstract getPublisherTrackingId()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation
.end method

.method protected abstract getScreen()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation
.end method

.method public hasDedupeExpired()Z
    .locals 6

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->isDedupable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 194
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->createdTimeInMillis:J

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->getDedupeExpiryTime()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->isDedupable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 214
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDedupable()Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method protected sendEventToTracker(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V
    .locals 3
    .param p1, "analyticsEvent"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "sendToPublishers"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->tracker()Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    .line 120
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->getPublisherTrackingId()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/apps/dots/android/newsstand/analytics/MultiTracker;->trackEvent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V

    .line 121
    return-void
.end method

.method protected shouldIgnore()Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public track(Z)V
    .locals 2
    .param p1, "sendToPublishers"    # Z

    .prologue
    const/4 v1, 0x1

    .line 81
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->hasBeenTracked:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 84
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->hasBeenTracked:Z

    .line 85
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$1;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->ANALYTICS:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$1;-><init>(Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;Lcom/google/apps/dots/android/newsstand/async/Queue;Z)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 90
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$1;->execute(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 91
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method trackInternal(Z)V
    .locals 7
    .param p1, "sendToPublishers"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->shouldIgnore()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Not sending analytics event %s as it should be ignored."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->getDeduper()Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/apps/dots/android/newsstand/analytics/AnalyticsDeduper;->shouldIgnore(Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Not sending duplicate analytics event: %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->fillCommonAnalyticsEventData()Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v0

    .line 107
    .local v0, "analyticsEvent":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 108
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 110
    invoke-virtual {p0, v0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->sendEventToTracker(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)V
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 111
    .end local v0    # "analyticsEvent":Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Failed to resolve analytics event: %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
