.class public Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "OnboardAuthPage.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPage;
.implements Lcom/google/android/play/onboard/OnboardPageInfo;


# static fields
.field private static final AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final STATE_PREFIX:Ljava/lang/String;

.field private static final STATE_SHOWING_SPINNER:Ljava/lang/String;


# instance fields
.field private authFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

.field private delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

.field private showingSpinner:Z

.field private final uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 34
    const-class v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->STATE_PREFIX:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->STATE_PREFIX:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "showingSpinner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->STATE_SHOWING_SPINNER:Ljava/lang/String;

    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userless()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->inherit()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 67
    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->onAuthSuccess()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->onAuthFailure(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)Lcom/google/android/play/onboard/OnboardHostControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    return-object v0
.end method

.method private getAuthFuture()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method private getDelayFuture(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "reset"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 182
    const-wide/16 v0, 0x5dc

    sget-object v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeTimerFuture(JLjava/util/concurrent/Executor;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->delayFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method private hideSpinner()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 201
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->spinner:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 202
    .local v1, "spinner":Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->cancelFade(Landroid/view/View;)V

    .line 203
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 205
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->caption:I

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 206
    .local v0, "caption":Landroid/view/View;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->cancelFade(Landroid/view/View;)V

    .line 207
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 208
    return-void
.end method

.method private onAuthFailure(Z)V
    .locals 1
    .param p1, "cancelled"    # Z

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->goToPreviousPage()V

    .line 129
    return-void
.end method

.method private onAuthSuccess()V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->getDelayFuture(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$2;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addInlineCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 125
    return-void
.end method

.method private setAuthFuture(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "authFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eq v0, p1, :cond_0

    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 91
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;-><init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 107
    :cond_0
    return-void
.end method

.method private showSpinner()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x1f4

    .line 196
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->spinner:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 197
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->caption:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 198
    return-void
.end method

.method private updateSpinner()V
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->showingSpinner:Z

    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->showSpinner()V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hideSpinner()V

    goto :goto_0
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method

.method public allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;
    .locals 0

    .prologue
    .line 138
    return-object p0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onAttachedToWindow()V

    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 73
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    if-nez v0, :cond_0

    .line 75
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 74
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->instanceForActivity(Landroid/support/v4/app/FragmentActivity;)Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->existingAuthFlowFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->setAuthFuture(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 79
    return-void
.end method

.method public onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 243
    invoke-interface {p1}, Lcom/google/android/play/onboard/OnboardHostControl;->goToPreviousPage()V

    .line 244
    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->uiScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 84
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->onDetachedFromWindow()V

    .line 85
    return-void
.end method

.method public onEnterPage(Z)V
    .locals 6
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    const/4 v5, 0x1

    .line 154
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onEnterPage. movingTowardsEnd=%b"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    if-eqz p1, :cond_0

    .line 156
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 157
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->showingSpinner:Z

    .line 158
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->updateSpinner()V

    .line 159
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->authUiHelper:Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/AuthUiHelper;->newAuthFlowFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->setAuthFuture(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 161
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->getAuthFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->track(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 163
    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->getDelayFuture(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardHostControl;->goToPreviousPage()V

    goto :goto_0
.end method

.method public onExitPage(Z)V
    .locals 3
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 171
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onExitPage"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->AUTH_PAGE_SCOPE:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 173
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hideSpinner()V

    .line 174
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 143
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->STATE_SHOWING_SPINNER:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->showingSpinner:Z

    .line 144
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->updateSpinner()V

    .line 145
    return-void
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 149
    sget-object v0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->STATE_SHOWING_SPINNER:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->showingSpinner:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    return-void
.end method

.method public setOnboardHostControl(Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0
    .param p1, "control"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    .line 134
    return-void
.end method

.method public shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 218
    const/4 v0, 0x1

    return v0
.end method
