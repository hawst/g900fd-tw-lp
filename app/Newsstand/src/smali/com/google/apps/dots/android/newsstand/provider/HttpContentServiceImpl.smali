.class public Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;
.super Ljava/lang/Object;
.source "HttpContentServiceImpl.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/provider/HttpContentService;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;,
        Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final rangePattern:Ljava/util/regex/Pattern;

.field private static rng:Ljava/security/SecureRandom;


# instance fields
.field private activeServerSocket:Ljava/net/ServerSocket;

.field private activeTasks:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;",
            ">;"
        }
    .end annotation
.end field

.field private activeTokens:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private appContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 57
    const-string v0, "bytes=(\\d+)-(\\d*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rangePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;

    .line 62
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;

    .line 121
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->appContext:Landroid/content/Context;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->revokeToken(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Ljava/net/Socket;)V
    .locals 0
    .param p0, "x0"    # Ljava/net/Socket;

    .prologue
    .line 47
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->closeSocket(Ljava/net/Socket;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Ljava/net/ServerSocket;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rangePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private static closeSocket(Ljava/net/Socket;)V
    .locals 7
    .param p0, "socket"    # Ljava/net/Socket;

    .prologue
    const/4 v6, 0x0

    .line 413
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/net/Socket;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 414
    sget-object v1, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Closing socket %s->%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 415
    invoke-virtual {p0}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-virtual {p0}, Ljava/net/Socket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v5

    aput-object v5, v3, v4

    .line 414
    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 418
    :catch_0
    move-exception v0

    .line 419
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Problem closing socket."

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private revokeToken(Ljava/lang/String;)V
    .locals 3
    .param p1, "requestToken"    # Ljava/lang/String;

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 206
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;

    .line 207
    .local v0, "task":Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->revokeToken(Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .end local v0    # "task":Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;
    :cond_0
    return-void
.end method


# virtual methods
.method public bind()Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .locals 23

    .prologue
    .line 129
    const/4 v11, 0x0

    .line 132
    .local v11, "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    if-nez v3, :cond_0

    .line 133
    const-string v3, "127.0.0.1"

    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v13

    .line 135
    .local v13, "loopback":Ljava/net/InetAddress;
    new-instance v3, Ljava/net/ServerSocket;

    const/16 v19, 0x0

    const/16 v20, 0x32

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v0, v1, v13}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    .line 136
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    sget-object v3, Lcom/google/apps/dots/android/newsstand/async/Queues;->HTTP_CONTENT_SERVICE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    iget v3, v3, Lcom/google/apps/dots/android/newsstand/async/Queue;->poolSize:I

    if-ge v12, v3, :cond_0

    .line 137
    new-instance v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;

    sget-object v19, Lcom/google/apps/dots/android/newsstand/async/Queues;->HTTP_CONTENT_SERVICE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;-><init>(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->execute()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 136
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 141
    .end local v12    # "i":I
    .end local v13    # "loopback":Ljava/net/InetAddress;
    :cond_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v19, "%s:%d"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    move-object/from16 v22, v0

    .line 143
    invoke-virtual/range {v22 .. v22}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/google/common/net/InetAddresses;->toUriString(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    move-object/from16 v22, v0

    .line 144
    invoke-virtual/range {v22 .. v22}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    .line 141
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v3, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "authority":Ljava/lang/String;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v19, "Bound listen socket: %s"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v8, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 153
    .local v14, "policy":Landroid/os/StrictMode$ThreadPolicy;
    :try_start_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rng:Ljava/security/SecureRandom;

    if-nez v3, :cond_1

    .line 154
    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    sput-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rng:Ljava/security/SecureRandom;

    .line 156
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->rng:Ljava/security/SecureRandom;

    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextLong()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v16

    .line 158
    .local v16, "rawRequestToken":J
    :try_start_2
    invoke-static {v14}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 161
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    const/16 v19, 0x10

    const/16 v20, 0x30

    .line 160
    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v3, v0, v1}, Lcom/google/common/base/Strings;->padStart(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v3

    const/16 v19, 0x0

    const/16 v20, 0x10

    .line 162
    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 164
    .local v4, "requestToken":Ljava/lang/String;
    const-string v3, "%s%s/"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "/w"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v4, v19, v20

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 165
    .local v18, "webDataPath":Ljava/lang/String;
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v19, "http"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 166
    invoke-virtual {v3, v8}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 168
    .local v7, "webDataBaseUri":Landroid/net/Uri;
    const-string v3, "%s%s/"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "/e"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v4, v19, v20

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 169
    .local v10, "exportedContentPath":Ljava/lang/String;
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v19, "http"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 170
    invoke-virtual {v3, v8}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 172
    .local v6, "exportedContentBaseUri":Landroid/net/Uri;
    const-string v3, "%s%s/"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "/c"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v4, v19, v20

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 173
    .local v9, "contentPath":Ljava/lang/String;
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v19, "http"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 174
    invoke-virtual {v3, v8}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 176
    .local v5, "contentBaseUri":Landroid/net/Uri;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v19, "Starting content services: %s, %s, %s"

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v7, v20, v21

    const/16 v21, 0x1

    aput-object v6, v20, v21

    const/16 v21, 0x2

    aput-object v5, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    new-instance v2, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$HandleImpl;-><init>(Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 184
    .end local v4    # "requestToken":Ljava/lang/String;
    .end local v5    # "contentBaseUri":Landroid/net/Uri;
    .end local v6    # "exportedContentBaseUri":Landroid/net/Uri;
    .end local v7    # "webDataBaseUri":Landroid/net/Uri;
    .end local v8    # "authority":Ljava/lang/String;
    .end local v9    # "contentPath":Ljava/lang/String;
    .end local v10    # "exportedContentPath":Ljava/lang/String;
    .end local v11    # "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .end local v14    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    .end local v16    # "rawRequestToken":J
    .end local v18    # "webDataPath":Ljava/lang/String;
    .local v2, "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    :goto_1
    return-object v2

    .line 158
    .end local v2    # "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .restart local v8    # "authority":Ljava/lang/String;
    .restart local v11    # "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .restart local v14    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    :catchall_0
    move-exception v3

    invoke-static {v14}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 179
    .end local v8    # "authority":Ljava/lang/String;
    .end local v14    # "policy":Landroid/os/StrictMode$ThreadPolicy;
    :catch_0
    move-exception v15

    .line 180
    .local v15, "tr":Ljava/lang/Throwable;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v3, v15}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->unbind()V

    move-object v2, v11

    .end local v11    # "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    .restart local v2    # "handle":Lcom/google/apps/dots/android/newsstand/provider/HttpContentService$Handle;
    goto :goto_1
.end method

.method public unbind()V
    .locals 4

    .prologue
    .line 188
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    if-eqz v2, :cond_0

    .line 190
    :try_start_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeServerSocket:Ljava/net/ServerSocket;

    .line 197
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTokens:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->clear()V

    .line 198
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->activeTasks:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;

    .line 199
    .local v0, "task":Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;->cancel()V

    goto :goto_1

    .line 193
    .end local v0    # "task":Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl$ServeConnectionTask;
    :catch_0
    move-exception v1

    .line 194
    .local v1, "tr":Ljava/lang/Throwable;
    sget-object v2, Lcom/google/apps/dots/android/newsstand/provider/HttpContentServiceImpl;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 202
    .end local v1    # "tr":Ljava/lang/Throwable;
    :cond_0
    return-void

    .line 191
    :catch_1
    move-exception v2

    goto :goto_0
.end method
