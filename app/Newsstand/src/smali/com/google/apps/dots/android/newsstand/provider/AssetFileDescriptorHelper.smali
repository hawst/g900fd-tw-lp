.class public Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;
.super Ljava/lang/Object;
.source "AssetFileDescriptorHelper.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public static afdForFuture(Lcom/google/common/util/concurrent/ListenableFuture;Z)Landroid/content/res/AssetFileDescriptor;
    .locals 9
    .param p1, "noPumpFd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;Z)",
            "Landroid/content/res/AssetFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "afdFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/content/res/AssetFileDescriptor;>;"
    if-eqz p1, :cond_0

    .line 49
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/res/AssetFileDescriptor;

    .line 50
    .local v8, "result":Landroid/content/res/AssetFileDescriptor;
    if-nez v8, :cond_1

    .line 51
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 57
    .end local v8    # "result":Landroid/content/res/AssetFileDescriptor;
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 61
    .local v7, "fds":[Landroid/os/ParcelFileDescriptor;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;

    invoke-direct {v0, v7}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$2;-><init>([Landroid/os/ParcelFileDescriptor;)V

    sget-object v1, Lcom/google/apps/dots/android/newsstand/async/Queues;->NSCLIENT_PRIVATE:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 88
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    const/4 v1, 0x0

    aget-object v1, v7, v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    move-object v8, v0

    .end local v7    # "fds":[Landroid/os/ParcelFileDescriptor;
    :cond_1
    return-object v8

    .line 58
    :catch_0
    move-exception v6

    .line 59
    .local v6, "e":Ljava/io/IOException;
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method public static closeQuietly(Landroid/content/res/AssetFileDescriptor;)V
    .locals 4
    .param p0, "fd"    # Landroid/content/res/AssetFileDescriptor;

    .prologue
    .line 103
    if-eqz p0, :cond_0

    .line 104
    :try_start_0
    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Failure closing AFD"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static closeQuietly(Landroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p0, "fd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 93
    if-eqz p0, :cond_0

    .line 94
    :try_start_0
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Failure closing PFD"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static extractAFD(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/store/StoreResponse;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "storeFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Lcom/google/apps/dots/android/newsstand/store/StoreResponse;>;"
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/provider/AssetFileDescriptorHelper$1;-><init>()V

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/AsyncFunction;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
