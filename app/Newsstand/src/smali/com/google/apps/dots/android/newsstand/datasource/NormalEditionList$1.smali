.class Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;
.source "NormalEditionList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;->logResponseNodeStats(Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

.field final synthetic val$appTitle:[Ljava/lang/String;

.field final synthetic val$posts:[I

.field final synthetic val$sections:[I


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;[Ljava/lang/String;[I[I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$appTitle:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$sections:[I

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$posts:[I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hasAppSummary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$appTitle:[Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 46
    :cond_0
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$sections:[I

    aget v1, v0, v2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v2

    .line 49
    :cond_1
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/NormalEditionList$1;->val$posts:[I

    aget v1, v0, v2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v2

    .line 52
    :cond_2
    return-void
.end method
