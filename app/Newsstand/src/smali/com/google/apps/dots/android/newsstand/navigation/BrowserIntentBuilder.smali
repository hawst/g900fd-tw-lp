.class public Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;
.source "BrowserIntentBuilder.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 13
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/BrowserIntentBuilder;->intent:Landroid/content/Intent;

    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 14
    return-void
.end method
