.class public Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;
.super Landroid/widget/LinearLayout;
.source "ActionableToastBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$ActionClickedListener;
    }
.end annotation


# static fields
.field public static final ACCESSIBILITY_ACTIONS_TO_HIDE_BAR:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_ACTION_ICON:I


# instance fields
.field private final a11yHideRunnable:Ljava/lang/Runnable;

.field private actionText:Landroid/widget/TextView;

.field private currentlyHidden:Z

.field private descriptionText:Landroid/widget/TextView;

.field private final fadeOutHandler:Landroid/os/Handler;

.field private final hideRunnable:Ljava/lang/Runnable;

.field private separator:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/common/collect/ImmutableSet$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableSet$Builder;-><init>()V

    const/16 v1, 0x1000

    .line 53
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->ACCESSIBILITY_ACTIONS_TO_HIDE_BAR:Ljava/util/Set;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z

    .line 75
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 76
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v3, Lcom/google/android/apps/newsstanddev/R$layout;->actionable_toast_bar:I

    invoke-virtual {v2, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 78
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->actionable_toast_bar_description_text:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    .line 79
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->actionable_toast_bar_separator:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->separator:Landroid/view/View;

    .line 80
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->actionable_toast_bar_action_text:I

    invoke-virtual {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    .line 82
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    .line 83
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hideRunnable:Ljava/lang/Runnable;

    .line 93
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 94
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getContentView()Landroid/view/View;

    move-result-object v1

    .line 95
    .local v1, "contentView":Landroid/view/View;
    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->a11yHideRunnable:Ljava/lang/Runnable;

    .line 116
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hide(Z)V

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    return-object v0
.end method

.method public static getActionableToastBar(Landroid/app/Activity;)Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 289
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->actionable_toast_bar:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    return-object v0
.end method

.method private static setDrawableStart(Landroid/widget/TextView;I)V
    .locals 3
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "resourceId"    # I

    .prologue
    const/4 v2, 0x0

    .line 237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v0

    if-nez v0, :cond_1

    .line 238
    :cond_0
    invoke-virtual {p0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_1
    invoke-virtual {p0, v2, v2, p1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method

.method private static setTextAlignmentStart(Landroid/widget/TextView;)V
    .locals 2
    .param p0, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 245
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    move-result v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_1
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public static showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
    .param p3, "replaceVisibleToast"    # Z

    .prologue
    .line 263
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->getActionableToastBar(Landroid/app/Activity;)Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    move-result-object v0

    .line 264
    .local v0, "bar":Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V

    .line 265
    return-void
.end method

.method public static showToast(Landroid/app/Activity;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
    .param p3, "replaceVisibleToast"    # Z
    .param p4, "displayMs"    # I

    .prologue
    .line 278
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->getActionableToastBar(Landroid/app/Activity;)Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;

    move-result-object v0

    .line 279
    .local v0, "bar":Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V

    .line 280
    return-void
.end method


# virtual methods
.method public hide(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z

    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 222
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->a11yHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 223
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 226
    if-eqz p1, :cond_1

    .line 228
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_fade_duration_ms:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v1, 0x0

    .line 227
    invoke-static {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeOut(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;Z)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
    .param p3, "replaceVisibleToast"    # Z

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V

    .line 131
    return-void
.end method

.method public show(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;ZI)V
    .locals 8
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
    .param p3, "replaceVisibleToast"    # Z
    .param p4, "displayMs"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 148
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z

    if-nez v3, :cond_0

    if-nez p3, :cond_0

    .line 212
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 156
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_display_duration_short_ms:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 159
    .local v2, "displayDuration":I
    if-eqz p2, :cond_2

    .line 161
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_display_duration_long_ms:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 164
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->setTextAlignmentStart(Landroid/widget/TextView;)V

    .line 165
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->separator:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->getOperationLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {p2}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;->getActionIconResourceId()I

    move-result v1

    .line 172
    .local v1, "actionIconResourceId":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->setDrawableStart(Landroid/widget/TextView;I)V

    .line 174
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    new-instance v4, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;

    invoke-direct {v4, p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    .end local v1    # "actionIconResourceId":I
    :goto_1
    iput-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->currentlyHidden:Z

    .line 193
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$integer;->actionable_toast_bar_fade_duration_ms:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const/4 v4, 0x0

    .line 192
    invoke-static {p0, v3, v4}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->fadeIn(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 195
    if-lez p4, :cond_1

    move v2, p4

    .line 197
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isA11yEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 198
    .local v0, "a11yEnabled":Z
    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    .line 202
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->a11yHideRunnable:Ljava/lang/Runnable;

    int-to-long v6, v2

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 203
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->focus(Landroid/view/View;)V

    goto :goto_0

    .line 186
    .end local v0    # "a11yEnabled":Z
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 187
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->separator:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 188
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->actionText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 204
    .restart local v0    # "a11yEnabled":Z
    :cond_3
    if-eqz v0, :cond_4

    if-nez p2, :cond_4

    .line 206
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hideRunnable:Ljava/lang/Runnable;

    int-to-long v6, v2

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 207
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->descriptionText:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->announce(Landroid/view/View;)V

    goto/16 :goto_0

    .line 210
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->fadeOutHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/ActionableToastBar;->hideRunnable:Ljava/lang/Runnable;

    int-to-long v6, v2

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
