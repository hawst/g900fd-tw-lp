.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;
.super Ljava/lang/Object;
.source "NewsWebView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->setupNewsArticleTail(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

.field final synthetic val$parentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;->val$parentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;->val$parentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->handleWebViewScrollChanged()V

    .line 200
    return-void
.end method

.method public onSizeChanged()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$1;->val$parentLayout:Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->handleWebViewSizeChanged()V

    .line 195
    return-void
.end method
