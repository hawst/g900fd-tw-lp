.class public final Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsSyncV3.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsSyncV3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Node"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;


# instance fields
.field private ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

.field private announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

.field private appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

.field private appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

.field private bitField0_:I

.field public child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

.field private clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

.field private clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

.field private collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

.field private debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

.field private experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

.field private exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

.field private exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

.field private finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

.field private form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

.field private geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

.field private incomplete_:Z

.field private merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

.field private offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

.field private popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

.field private postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

.field private postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

.field private postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

.field private psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

.field private purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

.field private readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

.field public resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

.field private searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

.field private sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

.field private self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

.field private type_:I

.field private webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 234
    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    .line 253
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    .line 272
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 275
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 179
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 5

    .prologue
    .line 812
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 816
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v3, :cond_0

    .line 817
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 819
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 820
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 821
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 822
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 823
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v4

    aput-object v4, v3, v2

    .line 821
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 813
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 814
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 827
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 828
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 829
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 830
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v3, v3, v2

    if-eqz v3, :cond_3

    .line 831
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    aput-object v4, v3, v2

    .line 829
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 835
    .end local v2    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v3, :cond_5

    .line 836
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 838
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-eqz v3, :cond_6

    .line 839
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 841
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v3, :cond_7

    .line 842
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 844
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v3, :cond_8

    .line 845
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 847
    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-eqz v3, :cond_9

    .line 848
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 850
    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-eqz v3, :cond_a

    .line 851
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    .line 853
    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v3, :cond_b

    .line 854
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 856
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v3, :cond_c

    .line 857
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 859
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v3, :cond_d

    .line 860
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 862
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-eqz v3, :cond_e

    .line 863
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    .line 865
    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-eqz v3, :cond_f

    .line 866
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .line 868
    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-eqz v3, :cond_10

    .line 869
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    .line 871
    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-eqz v3, :cond_11

    .line 872
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .line 874
    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-eqz v3, :cond_12

    .line 875
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Form;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 877
    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-eqz v3, :cond_13

    .line 878
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    .line 880
    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-eqz v3, :cond_14

    .line 881
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 883
    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v3, :cond_15

    .line 884
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 886
    :cond_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-eqz v3, :cond_16

    .line 887
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 889
    :cond_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-eqz v3, :cond_17

    .line 890
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    .line 892
    :cond_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-eqz v3, :cond_18

    .line 893
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    .line 895
    :cond_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-eqz v3, :cond_19

    .line 896
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 898
    :cond_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-eqz v3, :cond_1a

    .line 899
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    .line 901
    :cond_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-eqz v3, :cond_1b

    .line 902
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    .line 904
    :cond_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-eqz v3, :cond_1c

    .line 905
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    .line 907
    :cond_1c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-eqz v3, :cond_1d

    .line 908
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    .line 910
    :cond_1d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-eqz v3, :cond_1e

    .line 911
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    .line 913
    :cond_1e
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 918
    if-ne p1, p0, :cond_1

    .line 951
    :cond_0
    :goto_0
    return v1

    .line 919
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 920
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 921
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v3, :cond_3

    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 924
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 925
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v3, :cond_3

    .line 926
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-nez v3, :cond_3

    .line 927
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v3, :cond_3

    .line 928
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v3, :cond_3

    .line 929
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-nez v3, :cond_3

    .line 930
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-nez v3, :cond_3

    .line 931
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v3, :cond_3

    .line 932
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v3, :cond_3

    .line 933
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v3, :cond_3

    .line 934
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-nez v3, :cond_3

    .line 935
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-nez v3, :cond_3

    .line 936
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-nez v3, :cond_3

    .line 937
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-nez v3, :cond_3

    .line 938
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-nez v3, :cond_3

    .line 939
    :goto_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-nez v3, :cond_3

    .line 940
    :goto_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-nez v3, :cond_3

    .line 941
    :goto_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v3, :cond_15

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v3, :cond_3

    .line 942
    :goto_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v3, :cond_16

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v3, :cond_3

    .line 943
    :goto_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-nez v3, :cond_17

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-nez v3, :cond_3

    .line 944
    :goto_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-nez v3, :cond_18

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-nez v3, :cond_3

    .line 945
    :goto_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-nez v3, :cond_19

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-nez v3, :cond_3

    .line 946
    :goto_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-nez v3, :cond_1a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-nez v3, :cond_3

    .line 947
    :goto_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-nez v3, :cond_1b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-nez v3, :cond_3

    .line 948
    :goto_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-nez v3, :cond_1c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-nez v3, :cond_3

    .line 949
    :goto_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-nez v3, :cond_1d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-nez v3, :cond_3

    .line 950
    :goto_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-nez v3, :cond_1e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 951
    goto/16 :goto_0

    .line 921
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    .line 925
    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 926
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 927
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 928
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 929
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 930
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    .line 931
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 932
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 933
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 934
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    .line 935
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .line 936
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    .line 937
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .line 938
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 939
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    .line 940
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 941
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 942
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_12

    :cond_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 943
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_13

    :cond_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    .line 944
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_14

    :cond_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    .line 945
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_15

    :cond_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 946
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_16

    :cond_1a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    .line 947
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_17

    :cond_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    .line 948
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_18

    :cond_1c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    .line 949
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_19

    :cond_1d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    .line 950
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1a

    :cond_1e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    .line 951
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    return-object v0
.end method

.method public getAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    return-object v0
.end method

.method public getClientConfig()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    return-object v0
.end method

.method public getClusterSummary()Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    return-object v0
.end method

.method public getCollectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    return-object v0
.end method

.method public getExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    return-object v0
.end method

.method public getExploreLinkSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    return-object v0
.end method

.method public getGeoLocationSummary()Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    return-object v0
.end method

.method public getIncomplete()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    return v0
.end method

.method public getMerchandisingShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    return-object v0
.end method

.method public getOfferSummary()Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    return-object v0
.end method

.method public getPostReadState()Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    return-object v0
.end method

.method public getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    return-object v0
.end method

.method public getPurchaseSummary()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    return-object v0
.end method

.method public getReadNowEditionShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    return-object v0
.end method

.method public getSectionSummary()Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    return-object v0
.end method

.method public getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1108
    const/4 v1, 0x0

    .line 1109
    .local v1, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v3, :cond_0

    .line 1110
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 1111
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1113
    :cond_0
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 1114
    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    .line 1115
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 1117
    :cond_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    .line 1118
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    .line 1119
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 1121
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v3, :cond_4

    .line 1122
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v0, v4, v3

    .line 1123
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    if-eqz v0, :cond_3

    .line 1124
    const/4 v6, 0x4

    .line 1125
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 1122
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1129
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v3, :cond_6

    .line 1130
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v0, v3, v2

    .line 1131
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    if-eqz v0, :cond_5

    .line 1132
    const/4 v5, 0x5

    .line 1133
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1130
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1137
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v2, :cond_7

    .line 1138
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 1139
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1141
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-eqz v2, :cond_8

    .line 1142
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 1143
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1145
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v2, :cond_9

    .line 1146
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 1147
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1149
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v2, :cond_a

    .line 1150
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 1151
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1153
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-eqz v2, :cond_b

    .line 1154
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 1155
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1157
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-eqz v2, :cond_c

    .line 1158
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    .line 1159
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1161
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v2, :cond_d

    .line 1162
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 1163
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1165
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v2, :cond_e

    .line 1166
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 1167
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1169
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v2, :cond_f

    .line 1170
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 1171
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1173
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-eqz v2, :cond_10

    .line 1174
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    .line 1175
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1177
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-eqz v2, :cond_11

    .line 1178
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .line 1179
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1181
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-eqz v2, :cond_12

    .line 1182
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    .line 1183
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1185
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-eqz v2, :cond_13

    .line 1186
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .line 1187
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1189
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-eqz v2, :cond_14

    .line 1190
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 1191
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1193
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-eqz v2, :cond_15

    .line 1194
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    .line 1195
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1197
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-eqz v2, :cond_16

    .line 1198
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 1199
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1201
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v2, :cond_17

    .line 1202
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 1203
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1205
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-eqz v2, :cond_18

    .line 1206
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 1207
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1209
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-eqz v2, :cond_19

    .line 1210
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    .line 1211
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1213
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-eqz v2, :cond_1a

    .line 1214
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    .line 1215
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1217
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-eqz v2, :cond_1b

    .line 1218
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    .line 1219
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1221
    :cond_1b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-eqz v2, :cond_1c

    .line 1222
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    .line 1223
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1225
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-eqz v2, :cond_1d

    .line 1226
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 1227
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1229
    :cond_1d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-eqz v2, :cond_1e

    .line 1230
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    .line 1231
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1233
    :cond_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-eqz v2, :cond_1f

    .line 1234
    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    .line 1235
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1237
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-eqz v2, :cond_20

    .line 1238
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    .line 1239
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1241
    :cond_20
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->cachedSize:I

    .line 1242
    return v1
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    return v0
.end method

.method public getWebPageSummary()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    return-object v0
.end method

.method public hasAppFamilySummary()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAppSummary()Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClusterSummary()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCollectionSummary()Z
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExploreGroupSummary()Z
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExploreLinkSummary()Z
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGeoLocationSummary()Z
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMerchandisingShelfSummary()Z
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOfferSummary()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPostSummary()Z
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPurchaseSummary()Z
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReadNowEditionShelfSummary()Z
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSectionSummary()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSelf()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWebPageSummary()Z
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 955
    const/16 v1, 0x11

    .line 956
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 957
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 958
    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    add-int v1, v4, v2

    .line 959
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    add-int v1, v2, v4

    .line 960
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 966
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v2, :cond_6

    mul-int/lit8 v1, v1, 0x1f

    .line 972
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v2, :cond_8

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 973
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-nez v2, :cond_9

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 974
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v2, :cond_a

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 975
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v2, :cond_b

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 976
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-nez v2, :cond_c

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 977
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-nez v2, :cond_d

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 978
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v2, :cond_e

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 979
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v2, :cond_f

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 980
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v2, :cond_10

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 981
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-nez v2, :cond_11

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    .line 982
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-nez v2, :cond_12

    move v2, v3

    :goto_c
    add-int v1, v4, v2

    .line 983
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-nez v2, :cond_13

    move v2, v3

    :goto_d
    add-int v1, v4, v2

    .line 984
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-nez v2, :cond_14

    move v2, v3

    :goto_e
    add-int v1, v4, v2

    .line 985
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-nez v2, :cond_15

    move v2, v3

    :goto_f
    add-int v1, v4, v2

    .line 986
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-nez v2, :cond_16

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 987
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-nez v2, :cond_17

    move v2, v3

    :goto_11
    add-int v1, v4, v2

    .line 988
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v2, :cond_18

    move v2, v3

    :goto_12
    add-int v1, v4, v2

    .line 989
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v2, :cond_19

    move v2, v3

    :goto_13
    add-int v1, v4, v2

    .line 990
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-nez v2, :cond_1a

    move v2, v3

    :goto_14
    add-int v1, v4, v2

    .line 991
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-nez v2, :cond_1b

    move v2, v3

    :goto_15
    add-int v1, v4, v2

    .line 992
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-nez v2, :cond_1c

    move v2, v3

    :goto_16
    add-int v1, v4, v2

    .line 993
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-nez v2, :cond_1d

    move v2, v3

    :goto_17
    add-int v1, v4, v2

    .line 994
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-nez v2, :cond_1e

    move v2, v3

    :goto_18
    add-int v1, v4, v2

    .line 995
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-nez v2, :cond_1f

    move v2, v3

    :goto_19
    add-int v1, v4, v2

    .line 996
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-nez v2, :cond_20

    move v2, v3

    :goto_1a
    add-int v1, v4, v2

    .line 997
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-nez v4, :cond_21

    :goto_1b
    add-int v1, v2, v3

    .line 998
    return v1

    .line 957
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 958
    :cond_3
    const/4 v2, 0x2

    goto/16 :goto_1

    .line 962
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 963
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v3

    :goto_1d
    add-int v1, v4, v2

    .line 962
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 963
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->hashCode()I

    move-result v2

    goto :goto_1d

    .line 968
    .end local v0    # "i":I
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 969
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v3

    :goto_1f
    add-int v1, v4, v2

    .line 968
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 969
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hashCode()I

    move-result v2

    goto :goto_1f

    .line 972
    .end local v0    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 973
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 974
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 975
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 976
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 977
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 978
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 979
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 980
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 981
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 982
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 983
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 984
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 985
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 986
    :cond_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 987
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->hashCode()I

    move-result v2

    goto/16 :goto_11

    .line 988
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->hashCode()I

    move-result v2

    goto/16 :goto_12

    .line 989
    :cond_19
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->hashCode()I

    move-result v2

    goto/16 :goto_13

    .line 990
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;->hashCode()I

    move-result v2

    goto/16 :goto_14

    .line 991
    :cond_1b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;->hashCode()I

    move-result v2

    goto/16 :goto_15

    .line 992
    :cond_1c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->hashCode()I

    move-result v2

    goto/16 :goto_16

    .line 993
    :cond_1d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;->hashCode()I

    move-result v2

    goto/16 :goto_17

    .line 994
    :cond_1e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;->hashCode()I

    move-result v2

    goto/16 :goto_18

    .line 995
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;->hashCode()I

    move-result v2

    goto/16 :goto_19

    .line 996
    :cond_20
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;->hashCode()I

    move-result v2

    goto/16 :goto_1a

    .line 997
    :cond_21
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;->hashCode()I

    move-result v3

    goto/16 :goto_1b
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1251
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1255
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1256
    :sswitch_0
    return-object p0

    .line 1261
    :sswitch_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v6, :cond_1

    .line 1262
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 1264
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1268
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    .line 1269
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    goto :goto_0

    .line 1273
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1274
    .local v4, "temp":I
    if-eqz v4, :cond_2

    const/4 v6, 0x1

    if-eq v4, v6, :cond_2

    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_2

    const/4 v6, 0x6

    if-eq v4, v6, :cond_2

    const/4 v6, 0x7

    if-eq v4, v6, :cond_2

    const/16 v6, 0x8

    if-eq v4, v6, :cond_2

    const/16 v6, 0x9

    if-eq v4, v6, :cond_2

    const/16 v6, 0xa

    if-eq v4, v6, :cond_2

    const/16 v6, 0xb

    if-eq v4, v6, :cond_2

    const/16 v6, 0xc

    if-eq v4, v6, :cond_2

    const/16 v6, 0xd

    if-eq v4, v6, :cond_2

    const/16 v6, 0xe

    if-eq v4, v6, :cond_2

    const/16 v6, 0xf

    if-eq v4, v6, :cond_2

    const/16 v6, 0x10

    if-eq v4, v6, :cond_2

    const/16 v6, 0x11

    if-eq v4, v6, :cond_2

    const/16 v6, 0x12

    if-eq v4, v6, :cond_2

    const/16 v6, 0x13

    if-eq v4, v6, :cond_2

    const/16 v6, 0x14

    if-eq v4, v6, :cond_2

    const/16 v6, 0x15

    if-eq v4, v6, :cond_2

    const/16 v6, 0x16

    if-eq v4, v6, :cond_2

    const/16 v6, 0x17

    if-eq v4, v6, :cond_2

    const/16 v6, 0x18

    if-eq v4, v6, :cond_2

    const/16 v6, 0x19

    if-eq v4, v6, :cond_2

    const/16 v6, 0x1a

    if-eq v4, v6, :cond_2

    const/16 v6, 0x1b

    if-ne v4, v6, :cond_3

    .line 1302
    :cond_2
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    .line 1303
    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    goto/16 :goto_0

    .line 1305
    :cond_3
    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    goto/16 :goto_0

    .line 1310
    .end local v4    # "temp":I
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1311
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v6, :cond_5

    move v1, v5

    .line 1312
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 1313
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v6, :cond_4

    .line 1314
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1316
    :cond_4
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 1317
    :goto_2
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 1318
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;-><init>()V

    aput-object v7, v6, v1

    .line 1319
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1320
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1317
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1311
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :cond_5
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v1, v6

    goto :goto_1

    .line 1323
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :cond_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;-><init>()V

    aput-object v7, v6, v1

    .line 1324
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1328
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1329
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v6, :cond_8

    move v1, v5

    .line 1330
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 1331
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v6, :cond_7

    .line 1332
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1334
    :cond_7
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 1335
    :goto_4
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 1336
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    aput-object v7, v6, v1

    .line 1337
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1338
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1335
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1329
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v1, v6

    goto :goto_3

    .line 1341
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    new-instance v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    aput-object v7, v6, v1

    .line 1342
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1346
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :sswitch_6
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-nez v6, :cond_a

    .line 1347
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 1349
    :cond_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1353
    :sswitch_7
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-nez v6, :cond_b

    .line 1354
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 1356
    :cond_b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1360
    :sswitch_8
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v6, :cond_c

    .line 1361
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 1363
    :cond_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1367
    :sswitch_9
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v6, :cond_d

    .line 1368
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 1370
    :cond_d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1374
    :sswitch_a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-nez v6, :cond_e

    .line 1375
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 1377
    :cond_e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1381
    :sswitch_b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-nez v6, :cond_f

    .line 1382
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    .line 1384
    :cond_f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1388
    :sswitch_c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v6, :cond_10

    .line 1389
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 1391
    :cond_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1395
    :sswitch_d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-nez v6, :cond_11

    .line 1396
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 1398
    :cond_11
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1402
    :sswitch_e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-nez v6, :cond_12

    .line 1403
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 1405
    :cond_12
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1409
    :sswitch_f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-nez v6, :cond_13

    .line 1410
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    .line 1412
    :cond_13
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1416
    :sswitch_10
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-nez v6, :cond_14

    .line 1417
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .line 1419
    :cond_14
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1423
    :sswitch_11
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-nez v6, :cond_15

    .line 1424
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    .line 1426
    :cond_15
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1430
    :sswitch_12
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-nez v6, :cond_16

    .line 1431
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    .line 1433
    :cond_16
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1437
    :sswitch_13
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-nez v6, :cond_17

    .line 1438
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Form;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .line 1440
    :cond_17
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1444
    :sswitch_14
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-nez v6, :cond_18

    .line 1445
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    .line 1447
    :cond_18
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1451
    :sswitch_15
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-nez v6, :cond_19

    .line 1452
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 1454
    :cond_19
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1458
    :sswitch_16
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v6, :cond_1a

    .line 1459
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 1461
    :cond_1a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1465
    :sswitch_17
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v6, :cond_1b

    .line 1466
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 1468
    :cond_1b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1472
    :sswitch_18
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-nez v6, :cond_1c

    .line 1473
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    .line 1475
    :cond_1c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1479
    :sswitch_19
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-nez v6, :cond_1d

    .line 1480
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    .line 1482
    :cond_1d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1486
    :sswitch_1a
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-nez v6, :cond_1e

    .line 1487
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    .line 1489
    :cond_1e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1493
    :sswitch_1b
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-nez v6, :cond_1f

    .line 1494
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    .line 1496
    :cond_1f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1500
    :sswitch_1c
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-nez v6, :cond_20

    .line 1501
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 1503
    :cond_20
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1507
    :sswitch_1d
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-nez v6, :cond_21

    .line 1508
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    .line 1510
    :cond_21
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1514
    :sswitch_1e
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-nez v6, :cond_22

    .line 1515
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Ad;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    .line 1517
    :cond_22
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1521
    :sswitch_1f
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-nez v6, :cond_23

    .line 1522
    new-instance v6, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;-><init>()V

    iput-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    .line 1524
    :cond_23
    iget-object v6, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1251
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xfa -> :sswitch_1f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v0

    return-object v0
.end method

.method public setAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 344
    return-object p0
.end method

.method public setAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .prologue
    .line 321
    if-nez p1, :cond_0

    .line 322
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 324
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 325
    return-object p0
.end method

.method public setOfferSummary(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 416
    if-nez p1, :cond_0

    .line 417
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 419
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .line 420
    return-object p0
.end method

.method public setPostReadState(Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .prologue
    .line 397
    if-nez p1, :cond_0

    .line 398
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 400
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    .line 401
    return-object p0
.end method

.method public setPostSummary(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .prologue
    .line 283
    if-nez p1, :cond_0

    .line 284
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 286
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    .line 287
    return-object p0
.end method

.method public setType(I)Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    .line 259
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    .line 260
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1003
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v2, :cond_0

    .line 1004
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1006
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 1007
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->incomplete_:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1009
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    .line 1010
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->type_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1012
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v2, :cond_4

    .line 1013
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->resource:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 1014
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    if-eqz v0, :cond_3

    .line 1015
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1013
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1019
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v2, :cond_6

    .line 1020
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->child:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v0, v2, v1

    .line 1021
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    if-eqz v0, :cond_5

    .line 1022
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1020
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1026
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    if-eqz v1, :cond_7

    .line 1027
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1029
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    if-eqz v1, :cond_8

    .line 1030
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->sectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1032
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v1, :cond_9

    .line 1033
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1035
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-eqz v1, :cond_a

    .line 1036
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->appFamilySummary_:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1038
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    if-eqz v1, :cond_b

    .line 1039
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->webPageSummary_:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1041
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    if-eqz v1, :cond_c

    .line 1042
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clusterSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ClusterSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1044
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v1, :cond_d

    .line 1045
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postReadState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1047
    :cond_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    if-eqz v1, :cond_e

    .line 1048
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->offerSummary_:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1050
    :cond_e
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    if-eqz v1, :cond_f

    .line 1051
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->postMeteredState_:Lcom/google/apps/dots/proto/client/DotsShared$PostReadState;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1053
    :cond_f
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    if-eqz v1, :cond_10

    .line 1054
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->geoLocationSummary_:Lcom/google/apps/dots/proto/client/DotsShared$GeoLocationSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1056
    :cond_10
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    if-eqz v1, :cond_11

    .line 1057
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->merchandisingShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1059
    :cond_11
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    if-eqz v1, :cond_12

    .line 1060
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->searchResultSummary_:Lcom/google/apps/dots/proto/client/DotsShared$SearchResultSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1062
    :cond_12
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    if-eqz v1, :cond_13

    .line 1063
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreGroupSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1065
    :cond_13
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    if-eqz v1, :cond_14

    .line 1066
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->form_:Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1068
    :cond_14
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    if-eqz v1, :cond_15

    .line 1069
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->readNowEditionShelfSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ReadNowEditionShelfSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1071
    :cond_15
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-eqz v1, :cond_16

    .line 1072
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->purchaseSummary_:Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1074
    :cond_16
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v1, :cond_17

    .line 1075
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1077
    :cond_17
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-eqz v1, :cond_18

    .line 1078
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clientConfig_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1080
    :cond_18
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    if-eqz v1, :cond_19

    .line 1081
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->announcementSummary_:Lcom/google/apps/dots/proto/client/DotsShared$AnnouncementSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1083
    :cond_19
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    if-eqz v1, :cond_1a

    .line 1084
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->popularPosts_:Lcom/google/apps/dots/proto/client/DotsShared$PopularPosts;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1086
    :cond_1a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    if-eqz v1, :cond_1b

    .line 1087
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->debugInfo_:Lcom/google/apps/dots/proto/client/DotsShared$DebugInfo;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1089
    :cond_1b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    if-eqz v1, :cond_1c

    .line 1090
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->collectionSummary_:Lcom/google/apps/dots/proto/client/DotsShared$CollectionSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1092
    :cond_1c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-eqz v1, :cond_1d

    .line 1093
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->exploreLinkSummary_:Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1095
    :cond_1d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    if-eqz v1, :cond_1e

    .line 1096
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->psvStart_:Lcom/google/apps/dots/proto/client/DotsShared$PsvStart;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1098
    :cond_1e
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    if-eqz v1, :cond_1f

    .line 1099
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->ad_:Lcom/google/apps/dots/proto/client/DotsShared$Ad;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1101
    :cond_1f
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    if-eqz v1, :cond_20

    .line 1102
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->finskyDocument_:Lcom/google/apps/dots/proto/client/DotsFinsky$FinskyDocumentSummary;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1104
    :cond_20
    return-void
.end method
