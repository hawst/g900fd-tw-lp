.class Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;
.super Ljava/lang/Object;
.source "MoreReloadoIntentBuilder.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;->enableReloadoTransition(Landroid/view/ViewGroup;Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

.field final synthetic val$cardClass:Ljava/lang/Class;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;Ljava/lang/Class;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;->this$0:Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;->val$cardClass:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;)Z
    .locals 2
    .param p1, "input"    # Landroid/view/View;

    .prologue
    .line 56
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;->val$cardClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/MoreReloadoIntentBuilder$1;->apply(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
