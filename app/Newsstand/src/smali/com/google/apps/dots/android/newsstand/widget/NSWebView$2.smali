.class Lcom/google/apps/dots/android/newsstand/widget/NSWebView$2;
.super Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;
.source "NSWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NSWebView;->initWebView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NSWebView;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NSWebView;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NSWebView$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NSWebView;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;-><init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V

    return-void
.end method


# virtual methods
.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 102
    if-eqz p2, :cond_0

    const-string v0, "file"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Landroid/webkit/WebResourceResponse;

    invoke-direct {v0, v2, v2, v2}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 105
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/DotsWebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 110
    if-nez p2, :cond_1

    const/4 v1, 0x0

    .line 111
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getNSActivityFromView(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 112
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    if-eqz v1, :cond_0

    .line 113
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->isHttp(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 114
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/uri/UriDispatcher;->showInBrowser(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 110
    .end local v0    # "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 114
    .restart local v0    # "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
