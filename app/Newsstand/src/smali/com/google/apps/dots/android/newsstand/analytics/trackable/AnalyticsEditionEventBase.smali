.class public abstract Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionEventBase;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;
.source "AnalyticsEditionEventBase.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEditionBase;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 19
    if-eqz p2, :cond_0

    const-string v0, "Publisher"

    .line 21
    .local v0, "category":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    return-object v1

    .line 19
    .end local v0    # "category":Ljava/lang/String;
    :cond_0
    const-string v0, "Internal"

    goto :goto_0
.end method
