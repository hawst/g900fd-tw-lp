.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;
.super Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
.source "ImagePartView.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private attachmentId:Ljava/lang/String;

.field private file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field private imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

.field private paint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;)V

    .line 47
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->paint:Landroid/graphics/Paint;

    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->paint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;)Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    return-object p1
.end method

.method static synthetic access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private downloadAndInitBounds()V
    .locals 4

    .prologue
    .line 62
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->attachmentId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;)V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DECODE_BITMAP:Lcom/google/apps/dots/android/newsstand/async/Queue;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    .line 60
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 80
    return-void
.end method

.method private getCacheKey(I)Ljava/lang/String;
    .locals 3
    .param p1, "sampleSize"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->attachmentId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPreferredBitmapConfig(Z)Landroid/graphics/Bitmap$Config;
    .locals 2
    .param p0, "hasAlpha"    # Z

    .prologue
    .line 83
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getPerApplicationMemoryClass()I

    move-result v0

    .line 84
    .local v0, "memClass":I
    if-nez p0, :cond_0

    const/16 v1, 0x40

    if-le v0, v1, :cond_1

    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method private hasBitmap()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static loadBitmap(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "blobFile"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .param p1, "config"    # Landroid/graphics/Bitmap$Config;
    .param p2, "sampleSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 160
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 161
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-object p1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 162
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 163
    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 164
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 165
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 167
    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;

    invoke-direct {v1, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;-><init>(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/BitmapFactory$Options;)V

    .line 189
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView$2;->run()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    return-object v1
.end method


# virtual methods
.method public getBitmap(IZ)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "sampleSize"    # I
    .param p2, "purgeable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 98
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->hasBitmap()Z

    move-result v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 101
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->getScreenRect()Landroid/graphics/RectF;

    move-result-object v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->areaOf(Landroid/graphics/RectF;)F

    move-result v4

    .line 102
    .local v4, "screenArea":F
    if-ne p1, v0, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .line 103
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->getNumPixels()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3e800000    # 0.25f

    mul-float/2addr v6, v4

    cmpg-float v5, v5, v6

    if-lez v5, :cond_0

    sget-object v5, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    .line 104
    invoke-virtual {v5}, Lcom/google/android/libraries/bind/async/JankLock;->isPaused()Z

    move-result v5

    if-nez v5, :cond_1

    .line 106
    .local v0, "allowAlloc":Z
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget-boolean v5, v5, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->getPreferredBitmapConfig(Z)Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 108
    .local v1, "preferredConfig":Landroid/graphics/Bitmap$Config;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->getCacheKey(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getCachedBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 109
    .local v2, "purgeableBacking":Landroid/graphics/Bitmap;
    if-nez v2, :cond_3

    .line 110
    if-nez v0, :cond_2

    .line 111
    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;-><init>()V

    throw v5

    .line 104
    .end local v0    # "allowAlloc":Z
    .end local v1    # "preferredConfig":Landroid/graphics/Bitmap$Config;
    .end local v2    # "purgeableBacking":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    .restart local v0    # "allowAlloc":Z
    .restart local v1    # "preferredConfig":Landroid/graphics/Bitmap$Config;
    .restart local v2    # "purgeableBacking":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    invoke-static {v5, v1, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->loadBitmap(Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;Landroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 116
    :cond_3
    const/4 v3, 0x0

    .line 117
    .local v3, "result":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_6

    .line 119
    move-object v3, v2

    .line 135
    :cond_4
    :goto_1
    if-eqz v3, :cond_5

    .line 136
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->imageInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget-boolean v5, v5, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    invoke-virtual {v3, v5}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 138
    :cond_5
    return-object v3

    .line 120
    :cond_6
    if-eqz v2, :cond_4

    .line 122
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v5, v6, v7, v1, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 124
    if-eqz v3, :cond_8

    .line 125
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v5, v2, v8, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 131
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->getCacheKey(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 127
    :cond_8
    if-nez v0, :cond_7

    .line 128
    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException;-><init>()V

    throw v5
.end method

.method public releaseBitmap(Landroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "sampleSize"    # I

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->hasBitmap()Z

    move-result v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 145
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->getCacheKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->releaseBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setAttachmentId(Ljava/lang/String;)V
    .locals 1
    .param p1, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->attachmentId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 54
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->attachmentId:Ljava/lang/String;

    .line 55
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ImagePartView;->downloadAndInitBounds()V

    .line 56
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
