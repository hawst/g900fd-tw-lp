.class public final Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;


# instance fields
.field private backgroundColor_:Ljava/lang/String;

.field private bitField0_:I

.field private chromeStyle_:I

.field private landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

.field private portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

.field private suppressTextSizeControls_:Z

.field private templateType_:I

.field private template_:Ljava/lang/String;

.field private usesColumns_:Z

.field private zoomable_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9129
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9130
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 9141
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    .line 9160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    .line 9182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    .line 9202
    const-string v0, "#ffffff"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    .line 9224
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    .line 9281
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    .line 9300
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    .line 9130
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 3

    .prologue
    .line 9337
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9341
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v2, :cond_0

    .line 9342
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9344
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v2, :cond_1

    .line 9345
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9347
    :cond_1
    return-object v0

    .line 9338
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    :catch_0
    move-exception v1

    .line 9339
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9126
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9352
    if-ne p1, p0, :cond_1

    .line 9361
    :cond_0
    :goto_0
    return v1

    .line 9353
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 9354
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    .line 9355
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 9356
    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 9358
    :goto_2
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v3, :cond_3

    .line 9360
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v3, :cond_3

    .line 9361
    :goto_4
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 9355
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    .line 9356
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    .line 9358
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9360
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9361
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9204
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    return-object v0
.end method

.method public getLandscapeDisplay()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    .locals 1

    .prologue
    .line 9245
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    return-object v0
.end method

.method public getPortraitDisplay()Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;
    .locals 1

    .prologue
    .line 9264
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 9414
    const/4 v0, 0x0

    .line 9415
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9416
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    .line 9417
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9419
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9420
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    .line 9421
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9423
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9424
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    .line 9425
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9427
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 9428
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    .line 9429
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9431
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 9432
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    .line 9433
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9435
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v1, :cond_5

    .line 9436
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9437
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9439
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v1, :cond_6

    .line 9440
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9441
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9443
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 9444
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    .line 9445
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9447
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_8

    .line 9448
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    .line 9449
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9451
    :cond_8
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->cachedSize:I

    .line 9452
    return v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9162
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    return-object v0
.end method

.method public getUsesColumns()Z
    .locals 1

    .prologue
    .line 9184
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    return v0
.end method

.method public getZoomable()Z
    .locals 1

    .prologue
    .line 9226
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    return v0
.end method

.method public hasBackgroundColor()Z
    .locals 1

    .prologue
    .line 9215
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9367
    const/16 v0, 0x11

    .line 9368
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 9369
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    add-int v0, v1, v5

    .line 9370
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 9371
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    if-eqz v1, :cond_1

    move v1, v3

    :goto_1
    add-int v0, v5, v1

    .line 9372
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v5, v1

    .line 9373
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    if-eqz v1, :cond_3

    move v1, v3

    :goto_3
    add-int v0, v5, v1

    .line 9374
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v5, v1

    .line 9375
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v5, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 9376
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    add-int v0, v1, v2

    .line 9377
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    if-eqz v2, :cond_6

    :goto_6
    add-int v0, v1, v3

    .line 9378
    return v0

    .line 9370
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    move v1, v4

    .line 9371
    goto :goto_1

    .line 9372
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v1, v4

    .line 9373
    goto :goto_3

    .line 9374
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->hashCode()I

    move-result v1

    goto :goto_4

    .line 9375
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    move v3, v4

    .line 9377
    goto :goto_6
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9460
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 9461
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 9465
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 9466
    :sswitch_0
    return-object p0

    .line 9471
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 9472
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v4, :cond_1

    if-ne v1, v5, :cond_2

    .line 9475
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    .line 9476
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto :goto_0

    .line 9478
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    goto :goto_0

    .line 9483
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    .line 9484
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto :goto_0

    .line 9488
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    .line 9489
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto :goto_0

    .line 9493
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    .line 9494
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto :goto_0

    .line 9498
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    .line 9499
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto :goto_0

    .line 9503
    :sswitch_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v2, :cond_3

    .line 9504
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9506
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 9510
    :sswitch_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-nez v2, :cond_4

    .line 9511
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    .line 9513
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 9517
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 9518
    .restart local v1    # "temp":I
    if-eqz v1, :cond_5

    if-eq v1, v4, :cond_5

    if-ne v1, v5, :cond_6

    .line 9521
    :cond_5
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    .line 9522
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto/16 :goto_0

    .line 9524
    :cond_6
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    goto/16 :goto_0

    .line 9529
    .end local v1    # "temp":I
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    .line 9530
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    goto/16 :goto_0

    .line 9461
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9126
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9383
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9384
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->templateType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9386
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9387
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->template_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 9389
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9390
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->usesColumns_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9392
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 9393
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->backgroundColor_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 9395
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 9396
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->zoomable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9398
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v0, :cond_5

    .line 9399
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->landscapeDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 9401
    :cond_5
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    if-eqz v0, :cond_6

    .line 9402
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->portraitDisplay_:Lcom/google/apps/dots/proto/client/DotsShared$DisplayTarget;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 9404
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 9405
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->chromeStyle_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 9407
    :cond_7
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 9408
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;->suppressTextSizeControls_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 9410
    :cond_8
    return-void
.end method
