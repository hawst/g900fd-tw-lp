.class Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$20;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->setupInternalPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    .prologue
    .line 549
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$20;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 552
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment$20;->this$0:Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 553
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->clearDismissibleWelcomePreferences()Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    .line 554
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->clearDismissibleNewFeaturePreferences()V

    .line 555
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->reset_warm_welcome_cards_toast:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 556
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 557
    const/4 v1, 0x1

    return v1
.end method
