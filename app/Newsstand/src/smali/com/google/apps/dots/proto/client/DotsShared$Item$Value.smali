.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Value"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;,
        Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;


# instance fields
.field private altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

.field private audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

.field private dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

.field private html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

.field private image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

.field private location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

.field private nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

.field private number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

.field private pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

.field private product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

.field private streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

.field private text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

.field private url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

.field private video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10986
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10987
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 3

    .prologue
    .line 15322
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15326
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-eqz v2, :cond_0

    .line 15327
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    .line 15329
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-eqz v2, :cond_1

    .line 15330
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .line 15332
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-eqz v2, :cond_2

    .line 15333
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .line 15335
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_3

    .line 15336
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 15338
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-eqz v2, :cond_4

    .line 15339
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    .line 15341
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v2, :cond_5

    .line 15342
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 15344
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-eqz v2, :cond_6

    .line 15345
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .line 15347
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-eqz v2, :cond_7

    .line 15348
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    .line 15350
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v2, :cond_8

    .line 15351
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 15353
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v2, :cond_9

    .line 15354
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 15356
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-eqz v2, :cond_a

    .line 15357
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .line 15359
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v2, :cond_b

    .line 15360
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 15362
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-eqz v2, :cond_c

    .line 15363
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .line 15365
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-eqz v2, :cond_d

    .line 15366
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    .line 15368
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v2, :cond_e

    .line 15369
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 15371
    :cond_e
    return-object v0

    .line 15323
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    :catch_0
    move-exception v1

    .line 15324
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 10983
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15376
    if-ne p1, p0, :cond_1

    .line 15393
    :cond_0
    :goto_0
    return v1

    .line 15377
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 15378
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 15379
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-nez v3, :cond_3

    .line 15380
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-nez v3, :cond_3

    .line 15381
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 15382
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-nez v3, :cond_3

    .line 15383
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v3, :cond_3

    .line 15384
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-nez v3, :cond_3

    .line 15385
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-nez v3, :cond_3

    .line 15386
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v3, :cond_3

    .line 15387
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v3, :cond_3

    .line 15388
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-nez v3, :cond_3

    .line 15389
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v3, :cond_3

    .line 15390
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-nez v3, :cond_3

    .line 15391
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-nez v3, :cond_3

    .line 15392
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 15393
    goto/16 :goto_0

    .line 15379
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .line 15380
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .line 15381
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 15382
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    .line 15383
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 15384
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .line 15385
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    .line 15386
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 15387
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 15388
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .line 15389
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 15390
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .line 15391
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    .line 15392
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 15393
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAltFormat()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;
    .locals 1

    .prologue
    .line 15225
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    return-object v0
.end method

.method public getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;
    .locals 1

    .prologue
    .line 15016
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    return-object v0
.end method

.method public getDateTime()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;
    .locals 1

    .prologue
    .line 15035
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    return-object v0
.end method

.method public getHtml()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    .locals 1

    .prologue
    .line 15054
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    return-object v0
.end method

.method public getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 15073
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getInlineFrame()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;
    .locals 1

    .prologue
    .line 15263
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    return-object v0
.end method

.method public getLocation()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;
    .locals 1

    .prologue
    .line 15111
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    return-object v0
.end method

.method public getNativeBodies()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;
    .locals 1

    .prologue
    .line 15282
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    return-object v0
.end method

.method public getNumber()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;
    .locals 1

    .prologue
    .line 15130
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    return-object v0
.end method

.method public getPdf()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;
    .locals 1

    .prologue
    .line 15092
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    return-object v0
.end method

.method public getProduct()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;
    .locals 1

    .prologue
    .line 15206
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 15468
    const/4 v0, 0x0

    .line 15469
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-eqz v1, :cond_0

    .line 15470
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    .line 15471
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15473
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-eqz v1, :cond_1

    .line 15474
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .line 15475
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15477
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-eqz v1, :cond_2

    .line 15478
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .line 15479
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15481
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_3

    .line 15482
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 15483
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15485
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v1, :cond_4

    .line 15486
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 15487
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15489
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-eqz v1, :cond_5

    .line 15490
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .line 15491
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15493
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-eqz v1, :cond_6

    .line 15494
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    .line 15495
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15497
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v1, :cond_7

    .line 15498
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 15499
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15501
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v1, :cond_8

    .line 15502
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 15503
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15505
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-eqz v1, :cond_9

    .line 15506
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .line 15507
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15509
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v1, :cond_a

    .line 15510
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 15511
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15513
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-eqz v1, :cond_b

    .line 15514
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .line 15515
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15517
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-eqz v1, :cond_c

    .line 15518
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    .line 15519
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15521
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v1, :cond_d

    .line 15522
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 15523
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15525
    :cond_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-eqz v1, :cond_e

    .line 15526
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    .line 15527
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15529
    :cond_e
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->cachedSize:I

    .line 15530
    return v0
.end method

.method public getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;
    .locals 1

    .prologue
    .line 15244
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    return-object v0
.end method

.method public getText()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;
    .locals 1

    .prologue
    .line 15149
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    return-object v0
.end method

.method public getUrl()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;
    .locals 1

    .prologue
    .line 15168
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    return-object v0
.end method

.method public getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    .locals 1

    .prologue
    .line 15187
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    return-object v0
.end method

.method public hasAudio()Z
    .locals 1

    .prologue
    .line 15026
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDateTime()Z
    .locals 1

    .prologue
    .line 15045
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHtml()Z
    .locals 1

    .prologue
    .line 15064
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasImage()Z
    .locals 1

    .prologue
    .line 15083
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInlineFrame()Z
    .locals 1

    .prologue
    .line 15273
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocation()Z
    .locals 1

    .prologue
    .line 15121
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeBodies()Z
    .locals 1

    .prologue
    .line 15292
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumber()Z
    .locals 1

    .prologue
    .line 15140
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPdf()Z
    .locals 1

    .prologue
    .line 15102
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProduct()Z
    .locals 1

    .prologue
    .line 15216
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStreamingVideo()Z
    .locals 1

    .prologue
    .line 15254
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 15159
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUrl()Z
    .locals 1

    .prologue
    .line 15178
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideo()Z
    .locals 1

    .prologue
    .line 15197
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 15397
    const/16 v0, 0x11

    .line 15398
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 15399
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 15400
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 15401
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 15402
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 15403
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 15404
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 15405
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v3, v1

    .line 15406
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    add-int v0, v3, v1

    .line 15407
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    add-int v0, v3, v1

    .line 15408
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    add-int v0, v3, v1

    .line 15409
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-nez v1, :cond_a

    move v1, v2

    :goto_a
    add-int v0, v3, v1

    .line 15410
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v1, :cond_b

    move v1, v2

    :goto_b
    add-int v0, v3, v1

    .line 15411
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-nez v1, :cond_c

    move v1, v2

    :goto_c
    add-int v0, v3, v1

    .line 15412
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-nez v1, :cond_d

    move v1, v2

    :goto_d
    add-int v0, v3, v1

    .line 15413
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-nez v3, :cond_e

    :goto_e
    add-int v0, v1, v2

    .line 15414
    return v0

    .line 15399
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->hashCode()I

    move-result v1

    goto/16 :goto_0

    .line 15400
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->hashCode()I

    move-result v1

    goto/16 :goto_1

    .line 15401
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->hashCode()I

    move-result v1

    goto/16 :goto_2

    .line 15402
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v1

    goto/16 :goto_3

    .line 15403
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;->hashCode()I

    move-result v1

    goto/16 :goto_4

    .line 15404
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->hashCode()I

    move-result v1

    goto/16 :goto_5

    .line 15405
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->hashCode()I

    move-result v1

    goto :goto_6

    .line 15406
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;->hashCode()I

    move-result v1

    goto :goto_7

    .line 15407
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->hashCode()I

    move-result v1

    goto :goto_8

    .line 15408
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->hashCode()I

    move-result v1

    goto :goto_9

    .line 15409
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->hashCode()I

    move-result v1

    goto :goto_a

    .line 15410
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;->hashCode()I

    move-result v1

    goto :goto_b

    .line 15411
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->hashCode()I

    move-result v1

    goto :goto_c

    .line 15412
    :cond_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;->hashCode()I

    move-result v1

    goto :goto_d

    .line 15413
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;->hashCode()I

    move-result v2

    goto :goto_e
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15538
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 15539
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 15543
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 15544
    :sswitch_0
    return-object p0

    .line 15549
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-nez v1, :cond_1

    .line 15550
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    .line 15552
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15556
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-nez v1, :cond_2

    .line 15557
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .line 15559
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15563
    :sswitch_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-nez v1, :cond_3

    .line 15564
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .line 15566
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15570
    :sswitch_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v1, :cond_4

    .line 15571
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 15573
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15577
    :sswitch_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-nez v1, :cond_5

    .line 15578
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .line 15580
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15584
    :sswitch_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-nez v1, :cond_6

    .line 15585
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .line 15587
    :cond_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 15591
    :sswitch_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-nez v1, :cond_7

    .line 15592
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    .line 15594
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15598
    :sswitch_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-nez v1, :cond_8

    .line 15599
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .line 15601
    :cond_8
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15605
    :sswitch_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v1, :cond_9

    .line 15606
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 15608
    :cond_9
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15612
    :sswitch_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-nez v1, :cond_a

    .line 15613
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .line 15615
    :cond_a
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15619
    :sswitch_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-nez v1, :cond_b

    .line 15620
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    .line 15622
    :cond_b
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15626
    :sswitch_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-nez v1, :cond_c

    .line 15627
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .line 15629
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15633
    :sswitch_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-nez v1, :cond_d

    .line 15634
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    .line 15636
    :cond_d
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15640
    :sswitch_e
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-nez v1, :cond_e

    .line 15641
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    .line 15643
    :cond_e
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15647
    :sswitch_f
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-nez v1, :cond_f

    .line 15648
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    .line 15650
    :cond_f
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 15539
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10983
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    move-result-object v0

    return-object v0
.end method

.method public parseFrom([B)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .locals 1
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 15659
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;-><init>()V

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15419
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    if-eqz v0, :cond_0

    .line 15420
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->audio_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15422
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    if-eqz v0, :cond_1

    .line 15423
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->dateTime_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15425
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-eqz v0, :cond_2

    .line 15426
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->html_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15428
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_3

    .line 15429
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->image_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15431
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    if-eqz v0, :cond_4

    .line 15432
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->location_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15434
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    if-eqz v0, :cond_5

    .line 15435
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->number_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15437
    :cond_5
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    if-eqz v0, :cond_6

    .line 15438
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->text_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15440
    :cond_6
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    if-eqz v0, :cond_7

    .line 15441
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->url_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15443
    :cond_7
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-eqz v0, :cond_8

    .line 15444
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->video_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15446
    :cond_8
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    if-eqz v0, :cond_9

    .line 15447
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->product_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15449
    :cond_9
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    if-eqz v0, :cond_a

    .line 15450
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->altFormat_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15452
    :cond_a
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    if-eqz v0, :cond_b

    .line 15453
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->streamingVideo_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15455
    :cond_b
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    if-eqz v0, :cond_c

    .line 15456
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->inlineFrame_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$InlineFrame;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15458
    :cond_c
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    if-eqz v0, :cond_d

    .line 15459
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->nativeBodies_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$NativeBodies;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15461
    :cond_d
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    if-eqz v0, :cond_e

    .line 15462
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->pdf_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Pdf;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 15464
    :cond_e
    return-void
.end method
