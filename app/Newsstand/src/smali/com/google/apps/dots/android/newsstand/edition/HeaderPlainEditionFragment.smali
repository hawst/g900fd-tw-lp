.class public Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;
.source "HeaderPlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final UNSUBSCRIBED_HEADER_FILTER:Landroid/graphics/ColorFilter;


# instance fields
.field private activeSubscribeAnimator:Landroid/animation/Animator;

.field private backgroundView:Landroid/view/View;

.field private cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

.field private coloredOverlay:Landroid/view/View;

.field private editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

.field private heroView:Landroid/view/View;

.field private plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

.field private subscribeAnimatorForMenuItem:Landroid/animation/Animator;

.field private subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const-class v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 65
    const/4 v0, -0x1

    .line 67
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$integer;->unsubscribed_topic_black_percentage:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    const/4 v2, 0x0

    .line 66
    invoke-static {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilterPercentBlack(IIZ)Landroid/graphics/ColorFilter;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->UNSUBSCRIBED_HEADER_FILTER:Landroid/graphics/ColorFilter;

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    const-string v1, "HeaderPlainEditionFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->startSubscribeAnimation()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;
    .param p1, "x1"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->isSubscribed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotatorColor()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->backgroundView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->onPulledToRefresh()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;
    .param p1, "x1"    # Landroid/view/LayoutInflater;
    .param p2, "x2"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;
    .param p1, "x1"    # Landroid/view/LayoutInflater;
    .param p2, "x2"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    return-void
.end method

.method private addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 248
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->image_rotator_background:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->backgroundView:Landroid/view/View;

    .line 249
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->backgroundView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->header_image_rotator:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    .line 250
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->backgroundView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->colored_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;

    .line 251
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->backgroundView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 252
    return-void
.end method

.method private addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 243
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_hero:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;

    .line 244
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 245
    return-void
.end method

.method private edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method

.method private isSubscribed()Z
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPulledToRefresh()V
    .locals 5

    .prologue
    .line 383
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$8;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 292
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 293
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 292
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 294
    return-void
.end method

.method private setupImageRotator(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 2
    .param p1, "imageRotatorList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setImageIdList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 298
    if-eqz p1, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    .line 300
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setVisibility(I)V

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 303
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupSubscribeMenuHelper()V
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getHasOptionsMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$1;

    invoke-direct {v0, p0, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    .line 137
    :cond_0
    return-void
.end method

.method private startSubscribeAnimation()V
    .locals 2

    .prologue
    .line 341
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeAnimatorForMenuItem:Landroid/animation/Animator;

    if-nez v0, :cond_1

    .line 342
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotatorColor()V

    .line 380
    :goto_0
    return-void

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->activeSubscribeAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->activeSubscribeAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeAnimatorForMenuItem:Landroid/animation/Animator;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->activeSubscribeAnimator:Landroid/animation/Animator;

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeAnimatorForMenuItem:Landroid/animation/Animator;

    .line 351
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->activeSubscribeAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 379
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->activeSubscribeAnimator:Landroid/animation/Animator;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    goto :goto_0
.end method

.method private updateActionBarTitle()V
    .locals 4

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v1

    .line 167
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTitleHint()Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->updateContentDescriptionForActivityContentView(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 174
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 189
    return-void
.end method

.method private updateEditionSummary()V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 159
    return-void
.end method

.method private updateHeroView()V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->editionSummaryFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 225
    return-void
.end method

.method private updateImageRotator()V
    .locals 3

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->lifetimeScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private updateImageRotatorColor()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 401
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->isSubscribed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    .line 404
    .local v0, "color":I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$integer;->subscribed_topic_black_percentage:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 403
    invoke-static {v0, v2, v4}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getColorFilterPercentBlack(IIZ)Landroid/graphics/ColorFilter;

    move-result-object v1

    .line 406
    .local v1, "colorFilter":Landroid/graphics/ColorFilter;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v2, v1}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 410
    .end local v0    # "color":I
    .end local v1    # "colorFilter":Landroid/graphics/ColorFilter;
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->UNSUBSCRIBED_HEADER_FILTER:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method private updatePullToRefresh()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V

    .line 240
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 414
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment$9;-><init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeMenuHelper:Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SubscribeMenuHelper;->onDestroyView()V

    .line 105
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onDestroyView()V

    .line 106
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 327
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    .line 328
    .local v2, "ret":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 329
    .local v1, "id":I
    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_add_edition:I

    if-eq v1, v3, :cond_0

    sget v3, Lcom/google/android/apps/newsstanddev/R$id;->menu_remove_edition:I

    if-ne v1, v3, :cond_1

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 331
    .local v0, "addEditionMenuItemView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->coloredOverlay:Landroid/view/View;

    .line 333
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->isSubscribed()Z

    move-result v5

    .line 332
    invoke-static {v3, v4, v0, v5}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->getCenteredCircularReveal(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/animation/Animator;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->subscribeAnimatorForMenuItem:Landroid/animation/Animator;

    .line 337
    .end local v0    # "addEditionMenuItemView":Landroid/view/View;
    :cond_1
    return v2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onPause()V

    .line 111
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->stopRefresh()V

    .line 112
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->onResume()V

    .line 117
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerImageRotator:Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ImageRotatorView;->startRefresh()V

    .line 118
    return-void
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 3
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 88
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 89
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->setupHeaderListLayout()V

    .line 91
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 92
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->plain_edition_fragment:I

    .line 93
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .line 94
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->play_header_listview:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->cardListView:Lcom/google/apps/dots/android/newsstand/card/NSCardListView;

    .line 96
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->image_header_logo:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroImageLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 97
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->round_topic_header_logo:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->heroRoundTopicLogo:Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;

    .line 99
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->setupSubscribeMenuHelper()V

    .line 100
    return-void
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;)V
    .locals 3
    .param p1, "newState"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;

    .prologue
    const/4 v0, 0x0

    .line 143
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->plainEditionFragment:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    invoke-virtual {v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 145
    if-eqz p2, :cond_0

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v2, p2, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 146
    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 147
    .local v0, "editionChanged":Z
    :cond_1
    if-eqz v0, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateEditionSummary()V

    .line 149
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateActionBarTitle()V

    .line 150
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateHeroView()V

    .line 151
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotator()V

    .line 152
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updateImageRotatorColor()V

    .line 153
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/edition/HeaderPlainEditionFragment;->updatePullToRefresh()V

    .line 155
    :cond_2
    return-void
.end method
