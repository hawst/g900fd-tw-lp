.class Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$7;
.super Ljava/lang/Object;
.source "Janitor.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$RegionVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->compactStoreFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$7;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;)V
    .locals 4
    .param p1, "region"    # Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$7;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->getStats()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->shouldCompact(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$1300(Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;Lcom/google/apps/dots/android/newsstand/diskcache/StoreFileStats;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    # getter for: Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;->access$200()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Compacting %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$Region;->storeFile()Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->compact()V

    .line 338
    :cond_0
    return-void
.end method
