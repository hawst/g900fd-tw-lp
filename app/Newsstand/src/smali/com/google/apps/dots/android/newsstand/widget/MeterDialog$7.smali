.class Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;
.super Lcom/google/apps/dots/android/newsstand/async/QueueTask;
.source "MeterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->updateMeteredCount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field final synthetic val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;Lcom/google/apps/dots/android/newsstand/async/Queue;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;
    .param p2, "queue"    # Lcom/google/apps/dots/android/newsstand/async/Queue;

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/QueueTask;-><init>(Lcom/google/apps/dots/android/newsstand/async/Queue;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()V
    .locals 4

    .prologue
    .line 335
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$000(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->readStateCollection(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;

    move-result-object v0

    .line 336
    .local v0, "readCollection":Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->postId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$900(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->isRead(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    .line 356
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->this$0:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 347
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredPolicy:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$600(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;)Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v2

    iget v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->getMeteredReadCount()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 346
    # setter for: Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->meteredArticlesRemaining:I
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->access$1002(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;I)I

    .line 348
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;->val$token:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MeterDialog$7;)V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
