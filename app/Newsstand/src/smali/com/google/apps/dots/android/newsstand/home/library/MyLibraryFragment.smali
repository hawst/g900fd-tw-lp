.class public Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

.field private libraryPageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private onDeviceHelper:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

.field private pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

.field private pager:Landroid/support/v4/view/NSViewPager;

.field protected pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

.field private tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;-><init>()V

    const-string v1, "MyLibraryFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->my_library_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 56
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->libraryPageList()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Landroid/support/v4/view/NSViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/view/NSViewPager;)Landroid/support/v4/view/NSViewPager;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;
    .param p1, "x1"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupPager()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->currentEditionFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->isChangingState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->isChangingState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->updatePage()V

    return-void
.end method

.method private currentEditionFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 4

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 319
    .local v1, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 320
    .local v0, "childFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 322
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .line 326
    .end local v0    # "childFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCurrentPageIndex()I
    .locals 7

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPage()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    move-result-object v0

    .line 309
    .local v0, "libraryPage":Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 310
    .local v1, "logicalPosition":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 311
    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Unable to find page for library page: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    const/4 v1, 0x0

    move v2, v1

    .line 314
    .end local v1    # "logicalPosition":I
    .local v2, "logicalPosition":I
    :goto_0
    return v2

    .end local v2    # "logicalPosition":I
    .restart local v1    # "logicalPosition":I
    :cond_0
    move v2, v1

    .end local v1    # "logicalPosition":I
    .restart local v2    # "logicalPosition":I
    goto :goto_0
.end method

.method private libraryPage()Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 299
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->DEFAULT_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .line 301
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    goto :goto_0
.end method

.method private sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    .prologue
    .line 330
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen;->track(Z)V

    .line 331
    return-void
.end method

.method private setupHeaderListLayout()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 128
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 129
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V

    .line 167
    return-void
.end method

.method private setupPager()V
    .locals 3

    .prologue
    .line 171
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$3;

    .line 172
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$3;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/app/FragmentManager;Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    sget v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPageList;->DK_LIBRARY_TITLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setTitleKey(Ljava/lang/Integer;)V

    .line 183
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-direct {v0, p0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$4;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;Landroid/support/v4/view/NSViewPager;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    .line 203
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pageChangeListener:Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 205
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$5;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->tabListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V

    .line 224
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$6;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$6;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 240
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 241
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 243
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/NSViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 244
    return-void
.end method

.method private updatePage()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 275
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->getCurrentPageIndex()I

    move-result v0

    .line 277
    .local v0, "pageIndex":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->getCurrentLogicalItem()I

    move-result v2

    sub-int v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 282
    .local v1, "smoothScroll":Z
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/view/NSViewPager;->setCurrentLogicalItem(IZ)V

    goto :goto_0

    .line 281
    .end local v1    # "smoothScroll":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private updatePager()V
    .locals 6

    .prologue
    .line 267
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 268
    .local v0, "position":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 269
    sget-object v2, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Unable to find position for homePage: %s"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/NSViewPager;->setCurrentLogicalItem(I)V

    .line 272
    return-void
.end method


# virtual methods
.method protected createTransitionDelegate()Lcom/google/android/play/transition/delegate/TransitionDelegate;
    .locals 1

    .prologue
    .line 340
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$7;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment$7;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;)V

    return-object v0
.end method

.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->currentEditionFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->onDeviceHelper:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->onDeviceHelper:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->dispose()V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->onDeviceHelper:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->libraryPageListObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 253
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->destroy()V

    .line 254
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->onDestroyView()V

    .line 255
    return-void
.end method

.method public onViewCreated(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 77
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->header_list_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 78
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->setupHeaderListLayout()V

    .line 79
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->create(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->onDeviceHelper:Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;

    .line 80
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->setupActionBar(Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;)V

    .line 81
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 289
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    .line 290
    .end local p2    # "state":Landroid/os/Parcelable;
    invoke-virtual {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setLibraryFragmentState(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    .line 291
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 288
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    .restart local p2    # "state":Landroid/os/Parcelable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)V
    .locals 3
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;

    .prologue
    .line 259
    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;->libraryPage:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 260
    .local v0, "pageChanged":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 261
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->updatePager()V

    .line 262
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/library/MyLibraryFragment;->sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/library/LibraryFragmentState;)V

    .line 264
    :cond_1
    return-void

    .line 259
    .end local v0    # "pageChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
