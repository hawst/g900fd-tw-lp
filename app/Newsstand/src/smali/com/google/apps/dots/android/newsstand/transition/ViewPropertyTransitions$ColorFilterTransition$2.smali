.class Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;
.super Ljava/lang/Object;
.source "ViewPropertyTransitions.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;->createAnimation(Landroid/view/ViewGroup;Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field evaluator:Landroid/animation/ArgbEvaluator;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;

.field final synthetic val$endValue:Ljava/lang/Integer;

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$startValue:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->this$0:Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$startValue:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$endValue:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$imageView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->evaluator:Landroid/animation/ArgbEvaluator;

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->evaluator:Landroid/animation/ArgbEvaluator;

    .line 123
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$startValue:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$endValue:Ljava/lang/Integer;

    .line 122
    invoke-virtual {v1, v2, v3, v4}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 126
    .local v0, "color":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 127
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/transition/ViewPropertyTransitions$ColorFilterTransition$2;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 130
    :cond_0
    return-void
.end method
