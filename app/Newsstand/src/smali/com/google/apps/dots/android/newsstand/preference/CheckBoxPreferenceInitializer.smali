.class public abstract Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;
.super Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;
.source "CheckBoxPreferenceInitializer.java"


# instance fields
.field private final checkBoxPreference:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>(Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 0
    .param p1, "checkBoxPreference"    # Landroid/preference/CheckBoxPreference;
    .param p2, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;-><init>(Ljava/lang/String;)V

    .line 15
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;->checkBoxPreference:Landroid/preference/CheckBoxPreference;

    .line 16
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;->initialize()V

    .line 17
    return-void
.end method


# virtual methods
.method protected abstract getCurrentValue()Z
.end method

.method protected initialize()V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;->checkBoxPreference:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 29
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;->initialize()V

    .line 30
    return-void
.end method

.method protected abstract setNewValue(Z)V
.end method

.method protected update()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;->checkBoxPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/CheckBoxPreferenceInitializer;->getCurrentValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 35
    return-void
.end method
