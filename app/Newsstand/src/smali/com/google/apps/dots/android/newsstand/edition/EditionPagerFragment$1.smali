.class Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "EditionPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 146
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 147
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 138
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->edition_pager_content:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 140
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->section_pager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/NSViewPager;

    iput-object v1, v2, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->pager:Landroid/support/v4/view/NSViewPager;

    .line 141
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->setupPager()V
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;)V

    .line 142
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 133
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 134
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 161
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 166
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->showTabs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 171
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment;->alwaysShowActionBarText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 156
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->section_pager:I

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 151
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;, "Lcom/google/apps/dots/android/newsstand/edition/EditionPagerFragment$1;"
    const/4 v0, 0x1

    return v0
.end method
