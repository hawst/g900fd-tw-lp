.class Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;
.super Ljava/lang/Object;
.source "NormalArticleWidget.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadIfPossible()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x0

    .line 351
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Failed to load article: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->loadStateHelper:Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;->LOAD_FAILED:Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;

    invoke-virtual {v0, v1, v4, p1}, Lcom/google/apps/dots/android/newsstand/widget/LoadStateHelper;->setLoadState(Lcom/google/apps/dots/android/newsstand/widget/DelayedContentWidget$LoadState;ZLjava/lang/Throwable;)V

    .line 355
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 335
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    iput-object v0, v1, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->userRoles:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 339
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget$2;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;

    const/4 v1, 0x1

    .line 340
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Application;

    const/4 v2, 0x2

    .line 341
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    const/4 v3, 0x3

    .line 342
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    const/4 v4, 0x4

    .line 343
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    const/4 v5, 0x5

    .line 344
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;

    const/4 v6, 0x6

    .line 345
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    const/4 v7, 0x7

    .line 346
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 339
    invoke-virtual/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->onArticleAvailable(Lcom/google/apps/dots/proto/client/DotsShared$Application;Lcom/google/apps/dots/proto/client/DotsShared$Section;Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;Ljava/util/Map;Z)V

    .line 347
    return-void
.end method
