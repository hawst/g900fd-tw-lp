.class Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "AudioPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->updatePublisherInfo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    .prologue
    .line 718
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 7
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 721
    if-nez p1, :cond_1

    .line 722
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    sget v6, Lcom/google/android/apps/newsstanddev/R$mipmap;->ic_launcher_play_newsstand:I

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$2002(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->publisher:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$2102(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;Ljava/lang/String;)Ljava/lang/String;

    .line 726
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->iconSource()Lcom/google/apps/dots/android/newsstand/icon/IconSource;

    move-result-object v2

    .line 728
    .local v2, "iconSource":Lcom/google/apps/dots/android/newsstand/icon/IconSource;, "Lcom/google/apps/dots/android/newsstand/icon/IconSource<+Landroid/view/View;>;"
    instance-of v5, v2, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    if-eqz v5, :cond_2

    move-object v1, v2

    .line 729
    check-cast v1, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .line 733
    .local v1, "iconId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    :goto_1
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->isAttachmentId()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 734
    new-instance v5, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # getter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->notificationIconSizePx:I
    invoke-static {v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$2200(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->square(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v4

    .line 735
    .local v4, "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    .line 736
    .local v3, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->attachmentStore()Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    move-result-object v5

    iget-object v6, v1, Lcom/google/apps/dots/android/newsstand/icon/IconId;->attachmentId:Ljava/lang/String;

    .line 737
    invoke-virtual {v5, v3, v6, v4}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getBitmapAttachment(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 738
    .local v0, "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    new-instance v5, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;)V

    invoke-virtual {v3, v0, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 731
    .end local v0    # "bitmapFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Landroid/graphics/Bitmap;>;"
    .end local v1    # "iconId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    .end local v3    # "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .end local v4    # "transform":Lcom/google/apps/dots/android/newsstand/server/Transform;
    :cond_2
    sget-object v1, Lcom/google/apps/dots/android/newsstand/icon/IconId;->NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .restart local v1    # "iconId":Lcom/google/apps/dots/android/newsstand/icon/IconId;
    goto :goto_1

    .line 747
    :cond_3
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/icon/IconId;->isResourceId()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 748
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    iget v6, v1, Lcom/google/apps/dots/android/newsstand/icon/IconId;->resourceId:I

    # setter for: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->faviconDrawable:I
    invoke-static {v5, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$2002(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;I)I

    .line 749
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->this$0:Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;

    # invokes: Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->postUpdateNotification()V
    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;->access$1800(Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 718
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioPlayerService$9;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
