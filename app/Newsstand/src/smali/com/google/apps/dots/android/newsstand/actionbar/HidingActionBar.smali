.class public Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
.super Ljava/lang/Object;
.source "HidingActionBar.java"

# interfaces
.implements Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;,
        Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;,
        Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    }
.end annotation


# instance fields
.field private a11yStateListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

.field private accumulatedDelta:I

.field private actionBar:Landroid/support/v7/app/ActionBar;

.field private activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

.field private final articleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

.field private eventHandler:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;

.field private final hideActionBarRunnable:Ljava/lang/Runnable;

.field private final hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private mode:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

.field private scrollHideStartTime:J

.field private showing:Z

.field private toolbarContainer:Landroid/view/View;

.field private toolbarHeight:I

.field private toolbarIsHideAnimating:Z

.field private toolbarShadow:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->showing:Z

    .line 70
    sget-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->mode:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    .line 73
    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$1;-><init>(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideActionBarRunnable:Ljava/lang/Runnable;

    .line 81
    new-instance v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;-><init>(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->articleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarIsHideAnimating:Z

    return p1
.end method

.method private cancelAutoHide()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 272
    return-void
.end method

.method private hideToolbar(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    .line 236
    if-eqz p1, :cond_1

    .line 241
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarIsHideAnimating:Z

    if-nez v0, :cond_0

    .line 242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarIsHideAnimating:Z

    .line 243
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 244
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 245
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$3;-><init>(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)V

    .line 246
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 256
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarIsHideAnimating:Z

    goto :goto_0
.end method

.method private scheduleAutoHide()V
    .locals 4

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->cancelAutoHide()V

    .line 267
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideActionBarRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 268
    return-void
.end method

.method private showToolbar(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 220
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarIsHideAnimating:Z

    .line 221
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 222
    if-eqz p1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 224
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 225
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 229
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public attach(Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;)V
    .locals 5
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .prologue
    const/4 v4, 0x0

    .line 94
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 95
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [I

    sget v3, Lcom/google/android/apps/newsstanddev/R$attr;->actionBarSize:I

    aput v3, v2, v4

    invoke-virtual {p1, v1, v2}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 96
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarHeight:I

    .line 97
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 98
    new-instance v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$2;-><init>(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->a11yStateListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 106
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->a11yStateListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-static {p1, v1}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->registerForA11yChanges(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    .line 107
    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->actionBar:Landroid/support/v7/app/ActionBar;

    .line 108
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v1, p0}, Landroid/support/v7/app/ActionBar;->addOnMenuVisibilityListener(Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;)V

    .line 109
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->start()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 110
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    .line 113
    :cond_0
    return-void
.end method

.method public detach()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->a11yStateListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->unregisterForA11yChanges(Landroid/content/Context;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    .line 125
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->actionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, p0}, Landroid/support/v7/app/ActionBar;->removeOnMenuVisibilityListener(Landroid/support/v7/app/ActionBar$OnMenuVisibilityListener;)V

    .line 127
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->actionBar:Landroid/support/v7/app/ActionBar;

    .line 128
    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    .line 129
    return-void
.end method

.method public getArticleEventDelegate()Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->articleEventDelegate:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$ArticleEventDelegate;

    return-object v0
.end method

.method public hide(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    const/4 v1, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->activity:Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->isTouchExplorationEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->showing:Z

    .line 210
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hideToolbar(Z)V

    .line 211
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->eventHandler:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->eventHandler:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;->onActionBarVisibilityChange(Z)V

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->showing:Z

    return v0
.end method

.method public onClick()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->cancelAutoHide()V

    .line 156
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hide(Z)V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->show(Z)V

    .line 159
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    goto :goto_0
.end method

.method public onMenuVisibilityChanged(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->cancelAutoHide()V

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    goto :goto_0
.end method

.method public onOverscroll(Z)V
    .locals 2
    .param p1, "top"    # Z

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->mode:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->MODE_HIDE_ON_SCROLL:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->show(Z)V

    .line 190
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    .line 192
    :cond_0
    return-void
.end method

.method public onScroll(II)Z
    .locals 8
    .param p1, "scrollDelta"    # I
    .param p2, "scrollOffset"    # I

    .prologue
    const/4 v4, 0x1

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 166
    .local v2, "now":J
    iget-wide v6, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scrollHideStartTime:J

    sub-long v0, v2, v6

    .line 167
    .local v0, "elapsed":J
    const-wide/16 v6, 0x3e8

    cmp-long v5, v0, v6

    if-lez v5, :cond_1

    .line 168
    iput-wide v2, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scrollHideStartTime:J

    .line 169
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->accumulatedDelta:I

    .line 173
    :goto_0
    if-nez p2, :cond_2

    .line 175
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->show(Z)V

    .line 176
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    .line 184
    :cond_0
    :goto_1
    const/4 v4, 0x0

    :goto_2
    return v4

    .line 171
    :cond_1
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->accumulatedDelta:I

    add-int/2addr v5, p1

    iput v5, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->accumulatedDelta:I

    goto :goto_0

    .line 177
    :cond_2
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->accumulatedDelta:I

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarHeight:I

    div-int/lit8 v6, v6, 0x14

    if-le v5, v6, :cond_3

    .line 178
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->hide(Z)V

    goto :goto_1

    .line 179
    :cond_3
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->accumulatedDelta:I

    iget v6, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarHeight:I

    neg-int v6, v6

    if-ge v5, v6, :cond_0

    .line 180
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->show(Z)V

    .line 181
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    goto :goto_2
.end method

.method public setMode(Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;)V
    .locals 2
    .param p1, "mode"    # Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->mode:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    if-eq v0, p1, :cond_0

    .line 133
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->mode:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;

    .line 134
    sget-object v0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$4;->$SwitchMap$com$google$apps$dots$android$newsstand$actionbar$HidingActionBar$Mode:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 136
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    goto :goto_0

    .line 141
    :pswitch_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->cancelAutoHide()V

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->show(Z)V

    .line 143
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->scheduleAutoHide()V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setToolbarContainer(Landroid/view/View;)V
    .locals 2
    .param p1, "toolbarContainer"    # Landroid/view/View;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarContainer:Landroid/view/View;

    .line 117
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->toolbar_shadow:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarShadow:Landroid/view/View;

    .line 118
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->toolbarShadow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 121
    :cond_0
    return-void
.end method

.method public show(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    const/4 v1, 0x1

    .line 195
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->showing:Z

    .line 197
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->showToolbar(Z)V

    .line 198
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->eventHandler:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar;->eventHandler:Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;

    invoke-interface {v0, v1}, Lcom/google/apps/dots/android/newsstand/actionbar/HidingActionBar$EventHandler;->onActionBarVisibilityChange(Z)V

    .line 202
    :cond_0
    return-void
.end method
