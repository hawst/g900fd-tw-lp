.class public Lcom/google/apps/dots/android/newsstand/widget/MediaView;
.super Landroid/view/ViewGroup;
.source "MediaView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Landroid/widget/MediaController$MediaPlayerControl;
.implements Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MEDIA_PLAYER_INIT_RETRY_COUNT:I = 0x3

.field private static final MEDIA_PLAYER_INIT_RETRY_INTERVAL:I = 0xfa


# instance fields
.field private final asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private display:Landroid/view/View;

.field private file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private mediaController:Landroid/widget/MediaController;

.field private mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

.field private final options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

.field private progress:Landroid/widget/ProgressBar;

.field private shouldAutoStart:Z

.field private surfaceAvailable:Z

.field private surfaceView:Landroid/view/SurfaceView;

.field private textureView:Landroid/view/TextureView;

.field private uri:Landroid/net/Uri;

.field private videoHeight:I

.field private videoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 98
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p3, "options"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 82
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    .line 83
    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    .line 103
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    .line 104
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 106
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaController()V

    .line 108
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldUseTextureView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initTextureView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->addView(Landroid/view/View;)V

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initProgress()V

    .line 116
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initGestureDetector()V

    .line 117
    return-void

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initSurfaceView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;)Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->unbindSurface()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Landroid/widget/MediaController;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayerDataSource()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/MediaView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;
    .param p1, "x1"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayer(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->bindSurface()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    return v0
.end method

.method static synthetic access$900()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method private bindSurface()V
    .locals 3

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z

    if-eqz v0, :cond_0

    .line 554
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldUseTextureView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    new-instance v1, Landroid/view/Surface;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method private getState()I
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->getState()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initGestureDetector()V
    .locals 3

    .prologue
    .line 612
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->gestureDetector:Landroid/view/GestureDetector;

    .line 645
    return-void
.end method

.method private initMediaController()V
    .locals 3

    .prologue
    .line 186
    new-instance v1, Landroid/widget/MediaController;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;

    .line 188
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 189
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    .line 195
    return-void

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, p0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private initMediaPlayer(I)V
    .locals 6
    .param p1, "retryCount"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-nez v0, :cond_0

    .line 207
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Initializing player for MediaView/%d, auto-start=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getManagedMediaPlayer(Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer$OnReleaseListener;)Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 210
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-nez v0, :cond_1

    .line 211
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Could not retrieve MediaPlayer. Retrying %d more times"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    if-lez p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView$2;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;I)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 223
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 224
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 225
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    iget-boolean v1, v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->shouldLoop:Z

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setLooping(Z)V

    .line 229
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->bindSurface()V

    .line 231
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isDataSourceSpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayerDataSource()V

    goto :goto_0
.end method

.method private initMediaPlayerDataSource()V
    .locals 4

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v1, :cond_0

    .line 250
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    if-eqz v1, :cond_1

    .line 252
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    new-instance v3, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->submit(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 279
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 284
    :catch_0
    move-exception v0

    .line 285
    .local v0, "tr":Ljava/lang/Throwable;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 281
    .end local v0    # "tr":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "initDataSource called prematurely"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private initProgress()V
    .locals 2

    .prologue
    .line 175
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    .line 176
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->addView(Landroid/view/View;)V

    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 178
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->showProgress(Z)V

    .line 179
    return-void
.end method

.method private initSurfaceView()Landroid/view/View;
    .locals 2

    .prologue
    .line 522
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    .line 524
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 525
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 548
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method private initTextureView()Landroid/view/View;
    .locals 2

    .prologue
    .line 488
    new-instance v0, Landroid/view/TextureView;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    .line 489
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 517
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    return-object v0
.end method

.method private isDataSourceSpecified()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMediaNonError()Z
    .locals 2

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getState()I

    move-result v0

    .line 366
    .local v0, "mediaPlayerState":I
    if-eqz v0, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private shouldUseTextureView()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->fullScreenIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showProgress(Z)V
    .locals 2
    .param p1, "showing"    # Z

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 183
    return-void

    .line 182
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private unbindSurface()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 563
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_0

    .line 564
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldUseTextureView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 570
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method


# virtual methods
.method public canPause()Z
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x1

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x1

    return v0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->getBufferPercent()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 392
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaNonError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaPlayable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getState()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 398
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->getDuration()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public initMediaPlayer()V
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayer(I)V

    .line 203
    return-void
.end method

.method protected isMediaPlayable()Z
    .locals 2

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getState()I

    move-result v0

    .line 357
    .local v0, "mediaPlayerState":I
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaNonError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCompleted()V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v4, 0x0

    .line 321
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eq p1, v0, :cond_0

    .line 322
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Ignoring unexpected call to onCompletion()"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    :goto_0
    return-void

    .line 325
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "MediaView/%d completed. File: %s, URI: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onCompleted()V

    .line 327
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStopped()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 339
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 340
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "Releasing player for MediaView/%d, for File: %s, URI: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    aput-object v5, v4, v0

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v2, :cond_0

    .line 343
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->release()V

    .line 344
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-nez v2, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 346
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 344
    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 582
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getWidth()I

    move-result v3

    .line 583
    .local v3, "childWidth":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getHeight()I

    move-result v0

    .line 584
    .local v0, "childHeight":I
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    if-lez v8, :cond_0

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    if-gtz v8, :cond_3

    .line 593
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getWidth()I

    move-result v8

    sub-int/2addr v8, v3

    div-int/lit8 v1, v8, 0x2

    .line 594
    .local v1, "childLeft":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getHeight()I

    move-result v8

    sub-int/2addr v8, v0

    div-int/lit8 v2, v8, 0x2

    .line 595
    .local v2, "childTop":I
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    if-eqz v8, :cond_1

    .line 596
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->textureView:Landroid/view/TextureView;

    add-int v9, v1, v3

    add-int v10, v2, v0

    invoke-virtual {v8, v1, v2, v9, v10}, Landroid/view/TextureView;->layout(IIII)V

    .line 598
    :cond_1
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    if-eqz v8, :cond_2

    .line 599
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceView:Landroid/view/SurfaceView;

    add-int v9, v1, v3

    add-int v10, v2, v0

    invoke-virtual {v8, v1, v2, v9, v10}, Landroid/view/SurfaceView;->layout(IIII)V

    .line 603
    :cond_2
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v8}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v7

    .line 604
    .local v7, "progressWidth":I
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v8}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v4

    .line 605
    .local v4, "progressHeight":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getWidth()I

    move-result v8

    sub-int/2addr v8, v7

    div-int/lit8 v5, v8, 0x2

    .line 606
    .local v5, "progressLeft":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getHeight()I

    move-result v8

    sub-int/2addr v8, v4

    div-int/lit8 v6, v8, 0x2

    .line 607
    .local v6, "progressTop":I
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    add-int v9, v5, v7

    add-int v10, v6, v4

    invoke-virtual {v8, v5, v6, v9, v10}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 609
    return-void

    .line 586
    .end local v1    # "childLeft":I
    .end local v2    # "childTop":I
    .end local v4    # "progressHeight":I
    .end local v5    # "progressLeft":I
    .end local v6    # "progressTop":I
    .end local v7    # "progressWidth":I
    :cond_3
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getWidth()I

    move-result v9

    mul-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getHeight()I

    move-result v9

    iget v10, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    mul-int/2addr v9, v10

    if-le v8, v9, :cond_4

    .line 588
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    mul-int/2addr v8, v0

    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    div-int v3, v8, v9

    goto :goto_0

    .line 591
    :cond_4
    iget v8, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    mul-int/2addr v8, v3

    iget v9, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    div-int v0, v8, v9

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v1, 0x0

    .line 574
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 575
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    .line 576
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ProgressBar;->measure(II)V

    .line 577
    return-void
.end method

.method protected onPaused()V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setKeepScreenOn(Z)V

    .line 462
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v5, 0x0

    .line 294
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eq p1, v0, :cond_1

    .line 295
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Ignoring unexpected call to onPrepared()"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "MediaView/%d prepared. File: %s, URI: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->file:Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->seekTo(I)V

    .line 301
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPrepared(): auto-starting."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->start()V

    goto :goto_0
.end method

.method public onRelease()V
    .locals 5

    .prologue
    .line 239
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Media player released from MediaView/%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    .line 241
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStopped()V

    .line 242
    return-void
.end method

.method protected onStarted()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 444
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "start(): Starting playback for MediaView/%d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->showProgress(Z)V

    .line 446
    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setKeepScreenOn(Z)V

    .line 447
    return-void
.end method

.method protected onStopped()V
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->setKeepScreenOn(Z)V

    .line 476
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 650
    const/4 v0, 0x1

    return v0
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eq p1, v0, :cond_0

    .line 310
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "Ignoring unexpected call to onVideoSizeChanged()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    :goto_0
    return-void

    .line 313
    :cond_0
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoWidth:I

    .line 314
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->videoHeight:I

    .line 315
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->requestLayout()V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 451
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    .line 452
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getState()I

    move-result v0

    .line 453
    .local v0, "mediaPlayerState":I
    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 455
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->pause()V

    .line 457
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onPaused()V

    .line 458
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->seekTo(I)V

    .line 483
    :cond_0
    return-void
.end method

.method public setAttachmentId(Ljava/lang/String;)V
    .locals 7
    .param p1, "attachmentId"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    sget-object v5, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, p1, v5, v6}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->resolve(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Lcom/google/apps/dots/android/newsstand/server/Transform;)Lcom/google/apps/dots/android/newsstand/store/ResourceLink;

    move-result-object v3

    iget-object v0, v3, Lcom/google/apps/dots/android/newsstand/store/ResourceLink;->uri:Landroid/net/Uri;

    .line 132
    .local v0, "attachmentUri":Landroid/net/Uri;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v3, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v2, p1, v3}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 135
    .local v2, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    const-string v3, "http"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 138
    .local v1, "canStream":Z
    const/4 v1, 0x0

    .line 140
    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->availableVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .line 143
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 144
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->nsStore()Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v4, v5, v2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;

    invoke-direct {v5, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/MediaView;Landroid/net/Uri;)V

    .line 143
    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 157
    return-void
.end method

.method public setVideoUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->uri:Landroid/net/Uri;

    .line 161
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayerDataSource()V

    .line 162
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 413
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->display:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 414
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->surfaceAvailable:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/widget/WidgetUtil;->isVisible(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    .line 417
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "start(): Waiting on surface for MediaView/%d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 418
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->showProgress(Z)V

    .line 441
    :goto_0
    return-void

    .line 419
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaPlayable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 420
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->release()V

    .line 424
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 426
    :cond_1
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    .line 427
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->initMediaPlayer()V

    .line 428
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "start(): Waiting on media player for MediaView/%d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 429
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->showProgress(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 424
    goto :goto_1

    .line 431
    :cond_3
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    .line 432
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->start()V

    .line 435
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 436
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStarted()V

    goto :goto_0

    .line 438
    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStopped()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 465
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->shouldAutoStart:Z

    .line 466
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->isMediaPlayable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getState()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->mediaPlayer:Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/media/ManagedMediaPlayer;->stop()V

    .line 469
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStopped()V

    .line 470
    return-void
.end method

.method public triggerFullScreen()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->fullScreenIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->options:Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;->fullScreenIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 172
    :cond_0
    return-void
.end method
