.class Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;
.super Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.source "MediaDrawerPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->setupPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;
    .param p2, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;-><init>(Landroid/support/v4/view/NSViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageSelected(IZ)V
    .locals 6
    .param p1, "visualPosition"    # I
    .param p2, "userDriven"    # Z

    .prologue
    .line 115
    if-eqz p2, :cond_0

    .line 116
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->adapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$000(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 117
    .local v0, "logicalPosition":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .line 118
    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->pagerList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$100(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    sget v4, Lcom/google/apps/dots/android/newsstand/media/MediaItemsSource;->DK_MEDIA_ITEM:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 119
    .local v1, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .line 120
    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v4, v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->postId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-boolean v5, v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->restrictToSingleField:Z

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v2, v1, v4, v5, v3}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;-><init>(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Ljava/lang/String;ZLcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 121
    .local v2, "newState":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->changeState(Landroid/os/Parcelable;Z)V

    .line 123
    .end local v0    # "logicalPosition":I
    .end local v1    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .end local v2    # "newState":Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragmentState;
    :cond_0
    return-void
.end method
