.class public final Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplicationSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;


# instance fields
.field public appFamilyId:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field private appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

.field private bitField0_:I

.field private description_:Ljava/lang/String;

.field private headerBackgroundColor_:Ljava/lang/String;

.field private iconAttachmentId_:Ljava/lang/String;

.field private iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private languageCode_:Ljava/lang/String;

.field private leadCurationClientEntityId_:Ljava/lang/String;

.field private meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

.field public meteredSectionIds:[Ljava/lang/String;

.field public previewAttachmentId:[Ljava/lang/String;

.field private publicationDate_:J

.field private title_:Ljava/lang/String;

.field private translationCode_:Ljava/lang/String;

.field private translationEnabled_:Z

.field public trendingNewsCategory:[Ljava/lang/String;

.field private trendingOn_:J

.field private type_:I

.field private untranslatedAppFamilyId_:Ljava/lang/String;

.field private untranslatedAppId_:Ljava/lang/String;

.field private updateTime_:J

.field private useHeaderBackgroundImages_:Z

.field private userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5290
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    .line 5291
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5633
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 5636
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 5658
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 5661
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 5683
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    .line 5702
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    .line 5724
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    .line 5765
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    .line 5768
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    .line 5790
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    .line 5809
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    .line 5812
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    .line 5831
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    .line 5869
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    .line 5911
    const-string v0, "#000000"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    .line 5933
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    .line 5952
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    .line 5972
    const-string v0, "und"

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    .line 5994
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6016
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 5291
    return-void
.end method


# virtual methods
.method public clearTranslationCode()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 6010
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6011
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 6012
    return-object p0
.end method

.method public clearUntranslatedAppFamilyId()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 5652
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 5653
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 5654
    return-object p0
.end method

.method public clearUntranslatedAppId()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1

    .prologue
    .line 5677
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 5678
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 5679
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 3

    .prologue
    .line 6071
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6075
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 6076
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 6078
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 6079
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    .line 6081
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 6082
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    .line 6084
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-eqz v2, :cond_3

    .line 6085
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->clone()Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 6087
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 6088
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    .line 6090
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v2, :cond_5

    .line 6091
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 6093
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-eqz v2, :cond_6

    .line 6094
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 6096
    :cond_6
    return-object v0

    .line 6072
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :catch_0
    move-exception v1

    .line 6073
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5287
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6101
    if-ne p1, p0, :cond_1

    .line 6127
    :cond_0
    :goto_0
    return v1

    .line 6102
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 6103
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 6104
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6105
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6106
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6107
    :goto_4
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6109
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6110
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 6111
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    .line 6112
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6113
    :goto_8
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    .line 6115
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-nez v3, :cond_3

    .line 6118
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    .line 6119
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_3

    .line 6120
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-nez v3, :cond_3

    .line 6121
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6122
    :goto_c
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6125
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6126
    :goto_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 6127
    goto/16 :goto_0

    .line 6104
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6105
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 6106
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 6107
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    .line 6109
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    .line 6110
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 6111
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    .line 6112
    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    .line 6113
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    .line 6115
    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 6118
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    .line 6119
    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 6120
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 6121
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    .line 6122
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    .line 6125
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6126
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 6127
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;
    .locals 1

    .prologue
    .line 5893
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5770
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5913
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    return-object v0
.end method

.method public getIconAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5726
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 5748
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5974
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getLeadCurationClientEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6018
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicationDate()J
    .locals 2

    .prologue
    .line 5833
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 6255
    const/4 v2, 0x0

    .line 6256
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 6257
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6258
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 6259
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6260
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_0

    .line 6261
    const/4 v4, 0x3

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    .line 6262
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 6264
    :cond_0
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_1

    .line 6265
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    .line 6266
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6268
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_2

    .line 6269
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    .line 6270
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6272
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_3

    .line 6273
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    .line 6274
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6276
    :cond_3
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_4

    .line 6277
    const/16 v4, 0x8

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    .line 6278
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 6280
    :cond_4
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_6

    .line 6281
    const/4 v0, 0x0

    .line 6282
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v1, v5, v4

    .line 6284
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 6282
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 6286
    .end local v1    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v2, v0

    .line 6287
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 6289
    .end local v0    # "dataSize":I
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_7

    .line 6290
    const/16 v4, 0xb

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    .line 6291
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 6293
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_8

    .line 6294
    const/16 v4, 0xc

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    .line 6295
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 6297
    :cond_8
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-eqz v4, :cond_9

    .line 6298
    const/16 v4, 0xe

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 6299
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6301
    :cond_9
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_b

    .line 6302
    const/4 v0, 0x0

    .line 6303
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_a

    aget-object v1, v5, v4

    .line 6305
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 6303
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 6307
    .end local v1    # "element":Ljava/lang/String;
    :cond_a
    add-int/2addr v2, v0

    .line 6308
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 6310
    .end local v0    # "dataSize":I
    :cond_b
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_c

    .line 6311
    const/16 v4, 0x10

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 6312
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6314
    :cond_c
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v4, :cond_d

    .line 6315
    const/16 v4, 0x11

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 6316
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6318
    :cond_d
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_f

    .line 6319
    const/4 v0, 0x0

    .line 6320
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_e

    aget-object v1, v4, v3

    .line 6322
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 6320
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 6324
    .end local v1    # "element":Ljava/lang/String;
    :cond_e
    add-int/2addr v2, v0

    .line 6325
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 6327
    .end local v0    # "dataSize":I
    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-eqz v3, :cond_10

    .line 6328
    const/16 v3, 0x13

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 6329
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6331
    :cond_10
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    if-eqz v3, :cond_11

    .line 6332
    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    .line 6333
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6335
    :cond_11
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x400

    if-eqz v3, :cond_12

    .line 6336
    const/16 v3, 0x15

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    .line 6337
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 6339
    :cond_12
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x800

    if-eqz v3, :cond_13

    .line 6340
    const/16 v3, 0x16

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    .line 6341
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 6343
    :cond_13
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x1000

    if-eqz v3, :cond_14

    .line 6344
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    .line 6345
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6347
    :cond_14
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x2000

    if-eqz v3, :cond_15

    .line 6348
    const/16 v3, 0x18

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6349
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6351
    :cond_15
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_16

    .line 6352
    const/16 v3, 0x19

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6353
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6355
    :cond_16
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_17

    .line 6356
    const/16 v3, 0x1a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 6357
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6359
    :cond_17
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v3, v3, 0x4000

    if-eqz v3, :cond_18

    .line 6360
    const/16 v3, 0x1e

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 6361
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6363
    :cond_18
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->cachedSize:I

    .line 6364
    return v2
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5704
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslationCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5996
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslationEnabled()Z
    .locals 1

    .prologue
    .line 5954
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 5792
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    return-wide v0
.end method

.method public getUseHeaderBackgroundImages()Z
    .locals 1

    .prologue
    .line 5935
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    return v0
.end method

.method public hasHeaderBackgroundColor()Z
    .locals 1

    .prologue
    .line 5924
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIconAttachmentId()Z
    .locals 1

    .prologue
    .line 5737
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIconImage()Z
    .locals 1

    .prologue
    .line 5758
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLanguageCode()Z
    .locals 1

    .prologue
    .line 5985
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTranslationCode()Z
    .locals 1

    .prologue
    .line 6007
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v10, 0x20

    const/4 v3, 0x0

    .line 6131
    const/16 v1, 0x11

    .line 6132
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 6133
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 6134
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 6135
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 6136
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 6137
    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    add-int v1, v2, v6

    .line 6138
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 6139
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    .line 6140
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_9

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    .line 6141
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    if-nez v2, :cond_a

    mul-int/lit8 v1, v1, 0x1f

    .line 6147
    :cond_0
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    .line 6148
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 6149
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    if-nez v2, :cond_d

    mul-int/lit8 v1, v1, 0x1f

    .line 6155
    :cond_1
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 6156
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 6157
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-nez v2, :cond_f

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    .line 6158
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    if-nez v2, :cond_10

    mul-int/lit8 v1, v1, 0x1f

    .line 6164
    :cond_2
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v2, :cond_12

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    .line 6165
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-nez v2, :cond_13

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    .line 6166
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    .line 6167
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    if-eqz v2, :cond_15

    move v2, v4

    :goto_c
    add-int v1, v6, v2

    .line 6168
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    if-eqz v6, :cond_16

    :goto_d
    add-int v1, v2, v4

    .line 6169
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    if-nez v2, :cond_17

    move v2, v3

    :goto_e
    add-int v1, v4, v2

    .line 6170
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    if-nez v2, :cond_18

    move v2, v3

    :goto_f
    add-int v1, v4, v2

    .line 6171
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v4, :cond_19

    :goto_10
    add-int v1, v2, v3

    .line 6172
    return v1

    .line 6133
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 6134
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 6135
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 6136
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 6138
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 6139
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 6140
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 6143
    :cond_a
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 6144
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_b

    move v2, v3

    :goto_12
    add-int v1, v6, v2

    .line 6143
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 6144
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_12

    .line 6147
    .end local v0    # "i":I
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 6151
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 6152
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v3

    :goto_14
    add-int v1, v6, v2

    .line 6151
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 6152
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_14

    .line 6157
    .end local v0    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 6160
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 6161
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_11

    move v2, v3

    :goto_16
    add-int v1, v6, v2

    .line 6160
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 6161
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_16

    .line 6164
    .end local v0    # "i":I
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 6165
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 6166
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    :cond_15
    move v2, v5

    .line 6167
    goto/16 :goto_c

    :cond_16
    move v4, v5

    .line 6168
    goto/16 :goto_d

    .line 6169
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 6170
    :cond_18
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 6171
    :cond_19
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto/16 :goto_10
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 6372
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 6373
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 6377
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 6378
    :sswitch_0
    return-object p0

    .line 6383
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    goto :goto_0

    .line 6387
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    goto :goto_0

    .line 6391
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 6392
    .local v4, "temp":I
    if-eqz v4, :cond_1

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 6396
    :cond_1
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    .line 6397
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto :goto_0

    .line 6399
    :cond_2
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    goto :goto_0

    .line 6404
    .end local v4    # "temp":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    .line 6405
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto :goto_0

    .line 6409
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    .line 6410
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto :goto_0

    .line 6414
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    .line 6415
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto :goto_0

    .line 6419
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    .line 6420
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto :goto_0

    .line 6424
    :sswitch_8
    const/16 v5, 0x52

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 6425
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v1, v5

    .line 6426
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 6427
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6428
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    .line 6429
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 6430
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 6431
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 6429
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6434
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 6438
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    .line 6439
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6443
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    .line 6444
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6448
    :sswitch_b
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-nez v5, :cond_4

    .line 6449
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$RoleList;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    .line 6451
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 6455
    :sswitch_c
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 6456
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v1, v5

    .line 6457
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 6458
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6459
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    .line 6460
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 6461
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 6462
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 6460
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6465
    :cond_5
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 6469
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_d
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_6

    .line 6470
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 6472
    :cond_6
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 6476
    :sswitch_e
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v5, :cond_7

    .line 6477
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 6479
    :cond_7
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 6483
    :sswitch_f
    const/16 v5, 0x92

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 6484
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v1, v5

    .line 6485
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 6486
    .restart local v2    # "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6487
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    .line 6488
    :goto_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_8

    .line 6489
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 6490
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 6488
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6493
    :cond_8
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto/16 :goto_0

    .line 6497
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_10
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-nez v5, :cond_9

    .line 6498
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 6500
    :cond_9
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 6504
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    .line 6505
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6509
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    .line 6510
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6514
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    .line 6515
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6519
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    .line 6520
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6524
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6525
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6529
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6530
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6534
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 6535
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6539
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 6540
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    goto/16 :goto_0

    .line 6373
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0x92 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
        0xa8 -> :sswitch_12
        0xb0 -> :sswitch_13
        0xba -> :sswitch_14
        0xc2 -> :sswitch_15
        0xca -> :sswitch_16
        0xd2 -> :sswitch_17
        0xf2 -> :sswitch_18
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5287
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    return-object v0
.end method

.method public setAppType(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .prologue
    .line 5896
    if-nez p1, :cond_0

    .line 5897
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5899
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    .line 5900
    return-object p0
.end method

.method public setTranslationCode(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5999
    if-nez p1, :cond_0

    .line 6000
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6002
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    .line 6003
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 6004
    return-object p0
.end method

.method public setUntranslatedAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5641
    if-nez p1, :cond_0

    .line 5642
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5644
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 5645
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 5646
    return-object p0
.end method

.method public setUntranslatedAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5666
    if-nez p1, :cond_0

    .line 5667
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5669
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    .line 5670
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 5671
    return-object p0
.end method

.method public setUpdateTime(J)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 5795
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    .line 5796
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    .line 5797
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 6177
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6178
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6179
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_0

    .line 6180
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->type_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 6182
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_1

    .line 6183
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->title_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6185
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 6186
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6188
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_3

    .line 6189
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->description_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6191
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_4

    .line 6192
    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->updateTime_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 6194
    :cond_4
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 6195
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingNewsCategory:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_5

    aget-object v0, v3, v2

    .line 6196
    .local v0, "element":Ljava/lang/String;
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6199
    .end local v0    # "element":Ljava/lang/String;
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_6

    .line 6200
    const/16 v2, 0xb

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->trendingOn_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 6202
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_7

    .line 6203
    const/16 v2, 0xc

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->publicationDate_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 6205
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    if-eqz v2, :cond_8

    .line 6206
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->userRoles_:Lcom/google/apps/dots/proto/client/DotsShared$RoleList;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 6208
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 6209
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredSectionIds:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_9

    aget-object v0, v3, v2

    .line 6210
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6209
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6213
    .end local v0    # "element":Ljava/lang/String;
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_a

    .line 6214
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 6216
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v2, :cond_b

    .line 6217
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 6219
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 6220
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->previewAttachmentId:[Ljava/lang/String;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v0, v2, v1

    .line 6221
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v4, 0x12

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6220
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6224
    .end local v0    # "element":Ljava/lang/String;
    :cond_c
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    if-eqz v1, :cond_d

    .line 6225
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appType_:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 6227
    :cond_d
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_e

    .line 6228
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->headerBackgroundColor_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6230
    :cond_e
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_f

    .line 6231
    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->useHeaderBackgroundImages_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 6233
    :cond_f
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_10

    .line 6234
    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationEnabled_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 6236
    :cond_10
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_11

    .line 6237
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->languageCode_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6239
    :cond_11
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_12

    .line 6240
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->translationCode_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6242
    :cond_12
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_13

    .line 6243
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6245
    :cond_13
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_14

    .line 6246
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->untranslatedAppId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6248
    :cond_14
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_15

    .line 6249
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6251
    :cond_15
    return-void
.end method
