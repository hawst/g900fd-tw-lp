.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;
.super Ljava/lang/Object;
.source "SnapControlUtil.java"


# instance fields
.field private final snapControls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    return-void
.end method

.method public static adjustSnapControlsForLetterboxing(FLjava/util/List;)Ljava/util/List;
    .locals 6
    .param p0, "letterboxScale"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "snapControls":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 20
    .local v2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 21
    new-instance v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-direct {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;-><init>()V

    .line 22
    .local v0, "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getType()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->setType(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 23
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 24
    .local v3, "unzoomedX":I
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 25
    .local v4, "unzoomedY":I
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;-><init>()V

    invoke-virtual {v5, v3}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->setX(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->setY(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->setUnzoomedPoint(Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 26
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    .end local v0    # "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .end local v3    # "unzoomedX":I
    .end local v4    # "unzoomedY":I
    :cond_0
    return-object v2
.end method

.method public static isRecognizedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)Z
    .locals 1
    .param p0, "control"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getType()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->hasUnzoomedPoint()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUnzoomedScale(F)Z
    .locals 2
    .param p0, "scale"    # F

    .prologue
    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3dcccccd    # 0.1f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static nearAngleDotProduct(FFFF)F
    .locals 6
    .param p0, "dx"    # F
    .param p1, "dy"    # F
    .param p2, "vx"    # F
    .param p3, "vy"    # F

    .prologue
    .line 114
    mul-float v2, p2, p0

    mul-float v3, p3, p1

    add-float v1, v2, v3

    .line 116
    .local v1, "dot":F
    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    mul-float v3, p0, p0

    mul-float v4, p1, p1

    add-float/2addr v3, v4

    mul-float v4, p2, p2

    mul-float v5, p3, p3

    add-float/2addr v4, v5

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    .line 117
    .local v0, "anglesAreClose":Z
    :goto_0
    if-eqz v0, :cond_1

    .end local v1    # "dot":F
    :goto_1
    return v1

    .line 116
    .end local v0    # "anglesAreClose":Z
    .restart local v1    # "dot":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 117
    .restart local v0    # "anglesAreClose":Z
    :cond_1
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_1
.end method


# virtual methods
.method public getIndexOfSnapControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)I
    .locals 1
    .param p1, "control"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getNearestSnapControlTo(FFF)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F

    .prologue
    .line 61
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->isUnzoomedScale(F)Z

    move-result v8

    if-nez v8, :cond_1

    .line 62
    const/4 v5, 0x0

    .line 80
    :cond_0
    return-object v5

    .line 64
    :cond_1
    const/4 v5, 0x0

    .line 65
    .local v5, "nearestControl":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    const v6, 0x7f7fffff    # Float.MAX_VALUE

    .line 66
    .local v6, "nearestDistance":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 67
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 68
    .local v0, "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->isRecognizedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 66
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v7

    .line 72
    .local v7, "point":Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v8

    int-to-float v8, v8

    sub-float v2, v8, p1

    .line 73
    .local v2, "dx":F
    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v8

    int-to-float v8, v8

    sub-float v3, v8, p2

    .line 74
    .local v3, "dy":F
    mul-float v8, v2, v2

    mul-float v9, v3, v3

    add-float v1, v8, v9

    .line 75
    .local v1, "distance":F
    cmpg-float v8, v1, v6

    if-gez v8, :cond_2

    .line 76
    move v6, v1

    .line 77
    move-object v5, v0

    goto :goto_1
.end method

.method public getNearestSnapControlTo(FFFFF)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "scale"    # F
    .param p4, "vx"    # F
    .param p5, "vy"    # F

    .prologue
    .line 88
    invoke-static {p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->isUnzoomedScale(F)Z

    move-result v6

    if-nez v6, :cond_1

    .line 89
    const/4 v3, 0x0

    .line 106
    :cond_0
    return-object v3

    .line 91
    :cond_1
    const/4 v3, 0x0

    .line 92
    .local v3, "nearestControl":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    const v4, 0x7f7fffff    # Float.MAX_VALUE

    .line 93
    .local v4, "nearestDistance":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 94
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    .line 95
    .local v0, "control":Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->isRecognizedControl(Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 93
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 98
    :cond_3
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;->getUnzoomedPoint()Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;

    move-result-object v5

    .line 100
    .local v5, "point":Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getX()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, p1

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsNativeBody$Point;->getY()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v7, p2

    invoke-static {v6, v7, p4, p5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->nearAngleDotProduct(FFFF)F

    move-result v1

    .line 101
    .local v1, "distance":F
    const/4 v6, 0x0

    cmpg-float v6, v6, v1

    if-gez v6, :cond_2

    cmpg-float v6, v1, v4

    if-gez v6, :cond_2

    .line 102
    move v4, v1

    .line 103
    move-object v3, v0

    goto :goto_1
.end method

.method public getSnapControl(I)Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;

    return-object v0
.end method

.method public getSnapControlCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public set(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "snapControls":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/proto/client/DotsNativeBody$SnapControl;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 33
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/SnapControlUtil;->snapControls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 34
    return-void
.end method
