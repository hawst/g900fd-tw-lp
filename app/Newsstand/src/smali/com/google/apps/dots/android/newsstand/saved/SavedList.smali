.class public Lcom/google/apps/dots/android/newsstand/saved/SavedList;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.source "SavedList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/saved/SavedList$SavedCardListVisitor;
    }
.end annotation


# instance fields
.field private cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

.field connectivityListener:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/saved/SavedList;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    return-object v0
.end method


# virtual methods
.method public cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 68
    .local v0, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->rawCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 70
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    new-instance v2, Lcom/google/android/libraries/bind/data/StaticDataProvider;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_empty_bookmark:I

    sget v5, Lcom/google/android/apps/newsstanddev/R$string;->empty_list_caption_bookmarks:I

    invoke-static {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->makeStandardEmptyCardData(Landroid/content/Context;II)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/libraries/bind/data/StaticDataProvider;-><init>(Lcom/google/android/libraries/bind/data/Data;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/card/CardGroup;->setEmptyRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 74
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList$2;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/card/CardGroup;->setErrorRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 82
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->inContext(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;->addDismissibleBookmarksCardIfNeeded(Lcom/google/android/libraries/bind/card/CardListBuilder;)Lcom/google/apps/dots/android/newsstand/card/WarmWelcomeHelper;

    .line 83
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 84
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 85
    return-object v0
.end method

.method protected getRetryRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$3;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList$3;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)V

    return-object v0
.end method

.method protected onPreferencesChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "optPreferenceKey"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->onPreferencesChanged(Ljava/lang/String;)V

    .line 60
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->preferenceAffectsApiUri(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->updateErrorView()V

    .line 63
    :cond_0
    return-void
.end method

.method protected onRegisterForInvalidation()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->onRegisterForInvalidation()V

    .line 40
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/saved/SavedList$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/saved/SavedList$1;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedList;)V

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->addConnectivityListener(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->connectivityListener:Ljava/lang/Runnable;

    .line 46
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->onUnregisterForInvalidation()V

    .line 51
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->connectivityListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->connectivityListener:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->removeConnectivityListener(Ljava/lang/Runnable;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->connectivityListener:Ljava/lang/Runnable;

    .line 55
    :cond_0
    return-void
.end method

.method protected updateErrorView()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 92
    :cond_0
    return-void
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 108
    new-instance v0, Lcom/google/apps/dots/android/newsstand/saved/SavedList$SavedCardListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/saved/SavedList$SavedCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/saved/SavedList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
