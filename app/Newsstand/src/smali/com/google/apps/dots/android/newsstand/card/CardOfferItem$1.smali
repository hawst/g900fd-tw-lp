.class final Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
.source "CardOfferItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardOfferItem;->fillOffersCardData(Lcom/google/android/libraries/bind/data/Data;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;Lcom/google/android/libraries/bind/data/DataList;ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$analyticsReadingScreen:Ljava/lang/String;

.field final synthetic val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$analyticsReadingScreen:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .locals 4

    .prologue
    .line 243
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$analyticsReadingScreen:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$offerEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->val$offerSummary:Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferSeenEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardOfferItem$1;->get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    move-result-object v0

    return-object v0
.end method
