.class final Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardNewsItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardNewsItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;ZZZZLjava/lang/String;Lcom/google/android/libraries/bind/data/Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$postId:Ljava/lang/String;

.field final synthetic val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;->val$postId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 364
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;->val$postId:Ljava/lang/String;

    .line 365
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 366
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->restrictToSingleField(Z)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/card/CardNewsItem$7;->val$readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 367
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/MediaDrawerIntentBuilder;->start()V

    .line 369
    return-void
.end method
