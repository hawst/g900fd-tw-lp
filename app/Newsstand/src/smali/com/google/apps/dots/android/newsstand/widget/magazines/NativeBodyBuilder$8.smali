.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;
.super Landroid/widget/BaseAdapter;
.source "NativeBodyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPartAdapter(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)Landroid/widget/ListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

.field final synthetic val$children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

.field final synthetic val$parentPartContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    .prologue
    .line 481
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$parentPartContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 494
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "index"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 500
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$parentPartContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->forChild()Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->eventScope(Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;

    move-result-object v0

    .line 501
    .local v0, "childContext":Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$8;->val$children:[Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;

    aget-object v2, v2, p1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->buildPart(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V
    invoke-static {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;)V

    .line 502
    iget-object v1, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyBuilder$PartContext;->view:Landroid/view/View;

    return-object v1
.end method
