.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Form;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 186
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->formTemplateStore()Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->loadToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getPostTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/store/cache/FormTemplateStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;)V

    .line 185
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 182
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$4;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
