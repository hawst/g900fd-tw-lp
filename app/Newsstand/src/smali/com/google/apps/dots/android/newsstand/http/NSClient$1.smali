.class Lcom/google/apps/dots/android/newsstand/http/NSClient$1;
.super Ljava/lang/Object;
.source "NSClient.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/NSClient;->request(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

.field final synthetic val$isDotsBackend:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/http/NSClient;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$isDotsBackend:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 173
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->apply(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;

    .line 177
    .local v3, "httpClient":Lcom/google/apps/dots/android/newsstand/http/NSHttpClient;
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 178
    .local v6, "clientConfig":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v0, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSClient;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$clientRequest:Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;

    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/http/NSClient$1;->val$isDotsBackend:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequestCommand;-><init>(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lorg/apache/http/client/HttpClient;Lcom/google/apps/dots/android/newsstand/http/NSClient$ClientRequest;ZLcom/google/apps/dots/proto/client/DotsShared$ClientConfig;)V

    # invokes: Lcom/google/apps/dots/android/newsstand/http/NSClient;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v7, v8, v0}, Lcom/google/apps/dots/android/newsstand/http/NSClient;->access$000(Lcom/google/apps/dots/android/newsstand/http/NSClient;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
