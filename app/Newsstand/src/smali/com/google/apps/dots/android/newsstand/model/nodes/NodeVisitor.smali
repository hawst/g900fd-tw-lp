.class public interface abstract Lcom/google/apps/dots/android/newsstand/model/nodes/NodeVisitor;
.super Ljava/lang/Object;
.source "NodeVisitor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeTraversalT::",
        "Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")V"
        }
    .end annotation
.end method

.method public abstract visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeTraversalT;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;",
            ")V"
        }
    .end annotation
.end method
