.class Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "MagazineEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 256
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$300(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 257
    # invokes: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->inLiteMode()Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$200(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setInLiteMode(Z)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2$1;->this$1:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;

    .line 258
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->heroLogo:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;->access$100(Lcom/google/apps/dots/android/newsstand/edition/MagazineEditionFragment;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v0

    const/16 v1, 0xc8

    .line 259
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->startForResult(I)V

    .line 260
    return-void
.end method
