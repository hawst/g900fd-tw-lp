.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;
.super Ljava/lang/Object;
.source "TouchSlopInterceptor.java"


# instance fields
.field private hasReference:Z

.field private interceptable:Z

.field private referenceX:F

.field private referenceY:F

.field private final touchSlop:I

.field public wasOutsideHorizontalSlop:Z

.field public wasOutsideSlop:Z

.field public wasOutsideVerticalSlop:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->touchSlop:I

    .line 28
    return-void
.end method

.method private resetReference(Z)V
    .locals 1
    .param p1, "hasReference"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->hasReference:Z

    .line 40
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideHorizontalSlop:Z

    .line 41
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideVerticalSlop:Z

    .line 42
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideSlop:Z

    .line 43
    return-void
.end method


# virtual methods
.method public getReferenceX()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceX:F

    return v0
.end method

.method public getReferenceY()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceY:F

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 56
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_7

    move v0, v1

    .line 57
    .local v0, "actionDown":Z
    :goto_0
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->interceptable:Z

    or-int/2addr v3, v0

    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->interceptable:Z

    .line 58
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->interceptable:Z

    if-eqz v3, :cond_6

    .line 60
    if-nez v0, :cond_0

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->hasReference:Z

    if-nez v3, :cond_1

    .line 61
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->resetReference(Z)V

    .line 62
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceX:F

    .line 63
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceY:F

    .line 66
    :cond_1
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideHorizontalSlop:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceX:F

    .line 67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->touchSlop:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_8

    :cond_2
    move v3, v1

    :goto_1
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideHorizontalSlop:Z

    .line 68
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideVerticalSlop:Z

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->referenceY:F

    .line 69
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->touchSlop:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_9

    :cond_3
    move v3, v1

    :goto_2
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideVerticalSlop:Z

    .line 70
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideHorizontalSlop:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideVerticalSlop:Z

    if-eqz v3, :cond_5

    :cond_4
    move v2, v1

    :cond_5
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->wasOutsideSlop:Z

    .line 72
    :cond_6
    return-object p0

    .end local v0    # "actionDown":Z
    :cond_7
    move v0, v2

    .line 56
    goto :goto_0

    .restart local v0    # "actionDown":Z
    :cond_8
    move v3, v2

    .line 67
    goto :goto_1

    :cond_9
    move v3, v2

    .line 69
    goto :goto_2
.end method

.method public setInterceptable(Z)V
    .locals 1
    .param p1, "interceptable"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->interceptable:Z

    .line 50
    if-nez p1, :cond_0

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TouchSlopInterceptor;->resetReference(Z)V

    .line 53
    :cond_0
    return-void
.end method
